<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="taglib.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流量充值</title>
<link rel="stylesheet" href="static/css/global.css">
<link rel="stylesheet" href="static/semantic/semantic.min.css">
<link rel="stylesheet" href="static/datatables/css/dataTables.semanticui.min.css">
<link rel="stylesheet" href="static/calendar/calendar.css">
<style type="text/css">
#top_menu a {
	font-family: "Microsoft Yahei" !important;
}
</style>
<script src="static/js/jquery-3.1.0.min.js"></script>
<script src="static/js/ajax-noauth.js"></script>
<script src="static/semantic/semantic.min.js"></script>
<script src="static/calendar/calendar.js"></script>
<script src="static/datatables/js/jquery.dataTables.min.js"></script>
<script src="static/datatables/js/dataTables.semanticui.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(window).bind('hashchange', function() {
			triggerMenu();
		});
		
		var links = $('#top_menu').children('a');
		links.click(function(){
			if(window.location.hash.replace('#','')==links.index(this)) {
				triggerMenu();	//点击当前选中的菜单时同样生效.
			} else {
				window.location.hash=(links.index(this));	//修改后会触发hashchange.
			}
			return false
		});
		triggerMenu();
	})
	
	function triggerMenu() {
		var i=window.location.hash.replace('#','')||0;
		var links = $('#top_menu').children('a');
		if(links.length>0) {
			links.removeClass('active');
			var $menu = $(links[i]);
			$menu.addClass('active');
			$('#center').html('<div class="ui large active centered text loader"></div>').load($menu.attr('href'));
			window.scrollTo(0,0);
		}
	}
</script>
</head>
<body style="padding: 6% 0% 0; overflow-y:scroll;">

<div id="top_navbar_main" class="ui top inverted fixed menu primary-color height-1 " style="">
	<div id="top_menu" class="ui large secondary inverted pointing menu container" style="width:80%; border-width: 0 0 2px 0">
        <img class="ui item" src="static/img/brand.png" width="160px" style="padding:7px; margin-right: 20px">
        <c:forEach items="${menus}" var="v">
        <a class="item" href="${v.url}"><i class="${v.attr1}"></i>${v.name}</a>
        </c:forEach>
        <div class="right item">
			<i class="user icon"></i>${sessionScope.whoami.account}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		      	<a style="color:white" href="passport/logout.do" class="navbar-link"><i class="sign out icon"></i>退出</a>
	    </div>
	</div>
</div>
 <div id="center"></div>
 
</body>
</html>
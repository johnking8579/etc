<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<style type="text/css">
div.slider {
    display: none;
    margin: 0 20px 0;
}
</style>
<script src="static/js/jquery.form.js"></script>
<script src="static/js/my.js"></script>

<div style="margin: 0 12% 0;">
<div class="ui grid" style="margin-bottom: -2rem" id="mygrid">
	<div class="fourteen wide column">
	 	 <form id="searchForm" class="ui tiny form " onsubmit="return false;">
	 	 	<div class="inline fields">
		 	 	<div class="field">
			       <label class="bold">厂商</label>
					 <select class="ui dropdown" name="channel">
					  <option value="null">默认</option>
					 <c:forEach items="${channels}" var="v">
					  <option value="${v.key}">${v.value}</option>
					 </c:forEach>
					</select>
				</div>
	 	 		<div class="field">
			       	<label class="bold">运营商</label>
					<select class="ui dropdown" name="operators">
					  <c:forEach items="${operators}" var="v">
					  <option value="${v.key}">${v.value}</option>
					  </c:forEach>
					</select>
				</div>
			</div>
		  </form>
	</div>
	<div class="two wide column">
	
	 <div class="ui primary buttons" >
	  <div class="ui  fluid button" id="addBtn" onclick="$('#addWindow').modal(pkg.modalOpt).modal('show');">添加</div>
	  <div class="ui floating dropdown icon button" id="foldBtn">
	    <i class="dropdown icon"></i>
	    <div class="menu">
	      <div class="item" onclick="pkg.showRestore()"><i class="history icon"/>还原..</div> 
	    </div>
	  </div>
	</div>
	 
 </div>
 </div>
 

<div id="restoreWindow" class="ui small modal">
<div class="heiti header">选择要还原的厂商流量包</div>
<div class="content">
	<div class="ui one column relaxed equal height divided grid" id="grid">
	    <div class="column">
	      <div class="ui link list">
	       <c:forEach items="${channels}" var="v">
	        <a class="item">&nbsp;&nbsp;&nbsp;
	        	<div class="ui checkbox">
			      <input id="cbox_${v.key }" type="checkbox" value="${v.key}">
			      <label for="cbox_${v.key }">${v.value}</label>
			    </div>
	        </a>
	        </c:forEach>
	      </div>
	  </div>
	  </div>
</div>
<div class="actions">
 	<div class="ui cancel button">取消</div>
 	<div class="ui positive right labeled icon button">还原为默认配置<i class="checkmark icon"></i> </div>
 </div>
 </div>





 <!-- single line: 确保不换行 -->
	<table id="table" class="ui inverted brown table single line center aligned compact" cellspacing="0" width="100%">
		<thead class="">
			<tr>
				<th>#</th>
				<th>流量包</th>
				<th>标价</th>
				<th>售价</th>
				<th>售卖折扣</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
	
</div>
	
  <div class="ui popup" id="alert" style="min-width: 300px;">
	  <div class="header"><i class="warning sign icon"></i>删除?</div>
	  <div class="content">
	  <div class="" style="float: right;">
		  <div class="ui tiny button" onclick="$('.ui.popup').popup('hide all');">不了 </div>
	      <div class="ui red tiny button" onclick="pkg._delete(this)">是的</div>
	      <input type="hidden"/>
	  </div>
	  </div>
	</div>
  
  <div id="addWindow" class="ui small modal">
	  <div class="heiti header">编辑流量包</div>
	  <div class="content">
		  <form id="addForm"  method="post" action="api/saveFlowInfo" class="ui mini form">
		   	<div class="field">
				<label>流量包大小</label><input type="text" name="flowPackage">
			</div>
			<div class="field">
				<label >标价</label>
				<div class="ui right labeled input">
				  <input type="number" name="markedPrice">
				  <div class="ui basic label">元</div>
				</div>
			</div>
			<div class="field">
				<label>折扣</label>
				<div class="ui right labeled input">
				  <input type="number" value="10" step="0.01" id="rate">
				  <div class="ui basic label">折</div>
				</div>
			</div>
			<div class="field">
				<label>售价</label>
				<div class="ui right labeled input">
				  <input type="number" name="price" readonly="readonly">
				  <div class="ui basic label">元</div>
				</div>
			</div>
		  	<div class="ui error message"></div>
		  </form>
	  </div>
	  <div class="actions">
	  	<div class="ui cancel button">取消</div>
	  	<div class="ui positive right labeled icon button">提交<i class="checkmark icon"></i> </div>
	  </div>
  </div>
  
  <div id="formMsg" class="ui dimmer">
    <div class="content">
      <div class="center">
        <h2 class="ui inverted icon header"><i class="checkmark icon"></i> 操作成功 </h2>
      </div>
    </div>
  </div>
  
<script type="text/javascript">
var pkg = (function() {
	var table, tableOpt = {
		"ajax": {
            "url": "${path}/api/flowInfo",
            "type": "POST",
			"data":function(d){
				$.extend(d,serializeObj($('#searchForm')));
			}
     	},
		"searching": false,
		"ordering": false,
		"lengthChange": false,
		"lengthMenu": [ 10,100],
		"processing": true,
		//"pagingType":'simple',
        "serverSide": false,
        "columns": [
            { "data": "__dt__sequence__" },
            { "data": "flowPackage" },
            { "data": "markedPrice", "render": function ( data, type, row, meta) {
            	return data+'元';
            }},
            { "data": "price", "render": function ( data, type, row, meta) {
            	return data+'元';
            }},
            { "data": "rate"},
            { "data": null, "render": function ( data, type, row, meta) {	//must generate html, cannot bind dom event here
            	return '<button class="ui mini inverted button" onclick="pkg.edit('+meta.row+')"><i class="edit icon"></i>编辑</button>'
            	+'&nbsp;&nbsp;&nbsp;&nbsp;'
            	+'<button class="ui mini inverted button" delbtn data-value='+row.id+'><i class="remove icon"></i>删除</button>';
            }},
            { "data": "id",visible:false},
         ],
         "initComplete": function(settings, json) {
        	 //pkg.table.buttons().container().appendTo( $('div.eight.column:eq(0)', pkg.table.table().container()) ); //TODO 如何自定义btn位置
	   	 },
         fnRowCallback: function( nRow, aData, iDisplayIndex ) {
       	 },
         drawCallback: function() {
        	 $('button[delbtn]').popup({
	   			on:'click',
	   			hoverable:true,
	   			position:'top right',
	   			onShow: function(module) {
	   				$(this).find(':hidden').val($(module).attr('data-value'));
	   			},
	   		    popup : $('#alert')
	   		  })
	   		;
         },
	     "columnDefs": [
		 ]
	};
	
	var editData;
	
	var modalOpt={
			closable:false,
			//blurring: true,	//duration和blurring在chrome下帧数太低
			duration:100,
			onVisible : function(){
				var $form=$(this).find('form');
				$form.form({
				    fields: {
				    	flowPackage  : 'empty',
				        markedPrice : ['number','empty'],
				        price : ['number','empty']
			    	}
				});
				var markp=$form.find('[name=markedPrice]'),rate=$form.find('#rate'),price=$form.find('[name=price]');
				$.each(new Array(markp,rate),function(i,v){
					v.on('keyup change',function(){
						var a=new Number(markp.val()*rate.val()/10.0).toFixed(2);
						price.val(a);
					});
				});
				if(editData) {
					$.each(editData,function(i,val){
						try{$form.find('[name='+i+']').val(val);}catch(e){}
					})
					rate.val(price.val()/markp.val()*10);
				}
			},
			onHidden: function(){
				var $form=$(this).find('form');
				$form.find('.ui.error.message').html('');
				$form.form('reset');
				editData=null;
			},
			onApprove: function() {
				var form=$(this).find('form');
				if(!form.form('validate form'))return false; //不要直接使用domId. 原因和modal有关,具体不明
				var d={shop:$('[name=operators]').val(),
						channel:$('[name=channel]').val()};
				if(editData) d.id=editData.id;
				var ret;
				form.ajaxSubmit({
					async:false,//同步提交
					data:d,
					dataType:'json',
					success: function(data){
			    		//data=$.parseJSON(data);
			    		if(data.result_code==0){
			    			pkg.table.ajax.reload();
			    			$('#formMsg').dimmer('show');
			    			window.setTimeout(function(){
		    			    	$('#formMsg').dimmer('hide');
			    				$('#addWindow').modal('close');
			    			},1000);
			    			ret=true;
			    		} else {
			    			alert(data.result_msg);
							ret=false;
			    		}
			    	}
				});
				return ret;
			}
		};
	
	function _delete(elem) {
		var id=$(elem).next(':hidden').val();
		$.post('${path}/api/delFlowInfo',{ids:id},function(data){
			pkg.table.ajax.reload();
		},'json');
		$('.ui.popup').popup('hide all');
	}
	
	function edit(rowIndex) {
		editData=this.table.row(rowIndex).data();
		$('#addWindow').modal(pkg.modalOpt).modal('show');
	}
	
	function showRestore() {
		$('#restoreWindow').modal({
			closable:false,
			duration:100,
			onVisible : function(){
			},
			onHidden: function(){
			},
			onApprove: function() {
				var arr=[];
				$.each($(this).find('.checkbox'),function(i,n){
					if($(this).checkbox('is checked')){
						arr.push($(this).find(':checkbox').val());
					}
				});
				var ret=false;
				$.ajax({
					url:'${path}/api/unifyConf',
					method:'POST',
					async:false,//同步提交
					data:{type:'flowPackage',channel:arr.join(',')},
					dataType:'json',
					success: function(data){
			    		//data=$.parseJSON(data);
			    		if(data.err==0){
			    			pkg.table.ajax.reload();
			    			$('#formMsg').dimmer('show');
			    			window.setTimeout(function(){
		    			    	$('#formMsg').dimmer('hide');
			    				$('#addWindow').modal('close');
			    			},1000);
			    			ret=true;
			    		} else {
			    			alert(data.msg);
							ret=false;
			    		}
			    	}
				});
				return ret;
			}
		}).modal('show');
	}
	
	return {
		edit:edit,
		_delete:_delete,
		table:table, 
		tableOpt:tableOpt,
		modalOpt:modalOpt,
		showRestore:showRestore
	}
})();

	pkg.table = $('#table').DataTable(pkg.tableOpt); 
	$('[name=operators],[name=channel]').dropdown({
		onChange:function(value, text, $choice){
			pkg.table.ajax.reload();
		}
	});
	$('#foldBtn').dropdown();
</script>


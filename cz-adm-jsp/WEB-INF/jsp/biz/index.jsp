<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp"%>
<%--
<style type="text/css">
div.slider {
    display: none;
    margin: 0 20px 0;
}
 --%>
</style>
<script src="static/js/echarts.min.js"></script>

<div style="margin: 0 10% 0;">

<form id="searchForm" action="api/stat" class="ui tiny form " onsubmit="return false;">
  <div class="fields">
    <div class="field two wide" ></div>
    <div class="field two wide" >
       <label>位置</label>
		 <select class="ui compact dropdown" name="module">
		  <option value="0">全部</option>
		  <option value="1">黄页</option>
		</select>
    </div>
    <div class="field two wide" >
       <label>运营商</label>
		 <select class="ui compact dropdown" name="operators">
		  <option value="all">全部</option>
		  <c:forEach items="${operators}" var="v">
		  <option value="${v.key}">${v.value}</option>
		  </c:forEach>
		</select>
    </div>
    <div class="field two wide">
       <label>开始日期</label>
       <div class="ui calendar" id="rangestart">
         <div class="ui input left icon">
           <i class="calendar icon"></i>
           <input name="startTime" type="text" value="${start}" placeholder="包含所选日期" autocomplete="off">
         </div>
       </div>
    </div>
    <div class="field two wide">
      <label>结束日期</label>
      <div class="ui calendar" id="rangeend">
        <div class="ui input left icon">
          <i class="calendar icon"></i>
          <input name="endTime" type="text" value="${end}" placeholder="包含所选日期" autocomplete="off">
        </div>
      </div>
    </div> 
    <div class="field one wide">
    	<button class="ui primary tiny button" onclick="stat2.search(this)" style="position:absolute;bottom:4%;">查询</button>
    </div>
  </div>
  </form>

<div id="resultDiv" class="ui ">
<!-- 
	<h4 class="ui horizontal divider header"><i class="tag icon"></i></h4>
	<div class="ui divider"></div>
 -->
	 <div class="ui small three statistics" style="margin-top: 2em">
	   <div class="ui statistic">
		  <div class="value" id="pv">0</div>
		  <div class="label">充值流水(元)</div>
		</div>
		 <div class="ui statistic">
		  <div class="value" id="uv">0</div>
		  <div class="label">充值次数</div>
		</div>
		 <div class="ui statistic">
		  <div class="value" id="cost">0</div>
		  <div class="label">成功率</div>
		</div>
	</div>
	
	<!-- 
	<div class="ui horizontal divider header"><i class="bar chart icon"></i></div>
	 -->
	
	<div id="myeChart" style="height:350px; "></div>
</div>
</div>
	
	
<script type="text/javascript">
var stat2 = (function() {
	var chartOpt = {
		legend: {
        	right:'0',	top:'50%',	orient:'vertical',	
        	data:['充值流水','充值次数','成功率']
        },
        //title: {  text: '日趋势图'},
        toolbox: {show : true	},
        tooltip: {},
        xAxis: {data: []},
        yAxis: {},
        series: [
            {name: '充值流水',type: 'line',data: []},
            {name: '充值次数',type: 'line',data: []},
            {name: '成功率',type: 'line',data: []}
        ]
	};
	
	function search(btn) {
		var d={},arr=$('#searchForm').serializeArray();
        jQuery.each(arr, function(i, field){
        	d[field.name] = field.value;  
       	});
		var msg = ((!d.startTime||!d.endTime)&&'请选择日期');  
		if(msg) {
			alert(msg);return false;
		}
		if(d.operators=='all')d.operators='';
        
		$(btn).addClass('disabled loading');
		$.get($('#searchForm').attr('action'),d, function(data){
			$('#pv').html(data.totalVolume);
			$('#uv').html(data.totalOrders);
			$('#cost').html(data.totalSuccessRatio+"%");
			var xAxis=[],s0=[],s1=[],s2=[];
			for(var v in data.statList) {
				xAxis.push(data.statList[v].date);
				s0.push(data.statList[v].volume);
				s1.push(data.statList[v].orders);
				s2.push(data.statList[v].successRatio);
			}
			chartOpt.xAxis.data=xAxis;
			chartOpt.series[0].data=s0;
			chartOpt.series[1].data=s1;
			chartOpt.series[2].data=s2;
			echarts.init($('#myeChart')[0]).setOption(chartOpt);
			$(btn).removeClass('disabled loading');
		},'json');
	}
	
	return {
		search:search
	}
})();


$('#rangestart').calendar({ 
	maxDate : new Date('${end}'),
	endCalendar: $('#rangeend')		
});
$('#rangeend').calendar({	
	maxDate : new Date('${end}'),
	startCalendar: $('#rangestart')	
});

$('.ui.dropdown').dropdown();
$('.primary.button').click();
<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="../taglib.jsp" %>
<!doctype html>
<html>
<%-- 适用于已登录,但无权限的提示 --%>
<head>
    <meta charset="utf-8">
    <title>没有权限</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

        * {
            line-height: 1.2;
            margin: 0;
        }

        html {
            color: #888;
            display: table;
            font-family: sans-serif;
            height: 100%;
            text-align: center;
            width: 100%;
        }

        body {
            display: table-cell;
            vertical-align: middle;
            margin: 2em auto;
        }

        h1 {
            color: #555;
            font-size: 2em;
            font-weight: 400;
        }

        p {
            margin: 0 auto;
            width: 280px;
        }

        @media only screen and (max-width: 280px) {

            body, p {
                width: 95%;
            }

            h1 {
                font-size: 1.5em;
                margin: 0 0 0.3em;
            }

        }

    </style>
</head>
<body>
	<script type="text/javascript">
		if(self != top) {
			top.location.href = location;  
		}
	</script>
    <h1>没有权限</h1>
    <p>
    <c:choose>
    	<c:when test="${exception ne null}">${exception.message}</c:when>
    	<c:otherwise>请联系管理员授权</c:otherwise>
    </c:choose>
    </p>
    <%-- 单点登录页会自动转回造成循环跳转
    <p>您没有权限访问,请等待<span id="num">5</span>秒自动跳转到登录页面.</p>
    <script type="text/javascript">
	    setInterval(function(){
	    	var elem = document.getElementById("num");
	    	var num = parseInt(elem.innerHTML);
	    	if(--num <= 0) {
	    		window.top.location.href="${loginUrl}";
	    	} else {
	    		elem.innerHTML = num;
	    	}
	    }, 1000);
     --%>
    </script>
</body>
</html>

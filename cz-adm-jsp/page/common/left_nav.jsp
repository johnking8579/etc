<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% 
	String cur = request.getParameter("cur");
%>

<div style="width:240px;float:left">
	<ul class="nav nav-pills nav-stacked" role="tablist">
	  <li role="presentation" <% if("query".equals(cur)){%>class="active"<%}%>><a href="/page/list.do">查询</a></li>
	  <li role="presentation" <% if("tool".equals(cur)){%>class="active"<%}%>><a href="/page/generate.do">工具</a></li>
	  <li role="presentation" <% if("api".equals(cur)){%>class="active"<%}%>><a href="/page/apidoc.do">API</a></li>
	  <li role="presentation" <% if("stat".equals(cur)){%>class="active"<%}%>><a href="/page/statistic.do">统计</a></li>
	  <li role="presentation" <% if("custom".equals(cur)){%>class="active"<%}%>><a href="/page/custom.do">定制</a></li>
	</ul>
</div>
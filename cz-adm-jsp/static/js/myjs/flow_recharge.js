function send(url, param, callback) {
	if (!param) {
		return;
	}

	$.post(url, param, function(data) {
		if (typeof callback == 'function') {
			callback(data);
		}
	});
}

function getpackageInfo(yys, channel) {
	var param = {};
	param["yys"] = yys;
	param["channel"] = channel;
	var $tb = document.getElementById("tb_flowPackageInfo");
	clearTable($tb);
	send('/json/getPackageInfo.do', param, function(data) {
		if (data && data.length > 0) {
			$.each(data, function(i, item) {
				$('#tableTmpl').tmpl(item).appendTo($tb);
			});
		}
	});

}

function clearTable(tb) {
	var rowNum = tb.rows.length;
	for (i = 2; i < rowNum; i++) {
		tb.deleteRow(i);
		rowNum = rowNum - 1;
		i = i - 1;
	}
}

function loadFlowPackageInfo() {
	var yys = document.getElementById("select_yys").value;
	var channel = document.getElementById("select_channel").value;
	if (yys == "" || yys == null || channel == "" || channel == null) {
		return;
	} else {
		getpackageInfo(yys, channel);
	}
}

function changeFlowInfo(obj) {
var id=obj.id;
var value=obj.value;
var temp_array=id.split("_");
var recid=temp_array[0];
var type=temp_array[1];
}

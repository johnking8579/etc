function getUrlParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getHash(menuIndex) {
	var arr = window.location.hash.split("|",2);
	if(menuIndex == 1) {
		return arr[0].replace("#",""); 
	} else if(menuIndex == 2) {
		return arr[1];
	} else {
		return null;
	}
}

function setHash(menu1, menu2) {
	window.location.hash = menu1 + "|"+ menu2;
}

/**
 * 把form字段变成obj
 * @param $form
 * @returns {___anonymous715_716}
 */
function serializeObj($form) {
	var data={}, arr = $form.serializeArray();
    jQuery.each( arr, function(i, field){
    	data[field.name] = field.value;  
   	});
    return data;
}


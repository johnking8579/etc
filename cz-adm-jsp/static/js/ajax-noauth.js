/**
 * 权限管理系统对无权限有跳转,详见SecurityManagerImpl
 * 由于ajax的安全限制,无法响应302跳转,所以使用自定义响应头
 */
$.ajaxSetup({
	complete: function (xhr, status) { 
		if(status==500 || status=='error') {
			alert('出错了,请联系开发人员.');
		} else {
			var redirect = xhr.getResponseHeader("noauth_url");
			if(redirect!=null) window.location.href = redirect;
		}
	}
});
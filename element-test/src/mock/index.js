import Mock from 'mockjs'

export function mock(){
    Mock.setup({
        timeout: '200-600' // 表示响应时间介于 200 和 600 毫秒之间，默认值是'10-100'。
    })

    Mock.mock(/\/myurl/, {
        name:'xxxx',
        age:1111
    });

    Mock.mock(/\/left-menu/,[
        {
            "url" : "/a",
            "name":"表单测试",
        },
        {
            "name":"表格测试",
            "children":[
                {
                "url" : "/b",
                "name":"前端分页",
                },
                {
                "url" : "/c",
                "name":"后端分页",
                },
            ]
        },
        {
            "url" : "/d",
            "name":"第三个菜单",
        },
    ]);

    Mock.mock(/\/table1/,Mock.mock({
        'data|500':[{
            'id|+1':1,
            name:'@cname',
            address:'@county(true)',
            date:'@date'
        }]
    }).data);

    Mock.mock(/\/table2[?].*/,Mock.mock({
        'data|10':[{
            'id|+1':1,
            name:'@cname',
            address:'@county(true)',
            date:'@date'
        }],
        total:500
    }));

    Mock.mock(/\/table2\/del[?].*/,{
        err:500
    });


}
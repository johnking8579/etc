package com.jd.supplier.common;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;

import com.jd.supplier.BaseTest;
import com.jd.supplier.web.CookieUtils;

public class CookieUtilTest extends BaseTest {
	
	@Resource CookieUtils util;
	
	@Test
	public void testEncrypt()	{
		System.out.println(util.encrypt("1"));
		System.out.println(util.encrypt("2"));
		System.out.println(util.encrypt("一二三"));
	}
	
	@Test
	public void testDecrypt()	{
		Assert.assertEquals("1", util.decrypt("5LONKH3LIBDOC"));
		Assert.assertEquals("一二三", util.decrypt("JKPSAQ75LLAFI"));
		try {
			util.decrypt("faddasdfadsdfa");
			Assert.assertTrue(false);
		} catch (Exception e) {
			Assert.assertTrue(true);
		}
	}
	
}

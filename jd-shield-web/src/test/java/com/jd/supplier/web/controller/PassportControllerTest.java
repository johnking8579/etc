package com.jd.supplier.web.controller;

import static junit.framework.Assert.assertEquals;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.ModelAndViewAssert;
import org.springframework.web.servlet.ModelAndView;

import com.jd.supplier.BaseTest;
import com.jd.supplier.web.vo.RegForm;

public class PassportControllerTest extends BaseTest {
	
	@Resource PassportController ctrl;
	
	HttpServletRequest req = new MockHttpServletRequest();
	HttpServletResponse resp = new MockHttpServletResponse();
	
	@Test
	public void testLogin()	{
		String s = ctrl.login("jd19160284", "e10adc3949ba59abbe56e057f20f883e", "123", true, "123",resp);
		assertEquals("{\"err\":0}", s);
	}
	
	@Test
	public void testcheckAccount()	{
		assertEquals("0", ctrl.checkAccount("xxxxx"));
		assertEquals("1", ctrl.checkAccount("jd19160284"));
	}

	@Test
	public void testDoRegister() {
		RegForm form = new RegForm();
		form.setAccount("bjxxxx");
		form.setEmail("jingying@jd.com");
		form.setIsVerifyErr(false);
		form.setMobile("13112345678");
		form.setName("名字名字");
		form.setPwd("e10adc3949ba59abbe56e057f20f883e");
		form.setVerify("123");
		ModelAndView mv = ctrl.doRegister(form, "123", resp);
		ModelAndViewAssert.assertModelAttributeValue(mv, "stat", 1);
	}

	@Test
	public void testSendAuthcode() {
		String s = ctrl.sendAuthcode("bjxxxx", "jingying@jd.com", resp);
		assertEquals("{\"err\":0}", s);
	}

	@Test
	public void testCheckAuthcode() {
		String s = ctrl.checkAuthcode("bjxxxx", "jingying@jd.com", "456789", "456789", resp);
		assertEquals("{\"err\":0}", s);
	}
	
	@Test
	public void testCheckAuthcode4ErrorUserEmail() {
		String s = ctrl.checkAuthcode("bjxxxx", "ingying@jd.com", "456789", "456789", resp);
		assertEquals("{\"err\":1}", s);
	}
	
	@Test
	public void testCheckAuthcode4ErrorAuth() {
		String s = ctrl.checkAuthcode("bjxxxx", "jingying@jd.com", "456789", "4567888", resp);
		assertEquals("{\"err\":2}", s);
	}

	@Test
	public void testResetPwd() {
		String s = ctrl.resetPwd("bjxxxx", "jingying@jd.com", "456789", "e10adc3949ba59abbe56e057f20ffffff", "456789", resp);
		assertEquals("{\"err\":0}", s);
	}

}

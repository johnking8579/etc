var table;
var tableData;
$(function(){ 

//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='requirement' value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
		        { "sTitle": "项目名称" ,"sDefaultContent": "", "bSortable": true,"fnRender":function(obj){
		        	   return "<a href='#' onclick='showProjectDetail("+obj.aData.requirementId+")'>"+obj.aData.projectName+"</a>";
		        }},
		        {"mData": "createTime","sDefaultContent": "","bSortable":false,"sTitle":"报名日期","sDefaultContent":"","mRender":function(data){
		        	   return parseDate(data);
		        }},
		        {"mData": "finalFlag","sDefaultContent": "","sTitle":"状态","sDefaultContent": "","sWidth": "30px","bSortable": false,"mRender":function(data){
		        	   return parseStatus(data);
		        }},
	        ];

var  tableBtns  =[
				];


 table = $('#hello').dataTable( {
	 		"sDefaultContent":"notice",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":"/supplier/queryApplyRequirement",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
            	var searchProjectName=$("input#searchProjectName").val();
				if(searchProjectName==null) searchProjectName="";
				var time=$("#time option:selected").val();
				if(time==null) time="";
				aoData.push(
						{ "name": "searchProjectName", "value":searchProjectName },
						{ "name": "time", "value":time }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );

  	
  	parseDate= function(data){
  		 if(data!=""){
  			    var date = new Date(data);// 或者直接new Date();
			    return date.format("yyyy-MM-dd");
  		 }else return "";
  	}
  	
  	
  	$('#requirement_search').click(function() {
			table.fnDraw(); } 
	);
	$('#searchProjectName').keyup(function() {
			table.fnDraw(); 
	} );
	
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}


function parseStatus(data){
	if(data==0){
		return "初选";
	}else if(data==1){
		return "入围";
	}
	
}

function showProjectDetail(reqId){
	window.open("/supplier/requirement_viewpage?id="+reqId);
}

Date.prototype.format = function(format) {
		    var o = {
		    "M+" : this.getMonth() + 1, //month
		    "d+" : this.getDate(), //day
		    "h+" : this.getHours(), //hour
	        "m+" : this.getMinutes(), //minute
		    "s+" : this.getSeconds(), //second
		    "q+" : Math.floor((this.getMonth() + 3) / 3), //quarter
		    "S" : this.getMilliseconds()
		    //millisecond
		    }
		    if (/(y+)/.test(format)) {
		        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		    }
		 
		    for ( var k in o) {
		        if (new RegExp("(" + k + ")").test(format)) {
		        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
	        }
		    }
		    return format;
		}



<%@page import="com.jd.common.util.PaginatedList"%>
<%@page import="com.jd.supplier.model.Notice"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/slider.js"></script>
</head>
<body>
    <jsp:include page="../supplier/include/loginToolbar.jsp" />
    <jsp:include page="../supplier/include/header.jsp" />
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c2b page_zhaobiao">
            <div class="grid_m">
                <div class="grid_m_inner">
                    <div class="mod_col">
                        <div class="mod_col_hd">
                            <h2 class="title">招标预告</h2>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_yugao">
                                <thead>
                                    <tr>
                                        <th class="col1">中标标题</th>
                                        <th>中标单位</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<%
                                			PaginatedList <Notice> list=(PaginatedList)request.getAttribute("list");
                                			for( int i=0;i<list.size();i++){
                                			    Notice notice =(Notice)list.get(i);
                                	%>
                                
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="/supplier/requirement_viewpage?id=<%=notice.getRequirementId()%>" target="_blank"><%=notice.getTitle() %></a></div></td>
                                        <td><%if(notice.getSupplierName()!=null&&!notice.getSupplierName().equals("")){out.print(notice.getSupplierName());}%></td>
                                        <td><%if(notice.getPublishdatestr()!=null&&!notice.getPublishdatestr().equals("")){out.print(notice.getPublishdatestr());}%></td>
                                    </tr>
                                    
                                    
                                    <%
                                    }
                                    %>
                                </tbody>
                            </table>
                            
                            <%
                            	if(list.getTotalPage()!=0){
                            %>
                            
                            <div class="page_wrap">
                                <div class="page_navigater">
                                    <span class="page_num">
                                        <strong class="cg_strong"><%=list.getIndex()%></strong><span>/<%=list.getTotalPage() %></span>
                                    </span>
                                    <%
                                    if(list.getIndex()==1){
                                	%>
                                	 <a class="page_prev page_prev_disabled" href="#" ><span>上一页</span></a>
                                	<%
                                    	}else{
                                    %>
                                    	 <a class="page_prev page_prev_disabled" href="/supplier/notice_page?page=<%=list.getIndex()-1%>&type=3" ><span>上一页</span></a>
                                    	    <%
                                    	}
                                    %>
                                    
                                   <%
                                   	if(list.getTotalPage()==list.getIndex()){
                                   	    %>
                                   	      <a class="page_next"  href="#" ><span>下一页</span></a>
                                   	    <%
                                   	}else{
                                   	    %>
                                   	    <a class="page_next"  href="/supplier/notice_page?page=<%=list.getIndex()+1%>&type=3" ><span>下一页</span></a>
                                   	    <%
                                   	}
                                   %>
                                   
                                    
                                </div> 
                            </div>
                            
                            <%}else{ %>
                             <div class="page_wrap">
                                <div class="page_navigater" >
                                	无记录
                                 </div> 
                            </div>
                            
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid_s cg_notice mod_col">
                <div class="mod_col_hd">
                    <h2 class="title">公告栏</h2>
                </div>
                <div class="mod_col_bd">
                    <ul class="mod_list">
                    	<%
                          	List<Notice> bidNoticeList =(List<Notice>)request.getAttribute("bidNoticeList");
                          	if(bidNoticeList!=null&&bidNoticeList.size()>0){
	                           	int length=0;
	                           	int size=0;
	                           	if(bidNoticeList.size()>0&&bidNoticeList.size()<10){
	                           	    size=bidNoticeList.size();
                           		}else{
                           	    size=10;
                           	    }
                          
                         	    for(int i=0;i<size;i++){
                         		 	Notice notice=(Notice)bidNoticeList.get(i);
                         	%>	
             
               						  <li><a href="/supplier/bidNotice_detailPage?id=<%=notice.getId()%>" target="_blank" title="<%=notice.getTitle()%>"><%=notice.getTitle()%></a></li>
                 			 <%
                         		}
                               }
                             %>
                      
                    </ul>
                </div>
            </div>
        </div> 
        <!-- 招标预告和公告栏 E --> 
        <c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>

	
		<jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>

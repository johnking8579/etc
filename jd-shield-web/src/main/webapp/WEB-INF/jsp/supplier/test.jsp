<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="http://misc.360buyimg.com/lib/skin/2013/base.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="./common/js/slider.js"></script>
</head>
<body>
    <div class="cg_toolbar">
        <div class="grid_c1">
            <span class="login_wrap"><a href="javascript:;">你好，路人甲</a>|<a href="javascript:;">注销</a><a href="javascript:;">登录</a>/<a href="javascript:;">注册</a></span>
        </div>
    </div>
	<div class="cg_header">
		<div class="grid_c1">
			<h1 class="title"><a href="#" title="易迅商家后台管理系统">易迅商家后台管理系统<img src="../common/img/logo.png" alt="易迅商户后台"></a></h1>
			<div class="nav">
				<ul>
                    <li class="curr"><a href="#">首页</a></li>
					<li><a href="#">招标预告</a></li>
					<li><a href="#">入围公告</a></li>
					<li><a href="#">中标公告</a></li>
					<li><a href="#">我的投标</a></li>
				</ul>
			</div>
		</div>
	</div>
    <div class="cg_content">
        <!-- banner和公告栏 S -->
        <div class="grid_c2b cg_f1">
            <div class="grid_m">
                <div class="grid_m_inner banner_wrap">
                    <div id="J_slider" class="slider">
                        <ul class="slider_ctn" id="J_sliderCtn">
                            <li><a target="_blank" href="#" title="这个title要记得加上的哦"><img data-lazy="../common/images/banner.jpg"  src='../common/img/placeholder.png' alt="图片"/></a></li>
                            <li><a target="_blank" href="#" title="这个title要记得加上的哦"><img data-lazy="../common/images/banner.jpg" src='../common/img/placeholder.png' alt="图片"/></a></li>
                            <li><a target="_blank" href="#" title="这个title要记得加上的哦"><img data-lazy="../common/images/banner.jpg" src='../common/img/placeholder.png' alt="图片"/></a></li>
                            <li><a target="_blank" href="#" title="这个title要记得加上的哦"><img data-lazy="../common/images/banner.jpg" src='../common/img/placeholder.png' alt="图片"/></a></li>
                        </ul>
                        <ul class="slider_nav" id="J_sliderNav">
                            <li class="on">1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                        </ul>
                        <span class="slider_btn slider_btn_prev"></span>
                        <span class="slider_btn slider_btn_next"></span>
                    </div>
                </div>
            </div>
            <div class="grid_s cg_notice mod_col">
                <div class="mod_col_hd">
                    <h2 class="title">公告栏</h2>
                </div>
                <div class="mod_col_bd">
                    <ul class="mod_list">
                        <li><a href="#" target="_blank" title="这个title要填上">北京分公司临时租车项目招标公告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">玻璃清洗招标预告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">集团公司开关面板供应战略招标预告板供应战略招标预告板供应战略招标预告板供应战略</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">项目清洁新风管道和新风机组招标</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">木地板安装招标预告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">集团公司开关面板供应战略招标预告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">项目清洁新风管道和新风机组招标</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">木地板安装招标预告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">集团公司开关面板供应战略招标预告</a></li>
                        <li><a href="#" target="_blank" title="这个title要填上">项目清洁新风管道和新风机组招标</a></li>
                    </ul>
                </div>
            </div>
        </div> 
        <!-- banner和公告栏 E -->
        <!-- 招标预告和入围公告 S -->
        <div class="grid_c2 cg_f2">
            <div class="grid_m">
                <div class="grid_m_inner">
                    <div class="mod_col cg_f2_yugao">
                        <div class="mod_col_hd">
                            <h2 class="title">招标预告</h2>
                            <a class="more" href="#" target="_blank">更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_yugao">
                                <thead>
                                    <tr>
                                        <th class="col1">项目名称</th>
                                        <th>预计招标时间</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-18</td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-18</td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td> 
                                        <td>2014-04-18</td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-18</td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-18</td>
                                        <td>2014-04-12</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid_s">
                    <div class="mod_col cg_f2_ruwei">
                        <div class="mod_col_hd">
                            <h2 class="title">入围公告</h2>
                            <a class="more" href="#" target="_blank">更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_ruwei">
                                <thead>
                                    <tr>
                                        <th class="col1">项目名称</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                </tbody>
                            </table> 
                        </div>
                    </div> 
            </div>
        </div>
        <!-- 招标预告和入围公告 E --> 
        <!-- 中标公告 S -->
        <div class="grid_c1">
                   <div class="mod_col cg_f3_zhongbiao">
                        <div class="mod_col_hd">
                            <h2 class="title">中标公告</h2>
                            <a class="more" href="#" target="_blank">更多</a>
                        </div>
                        <div class="mod_col_bd">
                            <table class="mod_table table_zhongbiao">
                                <thead>
                                    <tr>
                                        <th class="col1">项目名称</th>
                                        <th class="col2">中标单位</th>
                                        <th>发布时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td class="col2"><div class="col2_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td class="col2"><div class="col2_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公北京分公司临时租车项目招标公</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td class="col2"><div class="col2_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr>
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td class="col2"><div class="col2_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr> 
                                    <tr>
                                        <td class="col1"><div class="col1_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告北京分公司临时租车项目招标公告</a></div></td>
                                        <td class="col2"><div class="col2_in"><a href="#" target="_blank">北京分公司临时租车项目招标公告</a></div></td>
                                        <td>2014-04-12</td>
                                    </tr> 
                                </tbody>
                            </table> 
                        </div>
                    </div> 
        </div>
        <!-- 中标公告 E --> 
        <!-- 友情链接 S --> 
        <div class="grid_c1">
            <div class="mod_col cg_f4_friendslink">
                <div class="mod_col_hd">
                    <h2 class="title">友情链接</h2>
                </div>
                <div class="mod_col_bd">
                    <ul class="clearfix">
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src=".../common/images/brand1.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src=".../common/images/brand2.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand3.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand4.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand5.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand6.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand7.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand8.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand9.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand10.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand11.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand12.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand13.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand14.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand15.jpg" alt="品牌名称填充到这来" /></a></li>
                        <li><a href="#" target="_blank" title="为了SEO品牌名称填充到这来"><img src="../common/images/brand16.jpg" alt="品牌名称填充到这来" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- 友情链接 E --> 
    </div>

	
	<div class="cg_footer">
        <div class="grid_c1">
            <ul class="cg_footer_list clearfix">
                <li><a href="#" target="_blank">商家入驻</a></li>
                <li><a href="#" target="_blank">迷你挑</a></li>
                <li><a href="#" target="_blank">手机京东</a></li>
                <li><a href="#" target="_blank">销售联盟</a></li>
                <li><a href="#" target="_blank">京东社区</a></li>
                <li><a href="#" target="_blank">京东公益</a></li>
                <li><a href="#" target="_blank">联系我们</a></li>
                <li><a href="#" target="_blank">关于我们</a></li>
                <li><a href="#" target="_blank">帮助文档</a></li>
                <li><a href="#" target="_blank">廉政举报</a></li>
            </ul>
        </div>
	</div>








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>

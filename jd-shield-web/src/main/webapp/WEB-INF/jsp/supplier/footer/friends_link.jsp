<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<title>友情链接</title>
<div class="grid_c1">
	<div class="mod_single">
		<h2 class="mod_single_hd">友情链接</h2>
		<div class="mod_single_bd">
			<p class="qa_a">京东公益（<a href="http://gongyi.jd.com/" target="_blank">http://gongyi.jd.com/</a>）</p>

			<p class="qa_a">手机京东（<a href="http://app.jd.com/"  target="_blank">http://app.jd.com/</a>）</p>

			<p class="qa_a">商家入驻（<a href="http://www.jd.com/contact/joinin.aspx"  target="_blank">http://www.jd.com/contact/joinin.aspx</a>）</p>
			
			<p class="qa_a">人才招聘（<a href="http://zhaopin.jd.com/"  target="_blank">http://zhaopin.jd.com/</a>）</p>
		</div>

	</div>
</div>

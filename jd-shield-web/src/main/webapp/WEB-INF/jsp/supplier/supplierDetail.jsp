<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../common/css/index.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../nvalidator/jquery.validator.css" type="text/css" media="screen" />
<script type="text/javascript" src="../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../common/js/slider.js"></script>
<script type="text/javascript" src="../nvalidator/jquery.validator.js" charset="utf-8"></script>
<script type="text/javascript" src="../nvalidator/zh_CN.js" charset="utf-8"></script>
<script type="text/javascript" src="../nvalidator/nValidator.js" charset="utf-8"></script>
<script type="text/javascript" src="../datepicker/WdatePicker.js" charset="utf-8"></script>

<link href="../static/uploadify3.2.1/uploadify.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../static/uploadify3.2.1/jquery.uploadify.js"></script>
<script type="text/javascript" src="../static/web/supplier.js"></script>

</head>
<body>
<jsp:include page="../supplier/include/loginToolbar.jsp" />
    <jsp:include page="../supplier/include/header.jsp" />
    <div  class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c2a page_myinfo">
         <input type="hidden" id="status" />        
            <div class="grid_m">
                <div class="grid_m_inner">
                <form id="contactsForm" method="post" action="/supplier/supplier_add">
         		<input type="hidden" name="id" value="${supplier.id}"/>
         		<input type="hidden" id="info1" name="infoStatus" value="${supplier.infoStatus}"/>
         		<input type="hidden" name="dataIntegrity" value="10"/>
                    <div class="info_item ">
                        <div class="info_item_hd clearfix">
                            <h2>基本信息</h2>
                            <span class="edit"> 
                                <a  class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div  id="11" class="info_item_bd">
                       
                            <table class="info_table intro_table">
                                <thead>
                                    <tr>
                                        <th><span class="strong">*</span>联系人</th>
                                        <th><span class="strong">*</span>职位</th>
                                        <th><span class="strong">*</span>办公座机</th>
                                        <th><span class="strong">*</span>手机</th>
                                        <th>传真</th>
                                        <th><span class="strong">*</span>邮箱</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody id="contact">
                                <c:if test="${supplier.contacts == '' || supplier.contacts == null ||fn:length(supplier.contacts) == 0}">  
                                <tr id="tr_contact">
                                    <td class="col1"><input type="text" class="input_m" required data-rule="required;chinese;length[~45];" name="contacts[0].contactor" value="${contacts.contactor}"/></td>
                                    <td class="col2"><input type="text" class="input_m" required data-rule="required;chinese;length[~45];" name="contacts[0].position" value="${contacts.position}"/></td>
                                    <td class="col3"><input type="text" class="input_n" required data-rule="required;length[~45];" name="contacts[0].telphone" value="${contacts.telphone}"/></td>
                                    <td class="col4"><input type="text" class="input_n" required data-rule="required;mobile;length[~45];" name="contacts[0].mobile" value="${contacts.mobile}"/></td>
                                    <td class="col5"><input type="text" class="input_n" name="contacts[0].fax" value="${contacts.fax}"/></td>
                                    <td class="col6"><input type="text" class="input_l" required data-rule="required;email;length[~45];" name="contacts[0].email" value="${contacts.email}"/></td>
                                    <td><input type="hidden"  name="contacts[0].id" value="${contacts.id}"/></td>
                                </tr>
                                </c:if>
                                <c:set var="i" value="0"></c:set>
                                <c:forEach items="${supplier.contacts}" var="contacts" varStatus="status">
                                 <input id="contactsId${i}" name="contacts[${i}].ids" type="hidden"/>
                                 <input id="contacts${i}"  type="hidden" value="${contacts.id}"/>
                                 <input type="hidden"  id="ddd" name="contacts[${i}].id" value="${contacts.id}"/>
                                <tr id="tr_contact_${i}">
                                    <td class="col1"><input type="text" class="input_m" required data-rule="required;chinese;length[~45];"  name="contacts[${i}].contactor" value="${contacts.contactor}"/></td>
                                    <td class="col2"><input type="text" class="input_m" required data-rule="required;chinese;length[~45];" name="contacts[${i}].position" value="${contacts.position}"/></td>
                                    <td class="col3"><input type="text" class="input_n" required data-rule="required;length[~45];" name="contacts[${i}].telphone" value="${contacts.telphone}"/></td>
                                    <td class="col4"><input type="text" class="input_n" required data-rule="required;mobile;length[~45];" name="contacts[${i}].mobile" value="${contacts.mobile}"/></td>
                                    <td class="col5"><input type="text" class="input_n" name="contacts[${i}].fax" value="${contacts.fax}"/></td>
                                    <td class="col6"><input type="text" class="input_l" required data-rule="required;email;length[~45];" name="contacts[${i}].email" value="${contacts.email}"/></td>                                 
                                    <c:if test="${status.index != 0}"><td class="col7"><a href="javascript:delRow(${i})" class="delete">删除</a></td></c:if>                                   
                                </tr>
                                <c:set var="i" value="${i+1}"></c:set>
                              	</c:forEach>
                              	<input id="i_hidden" type="hidden" value="${i}"/>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5"><p class="info_tip txt_notice">注：联系人最少添加一位，可添加多位</p></td>
                                        <td colspan="2" class="foot_btn_add"><a href="javascript:addRow();" class="btn_common"><span>添加联系人</span></a></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="foot_btn_save"> <a href="javascript:saveContacts();" class="btn_mini"><span>保存</span></a> </td>
                                    </tr>
                                </tfoot>
                            </table>
                         
                        </div>
                    </div>
                    </form><!-- /item -->
                      <form id="enterpriseForm" method="post" action="/supplier/supplier_save">         				
                    <div class="info_item qiye ">
                        <div class="info_item_hd clearfix">
                            <h2>企业状况</h2>
                            <span class="edit">
                                <a  class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div id="22" class="info_item_bd">
                        <input type="hidden" name="id" value="${supplier.id}"/>
         				<input type="hidden" name="enterprise.id" value="${supplier.enterprise.id}"/>
         				<input type="hidden" id="info2" name="infoStatus" value="${supplier.infoStatus}"/>
         				<input type="hidden" name="dataIntegrity" value="30"/>
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>供应商全称：</label>
                                <span class="input_wrap">
                                    <input class="input_xxxl" type="text" name="enterprise.name"  placeholder="名称与上传营业执照一致" required data-rule="required;length[~30];" value="${supplier.enterprise.name}" />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>供应商组织：</label>
                                <span class="input_wrap">
                                <select class="sel_l" name="enterprise.organize" data-rule="required;nospecial;length[~10]">
									<option value="">请选择</option>
									<c:forEach items="${orgList}" var="var">			 				
									  <option <c:if test="${var.id eq supplier.enterprise.organize}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>
									 </c:forEach>							
                         		</select>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">办公地址：</label>
                                <span class="input_wrap">
                                    <input class="input_xxxl" type="text" name="enterprise.officeAddress" value="${supplier.enterprise.officeAddress}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>注册地址：</label>
                                <span class="input_wrap">
                                    <input class="input_xxxl" type="text" name="enterprise.registeredAddress" value="${supplier.enterprise.registeredAddress}" required data-rule="required;length[~100];"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">工厂地址：</label>
                                <span class="input_wrap">
                                    <input class="input_xxxl" type="text" name="enterprise.factoryAddress" value="${supplier.enterprise.factoryAddress}" />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>企业性质：</label>
                                <span class="input_wrap">
                                <select class="sel_n" name="enterprise.enterpriseType"  data-rule="required;nospecial;length[~10]">
									<option value="">请选择</option>
									<c:forEach items="${enterpriseList}" var="var">							
									  <option <c:if test="${supplier.enterprise.enterpriseType == var.id}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>								
									</c:forEach>
                         		</select>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>企业法人：</label>
                                <span class="input_wrap">
                                    <input class="input_n" type="text" name="enterprise.enterpriseLaw" value="${supplier.enterprise.enterpriseLaw}" required data-rule="required;length[~30];"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>成立日期：</label>
                                <span class="input_wrap">
                                    <input class="input_l" type="text" name="enterprise.dateOne" data-rule="required;date;length[~13]" value="${supplier.enterprise.dateOne}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly="true" />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>员工人数：</label>
                                <span class="input_wrap">
                                    <input class="input_n" type="text" name="enterprise.employeeCount" value="${supplier.enterprise.employeeCount}"  required data-rule="required;integer;length[~30];"/>人
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">管理人数：</label>
                                <span class="input_wrap">
                                    <input class="input_n" type="text" name="enterprise.managerCount" value="${supplier.enterprise.managerCount}"/>人
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">技术人数：</label>
                                <span class="input_wrap">
                                    <input class="input_n" type="text" name="enterprise.tecCount" value="${supplier.enterprise.tecCount}"/>人
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">厂地面积：</label>
                                <span class="input_wrap">
                                    <input class="input_l" type="text" name="enterprise.measure" value="${supplier.enterprise.measure}"/>平方米
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">企业网址：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="enterprise.website" value="${supplier.enterprise.website}" />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>注册资金：</label>
                                <span class="input_wrap">
                                    <select  class="sel_n" name="enterprise.currency" data-rule="required;nospecial;length[~10]" >
	                                    <c:forEach items="${currencyList}" var="var">								
									  	<option <c:if test="${supplier.enterprise.currency == var.id}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>								
										</c:forEach>
                        			</select>
                                    <input class="input_n" type="text" required data-rule="required;float;length[~10]" name="enterprise.money" 
                                    <c:if test="${supplier.enterprise.funds != ''}">value="<fmt:formatNumber pattern="0.##" value='${supplier.enterprise.funds/100}'></fmt:formatNumber>"</c:if>/>万元
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>营业执照号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="enterprise.license" required data-rule="required;length[~50];" value="${supplier.enterprise.license}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>营业执照有效期：</label>
                                <span class="input_wrap">
                                    <input class="input_l" type="text" name="enterprise.dateTwo" data-rule="required;date;length[~13]" value="${supplier.enterprise.dateTwo}" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" readonly=true />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">经营许可证号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl placeholder" type="text" name="enterprise.managementNum" value="${supplier.enterprise.managementNum}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>纳税人登记号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl placeholder" type="text" name="enterprise.taxNum" required data-rule="required;length[~50];" value="${supplier.enterprise.taxNum}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>组织机构代码号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl placeholder" type="text" name="enterprise.orgCode" required data-rule="required;length[~50];" value="${supplier.enterprise.orgCode}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">行业资质：</label>
                                <span class="input_wrap">
                                    <input class="input_n placeholder" type="text" name="enterprise.qualification"  value="${supplier.enterprise.qualification}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">年产量：</label>
                                <span class="input_wrap">
                                    <input class="input_n" type="text" name="enterprise.perAcount"  value="${supplier.enterprise.perAcount}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>业务覆盖范围：</label>
                                <span class="input_wrap">
                                <c:forEach items="${coverageList}" var="var">	
                                    <input class="chk" type="checkbox" 
                                    <c:forEach items="${rangeList}" var="c">
									<c:if test="${var.id == c }">
									checked 
									</c:if>
									</c:forEach> 
                                    name="enterprise.supplierRange" value="${var.id}"/>
                                    <label class="lb" for="">${var.dictDataName}</label>
                                 </c:forEach>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">分支机构：</label>
                                <span class="input_wrap">
                                    <select class="sel_n" name="enterprise.branch">
									<option value="">请选择</option>								
									<option  <c:if test="${supplier.enterprise.branch == '0'}"> selected </c:if> value="0">无</option>	
									<option  <c:if test="${supplier.enterprise.branch == '1'}"> selected </c:if> value="1">有</option>								
                         			</select>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>经营内容：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="enterprise.content" required data-rule="required;length[~200];" value="${supplier.enterprise.content}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">企业及产品荣誉情况：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="enterprise.honor"  value="${supplier.enterprise.honor}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for="">同行业绩情况：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="enterprise.other"  value="${supplier.enterprise.other}"/>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_action"> <a class="btn_mini" href="javascript:enterpriseSave();"><span>保存</span></a> </div>
                        </div>
                    </div>
                    </form><!-- /item -->
                      <form id="fianceForm" method="post" action="/supplier/supplier_save">       			
                    <div class="info_item caiwu ">
                        <div class="info_item_hd clearfix">
                            <h2>财务状况</h2>
                            <span class="edit">
                                <a  class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div id="33" class="info_item_bd"> 
                        	<input type="hidden" name="id" value="${supplier.id}"/>
         				<input type="hidden" name="finance.id" value="${supplier.finance.id}"/>
         				<input type="hidden" id="info3" name="infoStatus" value="${supplier.infoStatus}"/>
         				<input type="hidden" name="dataIntegrity" value="50"/>
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>开户银行许可证：</label>
                                <span class="input_wrap">
                                     <select class="sel_n" name="finance.bankLicense" data-rule="required;nospecial;">
									<option value="">请选择</option>								
									<option <c:if test="${supplier.finance.bankLicense == '0'}"> selected </c:if>value="0">无</option>	
									<option <c:if test="${supplier.finance.bankLicense == '1'}"> selected </c:if> value="1">有</option>								
                            		</select>   
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item">
                                <label class="item_lb" for="">开户银行联行号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="finance.bankLinked" value="${supplier.finance.bankLinked}" />
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>开户银行：</label>
                                <span class="input_wrap">
                                    <input class="input_xl" type="text" name="finance.bankName" required data-rule="required;length[~30];" value="${supplier.finance.bankName}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>银行账号：</label>
                                <span class="input_wrap">
                                    <input class="input_xl placeholder" type="text" name="finance.bankAccount" required data-rule="required;length[~30];" value="${supplier.finance.bankAccount}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>税率：</label>
                                <span class="input_wrap">
                                    <input class="input_n placeholder" type="text" name="finance.bankRate" required data-rule="required;length[~30];" value="${supplier.finance.bankRate}"/>%
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>默认申报纳税类型：</label>
                                <span class="input_wrap">
                                    <select class="sel_n" name="finance.taxType" required data-rule="required;nospecial;length[~10]">
									<option value="">请选择</option>
									<c:forEach items="${taxList}" var="var">							
									  <option <c:if test="${supplier.finance.taxType == var.id}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>								
									</c:forEach>
                         		</select> 
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>年销售额：</label>
                                <span class="input_wrap">
                                   <select  class="sel_n" name="finance.currency" required data-rule="required;nospecial;length[~10]">
                                   	<option value="">请选择</option>
	                                    <c:forEach items="${currencyList}" var="var">								
									  	<option <c:if test="${supplier.finance.currency == var.id}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>								
										</c:forEach>
                        			</select>
                                    <input class="input_n" type="text" required data-rule="required;integer;length[~10]" name="finance.salesCount" value="${supplier.finance.salesCount}"/>万元
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_item">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>企业年所得税：</label>
                                <span class="input_wrap">
                                    <select  class="sel_n" name="finance.currencyTax" required data-rule="required;nospecial;length[~10]">
                                    	<option value="">请选择</option>
	                                    <c:forEach items="${currencyList}" var="var" >								
									  	<option <c:if test="${supplier.finance.currencyTax == var.id}"> selected </c:if> value="${var.id}">${var.dictDataName}</option>								
										</c:forEach>
                        			</select>
                                    <input class="input_n" type="text" required data-rule="required;integer;length[~10]" name="finance.enterpriseTax" value="${supplier.finance.enterpriseTax}"/>万元
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </span>
                            </div><!-- / form_item -->

                            <div class="form_action"> <a class="btn_mini" href="javascript:financeSave();"><span>保存</span></a> </div>

                        </div>
                    </div>
                    </form><!-- /item -->
                    <form id="telForm" method="post" action="/supplier/supplier_save">
         			
                    <div class="info_item jishu ">
                        <div class="info_item_hd clearfix">
                            <h2>技术状况及质量控制</h2>
                            <span class="edit">
                                <a  class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div id="44" class="info_item_bd">
                        	<input type="hidden" name="id" value="${supplier.id}"/>
	         				<input type="hidden" name="quality.id" value="${supplier.quality.id}"/>
	         				<input type="hidden" id="info4" name="infoStatus" value="${supplier.infoStatus}"/>
	         				<input type="hidden" name="dataIntegrity" value="70"/>
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>认证状况：</label>
                                <div class="input_wrap">
                                 <c:forEach items="${authenticList}" var="var">	
                                    <input class="chk" type="checkbox" 
                                    <c:forEach items="${auList}" var="a">
									<c:if test="${var.id == a }">
									checked 
									</c:if>
									</c:forEach> 
                                    name="quality.authentication" value="${var.id}"/>
                                    <label class="lb" for="">${var.dictDataName}</label>
                                 </c:forEach>
                                    <span>其他认证</span>
                                    <input type="text" class="input_n" name="quality.other" value="${supplier.quality.other}"/>
                                    <i class="i_icon i_right"></i>
                                    <i class="i_icon i_error"></i>
                                </div>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">主要生产设备（制造类必填）：</label>
                                <div class="input_wrap">
                                   <table class="info_table shebei_table">
                                       <thead>
                                        <tr>
                                            <th>名称/型号</th>
                                            <th>品牌</th>
                                            <th>数量</th>
                                            <th>产能</th>
                                            <th>使用年限</th>
                                            <th>操作</th>
                                        </tr>
                                       </thead>
                                    <tbody id="one">
	                                <c:if test="${supplier.quality.equipOne == '' || supplier.quality.equipOne == null ||fn:length(supplier.quality.equipOne) == 0}">  
	                                <tr id="tr_one">
	                                    <td><input type="text" class="input_m"  name="quality.equipOne[0].name" value="${equipOne.name}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipOne[0].brand" value="${equipOne.brand}"/></td>
	                                    <td><input type="text" class="input_m"  data-rule="integer;length[~45];" name="quality.equipOne[0].eqNumber" value="${equipOne.eqNumber}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipOne[0].eqProduction" value="${equipOne.eqProduction}"/></td>
	                                    <td><input type="text" class="input_n"  data-rule="integer;length[~45];" name="quality.equipOne[0].eqLimit" value="${equipOne.eqLimit}"/></td>
	                                    <td><input type="hidden"  name="quality.equipOne[0].id" value="${equipOne.id}"/></td>
	                                </tr>
	                                </c:if>
	                                <c:set var="j" value="0"></c:set>
	                                <c:forEach items="${supplier.quality.equipOne}" var="equipOne" varStatus="status">
	                                <input type="hidden" id="equipOne${j}" name="quality.equipOne[${j}].id" value="${equipOne.id}"/>
	                                <input id="equipOneId${j}" name="quality.equipOne[${j}].idOne" type="hidden"/>
	                                <tr id="tr_one_${j}">
	                                    <td><input type="text" class="input_m"  name="quality.equipOne[${j}].name" value="${equipOne.name}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipOne[${j}].brand" value="${equipOne.brand}"/></td>
	                                    <td><input type="text" class="input_m"  data-rule="integer;length[~45];" name="quality.equipOne[${j}].eqNumber" value="${equipOne.eqNumber}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipOne[${j}].eqProduction" value="${equipOne.eqProduction}"/></td>
	                                    <td><input type="text" class="input_n"  data-rule="integer;length[~45];" name="quality.equipOne[${j}].eqLimit" value="${equipOne.eqLimit}"/></td>                                 	                                    	                            
	                                    <c:if test="${status.index != 0}"><td><a href="javascript:delRowOne(${j})" class="delete">删除</a></td></c:if>
	                                    
	                                </tr>
	                                <c:set var="j" value="${j+1}"></c:set>
	                              	</c:forEach>
	                              	<input id="j_hidden" type="hidden" value="${j}"/>
                                </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" class="foot_btn_add"> <a href="javascript:addRowOne();" class="btn_common">新增</a></td>
                                            </tr>
                                        </tfoot>
                                   </table>
                                </div>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">主要检测设备（制造类必填）：</label>
                                <div class="input_wrap">
                                   <table class="info_table shebei_table">
                                       <thead>
                                        <tr>
                                            <th>名称/型号</th>
                                            <th>品牌</th>
                                            <th>数量</th>
                                            <th>产能</th>
                                            <th>使用年限</th>
                                            <th>操作</th>
                                        </tr>
                                       </thead>
                                       <tbody id="two">
	                                <c:if test="${supplier.quality.equipTwo == '' || supplier.quality.equipTwo == null ||fn:length(supplier.quality.equipTwo) == 0}">  
	                                <tr id="tr_two">
	                                    <td><input type="text" class="input_m"  name="quality.equipTwo[0].name" value="${equipTwo.name}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipTwo[0].brand" value="${equipTwo.brand}"/></td>
	                                    <td><input type="text" class="input_m"  data-rule="required;integer;length[~45];" name="quality.equipTwo[0].eqNumber" value="${equipTwo.eqNumber}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipTwo[0].eqProduction" value="${equipTwo.eqProduction}"/></td>
	                                    <td><input type="text" class="input_n"  data-rule="required;integer;length[~45];"name="quality.equipTwo[0].eqLimit" value="${equipTwo.eqLimit}"/></td>
	                                    <td><input type="hidden"  name="quality.equipTwo[0].id" value="${equipTwo.id}"/></td>
	                                </tr>
	                                </c:if>
	                                <c:set var="k" value="0"></c:set>
	                                <c:forEach items="${supplier.quality.equipTwo}" var="equipTwo" varStatus="status">
	                                <input type="hidden" id="equipTwo${k}" name="quality.equipTwo[${k}].id" value="${equipTwo.id}"/>
	                                <input id="equipTwoId${k}" name="quality.equipTwo[${k}].idTwo" type="hidden"/>
	                                <tr id="tr_two_${k}">
	                                    <td><input type="text" class="input_m"  name="quality.equipTwo[${k}].name" value="${equipTwo.name}"/></td>
	                                    <td><input type="text" class="input_n"  name="quality.equipTwo[${k}].brand" value="${equipTwo.brand}"/></td>
	                                    <td><input type="text" class="input_m"  data-rule="required;integer;length[~45];" name="quality.equipTwo[${k}].eqNumber" value="${equipTwo.eqNumber}"/></td>
	                                    <td><input type="text" class="input_n"  name="required;quality.equipTwo[${k}].eqProduction" value="${equipTwo.eqProduction}"/></td>
	                                    <td><input type="text" class="input_n"  data-rule="integer;length[~45];" name="quality.equipTwo[${k}].eqLimit" value="${equipTwo.eqLimit}"/></td>                                 	                                    	                            
	                                    <c:if test="${status.index != 0}"><td><a href="javascript:delRowTwo(${k})" class="delete">删除</a></td></c:if>
	                                    
	                                </tr>
	                                <c:set var="k" value="${k+1}"></c:set>
	                              	</c:forEach>
	                              	<input id="k_hidden" type="hidden" value="${k}"/>
                                </tbody>
                                       </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" class="foot_btn_add"> <a href="javascript:addRowTwo();" class="btn_common">新增</a></td>
                                            </tr>
                                        </tfoot>
                                   </table>
                                </div>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">原材料信息（制造类必填）：</label>
                                <div class="input_wrap">
                                   <table class="info_table shebei_table">
                                       <thead>
                                        <tr>
                                            <th>主要原料名称</th>
                                            <th>供应厂家</th>
                                            <th>操作</th>
                                        </tr>
                                       </thead>
                                        <tbody id="three">
		                                <c:if test="${supplier.quality.materials == '' || supplier.quality.materials == null ||fn:length(supplier.quality.materials) == 0}">  
		                                <tr id="tr_three">
		                                    <td><input type="text" class="xxx_l"  name="quality.materials[0].name" value="${materials.name}"/></td>
		                                    <td><input type="text" class="xxx_l"  name="quality.materials[0].factory" value="${materials.factory}"/></td>
		                                    <td><input type="hidden"  name="materials[0].id" value="${materials.id}"/></td>
		                                </tr>
		                                </c:if>
		                                <c:set var="m" value="0"></c:set>
		                                <c:forEach items="${supplier.quality.materials}" var="materials" varStatus="status">
		                                 <input type="hidden" id="materials${m}" name="quality.materials[${m}].id" value="${materials.id}"/>
		                                 <input id="materialsId${m}" name="quality.materials[${m}].ids" type="hidden"/>
		                                <tr id="tr_three_${m}">
		                                    <td><input type="text" class="xxx_l"  name="quality.materials[${m}].name" value="${materials.name}"/></td>
		                                    <td><input type="text" class="xxx_l"  name="quality.materials[${m}].factory" value="${materials.factory}"/></td>                               		                                   		                            
		                                    <c:if test="${status.index != 0}"><td><a href="javascript:delRowThree(${m})" class="delete">删除</a></td></c:if>		                                    
		                                </tr>
	                                	<c:set var="m" value="${m+1}"></c:set>
	                              		</c:forEach>
	                              	<input id="m_hidden" type="hidden" value="${m}"/>
                                       </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3" class="foot_btn_add"> <a href="javascript:addRowThree();" class="btn_common">新增</a></td>
                                            </tr>
                                        </tfoot>
                                   </table>
                                </div>
                            </div><!-- / form_item -->

                            <div class="form_action"> <a class="btn_mini" href="javascript:telSave();"><span>保存</span></a> </div>
                            
                        </div>
                    </div><!-- /item -->
                    </form>
                    <form id="otherForm" method="post" action="/supplier/supplier_detail">
         			
                    <div class="info_item other ">
                        <div class="info_item_hd clearfix">
                            <h2>其他信息</h2>
                            <span class="edit">
                                <a  class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div id="55" class="info_item_bd">
                        	<input type="hidden" name="id" value="${supplier.id}"/>
         				<input type="hidden" name="other.id" value="${supplier.other.id}"/>
         				<input type="hidden" id="info5" name="infoStatus" value="${supplier.infoStatus}"/>
         				<input type="hidden" name="dataIntegrity" value="90"/>
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">与京东合作过：</label>
                                <span class="input_wrap">
                                <select class="sel_n" name="other.isCooperation">
								<option value="">请选择</option>								
								<option  <c:if test="${supplier.other.isCooperation == '0'}"> selected </c:if> value="0">未合作</option>	
								<option  <c:if test="${supplier.other.isCooperation == '1'}"> selected </c:if> value="1">合作</option>								
                         		</select>    
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">现合作客户：</label>
                                <span class="input_wrap">
                                    <textarea class="txta" id="" name="other.cooperation"  cols="30" rows="10">${supplier.other.cooperation}</textarea>
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">与现合作客户进行考察：</label>
                                <span class="input_wrap">
                                <select class="sel_n" name="other.isVisit">
								<option value="">请选择</option>								
								<option  <c:if test="${supplier.other.isVisit == '0'}"> selected </c:if> value="0">否</option>	
								<option  <c:if test="${supplier.other.isVisit == '1'}"> selected </c:if> value="1">是</option>								
                         		</select>    
                                </span>
                            </div><!-- / form_item --> 
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">备注：</label>
                                <span class="input_wrap">
                                    <textarea class="txta" id="" name="other.mark" cols="30" rows="10">${supplier.other.mark}</textarea>
                                </span>
                            </div><!-- / form_item -->
                            <div class="form_action"> <a class="btn_mini" href="javascript:saveOther();"><span>保存</span></a> </div>

                            
                        </div>
                    </div><!-- /item -->
                    </form>
                    <form id="fileForm" method="post" action="/supplier/supplier_detail">
                 
                   <div  id="66" class="info_item upload ">
                        <div class="info_item_hd clearfix">
                            <h2>上传附件</h2>
                            <span class="edit">
                                <a href="#" class="action"><b class="arrow"></b>编辑</a>
                            </span>
                        </div>
                        <div id="fileQueue" style="float:right;margin:50px;"></div>
                           <input type="hidden" name="id" value="${supplier.id}"/>
		                    <input type="hidden" id="info6" name="infoStatus" value="${supplier.infoStatus}"/>
		                    <input type="hidden" name="dataIntegrity" value="100"/>
                        <div class="info_item_bd">
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>营业执照副本：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify1" />
									<input type="hidden" name="affix[0].fileName" id="fileName1"/>
									<input type="hidden" name="affix[0].fileKey" id="fileKey1"/>
									<input type="hidden" name="affix[0].fileType" value="1"/>																											
                                </span>
                                <a style="float: left;padding-top: 3px;" id="result1" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 1}">
									<a style="float: left;padding-top: 3px;"  id="result2" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div>
                            <!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>税务登记证：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify2"/>
									<input type="hidden" name="affix[1].fileName" id="fileName2"/>
									<input type="hidden" name="affix[1].fileKey" id="fileKey2"/>
									<input type="hidden" name="affix[1].fileType" value="2"/>																											
                                </span>
                                <a style="float: left;padding-top: 3px;" id="result3" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 2}">
									<a style="float: left;padding-top: 3px;"  id="result4" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>组织机构代码证：</label>
                               <span class="input_wrap">
                                    <input type="file" id="uploadify3"/>
									<input type="hidden" name="affix[2].fileName" id="fileName3"/>
									<input type="hidden" name="affix[2].fileKey" id="fileKey3"/>
									<input type="hidden" name="affix[2].fileType" value="3"/>																	
                                </span>
                                 <a style="float: left;padding-top: 3px;" id="result5" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 3}">
									<a style="float: left;padding-top: 3px;"  id="result6" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>银行开户许可证：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify4"/>
									<input type="hidden" name="affix[3].fileName" id="fileName4"/>
									<input type="hidden" name="affix[3].fileKey" id="fileKey4"/>
									<input type="hidden" name="affix[3].fileType" value="4"/>																	
                                </span>
                                 <a style="float: left;padding-top: 3px;" id="result7" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 4}">
									<a style="float: left;padding-top: 3px;"  id="result8" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for=""><span class="cg_strong">*</span>供应商结算银行信息表：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify5"/>
									<input type="hidden" name="affix[4].fileName" id="fileName5"/>
									<input type="hidden" name="affix[4].fileKey" id="fileKey5"/>
									<input type="hidden" name="affix[4].fileType" value="5"/>																		
                                </span>
                                <a style="float: left;padding-top: 3px;" id="result9" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 5}">
									<a style="float: left;padding-top: 3px;"  id="result10" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">行业资质认证：</label>
                               	<span class="input_wrap">
                                    <input type="file" id="uploadify6"/>
									<input type="hidden" name="affix[5].fileName" id="fileName6"/>
									<input type="hidden" name="affix[5].fileKey" id="fileKey6"/>
									<input type="hidden" name="affix[5].fileType" value="6"/>																	
                                </span>
                                <a style="float: left;padding-top: 3px;" id="result11" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 6}">
									<a style="float: left;padding-top: 3px;"  id="result12" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">环境认证：</label>
                               	<span class="input_wrap">
                                    <input type="file" id="uploadify7"/>
									<input type="hidden" name="affix[6].fileName" id="fileName7"/>
									<input type="hidden" name="affix[6].fileKey" id="fileKey7"/>
									<input type="hidden" name="affix[6].fileType" value="7"/>												
                                </span>
                                 <a style="float: left;padding-top: 3px;" id="result13" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 7}">
									<a style="float: left;padding-top: 3px;"  id="result14" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">质量认证：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify8"/>
									<input type="hidden" name="affix[7].fileName" id="fileName8"/>
									<input type="hidden" name="affix[7].fileKey" id="fileKey8"/>
									<input type="hidden" name="affix[7].fileType" value="8"/>														
                                </span>
                                <a style="float: left;padding-top: 3px;" id="result15" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 8}">
									<a style="float: left;padding-top: 3px;"  id="result16" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            <div class="form_item clearfix">
                                <label class="item_lb" for="">其他资料：</label>
                                <span class="input_wrap">
                                    <input type="file" id="uploadify9"/>
									<input type="hidden" name="affix[8].fileName" id="fileName9"/>
									<input type="hidden" name="affix[8].fileKey" id="fileKey9"/>
									<input type="hidden" name="affix[8].fileType" value="9"/>																		
                                </span>
                                   <a style="float: left;padding-top: 3px;" id="result17" href=""></a>
                                <c:forEach items="${supplier.affix}" var="affix" varStatus="status">
									<c:if test="${affix.fileType == 9}">
									<a style="float: left;padding-top: 3px;"  id="result18" href="${affix.fileKey}">${affix.fileName}</a>
									</c:if>
								</c:forEach>
                            </div><!-- / form_item -->
                            
                            <div class="form_action"> <a class="btn_mini" href="javascript:savefile();"><span>保存</span></a> </div>
                            
                        </div>
                    </div><!-- /item -->
                    </form>


                </div><!-- /grid_m_inner -->
            </div><!-- /grid_m -->
          <jsp:include page="../supplier/include/left.jsp" />
        </div> 
        <!-- 招标预告和公告栏 E --> 
    </div>	
 <jsp:include page="../supplier/include/footer.jsp" />
 
<div id="infoDiv" style="display:none">
	<iframe class="thickframe" id="" marginwidth="0" marginheight="0" frameborder="0" scrolling="no"></iframe>
	<div class="thickdiv" id=""></div>
	<div class="thickbox" id="" style="left: 741.5px; top: 222px; width: 368px;">
	    <div class="thickwrap" style="width:368px;">
	        <div class="thicktitle" id="thicktitler" style="width:368px">
	            <span>提示</span>
	        </div>
	        <input type="hidden" id="hidden"/>
	        <div class="thickcon" id="thickconr" style="width: 345px; height: 140px; padding-left: 10px; padding-right: 10px;">
	            <div class="cont_inner">
	                <p>您还未填完所有必填项，是否要现在保存？</p>
	                <div class="btn_wrap">
	                    <a class="btn_normal" href="javascript:hide();"><span>不保存</span></a>
	                    <a class="btn_strong" href="javascript:save();"><span>保存</span></a>
	                </div>
	            </div>
	        </div>
	        <a href="javascript:hide();" class="thickclose" id="thickcloser">×</a>
	    </div>
	</div>
</div>
<script>
function addRow(){  
	var i = $("#i_hidden").val();
	var rowStr = "<tr id='tr_contact_"+i+"'>";
	rowStr += "<td class='col1'><input maxlength='50' type='text' required data-rule='required;chinese;length[~45];' class='input_m' name='contacts["+i+"].contactor'></td>";
	rowStr += "<td class='col2'><input maxlength='50' type='text' required data-rule='required;chinese;length[~45];' class='input_m' name='contacts["+i+"].position'></td>";
	rowStr += "<td class='col3'><input maxlength='50' type='text' required data-rule='required;length[~45];' class='input_n' name='contacts["+i+"].telphone'></td>";
	rowStr += "<td class='col4'><input maxlength='20' type='text' required data-rule='required;mobile;length[~45];' class='input_n' required data-rule='required;length[~20];' name='contacts["+i+"].mobile'></td>";
	rowStr += "<td class='col5'><input maxlength='20' type='text' class='input_n' name='contacts["+i+"].fax'></td>";
	rowStr += "<td class='col6'><input maxlength='50' type='text' required data-rule='required;email;length[~45];' class='input_l' name='contacts["+i+"].email'></td>";
	rowStr += "<td class='col7'><a href='javascript:delRow("+i+")'>删除</a></td>";
	rowStr += "</tr>";
	$("#contact").append(rowStr);
	$("#i_hidden").val(parseInt(i)+1);
}

function delRow(i) {
	$("#contactsId"+i).val($("#contacts"+i).val());	
	$("#tr_contact_"+i).remove();
}

function addRowOne(){  
	var j = $("#j_hidden").val();
	var rowStr = "<tr id='tr_one_"+j+"'>";
	rowStr += "<td><input type='text' class='input_m'  name='quality.equipOne["+j+"].name' /></td>";
	rowStr += "<td><input type='text' class='input_n'  name='quality.equipOne["+j+"].brand'/></td>";
	rowStr += "<td><input type='text' class='input_m'  data-rule='required;integer;length[~45];'name='quality.equipOne["+j+"].eqNumber'/></td>";
	rowStr += "<td><input type='text' class='input_n'  name='quality.equipOne["+j+"].eqProduction'/></td>";
	rowStr += "<td><input type='text' class='input_n'  data-rule='required;integer;length[~45];' name='quality.equipOne["+j+"].eqLimit'/></td>";
	rowStr += "<td><a href='javascript:delRowOne("+j+")' class='delete'>删除</a></td>";
	rowStr += "</tr>";
	$("#one").append(rowStr);
	$("#j_hidden").val(parseInt(j)+1);
}

function delRowOne(j) {
	$("#equipOneId"+j).val($("#equipOne"+j).val());	
	$("#tr_one_"+j).remove();
}
function addRowTwo(){  
	var k = $("#k_hidden").val();
	var rowStr = "<tr id='tr_two_"+k+"'>";
	rowStr += "<td><input type='text' class='input_m'  name='quality.equipTwo["+k+"].name' /></td>";
	rowStr += "<td><input type='text' class='input_n'  name='quality.equipTwo["+k+"].brand'/></td>";
	rowStr += "<td><input type='text' class='input_m'  data-rule='integer;length[~45];' name='quality.equipTwo["+k+"].eqNumber'/></td>";
	rowStr += "<td><input type='text' class='input_n'  name='quality.equipTwo["+k+"].eqProduction'/></td>";
	rowStr += "<td><input type='text' class='input_n'  data-rule='integer;length[~45];' name='quality.equipTwo["+k+"].eqLimit'/></td>";
	rowStr += "<td><a href='javascript:delRowTwo("+k+")' class='delete'>删除</a></td>";
	rowStr += "</tr>";
	$("#two").append(rowStr);
	$("#k_hidden").val(parseInt(k)+1);
}

function delRowTwo(k) {
	$("#equipTwoId"+k).val($("#equipTwo"+k).val());	
	$("#tr_two_"+k).remove();
}
function addRowThree(){  
	var m = $("#m_hidden").val();
	var rowStr = "<tr id='tr_three_"+m+"'>";
	rowStr += "<td><input type='text' class='xxx_l'  name='quality.materials["+m+"].name' /></td>";
	rowStr += "<td><input type='text' class='xxx_l'  name='quality.materials["+m+"].factory'/></td>";
	rowStr += "<td><a href='javascript:delRowThree("+m+")' class='delete'>删除</a></td>";
	rowStr += "</tr>";
	$("#three").append(rowStr);
	$("#m_hidden").val(parseInt(m)+1);
}

function delRowThree(m) {
	$("#materialsId"+m).val($("#materials"+m).val());	
	$("#tr_three_"+m).remove();
}
function hide(){
	$("#infoDiv").hide();
}
function saveContacts() {
	$("#hidden").val("0");
	//如果全部验证通过,保存数据,否则跳过验证保存部分数据
	if(validate($("#contactsForm"))){
		$("#submit").attr("disabled","disabled"); 
		save();		
	 }else{		 
 		 $('#infoDiv').show();	
 		 return;
	 }
}
function enterpriseSave() {
	$("#hidden").val("1");	
		if(validate($("#enterpriseForm"))){
			 save();
		 }else{		 
	 		 $('#infoDiv').show();		 
		 }
	
}
function financeSave() {
	$("#hidden").val("2");
	if(validate($("#fianceForm"))){
		 save();
	 }else{		 
 		 $('#infoDiv').show();		 
	 }
	
}
function telSave() {
	$("#hidden").val("3");
    save();

}
function saveOther() {
	$("#hidden").val("4");	
	    save();		
}
function savefile() {
 	$("#hidden").val("5");	
// 	//验证必须上传的文件
// 	if($("#fileName1").val() == ""){alert("请上传营业执照副本!"); return;}
// 	if($("#fileName2").val() == ""){alert("请上传税务登记证!"); return;}
// 	if($("#fileName3").val() == ""){alert("请上传组织机构代码证!"); return;}
// 	if($("#fileName4").val() == ""){alert("请上传银行开户许可证!"); return;}
// 	if($("#fileName5").val() == ""){alert("请上传供应商银行结算信息表!"); return;}
	save();	
}
function save(){
	var i = parseInt($("#hidden").val());
	switch (i)
	{
	case 0:
		var params = $("#contactsForm").serialize();
	  break;
	case 1:
		var params = $("#enterpriseForm").serialize();
	  break;
	case 2:
		var params = $("#fianceForm").serialize();
	  break;
	case 3:
		var params = $("#telForm").serialize();
	  break;
	case 4:
		var params = $("#otherForm").serialize();
	  break;
	case 5:
		var params = $("#fileForm").serialize();
	  break;
	}
	var urlpara = "/supplier/supplier_save?random="+Math.random();
		$.ajax({
			url : urlpara,
			type : "POST",
			data : params,
			dataType : "json",
			cache:false,
			success : function(data) {
				if(data == 1){
					hide();	
				 	alert("操作成功!");
				 	if(i == '0'){ $("#11").load(location.href+" #11");}
				 	if(i == '1'){$("#22").load(location.href + " #22");}
				 	if(i == '2'){$("#33").load(location.href + " #33");}
				 	if(i == '3'){$("#44").load(location.href + " #44");}
				 	if(i == '4'){$("#55").load(location.href + " #55");}
// 				 	if(i == '5'){$("#66").load(location.href + " #66");}
				 //调用js方法
				}else if(data == 3){
					hide();
					alert("供应商名称已经存在,请重新输入!");
					return false;
				}else{
					hide();
					alert("操作异常,请联系管理员!");
					return false;
				}
			}
		
		});
		
}

$(".placeholder").focus(function(){
        $(this).removeClass("placeholder");
        });
$(".placeholder").blur(function(){
        if($(this.val()=="")){
            $(this).removeClass("placeholder");
        }
        });
$("input[type=text]").focus(function(){$(this).addClass("focus");});
$("input[type=text]").blur(function(){$(this).removeClass("focus");});     


</script>

<script type="text/javascript">

$(".edit").click(function(){
    var $parent=$(this).parents(".info_item");
    if($parent.hasClass("status_open")) {
        $parent.removeClass("status_open");
    } else  {
        $(".info_item").removeClass("status_open");
        $parent.addClass("status_open");
    }
    
});


</script>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<div class="grid_c1">
    <div class="mod_col cg_f4_friendslink">
        <div class="mod_col_hd">
            <h2 class="title">友情链接</h2>
        </div>
        <div class="mod_col_bd">
            <ul class="clearfix">
            <c:forEach items="${bottomList}" var="adv">
                <li><a href="${adv.linkUrl}" target="_blank" title="${adv.advertiser}"><img src="${adv.picUrl}" alt="${adv.advertiser}" /></a></li>
            </c:forEach>
            </ul>
        </div>
    </div>
</div>

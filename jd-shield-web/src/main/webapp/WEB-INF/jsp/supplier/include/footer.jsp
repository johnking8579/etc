<%@ page language="java" pageEncoding="UTF-8"%>
<div class="cg_footer">
	<div class="grid_c1">
	    <ul class="cg_footer_list clearfix">
	        <li><a href="${pageContext.request.contextPath}/supplier/footer/guanyuwomen" target="_blank">关于我们</a></li>
	        <li><a href="${pageContext.request.contextPath}/supplier/footer/lianxiwomen" target="_blank">联系我们</a></li>
	        <li><a href="http://sale.jd.com/act/y3surX7qpM.html" target="_blank">广告服务</a></li>
	        <li><a href="http://b.jd.com" target="_blank">京东购物</a></li>
	        <li><a href="${pageContext.request.contextPath}/supplier/footer/bangzhuwendang" target="_blank">常见问题</a></li>
	        <li><a href="${pageContext.request.contextPath}/supplier/footer/friends_link" target="_blank">友情链接</a></li>
	    </ul>
	</div>
</div>

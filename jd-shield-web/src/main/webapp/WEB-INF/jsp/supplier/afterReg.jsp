<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%-- <base href="<%=basePath%>"> --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	<title>京东采购_京盾招投标_首页</title>
	<link rel="stylesheet" href="http://misc.360buyimg.com/lib/skin/2013/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/findpassword.css" type="text/css" media="screen" />
	<script type="text/javascript" src="../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../common/js/slider.js"></script>
</head>
<body> 
	<div class="cg_header">
		<div class="grid_c1">
			<h2 class="title"><a href="#" title="京东采购"><img src="../common/img/logo.png" alt="京东采购"/></a></h2> 
		</div>
	</div>
    <div class="cg_content"> 
        <div class="grid_c1">
            <div class="mod_page">
                <h2 class="mod_page_hd">注册</h2>
                <div class="mod_page_bd">
                    <div class="mod_form">
                    	<c:choose>
                    	<c:when test="${stat == 0}">
	                        <!-- 注册成功 S -->
	                        <div class="fp_succeed">
	                            <i class="i_succeed"></i>
	                            <p class="succeed">恭喜您，注册成功！</p>
	                            <p>
	                                <a href="${pageContext.request.contextPath}/supplier/index" class="back">返回主页</a>
	                                <a href="${pageContext.request.contextPath}/supplier/index?loginStatus=1&account=${account}" class="login">登录</a>
	                            </p>
	                        </div>
	                        <!-- 注册成功 E -->
                    	</c:when>
                    	<c:otherwise>
                    		<p>出错了，请重新注册或联系管理员。<br><a href="${pageContext.request.contextPath}/supplier/index" class="back">返回主页</a></p>
                    	</c:otherwise>
                    	</c:choose>
                    </div>
                </div><!-- / fp_bd -->
            </div> <!-- / findpassword -->
        </div><!-- grid_c1 -->
    </div><!-- cg_content -->

    <%@include file="include/footer.jsp" %>

</body>
</html>

<%@page import="java.util.List"%>
<%@page import="com.jd.supplier.model.Requirement"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" %>
<%@page import="com.jd.official.modules.dict.model.DictData"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta  charset="utf-8" />
<title>京东采购_京盾招投标_首页</title>
<link rel="stylesheet" href="../../../common/css/global.css" type="text/css" media="screen" />
<link rel="stylesheet" href="../../../common/css/index.css" type="text/css" media="screen" />
<script type="text/javascript" src="../../../common/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../common/js/slider.js"></script>
</head>
<body>
<jsp:include page="../supplier/include/loginToolbar.jsp" />
    <jsp:include page="../supplier/include/header.jsp" />
    <%
	    List<DictData> isYearDatalist=(List<DictData>)request.getAttribute("isYearDatalist");
	    List<DictData> budgetSourceDatalist=(List<DictData>)request.getAttribute("budgetSourceDatalist");
	    List<DictData> sampleTestDatalist=(List<DictData>)request.getAttribute("sampleTestDatalist");
	    List<DictData> sceneTestDatalist=(List<DictData>)request.getAttribute("sceneTestDatalist");
	    List<DictData> recommendLevelDatalist=(List<DictData>)request.getAttribute("recommendLevelDatalist");
	    Requirement requirement=(Requirement)request.getAttribute("requirement");
    %>
    <div class="cg_content">
        <!-- 招标预告和公告栏 S -->
        <div class="grid_c1">
            <div class="page_detail"> 
                <div class="page_detail_hd">
                    <h2>项目详细情况</h2>
                </div>
                <div class="page_detail_bd">
                    <div class="detail_item">
                        <div class="detail_item_tit">项目名称：${requirement.projectName}</div>
                        <div class="detail_item_tit">申请日期：${requirement.projectDate}</div>
                    </div>
                    <div class="detail_item">
                   		<div class="detail_item_tit">是否年度招标：
                   		<%if(isYearDatalist!=null){
	                   		    for(int i=0;i<isYearDatalist.size();i++){
	                   				DictData dictdata=isYearDatalist.get(i);
	                   				if(String.valueOf(dictdata.getId()).equals(requirement.getIsYear())){
	                   				    out.print(dictdata.getDictDataName());
	                   				}
	                   		    }
                   		  }  
                   		 %>
                   		</div>
                        <div class="detail_item_tit">预算来源：
                        <%if(budgetSourceDatalist!=null){
	                   		    for(int i=0;i<budgetSourceDatalist.size();i++){
	                   				DictData dictdata=budgetSourceDatalist.get(i);
	                   				if(String.valueOf(dictdata.getId()).equals(requirement.getBudgetSource())){
	                   				    out.print(dictdata.getDictDataName());
	                   				}
	                   		    }
                   		  } %>
                       </div>
                    </div>
                    <div class="detail_item">
                       <div class="detail_item_tit">预算金额：${requirement.budgetAmount}</div>
                        <div class="detail_item_tit">计划外或紧急原因：${requirement.outsideReason}</div>
                    </div>
                    <div class="detail_item">
                         <div class="detail_item_tit">样品测试：
                         <%if(sampleTestDatalist!=null){
	                   		    for(int i=0;i<sampleTestDatalist.size();i++){
	                   				DictData dictdata=sampleTestDatalist.get(i);
	                   				if(String.valueOf(dictdata.getId()).equals(requirement.getSampleTest())){
	                   				    out.print(dictdata.getDictDataName());
	                   				}
	                   		    }
                   		  } %>
                   		  </div>
                        <div class="detail_item_tit">是否现场审核：
                         <%if(sceneTestDatalist!=null){
	                   		    for(int i=0;i<sceneTestDatalist.size();i++){
	                   				DictData dictdata=sceneTestDatalist.get(i);
	                   				if(String.valueOf(dictdata.getId()).equals(requirement.getSceneTest())){
	                   				    out.print(dictdata.getDictDataName());
	                   				}
	                   		    }
                   		  } %>
                        </div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">供应商推荐要求：
                        <%if(recommendLevelDatalist!=null){
	                   		    for(int i=0;i<recommendLevelDatalist.size();i++){
	                   				DictData dictdata=recommendLevelDatalist.get(i);
	                   				if(String.valueOf(dictdata.getId()).equals(requirement.getRecommendLevel())){
	                   				    out.print(dictdata.getDictDataName());
	                   				}
	                   		    }
                   		  } %>
                        </div>
                        <div class="detail_item_tit">技术规格及参数补充：${requirement.technical}</div>
                    </div>
                    <div class="detail_item">
                         <div class="detail_item_tit">资质要求：${requirement.aptitude}</div>
                        <div class="detail_item_tit">验收标准：${requirement.standard}</div>
                    </div>
                    <div class="detail_item">
                       <div class="detail_item_tit">配送地址：${requirement.address}</div>
                        <div class="detail_item_tit">交货期：${requirement.deliveryDate}</div>
                    </div> 
                    <div class="detail_item">
                        <div class="detail_item_tit">付款方式：${requirement.payMethod}</div>
                        <div class="detail_item_tit">售后服务要求：${requirement.serviceRequire}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">现合作单位服务情况：${requirement.cooperationDetail}</div>
                        <div class="detail_item_tit">其它补充说明：${requirement.supplementInfo}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">需求部门建议：${requirement.depSuggestion}</div>
                        <div class="detail_item_tit">申请人：${requirement.applicant}</div>
                    </div>
                    <div class="detail_item">
                        <div class="detail_item_tit">申请人联系电话：${requirement.applicantTel}</div>
                        <div class="detail_item_tit">申请部门：${requirement.applicantDep}</div>
                    </div>
                    
                    <div class="detail_action">
                        
                    </div>
                </div>
                
                
                
                
            </div>
        </div> 
        <!-- 招标预告和公告栏 E --> 
        <c:import url="${pageContext.request.contextPath}/adv/bottom"></c:import>
    </div>

	
	   <jsp:include page="../supplier/include/footer.jsp" />








<script>
		var slider = new Slider('#J_slider',{effect: 'fade'});
</script>

</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta  charset="utf-8" />
	<title>京东采购_修改密码</title>
	<link rel="stylesheet" href="http://misc.360buyimg.com/lib/skin/2013/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/global.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../common/css/findpassword.css" type="text/css" media="screen" />
	<script type="text/javascript" src="../common/js/jquery.min.js"></script>
	<script type="text/javascript" src="../common/js/jquery.md5.js"></script>
	<script type="text/javascript">
		$(function(){
			$("#newpwd").blur(checkPwd);
			$("#newpwd2").blur(checkPwd2);
		});
		
		function checkPwd()	{
			return validateCallback($("#newpwd"),/^.{6,20}$/i.test($("#newpwd").val()));
		}
		
		function checkPwd2(){
			return validateCallback($("#newpwd2"), $("#newpwd2").val() != '' && $("#newpwd").val() == $("#newpwd2").val());
		}
		
		function submit()	{
			if(checkPwd()==false || checkPwd2()==false)	
				return;
			
			$.post("doChangePwd", {oldPwd:$.md5($("#oldPwd").val()), newPwd:$.md5($("#newpwd2").val())}, function(data){
				if(data.err == 0)	{
					$("#step1").hide();
					$("#step2").show();
				} else if(data.err == 1)	{
					$("input").val('');
					$("#newpwd, #newpwd2").parents(".form_item").removeClass("error right");
					validateCallback($("#oldPwd"), false);
				} else	{
					alert("系统错误，请联系管理员。");
				}
			}, 'json');
		}
		
		/**
		 * input校验后改变样式
		 * @param inputEle 输入框
		 * @param isSuccess 输入是否有效
		 * @return isSuccess
		 */
		function validateCallback(inputEle, isSuccess)	{
			var div = $(inputEle).parents(".form_item").removeClass("error right");
			if(isSuccess)	div.addClass("right");
			else			div.addClass("error");
			return isSuccess;
		}
	</script>
</head>
<body>
    <%@ include file="include/loginToolbar.jsp" %>
    <%@ include file="include/header.jsp" %>
    <div class="cg_content">
        <!-- 修改密码 S -->
        <div class="grid_c2a">
            <div class="grid_m">
                <div class="grid_m_inner">
                   <div class="mod_page">
                        <h2 class="mod_page_hd">修改密码</h2>
                        <div class="mod_page_bd">
                        
	                        <div id="step1">
	                            <div class="form_item">
	                                <label class="item_lb" for="">原密码：</label>
	                                <span class="input_wrap">
	                                    <input id="oldPwd" class="input_xxl password placeholder" type="password">
	                                    <i class="i_icon i_right"></i>
	                                    <i class="i_icon i_error"></i>
	                                </span>
	                                <p class="p_tip"></p>
	                                <p class="p_error">密码不正确</p>
	                            </div>
	                            <div class="form_item">
	                                <label class="item_lb" for="">新密码：</label>
	                                <span class="input_wrap">
	                                    <input id="newpwd" class="input_xxl password placeholder" type="password" placeholder="由6-20个英文字母（区分大小写）或数字组成">
	                                    <i class="i_icon i_right"></i>
	                                    <i class="i_icon i_error"></i>
	                                </span>
	                                <p class="p_tip"></p>
	                                <p class="p_error">由6-20个英文字母（区分大小写）或数字组成</p>
	                            </div>
	                            <div class="form_item ">
	                                <label class="item_lb" for="">确认密码：</label>
	                                <span class="input_wrap">
	                                    <input id="newpwd2" class="input_xxl repassword placeholder" type="password" placeholder="请再输入一遍您在上面填写的密码">
	                                    <i class="i_icon i_right"></i>
	                                    <i class="i_icon i_error"></i>
	                                </span>
	                                <p class="p_tip"></p>
	                                <p class="p_error">两次输入不一致</p>
	                            </div>
	                            <div class="form_action">
	                                <a class="btn_submit" href="javascript:void(0);" onclick="submit()"><span>提交</span></a>
	                            </div>
	                        </div>
                            
                            
                            <!-- 修改成功 S -->
                            <div id="step2" class="fp_succeed" style="display:none">
                                <i class="i_succeed"></i>
                                <p class="succeed">恭喜您，修改成功！</p>
                                <p>
                                    <a href="${pageContext.request.contextPath}/supplier/index" class="back">返回主页</a>
                                </p>
                            </div>
                            <!-- 修改成功 E -->


                        </div>
                   </div>
                </div><!-- /grid_m_inner -->
            </div><!-- /grid_m -->
           <%@include file="include/left.jsp" %>
        </div> 
        <!-- 修改密码 E --> 
    </div>
	<%@include file="include/footer.jsp" %>
</body>
</html>

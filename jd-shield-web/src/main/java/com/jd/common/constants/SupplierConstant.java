package com.jd.common.constants;

public class SupplierConstant {
	
	
	/**
	 * 币种类型
	 */
	public static final String CURRENCY = "currency";
	/**
	 * 供应商覆盖范围
	 */
	public static final String SUPPLIER_COVERAGE = "supplier_coverage";
	/**
	 * 供应商归属区域
	 */
	public static final String SUPPLIER_OWNER = "supplier_owner";
	/**
	 * 供应商所属地
	 */
	public static final String SUPPLIER_HOME = "supplier_home";
	/**
	 * 资料完整度
	 */
	public static final String SUPPLIER_DATA_INTEGRITY = "data_integrity";
	/**
	 * 供应商组织
	 */
	public static final String SUPPLIER_ORG = "supplier_org";
	/**
	 * 供应商类型
	 */
	public static final String SUPPLIER_TYPE = "supplier_type";
	/**
	 * 企业性质
	 */
	public static final String ENTERPRISE = "enterprise";
	/**
	 * 默认申报类型
	 */
	public static final String TAX_TYPE = "tax";
	/**
	 * 认证状况
	 */
	public static final String AUTHENTIC_TYPE = "authentic";
}

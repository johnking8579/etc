package com.jd.common.constants;

/**
 * 通用常量
 * @author wangchanghui
 *
 */
public class CommonConstant {
	
	/**
	 * 符号常量
	 * @author wangchanghui
	 *
	 */
	public static class Symbol{
		
		/**
		 * windows下换行
		 */
		public static final String WRAP = "\r\n";
		
		/**
		 * 冒号
		 */
		public static final String COLON = ":";
		
		/**
		 * 逗号
		 */
		public static final String COMMA = ",";
		
		/**
		 * and
		 */
		public static final String AND = "&";
		
		/**
		 * 等号
		 */
		public static final String EQUAL_SIGN = "=";
	}
}

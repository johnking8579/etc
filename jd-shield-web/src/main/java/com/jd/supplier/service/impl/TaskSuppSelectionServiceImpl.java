package com.jd.supplier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.common.util.PaginatedList;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.TaskSuppSelectionDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.service.TaskSuppSelectionService;
import com.jd.supplier.web.vo.NoticeVO;
import com.jd.supplier.web.vo.PreSelectionVO;

@Service("taskSuppSelectionService")
public class TaskSuppSelectionServiceImpl extends BaseServiceImpl<TaskSuppSelection, Long> implements
	TaskSuppSelectionService {

    @Autowired
    private TaskSuppSelectionDao taskSuppSelectionDao;

    @Override
    public TaskSuppSelection getTaskSuppSelection(Long id) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<TaskSuppSelection> findTaskSuppSelectionList(Long taskId) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId) {
	return taskSuppSelectionDao.findTaskSuppSelectionListByReqId(requirementId);
    }

    public TaskSuppSelectionDao getDao() {
	return taskSuppSelectionDao;
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    public PaginatedList<TaskSuppSelection> pageSearchTaskSuppSelection(PreSelectionVO preSelectionVO) {

	/** 查询总记录数 */
	int count = taskSuppSelectionDao.countTaskSuppSelection(preSelectionVO);

	/** 设置总记录数 */
	preSelectionVO.setTotalItem(count);

	/** 查询记录list */
	List<TaskSuppSelection> list = taskSuppSelectionDao.listTaskSuppSelection(preSelectionVO);

	preSelectionVO.getPaginList().addAll(list);

	return preSelectionVO.getPaginList();
    }

}

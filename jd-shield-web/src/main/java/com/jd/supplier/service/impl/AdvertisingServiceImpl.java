package com.jd.supplier.service.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.Page;
import com.jd.supplier.dao.AdvertisingDao;
import com.jd.supplier.model.Advertising;
import com.jd.supplier.service.AdvertisingService;

@Component("advertisingService")
public class AdvertisingServiceImpl implements AdvertisingService	{
	
	@Resource
	private AdvertisingDao advertisingDao;

	@Override
	public Map<String, String> findPositionValue() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put(Advertising.POSITION_TOP, "顶部广告");
		map.put(Advertising.POSITION_BOTTOM, "底部广告");
		return map;
	}

	@Override
	public Page<Advertising> findByPosition(String position, int offset,
			int pageSize) {
		return advertisingDao.findByPosition(position, offset, pageSize);
	}


}

package com.jd.supplier.service;

import java.util.List;

import com.jd.common.util.PaginatedList;
import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.web.vo.NoticeVO;
import com.jd.supplier.web.vo.PreSelectionVO;

public interface TaskSuppSelectionService extends BaseService<TaskSuppSelection, Long> {

    TaskSuppSelection getTaskSuppSelection(Long id);

    List<TaskSuppSelection> findTaskSuppSelectionList(Long taskId);

    List<Supplier> findTaskSuppSelectionListByReqId(Long reqId);
    
    PaginatedList<TaskSuppSelection> pageSearchTaskSuppSelection(PreSelectionVO preSelectionVO);

}

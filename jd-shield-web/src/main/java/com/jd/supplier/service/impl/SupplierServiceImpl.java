package com.jd.supplier.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.common.constants.SupplierConstant;
import com.jd.official.core.dao.BaseDao;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.official.modules.dict.dao.DictDataDao;
import com.jd.official.modules.dict.model.DictData;
import com.jd.supplier.dao.IContactsDao;
import com.jd.supplier.dao.IEnterpriseDao;
import com.jd.supplier.dao.IEquipDao;
import com.jd.supplier.dao.IFinanceDao;
import com.jd.supplier.dao.IMaterialDao;
import com.jd.supplier.dao.IOtherDao;
import com.jd.supplier.dao.IQualityDao;
import com.jd.supplier.dao.ISupplierDao;
import com.jd.supplier.dao.RequirementAffixDao;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.model.Contacts;
import com.jd.supplier.model.Enterprise;
import com.jd.supplier.model.Equip;
import com.jd.supplier.model.Finance;
import com.jd.supplier.model.Material;
import com.jd.supplier.model.Other;
import com.jd.supplier.model.Quality;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.service.SupplierService;

@Component("supplierService")
public class SupplierServiceImpl extends BaseServiceImpl<Supplier, Long>
		implements SupplierService {
	static Logger log = Logger.getLogger(SupplierServiceImpl.class);
	@Autowired
	private JssService jssService;
	@Autowired
	private DictDataDao dictDataDao;
	@Autowired
	private ISupplierDao supplierDao;
	@Autowired
	private IContactsDao contactsDao;
	@Autowired
	private IEnterpriseDao enterpriseDao;
	@Autowired
	private IFinanceDao financeDao;
	@Autowired
	private IOtherDao otherDao;
	@Autowired
	private IQualityDao qulityDao;
	@Autowired
	private IMaterialDao materialDao;
	@Autowired
	private IEquipDao equipDao;
	@Autowired
	private RequirementAffixDao requirementAffixDao;

	@Override
	public void save4Register(Supplier supplier) {
		if (supplier.getId() == null) {
			supplier.setDataStatus("1");
			supplier.setSupplierStatus("0");
			supplierDao.insert(supplier);
		} else {
			supplierDao.update(supplier);
		}
	}

	@Override
	public Supplier findByAccount(String account) {
		List<Supplier> list = supplierDao.findByUserCode(account);
		return list.isEmpty() ? null : list.get(0);
	}

	/**
	 * 完善我的资料
	 * 
	 * @param supplier
	 * @return
	 */
	@Override
	public int insertData(Supplier supplier) throws Exception {
		try {
			Supplier s = supplierDao.get(supplier.getId());
			if (s!=null && s.getDataIntegrity() != null
					&& !"".equals(s.getDataIntegrity())) {
				DictData dd = (DictData) dictDataDao.get(Long.valueOf(s
						.getDataIntegrity()));
				if (dd != null) {
					if ("100".equals(dd.getDictDataName())) {
						supplier.setDataIntegrity(String.valueOf(dd.getId()));
					} else {
						supplier.setDataIntegrity(this.getDictId(supplier));
					}
				} else {
					supplier.setDataIntegrity("0");
				}
			}
			// 首先更新基本信息表资料完整度
			supplier.setSupplierStatus("0");
			supplierDao.update(supplier);
			// 插入或者更新联系人表
			if (supplier.getContacts() != null
					&& supplier.getContacts().size()>0) {
				for (Contacts con : supplier.getContacts()) {
					if (con.getId() != null) {
						if (con.getIds() != null && !"".equals(con.getIds())) {// 删除记录
							contactsDao.delete(Long.valueOf(con.getIds()));
						} else {
							contactsDao.update(con);
						}
					} else {
						con.setSupplierId(supplier.getId());
						contactsDao.insert(con);
					}
				}
			}
			// 插入或者更新企业基本信息表
			Enterprise en = supplier.getEnterprise();
			if (en != null) {
				//防止重复提交
				if(s.getEnterprise()!=null){
					if (s.getEnterprise().getId() != null && s.getEnterprise().getId()>0) {
					en.setId(s.getEnterprise().getId());
					}
				}
				//更新或者插入
				if (en.getId() != null && en.getId()>0) {
					enterpriseDao.update(en);
				} else {
					supplier.setName(supplier.getEnterprise().getName());
					if (supplier.getEnterprise().getName() != null
							&& !"".equals(supplier.getEnterprise().getName())) {
						List<Supplier> status = supplierDao.find(supplier);
						if (status.size() > 0) {
							return 3;
						}
					}
					en.setSupplierId(supplier.getId());
					enterpriseDao.insert(en);
				}
			}
			// 插入或者更新财务信息基本表
			Finance fi = supplier.getFinance();
			if (fi != null) {
				//防止重复提交
				if(s.getFinance()!=null){
					if (s.getFinance().getId() != null && s.getFinance().getId()>0) {
					fi.setId(s.getFinance().getId());
					}
				}
				//更新或者插入
				if (fi.getId() != null && fi.getId()>0) {
					financeDao.update(fi);
				} else {
					fi.setSupplierId(supplier.getId());
					financeDao.insert(fi);
				}
			}
			// 插入或者更新技术状况及质量控制表
			Quality quality = supplier.getQuality();
			if (quality != null) {
				//防止重复提交
				if(s.getQuality()!=null){
					if(s.getQuality().getId()!=null && s.getQuality().getId()>0){
					quality.setId(s.getQuality().getId());
					}
				}
				//插入或者更新
				if (quality.getId() != null && quality.getId()>0) {
					qulityDao.update(quality);
				} else {
					quality.setSupplierId(supplier.getId());
					quality.setStatus("1");
					qulityDao.insert(quality);
				}
				// 检测设备表
				if (supplier.getQuality().getEquipOne() != null
						&& supplier.getQuality().getEquipOne().size()>0) {
					for (Equip eq : supplier.getQuality().getEquipOne()) {
						if (eq.getId() != null) {
							if (eq.getIdOne() != null
									&& !"".equals(eq.getIdOne())) {
								equipDao.delete(Long.valueOf(eq.getIdOne()));
							} else {
								equipDao.update(eq);
							}
						} else {
							eq.setQualityId(quality.getId());
							eq.setType("1");
							equipDao.insert(eq);
						}
					}
				}
				if (supplier.getQuality().getEquipTwo() != null
						&& supplier.getQuality().getEquipTwo().size()>0) {
					for (Equip eq : supplier.getQuality().getEquipTwo()) {
						if (eq.getId() != null) {
							if (eq.getIdTwo() != null
									&& !"".equals(eq.getIdTwo())) {
								equipDao.delete(Long.valueOf(eq.getIdTwo()));
							} else {
								equipDao.update(eq);
							}
						} else {
							eq.setQualityId(quality.getId());
							eq.setType("2");
							equipDao.insert(eq);
						}
					}
				}
				// 原材料表
				if (supplier.getQuality().getMaterials() != null
						&& supplier.getQuality().getMaterials().size()>0) {
					for (Material m : supplier.getQuality().getMaterials()) {
						if (m.getId() != null) {
							if (m.getIds() != null && !"".equals(m.getIds())) {
								materialDao.delete(Long.valueOf(m.getIds()));
							} else {
								materialDao.update(m);
							}
						} else {
							m.setQualityId(quality.getId());
							materialDao.insert(m);
						}
					}
				}
			}
			// 插入或者其它信息表
			Other o = supplier.getOther();			
			if (o != null) {
				//防止重复提交
				if(s.getOther()!=null){
					if (s.getOther().getId() != null && s.getOther().getId()>0) {
					o.setId(s.getOther().getId());
					}
				}
				if (o.getId() != null && o.getId()>0) {
					otherDao.update(o);
				} else {
					o.setSupplierId(supplier.getId());
					otherDao.insert(o);
				}
			}
			// 插入上传文件表
			if (supplier.getAffix() != null && supplier.getAffix().size()>0) {
				for (RequirementAffix affix : supplier.getAffix()) {
					//如果过来的数据不为空,且在数据库中存在,则先删除数据库中的代码,然后再更新
					if(!"".equals(affix.getFileName())&& !"".equals(affix.getFileKey())){
						affix.setBussineId(supplier.getId());
						RequirementAffix raffix =(RequirementAffix) requirementAffixDao.get("getAffix", affix);
						if(raffix!=null){
							if(raffix.getFileKey().equals(affix.getFileKey())){//只更新数据库
								raffix.setFileName(affix.getFileName());
								raffix.setFileKey(affix.getFileKey());
								requirementAffixDao.update(raffix);
							}else{//先更新数据库,再删除jss
								raffix.setFileName(affix.getFileName());
								raffix.setFileKey(affix.getFileKey());
								requirementAffixDao.update(raffix);
								jssService.deleteObject(
										raffix.getFileKey().substring(7, raffix.getFileKey().lastIndexOf("/")),
										raffix.getFileKey().substring(raffix.getFileKey().lastIndexOf("/") + 1));					  
							}
					 }else{						 
						 //插入数据库
						affix.setBussineId(supplier.getId());
						affix.setBussineType("5");
						requirementAffixDao.insert(affix); 
					 }
					}					
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("完善个人资料信息异常...", e);
			return 0;
		}
		return 1;
	}

	/**
	 * 获取字典表的ID
	 * 
	 * @param supplier
	 * @return
	 */
	public String getDictId(Supplier supplier) {
		String id = null;
		List<DictData> data = dictDataDao
				.findDictDataList(SupplierConstant.SUPPLIER_DATA_INTEGRITY);
		for (int i = 0; i < data.size(); i++) {
			DictData res = data.get(i);
			if (supplier.getDataIntegrity().equals(res.getDictDataName())) {
				id = String.valueOf(res.getId());
				return id;
			}
		}
		return "";
	}

	public ISupplierDao getDao() {
		return supplierDao;
	}
}

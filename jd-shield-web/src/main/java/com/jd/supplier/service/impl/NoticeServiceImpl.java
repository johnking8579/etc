package com.jd.supplier.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.jd.common.util.PaginatedList;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.NoticeDao;
import com.jd.supplier.dao.NoticeMapper;
import com.jd.supplier.model.Notice;
import com.jd.supplier.service.NoticeService;
import com.jd.supplier.web.vo.NoticeVO;

@Service("noticeService")
@Transactional
public class NoticeServiceImpl extends BaseServiceImpl<Notice, Long> implements NoticeService {
    @Autowired
    private NoticeDao noticeDao;

    public NoticeDao getDao() {
	return noticeDao;
    }
    
  /*  @Autowired
    protected  NoticeMapper noticeMapper;*/

    @SuppressWarnings("unchecked")
    @Override
    public PaginatedList<Notice> pageSearchSrSupplier(NoticeVO noticevo) {

	/** 查询总记录数 */
	int count = noticeDao.countNotice(noticevo);

	/** 设置总记录数 */
	noticevo.setTotalItem(count);

	/** 查询记录list */
	List<Notice> list = noticeDao.listNotice(noticevo);

	noticevo.getPaginList().addAll(list);

	return noticevo.getPaginList();
    }

}

package com.jd.supplier.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Util {
	
	/**
	 * 生成一串随机数组成的字符串.可重复。
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randStr(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
	
	/**
	 * 生成一串随机数组成的字符串.
	 * @param num 随机数的位数
	 * @param isRepeat 是否重复
	 * @return
	 */
	public static String randStr(int num, boolean isRepeat)	{
		if(!isRepeat && num > 10)	{
			throw new IllegalArgumentException("num值不能大于10");
		} else	{
			LinkedList<Integer> list = new LinkedList<Integer>(Arrays.asList(0,1,2,3,4,5,6,7,8,9));
			StringBuilder sb = new StringBuilder();
			for(int i=0; i<num; i++)	{
				Collections.shuffle(list);
				sb.append(isRepeat ? list.peek() : list.pop());
			}
			return sb.toString();
		}
	}
	
	/** 
	 * MD5加密
	 * @param str 要加密的字符串 
	 * @return    加密后的字符串 
	 */  
	public static String toMD5(String str){  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(str.getBytes());  
	        byte[] byteDigest = md.digest();  
	        int i;  
	        StringBuffer buf = new StringBuffer("");  
	        for (int offset = 0; offset < byteDigest.length; offset++) {  
	            i = byteDigest[offset];  
	            if (i < 0)  i += 256;  
	            if (i < 16) buf.append("0");  
	            buf.append(Integer.toHexString(i));  
	        }  
//	        return buf.toString().substring(8, 24);		//16位加密     
	        return buf.toString();						//32位加密    
	    } catch (NoSuchAlgorithmException e) {  
	        throw new RuntimeException(e); 
	    }
	}
	
	/**
	 * 生成文件的md5码
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static String toMD5(File file) throws IOException {  
    	InputStream fis = null;  
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);  
			byte[] buf = new byte[1024];  
			int len = 0;  
			while ((len = fis.read(buf)) > 0) {  
			    md.update(buf, 0, len);  
			}  
			byte[] bytes = md.digest();
			
			char[] hex = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};  
			StringBuffer sb = new StringBuffer(2 * bytes.length);  
	        for (int i = 0; i < bytes.length; i++) {  
	            char c0 = hex[(bytes[i] & 0xf0) >> 4];// 取字节中高 4 位的数字转换  
	            // 为逻辑右移，将符号位一起右移,此处未发现两种符号有何不同  
	            char c1 = hex[bytes[i] & 0xf];// 取字节中低 4 位的数字转换  
	            sb.append(c0).append(c1);  
	        }  
	        return sb.toString();  
        } catch (NoSuchAlgorithmException e1) {
        	throw new RuntimeException(e1);
		} finally	{
			try {
				if(fis != null)	fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }  
  
	
	/**
	 * 把数组转换成字符串, 每个单位间以splitter隔开
	 * @param array 
	 * @param splitter 分隔符
	 * @return
	 */
	public static String join(int[] array, String splitter)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<array.length; i++)	{
			sb.append(array[i]);
			if(i < array.length-1)
				sb.append(splitter);
		}
		return sb.toString();
	}
	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param list 集合
	 * @param splitter 分隔符
	 * @return
	 */
	public static String join(List<String> list, String splitter)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<list.size(); i++)	{
			sb.append(list.get(i));
			if(i < list.size() - 1)	
				sb.append(splitter);
		}
		return sb.toString();
	}

	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param array 数组
	 * @param splitter 分隔符
	 * @return
	 */
	public static String join(Object[] array, String splitter)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<array.length; i++)	{
			sb.append(array[i].toString());
			if(i < array.length - 1)	
				sb.append(splitter);
		}
		return sb.toString();
	}
	
	/**
	 * 将src字符串写入dest文件中
	 * @param rsc
	 * @param dest
	 * @throws IOException
	 */
	public static void writeFile(String rsc, File dest) throws IOException	{
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(dest, "utf-8");
			pw.print(rsc);
			pw.flush();
		} finally	{
			if(pw != null)
				pw.close();
		}
	}
	
	/**
	 * 截取字符串中, 最后一个句号后的字符串, 在url中, 它一般代表文件的后缀名
	 * 
	 * @param url
	 * @return
	 */
	public static String getSuffix(String url) {
		return url.substring(url.lastIndexOf("."));
	}
	
	/**
	 * 关闭多个流
	 * @param closeable
	 */
	public static void closeStream(Closeable...closeable)	{
		for(Closeable c : closeable)	{
			try {
				if(c != null)	c.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 压缩文件至target, zipEntry默认保存名为srcFiles的短文件名
	 * @param target 生成的压缩文件, 不能已存在
	 * @param srcFiles
	 * @throws IOException
	 */
	public static void zip(File target, File[] srcFiles) throws IOException	{
		Map<String, File> src = new HashMap<String, File>();
		for(File f : srcFiles)	{
			src.put(f.getName().replace(f.getParent(), ""), f);
		}
		zip(target, src);
	}
	
	
	/**
	 * 压缩文件至target
	 * @param target 生成的压缩文件, 不能已存在
	 * @param src key:源文件压缩后的名称(不能包含路径分隔符), value:源文件,不能为目录
	 * @return 
	 * @throws IOException 
	 */
	public static void zip(File target, Map<String, File> src) throws IOException {
		if(target.exists())
			throw new IOException(target + "已存在");
		
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(target));
		WritableByteChannel outCh = Channels.newChannel(zos);
		
		for(Entry<String, File> entry : src.entrySet())	{
			if(!entry.getValue().isFile())	
				throw new UnsupportedOperationException(entry.getValue() + "不是有效文件");
			zos.putNextEntry(new ZipEntry(entry.getKey()));
			FileChannel inCh = new FileInputStream(entry.getValue()).getChannel();
			
			ByteBuffer buf = ByteBuffer.allocate(8192);
			int len;
			while((len = inCh.read(buf)) != -1)	{
				buf.flip();
				outCh.write(buf);
				buf.clear();
			}
			inCh.close();
		}
		outCh.close();
	}
	
	
	/**
	 * 解压zip里的第一个文件至临时目录
	 * @param zipFile
	 * @return
	 * @throws IOException 
	 */
	public static File unzip(File zipFile) throws IOException	{
		BufferedOutputStream bos = null;
		ZipInputStream zis = null;
		BufferedInputStream bis = null;
		try {
			zis = new ZipInputStream(new FileInputStream(zipFile));
			bis =new BufferedInputStream(zis);  
			ZipEntry entry = zis.getNextEntry();
			File tmpFile = File.createTempFile(entry.getName(), "");
			bos = new BufferedOutputStream(new FileOutputStream(tmpFile));  
			int i;
			while((i = bis.read())!=-1){  
				bos.write(i);
			}  
			bos.flush();
			return tmpFile;
		} finally	{
			closeStream(bos);
			closeStream(zis);
			closeStream(bis);
		}
	}
	
	/**
	 * 将src复制到target文件中
	 * 如果父路径不存在则自动创建
	 * @param src
	 * @param target
	 * @throws IOException 
	 */
	public static void copyFile(File src, File target) throws IOException	{
		if(!target.getParentFile().exists())	{
			target.getParentFile().mkdirs();
		}
		FileChannel fcIn = null, fcOut = null;
		try {
			FileInputStream fis = new FileInputStream(src);
			FileOutputStream fos = new FileOutputStream(target);
			fcIn = fis.getChannel();
			fcOut = fos.getChannel();
			fcIn.transferTo(0, fcIn.size(), fcOut);
		} finally {
			Util.closeStream(fcIn, fcOut);
		}
	}
	
	
	/**
	 * 剪切粘贴文件. 不使用File.renameTo()
	 * @param src 源文件
	 * @param target 目标文件
	 * @throws IOException
	 */
	public static void cutPasteFile(File src, File target) throws IOException	{
		if(!src.isFile())	
			throw new IOException("不是有效文件:" + src.getAbsolutePath());
		if(!target.isFile())	
			throw new IOException("不是有效文件:" + target.getAbsolutePath());
		
		if(!target.getParentFile().exists())	{
			target.getParentFile().mkdirs();
		}
		FileChannel fcIn = null, fcOut = null;
		try {
			FileInputStream fis = new FileInputStream(src);
			FileOutputStream fos = new FileOutputStream(target);
			fcIn = fis.getChannel();
			fcOut = fos.getChannel();
			fcIn.transferTo(0, fcIn.size(), fcOut);
			src.delete();
		} finally {
			Util.closeStream(fcIn, fcOut);
		}
	}
	
	/**
	 * 过滤字符串中的小数点和数字以外的字符
	 * @param s
	 * @return
	 */
	public static String filterNumAndPoint(String s) {
		StringBuilder sb = new StringBuilder();
		char[] cs = s.toCharArray();
		for (char c : cs) {
			if ((c > 47 && c < 58) || c == 46) {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(Util.randStr(8, false));
	}
}

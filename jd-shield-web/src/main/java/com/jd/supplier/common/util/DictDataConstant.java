package com.jd.supplier.common.util;

public class DictDataConstant {
	public static class Supplier{
		public static final String AREA = "AREA";
		
		public static final String ENTERPRISE = "ENTERPRISE";
		
		public static final String CURRENCY = "CURRENCY";
		
		public static final String QUALIFICATION = "QUALIFICATION";
	}
	
	public static class Task{
		public static final String TASK_STATUS = "task_status";
	}
}

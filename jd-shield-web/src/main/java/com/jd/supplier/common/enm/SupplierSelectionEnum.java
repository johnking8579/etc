package com.jd.supplier.common.enm;

public enum SupplierSelectionEnum {

	PREVIOUS(0,"预选"),FINAL(1,"入围");
	
	private final int type;
	
	private String name;
	
	SupplierSelectionEnum(int type,String name){	
		this.type = type;	
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public int getType() {
		return type;
	}

}

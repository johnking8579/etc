package com.jd.supplier.common;

import com.jd.common.util.PaginatedList;
import com.jd.supplier.common.util.PaginatedArrayList;

public abstract class BaseQuery {

	/**
	 * 分页查询开始行，mysql是从0开始
	 */
	protected int startRow;
	
	/**
	 * 分页每页记录数
	 */
	protected int pageSize = PaginatedArrayList.PAGESIZE_DEFAULT;
	
	/**
	 * 当前页码
	 */
	protected int page;
	
	/**
	 * 分页查询结果存放到此List中
	 */
	protected PaginatedList<?> paginList;
	
	/**
	 * 查询结果总记录数
	 */
	protected Integer totalItem=0;
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		
		this.paginList = new PaginatedArrayList(page, pageSize);
		
		this.page = page;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.paginList = new PaginatedArrayList(page, pageSize);
		this.pageSize = pageSize;
	}

	public PaginatedList getPaginList() {
		if (this.paginList == null){
			this.paginList = new PaginatedArrayList();
		}
		return paginList;
	}

	public void setPaginList(PaginatedList paginList) {
		this.paginList = paginList;
	}

	public Integer getTotalItem() {
		return totalItem;
	}

	/**
	 * 设置总记录数
	 * @param totalItem
	 */
	public void setTotalItem(Integer totalItem) {
		
		getPaginList().setTotalItem(totalItem);
		
		if(paginList.getStartRow() > 0){
			
			setStartRow(paginList.getStartRow()-1);
			
		}
		
		this.totalItem = totalItem;
	}
	
}

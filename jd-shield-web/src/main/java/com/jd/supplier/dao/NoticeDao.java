package com.jd.supplier.dao;

import java.util.List;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.web.vo.NoticeVO;

public interface NoticeDao extends BaseDao<Notice, Long> {

    Notice getByNodeId(Long nodeId);

    int countNotice(NoticeVO noticevo);

    List<Notice> listNotice(NoticeVO noticevo);
}

package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Task;


public interface TaskDao extends BaseDao<Task, Long>{
}
package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.web.vo.NoticeVO;
import com.jd.supplier.web.vo.PreSelectionVO;

public interface TaskSuppSelectionDao extends BaseDao<TaskSuppSelection, Long> {

    int findTaskSuppSelectionSize(Map<String, Object> map);

    List<Supplier> findFinalSupplierList(Map<String, Object> map);

    List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId);
    
    int countTaskSuppSelection(PreSelectionVO preSelectionVO);

    List<TaskSuppSelection> listTaskSuppSelection(PreSelectionVO preSelectionVO);
}
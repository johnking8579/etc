package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskSuppSelectionDao;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.web.vo.NoticeVO;
import com.jd.supplier.web.vo.PreSelectionVO;

@Repository("taskSuppSelectionDao")
public class TaskSuppSelectionDaoImpl extends MyBatisDaoImpl<TaskSuppSelection, Long> implements TaskSuppSelectionDao {

    @Override
    public int findTaskSuppSelectionSize(Map<String, Object> map) {
	String sql = new StringBuffer().append(this.sqlMapNamespace).append(".").append(map.get("sql")).toString();
	return (Integer)this.getSqlSession().selectOne(sql, map);
    }

    @Override
    public List<Supplier> findFinalSupplierList(Map<String, Object> map) {
	return this.getSqlSession().selectList("com.jd.supplier.model.TaskSuppSelection.findFinalSupplierList", map);
    }

    @Override
    public List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId) {
	Map<String, Object> reqMap = new HashMap<String, Object>();
	reqMap.put("requirementId", requirementId);
	return this.getSqlSession().selectList("com.jd.supplier.model.TaskSuppSelection.findFinalSupplierListByReqId",
		reqMap);
    }

    @Override
    public int countTaskSuppSelection(PreSelectionVO preSelectionVO) {
	return (Integer) this.getSqlSession().selectOne("com.jd.supplier.model.TaskSuppSelection.countTaskSuppSelection", preSelectionVO);
    }

    @Override
    public List<TaskSuppSelection> listTaskSuppSelection(PreSelectionVO preSelectionVO) {
	return this.getSqlSession().selectList("com.jd.supplier.model.TaskSuppSelection.listTaskSuppSelection", preSelectionVO);
    }

}

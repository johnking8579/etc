package com.jd.supplier.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.ISupplierDao;
import com.jd.supplier.model.Supplier;
@Component("supplierDao")
public class SupplierDaoImpl extends MyBatisDaoImpl<Supplier, Long> implements ISupplierDao {
	
	@Override
	public List<Supplier> findByUserCode(String account) {
		return getSqlSession().selectList(Supplier.class.getName() + ".findByUserCode", account);
	}
	
}

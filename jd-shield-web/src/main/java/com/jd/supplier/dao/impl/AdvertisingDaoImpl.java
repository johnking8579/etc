package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.core.dao.Page;
import com.jd.supplier.dao.AdvertisingDao;
import com.jd.supplier.model.Advertising;

@Component("advertisingDao")
public class AdvertisingDaoImpl extends MyBatisDaoImpl<Advertising, Long> implements AdvertisingDao	{
	
	public static final String NAMESPACE = Advertising.class.getName();

	@Override
	public Page<Advertising> findByPosition(String position, int offset,
			int pageSize) {
		Map map = new HashMap();
		map.put("position", position);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Advertising> list = getSqlSession().selectList(NAMESPACE + ".findByPosition", map);
		Long count = getSqlSession().selectOne(NAMESPACE + ".findByPosition-count", map);
		
		Page<Advertising> pager = new Page<Advertising>();
		pager.setResult(list);
		pager.setTotalCount(count);
		return pager;
	}
}

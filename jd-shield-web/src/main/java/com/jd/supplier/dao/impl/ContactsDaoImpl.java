package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IContactsDao;
import com.jd.supplier.model.Contacts;

@Component("contactsDao")
public class ContactsDaoImpl extends MyBatisDaoImpl<Contacts, Long>implements IContactsDao {

}

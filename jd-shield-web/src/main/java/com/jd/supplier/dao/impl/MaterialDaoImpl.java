package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IMaterialDao;
import com.jd.supplier.model.Material;
@Component("materialDao")
public class MaterialDaoImpl extends MyBatisDaoImpl<Material, Long> implements
		IMaterialDao {

}

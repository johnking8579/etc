package com.jd.supplier.dao;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Advertising;

public interface AdvertisingDao {

	Page<Advertising> findByPosition(String position, int offset, int pageSize);

}

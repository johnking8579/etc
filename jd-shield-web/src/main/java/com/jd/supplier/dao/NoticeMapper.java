package com.jd.supplier.dao;

import java.util.List;
import com.jd.supplier.model.Notice;
import com.jd.supplier.web.vo.NoticeVO;

public interface NoticeMapper {

	
	int insertSelective(Notice record);
	
	
	int updateByPrimaryKey(Notice record);
	
	
	int deleteByPrimaryKey(Long id);
	
	
	int countNotice(NoticeVO noticevo);
	
	
	List<Notice> listNotice (NoticeVO noticevo);
	
	
	public Notice detailSupplierByid(Long id);
	
	
	public String getMaxSupId();
	
	
	public int findBySupplierFullName(String supplierName);
}

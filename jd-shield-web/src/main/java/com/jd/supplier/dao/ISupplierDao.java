package com.jd.supplier.dao;

import java.util.List;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Supplier;

public interface ISupplierDao extends BaseDao<Supplier, Long>{

	List<Supplier> findByUserCode(String account);
}
package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Material;

public interface IMaterialDao extends BaseDao<Material, Long>{

}

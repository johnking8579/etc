package com.jd.supplier.model;

import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;

public class Enterprise extends IdModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Enterprise(){
		GregorianCalendar gc=new GregorianCalendar();
		enterpriseDate = gc.getTime();
		periodDate = gc.getTime();
	}

	private Long supplierId;
	
	private String code;

	private String name;

	private String organize;
	
	private String supplierType;

	private String officeAddress;

	private String registeredAddress;

	private String factoryAddress;

	private String enterpriseType;

	private String enterpriseLaw;

	private Date enterpriseDate;
	
	private String dateOne;

	private Integer employeeCount;

	private Integer managerCount;

	private Integer tecCount;

	private String measure;

	private String website;

	private Integer funds;

	private String currency;

	private String license;

	private Date periodDate;
	
	private String dateTwo;

	private String managementNum;

	private String taxNum;

	private String orgCode;

	private String qualification;

	private Integer perAcount;

	private String supplierRange;

	private String supplierOwner;

	private String supplierHome;

	private String branch;

	private String content;

	private String honor;

	private String other;

	private String mark;

	private String status;
	
	private float  money;

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getOrganize() {
		return organize;
	}

	public void setOrganize(String organize) {
		this.organize = organize == null ? null : organize.trim();
	}

	public String getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress == null ? null : officeAddress
				.trim();
	}

	public String getRegisteredAddress() {
		return registeredAddress;
	}

	public void setRegisteredAddress(String registeredAddress) {
		this.registeredAddress = registeredAddress == null ? null
				: registeredAddress.trim();
	}

	public String getFactoryAddress() {
		return factoryAddress;
	}

	public void setFactoryAddress(String factoryAddress) {
		this.factoryAddress = factoryAddress == null ? null : factoryAddress
				.trim();
	}

	public String getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(String enterpriseType) {
		this.enterpriseType = enterpriseType == null ? null : enterpriseType
				.trim();
	}

	public String getEnterpriseLaw() {
		return enterpriseLaw;
	}

	public void setEnterpriseLaw(String enterpriseLaw) {
		this.enterpriseLaw = enterpriseLaw == null ? null : enterpriseLaw
				.trim();
	}

	public Date getEnterpriseDate() {
		return (Date)enterpriseDate.clone();
	}

	public Integer getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(Integer employeeCount) {
		this.employeeCount = employeeCount;
	}

	public Integer getManagerCount() {
		return managerCount;
	}

	public void setManagerCount(Integer managerCount) {
		this.managerCount = managerCount;
	}

	public Integer getTecCount() {
		return tecCount;
	}

	public void setTecCount(Integer tecCount) {
		this.tecCount = tecCount;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure == null ? null : measure.trim();
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website == null ? null : website.trim();
	}
	
	public float getMoney() {
		return money;
	}

	public void setMoney(float money) {
		this.funds = (int) (money*100);
	}

	public Integer getFunds() {
		return funds;
	}

	public void setFunds(Integer funds) {
		this.funds = funds;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license == null ? null : license.trim();
	}

	public Date getPeriodDate() {
		return (Date)periodDate.clone();
	}
	

	public void setEnterpriseDate(Date enterpriseDate) {
		if(enterpriseDate != null)
			this.enterpriseDate = new Date(enterpriseDate.getTime());
		else
			this.enterpriseDate = null;
	}

	public void setPeriodDate(Date periodDate) {
		if(periodDate != null)
			this.periodDate = new Date(periodDate.getTime());
		else
			this.periodDate = null;
	}

	public String getManagementNum() {
		return managementNum;
	}

	public void setManagementNum(String managementNum) {
		this.managementNum = managementNum == null ? null : managementNum
				.trim();
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum == null ? null : taxNum.trim();
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode == null ? null : orgCode.trim();
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification == null ? null : qualification
				.trim();
	}

	public Integer getPerAcount() {
		return perAcount;
	}

	public void setPerAcount(Integer perAcount) {
		this.perAcount = perAcount;
	}

	public String getSupplierRange() {
		return supplierRange;
	}

	public void setSupplierRange(String supplierRange) {
		this.supplierRange = supplierRange == null ? null : supplierRange
				.trim();
	}

	public String getSupplierOwner() {
		return supplierOwner;
	}

	public void setSupplierOwner(String supplierOwner) {
		this.supplierOwner = supplierOwner == null ? null : supplierOwner
				.trim();
	}

	public String getSupplierHome() {
		return supplierHome;
	}

	public void setSupplierHome(String supplierHome) {
		this.supplierHome = supplierHome == null ? null : supplierHome.trim();
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch == null ? null : branch.trim();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content == null ? null : content.trim();
	}

	public String getHonor() {
		return honor;
	}

	public void setHonor(String honor) {
		this.honor = honor == null ? null : honor.trim();
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other == null ? null : other.trim();
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark == null ? null : mark.trim();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}

	public String getDateOne() {
		return dateOne;
	}

	public void setDateOne(String dateOne) throws ParseException {
		if(dateOne!=null && !"".equals(dateOne)){
		Date date = java.sql.Date.valueOf(dateOne);
		this.enterpriseDate = date;
		this.dateOne = dateOne;
		}
	}

	public String getDateTwo() {
		return dateTwo;
	}

	public void setDateTwo(String dateTwo ) throws ParseException {
		if(dateTwo!=null && !"".equals(dateTwo)){
		Date date = java.sql.Date.valueOf(dateTwo);
		this.periodDate = date;
		this.dateTwo = dateTwo;
		}
	}
	
}
package com.jd.supplier.model;

import java.util.ArrayList;
import java.util.List;

import com.jd.official.core.model.IdModel;

public class Quality extends IdModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long supplierId;

	private String authentication;
	
	private String other;

	private String status;

	private List<Material> materials = null;

	private List<Equip> equips = new ArrayList<Equip>();
	
	private List<Equip> equipOne = null;
	
	private List<Equip> equipTwo = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication == null ? null : authentication
				.trim();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Equip> getEquips() {
		return equips;
	}

	public void setEquips(List<Equip> equips) {
		this.equips = equips;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Equip> getEquipOne() {
		return equipOne;
	}

	public void setEquipOne(List<Equip> equipOne) {
		this.equipOne = equipOne;
	}

	public List<Equip> getEquipTwo() {
		return equipTwo;
	}

	public void setEquipTwo(List<Equip> equipTwo) {
		this.equipTwo = equipTwo;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
	
}
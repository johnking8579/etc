package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;

public class Requirement extends IdModel {
    private static final long serialVersionUID = 1659168822681619710L;

    public Requirement(){
    	GregorianCalendar gc=new GregorianCalendar();
    	deliveryDate = gc.getTime();
    }
    
    private Long requirementYearId;

    private String projectNum;

    private String projectDate;

    private String projectName;

    private String isYear;

    private String budgetSource;

    private Long budgetAmount;

    private String outsideReason;

    private String sampleTest;

    private String sceneTest;

    private String recommendLevel;

    private String outsideFileName;

    private String outsideFileKey;

    private String yearFileName;

    private String yearFileKey;

    private String testFileName;

    private String testFileKey;

    private String tecFileName;

    private String tecFileKey;

    private String otherFileName;

    private String otherFileKey;

    private String technical;

    private String aptitude;

    private String standard;

    private String address;

    private Date deliveryDate;
    
    private String deliveryDateStr;

    private String payMethod;

    private String serviceRequire;

    private String cooperationDetail;

    private String supplementInfo;

    private String depSuggestion;

    private String applicant;

    private String applicantDep;

    private String applicantDepId;

    private String applicantTel;

    private String demandStatus;

    private String examineStatus;

    private String mark;

    private String status;
    
    private String reqStatus;
    
    private RequirementAduit requirementAduit;
    
    public RequirementAduit getRequirementAduit() {
        return requirementAduit;
    }

    public void setRequirementAduit(RequirementAduit requirementAduit) {
        this.requirementAduit = requirementAduit;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getReqStatus() {
        return reqStatus;
    }

    public void setReqStatus(String reqStatus) {
        this.reqStatus = reqStatus;
    }

    public Long getRequirementYearId() {
	return requirementYearId;
    }

    public void setRequirementYearId(Long requirementYearId) {
	this.requirementYearId = requirementYearId;
    }

    public String getProjectNum() {
	return projectNum;
    }

    public void setProjectNum(String projectNum) {
	this.projectNum = projectNum == null ? null : projectNum.trim();
    }


    public String getProjectDate() {
	return projectDate;
    }

    public void setProjectDate(String projectDate) {
	this.projectDate = projectDate == null ? null : projectDate.trim();
    }

    public String getProjectName() {
	return projectName;
    }

    public void setProjectName(String projectName) {
	this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getIsYear() {
	return isYear;
    }

    public void setIsYear(String isYear) {
	this.isYear = isYear == null ? null : isYear.trim();
    }

    public String getBudgetSource() {
	return budgetSource;
    }

    public void setBudgetSource(String budgetSource) {
	this.budgetSource = budgetSource == null ? null : budgetSource.trim();
    }


    public Long getBudgetAmount() {
        return budgetAmount;
    }

    public void setBudgetAmount(Long budgetAmount) {
        this.budgetAmount = budgetAmount;
    }

    public String getOutsideReason() {
	return outsideReason;
    }

    public void setOutsideReason(String outsideReason) {
	this.outsideReason = outsideReason == null ? null : outsideReason.trim();
    }

    public String getSampleTest() {
	return sampleTest;
    }

    public void setSampleTest(String sampleTest) {
	this.sampleTest = sampleTest == null ? null : sampleTest.trim();
    }

    public String getSceneTest() {
	return sceneTest;
    }

    public void setSceneTest(String sceneTest) {
	this.sceneTest = sceneTest == null ? null : sceneTest.trim();
    }

    public String getRecommendLevel() {
	return recommendLevel;
    }

    public void setRecommendLevel(String recommendLevel) {
	this.recommendLevel = recommendLevel == null ? null : recommendLevel.trim();
    }

    public String getOutsideFileName() {
	return outsideFileName;
    }

    public void setOutsideFileName(String outsideFileName) {
	this.outsideFileName = outsideFileName == null ? null : outsideFileName.trim();
    }

    public String getOutsideFileKey() {
	return outsideFileKey;
    }

    public void setOutsideFileKey(String outsideFileKey) {
	this.outsideFileKey = outsideFileKey == null ? null : outsideFileKey.trim();
    }

    public String getYearFileName() {
	return yearFileName;
    }

    public void setYearFileName(String yearFileName) {
	this.yearFileName = yearFileName == null ? null : yearFileName.trim();
    }

    public String getYearFileKey() {
	return yearFileKey;
    }

    public void setYearFileKey(String yearFileKey) {
	this.yearFileKey = yearFileKey == null ? null : yearFileKey.trim();
    }

    public String getTestFileName() {
	return testFileName;
    }

    public void setTestFileName(String testFileName) {
	this.testFileName = testFileName == null ? null : testFileName.trim();
    }

    public String getTestFileKey() {
	return testFileKey;
    }

    public void setTestFileKey(String testFileKey) {
	this.testFileKey = testFileKey == null ? null : testFileKey.trim();
    }

    public String getTecFileName() {
	return tecFileName;
    }

    public void setTecFileName(String tecFileName) {
	this.tecFileName = tecFileName == null ? null : tecFileName.trim();
    }

    public String getTecFileKey() {
	return tecFileKey;
    }

    public void setTecFileKey(String tecFileKey) {
	this.tecFileKey = tecFileKey == null ? null : tecFileKey.trim();
    }

    public String getOtherFileName() {
	return otherFileName;
    }

    public void setOtherFileName(String otherFileName) {
	this.otherFileName = otherFileName == null ? null : otherFileName.trim();
    }

    public String getOtherFileKey() {
	return otherFileKey;
    }

    public void setOtherFileKey(String otherFileKey) {
	this.otherFileKey = otherFileKey == null ? null : otherFileKey.trim();
    }

    public String getTechnical() {
	return technical;
    }

    public void setTechnical(String technical) {
	this.technical = technical == null ? null : technical.trim();
    }

    public String getAptitude() {
	return aptitude;
    }

    public void setAptitude(String aptitude) {
	this.aptitude = aptitude == null ? null : aptitude.trim();
    }

    public String getStandard() {
	return standard;
    }

    public void setStandard(String standard) {
	this.standard = standard == null ? null : standard.trim();
    }

    public String getAddress() {
	return address;
    }

    public void setAddress(String address) {
	this.address = address == null ? null : address.trim();
    }

    public Date getDeliveryDate() {
	return (Date)deliveryDate.clone();
    }

    public void setDeliveryDate(Date deliveryDate) {
    	if(deliveryDate != null)
			this.deliveryDate = new Date(deliveryDate.getTime());
		else
			this.deliveryDate = null;
    }

    public String getPayMethod() {
	return payMethod;
    }

    public void setPayMethod(String payMethod) {
	this.payMethod = payMethod == null ? null : payMethod.trim();
    }

    public String getServiceRequire() {
	return serviceRequire;
    }

    public void setServiceRequire(String serviceRequire) {
	this.serviceRequire = serviceRequire == null ? null : serviceRequire.trim();
    }

    public String getCooperationDetail() {
	return cooperationDetail;
    }

    public void setCooperationDetail(String cooperationDetail) {
	this.cooperationDetail = cooperationDetail == null ? null : cooperationDetail.trim();
    }

    public String getSupplementInfo() {
	return supplementInfo;
    }

    public void setSupplementInfo(String supplementInfo) {
	this.supplementInfo = supplementInfo == null ? null : supplementInfo.trim();
    }

    public String getDepSuggestion() {
	return depSuggestion;
    }

    public void setDepSuggestion(String depSuggestion) {
	this.depSuggestion = depSuggestion == null ? null : depSuggestion.trim();
    }

    public String getApplicant() {
	return applicant;
    }

    public void setApplicant(String applicant) {
	this.applicant = applicant;
    }

    public String getApplicantTel() {
	return applicantTel;
    }

    public void setApplicantTel(String applicantTel) {
	this.applicantTel = applicantTel == null ? null : applicantTel.trim();
    }

    public String getDemandStatus() {
	return demandStatus;
    }

    public void setDemandStatus(String demandStatus) {
	this.demandStatus = demandStatus == null ? null : demandStatus.trim();
    }

    public String getExamineStatus() {
	return examineStatus;
    }

    public void setExamineStatus(String examineStatus) {
	this.examineStatus = examineStatus == null ? null : examineStatus.trim();
    }

    public String getMark() {
	return mark;
    }

    public void setMark(String mark) {
	this.mark = mark == null ? null : mark.trim();
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }

    public String getApplicantDep() {
	return applicantDep;
    }

    public void setApplicantDep(String applicantDep) {
	this.applicantDep = applicantDep;
    }

    public String getApplicantDepId() {
	return applicantDepId;
    }

    public void setApplicantDepId(String applicantDepId) {
	this.applicantDepId = applicantDepId;
    }
    
    public String getDeliveryDateStr() {
        return deliveryDateStr;
    }

    public void setDeliveryDateStr(String deliveryDateStr) {
        this.deliveryDateStr = deliveryDateStr;
    }

}
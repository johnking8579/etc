package com.jd.supplier.web.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.service.RequirementService;

@Controller
@RequestMapping("/supplier")
public class RequirementController {
    private static final Logger logger = Logger.getLogger(RequirementController.class);

    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private DictDataService dictDataService;


    @RequestMapping(value = "/requirement_viewpage")
    public ModelAndView requirement_edit(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirement_view");
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist = dictDataService.findDictDataList("recommendLevel");
	Requirement requirement = (Requirement) requirementService.get(Long.valueOf(id));
	model.addAttribute("requirement", requirement);
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist);
	logger.debug("requirement===" + requirement);

	return mav;// + userList.size();
    }

   

}

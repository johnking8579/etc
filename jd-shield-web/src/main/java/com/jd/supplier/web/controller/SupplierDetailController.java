package com.jd.supplier.web.controller;

import static com.jd.supplier.web.CookieConstant.USER_ID;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.constants.SupplierConstant;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.common.util.DateUtil;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.service.SupplierService;
import com.jd.supplier.web.CookieUtils;

@Controller
@RequestMapping("/supplier")
public class SupplierDetailController {
	private static final Logger logger = Logger
			.getLogger(SupplierDetailController.class);
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private DictDataService dictDataService;
	@Resource
    private CookieUtils cookieUtil;

	@RequestMapping(value = "/supplier_detail")
	public ModelAndView supplierDetail(@CookieValue(USER_ID) String id,HttpServletRequest request,
			HttpServletResponse response, Model view) throws Exception {
		List<String> rangeList= new ArrayList<String>();//供应商覆盖范围
		List<String> auList= new ArrayList<String>();//认证状况
		String supplierId = cookieUtil.decrypt(id);
		Supplier supplier = supplierService.get(Long.valueOf(supplierId));
		if(supplier!=null){	
			  if(supplier.getEnterprise()!=null){
				  if(supplier.getEnterprise().getSupplierRange()!=null && !"".equals(supplier.getEnterprise().getSupplierRange())){
				  String[] str=supplier.getEnterprise().getSupplierRange().split(",");
				  for(int i=0;i<str.length;i++){
					  rangeList.add(str[i]);
				  	}
				  }
			  }
			  if(supplier.getQuality()!=null){
				  if(supplier.getQuality().getAuthentication()!=null && !"".equals(supplier.getQuality().getAuthentication())){
					  String[] st=supplier.getQuality().getAuthentication().split(",");
					  for(int i=0;i<st.length;i++){
						  auList.add(st[i]);
					  }
				  }
			  }
			  if(supplier.getEnterprise()!=null){
				  if(supplier.getEnterprise().getEnterpriseDate()!=null && !"".equals(supplier.getEnterprise().getEnterpriseDate())){
					  supplier.getEnterprise().setDateOne(DateUtil.DateToStrHM(supplier.getEnterprise().getEnterpriseDate()));
					  supplier.getEnterprise().setDateTwo(DateUtil.DateToStrHM(supplier.getEnterprise().getPeriodDate()));
			  }
			 }
//			  if(supplier.getEnterprise()!=null && !"".equals(supplier.getEnterprise())){
//				  if(supplier.getEnterprise().getEnterpriseDate()!=null && !"".equals(supplier.getEnterprise().getEnterpriseDate())){
//				  SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
//				  supplier.getEnterprise().setDateOne(dd.format(supplier.getEnterprise().getEnterpriseDate()));
//				  }
//				  if(supplier.getEnterprise().getPeriodDate()!=null && !"".equals(supplier.getEnterprise().getPeriodDate())){
//					  SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
//					  supplier.getEnterprise().setDateTwo(dd.format(supplier.getEnterprise().getPeriodDate()));
//					  }
//			  }
		}
		// 币种类型
		List<DictData> currencyList = dictDataService
				.findDictDataList(SupplierConstant.CURRENCY);
		// 供应商组织
		List<DictData> orgList = dictDataService
				.findDictDataList(SupplierConstant.SUPPLIER_ORG);
		// 企业性质
		List<DictData> enterpriseList = dictDataService
				.findDictDataList(SupplierConstant.ENTERPRISE);
		// 供应商覆盖范围
		List<DictData> coverageList = dictDataService
				.findDictDataList(SupplierConstant.SUPPLIER_COVERAGE);
		// 供应商类型
		List<DictData> supplierTypeList = dictDataService
				.findDictDataList(SupplierConstant.SUPPLIER_COVERAGE);
		// 资料完整度
		List<DictData> integrityList = dictDataService
				.findDictDataList(SupplierConstant.SUPPLIER_DATA_INTEGRITY);
		// 默认申报纳税类型
		List<DictData> taxList = dictDataService
				.findDictDataList(SupplierConstant.TAX_TYPE);
		// 认证状况
		List<DictData> authenticList = dictDataService
				.findDictDataList(SupplierConstant.AUTHENTIC_TYPE);
		view.addAttribute("auList", auList);
		view.addAttribute("authenticList", authenticList);
		view.addAttribute("taxList", taxList);
		view.addAttribute("integrityList", integrityList);
		view.addAttribute("supplierTypeList", supplierTypeList);
		view.addAttribute("coverageList", coverageList);
		view.addAttribute("currencyList", currencyList);
		view.addAttribute("orgList", orgList);
		view.addAttribute("enterpriseList", enterpriseList);
		view.addAttribute("rangeList", rangeList);
		ModelAndView mv = new ModelAndView("supplier/supplierDetail");
		view.addAttribute("supplier", supplier);
		return mv;
	}

	/**
	 * 完成供应商资料
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_save",method = RequestMethod.POST)
	@ResponseBody
	public String saveSupplier(Supplier supplier, Model view) {
		try {
			int count = supplierService.insertData(supplier);
			return String.valueOf(count);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("新增供应商信息异常........", e);
			return "0";
		}
	}
}

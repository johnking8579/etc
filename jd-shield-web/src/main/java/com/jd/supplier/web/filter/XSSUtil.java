package com.jd.supplier.web.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

public class XSSUtil
{
  private final static String[] errorXss = { "<", ">" };

  public static boolean isXss(String name) {
    if (name == null)
      return false;
    for (int i = 0; i < errorXss.length; i++)
      if (StringUtils.contains(name, errorXss[i]))
      {
        return true;
      }
    return false;
  }

  public static String htmlSpecialChars(String s) {
    s = s.replaceAll("&", "&amp;");
    s = s.replaceAll("\"", "&quot;");
    s = s.replaceAll("<", "&lt;");
    s = s.replaceAll(">", "&gt;");
    return s;
  }

  public static String htmlChars(String s) {
    s = s.replaceAll("&lt;", "<");
    s = s.replaceAll("&gt;", ">");
    s = s.replaceAll("&quot;", "\"");
    s = s.replaceAll("&amp;", "&");
    return s;
  }

  public static String htmlEncode(String str)
  {
    if (str != null) {
      str = StringUtils.replace(str, "&", "&amp;");
      str = StringUtils.replace(str, "&amp;amp;", "&amp;");
      str = StringUtils.replace(str, "&amp;quot;", "&quot;");
      str = StringUtils.replace(str, "\"", "&quot;");
      str = StringUtils.replace(str, "'", "&acute;");
      str = StringUtils.replace(str, "&amp;lt;", "&lt;");
      str = StringUtils.replace(str, "<", "&lt;");
      str = StringUtils.replace(str, "&amp;gt;", "&gt;");
      str = StringUtils.replace(str, ">", "&gt;");
      str = StringUtils.replace(str, "&amp;nbsp;", "&nbsp;");
    }
    return str;
  }

  public static String regexReplace(String regex_pattern, String replacement, String s)
  {
    Pattern p = Pattern.compile(regex_pattern);
    Matcher m = p.matcher(s);
    return m.replaceAll(replacement);
  }

  public static String escapeComments(String s) {
    Pattern p = Pattern.compile("<!--(.*?)-->", 32);
    Matcher m = p.matcher(s);
    StringBuffer buf = new StringBuffer();
    if (m.find()) {
      String match = m.group(1);
      m.appendReplacement(buf, "<!--" + htmlSpecialChars(match) + "-->");
    }
    m.appendTail(buf);

    return buf.toString();
  }

  public static boolean isBadTag(String name) {
    return (name.equalsIgnoreCase("script")) || (name.equalsIgnoreCase("applet")) || (name.equalsIgnoreCase("object")) || (name.equalsIgnoreCase("iframe")) || (name.equalsIgnoreCase("frame")) || (name.equalsIgnoreCase("layer")) || (name.equalsIgnoreCase("ilayer")) || (name.equalsIgnoreCase("frameset")) || (name.equalsIgnoreCase("link")) || (name.equalsIgnoreCase("meta")) || (name.equalsIgnoreCase("title")) || (name.equalsIgnoreCase("base")) || (name.equalsIgnoreCase("basefont")) || (name.equalsIgnoreCase("bgsound")) || (name.equalsIgnoreCase("head")) || (name.equalsIgnoreCase("body")) || (name.equalsIgnoreCase("embed")) || (name.equalsIgnoreCase("style")) || (name.equalsIgnoreCase("blink"));
  }

  public static boolean isBadAttribute(String name)
  {
    return (StringUtils.startsWithIgnoreCase(name, "on")) || (StringUtils.startsWithIgnoreCase(name, "data")) || (name.equalsIgnoreCase("dynsrc")) || (name.equalsIgnoreCase("id")) || (name.equalsIgnoreCase("name"));
  }

  public static boolean isBadAttributeValue(String name, String value)
  {
    if ((name.equalsIgnoreCase("action")) || (name.equalsIgnoreCase("background")) || (name.equalsIgnoreCase("codebase")) || (name.equalsIgnoreCase("dynsrc")) || (name.equalsIgnoreCase("href")) || (name.equalsIgnoreCase("src")))
    {
      return (StringUtils.startsWithIgnoreCase(value, "javascript:")) || (StringUtils.startsWithIgnoreCase(value, "vbscript:")) || (StringUtils.startsWithIgnoreCase(value, "about:")) || (StringUtils.startsWithIgnoreCase(value, "chrome:")) || (StringUtils.startsWithIgnoreCase(value, "data:")) || (StringUtils.startsWithIgnoreCase(value, "disk:")) || (StringUtils.startsWithIgnoreCase(value, "hcp:")) || (StringUtils.startsWithIgnoreCase(value, "help:")) || (StringUtils.startsWithIgnoreCase(value, "livescript")) || (StringUtils.startsWithIgnoreCase(value, "lynxcgi:")) || (StringUtils.startsWithIgnoreCase(value, "lynxexec:")) || (StringUtils.startsWithIgnoreCase(value, "ms-help:")) || (StringUtils.startsWithIgnoreCase(value, "ms-its:")) || (StringUtils.startsWithIgnoreCase(value, "mhtml:")) || (StringUtils.startsWithIgnoreCase(value, "mocha:")) || (StringUtils.startsWithIgnoreCase(value, "opera:")) || (StringUtils.startsWithIgnoreCase(value, "res:")) || (StringUtils.startsWithIgnoreCase(value, "resource:")) || (StringUtils.startsWithIgnoreCase(value, "shell:")) || (StringUtils.startsWithIgnoreCase(value, "view-source:")) || (StringUtils.startsWithIgnoreCase(value, "vnd.ms.radio:")) || (StringUtils.startsWithIgnoreCase(value, "wysiwyg:"));
    }

    if (name.equalsIgnoreCase("style")) {
      return (StringUtils.containsIgnoreCase(value, "absolute")) || (StringUtils.containsIgnoreCase(value, "behaviour")) || (StringUtils.containsIgnoreCase(value, "content")) || (StringUtils.containsIgnoreCase(value, "expression")) || (StringUtils.containsIgnoreCase(value, "fixed")) || (StringUtils.containsIgnoreCase(value, "include-source")) || (StringUtils.containsIgnoreCase(value, "moz-binding")) || (StringUtils.containsIgnoreCase(value, "javascript"));
    }

    return false;
  }

  public static boolean sqlValidate(String str)
  {
    str = str.toLowerCase();
    String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|*|%|chr|mid|master|truncate|char|declare|sitename|net user|xp_cmdshell|;|or|-|+|,|like'|and|exec|execute|insert|create|drop|table|from|grant|use|group_concat|column_name|information_schema.columns|table_schema|union|where|select|delete|update|order|by|count|*|chr|mid|master|truncate|char|declare|or|;|-|--|+|,|like|//|/|%|#";

    String[] badStrs = badStr.split("\\|");
    for (int i = 0; i < badStrs.length; i++) {
      if (str.indexOf(badStrs[i]) >= 0) {
        return true;
      }
    }
    return false;
  }

  public static String replaceSQLInjection(String str)
  {
    if (str != null) {
      str = StringUtils.replace(str, "'", "''");
    }
    return str;
  }
}

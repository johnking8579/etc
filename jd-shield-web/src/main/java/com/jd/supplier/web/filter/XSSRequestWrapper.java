
package com.jd.supplier.web.filter;



import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author yfwangyunlong
 *
 */
public class XSSRequestWrapper extends HttpServletRequestWrapper
{
  public static Pattern[] patterns = { Pattern.compile("<([^>]*)>", 2), Pattern.compile("<script>(.*?)</script>", 2), Pattern.compile("src[\r\n]*=[\r\n]*\\'(.*?)\\'", 42), Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"", 42), Pattern.compile("</script>", 2), Pattern.compile("<script(.*?)>", 42), Pattern.compile("eval\\((.*?)\\)", 42), Pattern.compile("expression\\((.*?)\\)", 42), Pattern.compile("javascript:", 2), Pattern.compile("vbscript:", 2), Pattern.compile("onload(.*?)=", 42) };

  private static final Log logger = LogFactory.getLog(XSSRequestWrapper.class);

  HttpServletRequest orgRequest = null;

  private boolean checkXSS = false;
  protected String[] excludeParams;
  private Map sanitized;
  private Map orig;

  public XSSRequestWrapper(HttpServletRequest request)
  {
    super(request);
    this.orgRequest = request;
    this.orig = request.getParameterMap();
    this.sanitized = getParameterMap();
    if (logger.isDebugEnabled())
      snzLogger();
  }

  public XSSRequestWrapper(HttpServletRequest request, boolean checkXSS, boolean checkSQLinjection)
  {
    super(request);

    this.checkXSS = checkXSS;
    this.orgRequest = request;
    this.orig = request.getParameterMap();
    this.sanitized = getParameterMap();
    if (logger.isDebugEnabled())
      snzLogger();
  }
  //此构造函数待用
  public XSSRequestWrapper(HttpServletRequest request, boolean checkXSS, boolean checkSQLinjection, String params)
  {
    super(request);
    this.checkXSS = checkXSS;
    this.orgRequest = request;
    this.excludeParams = StringUtils.split(params, ",");
    this.orig = request.getParameterMap();
    this.sanitized = getParameterMap();
    if (logger.isDebugEnabled())
      snzLogger();
  }

  public String getParameter(String name)
  {
    String[] vals = (String[])(String[])getParameterMap().get(name);
    if ((vals != null) && (vals.length > 0)) {
      return vals[0];
    }
    return null;
  }

  public String[] getParameterValues(String name)
  {
    return (String[])(String[])getParameterMap().get(name);
  }

  public String getHeader(String name)
  {
    String value = super.getHeader(XSSUtil.htmlEncode(name));
    if (value != null) {
      value = XSSUtil.htmlEncode(value);
    }
    return value;
  }

  public Map getParameterMap() {
    if (this.sanitized == null)
      this.sanitized = sanitizeParamMap(this.orig);
    return this.sanitized;
  }

  private Map sanitizeParamMap(Map raw)
  {
    Map res = new HashMap();
    if (raw == null) {
      return res;
    }
    Iterator it = raw.keySet().iterator();
    while (it.hasNext()) {
      String key = it.next().toString();
      String[] rawVals = (String[])(String[])raw.get(key);
      if (isExempted(key)) {
        logger.debug("parameber " + key + " not filter html");
        res.put(key, rawVals);
      }
      else {
        logger.debug("parameber " + key + " have to filter html");
        String[] snzVals = new String[rawVals.length];
        for (int i = 0; i < rawVals.length; i++) {
          snzVals[i] = checkForBlackList(rawVals[i]);
        }
        res.put(key, snzVals);
      }
    }
    return res;
  }

  private String checkForBlackList(String param) {
    String str = param;
      //转义
      str = XSSUtil.htmlEncode(str);
      //过滤sql注入存在问题,暂时关闭
      //str = XSSUtil.replaceSQLInjection(str);
    logger.debug("all replace: " + str);
    return str;
  }

  private void snzLogger()
  {
    Iterator it = this.orig.keySet().iterator();
    while (it.hasNext()) {
      String key = it.next().toString();
      String[] rawVals = (String[])(String[])this.orig.get(key);
      String[] snzVals = (String[])(String[])this.sanitized.get(key);
      if ((rawVals != null) && (rawVals.length > 0))
        for (int i = 0; i < rawVals.length; i++)
          if (rawVals[i].equals(snzVals[i])) {
            logger.debug("Sanitization. Param seems safe: " + key + "[" + i + "]=" + snzVals[i]);
          }
          else
            logger.debug("Sanitization. Param modified: " + key + "[" + i + "]=" + snzVals[i]);
    }
  }

  public boolean hasSuspect()
  {
    boolean hasSuspect = false;

    for (Enumeration enu = getParameterNames(); enu.hasMoreElements(); ) {
      String name = (String)enu.nextElement();
      String[] values = super.getParameterValues(name);
      if (!isExempted(name))
      {
        for (int i = 0; (i < values.length) && 
          (!(hasSuspect = isSuspicious(values[i]))); i++);
      }

    }

    return hasSuspect;
  }

  protected boolean isExempted(String name)
  {
    for (int i = 0; i < this.excludeParams.length; i++) {
      if (this.excludeParams[i].equalsIgnoreCase(name))
        return true;
    }
    return false;
  }

  protected boolean isSuspicious(String value) {
    return XSSUtil.isXss(value);
  }

  public HttpServletRequest getOrgRequest()
  {
    return this.orgRequest;
  }

  public static HttpServletRequest getOrgRequest(HttpServletRequest req)
  {
    if ((req instanceof XSSRequestWrapper)) {
      return ((XSSRequestWrapper)req).getOrgRequest();
    }

    return req;
  }
}

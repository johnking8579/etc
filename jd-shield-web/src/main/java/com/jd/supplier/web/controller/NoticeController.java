package com.jd.supplier.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.util.PaginatedList;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.common.util.SupplierConstant;
import com.jd.supplier.model.Notice;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.service.NoticeService;
import com.jd.supplier.service.TaskSuppSelectionService;
import com.jd.supplier.web.vo.NoticeVO;

@Controller
@RequestMapping("/supplier")
public class NoticeController {
    private static final Logger logger = Logger.getLogger(NoticeController.class);

    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private TaskSuppSelectionService taskSuppSelectionService;

    @RequestMapping(value = "/bidNotice_index")
    public ModelAndView bidNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/bidNotice_index");
	// 查询公告
	Map<String, Object> bidNoticeMap = new HashMap<String, Object>();
	bidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	bidNoticeMap.put("type", SupplierConstant.NOTICETYPE_BIDNOTICE);
	List<Notice> bidNoticeList = noticeService.find("findByMap", bidNoticeMap);
	model.addAttribute("bidNoticeList", bidNoticeList);
	return mav;
    }

    @RequestMapping(value = "/selectedNotice_index")
    public ModelAndView selectedNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/selectedNotice_index");
	// 查询公告
	Map<String, Object> bidNoticeMap = new HashMap<String, Object>();
	bidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	bidNoticeMap.put("type", SupplierConstant.NOTICETYPE_BIDNOTICE);
	List<Notice> selectedNoticeList = noticeService.find("findByMap", bidNoticeMap);
	model.addAttribute("selectedNoticeList", selectedNoticeList);
	return mav;
    }

    @RequestMapping(value = "/winBidNotice_index")
    public ModelAndView winbidNotice(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/winBidNotice_index");
	// 查询公告
	Map<String, Object> bidNoticeMap = new HashMap<String, Object>();
	bidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	bidNoticeMap.put("type", SupplierConstant.NOTICETYPE_BIDNOTICE);
	List<Notice> winBidNoticeList = noticeService.find("findByMap", bidNoticeMap);
	model.addAttribute("winBidNoticeList", winBidNoticeList);
	return mav;
    }

    @RequestMapping(value = "/notice_page_old",method = RequestMethod.POST)
    @ResponseBody
    public Object notice_expression_page(HttpServletRequest request, HttpServletResponse response) {
	// 创建pageWrapper
	PageWrapper<Notice> pageWrapper = new PageWrapper<Notice>(request);

	// 添加搜索条件
	String type = request.getParameter("type");
	pageWrapper.addSearch("status", SupplierConstant.AVAILABLESTATUS);
	pageWrapper.addSearch("type", type);
	// 后台取值
	noticeService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());

	// 返回到页面的数据
//	List<Notice> notices = (List<Notice>) pageWrapper.getResult().get("aaData");
	response.setContentType("text/html; charset=utf-8");
	String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());

	return json;
    }

    @RequestMapping(value = "/notice_page")
    public String pageSeachSupplierList(@ModelAttribute(value = "supplierQuery") NoticeVO noticevo, Model view) {
	try {
	    PaginatedList<Notice> list = null;
	    list = noticeService.pageSearchSrSupplier(noticevo);
	    view.addAttribute("list", list);
//	    view.addAttribute("dictList", dictList);
	    
	 // 查询公告
	   Map<String, Object> bidNoticeMap = new HashMap<String, Object>();
	  bidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	  bidNoticeMap.put("type", SupplierConstant.NOTICETYPE_BIDNOTICE);
	  List<Notice> bidNoticeList = noticeService.find("findByMap", bidNoticeMap);
	  view.addAttribute("bidNoticeList", bidNoticeList);
	} catch (Exception e) {
	    logger.error("分页查询供应商信息异常...", e);
	}
	if(noticevo.getType().equals(SupplierConstant.NOTICETYPE_BIDNOTICE)){
	    return "/supplier/bidNotice_index";
	}else if(noticevo.getType().equals(SupplierConstant.NOTICETYPE_SELECTEDNOTICE)){
	    return "/supplier/selectedNotice_index";
	}else if(noticevo.getType().equals(SupplierConstant.NOTICETYPE_WINBIDNOTICE)){
	    return "/supplier/winBidNotice_index";
	}
	return "/supplier/bidNotice_index";
    }
    

    @RequestMapping(value = "/bidNotice_detailPage")
    public ModelAndView bidNoticeDetailPage(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/bidNotice_detail");
	Notice notice = (Notice) noticeService.get(Long.valueOf(id));
	model.addAttribute("notice", notice);
	return mav;// + userList.size();
    }

    @RequestMapping(value = "/selectedNotice_detailPage")
    public ModelAndView selectedNoticeDetailPage(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/selectedNotice_detail");
	Notice notice = (Notice) noticeService.get(Long.valueOf(id));
	// 查询投标单位
	List<Supplier> supplierList = taskSuppSelectionService.findTaskSuppSelectionListByReqId(notice
		.getRequirementId());
	model.addAttribute("supplierList", supplierList);
	model.addAttribute("notice", notice);
	return mav;// + userList.size();
    }

    @RequestMapping(value = "/winBidNotice_detailPage")
    public ModelAndView winBidNoticeDetailPage(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/winBidNotice_detail");
	Notice notice = (Notice) noticeService.get(Long.valueOf(id));
	model.addAttribute("notice", notice);
	return mav;// + userList.size();
    }

    @RequestMapping(value = "/notice_savepage")
    public ModelAndView notice_savepage(Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/notice_save");
	User user = ComUtils.getCurrentLoginUser();
	user = userService.get(user.getId());
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	model.addAttribute("applicantDep", user.getOrganization().getOrganizationName());
	model.addAttribute("applicant", user.getUserCode());
	model.addAttribute("projectDate", df.format(new Date()));
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	return mav;// + userList.size();
    }

}

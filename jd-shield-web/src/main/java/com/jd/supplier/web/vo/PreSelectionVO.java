package com.jd.supplier.web.vo;

import com.jd.supplier.common.BaseQuery;


public class PreSelectionVO extends BaseQuery {

    private String status;
    
    private String supplierId;
    
    private String searchProjectName;

    private String begintime;
    
    private String endtime;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSearchProjectName() {
        return searchProjectName;
    }

    public void setSearchProjectName(String searchProjectName) {
        this.searchProjectName = searchProjectName;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
    
    
}
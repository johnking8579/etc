
package com.jd.supplier.web.filter;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

public class XSSFilter extends AbstractFilter
{
  private List<String> exactMatchExcludedURLs = new ArrayList<String>();//url的过滤
  protected String excludeParams;
  protected String warningPage;
  private boolean checkXSS = false;
  private boolean checkSQLinjection = false;
  protected String encoding="utf-8";

  public void init()
    throws ServletException
  {
	this.encoding = findInitParameter("encoding", "");
    this.excludeParams = findInitParameter("excludeParams", "");
    this.warningPage = findInitParameter("warningPage", "");
    this.checkXSS = findInitParameter("xssCheck", "false").equalsIgnoreCase("true");
    
    this.checkSQLinjection = findInitParameter("checkSQLinjection", "false").equalsIgnoreCase("true");

    logger.debug("checkSQLinjection is  " + this.checkSQLinjection + "  checkXSS is " + this.checkXSS);
    loadConfiguration();
  }

  public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
    throws IOException, ServletException
  {
	request.setCharacterEncoding(encoding);
    String requestURI = request.getRequestURI();
    boolean isFiltered = true;
    //以下代码备用
    if (!this.exactMatchExcludedURLs.isEmpty()) {
      Iterator<String> iter = this.exactMatchExcludedURLs.iterator();
      while (iter.hasNext()) {
        String exactMatchExcludedURL = (String)iter.next();
        if (requestURI.equals(exactMatchExcludedURL)) {
          isFiltered = false;
          break;
        }
      }
    }

    logger.debug("the url " + requestURI + "  isFiltered is " + isFiltered);
//    this.checkXSS
    XSSRequestWrapper securtyRequest = new XSSRequestWrapper(request, this.checkXSS, this.checkSQLinjection, this.excludeParams);

    if ((isFiltered) && (StringUtils.isNotBlank(this.warningPage))) {
      if (securtyRequest.hasSuspect())
        super.forwardPage(request, response, this.warningPage);
      else
        chain.doFilter(request, response);
    }
    else if (isFiltered)
      chain.doFilter(securtyRequest, response);
    else
      chain.doFilter(request, response);
  }

  public void destroy()
  {
    this.excludeParams = null;
    this.warningPage = null;
    this.encoding = null;
  }

  private void loadConfiguration()
  {
    String exactMatchedURLString = findInitParameter("exactMatchExcludedURLs", "");

    if (null != exactMatchedURLString) {
      String[] tmps = exactMatchedURLString.split(",");
      for (int i = 0; i < tmps.length; i++)
        if (StringUtils.isNotBlank(tmps[i]))
          this.exactMatchExcludedURLs.add(StringUtils.trim(tmps[i]));
    }
  }
}
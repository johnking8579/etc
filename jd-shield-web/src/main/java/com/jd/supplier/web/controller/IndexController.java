package com.jd.supplier.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jd.supplier.common.util.SupplierConstant;
import com.jd.supplier.model.Advertising;
import com.jd.supplier.model.Notice;
import com.jd.supplier.service.AdvertisingService;
import com.jd.supplier.service.NoticeService;

@Controller
@RequestMapping(value = "/supplier")
public class IndexController {
    @Autowired
    private NoticeService noticeService;
    
    @Autowired
    private AdvertisingService advertisingService;

    /**
     * 
     * @param model
     * @param loginStatus 拦截器通知supplier/index,该用户是否已登陆, 1未登陆
     * @param account 用户账号
     * @return
     */
    @RequestMapping(value = "/index")
    public ModelAndView index(Model model, Integer loginStatus, String account) {
	ModelAndView mv = new ModelAndView("/supplier/index");
	// 查询公告
	Map<String, Object> bidNoticeMap = new HashMap<String, Object>();
	bidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	bidNoticeMap.put("type", SupplierConstant.NOTICETYPE_BIDNOTICE);
	List<Notice> bidNoticeList = noticeService.find("findByMap", bidNoticeMap);

	Map<String, Object> selectedNoticeMap = new HashMap<String, Object>();
	selectedNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	selectedNoticeMap.put("type", SupplierConstant.NOTICETYPE_SELECTEDNOTICE);
	List<Notice> selectedNoticeList = noticeService.find("findByMap", selectedNoticeMap);

	Map<String, Object> winBidNoticeMap = new HashMap<String, Object>();
	winBidNoticeMap.put("status", SupplierConstant.AVAILABLESTATUS);
	winBidNoticeMap.put("type", SupplierConstant.NOTICETYPE_WINBIDNOTICE);
	List<Notice> winBidNoticeList = noticeService.find("findByMap", winBidNoticeMap);
	
	List<Advertising> advList = advertisingService.findByPosition(Advertising.POSITION_TOP, 0, Integer.MAX_VALUE).getResult();

	model.addAttribute("bidNoticeList", bidNoticeList);
	model.addAttribute("selectedNoticeList", selectedNoticeList);
	model.addAttribute("winBidNoticeList", winBidNoticeList);
	model.addAttribute("loginStatus", loginStatus);
	model.addAttribute("account", account);
	model.addAttribute("advList", advList);
	return mv;
    }
    
    @RequestMapping("/footer/{jsp}")
    public ModelAndView footerLink(@PathVariable String jsp)	{
		return new ModelAndView("supplier/footer/frame")
					.addObject("jsp", jsp);
    }

}

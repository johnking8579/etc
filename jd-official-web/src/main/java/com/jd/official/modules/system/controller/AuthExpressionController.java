package com.jd.official.modules.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.tree.TreeNode;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.AuthExpression;
import com.jd.official.modules.system.model.BussinessAuthExpression;
import com.jd.official.modules.system.model.Level;
import com.jd.official.modules.system.model.Organization;
import com.jd.official.modules.system.model.Position;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.AuthExpressionService;
import com.jd.official.modules.system.service.BussinessAuthExpressionService;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;


/**
 * 
 * @author: wangdongxing@jd.com
 */
@Controller
@RequestMapping("/system")
public class AuthExpressionController {
    private static final Logger logger = Logger.getLogger(AuthExpressionController.class);

    @Autowired
	private UserAuthorizationService userAuthorizationService;
    
    @Autowired
	private UserService userService;
    
    @Autowired
	private OrganizationService organizationService;    
    
    @Autowired
	private PositionService positionService; 
    
    @Autowired
    private AuthExpressionService authExpressionService;
    
    @Autowired
    private BussinessAuthExpressionService bussinessAuthExpressionService;
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private LevelService levelService;
    
    
    
    /**
     * 
    
    * @desc Index页
    
    * @author WANGHUI
    
    * @date 2013年9月9日 上午10:57:29
    
    *
    
    * @param locale
    * @param request
    * @param response
    * @return
    * @throws Exception
     */
    
    @RequestMapping(value = "/authExpression_index")
    public ModelAndView auth_expression(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    
    	ModelAndView mav = new ModelAndView("system/authExpression_index");
		
		return mav;
    }
    
    
    /**
     * 
    
    * @desc Update页、Save页
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:49:45
    
    *
    
    * @param locale
    * @param expressions
    * @return
    * @throws Exception
     */
    @RecordLog(operationType=OperationTypeValue.add_update, entityName="AuthExpression")
    @RequestMapping(value = "/authExpression_save", produces = "application/json")
    @ResponseBody
    public Object save(AuthExpression entity,HttpServletRequest request) throws Exception {
    	Map<String,Object> resultMap=new HashMap<String, Object>();
    	
    	List<AuthExpression> authExpressions = JsonUtils.fromJsonByGoogle(entity.getExpressions(), new TypeToken<List<AuthExpression>>(){}) ;
    	
    	//id不为空
    	AuthExpression one = authExpressions.get(0);
    	
    	if(one!=null && one.getId()==0) {
    		
        	//判断是否重名
        	AuthExpression authExpression=new AuthExpression();
        	authExpression.setAuthExpressionName(one.getAuthExpressionName());
        	authExpression.setOwner(userService.getUserIdByUserName(ComUtils.getLoginNamePin()));
        	
        	List<AuthExpression> list=authExpressionService.find(authExpression);
        	if(null!=list&&list.size()>0){
        		resultMap.put("operator", false);
        		resultMap.put("message","存在重名数据，请修改名称！");
        		return resultMap;
        	}
        	
    	//插入授权表达式，并返回授权表达式的id列表
    		one.setId(null);
    		one.setOwner(userService.getUserIdByUserName(ComUtils.getLoginNamePin()));
    		List<Long> pks = authExpressionService.insert(authExpressions);
    		request.setAttribute("entityId", pks.get(0));
    		resultMap.put("operator", true);
    		resultMap.put("message","操作成功");
    		resultMap.put("pks",pks);
    	}else {
        	//判断是否重名
        	AuthExpression authExpression=new AuthExpression();
        	authExpression.setAuthExpressionName(one.getAuthExpressionName());
        	authExpression.setOwner(userService.getUserIdByUserName(ComUtils.getLoginNamePin()));
        	
        	List<AuthExpression> list=authExpressionService.find(authExpression);
        	if(null!=list&&list.size()>0){
        		if(!list.get(0).getId().equals(one.getId())){
        			resultMap.put("operator", false);
        			resultMap.put("message","存在重名数据，请修改名称！");
        			return resultMap;
        		}
        	}
    		
    		AuthExpression auth = (AuthExpression) authExpressionService.get(one.getId());
    		auth.setAuthExpressionName(one.getAuthExpressionName());
    		auth.setGroupId(one.getGroupId());
    		auth.setLevelAbove(one.getLevelAbove());
    		auth.setLevelDown(one.getLevelDown());
    		auth.setLevelUp(one.getLevelUp());
    		auth.setRoleId(one.getRoleId());
    		auth.setPositionId(one.getPositionId());
    		auth.setOrganizationId(one.getOrganizationId());
    		auth.setUserId(one.getUserId());
    		authExpressionService.update(auth);
    		resultMap.put("operator", true);
    		resultMap.put("message","操作成功");
    		request.setAttribute("id", one.getId());
		}
		return resultMap;
    }
    
    /**
     * 
    
    * @desc Edit页（被引用的不删除）
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:50:41
    
    *
    * @param entity
    * @return
     */
    //删除对应id的权限表达式
    @RecordLog(operationType=OperationTypeValue.delete, entityName="AuthExpression")
    @RequestMapping(value = "/authExpression_delete")
    @ResponseBody
    public String expression_delete(AuthExpression entity){
    	String expressionIds = entity.getIds();
    	 String [] ids =expressionIds.split(",");
    	 List<AuthExpression>  undeleted=authExpressionService.deleteOnlyUnused("deleteByPrimaryKey", ids);

    	 return JsonUtils.toJsonByGoogle(undeleted);
    }
    
    
    /**
     * 
    
    * @desc 权限表达式查询ajax
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:48:34
    
    *
    
    * @param request
    * @return
     */
	@RequestMapping(value="/authExpression_page")
	@ResponseBody
	public Object auth_expression_page(HttpServletRequest request){
		//创建pageWrapper
		PageWrapper<AuthExpression> pageWrapper=new PageWrapper<AuthExpression>(request);
		
		//添加搜索条件
		String auth_expression_name = request.getParameter("authExpressionName");
		boolean meIsAdmin=userService.isAdministrator(ComUtils.getLoginNamePin());
		if(meIsAdmin==false){
			pageWrapper.addSearch("owner",userService.getUserIdByUserName(ComUtils.getLoginNamePin()));
		}
		if(auth_expression_name!=null && ! ("").equals(auth_expression_name))
			pageWrapper.addSearch("auth_expression_name",auth_expression_name );
		
		String selectedVals=request.getParameter("selectedVals");
		if(selectedVals!=null&&!selectedVals.equals("")){
			List<String> ids = JsonUtils.fromJsonByGoogle(selectedVals,new TypeToken<List<String>>(){});
			pageWrapper.addSearch("idList", ids);
		}
		//后台取值
	    authExpressionService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
	    
	    //返回到页面的额外数据
	    pageWrapper.addResult("returnKey","returnValue");
	    //返回到页面的数据
	    List<AuthExpression> authExpressions  = (List<AuthExpression>)pageWrapper.getResult().get("aaData");
	    List<AuthExpression> names = userAuthorizationService.getAuthExpressionNameListByIds(authExpressions);
	    pageWrapper.addResult("names",names);
	    
		String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		
		
        logger.debug(json);
        return json;
	}
    
    
    
    /**
     * 
    
    * @desc 获取所有角色、组、级别的列表
    
    * @author WANGHUI
    
    * @date 2013年9月5日 下午4:47:27
    
    *
    
    * @param request
    * @return
     */
    @RequestMapping(value="/base_info")
	@ResponseBody
	public String base__info(HttpServletRequest request){
    	Map map = new HashMap<String, List>();

    	List<Role> roles = roleService.find("findAllRoles",new String[]{""});
    	List<Role> groups = roleService.find("findAllGroups",new String[]{""});
		List<Level> levels_1 = levelService.find("findLevel1List",new String[]{""});
		//List<Level> levels_2 = levelService.find("findLevel2List",new String[]{""});
		
		map.put("roles", roles);
		map.put("groups", groups);
		map.put("levels_1", levels_1);
		map.put("levels_2", levels_1);
		
    	return JsonUtils.toJsonByGoogle( map);
    }
    
    
   
    
   
  //根据权限表达式得到用户列表vm页面
    @RequestMapping(value = "/users_by_expressions_list")
    public ModelAndView users_by_expressions_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    
    	ModelAndView mav = new ModelAndView("system/authExpression_list");
		
		return mav;
    }
    
    //根据权限表达式得到用户列表Ajax
    @RequestMapping(value = "/users_by_expressions_search_list")
    @ResponseBody
    public String users_by_expressions_search_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam(required = false)  String authId) throws Exception {
    
    	if(authId== null || authId.equals(""))
    		return  "{\"aaData\" :[]}";
    	
		List<AuthExpression> list = authExpressionService.findAuthExpressionListByIds(new String[] { authId });
		List<User> userList = userAuthorizationService	.getUserListByAuthExpressionList(list);

		
		return "{\"aaData\" :"+JsonUtils.toJsonByGoogle(userList)+"}";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-25 上午11:14:13
     *
     * @param locale
     * @param authId
     * @return
     * @throws Exception
     */
    //根据权限表达式得到用户ERPID/NAME字符串Ajax
    @RequestMapping(value="/users_by_expressions_search_str",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map users_by_expressions_search_str(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam(required = false)  String authId) throws Exception {
    	
    	if(authId== null || authId.equals(""))
    		return  null;
    	
		List<AuthExpression> list = authExpressionService.findAuthExpressionListByIds(authId);		
		Map<String, String> map = userAuthorizationService.getUserMapByAuthExpressionList(list);
		return map;	
    }
     
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-25 上午11:14:13
     *
     * @param locale
     * @param authId
     * @return
     * @throws Exception
     */
    //根据business得到用户ERPID/NAME字符串Ajax
    @RequestMapping(value="/users_by_business",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map users_by_business(String businessType, String businessId) throws Exception {

    	if(businessType == null || businessType.equals("") || businessId == null || businessId.equals(""))
    		return  null;
    	
    	//转化任务类型
		String busType="";
		//if("process".equals(businessType))  busType=BussinessAuthExpression.BUSSINESS_TYPE_PROCESS;
		//if("node".equals(businessType))  busType=BussinessAuthExpression.BUSSINESS_TYPE_NODE;
		//if("supervise".equals(businessType)) busType=BussinessAuthExpression.BUSSINESS_TYPE_SUPERVISE;
		
    	List<AuthExpression> list = authExpressionService.findExpressionByBusinessIdAndBusinessType(busType, businessId);
		List<User> userList = userAuthorizationService.getUserListByAuthExpressionList(list);
		
		StringBuffer usersErpidB = new StringBuffer();
		StringBuffer usersNameB = new StringBuffer();
		for (User u : userList){
			usersErpidB.append(u.getUserName()).append(",");
			usersNameB.append(u.getRealName()).append(",");
		}
		
		if (usersErpidB.length()>0 || usersNameB.length()>0 ){
			String usersErpid = usersErpidB.toString();
			String usersName = usersNameB.toString();
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("usersErpid", usersErpid.substring(0, usersErpid.length()-1));
			map.put("usersName", usersName.substring(0, usersName.length()-1));
			
			return map;
		}else{
			return null;
		}
		
    }
    
    
    @RequestMapping(value = "/authExpression_assgin")
    public String assign(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    	
    	return "system/authExpression_assgin";
    }
    
    
    
    
    /**
     * 
    
    * @desc Edit页
    
    * @author WANGHUI
    
    * @date 2013年9月9日 上午11:30:28
    
    *
    
    * @param locale
    * @param expressions
    * @return
    * @throws Exception
     */
    
    @RequestMapping(value = "/authExpression_edit")
    @ResponseBody
    public String expression_edit(
            @RequestParam(value = "locale", required = false) Locale locale,
            @RequestParam(required = false) String expressions) throws Exception {
    	
    	List<AuthExpression> authExpressions = JsonUtils.fromJsonByGoogle(expressions, new TypeToken<List<AuthExpression>>(){}) ;
    	
    	authExpressionService.update(authExpressions);
    	
    	logger.debug("expressions==="+expressions);

    	
    	 
        return "ok:" ;//+ userList.size();
    }
    
    
    
    //根据文档Id列出所有权限表达式
    @RequestMapping(value = "/expression_by_businessid_list")
    @ResponseBody
    public String expressions_list(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam( required = false) String businessId){
    	
    	String doc=businessId;
    	
    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("business_id", doc);
    	List<BussinessAuthExpression> bussinessAuthExpressions = bussinessAuthExpressionService.find("findByBusinessId",map);
    	
    	List<AuthExpression> authExpressions = new ArrayList<AuthExpression>();
    	int length = bussinessAuthExpressions.size();
    	for (int i=0;i<length;i++) {
    		String id = bussinessAuthExpressions.get(i).getAuthExpressionId();
			authExpressions.add((AuthExpression) authExpressionService.get(id));
		}
    	
    	String json = JsonUtils.toJsonByGoogle(authExpressions);
    	
    	
    	return json;
    }
    
    
    
    
  //删除对应id的权限表达式
    @RequestMapping(value = "/resource_update")
    @ResponseBody
    public String resource_update(
    		@RequestParam(value = "locale", required = false) Locale locale,
    		@RequestParam( required = false) String expressionIds){
    	//bussinessAuthExpressionService.deleteBusinessIdKeyAndExpressionId(businessId, expressionId);
    	List<BussinessAuthExpression> bussinessAuthExpressions = JsonUtils.fromJsonByGoogle(expressionIds,new TypeToken<List<BussinessAuthExpression>>(){});
        bussinessAuthExpressionService.updateByResources(bussinessAuthExpressions);
    	return "ok";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-8-30 上午11:02:20
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/expression_findUserIndex")    
	public String findAuthExpressionUserIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/authExpressionUser_list";
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-8-30 下午12:21:27
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/expression_findUserList")
    @ResponseBody
	public Object findAuthExpressionUserBySearch(HttpServletRequest request){
		//创建pageWrapper
		PageWrapper<User> pageWrapper=new PageWrapper<User>(request);
		//添加搜索条件
		String realName=request.getParameter("realName");
		pageWrapper.addSearch("realName",realName);
		//后台取值
		userService.find(pageWrapper.getPageBean(), "findAuthExpressionUserBySearch", pageWrapper.getConditionsMap());
	    //返回到页面的数据
		String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        return json;
    }
    
    /**
     * 
     * @desc 组织机构Tree
     * @author WXJ
     * @date 2013-9-2 下午01:25:54
     *
     * @param locale
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/expression_orgTreeIndex")    
	public String orgTreeIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/organization_tree";
    }
    
    /**
     * 
     * @desc 树数据加载
     * @author WXJ
     * @date 2013-9-2 上午11:24:45
     *
     * @param organization
     * @return 树节点列表
     */
    @RequestMapping(value = "/expression_orgTreeLoad", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> orgTreeLoad(String id){
    	Map mp = new HashMap();
    	mp.put("id", id);
        List<Organization> organizationList = organizationService.findByParentId(mp);
        List<TreeNode> listData= this.getOrgTreeNodes(organizationList);    //转化成Ztree支持的数据格式
        return listData;
    }
    
    /**
     * 
     * @desc 
     * @author WXJ
     * @date 2013-9-2 上午11:30:38
     *
     * @param organizationList
     * @return
     */
    private List<TreeNode> getOrgTreeNodes(List<Organization> organizationList){
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for(Organization organization : organizationList){
            TreeNode treeNode = new TreeNode();
            treeNode.setId(organization.getId().toString());
            if(organization.getParentId()!=null&&!organization.getParentId().equals("")){
            	treeNode.setpId(organization.getParentId().toString());
            }
            treeNode.setName(organization.getOrganizationName());
            treeNode.setIsParent(organization.getIsParent());  
            treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", organization.getId());
            props.put("parentId", organization.getParentId());
            props.put("orgFullname", organization.getOrganizationFullname());
            props.put("orgCode", organization.getOrganizationCode());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
	}
        
    
    
    /**
     * 
     * @desc 职位Tree
     * @author WXJ
     * @date 2013-9-2 下午8:24:45
     *
     * @param locale
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/expression_posTreeIndex")    
	public String posTreeIndex(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response){
		
        return "/system/position_tree";
    }
    
   /**
    * 
    * @desc 树数据加载
    * @author WXJ
    * @date 2013-9-2 下午8:24:45
    *
    * @param organization
    * @return 树节点列表
    */
   @RequestMapping(value = "/expression_posTreeLoad", method = RequestMethod.POST, produces = "application/json")
   @ResponseBody
   public List<TreeNode> posTreeLoad(String  id){
	   Map mp = new HashMap();
	   mp.put("id", id);
	   List<Position> positionList = positionService.findByParentId(mp);
	   List<TreeNode> listData= this.getPosTreeNodes(positionList);    //转化成Ztree支持的数据格式
	   return listData;
   }
   
   /**
    * 
    * @desc 
    * @author WXJ
    * @date 2013-9-2 下午8:24:45
    *
    * @param organizationList
    * @return
    */
   private List<TreeNode> getPosTreeNodes(List<Position> positionList){
       List<TreeNode> treeNodes = new ArrayList<TreeNode>();
       for(Position position : positionList){
           TreeNode treeNode = new TreeNode();
           treeNode.setId(String.valueOf(position.getId()));
           if(position.getParentId()!=null){
        	   treeNode.setpId(position.getParentId().toString());
           }else{
        	   treeNode.setpId("");
           }
           treeNode.setName(position.getPositionName());
           treeNode.setIsParent(position.getIsParent());
           treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
           Map<String, Object> props = new HashMap<String, Object>();
           props.put("id", position.getId());
           props.put("parentId", position.getParentId());
           treeNode.setProps(props);
           treeNodes.add(treeNode);
       }
       return treeNodes;
	}
   @RequestMapping(value="/authExpression_select")
   public ModelAndView selectAuthExpression(String selectedIds,String byIFrame){
	   ModelAndView mav=new ModelAndView("system/authExpression_select");
	   //传递到页面上需要被选中的ID
	   mav.addObject("selectedIds", selectedIds);
	   //传递到页面授权页面是不是iframe中
	   mav.addObject("byIFrame",byIFrame==null?"0":byIFrame);
	   return mav;
   }
   
   
   @RequestMapping(value = "/resources_by_userid")
   @ResponseBody
   public String resources_by_userid(
		   @RequestParam(value = "locale", required = false) Locale locale,
   			@RequestParam( required = false) Long userid){
	   if(userid!=null && !userid.equals("")){
		   List<BussinessAuthExpression> list =  userAuthorizationService.getResourceByUserId(userid, "1");
	   	   return JsonUtils.toJsonByGoogle(list);
	   }
	   return "no argument : userid";
	   
   }
   
   @RequestMapping(value = "/resources_owned_by_userid")
   @ResponseBody
   public String resources_owned_by_userid(
		   @RequestParam(value = "locale", required = false) Locale locale,
   		   @RequestParam( required = false) String userid,
   		   @RequestParam( required = false) String resourceid){
	   
	   if(userid!=null && !userid.equals("") && resourceid!=null && !resourceid.equals("")){
		   boolean is= userAuthorizationService.isResourceOwnedByUser(resourceid, "1", userid);
		  
		  return JsonUtils.toJsonByGoogle(is);
	  
	   }
	   
	   return "no arguments : userid, resourceid";
	   
   }
}

/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.tree.TreeNode;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Organization;
import com.jd.official.modules.system.model.Position;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.AuthExpressionService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;

/**
 * @author liub
 * @Description: 公用的组织结构树封装
 *
 */
@Controller
@RequestMapping(value="/system")
public class AddressController {
	 private static final Logger logger = Logger.getLogger(AddressController.class);
	 @Autowired
	 private OrganizationService organizationService;
	 
	 @Autowired
	 private UserService userService;
	 
	 @Autowired
	 private AuthExpressionService authExpressionService;
	 
	 @Autowired
	 private UserAuthorizationService userAuthorizationService;
	 
	 @Autowired
	 private PositionService positionService; 
	 
	 /** 
      * @Description: 进入到查询分配角色主页面
      * @author yujiahe
     */
	 @RequestMapping(value = "/address_getOrgUser", method = RequestMethod.GET)
	 public ModelAndView getOrgUser(String source,String addressConfig,String processInstanceId){
		 String url ="system/address_orgUser";
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("addressConfig", addressConfig);
		 mav.addObject("currentProcessInstanceId", processInstanceId);
         mav.addObject("source", source);
		 return mav;
		 
	 }
	 
	 @RequestMapping(value = "/user_getByProcessInstanceId", method = RequestMethod.GET)
	 public ModelAndView user_getByProcessInstanceId(String addressConfig){
		 ModelAndView mav = new ModelAndView("system/user_getByProcessInstanceId");
		 return mav;
	 }
	 
	 @RequestMapping(value = "/address_getOrgDept", method = RequestMethod.GET)
	 public ModelAndView getOrgDept(String source,boolean isMulti){
		 ModelAndView mav = new ModelAndView("system/address_orgDept");
		 mav.addObject("isMulti", isMulti);
         mav.addObject("source", source);
		 return mav;
	 }
	 /**
	  * 服务端返回 tree node data json
	  * @param organization
	  * @return
	  */
	 @RequestMapping(value = "/address_getTreeNodeDataJson", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public List<TreeNode> getTreeNodeData(String id){
	    Map mp = new HashMap();
	    mp.put("id", id);
	    List<Organization> organizationList = organizationService.findByParentId(mp);
	    List<TreeNode> listData= this.getTreeNodes(organizationList);    //转化成Ztree支持的数据格式
	    return listData;
	 }
	 
	 @RequestMapping(value = "/address_getOrgUserDataJson")
	 @ResponseBody
	 public Object list(HttpServletRequest request,Long organizationId,String userName,String processInstanceId) throws Exception {
		 
		 if(processInstanceId == null){
			 PageWrapper<User> pageWrapper = new PageWrapper<User>(request);
			 
			 if(organizationId !=null && ! "".equals(organizationId)){
				Organization organization = organizationService.get(organizationId) ; 
				List<Long> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(organization.getId().toString()) ;
				pageWrapper.addSearch("allOrgIds", allOrgIds);
			 }
			 
//			 pageWrapper.addSearch("organizationId",organizationId);
	         if( userName.contains("%")){
	             userName=userName.replace("%","\\%");
	         }
			 pageWrapper.addSearch("userName", userName);
			 if( (organizationId !=null && ! "".equals(organizationId))|| StringUtils.isNotEmpty(userName)){
				 userService.find(pageWrapper.getPageBean(),"findByOrganizationId",pageWrapper.getConditionsMap());
			 }
			 pageWrapper.addResult("returnKey","returnValue");
			 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			 
			 
//			List<User> users  = getAddsignUsersByProcessInstanceId(processInstanceId, (List<User>)pageWrapper.getResult().get("aaData"));
			
			 
		     logger.debug("result:"+json);
		     return json;
		     
		 }
		 return null;
	 }
	 
	 /**
	  * 角色
	  * @param addressConfig
	  * @param processInstanceId
	  * @return
	  */
	 @RequestMapping(value = "/address_getRole", method = RequestMethod.GET)
	 public ModelAndView getRole(String source,String addressConfig,String processInstanceId){
		 String url ="system/address_role"; 
		 
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("addressConfig", addressConfig);
		 mav.addObject("currentProcessInstanceId", processInstanceId);
         mav.addObject("source", source);
		 return mav;
		 
	 }
	 
	 @RequestMapping(value = "/address_getWorkGroup", method = RequestMethod.GET)
	 public ModelAndView getWorkGroup(String source,String addressConfig,String processInstanceId){
		 String url ="system/address_workGroup"; 
		 
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("addressConfig", addressConfig);
		 mav.addObject("currentProcessInstanceId", processInstanceId);
         mav.addObject("source", source);
		 return mav;
		 
	 }
	 
	 /**
	  * 岗位
	  * @param addressConfig
	  * @param processInstanceId
	  * @return
	  */
	 @RequestMapping(value = "/address_getPosition", method = RequestMethod.GET)
	 public ModelAndView getPosition(String source,boolean isMulti){
		 String url ="system/address_position"; 
		 
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("isMulti", isMulti);
         mav.addObject("source", source);
		 return mav;
		 
	 }

        @RequestMapping(value = "/address_getOrgPositionDataJson")
        @ResponseBody
        public Object listPosition(HttpServletRequest request,String  organizationId,String positionName) throws Exception {
            //创建pageWrapper
        PageWrapper<Position> pageWrapper=new PageWrapper<Position>(request);
        if(StringUtils.isNotEmpty(organizationId) && StringUtils.isEmpty(positionName)){
            List<Long> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(organizationId);
            pageWrapper.addSearch("allOrgIds",allOrgIds);
        }
        if(StringUtils.isNotEmpty(positionName)){
            pageWrapper.addSearch("positionName",positionName);
        }
            if (request.getParameter("iSortingCols") != null && request.getParameter("iSortingCols").equals("0")) {
                pageWrapper.addSearch("sortFlag", "true");
            }
        if(StringUtils.isNotEmpty(organizationId) || StringUtils.isNotEmpty(positionName)){
        	positionService.find(pageWrapper.getPageBean(),"findByPage",pageWrapper.getConditionsMap());
        }
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:"+json);
        return json;
    }




	 /**
	  * 权限表达式
	  * @param addressConfig
	  * @param processInstanceId
	  * @return
	  */
	 @RequestMapping(value = "/address_getExpression", method = RequestMethod.GET)
	 public ModelAndView getExpression(String source,String addressConfig,String processInstanceId){
		 String url ="system/address_expression"; 
		 
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("addressConfig", addressConfig);
		 mav.addObject("currentProcessInstanceId", processInstanceId);
         mav.addObject("source", source);
		 return mav;
		 
	 }
	 
	 
	 @RequestMapping(value = "/address_getPositionTreeNodeDataJson", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public List<TreeNode> getPositionTreeNodeData(String id){
	    Map mp = new HashMap();
	    mp.put("id", id);
//	    List<Organization> organizationList = organizationService.findByParentId(mp);
	    List<Position> positionList = positionService.findByParentId(mp);
	    List<TreeNode> listData= this.getPositionTreeNodes(positionList);    //转化成Ztree支持的数据格式
	    return listData;
	 }
	 
	 /**
	  * 获取层级tree node collections
	  * @param organizationList
	  * @return
	  */
	 private List<TreeNode> getTreeNodes(List<Organization> organizationList){
	        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
	        for(Organization organization : organizationList){
	            TreeNode treeNode = new TreeNode();
	            treeNode.setId(organization.getId().toString());
	            treeNode.setpId(organization.getParentId()==null?"":organization.getParentId().toString());
	            treeNode.setName(organization.getOrganizationName());
	            treeNode.setIsParent(organization.getIsParent());  
	            treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
	            Map<String, Object> props = new HashMap<String, Object>();
	            props.put("id", organization.getId());
//	            props.put("parentId", organization.getParentId());
//	            props.put("orgFullname", organization.getOrganizationFullname());
	            props.put("organization", organization);
	            treeNode.setProps(props);
	            treeNodes.add(treeNode);
	        }
	        return treeNodes;
		}
	 
	 private List<TreeNode> getPositionTreeNodes(List<Position> positionList){
	        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
	        for(Position position : positionList){
	            TreeNode treeNode = new TreeNode();
	            treeNode.setId(position.getId().toString());
	            treeNode.setpId(position.getParentId()==null?"":position.getParentId().toString());
	            treeNode.setName(position.getPositionName());
	            treeNode.setIsParent(position.getIsParent());  
	            treeNode.setIconSkin(Resource.class.getSimpleName().toLowerCase());
	            Map<String, Object> props = new HashMap<String, Object>();
	            props.put("id", position.getId());
	            props.put("position", position);
	            treeNode.setProps(props);
	            treeNodes.add(treeNode);
	        }
	        return treeNodes;
		}
    
	 
	 
	 private List<User> filterByUserName(List<User> users,String name){
		 List<User> temp = new ArrayList<User>();
		 if(users != null && users.size() > 0){
			 for (User user : users) {
				if( (user.getUserName() != null && user.getUserName().indexOf(name) >= 0 ) || ( user.getRealName() != null && user.getRealName().indexOf(name) >= 0  )){

					temp.add(user);
				}
			}
			 
		 }
		 
		 return temp;
	 }
}

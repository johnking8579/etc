/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Organization;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.model.UserRole;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserRoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;

/**
 * @author yujiahe
 * @date 2013年8月30日
 * @Description: 给用户分配角色
 * 
 */
@Controller
@RequestMapping(value = "/system")
public class UserRoleController {
	private static final Logger logger = Logger
			.getLogger(RoleController.class);
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private UserAuthorizationService userAuthorizationService ;
	
	/**
	 * @Description: 进入到查询分配角色主页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/user_index", method = RequestMethod.GET)
	public String index() {
		return "system/userRole_index";
	}

	/**
	 * @Description: 进入到批量角色分配页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/userRoles_update", method = RequestMethod.GET)
	public String userRolesUpdate(Model model) {
		User user = new User();
		List<User> userList = userService.find(user);
		model.addAttribute("userList", userList);
		List<Role> roleList = roleService.findRoleList();
		model.addAttribute("roleList", roleList);
		List<Role> sysGroupList = roleService.findGroupList();
		model.addAttribute("sysGroupList", sysGroupList);
		return "system/userRoles_update";
	}

	/**
	 * @Description:用户角色分配分页数据查询
	 * @author yujiahe
	 */
	@RequestMapping(value = "/userRolePage")
	@ResponseBody
	public Object userRolePage(@RequestParam String realName,@RequestParam Long roleId,
			@RequestParam Long orgId, HttpServletRequest request) {
		// 创建pageWrapper
		PageWrapper<UserRole> pageWrapper = new PageWrapper<UserRole>(
				request);
		// 添加搜索条件 用户组 用户角色 用户名，其中用户组用户角色统一由roleId处理
        if( realName.contains("%")){
            realName=realName.replace("%","\\%");
        }
        if( realName.contains("_")){
            realName=realName.replace("_","\\_");
        }
		if (null != realName && !realName.equals("")) {
			pageWrapper.addSearch("realName", realName);
		}else if (null != orgId && !orgId.equals("")) {
			//Organization organization = organizationService.get(orgId) ; 
			List<Long> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(orgId.toString()) ;
			pageWrapper.addSearch("allOrgIds", allOrgIds);
//            pageWrapper.addSearch("orgId", orgId);
        }
		if (null != roleId && !roleId.equals("")) {
			pageWrapper.addSearch("roleId", roleId);
		}
		
		// 后台取值
		userRoleService.find(pageWrapper.getPageBean(), "findByMap",
				pageWrapper.getConditionsMap());
		// 返回到页面的数据
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}

	/**
	 * @Description:进入配置用户角色页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/userRole_update", method = RequestMethod.GET)
	public String userRoleUpdate(@RequestParam Long Id, Model model) {
		// 根据数据字典类别ID查询类别信息
		logger.debug("userRole_update" + Id);
		List<UserRole> userRoleList = userRoleService
				.getUserRoleListByUserId(Id);
		// String json=JsonUtils.toJsonByGoogle(userRoleList);
		model.addAttribute("userRoleList", userRoleList);
		// if(StringUtils.isEmpty(roleType) ){
		// map.put("message", "角色类别不能为空");
		// }
		List<Role> roleList = roleService
				.findUnUseRoleListByUserId(Id);
		model.addAttribute("roleList", roleList);
		model.addAttribute("Id", Id);
		User user = userService.get(Id);
		model.addAttribute("user", user);
		return "system/userRole_update";
	}
    /**
     * @Description: 根据用户ID查询用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
	@RequestMapping(value = "/getUserRoleListByUserId", method = RequestMethod.GET)
	@ResponseBody
	public String getUserRoleListByUserId(@RequestParam Long Id,
			Model model) {
		logger.debug("userRole_update" + Id);
		List<UserRole> userRoleList = userRoleService
				.getUserRoleListByUserId(Id);
		String json = JsonUtils.toJsonByGoogle(userRoleList);
		model.addAttribute("userRoleList", json);

		return json;
	}

	/**
	 * @Description: 保存修改某用户的角色
	 * @param roleIds
	 *            ,userId
	 * @return String
	 * @author yujiahe
	 */
	@RequestMapping(value = "/userRole_updateSave")
	@ResponseBody
	public String saveEditUserRole(@RequestParam String roleIds,
			@RequestParam Long userId) {
		Map<String, Object> map = new HashMap<String, Object>();

		// userRole.setUserId(userId);
		// userRoleService.delete(userRole);

		userRoleService.saveEditUserRole(roleIds, userId);
		map.put("operator", true);
		map.put("message", "添加成功");

		return JSONObject.fromObject(map).toString();
	}

	/**
	 * @Description: 保存批量修改某用户的角色
	 * @param roleId
	 *            ,userIds
	 * @return String
	 * @author yujiahe
	 */
	@RequestMapping(value = "/userRoles_updateSave")
	@ResponseBody
	public String saveEditUserRoles(@RequestParam Long roleId,
			@RequestParam String userIds) {
		Map<String, Object> map = new HashMap<String, Object>();
		userRoleService.saveEditUserRoles(roleId, userIds);
		map.put("operator", true);
		map.put("message", "添加成功");

		return JSONObject.fromObject(map).toString();
	}
    /**
     * @Description: 删除某角色下某用户
     * @param roleId ,userId
     * @return String
     * @author yujiahe
     */
    @RequestMapping(value = "/userRoles_deletebyUR")
    @ResponseBody
    public String userRolesDeletebyUR(@RequestParam Long roleId,
                                       @RequestParam Long userId) {
        Map<String, Object> map = new HashMap<String, Object>();
        userRoleService.deleteUserRoleByUR(roleId, userId);
        map.put("operator", true);
        map.put("message", "添加成功");
        return JSONObject.fromObject(map).toString();
    }
    
    /**
     * 递归获取所选节点下的所有叶子节点
     * @param nodeId
     * @param allLeafNodes
     * @return
     */
    private List<String> getAllLeafNodes(String nodeId,List<Long> allLeafNodes){
    	List<String> result = new ArrayList<String>();
    	Map<String,String> mp = new HashMap<String,String>();
    	mp.put("id", nodeId);
        List<Organization> organizationList = organizationService.findByParentId(mp);
        for(Organization bean:organizationList){
        	if(!bean.getIsParent()){
        		allLeafNodes.add(bean.getId());
        	} else {
        		result = this.getAllLeafNodes(bean.getId().toString(),allLeafNodes);
        	}
        }
        return result;
    }
}

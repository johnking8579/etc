package com.jd.official.modules.system.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.model.UserRole;
import com.jd.official.modules.system.service.UserRoleService;
import com.jd.official.modules.system.service.UserService;

/**
 * Created with IntelliJ IDEA.
 * User: yujiahe
 * Date: 13-9-9
 * Time: 下午1:35
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/system")
public class UserController {
    private static final Logger logger = Logger
            .getLogger(RoleController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    /**
     * @Description: 进入到查询用户详情页面
     * @author yujiahe
     */

    @RequestMapping(value = "/user_view", method = RequestMethod.GET)
    public String userGetById(@RequestParam Long userId,Model model) {
        User user=new User();
        user=userService.get(userId);
        model.addAttribute("user", user);
        List<UserRole> userRoleList = userRoleService
                .getUserRoleListByUserId(userId);
        model.addAttribute("userRoleList", userRoleList);
        return "system/user_view";
    }
    
    /**
     * 根据ID查看详情
     * @param id
     * @return JSON格式
     */
    @RequestMapping(value="/user_detail", produces="text/json")
	@ResponseBody
	public String detail(long id)	{
		User u = userService.get(id);
		return new GsonBuilder().serializeNulls().create().toJson(u);
	}
}

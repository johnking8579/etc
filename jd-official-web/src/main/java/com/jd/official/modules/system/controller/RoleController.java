package com.jd.official.modules.system.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.dao.Page;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.UserRole;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserRoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.alibaba.dubbo.common.utils.StringUtils;

/**
 * @author yujiahe
 * @Description: 系统角色Controller
 */
@Controller
@RequestMapping(value = "/system")
public class RoleController {
    private static final Logger logger = Logger.getLogger(RoleController.class);
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService sysUerService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private UserAuthorizationService userAuthorizationService;


    /**
     * @Description: 进入到查询系统角色页面
     * @author yujiahe
     */

    @RequestMapping(value = "/role_index", method = RequestMethod.GET)
    public String list() {
        return "system/role_index";
    }

    /**
     * @Description: 查询系统角色列表
     * @author yujiahe
     */
    @RequestMapping(value = "/role_page")
    @ResponseBody
    public Object page(HttpServletRequest request, Role role) {

        //创建pageWrapper
        PageWrapper<Role> pageWrapper = new PageWrapper<Role>(request);
        //添加搜索条件
        String roleName = request.getParameter("roleName");
        String roleType = request.getParameter("roleType");
        if( roleName.contains("%")){
            roleName=roleName.replace("%","\\%");
        }
        if( roleName.contains("_")){
            roleName=roleName.replace("_","\\_");
        }
        if (null != roleName && !roleName.equals("")) {
            pageWrapper.addSearch("roleName", roleName);
        }
        if (null != roleType && !roleType.equals("")) {
            pageWrapper.addSearch("roleType", roleType);
        }

        //后台取值
        roleService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        pageWrapper.getPageBean().getResult();
        pageWrapper.addResult("returnKey","returnValue");
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;

    }

    /**
     * @Description:进入新建系统角色页面
     * @author yujiahe
     */
    @RequestMapping(value = "/role_add", method = RequestMethod.GET)
    public String roleAdd() {
        return "system/role_add";
    }

    /**
     * @Description:进入编辑系统角色页面
     * @author yujiahe
     */
    @RequestMapping(value = "/role_update", method = RequestMethod.GET)
    public String showRole(Long sysroleId, Model model) {
        Role role = roleService.showRole(sysroleId);
        model.addAttribute("role", role);
        return "system/role_update";
    }

    /**
     * @param role
     * @return String
     * @Description: 保存新增角色
     * @author yujiahe
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="Role")
    @RequestMapping(value = "/role_addSave")
    @ResponseBody
    public String saveAddRole(Role role,HttpServletRequest request) {
    	Long id = 0l;
        Map<String, Object> map = new HashMap<String, Object>();
        String userid = ComUtils.getLoginName();//拿到当前用户
        role.setCreator(userid);//创建者
        role.setStatus("1");//1状态为可用
        role.setCreateTime(new Date());//新增时间
        role.setModifyTime(new Date());//最近修改时间
        id = roleService.insert(role);
        request.setAttribute("entityId", id);
        map.put("operator", true);
        map.put("message", "添加成功");

        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param role
     * @return String
     * @Description: 保存修改系统角色
     * @author yujiahe
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="Role")
    @RequestMapping(value = "/role_updateSave", method = RequestMethod.POST)
    @ResponseBody
    public String saveEditDictType(Role role) {
        Map<String, Object> map = new HashMap<String, Object>();
        role.setStatus("1");
        String userid = ComUtils.getLoginName(); //拿到当前用户
        role.setModifier(userid);//操作者
        role.setModifyTime(new Date());//最近修改时间
        roleService.update(role);
        map.put("operator", true);
        map.put("message", "添加成功");

        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param role
     * @return String
     * @Description: 删除单个系统角色
     * @author yujiahe
     */
    @RecordLog(operationType=OperationTypeValue.delete, entityName="Role")
    @RequestMapping(value = "/role_delete")
    @ResponseBody
    public String deleteRole(Role role) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userid = ComUtils.getLoginName();//拿到当前用户
        role.setModifier(userid);//操作者
        role.setModifyTime(new Date()); //最近修改时间
        role.setStatus("0");//逻辑删除
        roleService.update(role);
        map.put("operator", true);
        map.put("message", "删除成功");
        return JSONObject.fromObject(map).toString();
    }

    @RequestMapping(value = "/validate_test", method = RequestMethod.GET)
    public String validate() {
        return "validate_test";
    }

    /**
     * @return String
     * @Description: 查询角色类表
     * @author yujiahe
     * @date 2013-8-30下午08:03:40
     * @version V1.0
     */
    @RequestMapping(value = "/role_findroleList")
    @ResponseBody
    public String roleFindRoleList() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Role> roleList = roleService.findRoleList();
        map.put("roleList", roleList);
        map.put("message", "查询成功");
        String json = JsonUtils.toJsonByGoogle(roleList);
        return json;
    }

    @RequestMapping(value = "/privillege_list", method = RequestMethod.GET)
    public ModelAndView privilleges_list(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("system/privillege_list");
        return mav;
    }

    /**
     * @return String
     * @Description: 根据角色名称查询角色列表，返回个数用于判断新增用户组时判断是否重名
     * @author yujiahe
     * @date 2013-8-30下午08:03:40
     * @version V1.0
     */
    @RequestMapping(value = "/role_checkRoleNameUnique")
    @ResponseBody
    public int roleCheckRoleNameUnique(@RequestParam String roleName) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Role> roleList = roleService.findRoleByName(roleName);
        return roleList.size();
    }
    
    /**
     * 根据角色编码查询角色列表，返回个数用于判断新增用户组时判断是否有重编码
     * @param roleCode
     * @return
     */
    @RequestMapping(value = "/role_checkRoleCodeUnique")
    @ResponseBody
    public int roleCheckRoleCodeUnique(@RequestParam String roleCode){
    	List<Role> roleList = roleService.findRoleByCode(roleCode);
    	return roleList.size();
    }

    /**
     * @return String
     * @Description: 根据角色ID查找是否有用户属于该角色，用于删除前的判断
     * @author yujiahe
     * @version V1.0
     */
    @RequestMapping(value = "/userRole_deleteCheck")
    @ResponseBody
    public int userRoleDeleteCheck(@RequestParam Long roleId) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<UserRole> userRoleList = userRoleService.findUserRoleByRoleId(roleId);
        return userRoleList.size();
    }

    /**
     * @Description:进入编辑系统角色页面
     * @author yujiahe
     */

    @RequestMapping(value = "/role_view", method = RequestMethod.GET)
    public String roleView(Long sysroleId, Model model) {
        Role role = roleService.showRole(sysroleId);
        model.addAttribute("role", role);
        return "system/role_view";
    }


}

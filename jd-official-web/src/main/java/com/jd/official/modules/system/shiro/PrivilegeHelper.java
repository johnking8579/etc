package com.jd.official.modules.system.shiro;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * 权限认证 
 * User: zhouhuaqi@jd.com<br/>
 * Date: 2013-9-9<br/>
 * Time: 14:35:21<br/>
 * 
 * @author zhouhuaqi
 */

public class PrivilegeHelper {
//	private static final Logger logger = LoggerFactory
//	.getLogger(PrivilegeHelper.class);
//	
//	@Autowired
//	private SysUserService sysUserService;
//	@Autowired
//	private SysRoleService sysRoleService;
//	@Autowired
//	private SysPrivilegeService sysPrivilegeService;
//
//    /**
//     * 资源权限缓存时间
//     */
//    protected int privilegeCacheTime = 60 * 15;
//
//    /**
//     * 判断用户是否对此资源有权限。注意，比较时不分大不写
//     * 
//     * @param my
//     *            被check的权限
//     * @param privileges
//     *            用户所拥有的权限
//     * @return true 表示拥有，false 表示没有
//     */
//    public static boolean checkPrivilege(String[] my, List<String> privileges) {
//        if (privileges != null && my != null) {
//            for (String string : my) {
//                if (privileges.contains(string)) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
//
//    /**
//     * 判断是否有权限
//     * 
//     * @param userId
//     *            用户Id。
//     * @param privilege
//     *            被check的权限
//     * @return
//     */
//    public boolean hasPrivilege(String userId, String privilege) {
//        String[] strings = privilege.split(",");
//        // 获取用户分配的所有权限
//        List<String> privileges = sysPrivilegeService.findAssignedPrivileges(userId);
//        return checkPrivilege(strings, privileges);
//    }
//
//    public void setPrivilegeCacheTime(int privilegeCacheTime) {
//        this.privilegeCacheTime = privilegeCacheTime;
//    }
}
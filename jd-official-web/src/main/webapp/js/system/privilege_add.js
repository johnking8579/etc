function save_insert(){
	var obj = document.getElementById("operationName");
	var data = {
		url : document.getElementById('url').value,
		operationCode : document.getElementById('operationCode').value,
		operationId : document.getElementById("operationId").value,
		resourceId : resourceId,
		privilegeName : document.getElementById("privilegeName").value,
		privilegeCode : document.getElementById("privilegeCode").value
	};
	saveAjax(data);
}
function save_update(){
	var obj = document.getElementById("operationName");
	var data = {
		url : document.getElementById('url').value,
		operationCode : document.getElementById('operationCode').value,
		operationId : document.getElementById("operationId").value,
		resourceId : resourceId,
		privilegeName : document.getElementById("privilegeName").value,
		privilegeCode : document.getElementById("privilegeCode").value,
		id : document.getElementById("id").value
	};
	saveAjax(data);
}

function selectChange(obj){
	var index = obj.selectedIndex;
	var text = obj.options[index].text;
	var value = obj.options[index].value;
	if(value == '')	return;	//选择"请选择"时, 返回
	var operationCode  = obj.item(index).getAttribute('operationCode'); 
	document.getElementById("operationId").value = value;
	document.getElementById("operationCode").value = operationCode;
	
	document.getElementById("privilegeCode").value = resourceCode + "_" + operationCode;
	document.getElementById("privilegeName").value = resourceName + "_" + text;
}

function saveAjax(data){
	$.ajax({
        type : "POST",
        cache : false,
        url : baseUrl + "/system/privilege_save",
        data : data,
        success:function (msg) {
        	privTable.fnClearTable();
        	privTable.fnDestroy();
    		privTable = $("#privTable").dataTable(dataTablesOption);
        },
        error : function(msg){
        	
        }
    });
}

$(document).ready(function(){//页面初始化设置

    bindValidate($("#addPrivilege"));// 绑定验证
    bindValidate($("#updatePrivilege"));// 绑定验证
});
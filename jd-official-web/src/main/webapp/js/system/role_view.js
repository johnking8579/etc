var table;
$(document).ready(
	function() {

		 var columns = [
                        {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false}, 
	    	            {"mData": "realName", "sTitle": "用户姓名", "bSortable": true ,"sWidth":200},
	    	            {"mData": "organizationFullName", "sTitle": "部门", "bSortable": true ,"sDefaultContent": "","sWidth":200},
	    	            {
	    	                    "mData": "id",
	    	                    "sTitle": "操作",
	    	                    "bSortable": false,
	    	                	"mRender" : function(data, type, full) {
	    							var delHtml = "<a href='#' class='btn' title='删除' onclick='javascript:delInfo(\""
	    									+ data
	    									+ "\")'><img src='"
	    									+ springUrl
	    									+ "/static/admin/skin/img/del.png' alt='删除'/></a>";
	    							return delHtml;
	    						}
	    				}
	    	                
	    	        ];
		 
		 

        var tableBtns= [];

        table = $('#userdetail').dataTable( {
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/system/userRolePage",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
             
                var roleId = $("input#roleId").val();
                aoData.push({ "name": "realName", "value": "" },{ "name": "orgId", "value": "" },{ "name": "roleId", "value": roleId });
                jQuery.ajax({
                    type: "POST",
					async:true,
                    url: sSource,
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                        fnCallback(resp);
                    }
                });
            },
            "fnDrawCallback": function( oSettings ){
            	var that = this;                        
				this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
					that.fnUpdate( i+1, this.parentNode, 0, false, false );                
				} ); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );

});
function delInfo(userId) { // 删除某角色下面的用户
	var roleId = $("#roleId").val();
	var a = {
		"roleId" : roleId,
		"userId" : userId
	};
	Dialog.del(springUrl +'/system/userRoles_deletebyUR', a, function(result) {
		table.fnDraw();
	});
}
function cancel() {
	Dialog.hide();
}

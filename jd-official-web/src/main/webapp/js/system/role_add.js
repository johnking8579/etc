$(document).ready(function(){//页面初始化设置
	$('#newRoleCode').blur(function() {
		validateRoleCode();
	});
	
	$('#newRoleCode').focus(function() {
		$("#isRepeatRoleCode").html("");
	});
	
    bindValidate($("#addRole"));// 绑定验证
});

function validateRoleCode(){
	var roleCode=$("input#newRoleCode").val();
	if(roleCode!=''){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				roleCode:roleCode
	        },
	        url:springUrl + "/system/role_checkRoleCodeUnique",
	        success:function (data) {
	        	if(data>0){
	        		var str = "此编码已存在!";
	        		$("#isRepeatRoleCode").html(str).css("color","red");
	        	}
	        }
		});
	}
}

/**
 * Cancel按钮事件
 */
function cancel(){
}
function submit(model){//新增动作
    var newRoleType=$('#newRoleType').val();
    var newRoleName=$('#newRoleName').val();
    var newRoleCode=$('#newRoleCode').val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType: 'json',
        async:false,
        data:{
            roleType:newRoleType,
            roleName:newRoleName,
            roleCode:newRoleCode
        },
        url:springUrl+"/system/role_addSave.json",
        success:function (data) {
        	Dialog.alert("提示信息","添加成功！");
            Dialog.hideModal(model);
        	table.fnDraw();
        }
    });

}

function addRoles(model){ //新增保存按钮触发动作
    var roleName=$('#newRoleName').val();
    $.ajax({ //后台查询是否已有该名称角色
        type: "POST",
        cache:"false",
        url: springUrl+"/system/role_checkRoleNameUnique",
        data: "roleName="+roleName,
        success: function(data){
            if(data>0){//如果系统存在该名称的用户拒绝提交，前台显示
                Dialog.alert("系统提示","该角色已存在，请重置","")
            } else{ //如果不存在则提交
                submit(model);
            }
        }
    });
}




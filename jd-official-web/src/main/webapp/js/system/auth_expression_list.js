$(function(){


	$(".details").live('click',function(){
		bootbox.alert("Hello world!", function() {
			Example.show('Hi <b><a href="#" class="btn">查看详细</a></b>');       
			});
	});
	
	
	var options = {
	        "bProcessing": true,
	        "bServerSide": true,
	        "sDom": '<"toolbar">tr', //定制表格周边的工具栏frtip
	        "aaSorting": [[ 0, "asc" ]], //设置第一个元素升序排列
	        "sAjaxSource": 'users_by_expressions_search_list',
	        //绑定列的值
	        "aoColumns": [
	                      { "mDataProp": "id" },
	                      { "mDataProp": "userName" },
	                      { "mDataProp": "organizationId" },
	                      { "mDataProp": "positionCode" },
	                      { "mDataProp": "levelCode" },              
	                      { "mDataProp": "id" }
	                  ],
	                  
					//自定义列的显示
					"aoColumnDefs" : [ {
							"mRender" : function(data, type, row) {
								//return data + '+ddd';
								var k = '<a href="#" class="btn details">查看详细</a>';
								return k;
							},
							"aTargets" : [5 ]
						}
						// ,{ "bVisible": false, "aTargets": [ 3 ] },
						// { "sClass": "center", "aTargets": [ 4 ] }
					],
					
			//自定义AJAX请求
					fnServerData:function ( sSource, aoData, fnCallback ) {
						var authId=$("input#expression").val();
						//alert(authId);
						//if(authId==null || auth=='' ) return;
						aoData.push( { "name": "authId", "value":authId } );
						jQuery.ajax( {
				                    type: "POST", 
				                    url:sSource, 
				                    dataType: "json",
				                    data: aoData, 
				                    success: function(resp) {
				                            fnCallback(resp);
				                    }
					    });
					}
	    };
	
	var dataTables = $('#example').dataTable(options);
	$("#search").click(
			function(){dataTables.fnDraw();}
	);
});
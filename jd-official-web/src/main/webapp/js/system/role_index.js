var table;
var selectRoleId;

// 页面初始化加载分页
$(document).ready(function () {
    	    	
    	        var columns = [
                    {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false}, 
    	            {"mData": "roleName", "sTitle": "名称", "bSortable": true ,"sWidth":300},
    	            {"mData": "roleType", "sTitle": "类别", "bSortable": true ,"sWidth":240,
    	                    "mRender": function (data) {
    	                        if (data == 0) {
    	                            return '角色';
    	                        } else {
    	                            return '用户组';
    	                        }
    	                    }},
    	             { "mData": "modifyTime", "sTitle": "最后更新", "bSortable": true,"sWidth":220,"fnRender": function (obj) {
    	                 return new Date(obj.aData.modifyTime).format("yyyy-MM-dd hh:mm:ss");
    	             }},
    	             {
    	                    "mData": "id",
    	                    "sTitle": "操作",
    	                    "bSortable": false,
    	                    "fnRender": function (obj) {
    	                        var a = '{"id":"' + obj.aData.id + '"}';
    	                        var viewHtml = '<a href="#" class="btn" title="查看详情" onclick="view(\'' + obj.aData.id + '\')"><img src="'
    	                            + springUrl
    	                            + '/static/admin/skin/img/view_obj.gif" alt="查看详情"/></a>&nbsp;&nbsp;';
    	                        var editHtml = '<a href="#" class="btn" title="修改" onclick="editRole(\'' + obj.aData.id + '\',\'' + obj.aData.roleType + '\')"><img src="'
    	                            + springUrl
    	                            + '/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;';
    	                        var delHtml = "<a href='#' class='btn' title='删除' onclick='javascript:del(\""
    	                            + obj.aData.id + "\",\"" + obj.aData.roleType
    	                            + "\")'><img src='"
    	                            + springUrl
    	                            + "/static/admin/skin/img/del.png' alt='删除'/></a>&nbsp;&nbsp;";
    	                        var acHtml = '<a href="#" class="btn" title="功能授权" onclick="ac(\'' + obj.aData.id + '\')"><img src="'
    	                            + springUrl
    	                            + '/static/admin/skin/img/shouquan.png" alt="功能授权"/></a>&nbsp;&nbsp;';
    	                        var userHtml = '<a href="#" class="btn" title="分配用户" onclick="remoteOrgUserInfo(\''
    	                            + obj.aData.id
    	                            + '\');"><img src="'
    	                            + springUrl
    	                            + '/static/admin/skin/img/enjoy.png" alt="分配用户"/></a>&nbsp;&nbsp;';
                                if(obj.aData.roleType==0)
    	                            return viewHtml+editHtml + delHtml + acHtml + userHtml;
                                else
                                    return viewHtml+editHtml + delHtml + userHtml;

    	                    }
    	                }
    	        ];

    	        var tableBtns= [
    	            {
    	                "sExtends": "text",
    	                "sButtonText": "新增",
    	                "sButtonClass": "btn btn-success",
    	                "sToolTip": "",
    	                "fnClick": function (nButton, oConfig, oFlash) {
    	                    addRole();
    	                }
    	            }
    	        ];

    	        table = $('#sysoperation').dataTable( {
    	            "bProcessing": false,
    	            "bServerSide":true,
    	            "sPaginationType": "full_numbers",
    	            "sAjaxSource":springUrl+"/system/role_page",
    	            "sServerMethod": "POST",
    	            "bAutoWidth": false,
    	            "bStateSave": false,
    	            "sScrollY":"100%",
    	            "bScrollCollapse": true,
    	            "bPaginate":true,
    	            "oLanguage": {
    	                "sLengthMenu": "每页显示 _MENU_ 条记录",
    	                "sZeroRecords": "抱歉， 没有找到",
    	                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
    	                "sInfoEmpty": "没有数据",
    	                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
    	                "oPaginate": {
    	                    "sFirst": "首页",
    	                    "sPrevious": "前页",
    	                    "sNext": "后页",
    	                    "sLast": "尾页"}
    	            },
    	            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
    	            "sPaginationType": "bootstrap",
    	            "bJQueryUI": true,
    	            "bFilter":false,
    	            "fnServerData":function (sSource, aoData, fnCallback) {
    	                var roleName = $("input#roleName").val();
    	                var roleType = $("select#roleType").val();
    	                aoData.push({ "name": "roleName", "value": roleName },{ "name": "roleType", "value": roleType });
    	               
    	                jQuery.ajax({
    	                    type: "POST",
    						async:true,
    	                    url: sSource,
    	                    dataType: "json",
    	                    data: aoData,
    	                    success: function (resp) {
    	                        fnCallback(resp);
    	                    }
    	                });
    	            },
    	            "fnDrawCallback": function( oSettings ){
    	            	var that = this;                        
    					               
    					this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
    						that.fnUpdate( i+1, this.parentNode, 0, false, false );                
    					} );            
    					
    	            },
    	            "aaSorting":[],//默认排序
    	            "aoColumns":columns,
    	            "oTableTools":{
    	                "aButtons":tableBtns
    	            }
    	        } );
    	        
    	        $('#search').click(function () {
    	        	table.fnDraw();
    	        });
    	    });
  

/**
 * 新增角色
 * @param data
 */
function addRole() {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function (model) {
            	
            	var isRepeatRoleCode=$('#isRepeatRoleCode').html();
                if(!validate($("#addRole"))){
                    return  false;
                }else if(isRepeatRoleCode!=''){
                	return  false;
                }else{
                	addRoles(model);
                }
                return false;
            }
        }
    ];
    Dialog.openRemote('role_add','新增角色', springUrl+'/system/role_add', 600, 400, buttons);
}

/**
 * 更新角色
 * @param roleId,roleType
 */
function editRole(roleId, roleType) {
   
    if (roleType == 0) {//系统角色不能修改
        Dialog.alert("提示", "该角色属于系统角色，不能修改");
    } else {
    var buttons = [
                   {
                       "label": "取消",
                       "class": "btn-success",
                       "callback": function () {
                       }
                   },
                   {
                       "label": "保存",
                       "class": "btn-success",
                       "callback": function (modal) {
                    	   var isRepeatRoleCode=$('#isRepeatRoleCode').html();
                           if(!validate($("#editRole"))){
                               return false;
                           }else if(isRepeatRoleCode!=''){
                        	   return false;
                           }else{
                               modify(modal);
                           }
                           return false;
                       }
                   }
               ];
               Dialog.openRemote('role_update','编辑角色', springUrl+'/system/role_update?sysroleId=' + roleId, 550, 400, buttons);
               
    }
}

/**
 * 功能授权
 * @param data
 */
function ac(data) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "授权",
            "class": "btn-success",
            "callback": function (model) {
                saveAc(model);
                return false;
            }
          
        }
    ];
    Dialog.openRemote('rolePrivilege_index','功能授权', springUrl + '/system/rolePrivilege_index?roleId=' + data, 700, 600, buttons);
}

/**
 * 删除角色,传入角色ID
 */
function del(roleId, roleType) {
    if (roleType == 0) { //系统角色不能删除
        Dialog.alert("提示", "该角色属于系统角色，不能删除");
    } else {
        var a = {
            "id": roleId
        };
        $.ajax({
            type: "GET",
            cache: "false",
            url: springUrl +"/system/userRole_deleteCheck",
            dataType: "json",
            data: "roleId=" + roleId,
            success: function (data) { // 查询该角色在用户角色配置表中是否已有记录，返回为记录条数
                if (data > 0) { // 已有记录则不能删除
                    Dialog.alert("提示", "该角色下有用户，不能删除");
                } else {
                    // 无记录继续删除
                    Dialog.del(springUrl +'/system/role_delete', a, function (result) {
                        Dialog.alert("系统提示","删除成功");
                        refreshUserTable();
                    });
                }
            }
        });
    }
}
/**
 * 角色详情页面
 * @param roleId
 */
function view(roleId) {
    Dialog.openRemote('role_view','角色详情', springUrl +'/system/role_view?sysroleId=' + roleId, 700, 400);
}

/**
 * 刷新用户信息列表
 */
function refreshUserTable() {
	table.fnDraw();
}



/**
 * 分配用户
 * @param roleId
 */
function remoteOrgUserInfo(roleId) {
    selectRoleId = roleId;
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function (model) {
               callBack(model,selectRoleId);
               return false;
            }
            
        }
    ];
    Dialog.openRemote('address_getOrgUser','分配用户', springUrl + '/system/address_getOrgUser?addressConfig=addressConfig&roleId=' + roleId, 870, 550, buttons);
}

function setUserInfo(userArray,selectRoleId){
	var selectRecords = "";
	for(var i=0;i<userArray.length;i++) {
        selectRecords+=userArray[i].id+",";
        
    }
	$.ajax({
		type: "GET",
		cache: "false",
		url: springUrl+"/system/userRoles_updateSave",
		dataType: "json",
		data: "roleId=" + selectRoleId + "&userIds=" + selectRecords,
		success: function (json) {
			$('#progressBar').hide();
			$('#background').hide();
			if (json != null) {
				Dialog.alert("提示信息","角色用户分配成功!！");
				Dialog.hideModal(model);
				table.fnDraw();
			}
		}
	}); 

}

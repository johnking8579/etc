/**
 * 资源-修改页面JS
 * User: xulin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
/**
 * 修改资源-确定按钮响应函数
 */
function updateResource(){
	var isRepeatResourceCode=$('#isRepeatResourceCode').html();
    if(isRepeatResourceCode!=''){
  	  return false;
    }
    var isRepeatResourceName=$('#isRepeatResourceName').html();
    if(isRepeatResourceName!=''){
  	  return false;
    }
    
	if(validate($('#inputForm'))){
		var params = $("#inputForm").serialize();
        jQuery.ajax({
            type:"POST",
            cache:false,
            data:params,
			dataType: "json",
            url:springUrl + "/system/resource_updateSave",
            success:function(data) {
            	Dialog.alert("确定","资源更新成功!");
                if($("#parentId").val() == ""){
                    refreshNode(null);
                }
                else{
                    refreshNode($("#parentId").val());
                }
            	var id = $("#id").val();
            	$("#box-right").load(springUrl+"/system/resource_view?id="+id);
            },
			error : function(data) {
				 Dialog.alert("失败","程序异常结束!");
			}
        });
	}
};

/**
 * 修改资源-取消按钮响应函数
 */
function cancelResource(){
    $("#box-right").hide();
    refreshNode($("#parentId").val());
};

function selectChange(obj){
	var index = obj.selectedIndex;
	var value = obj.options[index].value;
	$("#displayStatus").attr("value",value);
}

/**
 * 初始化操作
 */
$(function() {
	
	/*资源编码重复性判断*/
	$('#resourceCode').blur(function() {
		validateResourceCode();
	});
	
	$('#resourceCode').focus(function() {
		$("#isRepeatResourceCode").html("");
	});
	
	/*资源名称重复性判断*/
	$('#resourceName').blur(function() {
		validateResourceName();
	});
	$('#resourceName').focus(function() {
		$("#isRepeatResourceName").html("");
	});
	
    //绑定保存按钮事件
    $('#updateResource').click(function() {
    	updateResource();
    });
    //绑定取消按钮事件
    $('#cancelResource').click(function() {
        cancelResource();
    });

    $('#displayStatusSelect').attr('value', displayStatus);
    
    //验证
	bindValidate($('#inputForm'));
});

function validateResourceCode(){
	var resourceCode=$("input#resourceCode").val();
	var resourceCodeOld=$("input#resourceCodeOld").val();
	if(resourceCode!='' && resourceCode != resourceCodeOld){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				resourceCode:resourceCode
	        },
	        url:springUrl + "/system/resource_isExistResourceCodePage",
	        success:function (data) {
	        	if(data){
	        		var str = "此资源编码已存在!";
	        		$("#isRepeatResourceCode").html(str).css("color","red");
	        	}
	        }
		});
	}
}

function validateResourceName(){
	var parentId=$("#parentId").val();
	var resourceName=$("input#resourceName").val();
	var resourceNameOld=$("input#resourceNameOld").val();
	if(resourceName!='' && resourceName != resourceNameOld){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				resourceName:resourceName,
				parentId:parentId
	        },
	        url:springUrl + "/system/resource_isExistResourceNamePage",
	        success:function (data) {
	        	if(data){
	        		var str = "此资源名称已存在!";
	        		$("#isRepeatResourceName").html(str).css("color","red");
	        	}
	        }
		});
	}
}

var table;
var tableData;
$(function(){ 
	//选择部门
	$('#organization_name').click(function() {
		selectOrgnaztion();
    });
	//选择职位
	$('#postion_name').click(function() {
		popPosition();
    });
	//选择员工 
	$('#user_name').click(function() {
		remoteOrgUserInfo();
    });
	
//	top.autoHeight($(document).height()+100);

//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='authExpression' value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
		        {"mData": "authExpressionName", "sTitle": "名称" , "bSortable": true},
		        {"mData": "mode","bSortable":false,"sTitle":"表达式","fnRender":function(obj){
					return describeExpressMap(obj.aData);
				}},
		        {"mData": "id", "sTitle": "操作", "bSortable": false,"mRender": function (data, type, full) {
		        	var id = data;
					return "<a href='#' onclick='showUsers(\""+id+"\");' class='btn' title='显示用户' ><img src='"+springUrl+"/static/admin/skin/img/search.png' alt='显示用户'></a>" +
							"&nbsp;&nbsp;<a href='#' class='btn' title='修改' onclick='getExpression4Edit(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'></a>";
		        }}
        
        
	        ];

var  tableBtns  =[
  				{
				    "sExtends":    "text",
				    "sButtonText": "新增",
					"sToolTip": "",
					"fnClick": function ( nButton, oConfig, oFlash ) {
					//top.autoHeight($(document).height()+$("#addPane").outerHeight());
						$("#addPane").show("fast");
						clearAddPane();
				    }
				},		
					{
							    "sExtends":    "text",
							    "sButtonText": "删除",
								"sToolTip": "",
								"fnClick": function ( nButton, oConfig, oFlash ) {
									var ids="";
									$('input[name=authExpression]').each(function() {
										if ($(this).attr('checked')=="checked") {
												ids+=$(this).val()+",";
										}
										});
									   /* alert(ids);
									var ids=table.getSelectedRowsIDs();*/
									if(ids.length==0){
										Dialog.alert("消息提示","没有选中项","确定");
									}else{
										//delAuthExpressionRecord(JSON.stringify(ids));
										delAuthExpressionRecord(ids);
									}
							    }
					}
				];


 table = $('#hello').dataTable( {
	 		"sDefaultContent":"expression",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/system/authExpression_page",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
                
				var authExpressionName=$("input#authExpressionName").val();
				if(authExpressionName==null) authExpressionName="";
				authExpressionName=authExpressionName.replace("_","\\_");
				aoData.push( 
						{ "name": "authExpressionName", "value":authExpressionName }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );



//新的加载方式  end//
/*
var	options={
			pageUrl:springUrl+"/auth/authExpression_page",
			useCheckbox:true,
			defaultSort:[],
			sendData:function ( sSource, aoData, fnCallback ) {
				var authExpressionName=$("input#authExpressionName").val();
				if(authExpressionName==null) authExpressionName="";
				authExpressionName=authExpressionName.replace("_","\\_");
				aoData.push( 
						{ "name": "authExpressionName", "value":authExpressionName }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                            fnCallback(resp);
		                    }
			    });
			},
			columns: [
	            { "mDataProp": "authExpressionName","sTitle":"名称","sClass": "my_class","sWidth":200},
				{"bSortable":false,"sTitle":"表达式","fnRender":function(obj){
					return describeExpressMap(obj.aData);
				}},
	            { "bSortable":false,"sWidth":100,"sTitle":"操作","fnRender":function(obj){
					var id = obj.aData.id;
					return "<a href='#' onclick='showUsers(\""+id+"\");' class='btn' title='显示用户' ><img src='"+springUrl+"/static/common/img/search.png' alt='显示用户'></a>" +
							"<a href='#' class='btn' title='修改' onclick='getExpression4Edit(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'></a>";
	            }}
	        ],
			btns:[
				{
						    "sExtends":    "text",
						    "sButtonText": "新增",
							"sToolTip": "",
							"fnClick": function ( nButton, oConfig, oFlash ) {
//								top.autoHeight($(document).height()+$("#addPane").outerHeight());
								$("#addPane").show("fast");	
								clearAddPane();
						    }
				},		
				{
						    "sExtends":    "text",
						    "sButtonText": "删除",
							"sToolTip": "",
							"fnClick": function ( nButton, oConfig, oFlash ) {
								var ids=Table.getSelectedRowsIDs(table);
								if(ids.length==0){
									Dialog.alert("消息提示","没有选中项","确定");
								}else{
									delAuthExpressionRecord(JSON.stringify(ids));
								}
						    }
				}
			]
	};
	table=Table.dataTable("hello",options);*/
 	

	$('#expression_search').click(function() {
		if(validate($("#authSearchForm"))){
			table.fnDraw(); } 
		}
	);
	$('#authExpressionName').keyup(function() {
		if(validate($("#authSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	$("#expression_add_btn").click(function(){
		var validateEl=$('#addPane');
		if(validate(validateEl)){
			addAuthExpressionRecord();
		}
	});
	
	
	getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#authSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}

//
function remoteOrgUserInfo() {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-cancel",
            "callback": function (modal) {
//                cancel();
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function (modal) {
                callBack(modal);
                return false;
            }
        }
    ];
    Dialog.openRemote('address_getOrgUser','选择人员', springUrl + '/system/address_getOrgUser?source=radio',800, 500, buttons);
}

//显示表达式对应的用户串
function showUsers(authId){
	var map = getUsers(authId);
//	$("#disp").html(map.usersName);	
//	$("#disp").show();
	if(map==null){
		Dialog.alert("提示","没有用户信息");
	}else{
		Dialog.alert("用户信息如下：",map.usersName);
	}
}

//隐藏表达式对应的用户串
function hideUsers(authId){
	$("#disp").hide();
}

//获得表达式对应的用户串Ajax
function getUsers(authId){
	var usersMap ;
    var data={
    		"authId":authId	
    };
	Dialog.post(springUrl+"/system/users_by_expressions_search_str",data,function(result){
		usersMap=result;
	});
	return usersMap;
}

//清空添加面板的数据
function clearAddPane(){
	setAuthExpressionId("");
	setAuthExpressionName("");
	setOrganization("","不限","不限");

    setPositionInfo([{id:"",positionName:"不限"}]);
    setUserInfo([{id:"",realName:"任何人"}]);
	setLevel("","","");
	setRole("");
	setGroup("");
}
//获取页面基础组件的信息（角色列表，用户组列表，职级列表）
function getBaseControlInfo(){
	jQuery.ajax( {
        type: "get", 
        url:springUrl+"/system/base_info",
        dataType: "json",
        success: function(result) {
        	$.each(result.levels_1,function(i,m){
        		var option='<option value="'+m.id+'">'+m.levelName+'</option>';
        		$("select[name='levelUp']").append(option);
        	});
        	$.each(result.levels_2,function(i,m){
        		var option='<option value="'+m.id+'">'+m.levelName+'</option>';
        		$("select[name='levelDown']").append(option);
        	});
        	$.each(result.roles,function(i,m){
        		var option='<option value="'+m.id+'">'+m.roleName+'</option>';
        		$("select[name='roleId']").append(option);
        	});
        	$.each(result.groups,function(i,m){
        		var option='<option value="'+m.id+'">'+m.roleName+'</option>';
        		$("select[name='groupId']").append(option);
        	});
        }
	});
}
//解释表达式
function describeExpressMap(expression){
	var nameVlaue;
	$.each(tableData.names,function(i,n){
		if(n.id==expression.id){
			nameVlaue=n;
			return false;
		}
	});
	
	var str_buf="";
	str_buf+="+[部门："+(nameVlaue.organizationName==null?"不限":nameVlaue.organizationName)+"]";
	str_buf+="+[职位："+(nameVlaue.positionName==null?"不限":nameVlaue.positionName)+"]";
	//levelAbove ,levelUp==levelDown 值为一致的
	/*if(expression.levelAbove!=null&&expression.levelAbove!=""){
		if(nameVlaue.levelAbove!=null){
			str_buf+="+[职级："+nameVlaue.levelAbove+"以上]";
		}else{
			str_buf+="没有找到对应职级";
		}
		
	}else{
		str_buf+="+[职级："+(nameVlaue.levelUp==null?"不限":nameVlaue.levelUp)+"]";
	}*/
	if(nameVlaue.levelUpName!= null && nameVlaue.levelDownName == null){
		str_buf+="+[职级："+nameVlaue.levelUpName+"以上]";
	}else if(nameVlaue.levelUpName == null && nameVlaue.levelDownName != null){
		str_buf+="+[职级："+nameVlaue.levelDownName+"以下]";
	}else if(nameVlaue.levelUpName != null && nameVlaue.levelDownName != null){
		if(nameVlaue.levelUpName == nameVlaue.levelDownName){
			str_buf+="+[职级："+nameVlaue.levelUpName+"]";
		}else{
			str_buf+="+[职级："+nameVlaue.levelUpName+"-"+nameVlaue.levelDownName+"]";
		}
	}else{
		str_buf+="+[职级：不限]";
	}
	str_buf+="+[用户："+(nameVlaue.userName==null?"任何人":nameVlaue.userName)+"]";
	str_buf+="+[角色："+(nameVlaue.roleName==null?"不限":nameVlaue.roleName)+"]";
	str_buf+="+[用户组："+(nameVlaue.groupName==null?"不限":nameVlaue.groupName)+"]";
	str_buf=str_buf.substring(1);
	return str_buf;
}
//获取要修改操作数据的值
function getExpression4Edit(id){
	var expression;
	$.each(tableData.aaData,function(i,n){
		if(n.id==id){
			expression=n;
			return false;
		}
	});
	var nameVlaue; //权限表达式对应的中文名称
	$.each(tableData.names,function(i,n){
		if(n.id==expression.id){
			nameVlaue=n;
			return false;
		}
	});
	nameVlaue.organizationName=nameVlaue.organizationName==null?"不限":nameVlaue.organizationName;
	nameVlaue.positionName=nameVlaue.positionName==null?"不限":nameVlaue.positionName;
	nameVlaue.userName=nameVlaue.userName==null?"任何人":nameVlaue.userName;
	
	setAuthExpressionId(expression.id);
	setAuthExpressionName(expression.authExpressionName);
	setOrganization(expression.organizationId,nameVlaue.organizationName,nameVlaue.organizationName);
    setPositionInfo([{id:expression.positionId,positionName:nameVlaue.positionName}]);
    setUserInfo([{id:expression.userId,realName:nameVlaue.userName}]);
	setLevel(expression.levelUp,expression.levelDown,expression.levelAbove);
	setRole(expression.roleId);
	setGroup(expression.groupId);
	$("#addPane").show("fast");
}
//赋值权限表达式的ID
function setAuthExpressionId(id){
	$("input[name='id']").val(id);
}
//赋值到权限表达式的名称
function setAuthExpressionName(authExpressionName){
	$("input[name='authExpressionName']").val(authExpressionName);
}
//从新弹窗赋值到"部门"
function setOrganization(orgId,orgName,orgFullname){
	$("input[name='organizationId']").val(orgId);
	$("#organization_name").text(orgName);
	$("#organization_name").attr("title",orgFullname);
}
////从新弹窗赋值到"职位"
//function setPostion(posId,posName,posFullname){
//	$("input[name='positionId']").val(posId);
//	$("#postion_name").text(posName);
//	$("#postion_name").attr("title",posFullname);
//}

//从新弹窗赋值到"职位"
function setPositionInfo(positionArray){
    $("input[name='positionId']").val(positionArray[0].id);
    $("#postion_name").text(positionArray[0].positionName);
    $("#postion_name").attr("title",positionArray[0].positionName);
}

//从新弹窗赋值到"员工"setUserInfo
function setUserInfo(userArray){
	$("input[name='userId']").val(userArray[0].id);
	$("#user_name").text(userArray[0].realName);
}
//设置职级
function setLevel(levelUp,levelDown,levelAbove){
	if(levelUp!=null){
		$("select[name='levelUp']").find("option[value='"+levelUp+"']").attr("selected",true);
	}
	if(levelDown!=null){
		$("select[name='levelDown']").find("option[value='"+levelDown+"']").attr("selected",true);
	}
}
//设置角色
function setRole(roleId){
	if(roleId==null||roleId==0) roleId='';
	$("select[name='roleId']").find("option[value='"+roleId+"']").attr("selected",true);
}
//设置管理组
function setGroup(groupId){
	if(groupId==null||groupId==0) groupId='';
	$("select[name='groupId']").find("option[value='"+groupId+"']").attr("selected",true);
}
//删除一条表达式
function delAuthExpressionRecord(ids){
	var data={
			ids:ids
	};
	Dialog.del(springUrl+"/system/authExpression_delete",data,function(result){
		if(result!=null&&result.length>0){
			Dialog.alert("删除操作提示","【"+result[0].authExpressionName+"】存在引用无法删除","确定");
		}else{
			Dialog.alert("删除操作提示","操作成功","确定");
			table.fnDraw();
		}

	});
}
//增加一条表达式
function addAuthExpressionRecord(e){
	$("#levelType").html("");
	var mode="";
	var id=$("input[name='id']").val();
	if(id==""){
		id=0;
	}
	var authExpressionName=$("input[name='authExpressionName']").val();
	var organizationId=$("input[name='organizationId']").val();
	if(organizationId==""){
		organizationId=0;
	}
	var positionId=$("input[name='positionId']").val();
	if(positionId==""){
		positionId=0;
	}
	var levelUp=$("select[name='levelUp']").val();
	if(levelUp==""){
		levelUp=0;
	}
	var levelDown=$("select[name='levelDown']").val();
	if(levelDown==""){
		levelDown=0;
	}
	//var levelAbove=$("input[name='levelAbove']").attr("checked")=="checked"?1:0;
	var userId=$("input[name='userId']").val();
	if(userId==""){
		userId=0;
	}
	var roleId=$("select[name='roleId']").val();
	if(roleId==""){
		roleId=0;
	}
	var groupId=$("select[name='groupId']").val();
	if(groupId==""){
		groupId=0;
	}
	if(levelUp != 0 && levelDown != 0){
		var levelUpName = "";
		levelUpName =($("select[name='levelUp'] option:selected").text()).substring(0,1);
		var leveDownName = "";
		var leveDownName = ($("select[name='levelDown'] option:selected").text()).substring(0,1);
		if(levelUpName != leveDownName){
			var str = "请选择相同职级";
			$("#levelType").html(str).css("color","red");
			return false;
		}
	}
	/*if(levelAbove==1){
		levelAbove=levelUp+"-"+levelDown;
		levelUp=null;
	}else{
		levelAbove=null;
		levelUp=levelUp+"-"+levelDown;
	}
	if(levelUp=="-") levelUp=null;
	if(levelAbove=="-") levelAbove=null;*/
	//表达式的map对象
	var expressionMap={
		"id":id,
		"mode":mode,
		"authExpressionName":authExpressionName,
		"organizationId":organizationId,
		"positionId":positionId,
		"levelUp":levelUp,
		"levelDown":levelDown,
		//"levelDown":levelUp,
		//"levelAbove":levelAbove,
		"userId":userId,
		"roleId":roleId,
		"groupId":groupId
	};
	//js的map对象转json
	var toJson=JSON.stringify(expressionMap);
	//封装成一个数组形式传入后台
	var expressions=new Array();
	expressions.push(expressionMap);
	var toJson=JSON.stringify(expressions);
	var data={
			"expressions":toJson
		};
	Dialog.post(springUrl+"/system/authExpression_save",data,function(result){
		if(result.operator==true){
			$("#addPane").hide("fast");
			table.fnDraw();
		}
		Dialog.alert("操作提示",result.message);
	});
	
}
//判断两个map是否相同
function isEqualMap(map1,map2){
	var bool=true;
	for(var key in map1){
		if(map1[key]!=map2[key]){
			bool=false;
			return false;
		}
	}
	return bool;
}

function selectOrgnaztion(){
	var btns = [				
				{
					"label": "取消",
					"class": "btn-cancel",
					"callback": function() {}
				},
				{
					"label": "确定",
					"class": "btn-success",
					"callback": function() {						
						var treeNodes = orgTree.getSelectedNodes();
						var treeNode = treeNodes[0];
						if(treeNode.id!=null){
							setOrganization(treeNode.id,treeNode.name,treeNode.props.orgFullname);
						} else {		
					        alert("请选择下级部门！");
						}	
					}
				}
			]
	Dialog.openRemote('expression_orgTreeIndex','选择部门',springUrl+'/system/expression_orgTreeIndex',400,600,btns);
}


function popPosition() {
	var btns = [				
				{
					"label": "取消",
					"class": "btn-cancel",
					"callback": function() {}
				},
				{
					"label": "确定",
					"class": "btn-success",
					"callback": function(modal) {
                        callBack(modal);
                        return false;
					}
				}
			]
    Dialog.openRemote('expression_posTreeIndex','选择职位',springUrl+'/system/address_getPosition?source=radio',800,600,btns);

	}
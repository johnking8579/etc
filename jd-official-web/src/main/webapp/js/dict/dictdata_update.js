/**
 * 数据字典-数据项修改、查看页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */



/**
 * 初始化操作
 */
$(function() {
	$('#dictDataName2').blur(function() {
		validateDictDataName();
	} );
	
	$('#dictDataName2').focus(function() {
		$("#isRepeatDictDataName").html("");
	});
	
	bindValidate($('#dictDataForm'));
//    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
//    $("#dictDataName").jqBootstrapValidation();
//    $("#sortNo").jqBootstrapValidation();
});


function validateDictDataName(){
	var dictTypeId=$("input#dictTypeId").val();
	var dictDataName=$("input#dictDataName2").val();
	var dictDataNameOld=$("input#dictDataNameOld").val();
	if(dictDataName!='' && dictDataName != dictDataNameOld){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictDataName:dictDataName,
				dictTypeId:dictTypeId
	        },
	        url:springUrl + "/dict/dictData_isExistDictDataName",
	        success:function (data) {
	        	if(data){
	        		var str = "此数据名称已存在!";
	        		$("#isRepeatDictDataName").html(str).css("color","red");
	        	}
	        }
		});
	}
}

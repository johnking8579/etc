package com.shb.monitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Layout;

/**
 * 实时日志.在RollingFileAppender基础上添加以下功能:
 * 1.定时器,午夜前自动滚动,防止长时间无日志时rolling不及时.
 * 2.滚动时回调. 统计性能
 * 
 * @author JingYing 2015年6月15日
 */
public class RealTimeRollingAppender extends AbstractRollingFileAppender {
	public RealTimeRollingAppender() {
		init();
	}

	public RealTimeRollingAppender(Layout layout, String filename,
			boolean append) throws IOException {
		super(layout, filename, append);
		init();
	}

	public RealTimeRollingAppender(Layout layout, String filename)
			throws IOException {
		super(layout, filename);
		init();
	}

	/**
	 * 每天晚上午夜前, 强制滚动日志, 防止漏统计未被滚动的日志
	 */
	public void init() {
		final RealTimeRollingAppender me = this;
		Calendar time = Calendar.getInstance();
		time.set(Calendar.HOUR_OF_DAY, 23);
		time.set(Calendar.MINUTE, 58);
		time.set(Calendar.SECOND, 0);

		new Timer("monitor.jar:flushRealtimelogAtMidnight", true).scheduleAtFixedRate(new TimerTask() {
			public void run() {
				try {
					me.rollOver();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, time.getTime(), 86400000L);
	}

	/**
	 * 从实时日志中统计
	 */
	public void afterRollover(final File lastBakFile) {
		new Thread(new Runnable() {
			public void run() {
				if (!lastBakFile.exists())
					return;

				long start = System.currentTimeMillis();
				Map<PerfKpiKey,PerfKpi> countMap = new HashMap<PerfKpiKey,PerfKpi>();
				BufferedReader br = null;
				String line;
				try {
					br = new BufferedReader(new InputStreamReader(new FileInputStream(lastBakFile), "utf-8"));
					while ((line = br.readLine()) != null)
						try {
							RealTimeRollingAppender.this.readLineToMap(countMap, line);
						} catch (Exception e) {
							LogManager.ERR.error(e.getMessage() + "  " + line);
						}
				} catch (IOException e) {
					LogManager.ERR.error(e.getMessage(), e);
				} finally {
					if(br != null){
						try {
							br.close();
						} catch (IOException e) {}
					}
				}
				LogManager.ERR.info(String.format("读取realtime roll文件[%s]用时[%s]", lastBakFile, System.currentTimeMillis() - start));
				RealTimeRollingAppender.this.logToHour(countMap);
				countMap.clear();
			}
		}).start();
	}

	/**
	 * 见MonitorLog.log()的写入格式
	 * @param countMap
	 * @param line
	 */
	private void readLineToMap(Map<PerfKpiKey, PerfKpi> countMap, String line) {
		String[] arr = line.split("\\|");
		String appId = arr[0], key = arr[1], start_yyyyMMddHH = arr[2];
		BigInteger cost = new BigInteger(arr[3]);
		
		PerfKpiKey mapKey = new PerfKpiKey(start_yyyyMMddHH, appId, key);
		PerfKpi kpi = (PerfKpi) countMap.get(mapKey);
		if (kpi != null) {
			kpi.addCount(1L);
			kpi.addTotalCostMillis(cost);
			if (cost.longValue() > kpi.getMaxCost())
				kpi.setMaxCost(cost.longValue());
			if (cost.longValue() < kpi.getMinCost())
				kpi.setMinCost(cost.longValue());
		} else {
			kpi = new PerfKpi();
			kpi.addCount(1L);
			kpi.addTotalCostMillis(cost);
			kpi.setMaxCost(cost.longValue());
			kpi.setMinCost(cost.longValue());
			countMap.put(mapKey, kpi);
		}
	}

	private void logToHour(Map<PerfKpiKey, PerfKpi> countMap) {
		for (Entry<PerfKpiKey,PerfKpi> e : countMap.entrySet()) {
			PerfKpiKey mapKey = e.getKey();
			PerfKpi kpi = e.getValue();
			BigInteger aver = kpi.getTotalCostMillis().divide(new BigInteger(kpi.getCount() + ""));
			LogManager.HOUR.info(String.format("%s|%s|%s|%s|%s|%s|%s",
					mapKey.getStartTime(), mapKey.getAppId(), mapKey.getKey(), 
					kpi.getCount(), aver, kpi.getMaxCost(), kpi.getMinCost()));
		}
	}
}

package com.shb.monitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class HourStatRollingAppender extends AbstractDailyRollingFileAppender {
	
	/**
	 * 得到昨天日期. 因为DailyRollingFileAppender零点后滚动. 而且LogManager.startScheduleFlushThread()在零点后滚动.
	 * @return
	 */
	private String getYesterday()	{
		Calendar now = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if("23".equals(new SimpleDateFormat("HH").format(now.getTime())))	{//增加判断,防止时间出异常
			return sdf.format(now);
		} else {
			now.add(Calendar.DATE, -1);
			return sdf.format(now.getTime());
		}
	}
	
	/**
	 * "小时段统计日志"滚动后的处理逻辑.
	 * 把小时段统计数据按天统计入map, 再把map写到按天统计日志中.
	 */
	public void afterRollover(final File lastBakFile) {
		new Thread(new Runnable() {
			public void run() {
				if (!lastBakFile.exists())
					return;

				long start = System.currentTimeMillis();
				Map<PerfKpiKey, PerfKpi> countMap = new HashMap<PerfKpiKey, PerfKpi>();
				BufferedReader br = null;
				String line;
				try {
					br = new BufferedReader(new InputStreamReader(new FileInputStream(lastBakFile), "utf-8"));
					while ((line = br.readLine()) != null)	{
						try {
							HourStatRollingAppender.this.readLineToMap(countMap, line, getYesterday());
						} catch (Exception e) {
							LogManager.ERR.error(e.getMessage() + "   " + line);
						}
					}
				} catch (IOException e) {
					LogManager.ERR.error(e.getMessage(), e);
				} finally {
					if(br != null){
						try {
							br.close();
						} catch (IOException e) {}
					}
				}

				LogManager.ERR.info(String.format("读取hourstat roll文件[%s]用时[%s]", lastBakFile, System.currentTimeMillis() - start));
				HourStatRollingAppender.this.logToDay(countMap);
				countMap.clear();
			}
		}).start();
	}

	/**
	 * 见RealTimeRollingAppender.logToHour()的写入格式
	 * @param countMap
	 * @param line
	 */
	private void readLineToMap(Map<PerfKpiKey, PerfKpi> countMap, String line, String date_yyyyMMdd) {
		String[] arr = line.split("\\|");
		String start_yyyyMMddHH = arr[0], appId = arr[1], key = arr[2];
		long count = Long.parseLong(arr[3]),
			average = Long.parseLong(arr[4]),
			max = Long.parseLong(arr[5]),
			min = Long.parseLong(arr[6]);

		String start_yyyyMMdd = start_yyyyMMddHH.substring(0, 8);	//截取时间段的天
		if(!start_yyyyMMdd.equals(date_yyyyMMdd))
			return;		//不统计非当天的数据
		PerfKpiKey mapKey = new PerfKpiKey(start_yyyyMMdd, appId, key);	
		PerfKpi kpi = (PerfKpi) countMap.get(mapKey);
		if (kpi != null) {
			kpi.addCount(count);
			if (max > kpi.getMaxCost())
				kpi.setMaxCost(max);
			if (min < kpi.getMinCost())
				kpi.setMinCost(min);
			long newAverage = calcAverage(count, average, kpi.getCount(), kpi.getAverageCost());
			kpi.setAverageCost(newAverage);
		} else {
			kpi = new PerfKpi();
			kpi.addCount(count);
			kpi.setMaxCost(max);
			kpi.setMinCost(min);
			kpi.setAverageCost(average);
			countMap.put(mapKey, kpi);
		}
	}

	private long calcAverage(long count1, long average1, long count2, long average2) {
		return (count1 * average1 + count2 * average2) / (count1 + count2);
	}

	//TODO 先使用文件存储, 以后使用数据库存储
	private void logToDay(Map<PerfKpiKey, PerfKpi> countMap) {
		Iterator<PerfKpiKey> it = countMap.keySet().iterator();
		if(it.hasNext())	{	//有数据表示有调用,无数据则表示无调用,不设置file即不生成当天的日志
			LogManager.setDayLog(it.next().getStartTime());
			LogManager.DAY.info("日期|appId|key|调用次数|平均耗时(ms)|最大耗时(ms)|最小耗时(ms)");
		}
		for (Entry<PerfKpiKey,PerfKpi> e : countMap.entrySet()) {
			PerfKpiKey mapKey = e.getKey();
			PerfKpi kpi = e.getValue();
			LogManager.DAY.info(String.format("%s|%s|%s|%s|%s|%s|%s",
					mapKey.getStartTime(), mapKey.getAppId(), mapKey.getKey(), 
					kpi.getCount(), kpi.getAverageCost(), kpi.getMaxCost(), kpi.getMinCost()));
		}
	}
}

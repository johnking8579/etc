package com.shb.monitor;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class MonitorLog {
	public static Logger log = LogManager.REAL_TIME;
	private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");

	/**
	 * 记录性能统计日志
	 * @param appId 不同的工程使用不同的appId. 
	 * @param key 用来标识一个唯一的method,推荐使用全类名+方法名
	 * @param startTimeMillis 方法开始调用时的毫秒时间
	 * @param costMillis 调用耗时,毫秒单位
	 */
	public static void log(String appId, String key, long startTimeMillis, long costMillis) {
		if (appId.contains("|")) 
			appId = appId.replace("|", "$");
		if (key.contains("|")) 
			key = key.replace("|", "$");
		String start_yyyyMMddHH = format.format(new Date(startTimeMillis));
		log.info(String.format("%s|%s|%s|%s", 
					appId, key, start_yyyyMMddHH, costMillis));
	}
	
}

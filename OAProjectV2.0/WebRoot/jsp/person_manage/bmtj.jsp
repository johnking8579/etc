<%@page contentType="text/html;charset=GB18030"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB18030">
<title>部门管理</title>
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css">
<link href="css/style_.css" rel="stylesheet" type="text/css">

<script>
function tianjia()
{
	var dname=document.getElementById('dname');
	var pid=document.getElementById('pid');
	var email=document.getElementById('email');
	var phone=document.getElementById('phone');
	if(dname.value==""){
		alert('请输入部门名称!');
		dname.focus();
		return false;
	}
	if(pid.value=="0"){
		alert('请选择上级部门!');
		return false;
	}
	if(email.value==""){
		alert('请输入部门邮箱!');
		email.focus();
		return false;
	}
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))){
		alert('请输入正确的邮箱地址!');
		return false;
	}
	var depid=document.getElementById("id");
	if(depid.value!=""){
		form1.action="departmentUpdate.action?action=update";
	}else{
		form1.action="departmentAdd.action";
	}
	form1.submit();
}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="25" align="center" valign="bottom" class="td06"> <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="2%" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif">&nbsp;<br>&nbsp;</td>
          <td width="2%" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif"><img src="${pageContext.request.contextPath}/images/main_28.gif" width="9" height="9" align="absmiddle"></td>
          <td height="30" valign="middle" background="${pageContext.request.contextPath}/images/bg_03.gif"><div align="left"><font color="#FFFFFF">部门添加</font></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <form name="form1" method="post" action="${pageContext.request.contextPath }/departmentAdd.action">
    <table width="95%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="15%" height="30" class="td_form01"> 部门名称</td>
        <td class="td_form02">
        <input type="hidden" name="id" value="${depbyid.id }">
          <input type="text" name="dname" id="dname" maxlength="10" class="input" style="width:95% " value="${depbyid.depname }">          
        </td>
      </tr>
      <!--  <tr>
        <td width="15%" height="30" class="td_form01"> 所属单位</td>
        <td class="td_form02">
          <select name="select" style="width:95% ">
          </select>
        </span> </span> <span><span class="td_form01">
      </span></span></td>
      </tr>-->
      <tr>
        <td width="15%" height="30" class="td_form01">上级部门</td>
        <td class="td_form02">
          <select name="pid" id="pid"  style="width:95% ">
          	<option value="0">选择部门
          	<c:forEach items="${deplist}" var="dep">
          		<c:if test="${dep.id==depbyid.pid}">
          			<option value="${dep.id }" selected="selected">${dep.depname }</option>
          		</c:if>
          		<c:if test="${dep.id!=depbyid.pid}">
          			<option value="${dep.id }">${dep.depname }</option>
          		</c:if>
          	</c:forEach>
          </select>
        </span> </span> <span><span class="td_form01"> </span></span></td>
      </tr>
      <tr>
        <td width="15%" height="30" class="td_form01"> 部门邮箱</td>
        <td class="td_form02">
        <input name="email" id="email" type="text" class="input" style="width:95% " value="${depbyid.email }"></td>
      </tr>
      <tr>
        <td width="15%" height="30" class="td_form01"> 办公电话</td>
        <td class="td_form02">
        <input name="phone" id="phone" type="text" class="input" style="width:95% " value="${depbyid.phone }"></td>
      </tr>
      <tr>
        <td width="15%" height="30" class="td_form01"> 部门描述</td>
        <td class="td_form02">
        <textarea rows="5" cols="" name="content" id="content" style="width:95% ">${depbyid.content }</textarea>
        </td>
      </tr>
    </table>
    <br>
    <table width="95%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" class="td_page"><div>
            <input name="Submit" type="submit" class="buttonface02" onClick="return tianjia()" value=" 提交 ">
&nbsp;
        <input name="Submit" type="reset" class="buttonface02" value="  重置  ">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="&#21333;&#20301;&#31649;&#29702;.htm">返回上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
        </div></td>
      </tr>
    </table>
    </p>
  </form>
</center>
</body>
</html>

package com.bjpowernode.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Users {
	private BigDecimal id;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Timestamp getWorktime() {
		return worktime;
	}
	public void setWorktime(Timestamp worktime) {
		this.worktime = worktime;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public BigDecimal getDepid() {
		return depid;
	}
	public void setDepid(BigDecimal depid) {
		this.depid = depid;
	}
	public BigDecimal getDuty() {
		return duty;
	}
	public void setDuty(BigDecimal duty) {
		this.duty = duty;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getHomephone() {
		return homephone;
	}
	public void setHomephone(String homephone) {
		this.homephone = homephone;
	}
	public String getWorkphone() {
		return workphone;
	}
	public void setWorkphone(String workphone) {
		this.workphone = workphone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMsn() {
		return msn;
	}
	public void setMsn(String msn) {
		this.msn = msn;
	}
	public Timestamp getBirthday() {
		return birthday;
	}
	public void setBirthday(Timestamp birthday) {
		this.birthday = birthday;
	}
	public String getHttpaddress() {
		return httpaddress;
	}
	public void setHttpaddress(String httpaddress) {
		this.httpaddress = httpaddress;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Timestamp getLogontime() {
		return logontime;
	}
	public void setLogontime(Timestamp logontime) {
		this.logontime = logontime;
	}
	
	public BigDecimal getLogoncount() {
		return logoncount;
	}
	public void setLogoncount(BigDecimal logoncount) {
		this.logoncount = logoncount;
	}
	private String username;
	private String password;
	private String nickname;
	private Timestamp worktime;
	private String sex;
	private BigDecimal depid;
	private BigDecimal duty;
	private String email;
	private String mobile;
	private String homephone;
	private String workphone;
	private String fax;
	private String msn;
	private Timestamp birthday;
	private String httpaddress;
	private String address;
	private String content;
	private Timestamp logontime;
	private Timestamp lastlogontime;
	public Timestamp getLastlogontime() {
		return lastlogontime;
	}
	public void setLastlogontime(Timestamp lastlogontime) {
		this.lastlogontime = lastlogontime;
	}
	private BigDecimal logoncount;
}

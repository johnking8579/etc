package com.bjpowernode.file.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public abstract class BaseAction extends HttpServlet {
	
	protected final String SUCCESS="/jsp/success.jsp";
	protected final String ERROR="/jsp/error.jsp";

	private final String M_UPLOAD = "upload";
	private final String M_DOWNLOAD = "download";
	private final String M_ALLFILE = "allfile";

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if (M_UPLOAD.equals(action)) {
			upload(request, response);
		}else if(M_DOWNLOAD.equals(action)){
			download(request,response);
		}else if(M_ALLFILE.equals(action)){
			allFile(request,response);
		}
	}

	protected abstract void upload(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	protected abstract void download(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	protected abstract void allFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
}

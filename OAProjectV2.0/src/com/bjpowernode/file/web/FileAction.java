package com.bjpowernode.file.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.omg.CORBA_2_3.portable.OutputStream;

import com.bjpowernode.file.service.FileService;
import com.bjpowernode.file.service.FileServiceImpl;

@SuppressWarnings("serial")
public class FileAction extends BaseAction {

	private FileService fileService;
	@Override
	protected void allFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		fileService=new FileServiceImpl();
		List<?> list=fileService.getAllFile();
		request.setAttribute("filelist", list);
		request.getRequestDispatcher("/jsp/work_manage/gxxz.jsp").forward(request, response);
	}

	@Override
	protected void download(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		//String id=request.getParameter("id");
		String fileName=request.getParameter("filename");
		//获取下载的文件
		File file=new File(getServletContext().getRealPath("\\saveDir\\")+"\\"+fileName);
		FileInputStream fis=new FileInputStream(file);
		response.setContentType("application/force-download");
		response.setHeader("content-length", String.valueOf(file.length()));
		response.setHeader("Content-disposition", "attachment;filename="+new String(file.getName().getBytes("GB18030"),"iso8859-1"));
		ServletOutputStream os=response.getOutputStream();
		int length=0;
		while((length=fis.read())!=-1){
			os.write(length);
		}
		fis.close();
		os.flush();
		os.close();
	}

	@Override
	protected void upload(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path=SUCCESS;
		try{
		FileService fileService=new FileServiceImpl();
		fileService.fileUpload(request);
		}catch(Exception e){
			request.setAttribute("message", e.getMessage());
			path=ERROR;
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}

package com.bjpowernode.file.service;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.bjpowernode.file.dao.FileDao;
import com.bjpowernode.file.dao.impl.FileDaoImpl;
import com.bjpowernode.system.ApplicationException;

public class FileServiceImpl implements FileService {

	private FileDao fileDao=new FileDaoImpl();
	@SuppressWarnings("deprecation")
	public void fileUpload(HttpServletRequest request) {
		try {
			// 上传组件的保存目录
			String savePath = request.getRealPath("/saveDir/");
			// 上传组件的临时目录
			String tmpPath = request.getRealPath("/tmpDir/");
			// 初始化文件上传的目录
			File tmpDir = new File(tmpPath);
			File saveDir = new File(savePath);

			if (!tmpDir.isDirectory())
				tmpDir.mkdir();
			if (!saveDir.isDirectory())
				saveDir.mkdir();

			String t_name = null;
			/*
			 * isMultipartContent方法用于判断请求消息中的内容是否是"multipart/form-data"类型
			 */
			if (ServletFileUpload.isMultipartContent(request)) {
				// 实例化一个硬盘文件工厂,用来配置上传组件
				DiskFileItemFactory dff = new DiskFileItemFactory();
				// 指定上传文件的临时目录
				dff.setRepository(tmpDir);
				// 指定在内存当中缓存数据大小
				dff.setSizeThreshold(1024000);
				// 实例化上传组件
				ServletFileUpload sfu = new ServletFileUpload(dff);
				// 设置单个文件的大小,-1表示没有限制,默认是字节
				sfu.setFileSizeMax(500000000);

				// 从上传域得到所有的上传列表
				List<?> fileList = null;

				fileList = sfu.parseRequest(request);
				// 得到所有的上傳文件
				Iterator<?> fileItr = fileList.iterator();
				// 允许上传的文件格式列表
				final String[] allowExt = new String[] { "jpg", "doc", "txt",
						"gif" };
				String path = null;
				
				while (fileItr.hasNext()) {
					FileItem fileItem = null;
					/*
					 * FileItem类用来封装单个表单字段元素的数据
					 */
					fileItem = (FileItem) fileItr.next();
					//isFormField方法用来判断FileItem类对象封装的数据是否是一个普通的文本域
					if (fileItem.isFormField()) {
						String content=fileItem.getString("GB18030");
						System.out.println(content);
						String filedName=fileItem.getFieldName();
						System.out.println(filedName);
						request.setAttribute(filedName, content);
					} else {
						// 得到文件的完整路径
						path = fileItem.getName();
						// 得到文件的名称
						t_name = path.substring(path.lastIndexOf("\\") + 1);
						// 得到文件的类型
						String t_ext = t_name
								.substring(t_name.lastIndexOf(".") + 1);
						// 过滤接收规定文件格式之外的文件类型
						int allowFlag = 0;
						int allowedExtCount = allowExt.length;
						for (; allowFlag < allowedExtCount; allowFlag++) {
							if (allowExt[allowFlag].equals(t_ext))
								break;
						}
						fileItem.write(new File(savePath + "\\" + t_name));
					}
				}
			}
			
			//保存上传文件的相关信息
			insertFileInfo(t_name,request.getAttribute("content").toString());
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("上传组件失败!");
		}
	}
	
	public void insertFileInfo(String fileName, String content){
		fileDao.insertFileInfo(fileName, content);
	}
	
	//获取所有可供下载的资源
	public List<?> getAllFile(){
		return fileDao.getAllFile();
	}

}

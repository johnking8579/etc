package com.bjpowernode.file.dao;

import java.util.List;

public interface FileDao {
	// 保存上传文件的相关信息
	public void insertFileInfo(String fileName, String content);
	
	//获取所有可供下载的资源
	public List<?> getAllFile();
}

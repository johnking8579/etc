package com.bjpowernode.file.dao.impl;

import java.util.List;

import com.bjpowernode.dao.impl.BaseDaoSupport;
import com.bjpowernode.file.dao.FileDao;
import com.bjpowernode.system.OAUtils;

public class FileDaoImpl extends BaseDaoSupport implements FileDao {

	public void insertFileInfo(String fileName, String content) {
		String sql="insert into t_file values(req_oa.nextval,?,?,?,?,?)";
		super.insert(sql, fileName,content,1,OAUtils.getSysCurrentTime(),0);
	}
	
	//获取所有可供下载的资源
	public List<?> getAllFile(){
		String sql="select id,filename,uploadtime,count from t_file";
		List<?> list=queryForListByMap(sql);
		return list;
	}

}

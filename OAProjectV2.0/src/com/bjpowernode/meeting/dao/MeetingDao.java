package com.bjpowernode.meeting.dao;

import java.util.List;
import java.util.Map;

public interface MeetingDao {
	//添加会议室
	public void addMeetingRoom(Map dto);
	//获取所有部门
	public List getAllDep();
	//获取所有员工
	public List getAllUser();
}

package com.bjpowernode.meeting.service;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.bjpowernode.dao.conn.DBUtils;
import com.bjpowernode.meeting.dao.MeetingDao;
import com.bjpowernode.system.ApplicationException;
import com.bjpowernode.system.SysBeanFactory;

public class MeetingServiceImpl implements MeetingService {

	private Logger logs = Logger.getLogger(MeetingServiceImpl.class);
	private MeetingDao meetingDao;

	public void setMeetingDao(MeetingDao meetingDao) {
		this.meetingDao = meetingDao;
	}

	public void addMeetingRoom(Map dto) {
		try {
			//DBUtils.setAutoCommit(false);
			meetingDao.addMeetingRoom(dto);
			//DBUtils.commit();
		} catch (Exception e) {
			//DBUtils.rollback();
			throw new ApplicationException("����������ʧ��!");
		}
	}
	
	public List getAllDep(){
		return meetingDao.getAllDep();
	}
	
	public List getAllUser(){
		return meetingDao.getAllUser();
	}
}

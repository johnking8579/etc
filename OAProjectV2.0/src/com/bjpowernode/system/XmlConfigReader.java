package com.bjpowernode.system;

import java.io.InputStream;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.bjpowernode.system.bean.JdbcConfig;

public class XmlConfigReader {
	// 饿汉式(预先加载)
	// private static XmlConfigReader instance=new XmlConfigReader();
	// private XmlConfigReader(){}
	/*
	 * public static XmlConfigReader getInstance(){ return instance; }
	 */

	// 懒汉式(延迟加载)
	private static XmlConfigReader instance = null;

	public static synchronized XmlConfigReader getInstance() {
		if (instance == null)
			instance = new XmlConfigReader();
		return instance;
	}

	// 保存jdbc相关配置信息
	private JdbcConfig jdbcConfig = new JdbcConfig();

	public JdbcConfig getJdbcConfig(){
		return jdbcConfig;
	}
	
	private XmlConfigReader() {
		SAXReader reader = new SAXReader();
		InputStream in = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("DBOption-config.xml");
		try {
			Document doc = reader.read(in);
			// 获取jdbc相关配置信息
			Element driverName = (Element) doc
					.selectObject("/config/db-info/driver-name");
			Element url = (Element) doc.selectObject("/config/db-info/url");
			Element userName = (Element) doc
					.selectObject("/config/db-info/user-name");
			Element password = (Element) doc
					.selectObject("/config/db-info/passwrod");
			Element use_datasource = (Element) doc
			.selectObject("/config/db-info/use-datasource");
			Element datasource_name = (Element) doc
			.selectObject("/config/db-info/datasource-name");
			// 设置jdbc相关的配置
			jdbcConfig.setDriverName(driverName.getStringValue());
			jdbcConfig.setUserName(userName.getStringValue());
			jdbcConfig.setUrl(url.getStringValue());
			jdbcConfig.setPassword(password.getStringValue());
			jdbcConfig.setUse_datasource(use_datasource.getStringValue());
			jdbcConfig.setDatasource_name(datasource_name.getStringValue());
		} catch (Exception e) {
		}
	}
}

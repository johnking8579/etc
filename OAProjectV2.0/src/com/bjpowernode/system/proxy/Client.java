package com.bjpowernode.system.proxy;

import java.lang.reflect.Proxy;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//UserService us=new UserServiceImpl();
		//us.addUser("admin","admin");
		
		//UserService us=new UserServiceProxy(new UserServiceImpl());
		//us.addUser("admin","admin");
		
		//1、Proxy.newProxyInstance() 来在内存当中创建了一个代理类,这个代理类同样实现了目标对象的接口，因为只有实现了接口才能知道目标对象有哪些方法。
		//2、当调用目标对象方法的时候，代理类通过调用一个实现了InvocationHandler类的方法invoke
		SysServiceImplProxy usip=new SysServiceImplProxy();
		UserService us=(UserService)usip.crateProxyInstance(new UserServiceImpl());
		//us.addUser("admin", "admin");
		System.out.println(us.getUser("admin"));
		
		//MtService mt=(MtService)usip.crateProxyInstance(new MtServiceImpl());
		//mt.addMeeting("工作会议");
	}

}

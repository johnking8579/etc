package com.bjpowernode.system;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.bjpowernode.dao.conn.DBUtils;

public class IdGenerator {
	//第一种方式加锁:整个方法在执行过程当中其他用户是执行不了的
	//public synchronized static int generate() {
		 public static int generate() {
		// synchronized (1=1) {
		//第二种方式:对表进行加锁
		String sql = "select table_id from t_id for update";
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int value = 0;

		try {
			conn = DBUtils.getConnection();
			conn.setAutoCommit(false);
			pstm = conn.prepareStatement(sql);
			rs = pstm.executeQuery();
			if (rs.next())
				value = rs.getInt(1);
			sql = "update t_id set table_id=table_id+1";
			pstm = conn.prepareStatement(sql);
			pstm.executeUpdate();
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtils.close(rs);
			DBUtils.close(pstm);
			//DBUtils.close();
		}
		return value;
		// }
	}

	public static void main(String[] args) {
		System.out.println(IdGenerator.generate());
	}
}

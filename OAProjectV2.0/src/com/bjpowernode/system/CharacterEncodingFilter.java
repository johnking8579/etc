package com.bjpowernode.system;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CharacterEncodingFilter implements Filter {

	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	private String encoding = "GB18030";

	public void destroy() {
		filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(encoding);
		// 如果过滤器不只一个,执行其他配置的过滤器
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		// 获取filter配置参数
		this.filterConfig = filterConfig;
		String str = filterConfig.getInitParameter("encoding");
		if (str != null)
			encoding = str;
	}

}

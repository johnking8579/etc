package com.bjpowernode.system.bean;

public class JdbcConfig {
	private String driverName;
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	private String url;
	private String userName;
	private String password;
	//是否使用数据库连接池
	private String use_datasource;
	public String getUse_datasource() {
		return use_datasource;
	}
	public void setUse_datasource(String useDatasource) {
		use_datasource = useDatasource;
	}
	public String getDatasource_name() {
		return datasource_name;
	}
	public void setDatasource_name(String datasourceName) {
		datasource_name = datasourceName;
	}
	private String datasource_name;
}

package com.bjpowernode.system;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class OAUtils {
	public static int bigDecimalToInt(Object o) {
		return ((BigDecimal) o).intValue();
	}

	public static Timestamp getSysCurrentTime() {
		return new Timestamp((new Date()).getTime());
	}

	public static Timestamp dateToTime(Object o) {
		return new Timestamp(((Date) o).getTime());
	}

	public static BigDecimal stringToBigDecimal(String var) {
		return new BigDecimal(var);
	}

	public static Timestamp stringToTime(String str) {
		return new Timestamp(getDateByStrToYMD(str).getTime());
	}

	public static Date getDateByStrToYMD(String str) {
		Date date = null;
		if (str != null && str.length() > 0) {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = format.parse(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	// 批量删除功能sql语句的处理
	public static String sqlConverter(String sql, String[] args) {
		// ?为一个特殊字符,需要使用[?]来处理
		String[] sql1 = sql.split("[?]");
		String str = "";
		for (int i = 0; i < args.length; i++) {
			str = str + "?";
			if (i != args.length - 1) {
				str = str + ",";
			}
		}
		return sql1[0] + str + sql1[1];
	}
	
	public static String sqlConverter(String sql, Map<?, ?> map) {
		// ?为一个特殊字符,需要使用[?]来处理
		String[] sql1 = sql.split("[?]");
		String str = "";
		for (int i = 0; i < map.size(); i++) {
			str = str + "?";
			if (i != map.size() - 1) {
				str = str + ",";
			}
		}
		return sql1[0] + str + sql1[1];
	}
	
	//判断指定字符串是否是一个日期类型
	public static boolean isValidDate(String str){
		final SimpleDateFormat dataFormat=new SimpleDateFormat("yyyy-MM-dd");
		try{
			dataFormat.parse(str);
			return true;
		}catch(Exception e){
			//如果抛出ParseException,就说明格式不对.
			return false;
		}
	} 
}

package com.bjpowernode.system;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class SysBeanFactory {

	// 记录日志
	private Logger logs = Logger.getLogger(SysBeanFactory.class);
	// 系统缺省配置文件
	private String beansConfgFile = "ApplicationContext.xml";
	// 存放系统所有dao组件
	private Map<String, Object> daoMap = new HashMap<String, Object>();
	// 存放系统所有service组件
	private Map<String, Object> serviceMap = new HashMap<String, Object>();
	// 存放系统所有servlet组件
	private Map<String, Object> servletMap = new HashMap<String, Object>();

	public Object getDaoObject(String daoId) {
		return daoMap.get(daoId);
	}

	public Object getServiceObject(String serviceId) {
		return serviceMap.get(serviceId);
	}

	public Object getServletObject(String servletId) {
		return servletMap.get(servletId);
	}

	// 私有构造方法保证外部类不可以直接实例化SysBeanFactory
	private SysBeanFactory() {
	}

	private static SysBeanFactory factory = null;

	public static SysBeanFactory getInstance() {
		if (factory == null)
			factory = new SysBeanFactory();
		return factory;
	}

	// 创建系统所有组件的对象
	public void createBean() {
		logs.info("加载解析ApplicationContext.xml");
		try {
			SAXReader reader = new SAXReader();
			Document c = reader.read(Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream(beansConfgFile));
			// 获取根节点
			Element rootNode = c.getRootElement();
			logs.info("创建所有的dao对象");
			List<Element> daoNodeList = rootNode.selectNodes("./dao");
			for (Element e : daoNodeList) {
				// 获取dao元素的id属性
				String attId = e.attributeValue("id");
				// 获取dao元素的class属性
				String attClassName = e.attributeValue("class");
				// 创建dao对象
				Object daoObj = Class.forName(attClassName).newInstance();
				daoMap.put(attId, daoObj);
			}

			logs.info("创建所有的service对象");
			List<Element> serviceNodeList = rootNode.selectNodes("./service");
			for (Element e : serviceNodeList) {
				// 获取dao元素的id属性
				String attId = e.attributeValue("id");
				// 获取dao元素的class属性
				String attClassName = e.attributeValue("class");
				// 创建dao对象
				Object serviceObj = Class.forName(attClassName).newInstance();
				serviceMap.put(attId, new TransactionProxy().crateProxyInstance(serviceObj));

				// 查找依赖dao
				List<Element> paramDaoList = e.selectNodes("./param");
				for (Element param : paramDaoList) {
					String daoId = param.attributeValue("ref");
					// 获取dao对象
					Object daoObj = daoMap.get(daoId);
					// 组装service当中给dao赋值的方法
					String methodName = "set"
							+ daoId.substring(0, 1).toUpperCase()
							+ daoId.substring(1);
					// 獲取service當中的方法
					Method m = Class.forName(attClassName).getMethod(
							methodName, daoObj.getClass().getInterfaces()[0]);
					m.invoke(serviceObj, daoObj);
				}
			}
			logs.info("创建所有的servlet对象");
			List<Element> servletNodeList = rootNode.selectNodes("./servlet");
			for (Element e : servletNodeList) {
				// 获取servlet元素的id属性
				String attId = e.attributeValue("id");
				// 获取servlet元素的class属性
				String attClassName = e.attributeValue("class");
				// 创建servlet对象
				Object servletObj = Class.forName(attClassName).newInstance();
				servletMap.put(attId, servletObj);

				// 查找依赖dao
				List<Element> paramServiceList = e.selectNodes("./param");
				for (Element param : paramServiceList) {
					String serviceId = param.attributeValue("ref");
					// 获取service对象
					Object serviceObj = serviceMap.get(serviceId);
					// 组装servlet当中给service赋值的方法
					String methodName = "set"
							+ serviceId.substring(0, 1).toUpperCase()
							+ serviceId.substring(1);
					// 獲取service當中的方法
					Method m = Class.forName(attClassName).getMethod(
							methodName,
							serviceObj.getClass().getInterfaces()[0]);
					m.invoke(servletObj, serviceObj);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.bjpowernode.system;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.bjpowernode.dao.conn.DBUtils;

public class TransactionProxy implements InvocationHandler {

	private Object targetObj;

	public Object crateProxyInstance(Object targetObj) {
		this.targetObj = targetObj;
		return Proxy.newProxyInstance(targetObj.getClass().getClassLoader(),
				targetObj.getClass().getInterfaces(), this);
	}

	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		Object obj = null;
		try {
			if (method.getName().startsWith("add")
					|| method.getName().startsWith("update")
					|| method.getName().startsWith("delete")) {
				DBUtils.setAutoCommit(false);
			}
			obj = method.invoke(targetObj, args);
			DBUtils.commit();
		} catch (Exception e) {
			DBUtils.rollback();
			throw new ApplicationException("业务处理操作失败!");
		}
		return obj;
	}
}

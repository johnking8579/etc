package com.bjpowernode.service;

import java.util.List;

import com.bjpowernode.bean.Department;
import com.bjpowernode.system.bean.PageModel;

public interface DepartmentService {
	//获取部门信息
	public List<Department> getAllDep();
	//新增部门
	public void addDep(Department dep);
	
	public PageModel getDepPage(int pageNo,String dname);
	
	//根据部门id查询数据
	public Department getDepById(int id);
	
	//根据部门id修改数据
	public void updateDep(Department dep);
	
	public void deleteDep(String args[]);
}

package com.bjpowernode.service.impl;

import java.util.List;

import com.bjpowernode.bean.Department;
import com.bjpowernode.dao.DepartmentDao;
import com.bjpowernode.dao.impl.DepartmentDaoImpl;
import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.system.bean.PageModel;

public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentDao depDao = new DepartmentDaoImpl();

	public List<Department> getAllDep() {
		return depDao.getAllDep();
	}

	// 新增部门
	public void addDep(Department dep) {
		depDao.addDep(dep);
	}

	public PageModel getDepPage(int pageNo,String dname) {
		PageModel pm = new PageModel();
		// 得到总记录数
		int total = depDao.getDepCount(dname);
		int pageno = pageNo;
		int pageUnit = 2;
		int pageSize = (total - 1) / pageUnit + 1;
		pageno = pageno <= 1 ? 1 : (pageno >= pageSize ? pageSize : pageno);
		String path = "departmentSelectByPage.action";
		List rows = depDao.getDepByPage((pageno - 1) * pageUnit, pageUnit,dname);

		pm.setPageNo(pageno);
		pm.setPageSize(pageSize);
		pm.setPageUnit(pageUnit);
		pm.setPath(path);
		pm.setRows(rows);
		pm.setTotal(total);
		return pm;
	}
	//根据部门id查询数据
	public Department getDepById(int id){
		return depDao.getDepById(id);
	}
	//根据部门id修改数据
	public void updateDep(Department dep){
		depDao.updateDep(dep);
	}
	
	public void deleteDep(String args[]){
		depDao.deleteDep(args);
	}
}

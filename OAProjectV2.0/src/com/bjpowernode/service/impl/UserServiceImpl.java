package com.bjpowernode.service.impl;

import java.util.Map;

import com.bjpowernode.bean.Users;
import com.bjpowernode.dao.UserDao;
import com.bjpowernode.dao.impl.UserDaoImpl;
import com.bjpowernode.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDao=new UserDaoImpl();
	public boolean isLoginUser(String username) {
		return userDao.isLoginUser(username);
	}
	
	//判断用户名和密码是否匹配
	public boolean isLoginUser(String username,String password){
		return userDao.isLoginUser(username, password);
	}
	
	public void updateLogonTimeAndCount(String username){
		userDao.updateLogonTimeAndCount(username);
	}
	
	public Users getUsersInfo(String username){
		return userDao.getUsersInfo(username);
	}
	
	// 添加用户
	public void addUser(Map dto){
		userDao.addUser(dto);
	}
}

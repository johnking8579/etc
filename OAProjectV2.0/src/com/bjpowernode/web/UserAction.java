package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.service.UserService;
import com.bjpowernode.service.impl.UserServiceImpl;

@SuppressWarnings("serial")
public class UserAction extends HttpServlet {
	private UserService userService;

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		userService = new UserServiceImpl();
		boolean isUser = userService.isLoginUser(username);
		String message = null;
		String path = "/jsp/login.jsp";
		if (isUser) {
			// 判断密码是否匹配....
			String password = request.getParameter("password");
			isUser = userService.isLoginUser(username, password);
			if (isUser) {
				// 修改用户登录的时间、登录次数
				userService.updateLogonTimeAndCount(username);
				// 获取用户登录详细信息 存放到session
				request.getSession().setAttribute("users",
						userService.getUsersInfo(username));
				// 记住用户名称的操作
				String cbox=request.getParameter("cbox");
				addCookie(request, response, username, password, cbox);
				path = "/jsp/index.jsp";
			} else {
				// 密码不匹配
				message = "密码和用户名称不匹配!";
			}
		} else
			message = "您输入的用户名称不存在,请重新输入!";
		request.setAttribute("message", message);
		request.getRequestDispatcher(path).forward(request, response);
	}

	private void addCookie(final HttpServletRequest request,
			final HttpServletResponse response, final String username,
			final String password, final String cbox) {
		Cookie[] cookies=request.getCookies();
		if(cookies!=null){
			for(int i=0;i<cookies.length;i++){
				String uname=cookies[i].getName();
				if(uname.equals("cbox")||uname.equals(username)){
					cookies[i].setMaxAge(0);
					response.addCookie(cookies[i]);
				}
			}
		}
		
		
		if(cbox!=null){
			Cookie cookie=new Cookie(username,password);
			cookie.setMaxAge(7*24*60*60);
			response.addCookie(cookie);
		}
		Cookie cookie=new Cookie("cbox",cbox);
		cookie.setMaxAge(7*24*60*60);
		response.addCookie(cookie);
	}

}

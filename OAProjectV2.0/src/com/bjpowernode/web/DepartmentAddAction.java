package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.bean.Department;
import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.service.impl.DepartmentServiceImpl;
import com.bjpowernode.system.OAUtils;

public class DepartmentAddAction extends BaseAction  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -33502029499892453L;
	private DepartmentService depService;
	@Override
	protected void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("GB18030");
		
		depService=new DepartmentServiceImpl();
		String dname=request.getParameter("dname");
		String pid=request.getParameter("pid");
		String email=request.getParameter("email");
		String phone=request.getParameter("phone");
		String content=request.getParameter("content");
		
		Department dep=new Department();
		dep.setContent(content);
		dep.setDepname(dname);
		dep.setEmail(email);
		dep.setPhone(phone);
		dep.setPid(OAUtils.stringToBigDecimal(pid));
		
		depService.addDep(dep);
		success(request, response);
		
	}

}

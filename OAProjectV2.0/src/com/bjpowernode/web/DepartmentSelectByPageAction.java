package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.service.DepartmentService;
import com.bjpowernode.service.impl.DepartmentServiceImpl;
import com.bjpowernode.system.bean.PageModel;

public class DepartmentSelectByPageAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DepartmentService depService;
	@Override
	protected void execute(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		depService=new DepartmentServiceImpl();
		String pn=req.getParameter("pn");
		pn=(pn==null)?"1":pn;
		String dname=req.getParameter("dname");
		dname=(dname==null)?"":dname;
		PageModel pm=depService.getDepPage(Integer.parseInt(pn),dname);
		req.setAttribute("pagemodel", pm);
		req.setAttribute("dname", dname);
		req.getRequestDispatcher("/jsp/person_manage/bmgl.jsp").forward(req, resp);
	}

}

package com.bjpowernode.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class UserGetCookieAction extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Cookie[] cookies = request.getCookies();
		String uname = null;
		String pass = null;
		boolean c = false;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				String username = cookies[i].getName();
				System.out.println(username + ":" + cookies[i].getValue());
				if (username.equals("JSESSIONID") || username.equals("cbox")) {
					if (username.equals("cbox")) {
						if (!cookies[i].getValue().equals(""))
							c = true;
					}
					continue;
				}
				uname = cookies[i].getName();
				pass = cookies[i].getValue();
			}
		}
		request.setAttribute("uname", uname);
		request.setAttribute("pass", pass);
		request.setAttribute("c", c);
		request.getRequestDispatcher("/jsp/login.jsp").forward(request,
				response);
	}

}

package com.bjpowernode.workmanage.dao;

import java.util.Map;

public interface WorkDao {
	//添加工作日报
	public void addWork(Map<?, ?> dto);
	
	//添加日报日志
	public void addLog(String loginfo);
}

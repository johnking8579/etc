/*
 * 使用反射对BaseAction进行封装
 */

package com.bjpowernode.workmanage.web;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.system.SysBeanFactory;

public class BaseAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String action = req.getParameter("action");
		
		String path=req.getServletPath();//----->/meeting.action
		path=path.substring(0,path.length()-7);
		Object servletObj=SysBeanFactory.getInstance().getServletObject(path);
		Class<? extends Object> servletClass=servletObj.getClass();
		
		try {
			// 获取本类当中的方法,方法名称就是action参数所指定的值
			// 获取action值所对应的方法,并且这个方法只有两个参数,第一个为:HttpServletRequest,第二个为:HttpServletResponse
			Method method = servletClass.getMethod(action,
					HttpServletRequest.class, HttpServletResponse.class);
			// 相当于调用action参数所指定值所对应的方法
			// add()
			method.invoke(servletObj, req, resp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void forward(String path, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(path).forward(req, resp);
	}

}

package com.bjpowernode.usermanage.web;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bjpowernode.service.UserService;
import com.bjpowernode.service.impl.UserServiceImpl;
import com.bjpowernode.system.RequestParseUtil;
import com.bjpowernode.usermanage.web.form.UserForm;

@SuppressWarnings("serial")
public class UserManageAction extends BaseAction {

	@Override
	public void addUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path=null;
		String message=null;
		try{
			UserForm userForm = new UserForm();
			RequestParseUtil.requestDataToFormBean(request, userForm);
			System.out.println(userForm.getPassword());
			Map<?, ?> dto=RequestParseUtil.makeFormToDTO(userForm, false);
			System.out.println(dto.size());
			UserService userService = new UserServiceImpl();
			userService.addUser(dto);
			path="/jsp/success.jsp";
		}catch(Exception e){
			message=e.getMessage();
			path="/jsp/error.jsp";
			System.out.println(e.getMessage());
		}
		request.setAttribute("message", message);
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	@Override
	public void deleteUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void isUser(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uname = request.getParameter("uname");
		UserService userService = new UserServiceImpl();
		boolean isUser = userService.isLoginUser(uname);
		response.setCharacterEncoding("utf-8");
		if (isUser)
			request.setAttribute("message", "此账号已经存在,请重新输入!");
		// response.getWriter().print("此账号已经存在,请重新输入!");
		else
			// response.getWriter().print("此账号可以使用!");
			request.setAttribute("message", "此账号可以使用!");
		request.getRequestDispatcher("/jsp/person_manage/checkuser.jsp")
				.forward(request, response);
		
	}

	@Override
	public void selectDep(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectUserByPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
}

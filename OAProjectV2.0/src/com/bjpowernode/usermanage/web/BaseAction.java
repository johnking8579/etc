package com.bjpowernode.usermanage.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public abstract class BaseAction extends HttpServlet {

	private static final String M_ADD = "addUser";
	private static final String M_SELECTDEP = "selectdep";
	private static final String M_SELECTUSERBYPAGE = "selectuserbypage";
	private static final String M_UPDATEUSER = "updateuser";
	private static final String M_DELETEUSER = "deleteteuser";
	private static final String M_ISUSER = "isuser";

	@Override
	protected void service(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
		final String action = request.getParameter("action");
		if (action.equals(M_ADD)) {
			addUser(request, response);
		}else if(action.equals(M_SELECTDEP)){
			selectDep(request,response);
		}else if(action.equals(M_SELECTUSERBYPAGE)){
			selectUserByPage(request,response);
		}else if(action.equals(M_UPDATEUSER)){
			updateUser(request,response);
		}else if(action.equals(M_DELETEUSER)){
			deleteUser(request,response);
		}else if(action.equals(M_ISUSER)){
			isUser(request,response);
		}
	}

	public abstract void addUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	public abstract void selectDep(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	public abstract void selectUserByPage(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	public abstract void updateUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	public abstract void deleteUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
	public abstract void isUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;
}

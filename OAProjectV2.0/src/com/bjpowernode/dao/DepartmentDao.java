package com.bjpowernode.dao;

import java.util.List;

import com.bjpowernode.bean.Department;

public interface DepartmentDao {
	//获取部门信息
	public List<Department> getAllDep();
	//新增部门
	public void addDep(Department dep);
	
	//得到部门总数
	public int getDepCount(String dname);
	
	//得到分页后的部门
	public List<?> getDepByPage(int start,int unit,String dname);
	
	//根据部门id查询数据
	public Department getDepById(int id);
	
	//根据部门id修改数据
	public void updateDep(Department dep);
	
	//根据部门id删除数据
	public void deleteDep(String args[]);
}

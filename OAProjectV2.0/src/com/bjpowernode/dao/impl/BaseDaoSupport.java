package com.bjpowernode.dao.impl;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.bjpowernode.dao.conn.DBUtils;
import com.bjpowernode.system.ApplicationException;

public class BaseDaoSupport {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	protected Connection getConn() {
		return DBUtils.getConnection();
	}

	protected void closeAll() {
		DBUtils.close(rs);
		DBUtils.close(pstm);
		//DBUtils.close();
	}

	private void setValues(final PreparedStatement pstm, final Object... obj)
			throws SQLException {
		for (int i = 0; i < obj.length; i++) {
			pstm.setObject(i + 1, obj[i]);
		}
	}

	protected Object queryForObject(String sql, Object... obj) {
		Object o = null;
		try {
			conn = getConn();
			pstm = conn.prepareStatement(sql);
			setValues(pstm, obj);
			rs = pstm.executeQuery();
			while (rs.next())
				o = rs.getObject(1);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("数据库操作出现异常!");
		} finally {
			closeAll();
		}
		return o;
	}

	protected void update(String sql, Object... obj) {
		try {
			conn = getConn();
			pstm = conn.prepareStatement(sql);
			setValues(pstm, obj);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("数据库操作出现异常!");
		} finally {
			closeAll();
		}
	}

	protected void insert(String sql, Object... obj) {
		update(sql, obj);
	}
	
	protected void delete(String sql, Object... obj) {
		update(sql, obj);
	}
	

	private String[] getColNames(ResultSetMetaData rsmd) throws SQLException {
		// 得到总列数
		int count = rsmd.getColumnCount();
		String[] cols = new String[count];
		for (int i = 0; i < count; i++) {
			cols[i] = rsmd.getColumnLabel(i + 1);
		}
		return cols;
	}

	protected Object queryForObject(Class<?> c, String sql, Object... obj) {
		List<Object> list = queryForList(c, sql, obj);
		if (list.size() > 0)
			return list.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	protected List<Object> queryForList(Class<?> c, String sql, Object... obj) {
		List<Object> list = new ArrayList<Object>();
		try {
			conn = getConn();
			pstm = conn.prepareStatement(sql);
			setValues(pstm, obj);
			rs = pstm.executeQuery();

			// 获取结果集的所有的列
			ResultSetMetaData rsmd = rs.getMetaData();
			String[] cols = getColNames(rsmd);
			while (rs.next()) {
				// 实例化参数c
				Object o = c.newInstance();
				for (int i = 0; i < cols.length; i++) {
					// 得到目标对象当中所有的方法
					Method[] methods = c.getDeclaredMethods();
					for (Method m : methods) {
						if (m.getName().toLowerCase().equalsIgnoreCase(
								"set" + cols[i])) {
							// 对带有指定参数的指定对象调用由此 Method 对象表示的底层方法。
							// o.setid(rs.getObject(i+1))=====m.invoke(o,
							// rs.getObject(i+1))
							Class[] paramClass = m.getParameterTypes();
							if (paramClass[0] == Timestamp.class) {
								// m.invoke(o,
								// OAUtils.dateToTime(rs.getObject(i+1)));
								m.invoke(o, rs.getTimestamp(i + 1));
							} else
								m.invoke(o, rs.getObject(i + 1));
							break;
						}
					}
				}
				list.add(o);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("数据库操作出现异常!");
		} finally {
			closeAll();
		}
		return list;
	}

	public List<Map<Object, Object>> queryForListByMap(String sql,
			Object... obj) {
		List<Map<Object, Object>> list = new ArrayList<Map<Object, Object>>();
		try {
			conn = getConn();
			pstm = conn.prepareStatement(sql);
			this.setValues(pstm, obj);
			rs = pstm.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			String[] cols = getColNames(rsmd);
			while (rs.next()) {
				Map<Object, Object> map = new HashMap<Object, Object>();
				for (int i = 0; i < cols.length; i++) {
					map.put(cols[i], rs.getObject(i + 1));
				}
				list.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("数据库操作出现异常!");
		} finally {
			closeAll();
		}
		return list;
	}
}

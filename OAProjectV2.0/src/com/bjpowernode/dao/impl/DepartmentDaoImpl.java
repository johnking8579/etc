package com.bjpowernode.dao.impl;

import java.util.List;

import com.bjpowernode.bean.Department;
import com.bjpowernode.dao.DepartmentDao;
import com.bjpowernode.system.OAUtils;

public class DepartmentDaoImpl extends BaseDaoSupport implements DepartmentDao {

	@SuppressWarnings("unchecked")
	public List<Department> getAllDep() {
		String sql="select id, depname from department";
		List list=queryForList(Department.class, sql);
		return list;
	}
	
	//新增部门
	public void addDep(Department dep){
		String sql="insert into department values(req_oa.nextval,?,?,?,?,?)";
		insert(sql, dep.getDepname(),dep.getPid(),dep.getEmail(),dep.getPhone(),dep.getContent());
	}
	
	//得到部门总数
	public int getDepCount(String dname){
		StringBuilder sql=new StringBuilder("select count(*) from department");
		if(!"".equals(dname))
			sql.append("  where depname like '%"+dname+"%'");
		return OAUtils.bigDecimalToInt(queryForObject(sql.toString())) ;
	}
	
	//得到分页后的部门
	@SuppressWarnings("unchecked")
	public List getDepByPage(int start,int unit,String dname){
		StringBuilder sql=new StringBuilder()
				.append("select d1.id id,d1.depname dname1,d1.email email,d1.phone phone,d2.depname dname2")
				.append("   from department d1,department d2")
				.append("   where d1.pid=d2.id")
				//.append("   order by d1.id desc")
		;
		if(!"".equals(dname))
			sql.append(" and d1.depname like '%"+dname+"%'");
		String sql2[] = sql.toString().split("from");
		StringBuilder sql3=new StringBuilder()
				.append("select t.* from (")
				.append(sql2[0]+",ROWNUM AS R from"+ sql2[1])
				.append(") t")
				.append("  where t.r>"+start)
				.append("  and t.r<="+(start+unit));
		;
		List list=queryForListByMap(sql3.toString());
		return list;
	}
	
	public Department getDepById(int id){
		String sql="select * from department where id=?";
		return (Department)queryForObject(Department.class, sql, id);
	}
	
	//根据部门id修改数据
	public void updateDep(Department dep){
		String sql="update department set depname=?,email=?,phone=?,content=?,pid=? where id=?";
		update(sql, dep.getDepname(),dep.getEmail(),dep.getPhone(),dep.getContent(),dep.getPid(),dep.getId());
	}
	
	public void deleteDep(String args[]){
		String sql="delete from department where id in(?)";
		delete(OAUtils.sqlConverter(sql, args), args);
	}
}

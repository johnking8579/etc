package com.i8app.shop.web.action;

import static org.easymock.EasyMock.*;

import java.util.Date;

import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import com.i8app.shop.domain.AppActive;
import com.i8app.shop.service.SimpleQueryService;
import com.opensymphony.xwork2.ActionProxy;

public class PrintActionTest extends StrutsSpringTestCase{
	
	@Test
	public void testAppActive() throws Exception	{
		request.setParameter("info", "50001,111;50003,222;");
		ActionProxy proxy = getActionProxy("/print/appActive");
		PrintAction action = (PrintAction)proxy.getAction();

		SimpleQueryService mockService = createMock(SimpleQueryService.class);
		expect(mockService.findAppActive(50001, 111)).andReturn(new AppActive(50001, 111, 0));
		expect(mockService.findAppActive(50003, 222)).andReturn(new AppActive(50003, 222, 1));
		replay(mockService);
		
		action.setSimpleQueryService(mockService);
		proxy.execute();
		verify(mockService);
		assertEquals("50001,111,0;50003,222,1;", response.getContentAsString());
	}
	
	@Test
	public void testInstantMsg() throws Exception	{
		request.setParameter("from", "1357474982");
		request.setParameter("areaCode", "1");
		//request.setParameter("count", "5");
		ActionProxy proxy = getActionProxy("/print/instantMsg");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	
	
}

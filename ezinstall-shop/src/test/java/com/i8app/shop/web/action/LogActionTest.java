package com.i8app.shop.web.action;


import java.util.Date;

import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import com.i8app.shop.common.TripleDES;
import com.opensymphony.xwork2.ActionProxy;

public class LogActionTest extends StrutsSpringTestCase	{
	
	@Test
	public void testSoftLog() throws Exception	{
		request.setParameter("source", "0");
		request.setParameter("softId", "123");
		request.setParameter("osType", "200");
		request.setParameter("typeName", "型号#");
		request.setParameter("phoneNumber", "手机号");
		request.setParameter("installType", "1");
		request.setParameter("name", "软件名字");
		request.setParameter("manuName", "厂商名");
		request.setParameter("typeNameNew", "新机型名");
		request.setParameter("osVersion", "os型号");
		request.setParameter("optStatus", "1");
		request.setParameter("psn", "PSNPSNPSN");
		request.setParameter("versionId", "11");
		request.setParameter("imsi", "IMSI号");
		request.setParameter("phoneIMEI", "IMEI号");
		request.setParameter("typeId", "0");
		request.setParameter("empId", "30");
		request.setParameter("hallId", "1");
		request.setParameter("empNo", "empnonono");
		request.setParameter("isOtherNet", "1");
		request.setParameter("isSmsVal", "1");
		request.setParameter("isGuide", "1");
		request.setParameter("isAutoOpen", "1");
		request.setParameter("rscVersion", "rscVersion==");
		request.setParameter("seqNo", "seqnoseqno");
		ActionProxy proxy = getActionProxy("/log/softInstallLog");
		proxy.execute();
		assertEquals("1", response.getContentAsString());
	}
	
	@Test
	public void testGameLog() throws Exception	{
		request.setParameter("source", "0");
		request.setParameter("gameId", "123");
		request.setParameter("osType", "200");
		request.setParameter("typeName", "型号#");
		request.setParameter("phoneNumber", "手机号");
		request.setParameter("installType", "1");
		request.setParameter("name", "游戏名##");
		request.setParameter("manuName", "厂商名");
		request.setParameter("typeNameNew", "新机型名");
		request.setParameter("osVersion", "os型号");
		request.setParameter("optStatus", "1");
		request.setParameter("psn", "psnpsnpsn");
		request.setParameter("versionId", "11");
		request.setParameter("imsi", "IMSIIMSI");
		request.setParameter("phoneIMEI", "IMEIIMEI");
		request.setParameter("typeId", "0");
		request.setParameter("empId", "30");
		request.setParameter("hallId", "1");
		request.setParameter("empNo", "empnononO");
		request.setParameter("isOtherNet", "1");
		request.setParameter("isSmsVal", "1");
		request.setParameter("isGuide", "1");
		request.setParameter("isAutoOpen", "1");
		request.setParameter("rscVersion", "rscVersion==");
		request.setParameter("seqNo", "seqnoseqno");
		
		ActionProxy proxy = getActionProxy("/log/gameInstallLog");
		proxy.execute();
		assertEquals("1", response.getContentAsString());
	}
	
	@Test
	public void testEmpLogin() throws Exception	{
		request.setParameter("areaCode", "5");
		request.setParameter("empNo", "jn01-001");
		request.setParameter("empPwd", "123456");
		request.setParameter("psn", "psnpsnpsn");
		
		ActionProxy proxy = getActionProxy("/log/empLogin");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	@Test
	public void testEmpLogin2() throws Exception	{
		request.setParameter("areaCode", "6");
		request.setParameter("empNo", "GZLW0284");
		request.setParameter("empPwd", "123456");
		request.setParameter("psn", "psnpsnpsn");
		
		ActionProxy proxy = getActionProxy("/log/empLogin2");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	@Test
	public void testEmpLogout() throws Exception	{
		ActionProxy proxy = getActionProxy("/log/empLogout");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	@Test
	public void testUpwork() throws Exception	{
//		System.out.println(new Date().getTime());
		String param = "imei=xxx&imsi=xxx&mac=xxx&modelid=xxx&os=xxx&osver=xxx&phoneno=xxx&uuid=xxx" +
				"&installtime=1373350738039&appname=xxx&appid=123&installtype=1&provid=20&chanid=123&empno=123";
		byte[] content = TripleDES.encrypt(param.getBytes("utf-8"), "aaabbbccc");
		request.setMethod("POST");
		request.setContent(content);
		ActionProxy proxy = getActionProxy("/log/upwork");
		proxy.execute();
	}
	
	
}

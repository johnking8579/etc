package com.i8app.shop.web.action;

import org.apache.struts2.StrutsSpringTestCase;
import org.junit.Test;

import com.opensymphony.xwork2.ActionProxy;

public class Woplus2ActionTest extends StrutsSpringTestCase	{
	
	@Test
	public void testGetAppVersion() throws Exception	{
		request.setParameter("id", "200001001201209220012160464");
		ActionProxy proxy = getActionProxy("/woplus/getAppVersion");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}

}

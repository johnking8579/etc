package com.i8app.shop.service;

import static org.easymock.EasyMock.*;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.i8app.shop.common.BaseTest;
import com.i8app.shop.common.Util;
import com.i8app.shop.dao.EmployeeDao;
import com.i8app.shop.domain.Employee;

public class LoginSecurityServiceTest extends BaseTest {
	
	@Resource
	private LoginSecurityService service;
	EmployeeDao mock;
	
	@Before
	public void setUp()	{
		mock = createMock(EmployeeDao.class);
		service.setEmployeeDao(mock);
	}
	
	@Test
	public void testCheckEmpLogin1()	{
		Employee e = new Employee();
		e.setEmpPwd(Util.toMD5("123"));
		e.setOrgLevel(4);
		
		expect(mock.findByNo("111")).andReturn(e);
		replay(mock);
		
		int i = service.checkEmpLogin("15", "111", "123");
		Assert.assertEquals(1, i);
	}
	
	public void testCheckEmplogin4()	{
		Employee e = new Employee();
		e.setEmpPwd(Util.toMD5("123"));
		
		expect(mock.findByNo("111")).andReturn(e);
		replay(mock);
		
		int i = service.checkEmpLogin("15", "111", "123");
		Assert.assertEquals(4, i);
	}
	
	@Test
	public void testCheckEmplogin5()	{
		Employee e = new Employee();
		e.setEmpPwd(Util.toMD5("123"));
		e.setOrgLevel(4);
		e.setIsDeleted(1);
		
		expect(mock.findByNo("111")).andReturn(e);
		replay(mock);
		
		int i = service.checkEmpLogin("15", "111", "123");
		Assert.assertEquals(5, i);
	}

}

package com.i8app.shop.service;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.easymock.EasyMock;
import org.junit.Test;

import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.shop.common.BaseTest;
import com.i8app.shop.common.wsclient.SmsSender;

public class SmsHessianServiceTest extends BaseTest	{
	
	@Resource
	private SmsHessianService service;
	@Resource
	private SmsSender sender;
	
	@Test
	public void test()	{
		service.sendAuthcode("13112345678", "2");
	}
	
	/**
	 * void sendAppUrl(String areaId, String phoneNumber, String appName, String appUrl)
	 * @throws Exception 
	 * @throws GatewayAccessException 
	 * @throws UnsupportAreaException 
	 */
	@Test
	//@Rollback(false)
	public void test2() throws  Exception	{
		SmsSender mock = EasyMock.createMock(SmsSender.class);
		mock.sendDownloadLink("area", "131", "xxx", "url");

		service.setSmsSender(mock);
		service.sendShortUrl(4, "18210598298", "area", 9905874, "adec6718-4b6c-4c01-b017-b2471146b7e7", null, null);
	}
	
	@Test
	//@Rollback(false)
	public void test3() throws CpServerAccessException	{
		service.sendShortUrl(4, "18210598298", "area", 9905874, "adec6718-4b6c-4c01-b017-b2471146b7e7", null, null);
	}

	@Test
	//@Rollback(false)
	public void testMockNewSendShortUrl() throws  Exception	{
		SmsSender mock = EasyMock.createMock(SmsSender.class);
		mock.sendDownloadLink("area", "131", "xxx", "url");
		
		service.setSmsSender(mock);
		service.sendShortUrl(4, "18210598298", "area", "adec6718-4b6c-4c01-b017-b2471146b7e7", 1);
	}
	
	@Test
	//@Rollback(false)
	public void testNewSendShortUrl() throws  Exception	{
		service.sendShortUrl(4, "18210598298", "area", "adec6718-4b6c-4c01-b017-b2471146b7e7", 1);
	}
	
	@Test
	public void testSend() throws Exception	{
		int count = 20;
		this.simpleJdbcTemplate.update("truncate sms_log");
		for(int i=0; i<count; i++)	{
			//Thread.sleep(200);
			sender.testSend("123", "11111111");
		}
		Thread.sleep(3000);
		assertEquals(count, simpleJdbcTemplate.queryForInt("SELECT COUNT(*) FROM SMS_LOG"));
	}

}

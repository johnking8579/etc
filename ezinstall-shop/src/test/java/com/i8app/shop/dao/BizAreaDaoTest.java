package com.i8app.shop.dao;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.shop.common.BaseTest;
import com.i8app.shop.domain.PkgGroup;

public class BizAreaDaoTest extends BaseTest {
	
	@Resource BizAreaDao dao;
	
	@Test
	public void test()	{
		AreaParam ap = new AreaParam("01", 1, 1);
		List<PkgGroup> list = dao.findByBizId(1, ap);
		for(PkgGroup pg : list)	{
			System.out.println(pg.getName());
		}
	}

}

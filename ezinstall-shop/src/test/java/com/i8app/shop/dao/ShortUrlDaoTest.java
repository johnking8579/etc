package com.i8app.shop.dao;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.shop.common.BaseTest;
import com.i8app.shop.domain.ShortUrl;

public class ShortUrlDaoTest extends BaseTest{
	
	@Resource ShortUrlDao dao;
	
	@Test
	public void test()	{
		ShortUrl s = new ShortUrl();
		s.setId("abcdef");
		s.setLongUrl("");
		dao.persist(s);
	}

}

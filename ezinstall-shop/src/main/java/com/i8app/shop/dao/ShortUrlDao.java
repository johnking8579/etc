package com.i8app.shop.dao;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.ShortUrl;

@Repository
public class ShortUrlDao extends BaseDao<ShortUrl>{
	
	private static Logger logger = Logger.getLogger(ShortUrlDao.class);
	
	public void persist(ShortUrl shortUrl)	{
		try {
			getHibernateTemplate().persist(shortUrl);
			getHibernateTemplate().flush();	//刷新session, 防止外围程序无法捕获异常
		} catch (DataIntegrityViolationException e) {
			logger.info("短链码已存在,跳过. " + shortUrl.getId());
		}
		
	}
	
	public ShortUrl findById(String id)	{
		return (ShortUrl)getHibernateTemplate().get(ShortUrl.class, id);
	}

}

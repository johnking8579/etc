package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.OperateLog;

@Repository
public class OperateLogDao extends BaseDao<OperateLog> {
	
	public OperateLog findById(int id)	{
		return getHibernateTemplate().get(OperateLog.class, id);
	}
	
	public List<OperateLog> list()	{
		return this.getHibernateTemplate().loadAll(OperateLog.class);
	}
	

}

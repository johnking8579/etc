package com.i8app.shop.dao;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.namespace.QName;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.MD5;

@Repository
public class RecommendDao extends BaseDao {
	
	static Logger log = Logger.getLogger(RecommendDao.class);

	private static final String 
//			baseurl = "http://open.17wo.cn/OpenApi", 			// 正式地址
			baseurl = "http://112.96.28.170:38055/BssService/services/dspService?wsdl", 	//测试地址
			pKey = "c6d2979e2dee86b0";
	
	private MD5 md5 = new MD5();

	
	public String recommendList(String Msisdn, String servType) throws Exception {
		
		// 加密方式：将接口参数需要加密的部分，按tm+Msisdn+pkey进行MD5加密，采取32位大写
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String tm = sdf.format(new Date());
		String code = tm + Msisdn + pKey ;
		code = md5.toMD5(code); // 通过MD5 加密得到code
		
		//消息头（包括操作员ID、密码）用于鉴权（此处密码是对运营平台提供的密码进行MD5加密，采取32大写）,两个值之间以逗号分隔
		String rquestHeader = "hycx," + md5.toMD5("bss_hycx_2578");
		int type = 10;
		String paramStr = rquestHeader+"|"+Msisdn+"|"+ servType +"|0000|"+type+"||"+pKey+"|"+tm+"|"+code;
		
		String returnStr = null;
		Service service = new Service();
		Call call = (Call) service.createCall();
		call.setTimeout(10000);	//设置10秒超时
		call.setTargetEndpointAddress(new java.net.URL(baseurl));
		call.setOperationName(new QName(baseurl,"getIndividRemcommendInfo"));
		Object[] sss = new Object[]{paramStr};
		returnStr = (String) call.invoke(sss);
		return returnStr;
	}
}

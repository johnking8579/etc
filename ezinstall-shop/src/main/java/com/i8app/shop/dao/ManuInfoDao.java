package com.i8app.shop.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Pager;
import com.i8app.shop.domain.ManuInfo;

@Repository
public class ManuInfoDao extends BaseDao{

	/**
	 * 按orderId, 厂商名字升序排列
	 * @return
	 */
	public List<ManuInfo> list()	{
		String hql = "from ManuInfo order by orderId, manuName";
		return this.getHibernateTemplate().find(hql);
	}
	
	/**
	 * 分页查询
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<ManuInfo> list(int offset, int pageSize)		{
		String hql = "from ManuInfo order by orderId, manuName";
		return this.queryPager(hql, offset, pageSize);
	}
}

package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.ClientExcept;

@Repository
public class ClientExceptDao extends BaseDao	{
	
	public void persist(ClientExcept clientExcept)	{
		getHibernateTemplate().persist(clientExcept);
	}

}

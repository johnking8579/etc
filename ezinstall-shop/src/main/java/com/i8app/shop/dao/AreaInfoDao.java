package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Areainfo;

@Repository
public class AreaInfoDao extends BaseDao {
	
	public List<Areainfo> listAreaInfo(){
		String hql = "from Areainfo where isSpecified = 0";
		return hibernateTemplate.find(hql);
	}
	
	public Areainfo findById(String areaCode)	{
		return hibernateTemplate.get(Areainfo.class, areaCode);
	}
	
	public List<Areainfo> findByMemo(String memo)	{
		String hql = "from Areainfo where memo = ?";
		return hibernateTemplate.find(hql, memo);
	}
}

package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.City;

@Repository
public class CityDao extends BaseDao {
	
	public City findById(int id) {
		return this.getHibernateTemplate().get(City.class, id);
	}

}

package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Mobile;

@Repository
public class MobileDao extends BaseDao{

	public void persist(Mobile mobile)	{
		getHibernateTemplate().persist(mobile);
	}
}

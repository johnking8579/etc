package com.i8app.shop.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Pager;
import com.i8app.shop.domain.TypeInfo;

/**
 * TYPEINFO的查询. JAVA和SYMBIAN的查询方法在这里
 * @author jing
 *
 */
@Repository
public class TypeInfoDao extends BaseDao	{
	
	public TypeInfo findById(int id)	{
		return getHibernateTemplate().get(TypeInfo.class, id);
	}
	
	/**
	 * 根据 manuName、modelName 查询对应的压缩图片地址
	 * 匹配规则：
	 * 1.厂商和机型各自进行双向匹配
	 * 2.机型对厂商及型号进行双向匹配
	 * 
	 * @return
	 */
	public TypeInfo findByTemp(String manuName, String typeName, String osType){
		/**
		 *  from TypeInfo t 
		 *   where (( LOWER(t.beizhu) LIKE CONCAT('%',typename,'%') OR LOWER(typename) LIKE CONCAT('%', t.beizhu, '%'))  //机型双向匹配
		 *   and (LOWER(t.manuInfo.manuName) LIKE CONCAT('%',manuName,'%') OR LOWER(manuName) LIKE CONCAT('%',t.manuInfo.manuName,'%')))  //厂商双向匹配
		 *   OR (LOWER(typename) LIKE CONCAT('%', t.beizhu, '%') and LOWER(typename) LIKE CONCAT('%',t.manuInfo.manuName,'%'))  //机型参数包含机型 并且 机型参数包含厂商
		 */
		String hql = "from TypeInfo t " +
				" where (( LOWER(t.beizhu) LIKE CONCAT('%',?,'%') OR LOWER(?) LIKE CONCAT('%', t.beizhu, '%')) " +
				" and (LOWER(t.manuInfo.manuName) LIKE CONCAT('%',?,'%') OR LOWER(?) LIKE CONCAT('%',t.manuInfo.manuName,'%'))) " +
				" OR (LOWER(?) LIKE CONCAT('%', t.beizhu, '%') and LOWER(?) LIKE CONCAT('%',t.manuInfo.manuName,'%')) ";
		List<TypeInfo> list = this.getHibernateTemplate().find(hql, typeName, typeName, manuName, manuName, typeName, typeName);
		if(list.isEmpty()){
			if(osType == null && !"".equals(osType)){
				osType = "200";
			}
			//根据机型双向匹配
			hql = " from TypeInfo t " +
					" where ((( LOWER(t.beizhu) LIKE CONCAT('%',?,'%') OR LOWER(?) LIKE CONCAT('%', t.beizhu, '%')) " +
					" OR (LOWER(t.typeName) LIKE CONCAT('%',?,'%') OR LOWER(?) LIKE CONCAT('%',t.typeName,'%')))) and t.osInfo.osType = ? ";
			list = this.getHibernateTemplate().find(hql, typeName, typeName, typeName, typeName, Integer.parseInt(osType));
			if(list == null || list.isEmpty()){
				return null;
			}else{
				return list.get(0);
			}
		}else{
			return list.get(0);
		}
	}
	
	/**
	 * 根据ManuId进行查询, 按首字母升序查询
	 * @param manuId
	 * @return
	 */
	public List<TypeInfo> findByManuId(int manuId)	{
		String hql = "from TypeInfo t where t.manuInfo.id = ? order by t.firstLetter,typeName";
		return this.getHibernateTemplate().find(hql, manuId);
	}
	
	/**
	 * 根据首字母和厂商ID进行查询, 如果首字母不存在, 则查询任何首字母
	 * @param initial
	 * @param manuId
	 * @return
	 */
	public List<TypeInfo> findByInitial(String initial, Integer manuId)	{
		String hql;
		if(initial == null || "".equals(initial))	{
			hql = "from TypeInfo t where t.manuInfo.id = ? order by t.firstLetter, t.typeName";
			return this.getHibernateTemplate().find(hql, manuId);
		} else	{
			hql = "from TypeInfo where firstLetter = upper(?) and manuInfo.id = ? order by firstLetter, typeName";
			return this.getHibernateTemplate().find(hql, initial, manuId);
		}
	}

	/**
	 * 根据首字母和厂商ID进行查询, 如果首字母不存在, 则查询任何首字母
	 * @param initial
	 * @param manuId
	 * @return
	 */
	public Pager<TypeInfo> findByInitial(String initial, Integer manuId, int offset, int pageSize)	{
		String hql;
		if(initial == null || "".equals(initial))	{
			hql = "from TypeInfo where manuInfo.id = ? order by firstLetter, typeName";
			return this.queryPager(hql, new Object[]{manuId}, offset, pageSize);
		} else	{
			hql = "from TypeInfo where firstLetter = upper(?) and manuInfo.id = ? order by firstLetter, typeName";
			return this.queryPager(hql, new Object[]{initial, manuId}, offset, pageSize);
		}
	}
	
	/**
	 * 根据manuId和firstLetter查询,
	 * 条件1: compressPic不为空
	 * 条件2: 只查第一个
	 * @param initial
	 * @param manuId
	 * @return
	 */
	public TypeInfo findFirstByInitial(String initial, Integer manuId)	{
		String hql = "from TypeInfo where firstLetter = ? and manuId = ? and compressPic is not null and length(compressPic) > 0";
		List<TypeInfo> list = this.getHibernateTemplate().find(hql, initial, manuId);
		if(list.isEmpty()) return null;
		else return list.get(0);
	}
	
	/**
	 * Symbian的查询规则,根据MANUName和Typename参数查询TYPEINFO表的ID
	 * select id from TypeInfo t 
	 *  where( lower(t.beizhu) like concat('%',typename,'%') //typeName和备注字段, 双向模糊匹配
	 *  	or lower(typename) like concat('%', t.beizhu, '%'))
	 *  and t.manuInfo.manuName = ?		
	 *  order by t.osInfo.osType asc;
	 * @param typeName
	 * @param manuName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> list4Symbian(String typeName, String manuName)	{
		String hql = "select id from TypeInfo where (lower(beizhu) like ? or lower(?) like concat('%', beizhu, '%')) " +
				"and manuInfo.manuName = ? order by osInfo.osType";
		return this.getHibernateTemplate().find(hql, "%" + typeName + "%", typeName, manuName);
	}
	
	/**
	 * JAVA手机的查询规则:
	 * TypeInfo.typeName必须和传入的参数typeName精确匹配
	 * JAVA的排在前面
	 * 不需要限制osInfo.osType为500
	 * @param map
	 * @return
	 */
	public List<Integer> list4Java(String typeName, String manuName) {
		String hql = "select id from TypeInfo where typeName = ? and manuInfo.manuName = ? order by osInfo.osType desc";
		return this.getHibernateTemplate().find(hql, typeName, manuName);
	}
	
	
	/**
	 * 按hotorder升序, 分页查询
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<TypeInfo> list(int offset, int pageSize)	{
		String hql = "from TypeInfo order by hotOrder";
		return this.queryPager(hql, offset, pageSize);
	}
	
	/**
	 * 搜索. 模糊匹配typeName和manuName
	 * @param keyword
	 * @return
	 */
	public Pager<TypeInfo> search(String keyword, int offset, int pageSize)	{
		String hql = "from TypeInfo where typeName like ? or manuInfo.manuName like ? order by hotOrder";
		return this.queryPager(hql, new Object[]{ "%" + keyword + "%" , "%" + keyword + "%"}, offset, pageSize);
	}
	
	/**
	 * 查询某厂商所有机型名称的首字母集合
	 * @param manuId
	 * @return
	 */
	public List<String> findInitial(Integer manuId)	{
		String hql = "select distinct firstLetter from TypeInfo where manuId = ? order by firstLetter";
		return this.getHibernateTemplate().find(hql, manuId);
	}
	
	/**
	 * 查询某厂商所有机型名称的首字母集合
	 * 包含了distinct和分页,所以不能使用baseDao的分页
	 * @param manuId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Pager<String> findInitial(final int manuId, final int offset, final int pageSize)	{
		final String hql = "select distinct firstLetter from TypeInfo where manuInfo.id = ? order by firstLetter";
		List<String> list = this.getHibernateTemplate().executeFind(new HibernateCallback(){

			@Override
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery(hql).setParameter(0, manuId)
						.setFirstResult(offset).setMaxResults(pageSize).list();
			}
			
		});
		
		String countHql = "select count(distinct firstLetter) from TypeInfo where manuInfo.id = ? order by firstLetter";
		Long count = (Long)this.getHibernateTemplate().find(countHql, manuId).get(0);
		return new Pager(list, count.intValue());
	}
	
	
}
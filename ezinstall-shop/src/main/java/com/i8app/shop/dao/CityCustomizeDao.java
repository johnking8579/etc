package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.CityCustomize;

@Repository
public class CityCustomizeDao extends BaseDao	{
	
	public List<CityCustomize> findByCityId(int cityId, int operator)	{
		String hql = "from CityCustomize where cityId = ? and operator = ? ";
		return this.getHibernateTemplate().find(hql, new Object[]{cityId, operator});
	}

}

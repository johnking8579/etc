package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.ClientDeployInfo;

@Repository
public class ClientDeployInfoDao extends BaseDao	{
	
	public ClientDeployInfo findByPsn(String psn)	{
		String hql = "from ClientDeployInfo where psn = ?";
		List<ClientDeployInfo> list = this.getHibernateTemplate().find(hql, psn);
		if(list.isEmpty())
			return null;
		else
			return list.get(0);
	}
	

}

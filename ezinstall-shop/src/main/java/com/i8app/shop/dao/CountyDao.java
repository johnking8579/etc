package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.County;

@Repository
public class CountyDao extends BaseDao	{
	
	public County findById(int id)	{
		return this.getHibernateTemplate().get(County.class, id);
	}

}

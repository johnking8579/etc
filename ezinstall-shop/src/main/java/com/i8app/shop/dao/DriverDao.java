package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class DriverDao extends BaseDao{
	
	/**
	 * 根据传入的参数查询对应的filePath集合
	 * @param vid
	 * @param pid
	 * @param osType
	 * @return
	 */
	public List<String> list(String vid, String pid, int osType)	{
		String sql;
		List<String> strList;
		int specialVid = jdbcTemplate.queryForInt("SELECT COUNT(*) FROM DRIVERVID WHERE VID = ?", vid);
		if(specialVid > 0)	{
			sql = "SELECT FILEPATH FROM DRIVERINFO WHERE VID=? AND OSTYPE=?";
			strList = jdbcTemplate.queryForList(sql, String.class, vid, new Integer(osType));
		} else	{
			sql = "SELECT FILEPATH FROM DRIVERINFO WHERE VID=? AND PIDS LIKE ? AND OSTYPE=?";
			strList = jdbcTemplate.queryForList(sql, String.class, vid, "%" + pid + "%", new Integer(osType));
		}
		return strList;
	}
	
	/**
	 * 根据传入的参数查询对应的filePath唯一值
	 * @param vid
	 * @param pid
	 * @param osType
	 * @return
	 */
	public String getUniqueResult(String vid, String pid, int osType)	{
		if(list(vid, pid, osType).size() > 0)	
			return list(vid, pid, osType).get(0);
		return null;
	}
}

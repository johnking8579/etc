package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.shop.domain.PkgGroup;

@Repository
public class BizAreaDao extends BaseDao{
	
	/**
	 * SELECT PG.*, biz.id bizid, biz.name bizname, ba.deptid, ba.provid, ba.cityid FROM biz_area ba 
	JOIN biz biz on biz.id = ba.bizId
	LEFT JOIN PKG_GROUP PG ON PG.ID = BA.pkgGroupId 
	where biz.id = #bizId# AND PG.isDel = 0
	and(
		(deptid is null and provid is null and cityid is null)
	 	or (deptid = #deptId# and provid is null and cityid is null) 
		or (deptid = #deptId# and provid = #provId# and cityid is null)
		or(deptid = #deptId# and provid = #provId# and cityid = #cityId#)
	) group by ba.id order by ba.deptid, ba.provid, ba.cityid
	 * @param bizId
	 * @param areaParam
	 * @return
	 */
	public List<PkgGroup> findByBizId(int bizId, AreaParam areaParam)	{
		String hql = "select ba.pkgGroup from BizArea ba where ba.biz.id = ? and ba.pkgGroup.isDel = 0 and " +
				"(" +
				"(ba.deptId = null and ba.provId = null and ba.cityId = null) " +
				"or (ba.deptId = ? and ba.provId = null and ba.cityId = null) " +
				"or (ba.deptId = ? and ba.provId = ? and ba.cityId = null) " +
				"or (ba.deptId = ? and ba.provId = ? and ba.cityId = ?) " +
				") group by ba.id order by ba.deptId, ba.provId, ba.cityId";
		return getHibernateTemplate().find(hql, bizId, 
				areaParam.getDeptId(), 
				areaParam.getDeptId(), areaParam.getProvId(), 
				areaParam.getDeptId(), areaParam.getProvId(), areaParam.getCityId());
	}

}

package com.i8app.shop.dao;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Pcconfigfour;

@Repository
public class PcConfigFourDao extends BaseDao	{

	public Pcconfigfour getPcConfigFour(String areacode){
		return hibernateTemplate.get(Pcconfigfour.class, areacode);
	}
}

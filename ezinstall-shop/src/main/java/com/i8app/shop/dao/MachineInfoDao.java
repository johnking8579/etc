package com.i8app.shop.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.InstallPSN;
import com.i8app.shop.common.Util;
import com.i8app.shop.domain.Machineinfo;

/**
 * TODO 未测试
 * 
 * @author JingYing 2013-6-5
 */
@Repository
public class MachineInfoDao extends BaseDao {

	public String checkInstallPSN(String psnstr) {
		int id = 0;
		String machineCode = "";
		String sn = "";

		try {
			Machineinfo mac = InstallPSN.DecodingPSN(psnstr);
			String hql1 = "from Machineinfo where psn = ?";
			List<Machineinfo> list = hibernateTemplate.find(hql1, mac.getPsn());
			if (!list.isEmpty()) {
				Machineinfo m = list.get(0);
				id = m.getId();
				machineCode = m.getMachineCode();
				sn = m.getSn();
			} else {
				return "failure";
			}

			if (sn == null)
				sn = "";
			if (machineCode == null)
				machineCode = "";

			if (machineCode.trim().equals(mac.getMachineCode().trim())
					&& sn.trim().equals(mac.getSn().trim())) {
				String hql2 = "update Machineinfo set installNum = installNum+1 where psn = ? and id = ?";
				int result = hibernateTemplate.bulkUpdate(hql2, mac.getPsn(),
						id);
				if (result != 0) // TODO
					return sn;
			} else if (machineCode.trim().equals("") && sn.trim().equals("")) {
				String hql3 = "update Machineinfo set sn=?,machineCode=?,machineName=?,diskSerial=?,"
						+ "macAddr=?,iP=?,installNum = installNum+1,installTime=?  where psn = ? and id = ?";
				int result = hibernateTemplate.bulkUpdate(hql3, mac.getSn(),
						mac.getMachineCode(), mac.getDiskSerial(),
						mac.getMacAddr(), mac.getIp(), Util.getNOWTIME(),
						mac.getPsn(), id);
				if (result != 0) {
					return mac.getSn();
				}
			}
			return "failure";
		} catch (DataAccessException e) {
			e.printStackTrace();
			return "failure";
		}
	}
}

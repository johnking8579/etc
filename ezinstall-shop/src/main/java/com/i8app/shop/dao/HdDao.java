package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Pager;
import com.i8app.shop.domain.Hd;

@Repository
public class HdDao extends BaseDao {
	
	public Hd findById(String hd)	{
		return this.getHibernateTemplate().get(Hd.class, hd);
	}
	
	/**
	 * 根据手机号查询运营商, 查前7位
	 * @param phoneNumber
	 * @return
	 */
	public Integer findOperByNo7(String phoneNumber)	{
		//截取前7位, 从数据库中判断
		phoneNumber = phoneNumber.substring(0, 7);
		String hql = "select operator from Hd where hd = ?";
		List<String> strList = this.getHibernateTemplate().find(hql, phoneNumber);
		if(strList.isEmpty())
			return null;
		else
			return Integer.parseInt(strList.get(0));
	}
	
	/**
	 * 只查前3位
	 * @param phoneNumber
	 * @return 1联通, 2移动, 3电信
	 */
	@SuppressWarnings("unchecked")
	public Integer findOperByNum3(String phoneNumber)	{
		phoneNumber = phoneNumber.substring(0, 3);
		String hql = "select operator from Hd where hd like ?";
		Pager<Integer> pager = this.queryPager(hql, new Object[]{phoneNumber + "%"}, 0, 1);
		if(pager.getTotalCount() == 0)
			return null;
		else
			return pager.getList().get(0);
	}
	
}

package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class PkgInfoDao extends BaseDao	{

	public List<String> findByPkgId(int pkgId)	{
		String hql = "select appId from PkgInfo where pkgId = ? order by num desc";
		return getHibernateTemplate().find(hql, pkgId);
	}
}

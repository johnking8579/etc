package com.i8app.shop.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.i8app.shop.domain.Announcement;

@Repository
public class AnnounceDao extends BaseDao<Announcement> {
	
	/**
	 * 根据AreaCode查找已发布的公告, 按公告时间降序排
	 * @param areaCode
	 * @return
	 */
	public List<Announcement> findByAreaCode(String areaCode)	{
		String hql = "from Announcement where areaCode =? and hasAnnounced =1 order by uploadTime desc";
		return this.getHibernateTemplate().find(hql, areaCode);
	}

}

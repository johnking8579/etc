package com.i8app.shop.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import com.i8app.shop.common.Util;
import com.i8app.shop.domain.Applemailinfo;

@Repository
public class AppleMailInfoDao extends BaseDao	{
	
	public int updateApplemailInfoUseNumber(int appleid){
		String sql = "update applemailinfo set mail = '' where id = ?";
		int result;
		try {
			result = simpleJdbcTemplate.update(sql, appleid);
			if(result == 1) {
				System.out.println(Util.getNOWTIME()+"applemail清空成功");
			} else {
				System.out.println(Util.getNOWTIME()+"apple清空异常");
			}
		} catch (DataAccessException e1) {
			e1.printStackTrace();
			return -1;
		}
		return result;
	}
	
	public Applemailinfo getAppleMailInfo(){
		String sql = "SELECT id,mac,guid,mail FROM applemailinfo WHERE mail <> '' ORDER BY RAND() LIMIT 1";
		return simpleJdbcTemplate.queryForObject(sql, new ParameterizedRowMapper<Applemailinfo>() {

			@Override
			public Applemailinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
				Applemailinfo applemail = new Applemailinfo();
				applemail.setId(rs.getInt(1));
				applemail.setMac(rs.getString(2));
				applemail.setGuid(rs.getString(3));
				applemail.setMail(rs.getString(4));
				return applemail;
			}
		});
		
	}
	
	public Applemailinfo getAppleMailInfo(final String mail){
		String sql1 = "SELECT id,mac,guid FROM applemailinfo where (mail = '' OR mail IS NULL) ORDER BY RAND() LIMIT 1";
		boolean flag = false;
		Applemailinfo applemail = (Applemailinfo) simpleJdbcTemplate.queryForObject(sql1, new ParameterizedRowMapper() {
			@Override
			public Applemailinfo mapRow(ResultSet rs, int rowNum) throws SQLException {
				Applemailinfo applemail = new Applemailinfo();
				applemail.setId(rs.getInt(1));
				applemail.setMac(rs.getString(2));
				applemail.setGuid(rs.getString(3));
				applemail.setMail(mail);
				return applemail;
			}
		});
		if(applemail != null)
			flag = true;
		
		if(flag){
			String sql2 = "update applemailinfo set mail = ? where id = ?";
			int result = simpleJdbcTemplate.update(sql2, mail, applemail.getId());
			if(result == 1) {
				System.out.println(Util.getNOWTIME()+"applemail使用更新成功");
			} else {
				System.out.println(Util.getNOWTIME()+"applemail使用更新异常");
			}
			return applemail;
		}
		return null;
	}

}

package com.i8app.shop.web.action;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.shop.common.DiskElement;
import com.i8app.shop.service.DiskElementFactory;

@Controller
@Scope("prototype")
public class DangjianAction extends BaseAction {

	private String filePath;
	private List<DiskElement> diskElementList;

	public String doc() {
		return SUCCESS;
	}

	public String docLeft() {
		return SUCCESS;
	}

	public String docMain() {
		diskElementList = DiskElementFactory.factory(filePath).getChildren();
		return SUCCESS;
	}

	/**
	 * 点击文档后, 向客户端展示下载信息
	 * 
	 * @return
	 */
	public String docDown() {
		DiskElement de = DiskElementFactory.factory(filePath);
		return doPrint(de.getName() + "," + de.toHttpPath() + ";");
	}

	/**
	 * 异步加载子目录或子文件信息. jquery.ztree需要一个JSON, 其中两个属性必需:name,isParent
	 * 
	 * @return
	 */
	public String asyncSubFolder() {
		DiskElement de = DiskElementFactory.factory(filePath);
		JsonArray array = new JsonArray();
		List<DiskElement> list = de.getChildren();
		if (list != null && !list.isEmpty()) {
			for (DiskElement d : list) {
				if(d.getChildren() != null)	{	//只显示子目录, 不显示子文件
					JsonObject json = new JsonObject();
					json.addProperty("name", d.getName());
					json.addProperty("filePath", d.getStr());
					json.addProperty("isParent", true);
					array.add(json);
				}
			}
		}
		return doPrint(array.toString());
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public List<DiskElement> getDiskElementList() {
		return diskElementList;
	}

	public void setDiskElementList(List<DiskElement> diskElementList) {
		this.diskElementList = diskElementList;
	}

}

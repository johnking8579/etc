 package com.i8app.shop.web.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants.OrderBy;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppParam;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.common.config.ConfigProp;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.common.wsclient.SmsSender;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.Clientconfigdate;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.PkgGroup;
import com.i8app.shop.domain.ResourceType;
import com.i8app.shop.domain.UnionreadPkgView;
import com.i8app.shop.service.AppService;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.ISmsService;
import com.i8app.shop.service.InstallLogService;
import com.i8app.shop.service.ResourceService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
public class WoshopAction extends BaseAction {
	@Resource
	private ResourceService resourceService;
	@Resource
	private AppService appService;
	@Resource
	private EmpService empService;
	@Resource
	private InstallLogService installLogService;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private ISmsService smsHessianService;
	@Resource
	private SmsSender smsSender;
	
	
	@Resource
	private Config_i8 config_i8;

	private List<String> charList;
	private Pager<AppDTO> appPager;
	private List<ResourceType> appTypeList;
	private Map<String, Pager<AppDTO>> appMap;
	private List<Pkg> pkgList;
	private List<AppDTO> appList;// 不分页的
	private Map<Pkg, List<AppDTO>> pkgMap = new HashMap<Pkg, List<AppDTO>>();//一键装机
	private Map<ResourceType, List<AppDTO>> typeMap = new LinkedHashMap<ResourceType, List<AppDTO>>();//应用宝库二级页面
	private List<AppInstallPack> appInstallPackList;
	
	private AppDTO app;
	private Pkg pkg;
	private ResourceType resType;
	

	private String installType = "1";
	private String packUuid, keyword, typeId, ids, version, phoneNo, imoduletype, bizType, pkgId, pkgName;
	private int empId, provId, appOsType, isIOS, smallId, bigId, pageIndex, pageSize2;//pageSize2 这个是用于一个方法无法满足2个分页功能而作的
	private boolean orderBy;
	private Date cpUpdateTime;

	
	
	/** =================================== 查询数据模块 ====================================== */
	
	/**
	 * 参数说明：
	 * @param empId（营业员id）
	 * @param isIOS 是否为正版下载地址 (0：非正版的；1：正版下载地址 (SoftUrl值，是CP_APPSTORE 的值))
	 * @param installType 安装方式 
	 * 		见pdm-installLog-installType取值
	 * 		( 安装方式：1:pc端直连;2:蓝牙下载;3:USB连接;4:pc客户端短信下载;
	 * 		5:二维码下载;6:web短信下载;7web直接下载;8:wap短 信下载;9:wap直接下载;
	 * 		10.手机客户端直接下载;11.手机客户端闪传;12.手机客户端短信下载;
	 * 		13.pc端直连(本地资源);14: 蓝牙下载(本地资源);15:USB连接(本地资源))
	 * 
	 * @param appOsType（操作系统）
	 * @param version（操作系统版本）
	 * @param pageSize （每页显示的应用个数）
	 * @param pageSize2 （每页显示的应用个数2）
	 * @param cpUpdateTime 查询三个月内的数据 -- 暂时放到这
	 * 
	 * @param pkgId pkg 的 id(主要是包选项页做切换的时候能用到，默认拿第一个包的软件)
	 * @param bizType 业务类别 （1：手机必备业务；8：地市业务；9：全国包业务）
	 * @param keyword 搜索用的关键字
	 * @param typeId （app类别：null 或者 101开头的为软件；102开头的为游戏。）
	 * @param orderBy 是否根据时间排序,只有为true才会做排序
	 * @param imoduletype 见 redmine 灵动一键装机系统 的 讨论区
	 * @param ids (包括：uuid_packUuid_discriminator)
	 * 
	 * @param provId（省份id）
	 * @param smallId（最小的配置类别id）
	 * @param bigId（最大的配置类别id）
	 * 
	 * cp商对应的业务：
	 * 		沃商店:  android+symbian = cp03. 其它cp09
			广东: android=cp04. 其它cp09.
			山西: 所有=cp09
	 */
	
	
	
	/**
	 * 沃商店首页
	 * 
	 * @param empId 
	 * @param isIOS 
	 * @param installType 
	 * @param appOsType
	 * @param version 
	 * @param pageSize
	 * 
	 * @return appPager and appList
	 */
	public String homeIndex() {

		Os os = this.adaptToOs(appOsType, version);
		Employee emp = empService.findById(empId);
		provId = emp.getProvince().getId();//接收一遍是为了方便页面上使用 provId
		
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		//根据各省不同的要求配置不同的业务
		List<String> bizArr = new ArrayList<String>();
		if(!Os.ANDROID.equals(os) && "1".equals(woStore_NATION_PKG)){//沃商店全国包：要求 非山西 并且 非android 并且 开关打开
			bizArr.add(AreaParam.BIZ_WOSTORE_NATION_PKG + "");
		}
		bizArr.add(AreaParam.BIZ_ESSENTIAL + "");
		bizArr.add(AreaParam.BIZ_CITY_PKG + "");
		
		pkgList = new ArrayList<Pkg>();
		
		if(provId == 44){//如果是上海的，先显示上海指定的包
			
			// 根据不同的业务获取不同的包组
			for (String s : bizArr) {
				bizType = s;
				List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(Integer.parseInt(bizType), ap);
				if (wostoreNationPkgList != null && wostoreNationPkgList.size() > 0) {
					for (PkgGroup pkgGroupDTO : wostoreNationPkgList) {
						if (pkgGroupDTO.getId() > 0) {
							List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupDTO.getId());// 获取该包组下所有的子包
							if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
								for (int n = 0; n < pkgDTOList.size(); n++) {
									Pkg p = pkgDTOList.get(n);
									p.setInfo(bizType);//占时用这个字段带业务类别到页面。
									pkgList.add(p);
									if(pkg == null && n == 0){
										pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
									}
								}
							}
						}
					}
				}
			}
			
			if(Os.ANDROID.equals(os)){//沃商店android系统的需要增入阅联的快捷安装包
				
				bizType = "bizYueLian";//先用该变量代替送到页面，方便页面做下载的时候做判断，从而来确定imoduletype的值！
				List<UnionreadPkgView> pkgViewlist = resourceService.findPkgViewByProvid(provId);
				
				/**将 pkgViewlist 转换成 pkgList*/
				for (UnionreadPkgView pkgView : pkgViewlist) {
					Pkg p = new Pkg();
					p.setId(Integer.parseInt(pkgView.getPkgId()));
					p.setName(pkgView.getPkgName());
					p.setIcon(pkgView.getIcon() != null ? getFileserverUrl() + pkgView.getIcon() : null);
					p.setInfo(bizType);
					
					if(pkg == null ){
						pkg = p;//页面初次加载的时候展示用的包
					}
					pkgList.add(p);
				}
			}
			
		}else{
			if(Os.ANDROID.equals(os)){//沃商店android系统的需要增入阅联的快捷安装包
				
				bizType = "bizYueLian";//先用该变量代替送到页面，方便页面做下载的时候做判断，从而来确定imoduletype的值！
				List<UnionreadPkgView> pkgViewlist = resourceService.findPkgViewByProvid(provId);
				
				/**将 pkgViewlist 转换成 pkgList*/
				for (UnionreadPkgView pkgView : pkgViewlist) {
					Pkg p = new Pkg();
					p.setId(Integer.parseInt(pkgView.getPkgId()));
					p.setName(pkgView.getPkgName());
					p.setIcon(pkgView.getIcon() != null ? getFileserverUrl() + pkgView.getIcon() : null);
					p.setInfo(bizType);
					
					if(pkg == null ){
						pkg = p;//页面初次加载的时候展示用的包
					}
					pkgList.add(p);
				}
			}
			
			// 根据不同的业务获取不同的包组
			for (String s : bizArr) {
				bizType = s;
				List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(Integer.parseInt(bizType), ap);
				if (wostoreNationPkgList != null && wostoreNationPkgList.size() > 0) {
					for (PkgGroup pkgGroupDTO : wostoreNationPkgList) {
						if (pkgGroupDTO.getId() > 0) {
							List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupDTO.getId());// 获取该包组下所有的子包
							if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
								for (int n = 0; n < pkgDTOList.size(); n++) {
									Pkg p = pkgDTOList.get(n);
									p.setInfo(bizType);//占时用这个字段带业务类别到页面。
									pkgList.add(p);
									if(pkg == null && n == 0){
										pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
									}
								}
							}
						}
					}
				}
			}
		}

		return SUCCESS;
	}

	/**
	 * 沃商店首页--根据pkg主键 以及 对应的业务 查询对应的软件列表
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param empId 
	 * @param pageSize 
	 * 
	 * @param pkgId 
	 * @param bizType 
	 * 
	 * @return appPager and appList
	 */
	public String findAppByPkg() {
		
		Employee emp = empService.findById(empId);
		provId = emp.getProvince().getId();
		
		Os os = this.adaptToOs(appOsType, version);
		if(Os.ANDROID.equals(os) && "bizYueLian".equals(bizType)){//沃商店的需要 对接阅联的快捷安装包
			List<UnionreadPkgView>  list = resourceService.findPackUuidByPkgId(provId, pkgId);
			appInstallPackList = new ArrayList<AppInstallPack>();
			for (UnionreadPkgView pkgView : list) {
				AppInstallPack pkgApp = resourceService.findAppPkgByid(pkgView.getPackUuid());
				if(pkgApp != null ){
					//阅联的标签需要根据省份和PackUuid再次查询app_marker表，获得这个对应的标签才是正确的。
					String markerType = resourceService.findMarkerType(provId, pkgView.getPackUuid());
					if(pkgApp.getApp() != null){
						pkgApp.getApp().setMarkerType(markerType);
					}
					appInstallPackList.add(pkgApp);
				}
			}
		}else{
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
			appPager = appQuerier.findInstallPackByPkg(ap, Integer.parseInt(pkgId), os, offset, pageSize); // 查找对应必备包的软件列表
		}
		return SUCCESS;
	}
	
	/**
	 * 软件宝库（先查应用宝库，不排序的；再查最新上架，排序的）
	 * @param appOsType
	 * @param version
	 * @param empId 
	 * @param pageSize 
	 * @param installType
	 * @param pageSize2
	 * 
	 * @return  appList
	 */
	public String appIndex(){
		
		//调用应用宝库
		if(typeId == null){
			typeId = "101";
		}
		int fristOffset = offset;
		int fristpageSize = pageSize;
		
		//调用最新上架 
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -3);    //得到前三个月   
		int year = calendar.get(Calendar.YEAR);   
		int month = calendar.get(Calendar.MONTH);   //注意月份这个值如果单独拿来使用，需要加一才能是我们正常的月份
		int today = Integer.parseInt(new SimpleDateFormat("dd").format(new Date()));
		calendar.set(year, month, today);
		Date lastThreeDate = calendar.getTime();
		cpUpdateTime = lastThreeDate;
		
		offset = 0;
		pageSize = pageSize2;// pageSize2 是特殊情况才会用的变量。
		appList = this.findAppPager(appOsType, version, keyword, typeId, true, cpUpdateTime, empId, false, pageSize).getList();
		offset = fristOffset;
		pageSize = fristpageSize;//查完之后，值各自归位
		
		return SUCCESS;
	}
	
	/**
	 * 软件宝库
	 * 根据类别分页查询app集合
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param empId
	 * @param pageSize 
	 * 
	 * @param keyword 
	 * @param typeId
	 * @param 
	 * 
	 * @return
	 */
	public String findAppByType(){
		
		//调用应用宝库
		if(typeId == null){
			typeId = "101";
		}
		appPager = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, false, pageSize);
		return SUCCESS;
	}

	/**
	 * 应用宝库--二级页面
	 * @return
	 */
	public String appMain(){
		
		return SUCCESS;
	}
	
	public String top(){
		
		return SUCCESS;
	}
	
	/** 
	 * 软件宝库--子类
	 * 根据软件的类别查询子类别
	 * @param typeId 
	 */
	public String right(){
		//调用应用宝库
		if(typeId == null){
			typeId = "101";
		}
		appTypeList = resourceService.appTypeList(typeId);
		return SUCCESS;
	}
	
	/**
	 * 软件宝库--子类
	 * 根据类别分页查询app集合
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param empId 
	 * @param pageSize
	 * 
	 * @param typeId 
	 * @param keyword
	 * 
	 * @return 
	*/
	public String left(){
		//调用应用宝库
		if(typeId == null || typeId.startsWith("101")){
			typeId = "101";
		}else if(typeId.startsWith("102")){
			typeId = "102";
		}
		//查出所有的子类别
		appTypeList = resourceService.appTypeList(typeId);
		int fristpageSize = pageSize;
		pageSize = Integer.parseInt(pageSize3);
		//调用应用宝库
		for (ResourceType resType : appTypeList) {
			orderBy = false;
			typeId = resType.getId();
			appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
			typeMap.put(resType, appPager.getList());
		}
		pageSize = fristpageSize;
		return SUCCESS;
	} 
	
	
	/**
	 * 根据二级类别查询对应的软件
	 * 
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param pageSize
	 * 
	 * @param typeId
	 * @param 
	 * 
	 * @return
	 */
	public String secondAppList(){
		if(typeId == null){
			typeId = "101";
		}
		resType = resourceService.findAppType(typeId);
		orderBy = false;
		appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
		return SUCCESS;
	}

	/**
	 * 下载排行 根据不同的必备包id查询对应的软件列表 
	 * @param appOsType
	 * @param version
	 * @param pageSize
	 * @param empId
	 * 
	 * @return pkgList、appMap
	 */
	public String rank() { 
		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam(); 
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);//运营商id
		
		List<PkgGroup> wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_RANK, ap); 
		if(wostorePkgList != null && wostorePkgList.size()>0){ 
			for (PkgGroup pkgGroupDTO : wostorePkgList) {
				pkgList = appService.findPkgByGroupId(pkgGroupDTO.getId());//获取该包组里的所有的包（里头包含了周排行、月排行、总排行） 
				appMap = new HashMap<String, Pager<AppDTO>>();
				if(!pkgList.isEmpty()){ 
					int index = 1;
					for (Pkg pkgDTO : pkgList){//如何获取对应的包 
				 		Os os = this.adaptToOs(appOsType, version); 
				 		List<String> cpIdList = new ArrayList<String>();
						//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
						if ( os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
							cpIdList.add(AreaParam.CP_WOSTORE);
						} else {// 游戏和软件都查出来
							cpIdList.add(AreaParam.CP_I8APP);
						}
						
				 		appPager = appQuerier.findInstallPackByPkg(ap, pkgDTO.getId(), os, offset, pageSize);//查找对应必备包的软件列表 
				 		if(index==1){
				 			appMap.put("w"+pkgDTO.getName(), appPager);//周排行
				 		}else if(index==2){
				 			appMap.put("m"+pkgDTO.getName(), appPager);//月排行
				 		}else if(index==3){
				 			appMap.put("t"+pkgDTO.getName(), appPager);//总排行
				 		}
				 		index++;
					}
				}
			}
		} 
		return SUCCESS; 
	 }
	
	/**
	 * 搜索页面
	 * @param appOsType 
	 * @param version 
	 * @param pageSize 
	 * 
	 * @param keyword 
	 * @param typeId 
	 * @param orderBy 
	 * 
	 * @param appPager
	 */
	public String search() {
		typeId = null;//主要是为了能搜出所有的软件
		appPager = this.findAppPager(appOsType, version, keyword, typeId, false, null, empId, true, pageSize);
		return SUCCESS;
	}
	
	/**
	 * 一键装机
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param pageSize
	 * 
	 * @param appPager
	 */
	public String yjzj_pkgList() {
		Os os = this.adaptToOs(appOsType, version);

		Employee emp = empService.findById(empId);
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id

		List<String> cpIdList = new ArrayList<String>();
		//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpIdList.add(AreaParam.CP_WOSTORE);
		} else {// 游戏和软件都查出来
			cpIdList.add(AreaParam.CP_I8APP);
		}
		
		// ================= 手机必备 =================
		List<PkgGroup> wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ESSENTIAL, ap);
		for (PkgGroup pg : wostorePkgList) {
			pkgList = appService.findPkgByGroupId(pg.getId());
			for (Pkg pkg : pkgList) {
				appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize);// 查找对应必备包的软件列表
				pkgMap.put(pkg, appPager.getList());
			}
		}
		return SUCCESS;
	}
	
	/**
	 * 根据安装包ID, 查询单个软件的详细信息.包括截图 
	 * @param appOsType 
	 * @param version
	 * 
	 * @param imoduletype
	 * @param packUuid
	 * @param app.getUuid()
	 * 
	 * @return AppDTO
	 */
	public String appInfo() {
		if(imoduletype != null && (packUuid == null || "".equals(packUuid)) 
				&& ("10".equals(imoduletype) || "9".equals(imoduletype))){//来自应用首页的软件，该处软件没有packUuid，只能根据Uuid和OS查询详情

			Os os = this.adaptToOs(appOsType, version);
			app = appQuerier.findSingle(app.getUuid(), os);
		}else{
			app = appQuerier.findSinglePack(packUuid);
		}
		return SUCCESS;
	}

	/**
	 * 客户端配置数据
	 * @param provId（省份id）
	 * @param smallId（最小的配置类别id）
	 * @param bigId（最大的配置类别id）
	 * 
	 * @return JsonObject
	 */
	public String configdate() {
		List<Clientconfigdate> ccDatelist = resourceService.findCcDate(provId,smallId, bigId);
		JSONArray jarr = new JSONArray();
		for (Clientconfigdate cctDate : ccDatelist) {
			JSONObject jo = new JSONObject();
			try {
				jo.put("id", cctDate.getClientconfigtype().getId());
				jo.put("content", cctDate.getContent());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			jarr.put(jo);
		}

		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(joMap.toString());
	}
	
	
	/** =================================== 下载安装 模块 ====================================== */ 
	/**
	 * 短信下载  
	 * ios短信下载改成: 发送itunes链接, 不使用短链
	 * @param empId
	 * @param installType
	 * 
	 * @param packUuid
	 * @param phoneNo
	 * @param imoduletype 
	 * @param appOsType
	 * @param version
	 * 
	 * @return
	 * @throws CpServerAccessException
	 */
	public String duanXinDown() throws CpServerAccessException {

		Employee emp = empService.findById(empId);
		if (emp == null)
			throw new IllegalArgumentException(
					"no matching areacode of this province id on empId :" + empId);
		
		int logId = installLogService.buildSmsInstallLog(Integer.parseInt(installType), empId, packUuid, phoneNo, 
				70004, SessionAccessor.getPhoneLoginUuid().toString(), imoduletype);
		
		String areaCode = config_i8.getProvAreaMap().get(emp.getProvince().getId());
		try {
			//IOS的操作系统的短信全部发送itunes链接
			Os os = this.adaptToOs(appOsType, version);
			if(Os.IOS.equals(os) || Os.IOSX.equals(os)){
				AppInstallPack IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
				String url = String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim());
				
				smsSender.sendDownloadLink(areaCode, phoneNo, IOSAppPkg.getApp().getName(), url);
				
			}else{
				smsHessianService.sendShortUrl(Integer.parseInt(installType), phoneNo, areaCode, packUuid, logId);// 该packUuid是从页面上传入的
			}
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	
	/**
	 * 批量下载、强制安装、隐藏安装
	 * 
	 * 根据 操作系统、软件类别、uuid 或 packUuid 获取下载信息 (统一下载方法)
	 * @param appOsType
	 * @param version
	 * @param empId
	 * 
	 * @param ids
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONObject
	 * @throws CpServerAccessException
	 */
	public String downLoad() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		Os os = this.adaptToOs(appOsType, version);

		Employee emp = empService.findById(empId);
		provId = emp.getProvince().getId();
		
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id

		// =============================== 页面下载 ===============================
		if (ids != null && !"".equals(ids)) {
			String[] idsstr = ids.split(",");
			for (String disc : idsstr) {
				String[] disc_str = disc.split("_");
				String uuid = null;
				String packUuid = null;
				Integer discriminator = null;
				if (disc_str.length > 0) {
					uuid = disc_str[0];
				}
				if (disc_str.length > 1) {
					packUuid = disc_str[1];
				}
				if (disc_str.length > 2) {
					if (disc_str[2] != null && !"null".equals(disc_str[2]) && !"".equals(disc_str[2])) {
						discriminator = Integer.parseInt(disc_str[2]);
					}
				}
				// 下载 
				try {
					jarr = this.downMsg(jarr, ids, app, discriminator, uuid, packUuid, os, ap, pkgId, imoduletype, isIOS, installType, pageSize, emp.getProvince().getId(), pkgName);
					
				} catch (JSONException e) {
					e.printStackTrace();
				} 

			}
		} else {
			List<PkgGroup> wostorePkgList = new ArrayList<PkgGroup>();
			// 根据不同的业务获取不同的包组
			if ("2".equals(imoduletype)) {// ========================= 强制安装 ====================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_FORCEINSTALL, ap);
			} else if ("3".equals(imoduletype)) {// ==================  隐藏安装  ================
				wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_HIDDEN_INSTALL, ap);
				os = null;
			}
			if (wostorePkgList != null && wostorePkgList.size() > 0) {
				for (PkgGroup pkgGroupDTO : wostorePkgList) {
					pkgList = appService.findPkgByGroupId(pkgGroupDTO.getId());// 获取该包组内所有的包
					if (pkgList != null && pkgList.size() > 0) {
						for (Pkg pkg : pkgList)  {
							if (pkg != null) {
								appPager = appQuerier.findInstallPackByPkg(ap, pkg.getId(), os, offset, pageSize);
								appList = appPager.getList();
								
								for (AppDTO app : appList) {
									// 下载
									try {
										jarr = this.downMsg(jarr, ids, app, app.getDiscriminator(), app.getUuid(), app.getPackUuid(), os, ap, pkg.getId().toString(), imoduletype, isIOS, installType, pageSize, provId, pkg.getName());
										
									} catch (JSONException e) {
										e.printStackTrace();
									} 
								}
							}
						}
					}
				}
			}
		}

		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	
	/**
	 * 缓存安装,业务包  --  根据省份获取该省份下所有的阅联业务包。
	 * @param empId 
	 * 
	 * @return
	 * @throws CpServerAccessException 
	*/
	public String cachePkg() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		
		Employee emp = empService.findById(empId);
		List<UnionreadPkgView> pkgViewlist = resourceService.findPkgViewByProvid(emp.getProvince().getId());
		
		for (UnionreadPkgView pkgView : pkgViewlist) {
			JSONObject jo = new JSONObject();
			try {
				jo.put("pkgId", Integer.parseInt(pkgView.getPkgId()));
				jo.put("pkgName", pkgView.getPkgName());
				jo.put("icon", pkgView.getIcon());
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			jarr.put(jo);
		}
		
		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "pkgInfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	 
	/**
	 * 缓存安装  --  根据省份和业务包 获取该业务包下所有的软件列表。
	 * @param pkgId
	 * @param empId 
	 * @param appOsType
	 * @param version
	 * @param imoduletype
	 * @param isIOS
	 * @param installType
	 * @param offset
	 * @param pageSize
	 * 
	 * @return
	 * @throws CpServerAccessException 
	*/
	public String cacheDownLoad() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		Employee emp = empService.findById(empId);
		Os os = this.adaptToOs(appOsType, version);
		
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		if (pkgId != null) {
			appPager = appQuerier.findInstallPackByPkg(ap, Integer.parseInt(pkgId), os, offset, pageSize);
			appList = appPager.getList();
			
			for (AppDTO app : appList) {
				// 下载
				try {
					jarr = this.downMsg(jarr, ids, app, app.getDiscriminator(), app.getUuid(), app.getPackUuid(), os, ap, pkgId, imoduletype, isIOS, installType, pageSize, emp.getProvince().getId(), pkg.getName());
					
				} catch (JSONException e) {
					e.printStackTrace();
				} 
			}
		}else{
			jarr.put("无法查询 ，因为 pkgId："+pkgId);
		}
		
		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "appInfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	

	/**
	 * 阅联 强制安装
	 * 
	 * 根据 操作系统、软件类别、uuid 或 packUuid 获取下载信息 (统一下载方法)
	 * @param appOsType
	 * @param version
	 * @param empId
	 * 
	 * @param ids
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONObject
	 * @throws CpServerAccessException
	 */
	public String forceinstallDownLoad() throws CpServerAccessException {
		
		JSONArray jarr = new JSONArray();
		Os os = this.adaptToOs(appOsType, version);

		Employee emp = empService.findById(empId);
		provId = emp.getProvince().getId();
		
		AreaParam ap = new AreaParam();
		ap.setProvId(emp.getProvince().getId());
		ap.setCityId(emp.getCity().getId());
		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		List<UnionreadPkgView> list = resourceService.findPackUuidByPkgId(provId, qz_pkgId);
		for (UnionreadPkgView pkgView : list) {
			AppInstallPack pkgApp = resourceService.findAppPkgByid(pkgView.getPackUuid());
			// 下载
			try {
				jarr = this.downMsg(jarr, ids, null, 2, pkgApp.getApp().getUuid(), pkgApp.getUuid(), os, ap, pkgView.getPkgId(), imoduletype, isIOS, installType, pageSize, provId, pkgView.getPkgName());
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return this.doPrint(joMap.toString());
	}
	
	
	
	/**
	 * 一键下载（整个包下载）== 先屏蔽此功能
	 * @param appOsType 
	 * @param version 
	 * @param empId
	 * @param pkgId 
	 * @param pageSize
	 * @return
	 * @throws
	 */
	public String packDown(){
		JSONArray jarr = new JSONArray();
//		Os os = this.adaptToOs(appOsType, version);
//
//		Employee emp = empService.findById(empId);
//		AreaParam ap = new AreaParam();
//		ap.setProvId(emp.getProvince().getId());
//		ap.setCityId(emp.getCity().getId());
//		ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
//
//		List<String> cpIdList = new ArrayList<String>();
//		//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
		/**该处是否根据山西的省id做限制，限制山西的所有项目都使用cp09的软件**/
//		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os) ) {
//			cpIdList.add(AreaParam.CP_WOSTORE);
//		} else {// 游戏和软件都查出来
//			cpIdList.add(AreaParam.CP_I8APP);
//		}
//		
//		// ================= 手机必备 =================
//		appPager = appQuerierImpl.findInstallPackByPkg(ap, pkgId, os, offset, pageSize); // 查找对应必备包的软件列表
//		for (AppDTO ad : appPager.getList()) {
//			// 下载 
//			try {
//				jarr = this.downMsg(jarr, ad, ad.getDiscriminator(), ad.getUuid(), ad.getPackUuid(), os, ap);
//				
//			} catch (JSONException e) {
//				e.printStackTrace();
//			} catch (CpServerAccessException e) {
//				e.printStackTrace();
//			} 
//		}
		
		JSONObject joMap = new JSONObject();
		try {
			joMap.put("name", "msginfo");
			joMap.put("array", jarr);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(joMap.toString());
	}


	
	
	/** =================================== 内置方法 模块 ====================================== */

	/**
	 * 查询软件列表
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param pageSize
	 * @param cpUpdateTime
	 * @param keyword
	 * @param typeId
	 * @param orderBy
	 * @param isSearch
	 * 
	 * @return Pager<AppDTO>
	 */
	public Pager<AppDTO> findAppPager(int appOsType, String version, String keyword, 
			String typeId, boolean orderBy, Date cpUpdateTime, int empId, boolean isSearch, int pageSize) {
		
		Employee emp = empService.findById(empId);
		AreaParam areap = new AreaParam();
		areap.setProvId(emp.getProvince().getId());
		areap.setCityId(emp.getCity().getId());
		areap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
		
		AppParam appp = new AppParam();
		if("".equals(keyword)){
			keyword = null;
		}
		appp.setName(keyword);
		Os os = this.adaptToOs(appOsType, version);
		List<String> cpIdList = this.cpIdList(os, emp.getProvince().getId());
		appp.setCpIds(cpIdList);

		if (orderBy) {// 为 true 则排序
			appp.setOrderBy(OrderBy.cpUpdateTime);
			appp.setCpUpdateTime(cpUpdateTime);
		}
		if (typeId == null || "".equals(typeId)) {
			appp.setRscTypeId(AppParam.RSCTYPE_APP);
		} else {
			appp.setRscTypeId(typeId);
		}
		
		Pager<AppDTO> appPager = null;
		if(isSearch){//做关键字搜索
			appPager = appQuerier.search(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}else {
			appPager = appQuerier.queryPack(areap, os, appp, offset, pageSize);// 里面的packUuid不正确
		}
		return appPager;
	}
	
	/**
	 * 拼接单个json字符对象
	 * 
	 * @param jarr
	 * @param ids
	 * @param app
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * @param os
	 * @param ap
	 * @param pkgId
	 * @param imoduletype
	 * 
	 * @return JSONArray
	 * @throws CpServerAccessException
	 * @throws JSONException
	 */
	public JSONArray downMsg(JSONArray jarr, String ids, AppDTO app, Integer discriminator, String uuid,
			String packUuid, Os os, AreaParam ap, String pkgId, String imoduletype, Integer isIOS, 
			String installType, Integer pageSize, Integer proId, String pkgName)
			throws CpServerAccessException, JSONException {
		// ============================ 开始 == 获取下载地址 ===================================

		if (app == null && uuid != null && !"".equals(uuid)) {
			app = appQuerier.findSingle(uuid, os);//这个查询只为获取名称和类别等信息，uuid和packUuid是以外面调用的时候传入的为准
		}
		String cpId = null;
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpId = AreaParam.CP_WOSTORE;
		} else { // 非android、SYMBIAN 系统使用i8资源, i8资源需要判断soft还是game
			cpId = AreaParam.CP_I8APP;
		}

		/**
		 * 	非正版的操作系统,则根据 discriminator 、packUuid 或 appUuid 、os、cpId 查询 AppInstallPack 对象
		 *  如果是正版的操作系统，并且isIOS == 1 获取正版包对象 
		 * 业务：
		 * 		1、根据 appUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象,如果根据这个获取不到，那就是没有数据。
		 * 		2、根据 packUuid 查询 AppInstallPack, 获取 ipaItemId 不为空的对象；
		 * 			如果为空, 那么再根据 刚查到的对象的 ipaBundleId 字段查询 AppInstallPack ,获取 ipaItemId 不为空的对象 
		 */
		AppInstallPack IOSAppPkg = null;
		AppInstallPack appPkg =  null;
		
		if (Os.IOS.equals(os) && isIOS == 1) {//获取正版url
			IOSAppPkg = resourceService.findIOSAppPackByPkgUuid(packUuid);//根据 packUuid 获取正版url
			
		}else if(isIOS != 1){//不获取正版地址
			appPkg = resourceService.findAppPkgByid(packUuid);
			
		}
		
		//获取非正版的其他下载地址
		Map<String, String> urlMap = this.downLoad(discriminator, uuid, packUuid, os, cpId);
		String pkgUrl = urlMap.get("pkgUrl");

		// ============================ 结束 == 获取下载地址 ================================

		// ==================== 开始 == 是否自动打开 ==========================
		int autoOpen = 0;// 是否自动打开 1：是 ；0：否
		List<PkgGroup> autoOpenPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_AUTO_OPEN, ap);// 是否自动打开
		if (autoOpenPkgList != null && autoOpenPkgList.size() > 0) {
			for (PkgGroup pkgGroupDTO : autoOpenPkgList) {
				List<Pkg> pkgList = appService.findPkgByGroupId(pkgGroupDTO.getId());
				if (pkgList != null && pkgList.size() > 0) {
					for (Pkg p : pkgList) {
						if (p != null) {
							appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize);// 查找对应必备包的软件列表
							appList = appPager.getList();
							for (AppDTO ad : appList) {
								if (packUuid != null && discriminator==2 && ad.getPackUuid().equals(packUuid)) {
									autoOpen = 1;
								} else if ((discriminator == null || discriminator==1) && uuid != null &&  ad.getUuid().equals(uuid)) {
									autoOpen = 1;
								}
							}
						}
					}
				}
			}
			
		}
		// ==================== 结束 == 是否自动打开 ==========================

		// ==================== 开始 == 拼接json字符串 ==========================
		JSONObject jo = new JSONObject();
		if (installType == null || "".equals(installType)) {
			installType = "1";
		}
		jo.put("imoduletype", imoduletype);
		jo.put("installType", installType);
		jo.put("autoOpen", autoOpen);// 是否自动打开 1：是 ；0：否
		if(pkgId == null){
			pkgId = "";
		}
		jo.put("pkgId", pkgId);
		jo.put("pkgName", pkgName);
		jo.put("uuid", app.getUuid());

		if (null != appPkg && !"".equals(appPkg)) {
			jo.put("pkgUUid", appPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("originalPackId", appPkg.getOriginalPackId());
			jo.put("sourcetype", app.getRscTypeId());
			if (appPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", appPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != appPkg.getCp().getId() && "cp03".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != appPkg.getCp().getId() && ("cp04".equals(appPkg.getCp().getId()) || "cp05".equals(appPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != appPkg.getCp().getId() && "cp06".equals(appPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", appPkg.getVersion());
			} else {
				jo.put("version", appPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {// 沃商店 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", appPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", appPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}else if( null != IOSAppPkg ){
			jo.put("pkgUUid", IOSAppPkg.getUuid());// app_install_pack(安装包唯一的id)
			jo.put("appName", app.getName());
			jo.put("originalPackId", IOSAppPkg.getOriginalPackId());
			jo.put("sourcetype", app.getRscTypeId());
			if (IOSAppPkg.getCpUpdateTime() != null) {
				jo.put("updateTime", IOSAppPkg.getCpUpdateTime().replaceAll("[-\\s:]", ""));
			} else {
				jo.put("updateTime", "");
			}
			if ("cp01".equals(IOSAppPkg.getCp().getId()) || "cp02".equals(IOSAppPkg.getCp().getId()) || "cp09".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 0);
			} else if (null != IOSAppPkg.getCp().getId() && "cp03".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 2);
			} else if (null != IOSAppPkg.getCp().getId() && ("cp04".equals(IOSAppPkg.getCp().getId()) || "cp05".equals(IOSAppPkg .getCp().getId()))) {
				jo.put("cpid", 1);
			} else if (null != IOSAppPkg.getCp().getId() && "cp06".equals(IOSAppPkg.getCp().getId())) {
				jo.put("cpid", 5);
			}

			if (IOSAppPkg.getVersion() != null || !"".equals(IOSAppPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("version", IOSAppPkg.getVersion());
			} else {
				jo.put("version", IOSAppPkg.getAppVer());
			}

			if (Os.IOS.equals(os) && isIOS == 1) {// IOS 的softUrl
				jo.put("url", String.format("http://itunes.apple.com/cn/app/id%s?mt=8",IOSAppPkg.getIpaItemId().trim()));// appStore的地址
			} else {
				if (Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {// 沃商店 资源
					jo.put("url", pkgUrl.replaceAll("[,;]*",""));

					if (Os.ANDROID.equals(os)) {
						jo.put("version", IOSAppPkg.getVersion());
					} else if (Os.SYMBIAN.equals(os)) {
						jo.put("version", IOSAppPkg.getAppVer());
					}

				} else { // 自有资源
					String downloadUrl = pkgUrl.replaceAll("[,;]*", "");
					jo.put("url", downloadUrl);
				}
			}
			jarr.put(jo);
		}
		
		// ==================== 结束 == 拼接json字符串  ==========================

		// 客户端调用下载地址后, app的downCount值+1
		resourceService.updateDownNum(app.getUuid());

		return jarr;
	}

	/**
	 * 软件下载
	 * 
	 * @param cpId
	 * @param os
	 * @param discriminator
	 * @param uuid
	 * @param packUuid
	 * 
	 * @return Map<String, Object>
	 * @throws CpServerAccessException
	 */
	public Map<String, String> downLoad(Integer discriminator,
			String uuid, String packUuid, Os os, String cpId)
			throws CpServerAccessException {
		
		Map<String, String> urlMap = new HashMap<String, String>();
		if (discriminator == null || 1 == discriminator) {// appUuid
			if (os != null) {
				urlMap.put("pkgUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, os, cpId)));
			} else {
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.ANDROID, cpId)));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOS, cpId)));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.IOSX, cpId)));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.SYMBIAN, cpId)));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(appQuerier.confirmPack(uuid, Os.JAVA, cpId)));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(appQuerier.confirmPack( uuid, Os.WINMOBILE, cpId)));
			}
		} else if (2 == discriminator) {// packUuid 
			urlMap.put("pkgUrl", appQuerier.getPackUrl(packUuid));
			if(os==null){
				urlMap.put("AndroidUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("IOSXUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("SymbianUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("JavaUrl", appQuerier.getPackUrl(packUuid));
				urlMap.put("WinmobileUrl", appQuerier.getPackUrl(packUuid));
			}
		}

		return urlMap;
	}

	/**
	 * ANDROID 、SYMBIAN 取沃商店资源，其它系统取灵动资源
	 * @param os
	 * 
	 * @return List<String>
	 */
	public List<String> cpIdList(Os os, Integer proId )	{
		List<String> cpIdList = new ArrayList<String>();
		//这个null只是为了能看详情页面，下载的时候还是要验证的手机操作系统的
		if(os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)){//android、SYMBIAN 使用沃商店的，其他的使用i8的
			cpIdList.add(AreaParam.CP_WOSTORE);
		}else{
			cpIdList.add(AreaParam.CP_I8APP);
		}
		return cpIdList;
	}
	
	
	/**  ========================================= 手机客户端的接口 模块  ================================ */
	/**
	 * 手机客户端--软件宝库
	 * 
	 * @param appOsType
	 * @param version
	 * @param pageIndex
	 * @param pageSize
	 * @return JsonArray
	 */
	public String phone_findApps() {
		Os os = this.adaptToOs(appOsType, version);
		offset = (pageIndex - 1) * pageSize;
		appPager = this.findAppPager(appOsType, version, keyword, typeId, orderBy, cpUpdateTime, empId, false, pageSize);
		JSONArray jsonArr = new JSONArray();
		for (AppDTO ad : appPager.getList()) {
			String cpId = null;
			if (Os.SYMBIAN.equals(os) || Os.ANDROID.equals(os)) {
				cpId = AreaParam.CP_WOSTORE;
			} else {
				cpId = AreaParam.CP_I8APP;
			}
			String packUuid = appQuerier.confirmPack( ad.getUuid(), os, cpId);//为null时代表没有找到符合条件的安装包
			ad.setPackUuid(packUuid);
			jsonArr.put(this.jsonAppInfo(ad, packUuid));
		}
		return this.doPrint(jsonArr.toString());
	}

	public String phone_sendAuthcode() {
		Employee emp = empService.findById(empId);
		if (emp == null)
			throw new IllegalArgumentException(
					"no matching areacode of this province id on empId :" + empId);
		String areaCode = config_i8.getProvAreaMap().get( emp.getProvince().getId());

		String authcode = smsHessianService.sendAuthcode(phoneNo, areaCode);
		JSONObject json = new JSONObject();
		try {
			json.put("authcode", authcode);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return this.doPrint(json.toString());

	}

	/**
	 * 根据软件id和操作系统查询对应的单个软件信息
	 * 
	 * @param appOsType
	 * @param version
	 * @return JsonObject
	 */
	public String phone_AppInfo() {
		Os os = this.adaptToOs(appOsType, version);
		app = appQuerier.findSingle(app.getUuid(), os);
		String cpId = null;
		if (os == null || Os.ANDROID.equals(os) || Os.SYMBIAN.equals(os)) {
			cpId = AreaParam.CP_WOSTORE;
		} else {
			cpId = AreaParam.CP_I8APP;
		}
		String packUuid = appQuerier.confirmPack( app.getUuid(), os, cpId);//为null时代表没有找到符合条件的安装包
		return this.doPrint(this.jsonAppInfo(app, packUuid).toString());
	}

	/**
	 * 手机客户端--短信下载
	 * 
	 * @param installType
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param phoneNo
	 * @param ids 
	 * @param app.uuid
	 * @return
	 */
	public String phone_duanXinDown() {
		try {
			// 这中条件主要针对手机客户端的( PC客户端最好是通过 app.PackUuid 做短信下载 )
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
//			Os os = this.adaptToOs(appOsType, version);

			empId = Integer.parseInt(jsnb.getString("empId"));
			Employee emp = empService.findById(empId);
			if (emp == null)
				throw new IllegalArgumentException(
						"no matching areacode of this province id on empId :" + empId);
			String areaCode = config_i8.getProvAreaMap().get(emp.getProvince().getId());
			
			phoneNo = jsnb.getString("phoneNo");
			installType = jsnb.getString("installType");
			JSONArray ids = jsnb.getJSONArray("ids");
			for (int i = 0; i < ids.length(); i++) {
				String packUuid = ids.getString(i);
				int logId = installLogService.buildSmsInstallLog(Integer.parseInt(installType), empId, packUuid, phoneNo, 70004, null, "6");//6：营业人员的手机客户端
				smsHessianService.sendShortUrl(Integer.parseInt(installType), phoneNo, areaCode, packUuid, logId);
			}
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 组装单个json对象
	 * 
	 * @param app
	 * @return JsonObject
	 */
	public JSONObject jsonAppInfo(AppDTO app, String packUuid) {
		JSONObject json = new JSONObject();
		try {
			json.put("uuid", app.getUuid());
			json.put("name", app.getName());
			if(app.getIconUrl() == null || "".equals(app.getIconUrl())){
				json.put("icon", "");
			}else{
				json.put("icon", app.getIconUrl());
			}
			json.put("pinyin", app.getPinyin());
			json.put("disc", app.getDiscriminator());
			json.put("downCount", app.getDownCount());
			json.put("installCount", app.getInstallCount());
			json.put("point", app.getPoint());
			json.put("developer", app.getDeveloper());
			json.put("originalAppId", app.getOriginalAppId());

			json.put("packAppVer", app.getPackAppVer());
			json.put("packCpId", app.getPackCpId());
			json.put("packCpUpdateTime", app.getPackCpUpdateTime());
			json.put("packFileSize", app.getPackFileSize());
			json.put("packOs", app.getPackOs());
			json.put("packOsMinVer", app.getPackOsMinVer());
			json.put("packUuid", packUuid);

			json.put("cpId", app.getCpId());
			json.put("cpName", app.getCpName());
			json.put("cpUpdateTime", app.getCpUpdateTime());
			json.put("info", app.getInfo());
			json.put("infoHtml", app.getInfoHtml());
			json.put("markerType", app.getMarkerType());
			json.put("markerTypeName", app.getMarkerTypeName());

			json.put("rscTypeId", app.getRscTypeId());
			json.put("rscTypeName", app.getRscTypeName());
			json.put("rscTypePid", app.getRscTypePid());

			JSONArray pics = new JSONArray();
			if (app.getPics() != null) {
				for (String p : app.getPics()) {
					pics.put(ConfigProp.getInstance().getFileServerUrl() + p);
				}
			}
			json.put("pics", pics);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}
	
	/**
	 * 手机客户端--下载（只能是单个的下载）---该方法可能会存在问题，需要客户端配合调试（2013-08-29 修改过）
	 * 
	 * @param appOsType
	 * @param version
	 * @param empId
	 * @param ids
	 * @param installType
	 * @param app.uuid
	 * @return
	 */
	public String phone_DownLoad(){
		JSONObject jo = new JSONObject();
		BufferedReader br;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
			Os os = this.adaptToOs(appOsType, version);

			empId = Integer.parseInt(jsnb.getString("empId"));
			
			Employee emp = empService.findById(empId);
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
			
			String uuid = (String) jsnb.get("uuid");
			String packUuid = (String) jsnb.get("packUuid");
			
			AppInstallPack appPkg = resourceService.findAppPkgByid(appQuerier.confirmPack(uuid, os, AreaParam.CP_WOSTORE));
			
			//获取非正版的其他下载地址  这个是query查询的，所以 discriminator 的值是娶不到的，所以直接给个 null
			String discriminator = null;
			Map<String, String> urlMap = this.downLoad(null, uuid, packUuid, os, AreaParam.CP_WOSTORE);
			String pkgUrl = urlMap.get("pkgUrl");
			
			// ==================== 开始 == 是否自动打开 ==========================
			int autoOpen = 0;// 是否自动打开 1：是 ；0：否
			List<PkgGroup> wostorePkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_AUTO_OPEN, ap);// 是否自动打开
			if (wostorePkgList != null && wostorePkgList.size() > 0) {
				for (PkgGroup pkgGroupDTO : wostorePkgList) {
					pkgList = appService.findPkgByGroupId(pkgGroupDTO.getId());
					if (pkgList != null && pkgList.size() > 0) {
						for (Pkg p : pkgList) {
							if (p != null) {
								appPager = appQuerier.findInstallPackByPkg(ap, Integer.parseInt(pkgId), os, offset, pageSize);// 查找对应必备包的软件列表
								appList = appPager.getList();
								if(pkg == null){
									pkg = p;
								}
							}
							for (AppDTO ad : appList) {
								if (Integer.parseInt(discriminator)==2 && ad.getPackUuid().equals(appPkg.getUuid())) {
									autoOpen = 1;
								} else if ((discriminator == null || Integer.parseInt(discriminator) == 1 ) && ad.getUuid().equals(appPkg.getApp().getUuid())) {
									autoOpen = 1;
								}
							}
						}
					}
				}
			}
			// ==================== 结束 == 是否自动打开 ==========================
			
			// =================== 拼接json串 ======================
			jo.put("pkgUuid", appPkg.getUuid());
			jo.put("apkPkgName", appPkg.getApkPkgName());
			jo.put("isAutoOpen", autoOpen);
			if ("cp01".equals(appPkg.getCp().getId()) || "cp02".equals(appPkg.getCp().getId()) || "cp09".equals(appPkg.getCp().getId())) {
				jo.put("source", 0);
			} else if ("cp03".equals(appPkg.getCp().getId())) {
				jo.put("source", 2);
			} else if (("cp04".equals(appPkg.getCp().getId()) || "cp05".equals(appPkg.getCp().getId()))) {
				jo.put("source", 1);
			} else if ("cp06".equals(appPkg.getCp().getId())) {
				jo.put("source", 5);
			}
			if (appPkg.getVersion() != null || !"".equals(appPkg.getVersion())) {// 安装包的版本不为空，优先获取安装包的版本
				jo.put("rscVersion", appPkg.getVersion());
			} else {
				jo.put("rscVersion", appPkg.getAppVer());
			}
			
			jo.put("url", pkgUrl.replaceAll("[,;]*",""));//这个只给android系统用
			
			// 客户端调用下载地址后, app的downCount值+1 source
			resourceService.updateDownNum(uuid);
			
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (CpServerAccessException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(jo.toString());
	}
	
	
	/**
	 * 手机客户端--推荐专区
	 * 
	 * @param appOsType 
	 * @param version 
	 * @param pageIndex
	 * @param pageSize 
	 * @param empId
	 * 
	 * @return JSONArray
	 */
	public String phone_homeIndex() {

		JSONArray jarr = new JSONArray();
		BufferedReader br;
		try {
			br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			
			String s = br.readLine();
			JSONObject jsnb = new JSONObject(s);
		
			appOsType = Integer.parseInt(jsnb.getString("appOsType"));
			version = jsnb.getString("version");
			Os os = this.adaptToOs(appOsType, version);
			
			pageIndex = Integer.parseInt(jsnb.getString("pageIndex"));
			pageSize = Integer.parseInt(jsnb.getString("pageSize"));
			offset = (pageIndex - 1) * pageSize;
			
			empId = Integer.parseInt(jsnb.getString("empId"));
			Employee emp = empService.findById(empId);
			AreaParam ap = new AreaParam();
			ap.setProvId(emp.getProvince().getId());
			ap.setCityId(emp.getCity().getId());
			ap.setDeptId(AreaParam.DEPTID_LIANTONG);// 运营商id
	
			// 根据不同的业务获取不同的包组
			/** ================= 全国安装包 =================*/
			List<PkgGroup> wostoreNationPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_WOSTORE_NATION_PKG, ap);
			if (wostoreNationPkgList != null && wostoreNationPkgList.size() > 0) {
				for (PkgGroup wostorePkgGroupDTO : wostoreNationPkgList) {
					if (wostorePkgGroupDTO.getId() > 0) {
						List<Pkg> pkgDTOList = appService.findPkgByGroupId(wostorePkgGroupDTO.getId());
						if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
							
							for (int i = 0; i < pkgDTOList.size(); i++) {//添加全国包所有子包
								Pkg p = pkgDTOList.get(i);
								System.out.println(p.getName() + "			" +pkgDTOList.size());
								JSONObject pkgJo = new JSONObject();
								pkgJo.put("groupId", p.getGroupId());
								pkgJo.put("pkgId", p.getId());
								if(p.getName() == null || "".equals(p.getName())){
									pkgJo.put("pkgName", "");
								}else{
									pkgJo.put("pkgName", p.getName());
								}
								if(p.getIcon() == null || "".equals(p.getIcon())){
									pkgJo.put("icon", "");
								}else{
									pkgJo.put("icon", p.getIcon());
								}
								if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
									pkgJo.put("largeIcon", "");
								}else{
									pkgJo.put("largeIcon", p.getLargeIcon());
								}
								pkgJo.put("num", p.getNum());
								pkgJo.put("info", p.getInfo());
								
								JSONArray appJo = new JSONArray();
								if(p.getId() > 0){
									appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
									int index = 0;
									for (AppDTO app : appPager.getList()) {
										appJo.put(this.jsonAppInfo(app, app.getPackUuid()));
										index ++;
									}
								}
								pkgJo.put("appArray", appJo);
								jarr.put(pkgJo);
								pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的全国包
							}
							
							
						}
					}
				}
			}
	
			
			/** ================= 手机必备包 ================= **/
			List<PkgGroup> essentialPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_ESSENTIAL, ap);
			if (essentialPkgList != null && essentialPkgList.size() > 0) {
				for (PkgGroup pkgGroupDTO : essentialPkgList) {
					if (pkgGroupDTO.getId() > 0) {
						List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupDTO.getId());// 获取第一个包组（里头包含了该包（手机必备）的所有子包）
						if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
							for (int i = 0; i < pkgDTOList.size(); i++) {
								Pkg p = pkgDTOList.get(i);
								System.out.println(p.getName() + "			" +pkgDTOList.size());
								JSONObject pkgJo = new JSONObject();
								pkgJo.put("groupId", p.getGroupId());
								pkgJo.put("pkgId", p.getId());
								if(p.getName() == null || "".equals(p.getName())){
									pkgJo.put("pkgName", "");
								}else{
									pkgJo.put("pkgName", p.getName());
								}
								if(p.getIcon() == null || "".equals(p.getIcon())){
									pkgJo.put("icon", "");
								}else{
									pkgJo.put("icon", p.getIcon());
								}
								if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
									pkgJo.put("largeIcon", "");
								}else{
									pkgJo.put("largeIcon", p.getLargeIcon());
								}
								pkgJo.put("num", p.getNum());
								pkgJo.put("info", p.getInfo());
								
								JSONArray appJo = new JSONArray();
								
								if(p.getId() > 0){
									appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
									int index = 0;
									for (AppDTO app : appPager.getList()) {
										appJo.put(this.jsonAppInfo(app, app.getPackUuid()));
										index ++;
									}
								}
								pkgJo.put("appArray", appJo);
								jarr.put(pkgJo); //添加所有手机必备包
								if(pkg == null){
									pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
								}
							}
						}
					}
				}
			}
			
			
			/** ================= 地市包 ================= **/
			List<PkgGroup> cityPkgList = appService.findPkgGroupByBizId(AreaParam.BIZ_CITY_PKG, ap);
			if (cityPkgList != null && cityPkgList.size() > 0) {
				for (PkgGroup pkgGroupDTO : cityPkgList) {
					if (pkgGroupDTO.getId() > 0) {
						List<Pkg> pkgDTOList = appService.findPkgByGroupId(pkgGroupDTO.getId());
						if (pkgDTOList != null && !pkgDTOList.isEmpty() && pkgDTOList.size() > 0) {
							for (int i = 0; i < pkgDTOList.size(); i++) {
								
								Pkg p = pkgDTOList.get(i);
								System.out.println(p.getName() + "			" +pkgDTOList.size());
								JSONObject pkgJo = new JSONObject();
								pkgJo.put("groupId", p.getGroupId());
								pkgJo.put("pkgId", p.getId());
								if(p.getName() == null || "".equals(p.getName())){
									pkgJo.put("pkgName", "");
								}else{
									pkgJo.put("pkgName", p.getName());
								}
								if(p.getIcon() == null || "".equals(p.getIcon())){
									pkgJo.put("icon", "");
								}else{
									pkgJo.put("icon", p.getIcon());
								}
								
								if(p.getLargeIcon() == null || "".equals(p.getLargeIcon())){
									pkgJo.put("largeIcon", "");
								}else{
									pkgJo.put("largeIcon", p.getLargeIcon());
								}
								pkgJo.put("num", p.getNum());
								pkgJo.put("info", p.getInfo());
								
								JSONArray appJo = new JSONArray();
								if(p.getId() > 0){
									appPager = appQuerier.findInstallPackByPkg(ap, p.getId(), os, offset, pageSize); // 查找对应包的软件列表
									int index = 0;
									for (AppDTO app : appPager.getList()) {
										appJo.put( this.jsonAppInfo(app, app.getPackUuid()));
										index ++;
									}
								}
								pkgJo.put("appArray", appJo);
								jarr.put(pkgJo); //添加所有地市包
								if(pkg == null){
									pkg = pkgDTOList.get(0);//页面初次加载的时候展示用的包
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return this.doPrint(jarr.toString());
	}

	

	/***************************************************************************
	 * 访问器
	 **************************************************************************/
	public List<String> getCharList() {
		return charList;
	}

	public void setCharList(List<String> charList) {
		this.charList = charList;
	}

	public AppQuerier getAppQuerierImpl() {
		return appQuerier;
	}

	public void setAppQuerierImpl(AppQuerier appQuerierImpl) {
		this.appQuerier = appQuerierImpl;
	}

	public Pager<AppDTO> getAppPager() {
		return appPager;
	}

	public void setAppPager(Pager<AppDTO> appPager) {
		this.appPager = appPager;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public int getAppOsType() {
		return appOsType;
	}

	public void setAppOsType(int appOsType) {
		this.appOsType = appOsType;
	}

	public List<ResourceType> getAppTypeList() {
		return appTypeList;
	}

	public void setAppTypeList(List<ResourceType> appTypeList) {
		this.appTypeList = appTypeList;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public AppDTO getApp() {
		return app;
	}

	public void setApp(AppDTO app) {
		this.app = app;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public Pkg getPkg() {
		return pkg;
	}

	public void setPkg(Pkg pkg) {
		this.pkg = pkg;
	}
	
	public String getPkgId() {
		return pkgId;
	}

	public void setPkgId(String pkgId) {
		this.pkgId = pkgId;
	}

	public boolean isOrderBy() {
		return orderBy;
	}

	public void setOrderBy(boolean orderBy) {
		this.orderBy = orderBy;
	}

	public Map<String, Pager<AppDTO>> getAppMap() {
		return appMap;
	}

	public void setAppMap(Map<String, Pager<AppDTO>> appMap) {
		this.appMap = appMap;
	}

	public List<Pkg> getPkgList() {
		return pkgList;
	}

	public void setPkgList(List<Pkg> pkgList) {
		this.pkgList = pkgList;
	}

	public List<AppDTO> getAppList() {
		return appList;
	}

	public void setAppList(List<AppDTO> appList) {
		this.appList = appList;
	}

	public String getInstallType() {
		return installType;
	}

	public void setInstallType(String installType) {
		this.installType = installType;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getIsIOS() {
		return isIOS;
	}

	public void setIsIOS(int isIOS) {
		this.isIOS = isIOS;
	}

	public int getSmallId() {
		return smallId;
	}

	public void setSmallId(int smallId) {
		this.smallId = smallId;
	}

	public int getBigId() {
		return bigId;
	}

	public void setBigId(int bigId) {
		this.bigId = bigId;
	}

	public int getProvId() {
		return provId;
	}

	public void setProvId(int provId) {
		this.provId = provId;
	}

	public String getImoduletype() {
		return imoduletype;
	}

	public void setImoduletype(String imoduletype) {
		this.imoduletype = imoduletype;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Map<Pkg, List<AppDTO>> getPkgMap() {
		return pkgMap;
	}

	public void setPkgMap(Map<Pkg, List<AppDTO>> pkgMap) {
		this.pkgMap = pkgMap;
	}

	public Date getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(Date cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public int getPageSize2() {
		return pageSize2;
	}

	public void setPageSize2(int pageSize2) {
		this.pageSize2 = pageSize2;
	}

	public Map<ResourceType, List<AppDTO>> getTypeMap() {
		return typeMap;
	}

	public void setTypeMap(Map<ResourceType, List<AppDTO>> typeMap) {
		this.typeMap = typeMap;
	}

	public ResourceType getResType() {
		return resType;
	}

	public void setResType(ResourceType resType) {
		this.resType = resType;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getPackUuid() {
		return packUuid;
	}

	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}

	public List<AppInstallPack> getAppInstallPackList() {
		return appInstallPackList;
	}

	public void setAppInstallPackList(List<AppInstallPack> appInstallPackList) {
		this.appInstallPackList = appInstallPackList;
	}

	public String getPkgName() {
		return pkgName;
	}

	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
}

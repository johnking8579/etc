package com.i8app.shop.web.action;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.FaqType;
import com.i8app.shop.domain.Faqs;
import com.i8app.shop.service.FaqsService;
import com.i8app.shop.service.ISmsService;

/**
 * FAQ页面Action
 * 
 */
@Controller
@Scope("prototype")
public class FaqsAction extends BaseAction {

	@Resource
	private FaqsService faqsService;
	@Resource
	private ISmsService smsService;

	private List<Faqs> faqsList;
	private List<FaqType> faqTypeList;
	private Map<FaqType, List<Faqs>> faqMap;

	private int id;

	/**
	 * 电话号, 搜索字, 添加的问题内容
	 */
	private String phoneNumber, keyword, questiont;

	/**
	 * 首页
	 */
	public String index() {
		faqTypeList = faqsService.typeList();
		faqMap = faqsService.getAllMap();

		Employee emp = SessionAccessor.getEmp();
		if (emp != null)
			faqsList = faqsService.findByEmpId(emp.getId());
		return SUCCESS;
	}

	/**
	 * 提问一条问题
	 */
	public String add() {
		this.doPrint(String.format("{\"errorCode\":%d}",
				faqsService.addFaqs(questiont, null, 
						SessionAccessor.getEmp(), SessionAccessor.getHasFaqed())));
		return null;
	}

	/**
	 * 搜索列表
	 */
	public String search() {
		faqsList = faqsService.findByQuestion(keyword);
		return SUCCESS;
	}


	/**************************************
	 * 以下是访问器
	 ***********************************/
	public String getQuestiont() {
		return questiont;
	}

	public void setQuestiont(String questiont) {
		this.questiont = questiont;
	}

	public List<FaqType> getFaqTypeList() {
		return faqTypeList;
	}

	public void setFaqTypeList(List<FaqType> faqTypeList) {
		this.faqTypeList = faqTypeList;
	}

	public Map<FaqType, List<Faqs>> getFaqMap() {
		return faqMap;
	}

	public void setFaqMap(Map<FaqType, List<Faqs>> faqMap) {
		this.faqMap = faqMap;
	}

	public List<Faqs> getFaqsList() {
		return faqsList;
	}

	public void setFaqsList(List<Faqs> faqsList) {
		this.faqsList = faqsList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
}

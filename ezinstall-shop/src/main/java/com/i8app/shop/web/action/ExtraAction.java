package com.i8app.shop.web.action;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.i8app.shop.common.Pager;
import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.domain.Announcement;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.City;
import com.i8app.shop.domain.ClientExcept;
import com.i8app.shop.domain.County;
import com.i8app.shop.domain.InstantMsg;
import com.i8app.shop.domain.ManuInfo;
import com.i8app.shop.domain.Province;
import com.i8app.shop.domain.TypeInfo;
import com.i8app.shop.service.EmpService;
import com.i8app.shop.service.SimpleQueryService;
import com.i8app.shop.service.TypeInfoService;

/**
 * 未归类功能的Action
 * @author jing
 */
@Controller
@Scope("prototype")
public class ExtraAction extends BaseAction {
	
	private static Logger logger = Logger.getLogger(ExtraAction.class);
	
	//flag 是否为iphone正版, 0否1是; type资源类型:1软件2游戏3沃商店
	private int id, manuId, type, flag;	
	private TypeInfo typeInfo;
	private String keyword, contentLength, fileName, empNo, areaCode, descript, mac;
	private Integer itype, exceptid;

	private Set<City> citySet;
	private List<ManuInfo> manuList;
	private List<TypeInfo> typeInfoList;
	private Pager<TypeInfo> typeInfoPager;
	private Pager<InstantMsg> msgPager;
	private List<Announcement> announceList;

	@Resource
	private EmpService empService;
	@Resource
	private TypeInfoService typeInfoService;
	@Resource
	private SimpleQueryService simpleQueryService;
	
	/**
	 * 厂商选择页面
	 * @return
	 */
	public String manuSel()	{
		manuList = simpleQueryService.manuList();
		return SUCCESS;
	}
	
	/**
	 * 厂商选择页面（新版） 
	 * @return
	 */
	public String manuSel2()	{
		manuList = simpleQueryService.manuList();
		return SUCCESS;
	}
	
	/**
	 * 机型选择页面
	 * @return
	 */
	public String phoneSel()	{
		typeInfoList = typeInfoService.findByManuId(manuId);
		return SUCCESS;
	}
	
	/**
	 * 机型选择页面 （新版）
	 * @return
	 */
	public String modelSel()	{
		typeInfoList = typeInfoService.findByManuId(manuId);
		return SUCCESS;
	}
	
	/**
	 * 根据MANUID, 获得该厂商所有的TYPEINFO列表
	 * @return
	 */
	public String typeList()	{
		typeInfoList = typeInfoService.findByManuId(manuId);
		this.doPrint(new Gson().toJson(typeInfoList));
		return null;
	}
	
	/**
	 * 终端演示:获得所有手机列表, 按HOTORDER升序排列. 如果传入了keyword, 则为搜索页面
	 * @return
	 */
	public String phoneList()	{
		pageSize = 28;
		if(keyword == null || "".equals(keyword))
			typeInfoPager = typeInfoService.list(offset, pageSize);
		else
			typeInfoPager = typeInfoService.search(keyword, offset, pageSize);
			
		return SUCCESS;
	}

	/**
	 * 终端演示:查询手机的详细信息;有5种不同的页面
	 * 安卓Z710e,
	 * @return
	 */
	public String phoneInfo()	{
		typeInfo = typeInfoService.findById(id);
		
		String typeName = typeInfo.getTypeName().toLowerCase();
		if(typeName.contains("iphone") && typeName.contains("3"))
			return "3";
		else if(typeName.contains("iphone") && typeName.contains("4s"))
			return "4s";
		else if(typeName.contains("iphone") && typeName.contains("4"))
			return "4";
		else if(typeName.contains("z710e"))
			return "android";
		else
			return SUCCESS;
	}
	
	/**
	 * 联通广告页
	 * @return
	 */
	public String advertise()	{
		return SUCCESS;
	}
	
	/**
	 * 营业厅选择页面, 查出城市列表
	 * @return
	 */
	public String hallSel()	{
		int pid;
		try {
			pid = empService.getProvinceIdByEmp(empNo);
		} catch (Exception e) {
			this.doPrint("{\"errorcode\":1}");
			return null;
		}
		Province p = simpleQueryService.findProvinceById(pid);
		citySet = p.getCitySet();
		return SUCCESS;
	}
	
	/**
	 * 查出 城市下区县列表
	 * @return
	 */
	public String countyList()	{
		City c = simpleQueryService.findCityById(id);
		Set<County> countySet = c.getCountySet();
		StringBuilder sb = new StringBuilder();
		if(countySet.isEmpty())	{
			sb.append("<option value=\"\">-没有找到区县-</option>");
		} else	{
			sb.append("<option value=\"\">-请选择区县-</option>");
			for(County co : countySet)	{
				sb.append(String.format("<option value=\"%s\">%s</option>", co.getId(), co.getName()));
			}
		}
		this.doPrint(sb.toString());
		return null;
	}
	
	/**
	 * 查出区县下厅列表 
	 * @return
	 */
	public String hallList()	{
		County c = simpleQueryService.findCountyById(id);
		Set<BusinessHall> hallSet = c.getHallSet();
		StringBuilder sb = new StringBuilder();
		if(hallSet.isEmpty())
			sb.append("<option value=\"\">-没有找到营业厅-</option>");
		else	{
			sb.append("<option value=\"\">-请选择营业厅-</option>");
			for(BusinessHall b : hallSet)	{
				sb.append(String.format("<option value=\"%s\">%s</option>", b.getId(), b.getHallName()));
			}
		}
		this.doPrint(sb.toString());
		return null;
	}
	
	
	/**
	 * 查出营业厅信息
	 * @return
	 */
	public String hallInfo()	{
		BusinessHall b = simpleQueryService.findBusinessHallById(id);
		this.doPrint(b.getId() + "," + b.getHallName());
		return null;
	}
	
	 
	/**
	 * 公告页面
	 * @return
	 */
	public String announceList()	{
		//TODO 以后只从参数中取, 不再从session中获取areacode
		announceList = simpleQueryService.findAnnounce(areaCode != null ? areaCode : SessionAccessor.getAreaCode());
		return SUCCESS;
	}
	
	/**
	 * 最新的一条公告. 如果无公告, 返回"". 如果有公告, 但isUrl=1, 则转向其它action.
	 * 如果isUrl=0, 返回content字段里的文本.
	 * @return
	 */
	public String announceInfo()	{
		List<Announcement> list = simpleQueryService.findAnnounce(areaCode);
		if(list.isEmpty())
			return doPrint("");
		else	{
			Announcement a = list.get(0);
			if(a.getIsUrl())	{
				id = Integer.parseInt(a.getContent());
				return SUCCESS;
			}  else 	{
				return doPrint(a.getContent());
			}
		}
	}
	
	/**
	 * 特殊公告. 即isUrl=1的公告
	 * @return
	 */
	public String specAnnounce()	{
		switch(id)	{
		case 1:	//TODO
			break;
		default:
			throw new UnsupportedOperationException(id + "对应的特殊公告还未设置");
		}
		return SUCCESS;
	}
	
	public String msgList()	{
		pageSize = 20;
		msgPager = simpleQueryService.findInstantMsg(areaCode, offset, pageSize);
		return SUCCESS;
	}
	
	/**
	 * 上传客户端的异常日志
	 * @return
	 */
	public String uploadExcept()	{
		JsonObject json = new JsonObject();
		int errorCode = 0;
		String desc = null;
		try	{
			ClientExcept ce = new ClientExcept();
			ce.setDescript(descript);
			ce.setExceptid(exceptid);
			ce.setItype(itype);
			ce.setMac(mac);
			ce.setExceptTime(new Date());
			simpleQueryService.persistClientExcept(ce);
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
			errorCode = 500;
			desc = e.getMessage();
		}
		json.addProperty("errorCode", errorCode);
		json.addProperty("desc", desc);
		return doPrint(json.toString());
	}
	
	
	/********************************************************
	 * 以下是访问器
	 ********************************************************/
	public List<ManuInfo> getManuList() {
		return manuList;
	}

	public void setManuList(List<ManuInfo> manuList) {
		this.manuList = manuList;
	}

	public List<TypeInfo> getTypeInfoList() {
		return typeInfoList;
	}

	public void setTypeInfoList(List<TypeInfo> typeInfoList) {
		this.typeInfoList = typeInfoList;
	}

	public int getManuId() {
		return manuId;
	}

	public void setManuId(int manuId) {
		this.manuId = manuId;
	}

	public List<Announcement> getAnnounceList() {
		return announceList;
	}

	public void setAnnounceList(List<Announcement> announceList) {
		this.announceList = announceList;
	}

	public Pager<TypeInfo> getTypeInfoPager() {
		return typeInfoPager;
	}

	public void setTypeInfoPager(Pager<TypeInfo> typeInfoPager) {
		this.typeInfoPager = typeInfoPager;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TypeInfo getTypeInfo() {
		return typeInfo;
	}

	public void setTypeInfo(TypeInfo typeInfo) {
		this.typeInfo = typeInfo;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public Set<City> getCitySet() {
		return citySet;
	}

	public void setCitySet(Set<City> citySet) {
		this.citySet = citySet;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public void setTypeInfoService(TypeInfoService typeInfoService) {
		this.typeInfoService = typeInfoService;
	}

	public void setSimpleQueryService(SimpleQueryService simpleQueryService) {
		this.simpleQueryService = simpleQueryService;
	}

	public Pager<InstantMsg> getMsgPager() {
		return msgPager;
	}

	public void setMsgPager(Pager<InstantMsg> msgPager) {
		this.msgPager = msgPager;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public Integer getItype() {
		return itype;
	}

	public void setItype(Integer itype) {
		this.itype = itype;
	}

	public Integer getExceptid() {
		return exceptid;
	}

	public void setExceptid(Integer exceptid) {
		this.exceptid = exceptid;
	}
}

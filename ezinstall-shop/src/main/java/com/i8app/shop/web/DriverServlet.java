package com.i8app.shop.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.i8app.shop.common.config.ConfigProp;
import com.i8app.shop.dao.DriverDao;
import com.i8app.shop.dao.DriverMissingDao;
import com.i8app.shop.domain.DriverMissing;

/**
 * 驱动相关的入口
 * @author jing
 *
 */
public class DriverServlet extends HttpServlet {
	
	private static Logger logger = Logger.getLogger(DriverServlet.class);
	

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cmd = request.getParameter("cmd");
		String msg;
		if("report".equals(cmd))	{
			msg = reportDriver(request, response);
		} else	{
			msg = downloadDriver(request, response);
		}
			
		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw = response.getWriter();
		pw.print(msg);
		pw.flush();
		pw.close();
	}
	
	
	/**
	 * 报告无法识别的驱动
	 * @param request
	 * @param response
	 * @return
	 */
	private String reportDriver(HttpServletRequest request, HttpServletResponse response)	{
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		String result = "0";
		String manu = request.getParameter("manu");
		String type = request.getParameter("type");
		String info1 = request.getParameter("info1");
		String info2 = request.getParameter("info2");
		DriverMissing dm = new DriverMissing(manu, type, info1, info2);
		try	{
			DriverMissingDao dao = applicationContext.getBean(DriverMissingDao.class);
			dao.persist(dm);
			result = "1";
		} catch(Exception e)	{
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	
	/**
	 * 显示驱动下载地址.
	 * 根据传入的参数vid, pid, osType, 从数据库中找到对应驱动的下载地址并返回
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	private String downloadDriver(HttpServletRequest request, HttpServletResponse response) throws IOException	{
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		String path = ConfigProp.getInstance().getFileServerUrl();

		String msg = "failed";
		String vid = request.getParameter("vid");
		String pid = request.getParameter("pid");
		int osType = Integer.parseInt(request.getParameter("osType"));
		
		DriverDao driverDao = applicationContext.getBean(DriverDao.class);
		String result = driverDao.getUniqueResult(vid, pid, osType);
		
		if(result != null)	{
			msg = path + result.replace("\\", "/");
		}
		return msg;
	}

}

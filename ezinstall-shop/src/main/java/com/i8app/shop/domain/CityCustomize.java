package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CityCustomize {
	private Integer id;
	private int provinceId, cityId, operator;	//省id, 市id, 运营商
	private boolean isBounce;

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(int provinceId) {
		this.provinceId = provinceId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public boolean getIsBounce() {
		return isBounce;
	}

	public void setIsBounce(boolean isBounce) {
		this.isBounce = isBounce;
	}

	@Column(name="yunyingshang")
	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}
}

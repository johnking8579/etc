package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class App {
	private String uuid, name, pinyin, developer, info, infoHtml;
	private String icon, banner, pic1, pic2, pic3, pic4;
	private ResourceType rscType;
	private String markerType;
	private boolean isAreaCustom, isDel;
	private int downCount, installCount;
	private Date updateTime;
	private String originalAppId, originalTypeId;
	private ContentProvider cp;
	private Float point;
	
	public Float getPoint() {
		return point;
	}

	public void setPoint(Float point) {
		this.point = point;
	}

	@ManyToOne
	@JoinColumn(name="cpId")
	public ContentProvider getCp() {
		return cp;
	}

	public void setCp(ContentProvider cp) {
		this.cp = cp;
	}
	
	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}

	public String getPic4() {
		return pic4;
	}

	public void setPic4(String pic4) {
		this.pic4 = pic4;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	@Id
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfoHtml() {
		return infoHtml;
	}

	public void setInfoHtml(String infoHtml) {
		this.infoHtml = infoHtml;
	}
	
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@ManyToOne
	@JoinColumn(name="rscTypeId")
	public ResourceType getRscType() {
		return rscType;
	}

	public void setRscType(ResourceType rscType) {
		this.rscType = rscType;
	}

	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public boolean getIsAreaCustom() {
		return isAreaCustom;
	}

	public void setIsAreaCustom(boolean isAreaCustom) {
		this.isAreaCustom = isAreaCustom;
	}

	public boolean getIsDel() {
		return isDel;
	}

	public void setIsDel(boolean isDel) {
		this.isDel = isDel;
	}

	public int getDownCount() {
		return downCount;
	}

	public void setDownCount(int downCount) {
		this.downCount = downCount;
	}

	public int getInstallCount() {
		return installCount;
	}

	public void setInstallCount(int installCount) {
		this.installCount = installCount;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getOriginalAppId() {
		return originalAppId;
	}

	public void setOriginalAppId(String originalAppId) {
		this.originalAppId = originalAppId;
	}

	public String getOriginalTypeId() {
		return originalTypeId;
	}

	public void setOriginalTypeId(String originalTypeId) {
		this.originalTypeId = originalTypeId;
	}
}

package com.i8app.shop.domain;

/**
 * Appleinfo entity. @author MyEclipse Persistence Tools
 */

public class Applemailinfo implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String mac;
	private String guid;
	private Integer number;
    private String mail;
	// Constructors

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	/** default constructor */
	public Applemailinfo() {
	}

	/** full constructor */
	public Applemailinfo(String mac, String guid, Integer number) {
		this.mac = mac;
		this.guid = guid;
		this.number = number;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMac() {
		return this.mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

}
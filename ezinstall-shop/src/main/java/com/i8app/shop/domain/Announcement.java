package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage=CacheConcurrencyStrategy.NONE)
public class Announcement {
	private Integer id;
	private String title, content, areaCode, uploadTime;
	private Integer hasAnnounced;
	private boolean isUrl;	//指示content字段是一个url还是普通文字,不能为空,默认0
	
	public Announcement() {	}

	public Announcement(String title, String content,
			String areaCode, String uploadTime, Integer hasAnnounced,
			boolean isUrl) {
		this.title = title;
		this.content = content;
		this.areaCode = areaCode;
		this.uploadTime = uploadTime;
		this.hasAnnounced = hasAnnounced;
		this.isUrl = isUrl;
	}

	@Id
	@GeneratedValue
	@Column(name="aid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(length=50)
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Column(length=50)
	public String getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	@Column(name="isOk")
	public Integer getHasAnnounced() {
		return hasAnnounced;
	}

	public void setHasAnnounced(Integer hasAnnounced) {
		this.hasAnnounced = hasAnnounced;
	}

	public boolean getIsUrl() {
		return isUrl;
	}

	public void setIsUrl(boolean isUrl) {
		this.isUrl = isUrl;
	}
}

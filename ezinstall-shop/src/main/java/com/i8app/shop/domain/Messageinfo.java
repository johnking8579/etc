package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "messageinfo")
public class Messageinfo {
	private int id;
	private Employee empid;
	private Faqs fid;
	private String phoneNum;
	private String mdate;
	private String areacode;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getAreacode() {
		return areacode;
	}

	public void setAreacode(String areacode) {
		this.areacode = areacode;
	}

	@ManyToOne
	@JoinColumn(name = "fid")
	public Faqs getFid() {
		return fid;
	}

	public void setFid(Faqs fid) {
		this.fid = fid;
	}

	public void setEmpid(Employee empid) {
		this.empid = empid;
	}

	@ManyToOne
	@JoinColumn(name = "empid")
	public Employee getEmpid() {
		return empid;
	}

}

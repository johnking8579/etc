package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.shop.common.config.ConfigProp;

@Entity
@Table(name = "app_install_pack")
public class AppInstallPack {

	private String uuid;
	private App app;
	private String appVer;
	private Long appVerValue;
	private String os, osMinVer;
	private Long osMinVerValue;
	private String fileSize, version, apkPkgName, ipaBundleId,
			originalPackId, cpUpdateTime, ipaItemId, appleId, originalName;
	private Integer apkVersionCode, ipaVersionCode, status;
	private Date updateTime;
	private ContentProvider cp;
	
	public AppDTO toAppDTO()	{
		String fsUrl = ConfigProp.getInstance().getFileServerUrl();
		AppDTO dto = new AppDTO();
		dto.setBanner(app.getBanner() != null ? fsUrl + app.getBanner() : null);
		dto.setBundleId(null);
		dto.setCpId(app.getCp().getId());
		dto.setCpName(app.getCp().getName());
		dto.setCpUpdateTime(cpUpdateTime);
		dto.setDeveloper(app.getDeveloper());
		dto.setDiscriminator(2);
		dto.setDownCount(app.getDownCount());
		dto.setIcon(app.getIcon() != null ? fsUrl + app.getIcon() : null);
		dto.setInfo(app.getInfo());
//		dto.setInfoHtml(infoHtml);	废弃
		dto.setInstallCount(app.getInstallCount());
//TODO		dto.setMarkerType(markerType);	暂不传值
//TODO		dto.setMarkerTypeName(markerTypeName);	暂不传值
		dto.setName(app.getName());
		dto.setOriginalAppId(app.getOriginalAppId());
		dto.setOriginalPackId(originalPackId);
		dto.setPackAppVer(appVer);
		dto.setPackCpId(cp.getId());
		dto.setPackCpUpdateTime(cpUpdateTime);
		dto.setPackFileSize(fileSize);
		dto.setPackOs(os);
		dto.setPackOsMinVer(osMinVer);
		dto.setPackUuid(uuid);
		dto.setPic1(app.getPic1() != null ? fsUrl + app.getPic1() : null);
		dto.setPic2(app.getPic2() != null ? fsUrl + app.getPic2() : null);
		dto.setPic3(app.getPic3() != null ? fsUrl + app.getPic3() : null);
		dto.setPic4(app.getPic4() != null ? fsUrl + app.getPic4() : null);
		dto.setPinyin(app.getPinyin());
		dto.setPoint(app.getPoint());
		dto.setRscTypeId(app.getRscType().getId());
		dto.setRscTypeName(app.getRscType().getName());
		dto.setRscTypePid(app.getRscType().getPid());
		dto.setUuid(app.getUuid());
		dto.setVersionCode(null);	//TODO 以后再赋值
		return dto;
	}
	

	@ManyToOne
	@JoinColumn(name="cpId")
	public ContentProvider getCp() {
		return cp;
	}

	public void setCp(ContentProvider cp) {
		this.cp = cp;
	}

	@Id
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@ManyToOne
	@JoinColumn(name="appId")
	public App getApp() {
		return app;
	}

	public void setApp(App app) {
		this.app = app;
	}
	
	public String getAppVer() {
		return appVer;
	}

	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	public Long getAppVerValue() {
		return appVerValue;
	}

	public void setAppVerValue(Long appVerValue) {
		this.appVerValue = appVerValue;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOsMinVer() {
		return osMinVer;
	}

	public void setOsMinVer(String osMinVer) {
		this.osMinVer = osMinVer;
	}

	public Long getOsMinVerValue() {
		return osMinVerValue;
	}

	public void setOsMinVerValue(Long osMinVerValue) {
		this.osMinVerValue = osMinVerValue;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getApkPkgName() {
		return apkPkgName;
	}

	public void setApkPkgName(String apkPkgName) {
		this.apkPkgName = apkPkgName;
	}

	public String getIpaBundleId() {
		return ipaBundleId;
	}

	public void setIpaBundleId(String ipaBundleId) {
		this.ipaBundleId = ipaBundleId;
	}

	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}

	public String getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(String cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public Integer getApkVersionCode() {
		return apkVersionCode;
	}

	public void setApkVersionCode(Integer apkVersionCode) {
		this.apkVersionCode = apkVersionCode;
	}

	public Integer getIpaVersionCode() {
		return ipaVersionCode;
	}

	public void setIpaVersionCode(Integer ipaVersionCode) {
		this.ipaVersionCode = ipaVersionCode;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIpaItemId() {
		return ipaItemId;
	}

	public void setIpaItemId(String ipaItemId) {
		this.ipaItemId = ipaItemId;
	}

	public String getAppleId() {
		return appleId;
	}

	public void setAppleId(String appleId) {
		this.appleId = appleId;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}

package com.i8app.shop.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="countyInfo")
public class County implements Serializable {
	private Integer id;
	private String name;
	private City city;
//	private Integer order;
	private Set<BusinessHall> hallSet;

	@Id
	@GeneratedValue
	@Column(name="countyId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="countyName")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@JoinColumn(name="cityId")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

//	public Integer getOrder() {
//		return order;
//	}
//
//	public void setOrder(Integer order) {
//		this.order = order;
//	}

	@OneToMany(mappedBy="county")
	@OrderBy(value="id")
	@JoinColumn(name="countyId")
	public Set<BusinessHall> getHallSet() {
		return hallSet;
	}

	public void setHallSet(Set<BusinessHall> hallSet) {
		this.hallSet = hallSet;
	}
}

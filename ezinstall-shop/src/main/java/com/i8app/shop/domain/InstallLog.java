package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InstallLog {
	private Integer id;
	private Integer typeid, ostype, hallId, installType, source, optStatus,
			versionId, isOtherNet, isAutoOpen, isSmsVal, provinceId, cityId,
			countyId, bhid, ctid, ccid, rccid, isGuide, filterrule, pkgId;	//filterule暂不处理
	private String imei, rscid, installTime, empId, phoneNumber, uuid, rscName,
			rscVersion, manuName, modelName, osVersion, psn, imsi, empNo,
			provinceName, cityName, countyName, hallName, empName, hallno,
			sourceType, seqNo, appUuid, packUuid;
	private String chanType, chanGrade, chanLevel, imoduletype, cpId, markerType, pkgName;

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTypeid() {
		return typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	public Integer getOstype() {
		return ostype;
	}

	public void setOstype(Integer ostype) {
		this.ostype = ostype;
	}

	public Integer getHallId() {
		return hallId;
	}

	public void setHallId(Integer hallId) {
		this.hallId = hallId;
	}

	public Integer getInstallType() {
		return installType;
	}

	public void setInstallType(Integer installType) {
		this.installType = installType;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getOptStatus() {
		return optStatus;
	}

	public void setOptStatus(Integer optStatus) {
		this.optStatus = optStatus;
	}

	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public Integer getIsOtherNet() {
		return isOtherNet;
	}

	public void setIsOtherNet(Integer isOtherNet) {
		this.isOtherNet = isOtherNet;
	}

	public Integer getIsAutoOpen() {
		return isAutoOpen;
	}

	public void setIsAutoOpen(Integer isAutoOpen) {
		this.isAutoOpen = isAutoOpen;
	}

	public Integer getIsSmsVal() {
		return isSmsVal;
	}

	public void setIsSmsVal(Integer isSmsVal) {
		this.isSmsVal = isSmsVal;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getCountyId() {
		return countyId;
	}

	public void setCountyId(Integer countyId) {
		this.countyId = countyId;
	}

	public Integer getBhid() {
		return bhid;
	}

	public void setBhid(Integer bhid) {
		this.bhid = bhid;
	}

	public Integer getCtid() {
		return ctid;
	}

	public void setCtid(Integer ctid) {
		this.ctid = ctid;
	}

	public Integer getCcid() {
		return ccid;
	}

	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

	public Integer getRccid() {
		return rccid;
	}

	public void setRccid(Integer rccid) {
		this.rccid = rccid;
	}

	public Integer getIsGuide() {
		return isGuide;
	}

	public void setIsGuide(Integer isGuide) {
		this.isGuide = isGuide;
	}

	public Integer getFilterrule() {
		return filterrule;
	}

	public void setFilterrule(Integer filterrule) {
		this.filterrule = filterrule;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getRscid() {
		return rscid;
	}

	public void setRscid(String rscid) {
		this.rscid = rscid;
	}

	public String getInstallTime() {
		return installTime;
	}

	public void setInstallTime(String installTime) {
		this.installTime = installTime;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getRscName() {
		return rscName;
	}

	public void setRscName(String rscName) {
		this.rscName = rscName;
	}

	public String getRscVersion() {
		return rscVersion;
	}

	public void setRscVersion(String rscVersion) {
		this.rscVersion = rscVersion;
	}

	public String getManuName() {
		return manuName;
	}

	public void setManuName(String manuName) {
		this.manuName = manuName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getPsn() {
		return psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getHallno() {
		return hallno;
	}

	public void setHallno(String hallno) {
		this.hallno = hallno;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getAppUuid() {
		return appUuid;
	}

	public void setAppUuid(String appUuid) {
		this.appUuid = appUuid;
	}

	public String getPackUuid() {
		return packUuid;
	}

	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}

	public String getChanType() {
		return chanType;
	}

	public void setChanType(String chanType) {
		this.chanType = chanType;
	}

	public String getChanGrade() {
		return chanGrade;
	}

	public void setChanGrade(String chanGrade) {
		this.chanGrade = chanGrade;
	}

	public String getChanLevel() {
		return chanLevel;
	}

	public void setChanLevel(String chanLevel) {
		this.chanLevel = chanLevel;
	}

	public String getImoduletype() {
		return imoduletype;
	}

	public void setImoduletype(String imoduletype) {
		this.imoduletype = imoduletype;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public Integer getPkgId() {
		return pkgId;
	}

	public void setPkgId(Integer pkgId) {
		this.pkgId = pkgId;
	}

	public String getPkgName() {
		return pkgName;
	}

	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	
}

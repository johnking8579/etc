package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pcconfigtwo {

	private String areaCode;
	private String remoteadr;
	private Integer jifen;
	private Integer wushoujibb;
	private Integer yunyings;
	private Integer guanjiflag;
	private Integer qiangzhiflag;
	private Integer isyouxiaoqi;
	private Integer youxiaoqi;
	private Integer liantongtubiao;
	private String kuozhang1;
	private String kuozhang2;
	private String kuozhang3;
	private String kuozhang4;
	private String kuozhang5;
	private String kuozhang6;
	private String kuozhang7;
	private String kuozhang8;
	private String kuozhang9;
	private String kuozhang10;
	private String kuozhang11;
	private String kuozhang12;
	private String kuozhang13;
	private String kuozhang14;
	private String kuozhang15;
	private String kuozhang16;
	private String kuozhang17;
	private String kuozhang18;
	private String kuozhang19;
	private String kuozhang20;
	private String kuozhang21;
	private String kuozhang22;
	private String kuozhang23;
	private String kuozhang24;
	private String kuozhang25;
	private String kuozhang26;
	private String kuozhang27;
	private String kuozhang28;
	private String kuozhang29;
	private String kuozhang30;
	
	public Pcconfigtwo () {}

	@Id
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getRemoteadr() {
		return remoteadr;
	}

	public void setRemoteadr(String remoteadr) {
		this.remoteadr = remoteadr;
	}

	public Integer getJifen() {
		return jifen;
	}

	public void setJifen(Integer jifen) {
		this.jifen = jifen;
	}

	public Integer getWushoujibb() {
		return wushoujibb;
	}

	public void setWushoujibb(Integer wushoujibb) {
		this.wushoujibb = wushoujibb;
	}

	public Integer getYunyings() {
		return yunyings;
	}

	public void setYunyings(Integer yunyings) {
		this.yunyings = yunyings;
	}

	public Integer getGuanjiflag() {
		return guanjiflag;
	}

	public void setGuanjiflag(Integer guanjiflag) {
		this.guanjiflag = guanjiflag;
	}

	public Integer getQiangzhiflag() {
		return qiangzhiflag;
	}

	public void setQiangzhiflag(Integer qiangzhiflag) {
		this.qiangzhiflag = qiangzhiflag;
	}

	public Integer getIsyouxiaoqi() {
		return isyouxiaoqi;
	}

	public void setIsyouxiaoqi(Integer isyouxiaoqi) {
		this.isyouxiaoqi = isyouxiaoqi;
	}

	public Integer getYouxiaoqi() {
		return youxiaoqi;
	}

	public void setYouxiaoqi(Integer youxiaoqi) {
		this.youxiaoqi = youxiaoqi;
	}

	public Integer getLiantongtubiao() {
		return liantongtubiao;
	}

	public void setLiantongtubiao(Integer liantongtubiao) {
		this.liantongtubiao = liantongtubiao;
	}

	public String getKuozhang1() {
		return kuozhang1;
	}

	public void setKuozhang1(String kuozhang1) {
		this.kuozhang1 = kuozhang1;
	}

	public String getKuozhang2() {
		return kuozhang2;
	}

	public void setKuozhang2(String kuozhang2) {
		this.kuozhang2 = kuozhang2;
	}

	public String getKuozhang3() {
		return kuozhang3;
	}

	public void setKuozhang3(String kuozhang3) {
		this.kuozhang3 = kuozhang3;
	}

	public String getKuozhang4() {
		return kuozhang4;
	}

	public void setKuozhang4(String kuozhang4) {
		this.kuozhang4 = kuozhang4;
	}

	public String getKuozhang5() {
		return kuozhang5;
	}

	public void setKuozhang5(String kuozhang5) {
		this.kuozhang5 = kuozhang5;
	}

	public String getKuozhang6() {
		return kuozhang6;
	}

	public void setKuozhang6(String kuozhang6) {
		this.kuozhang6 = kuozhang6;
	}

	public String getKuozhang7() {
		return kuozhang7;
	}

	public void setKuozhang7(String kuozhang7) {
		this.kuozhang7 = kuozhang7;
	}

	public String getKuozhang8() {
		return kuozhang8;
	}

	public void setKuozhang8(String kuozhang8) {
		this.kuozhang8 = kuozhang8;
	}

	public String getKuozhang9() {
		return kuozhang9;
	}

	public void setKuozhang9(String kuozhang9) {
		this.kuozhang9 = kuozhang9;
	}

	public String getKuozhang10() {
		return kuozhang10;
	}

	public void setKuozhang10(String kuozhang10) {
		this.kuozhang10 = kuozhang10;
	}

	public String getKuozhang11() {
		return kuozhang11;
	}

	public void setKuozhang11(String kuozhang11) {
		this.kuozhang11 = kuozhang11;
	}

	public String getKuozhang12() {
		return kuozhang12;
	}

	public void setKuozhang12(String kuozhang12) {
		this.kuozhang12 = kuozhang12;
	}

	public String getKuozhang13() {
		return kuozhang13;
	}

	public void setKuozhang13(String kuozhang13) {
		this.kuozhang13 = kuozhang13;
	}

	public String getKuozhang14() {
		return kuozhang14;
	}

	public void setKuozhang14(String kuozhang14) {
		this.kuozhang14 = kuozhang14;
	}

	public String getKuozhang15() {
		return kuozhang15;
	}

	public void setKuozhang15(String kuozhang15) {
		this.kuozhang15 = kuozhang15;
	}

	public String getKuozhang16() {
		return kuozhang16;
	}

	public void setKuozhang16(String kuozhang16) {
		this.kuozhang16 = kuozhang16;
	}

	public String getKuozhang17() {
		return kuozhang17;
	}

	public void setKuozhang17(String kuozhang17) {
		this.kuozhang17 = kuozhang17;
	}

	public String getKuozhang18() {
		return kuozhang18;
	}

	public void setKuozhang18(String kuozhang18) {
		this.kuozhang18 = kuozhang18;
	}

	public String getKuozhang19() {
		return kuozhang19;
	}

	public void setKuozhang19(String kuozhang19) {
		this.kuozhang19 = kuozhang19;
	}

	public String getKuozhang20() {
		return kuozhang20;
	}

	public void setKuozhang20(String kuozhang20) {
		this.kuozhang20 = kuozhang20;
	}

	public String getKuozhang21() {
		return kuozhang21;
	}

	public void setKuozhang21(String kuozhang21) {
		this.kuozhang21 = kuozhang21;
	}

	public String getKuozhang22() {
		return kuozhang22;
	}

	public void setKuozhang22(String kuozhang22) {
		this.kuozhang22 = kuozhang22;
	}

	public String getKuozhang23() {
		return kuozhang23;
	}

	public void setKuozhang23(String kuozhang23) {
		this.kuozhang23 = kuozhang23;
	}

	public String getKuozhang24() {
		return kuozhang24;
	}

	public void setKuozhang24(String kuozhang24) {
		this.kuozhang24 = kuozhang24;
	}

	public String getKuozhang25() {
		return kuozhang25;
	}

	public void setKuozhang25(String kuozhang25) {
		this.kuozhang25 = kuozhang25;
	}

	public String getKuozhang26() {
		return kuozhang26;
	}

	public void setKuozhang26(String kuozhang26) {
		this.kuozhang26 = kuozhang26;
	}

	public String getKuozhang27() {
		return kuozhang27;
	}

	public void setKuozhang27(String kuozhang27) {
		this.kuozhang27 = kuozhang27;
	}

	public String getKuozhang28() {
		return kuozhang28;
	}

	public void setKuozhang28(String kuozhang28) {
		this.kuozhang28 = kuozhang28;
	}

	public String getKuozhang29() {
		return kuozhang29;
	}

	public void setKuozhang29(String kuozhang29) {
		this.kuozhang29 = kuozhang29;
	}

	public String getKuozhang30() {
		return kuozhang30;
	}

	public void setKuozhang30(String kuozhang30) {
		this.kuozhang30 = kuozhang30;
	}
	

}

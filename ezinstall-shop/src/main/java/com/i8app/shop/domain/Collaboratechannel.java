package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Collaboratechannel {

	
	private Integer id;
	
	private String ccName;
	
	private String ccDesc;
	
	private Channeltype ctid;

	@Id
	@GeneratedValue
	@Column(name="ccId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getCcDesc() {
		return ccDesc;
	}

	public void setCcDesc(String ccDesc) {
		this.ccDesc = ccDesc;
	}
	
	@ManyToOne
	@JoinColumn(name="ctid")
	public Channeltype getCtid() {
		return ctid;
	}

	public void setCtid(Channeltype ctid) {
		this.ctid = ctid;
	}

}

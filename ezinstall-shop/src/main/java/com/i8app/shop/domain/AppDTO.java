package com.i8app.shop.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "app")
public class AppDTO {

	private String uuid;
	private ResourceType resourceType;
	private String icon;
	private String name, pinyin, developer, info, infoHtml, markerType, cpId, originalAppId, originalTypeId;
	private Integer isAreaCustom, isDel, downCount, installCount;
	private Date updateTime;

	@Id
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@ManyToOne
	@JoinColumn(name="rscTypeId")
	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfoHtml() {
		return infoHtml;
	}

	public void setInfoHtml(String infoHtml) {
		this.infoHtml = infoHtml;
	}
	
	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOriginalAppId() {
		return originalAppId;
	}

	public void setOriginalAppId(String originalAppId) {
		this.originalAppId = originalAppId;
	}

	public String getOriginalTypeId() {
		return originalTypeId;
	}

	public void setOriginalTypeId(String originalTypeId) {
		this.originalTypeId = originalTypeId;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getIsAreaCustom() {
		return isAreaCustom;
	}

	public void setIsAreaCustom(Integer isAreaCustom) {
		this.isAreaCustom = isAreaCustom;
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	public Integer getDownCount() {
		return downCount;
	}

	public void setDownCount(Integer downCount) {
		this.downCount = downCount;
	}

	public Integer getInstallCount() {
		return installCount;
	}

	public void setInstallCount(Integer installCount) {
		this.installCount = installCount;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
}

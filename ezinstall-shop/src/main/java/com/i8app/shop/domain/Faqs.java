package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "faqinfo")
public class Faqs {
	private Integer id;
	private Employee emp;
	private String question;
	private String answer;
	private Integer checked;
	private String submitTime;
	private FaqType faqType;

	@Id
	@GeneratedValue
	@Column(name="fid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "empId")
	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	@Column(name="problem",length = 500)
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(length = 500)
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Column(name="shenhe")
	public Integer getChecked() {
		return checked;
	}
	
	public void setChecked(Integer checked) {
		this.checked = checked;
	}
	

	@Column(name="fdate",length = 100)
	public String getSubmitTime() {
		return submitTime;
	}


	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	@ManyToOne
	@JoinColumn(name ="fAQTypeid")
	public FaqType getFaqType() {
		return faqType;
	}

	public void setFaqType(FaqType faqType) {
		this.faqType = faqType;
	}


}

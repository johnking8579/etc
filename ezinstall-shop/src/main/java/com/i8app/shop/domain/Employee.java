package com.i8app.shop.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author JingYing
 */
@Entity
@Table(name = "employeeinfo")
public class Employee implements Serializable{

	private Integer id;// EmpID
	private String name;
	private String empNo;
	private String empPwd;	//MD5格式
	private Integer orgId;	//如果orgLevel为4, 则本字段为营业厅ID
	private Integer orgLevel;
	private Integer isDeleted;
	private Integer isFreezed;
	private String orgNameStr;
	private String orgIdStr;
	private String phoneNumber;
	private String bank, bankUser, bankAccount;
	private Province province;
	private City city;
	private County county;

	public Employee() {
	}

	public Employee(int id) {
		this.id = id;
	}

	@Id
	@GeneratedValue
	@Column(name = "empId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "empName", length = 32)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 32)
	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	@Column(length = 32)
	public String getEmpPwd() {
		return empPwd;
	}

	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getOrgLevel() {
		return orgLevel;
	}

	public void setOrgLevel(Integer orgLevel) {
		this.orgLevel = orgLevel;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getIsFreezed() {
		return isFreezed;
	}

	public void setIsFreezed(Integer isFreezed) {
		this.isFreezed = isFreezed;
	}

	public String getOrgNameStr() {
		return orgNameStr;
	}

	public void setOrgNameStr(String orgNameStr) {
		this.orgNameStr = orgNameStr;
	}

	public String getOrgIdStr() {
		return orgIdStr;
	}

	public void setOrgIdStr(String orgIdStr) {
		this.orgIdStr = orgIdStr;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getBankUser() {
		return bankUser;
	}

	public void setBankUser(String bankUser) {
		this.bankUser = bankUser;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	@ManyToOne
	@JoinColumn(name="provId")
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	@ManyToOne
	@JoinColumn(name="cityId")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne
	@JoinColumn(name="countyId")
	public County getCounty() {
		return county;
	}

	public void setCounty(County county) {
		this.county = county;
	}
}

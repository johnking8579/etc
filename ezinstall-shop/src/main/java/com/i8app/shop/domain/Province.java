package com.i8app.shop.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "provinceinfo")
public class Province implements Serializable {
	private Integer id;
	private String name, provinceCode, unionreadCode;
	private Set<City> citySet;

	@Id
	@GeneratedValue
	@Column(name="provinceId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="provinceName")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy="province")
	@OrderBy(value="id")
	@JoinColumn(name="provinceId")
	public Set<City> getCitySet() {
		return citySet;
	}

	public void setCitySet(Set<City> citySet) {
		this.citySet = citySet;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getUnionreadCode() {
		return unionreadCode;
	}

	public void setUnionreadCode(String unionreadCode) {
		this.unionreadCode = unionreadCode;
	}
	
}

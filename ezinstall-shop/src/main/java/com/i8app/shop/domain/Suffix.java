package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="softsuffix")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Suffix{
	
	@Id
	@GeneratedValue
	@Column(name = "suffixId")
	private Integer id;

	@Column(name="suffixName")
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

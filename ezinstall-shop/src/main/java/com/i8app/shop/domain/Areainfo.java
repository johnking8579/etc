package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * easyinfo数据库的areainfo表, 和db_tms_two表的areainfo不同
 * 
 * @author JingYing 2013-6-9
 */
@Entity
public class Areainfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String areaCode;
	private String areaName;
	private Integer isSpecified;
	private Integer operator;	//运营商
	private String memo;

	/** default constructor */
	public Areainfo() {	}

	/** minimal constructor */
	public Areainfo(String areaCode) {
		this.areaCode = areaCode;
	}

	/** full constructor */
	public Areainfo(String areaCode, String areaName, 
			Integer isSpecified, Integer operator, 
			String memo) {
		this.areaCode = areaCode;
		this.areaName = areaName;
		this.isSpecified = isSpecified;
		this.operator = operator;
		this.memo = memo;
	}

	@Id
	public String getAreaCode() {
		return this.areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getIsSpecified() {
		return this.isSpecified;
	}

	public void setIsSpecified(Integer isSpecified) {
		this.isSpecified = isSpecified;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getOperator() {
		return operator;
	}

	public void setOperator(Integer operator) {
		this.operator = operator;
	}
}
package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class OsInfo {
	private Integer id;
	private Integer osType;
	private String osVersion;
	private Suffix suffix1;
	private Suffix suffix2;
	private Suffix suffix3;
	private Suffix suffix4;
	private Suffix suffix5;
	private Suffix suffix6;
	
	@Id
	@Column(name="osId")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	
	@Column(length=128)
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	@ManyToOne(fetch=FetchType.LAZY)	
	@JoinColumn(name="suffix1")
	public Suffix getSuffix1() {
		return suffix1;
	}
	public void setSuffix1(Suffix suffix1) {
		this.suffix1 = suffix1;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffix2")
	public Suffix getSuffix2() {
		return suffix2;
	}
	public void setSuffix2(Suffix suffix2) {
		this.suffix2 = suffix2;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffix3")
	public Suffix getSuffix3() {
		return suffix3;
	}
	public void setSuffix3(Suffix suffix3) {
		this.suffix3 = suffix3;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffix4")
	public Suffix getSuffix4() {
		return suffix4;
	}
	public void setSuffix4(Suffix suffix4) {
		this.suffix4 = suffix4;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffix5")
	public Suffix getSuffix5() {
		return suffix5;
	}
	public void setSuffix5(Suffix suffix5) {
		this.suffix5 = suffix5;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffix6")
	public Suffix getSuffix6() {
		return suffix6;
	}
	public void setSuffix6(Suffix suffix6) {
		this.suffix6 = suffix6;
	}
	

}

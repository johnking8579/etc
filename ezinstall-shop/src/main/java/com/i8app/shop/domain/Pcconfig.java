package com.i8app.shop.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Pcconfig entity.
 * 
 * @author MyEclipse Persistence Tools
 */

@Entity
public class Pcconfig implements java.io.Serializable {

	// Fields

	private String areaCode;
	private String remoteadr;
	private Integer jifen;
	private Integer wushoujibb;
	private Integer yunyings;
	private Integer guanjiflag;
	private Integer qiangzhiflag;
	private Integer isyouxiaoqi;
	private Integer youxiaoqi;
	private Integer liantongtubiao;
	private Integer kuozhang1;
	private Integer kuozhang2;
	private Integer kuozhang3;
	private Integer kuozhang4;
	private Integer kuozhang5;
	private Integer kuozhang6;
	private Integer kuozhang7;
	private Integer kuozhang8;
	private Integer kuozhang9;
	private Integer kuozhang10;
	private String kuozhang11;
	private String kuozhang12;
	private String kuozhang13;
	private String kuozhang14;
	private String kuozhang15;

	// Constructors

	/** default constructor */
	public Pcconfig() {
	}

	/** minimal constructor */
	public Pcconfig(String areaCode) {
		this.areaCode = areaCode;
	}

	/** full constructor */
	public Pcconfig(String areaCode, String remoteadr, Integer jifen,
			Integer wushoujibb, Integer yunyings, Integer guanjiflag,
			Integer qiangzhiflag, Integer isyouxiaoqi, Integer youxiaoqi,
			Integer liantongtubiao, Integer kuozhang1, Integer kuozhang2,
			Integer kuozhang3, Integer kuozhang4, Integer kuozhang5,
			Integer kuozhang6, Integer kuozhang7, Integer kuozhang8,
			Integer kuozhang9, Integer kuozhang10, String kuozhang11,
			String kuozhang12, String kuozhang13, String kuozhang14,
			String kuozhang15) {
		this.areaCode = areaCode;
		this.remoteadr = remoteadr;
		this.jifen = jifen;
		this.wushoujibb = wushoujibb;
		this.yunyings = yunyings;
		this.guanjiflag = guanjiflag;
		this.qiangzhiflag = qiangzhiflag;
		this.isyouxiaoqi = isyouxiaoqi;
		this.youxiaoqi = youxiaoqi;
		this.liantongtubiao = liantongtubiao;
		this.kuozhang1 = kuozhang1;
		this.kuozhang2 = kuozhang2;
		this.kuozhang3 = kuozhang3;
		this.kuozhang4 = kuozhang4;
		this.kuozhang5 = kuozhang5;
		this.kuozhang6 = kuozhang6;
		this.kuozhang7 = kuozhang7;
		this.kuozhang8 = kuozhang8;
		this.kuozhang9 = kuozhang9;
		this.kuozhang10 = kuozhang10;
		this.kuozhang11 = kuozhang11;
		this.kuozhang12 = kuozhang12;
		this.kuozhang13 = kuozhang13;
		this.kuozhang14 = kuozhang14;
		this.kuozhang15 = kuozhang15;
	}

	// Property accessors

	@Id
	public String getAreaCode() {
		return this.areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getRemoteadr() {
		return this.remoteadr;
	}

	public void setRemoteadr(String remoteadr) {
		this.remoteadr = remoteadr;
	}

	public Integer getJifen() {
		return this.jifen;
	}

	public void setJifen(Integer jifen) {
		this.jifen = jifen;
	}

	public Integer getWushoujibb() {
		return this.wushoujibb;
	}

	public void setWushoujibb(Integer wushoujibb) {
		this.wushoujibb = wushoujibb;
	}

	public Integer getYunyings() {
		return this.yunyings;
	}

	public void setYunyings(Integer yunyings) {
		this.yunyings = yunyings;
	}

	public Integer getGuanjiflag() {
		return this.guanjiflag;
	}

	public void setGuanjiflag(Integer guanjiflag) {
		this.guanjiflag = guanjiflag;
	}

	public Integer getQiangzhiflag() {
		return this.qiangzhiflag;
	}

	public void setQiangzhiflag(Integer qiangzhiflag) {
		this.qiangzhiflag = qiangzhiflag;
	}

	public Integer getIsyouxiaoqi() {
		return this.isyouxiaoqi;
	}

	public void setIsyouxiaoqi(Integer isyouxiaoqi) {
		this.isyouxiaoqi = isyouxiaoqi;
	}

	public Integer getYouxiaoqi() {
		return this.youxiaoqi;
	}

	public void setYouxiaoqi(Integer youxiaoqi) {
		this.youxiaoqi = youxiaoqi;
	}

	public Integer getLiantongtubiao() {
		return this.liantongtubiao;
	}

	public void setLiantongtubiao(Integer liantongtubiao) {
		this.liantongtubiao = liantongtubiao;
	}

	public Integer getKuozhang1() {
		return this.kuozhang1;
	}

	public void setKuozhang1(Integer kuozhang1) {
		this.kuozhang1 = kuozhang1;
	}

	public Integer getKuozhang2() {
		return this.kuozhang2;
	}

	public void setKuozhang2(Integer kuozhang2) {
		this.kuozhang2 = kuozhang2;
	}

	public Integer getKuozhang3() {
		return this.kuozhang3;
	}

	public void setKuozhang3(Integer kuozhang3) {
		this.kuozhang3 = kuozhang3;
	}

	public Integer getKuozhang4() {
		return this.kuozhang4;
	}

	public void setKuozhang4(Integer kuozhang4) {
		this.kuozhang4 = kuozhang4;
	}

	public Integer getKuozhang5() {
		return this.kuozhang5;
	}

	public void setKuozhang5(Integer kuozhang5) {
		this.kuozhang5 = kuozhang5;
	}

	public Integer getKuozhang6() {
		return this.kuozhang6;
	}

	public void setKuozhang6(Integer kuozhang6) {
		this.kuozhang6 = kuozhang6;
	}

	public Integer getKuozhang7() {
		return this.kuozhang7;
	}

	public void setKuozhang7(Integer kuozhang7) {
		this.kuozhang7 = kuozhang7;
	}

	public Integer getKuozhang8() {
		return this.kuozhang8;
	}

	public void setKuozhang8(Integer kuozhang8) {
		this.kuozhang8 = kuozhang8;
	}

	public Integer getKuozhang9() {
		return this.kuozhang9;
	}

	public void setKuozhang9(Integer kuozhang9) {
		this.kuozhang9 = kuozhang9;
	}

	public Integer getKuozhang10() {
		return this.kuozhang10;
	}

	public void setKuozhang10(Integer kuozhang10) {
		this.kuozhang10 = kuozhang10;
	}

	public String getKuozhang11() {
		return this.kuozhang11;
	}

	public void setKuozhang11(String kuozhang11) {
		this.kuozhang11 = kuozhang11;
	}

	public String getKuozhang12() {
		return this.kuozhang12;
	}

	public void setKuozhang12(String kuozhang12) {
		this.kuozhang12 = kuozhang12;
	}

	public String getKuozhang13() {
		return this.kuozhang13;
	}

	public void setKuozhang13(String kuozhang13) {
		this.kuozhang13 = kuozhang13;
	}

	public String getKuozhang14() {
		return this.kuozhang14;
	}

	public void setKuozhang14(String kuozhang14) {
		this.kuozhang14 = kuozhang14;
	}

	public String getKuozhang15() {
		return this.kuozhang15;
	}

	public void setKuozhang15(String kuozhang15) {
		this.kuozhang15 = kuozhang15;
	}

}
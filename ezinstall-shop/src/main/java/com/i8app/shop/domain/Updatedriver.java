package com.i8app.shop.domain;

/**
 * Updatedriver entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Updatedriver implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer type;
	private String version;
	private String address;

	// Constructors

	/** default constructor */
	public Updatedriver() {
	}

	/** minimal constructor */
	public Updatedriver(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public Updatedriver(Integer id, Integer type, String version,
			String address) {
		this.id = id;
		this.type = type;
		this.version = version;
		this.address = address;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
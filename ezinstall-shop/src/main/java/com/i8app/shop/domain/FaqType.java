package com.i8app.shop.domain;

import javax.persistence.*;

@Entity
@Table(name="faqTypeinfo")
public class FaqType {

	private Integer id;
	private String typeName;

	@Id
	@GeneratedValue
	@Column(name="fAQTypeid")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}

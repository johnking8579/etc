package com.i8app.shop.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 该类不完整
 * @author JingYing
 *
 */
@Entity
public class BusinessHall implements Serializable {
	
	private Integer id;
	private String hallName, hallAddr, description, hallNo;
	private County county;
	private City city;
	private Province province;
//	private Collaboratechannel cc;
//	private Reifiercollaboratechannel rcc;
	private Integer ccid, rccid, ctid;
	private int bhid;
	private boolean hasLogined;
//	private Channeltype ct;
	private Date deployTime;
	private String chanType, chanGrade, chanLevel;
	
	public BusinessHall()	{}
	
	public BusinessHall(int id)	{
		this.id = id;
	}

	@Id
	@GeneratedValue
	@Column(name="hallId")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length=128)
	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public String getHallAddr() {
		return hallAddr;
	}

	public void setHallAddr(String hallAddr) {
		this.hallAddr = hallAddr;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToOne
	@JoinColumn(name="ProvinceId")
	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
	
	@ManyToOne
	@JoinColumn(name="cityId")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne
	@JoinColumn(name="countyId")
	public County getCounty() {
		return county;
	}

	public void setCounty(County county) {
		this.county = county;
	}

	@Column(name="has_logined")
	public boolean getHasLogined() {
		return hasLogined;
	}

	public void setHasLogined(boolean hasLogined) {
		this.hasLogined = hasLogined;
	}

	public String getHallNo() {
		return hallNo;
	}

	public void setHallNo(String hallNo) {
		this.hallNo = hallNo;
	}

	public int getBhid() {
		return bhid;
	}

	public void setBhid(int bhid) {
		this.bhid = bhid;
	}

//	@ManyToOne
//	@JoinColumn(name="ctId")
//	public Channeltype getCt() {
//		return ct;
//	}
//
//	public void setCt(Channeltype ct) {
//		this.ct = ct;
//	}
//	
//	
//
//	@ManyToOne
//	@JoinColumn(name="ccId")
//	public Collaboratechannel getCc() {
//		return cc;
//	}
//
//	public void setCc(Collaboratechannel cc) {
//		this.cc = cc;
//	}
//	
//	@ManyToOne
//	@JoinColumn(name="rccId")
//	public Reifiercollaboratechannel getRcc() {
//		return rcc;
//	}
//
//	public void setRcc(Reifiercollaboratechannel rcc) {
//		this.rcc = rcc;
//	}
	
	public Integer getCcid() {
		return ccid;
	}

	public Integer getCtid() {
		return ctid;
	}

	public void setCtid(Integer ctid) {
		this.ctid = ctid;
	}

	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

	public Integer getRccid() {
		return rccid;
	}

	public void setRccid(Integer rccid) {
		this.rccid = rccid;
	}

	public Date getDeployTime() {
		return deployTime;
	}

	public void setDeployTime(Date deployTime) {
		this.deployTime = deployTime;
	}

	public String getChanType() {
		return chanType;
	}

	public void setChanType(String chanType) {
		this.chanType = chanType;
	}

	public String getChanGrade() {
		return chanGrade;
	}

	public void setChanGrade(String chanGrade) {
		this.chanGrade = chanGrade;
	}

	public String getChanLevel() {
		return chanLevel;
	}

	public void setChanLevel(String chanLevel) {
		this.chanLevel = chanLevel;
	}
}

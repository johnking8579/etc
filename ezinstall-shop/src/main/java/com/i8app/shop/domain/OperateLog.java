package com.i8app.shop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "activedegree")
public class OperateLog {

	private Integer id;
	private Integer empId;
	private String empName;
	private String orgIdStr;
	private String orgNameStr;
	private String operateTag;
	private String clientType;
	private String operateTime;
	private Integer actionId;
	private String psn;
	private String provinceName, cityName, countyName, hallName, hallno, empno;
	private Integer provinceId, cityId, countyId, hallId, bhid;

	public OperateLog() {
	}

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "EmpId")
	public Integer getEmpId() {
		return this.empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	@Column(name = "EmpName", length = 32)
	public String getEmpName() {
		return this.empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Column(name = "OrgIdStr", length = 100)
	public String getOrgIdStr() {
		return this.orgIdStr;
	}

	public void setOrgIdStr(String orgIdStr) {
		this.orgIdStr = orgIdStr;
	}

	@Column(name = "OrgNameStr", length = 100)
	public String getOrgNameStr() {
		return this.orgNameStr;
	}

	public void setOrgNameStr(String orgNameStr) {
		this.orgNameStr = orgNameStr;
	}

	@Column(name = "OperateTag", length = 10)
	public String getOperateTag() {
		return this.operateTag;
	}

	public void setOperateTag(String operateTag) {
		this.operateTag = operateTag;
	}

	@Column(name = "clientType", length = 2)
	public String getClientType() {
		return this.clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	@Column(name = "OperateTime", length = 25)
	public String getOperateTime() {
		return this.operateTime;
	}

	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}

	@Column(name = "ActionId")
	public Integer getActionId() {
		return this.actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public String getPsn() {
		return psn;
	}

	public void setPsn(String psn) {
		this.psn = psn;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public String getHallno() {
		return hallno;
	}

	public void setHallno(String hallno) {
		this.hallno = hallno;
	}

	public String getEmpno() {
		return empno;
	}

	public void setEmpno(String empno) {
		this.empno = empno;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getCountyId() {
		return countyId;
	}

	public void setCountyId(Integer countyId) {
		this.countyId = countyId;
	}

	public Integer getHallId() {
		return hallId;
	}

	public void setHallId(Integer hallId) {
		this.hallId = hallId;
	}

	public Integer getBhid() {
		return bhid;
	}

	public void setBhid(Integer bhid) {
		this.bhid = bhid;
	}
}
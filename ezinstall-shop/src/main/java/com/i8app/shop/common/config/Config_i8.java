package com.i8app.shop.common.config;

import java.util.Map;

public class Config_i8 extends Config {

	private Map<String, int[]> areaProvMap;
	private Map<Integer, String> provAreaMap;

	public Map<String, int[]> getAreaProvMap() {
		return areaProvMap;
	}

	public void setAreaProvMap(Map<String, int[]> areaProvMap) {
		this.areaProvMap = areaProvMap;
	}

	public Map<Integer, String> getProvAreaMap() {
		return provAreaMap;
	}

	public void setProvAreaMap(Map<Integer, String> provAreaMap) {
		this.provAreaMap = provAreaMap;
	}
	
}

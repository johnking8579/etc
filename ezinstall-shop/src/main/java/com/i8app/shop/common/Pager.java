package com.i8app.shop.common;

import java.util.List;

/**
 * PAGER-TAGLIB用分页模型
 * @author jing
 * @param <T>
 */
public class Pager <T> {
	
	private int totalCount, pageNo, pageSize;
	
	private int source;	//资源的来源方, 具体值查看com.i8app.shop.Source
	
	private List<T> list;
	
	public Pager() {}
	
	public Pager(com.i8app.ezinstall.common.Pager<T> ezinstallPager)	{
		this.list = ezinstallPager.getList();
		this.totalCount = ezinstallPager.getTotalCount();
	}
	
	public Pager(List<T> list, int totalCount)	{
		this.list = list;
		this.totalCount = totalCount;
	}
	
	
	public Pager(List<T> list, int totalCount, int pageNo, int pageSize)	{
		this.list = list;
		this.totalCount = totalCount;
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}
	
	
	/**
	 * 把一个整集合list, 根据所需要的offset和pageSize, 截取成一个分页模型
	 * @param list
	 * @param offset
	 * @param pageSize
	 */
	public Pager(List<T> list , int offset, int pageSize)	{
		this.totalCount = list.size();
		int bound = offset + pageSize > list.size() ? list.size() : offset + pageSize;
		this.list = list.subList(offset, bound);
	}
	
	/**
	 * 根据totalCount, pageSize计算总页数
	 * @return
	 */
	public int getTotalPages() {
	   return (totalCount + pageSize - 1) / pageSize;
	}
	
	
	/****************************************************
	 * 以下是访问器
	 ************************************************/
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}
}

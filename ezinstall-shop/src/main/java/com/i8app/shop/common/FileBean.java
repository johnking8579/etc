package com.i8app.shop.common;

import java.io.File;
import java.util.List;

/**
 * 使用合成模式来体现树形关系. 并组合FILE类简化FILEBEAN的实现方式.
 * @author JingYing
 *
 */
public class FileBean extends DiskElement	{
	
	public FileBean(File file)	{
		super(file);
	}

	@Override
	public List<DiskElement> getChildren() {
		return null;
	}

	@Override
	public String getSuffix() {
		String s = file.getName();
		return s.substring(s.lastIndexOf(".") + 1);
	}

}

package com.i8app.shop.common;

/**
 * 进行验证码发送和检查时使用的异常, 做返回值使用
 * @author jing
 *
 */
public class AuthcodeException extends Exception	{

	public AuthcodeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuthcodeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AuthcodeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AuthcodeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

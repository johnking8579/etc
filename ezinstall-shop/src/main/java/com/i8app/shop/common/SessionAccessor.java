package com.i8app.shop.common;

import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.TypeInfo;

/**
 * Session访问器, 用来操作Session里的信息, Action层和Service层统一在此处调用或删除Session信息
 * 前端页面不使用这个类, 但前台页面在调用 Session信息时需要参照本类中制定的命名规则
 * 
 * @author jing
 */
public class SessionAccessor {
	
	public static final String 
		AUTHCODE = "authcode";
	

	String phoneIMEI, areaCode;

	/**
	 * smsSwitch参数需要在页面上直接使用{sessionScope.smsSwitch}来调用
	 */
	Integer smsSwitch;
	
	/*营业厅人员相关信息*/
	public static String getAreaCode() {
		return (String) getSession().getAttribute("areaCode");
	}

	public static Employee getEmp() {
		return (Employee) getSession().getAttribute("emp");
	}

	/**
	 * 页面上使用了该变量
	 * @return
	 */
	public static Integer getSmsSwitch() {
		return (Integer) getSession().getAttribute("smsSwitch");
	}
	
	public static Integer getHasFaqed() {
		return (Integer) getSession().getAttribute("hasFaqed");
	}
	
	public static Integer getLastCommented() {
		return (Integer) getSession().getAttribute("lastCommented");
	}
	
	public static String getPhoneIMEI() {
		return (String) getSession().getAttribute("phoneIMEI");
	}

	public static PhoneOs getPhoneOs() {
		return (PhoneOs) getSession().getAttribute("phoneOs");
	}

	public static HttpSession getSession() {
		return ServletActionContext.getRequest().getSession();
	}

	public static TypeInfo getTypeInfo() {
		return (TypeInfo) getSession().getAttribute("typeInfo");
	}

	public static SessionAccessor removeAreaCode() {
		getSession().removeAttribute("areaCode");
		return new SessionAccessor();
	}

	public static SessionAccessor removeEmp() {
		getSession().removeAttribute("emp");
		return new SessionAccessor();
	}

	public static SessionAccessor removeLastCommented() {
		getSession().removeAttribute("lastCommented");
		return new SessionAccessor();
	}

	public static SessionAccessor removeOperator() {
		getSession().removeAttribute("operator");
		return new SessionAccessor();
	}

	public static SessionAccessor removePhoneIMEI() {
		getSession().removeAttribute("phoneIMEI");
		return new SessionAccessor();
	}

	public static SessionAccessor removePhoneOs() {
		getSession().removeAttribute("phoneOs");
		return new SessionAccessor();
	}

	public static SessionAccessor removeSmsSwitch() {
		getSession().removeAttribute("smsSwitch");
		return new SessionAccessor();
	}

	public static SessionAccessor removeTypeInfo() {
		getSession().removeAttribute("typeInfo");
		return new SessionAccessor();
	}

	public static SessionAccessor setAreaCode(String areaCode) {
		getSession().setAttribute("areaCode", areaCode);
		return new SessionAccessor();
	}

	public static SessionAccessor setEmp(Employee emp) {
		getSession().setAttribute("emp", emp);
		return new SessionAccessor();
	}

	public static SessionAccessor setLastCommented(Integer i) {
		getSession().setAttribute("lastCommented", i);
		return new SessionAccessor();
	}

	public static SessionAccessor setPhoneOs(PhoneOs os) {
		getSession().setAttribute("phoneOs", os);
		return new SessionAccessor();
	}

	public static SessionAccessor setSmsSwitch(Integer smsSwitch) {
		getSession().setAttribute("smsSwitch", smsSwitch);
		return new SessionAccessor();
	}

	public static SessionAccessor setTypeInfo(TypeInfo typeInfo) {
		getSession().setAttribute("typeInfo", typeInfo);
		return new SessionAccessor();
	}

	public static SessionAccessor setVersion(Integer version) {
		getSession().setAttribute("version", version);
		return new SessionAccessor();
	}

	public static SessionAccessor setHasFaqed(Integer hasFaqed) {
		getSession().setAttribute("hasFaqed", hasFaqed);
		return new SessionAccessor();
	}
	
	public static SessionAccessor setPhoneIMEI(String phoneIMEI)	{
		getSession().setAttribute("phoneIMEI", phoneIMEI);
		return new SessionAccessor();
	}
	
	public static SessionAccessor removeHasFaqed()	{
		getSession().removeAttribute("hasFaqed");
		return new SessionAccessor();
	}
	
	public static UUID getPhoneLoginUuid()	{
		return (UUID)getSession().getAttribute("uuid");
	}
	
	public static void setPhoneLoginUuid(UUID uuid)	{
		getSession().setAttribute("uuid", uuid);
	}
	
	public static SessionAccessor removePhoneLoginUuid()	{
		getSession().removeAttribute("uuid");
		return new SessionAccessor();
	}
	
	public static SessionAccessor setOsVersion(Integer osVersion)	{		//将转换后的操作系统版本号放入session
		getSession().setAttribute("osVersion", osVersion);
		return new SessionAccessor();
	}
	
	public static Integer getOsVersion()	{
		return (Integer)getSession().getAttribute("osVersion");
	}
	
	public static SessionAccessor removeOsVersion()	{
		getSession().removeAttribute("osVersion");
		return new SessionAccessor();
	}
	
	public static SessionAccessor setAuthcode(String authcode)	{
		getSession().setAttribute(AUTHCODE, authcode);
		return new SessionAccessor();
	}
	
	public static String getAuthcode()	{
		return (String)getSession().getAttribute(AUTHCODE);
	}
	
	public static SessionAccessor removeAuthcode()	{
		getSession().removeAttribute(AUTHCODE);
		return new SessionAccessor();
	}

}

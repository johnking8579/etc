package com.i8app.shop.common;

import java.io.File;
import java.util.List;

import com.i8app.shop.common.config.ConfigProp;

/**
 * filebean和folder的父类.
 * 使用合成模式来体现树形关系. 并组合FILE类简化子类的实现方式.
 * @author JingYing
 *
 */
public abstract class DiskElement {
	
	protected File file;
	
	public DiskElement(File file)	{
		this.file = file;
	}
	
	public abstract List<DiskElement> getChildren();
	
	public abstract String getSuffix();

	public String getStr()	{
		return file.toString();
	}
	
	public String toHttpPath()	{
		ConfigProp cp = ConfigProp.getInstance();
		String s = file.toString().replace(cp.getFileServerDir(), cp.getFileServerUrl());
		return s.replace("\\", "/");
	}
	
	public String getName()	{
		return file.getName();
	}
}

package com.i8app.shop.common;

import java.io.UnsupportedEncodingException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * 3DES加解密
 * 
 * @author JingYing 2013-7-8
 */
public class TripleDES {

	/**
	 * 加密方法
	 * @param src 源数据的字节数组
	 * @return
	 */
	public static byte[] encrypt(byte[] src, String secretKey) {
		try {
			SecretKey deskey = new SecretKeySpec(build3DesKey(secretKey),"DESede"); // 生成密钥
			Cipher c1 = Cipher.getInstance("DESede"); // 实例化负责加密/解密的Cipher工具类
			c1.init(Cipher.ENCRYPT_MODE, deskey); // 初始化为加密模式
			return c1.doFinal(src);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 解密函数
	 * @param src 密文的字节数组
	 * @return
	 */
	public static byte[] decrypt(byte[] src, String secretKey) throws BadPaddingException {
			try {
				SecretKey deskey = new SecretKeySpec(build3DesKey(secretKey),"DESede");
				Cipher c1 = Cipher.getInstance("DESede");
				c1.init(Cipher.DECRYPT_MODE, deskey); // 初始化为解密模式
				return c1.doFinal(src);
			} catch (BadPaddingException e) {
				throw e;
			} catch(Exception e)	{
				throw new RuntimeException(e);
			}
	}

	/**
	 * 根据字符串生成密钥字节数组
	 * @param keyStr 密钥字符串
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] build3DesKey(String secretKey)
			throws UnsupportedEncodingException {
		byte[] key = new byte[24]; // 声明一个24位的字节数组，默认里面都是0
		byte[] temp = secretKey.getBytes("UTF-8"); // 将字符串转成字节数组

		//执行数组拷贝 System.arraycopy(源数组，从源数组哪里开始拷贝，目标数组，拷贝多少位)
		if (key.length > temp.length) {
			// 如果temp不够24位，则拷贝temp数组整个长度的内容到key数组中
			System.arraycopy(temp, 0, key, 0, temp.length);
		} else {
			// 如果temp大于24位，则拷贝temp数组24个长度的内容到key数组中
			System.arraycopy(temp, 0, key, 0, key.length);
		}
		return key;
	}

	public static void main(String[] args) throws Exception {
		// 加密
		byte[] secretArr = TripleDES.encrypt("3DES加密解密案例".getBytes("utf-8"),
				"123456");
		System.out.println("【加密后】：" + new String(secretArr, "utf-8"));

		// 解密
		byte[] myMsgArr = TripleDES.decrypt(secretArr, "123456");
		System.out.println("【解密后】：" + new String(myMsgArr, "utf-8"));
	}
}

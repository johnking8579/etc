package com.i8app.shop.common.config;

public abstract class Config {

	/**
	 * 图片头地址; 渠道号; 下载地址的格式; 服务器地址的头路径;
	 */
	protected String imgBasePath, channelId, downUrl, baseUrl;

	public String getImgBasePath() {
		return imgBasePath;
	}

	public void setImgBasePath(String imgBasePath) {
		this.imgBasePath = imgBasePath;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getDownUrl() {
		return downUrl;
	}

	public void setDownUrl(String downUrl) {
		this.downUrl = downUrl;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}

package com.i8app.shop.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.shop.dao.AppInstallPackDao;
import com.i8app.shop.dao.BizAreaDao;
import com.i8app.shop.dao.PkgDao;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.Pkg;
import com.i8app.shop.domain.PkgGroup;

@Service
public class AppService {
	
	@Resource
	private BizAreaDao bizAreaDao;
	@Resource
	private PkgDao pkgDao;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private AppInstallPackDao packDao;
	
	public List<Pkg> findPkgByBizId(int bizId, AreaParam areaParam)	{
		List<PkgGroup> groups = bizAreaDao.findByBizId(bizId, areaParam);
		List<Pkg> pkgs = new ArrayList<Pkg>();
		for(PkgGroup pg : groups)	{
			pkgs.addAll(pkgDao.findByGroupId(pg.getId()));
		}
		return pkgs;
	}
	
	public List<PkgGroup> findPkgGroupByBizId(int bizId, AreaParam areaParam)	{
		return bizAreaDao.findByBizId(bizId, areaParam);
	}
	
	public List<Pkg> findPkgByGroupId(int groupId)	{
		return pkgDao.findByGroupId(groupId);
	}
	
	public AppInstallPack findPackById(String id)	{
		return packDao.findById(id);
	}
	
	public Pager<AppDTO> findInstallPackByPkg(AreaParam areaParam, int pkgId, Os os, int offset, int pageSize)	{
		return appQuerier.findInstallPackByPkg(areaParam, pkgId, os, offset, pageSize);
	}

}

package com.i8app.shop.service;

import java.io.File;

import com.i8app.shop.common.DiskElement;
import com.i8app.shop.common.FileBean;
import com.i8app.shop.common.Folder;
import com.i8app.shop.common.config.ConfigProp;

/**
 * diskElement的工厂类
 * @author JingYing
 *
 */
public class DiskElementFactory {
	
	public static final String DANG_JIAN = "\\dangjian\\";
	
	public static final String 
		baseHttpPath = ConfigProp.getInstance().getFileServerUrl(), 
		baseDiskPath = ConfigProp.getInstance().getFileServerDir();
	
	public static DiskElement factory(String filePath) {
		if(filePath == null)
			filePath = baseDiskPath + DANG_JIAN;
		File file = new File(filePath);
		if(file.isDirectory())
			return new Folder(file);
		else
			return new FileBean(file);
	}
}

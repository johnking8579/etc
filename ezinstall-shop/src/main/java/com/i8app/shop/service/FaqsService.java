package com.i8app.shop.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.common.SessionAccessor;
import com.i8app.shop.dao.FaqsDao;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.FaqType;
import com.i8app.shop.domain.Faqs;

@Service
public class FaqsService {
	@Resource
	private FaqsDao faqsDao;

	/**
	 * 问题类型列表
	 * @return
	 */
	public List<FaqType> typeList()	{
		return faqsDao.typeList();
	}
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Faqs findById(Integer id)	{
		return faqsDao.findById(id);
	}
	
	/**
	 * 获得所有的FAQ, 按类型进行归类
	 * @return
	 */
	public Map<FaqType, List<Faqs>> getAllMap() {
		return faqsDao.getAllMap();
	}
	
	/**
	 * 根据QUESTION的关键字进行查询
	 * @param question
	 * @return
	 */
	public List<Faqs> findByQuestion(String question)	{
		return this.faqsDao.findByQuestion(question);
	}
	
	/**
	 * 某一员工的所有问题
	 * @param empId
	 * @return
	 */
	public List<Faqs> findByEmpId(Integer empId)	{
		return this.faqsDao.findByEmpId(empId);
	}

	/**
	 * 添加一条Faq, 返回值为各种错误码
	 * @param faqs
	 * @return
	 * 1: 提问成功   2: 提问次数超过3次
	 */
	public Integer addFaqs(String question, Integer typeId, Employee emp, Integer hasFaqed)	{
		Faqs faqs = new Faqs();
		if(emp != null)
			faqs.setEmp(emp);
		faqs.setQuestion(question);
		SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
		String strdate = spdf.format(new Date());
		faqs.setSubmitTime(strdate);
		if(typeId != null)
			faqs.setFaqType(this.faqsDao.findTypeById(typeId));
		
		String msg = "";
		if(hasFaqed == null)
			hasFaqed = 0;
		if(hasFaqed <= 3)	{
			this.faqsDao.saveOrUpdate(faqs);
			SessionAccessor.setHasFaqed(++ hasFaqed);
			return 1;
		} else
			return 2;
	}
}

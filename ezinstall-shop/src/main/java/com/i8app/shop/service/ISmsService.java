package com.i8app.shop.service;

import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.shop.common.NoSuchRecordException;
import com.i8app.shop.common.SmsAccessException;

public interface ISmsService {


	/**
	 * 发送短信验证码
	 * @param phoneNumber
	 * @param areaCode
	 * @return 生成的验证码
	 * @throws SmsAccessException 短信接口访问异常
	 */
	String sendAuthcode(String phoneNumber, String areaCode);
	
	/**
	 * 发送短链短信
	 * @param installType	见pdm-installLog-installType取值(4:pc客户端短信下载;6:web短信下载;8:wap短信下载;12.手机客户端短信下载;)
	 * @param phoneNo
	 * @param areaCode
	 * @param empId
	 * @param installPackUuid 安装包ID
	 * @param serviceSequence	服务序列号, 对应installlog表的uuid
	 * @param imoduleType
	 * @throws CpServerAccessException 
	 * @throws NoSuchRecordException	安装包ID不存在 
	 * @throws SmsAccessException 短信接口访问异常
	 * @deprecated 短链解析后,不再向日志表插入记录,改为发链接前插入记灵.被sendShortUrl(int installType, String phoneNo, String areaCode, String installPackUuid, int logId)替代
	 */
	void sendShortUrl(int installType, String phoneNo, String areaCode, int empId, String installPackUuid,
			String serviceSequence, String imoduleType)	throws CpServerAccessException;

	/**
	 * 发送短链短信
	 * @param installType 见pdm-installLog-installType取值(4:pc客户端短信下载;6:web短信下载;8:wap短信下载;12.手机客户端短信下载;)
	 * @param phoneNo
	 * @param areaCode
	 * @param installPackUuid
	 * @param logId installLog的id
	 * @throws CpServerAccessException
	 * @throws NoSuchRecordException	安装包ID不存在 
	 * @throws SmsAccessException 短信接口访问异常
	 */
	void sendShortUrl(int installType, String phoneNo, String areaCode,
			String installPackUuid, int logId) throws CpServerAccessException;

}
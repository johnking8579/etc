package com.i8app.shop.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.shop.common.NoSuchRecordException;
import com.i8app.shop.common.SmsAccessException;
import com.i8app.shop.common.Util;
import com.i8app.shop.common.config.ConfigProp;
import com.i8app.shop.common.wsclient.SmsSender;
import com.i8app.shop.dao.AppInstallPackDao;
import com.i8app.shop.dao.ShortUrlDao;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.ShortUrl;

/**
 * 调用接口服务器上的发短信的hessian接口
 * 
 * @author JingYing 2013-6-5
 */
@Service
public class SmsHessianService implements ISmsService	{
	private static Logger log = Logger.getLogger(SmsHessianService.class);
	
	@Resource
	private SmsSender smsSender;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private AppInstallPackDao appInstallPackDao;
	@Resource
	private ShortUrlDao shortUrlDao;
	@Resource
	private InstallLogService installLogService;

	@Override
	public String sendAuthcode(String phoneNumber, String areaCode) {
		try {
			String verifyCode = Util.createRandomString(6);
			smsSender.sendVerifyCode(areaCode, phoneNumber, verifyCode);
			return verifyCode;
		} catch(Exception e)	{
			log.error(e.getMessage(), e);
			throw new SmsAccessException(e);
		}
	}
	
	/**
	 * 先向安装日志存入一条optstatus为70004的记录
	 * 根据"下载入口"的参数要求, 查询相关参数, 生成完整长链(type:安装类型, empId:员工ID, lid:已存的安装日志ID, no:手机号, id:安装包uuid, url:最终下载地址)
	 * 根据ISmsSender的发送接口要求, 查询相关参数, 并调用 
	 * @deprecated
	 */
	@Override
	public void sendShortUrl(int installType, String phoneNo, String areaCode, int empId, String installPackUuid, 
				String serviceSequence, String imoduleType) throws CpServerAccessException	{
		
		AppInstallPack pack = appInstallPackDao.findById(installPackUuid);	
		if(pack == null)		
			throw new NoSuchRecordException("安装包ID不存在:" + installPackUuid);
		String url = appQuerier.getPackUrl(installPackUuid);

		int logId = installLogService.buildSmsInstallLog(installType, empId, installPackUuid, phoneNo, 
										70004, serviceSequence, imoduleType);
		String longUrl;
		try {
			
			longUrl = String.format("%sdl/log.action?type=%s&lid=%s&empId=%s&no=%s&id=%s&url=%s", //根据'下载入口'要求,拼接参数
							ConfigProp.getInstance().getSmsIp2(), installType, logId, empId, phoneNo, 
							installPackUuid, URLEncoder.encode(url,"utf-8"));
			log.debug("生成长链:" + longUrl);
			String shortUrl = ConfigProp.getInstance().getSmsIp1() + genShortUrl(longUrl, installType);
			smsSender.sendDownloadLink(areaCode, phoneNo, pack.getApp().getName(), shortUrl);	//根据'短信接口'要求,拼接参数
			
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new SmsAccessException(e);
		}	
	}
	
	@Override
	public void sendShortUrl(int installType, String phoneNo, String areaCode, String installPackUuid, int logId) 
				throws CpServerAccessException	{
		AppInstallPack pack = appInstallPackDao.findById(installPackUuid);	
		if(pack == null)		
			throw new NoSuchRecordException("安装包ID不存在:" + installPackUuid);
		String url = appQuerier.getPackUrl(installPackUuid);
		String longUrl;
		try {
			
			longUrl = String.format("%sdl/log.action?type=%s&lid=%s&no=%s&id=%s&url=%s", //根据'下载入口'要求,拼接参数
							ConfigProp.getInstance().getSmsIp2(), installType, logId, phoneNo, 
							installPackUuid, URLEncoder.encode(url,"utf-8"));
			log.debug("生成长链:" + longUrl);
			String shortUrl = ConfigProp.getInstance().getSmsIp1() + genShortUrl(longUrl, installType);
			smsSender.sendDownloadLink(areaCode, phoneNo, pack.getApp().getName(), shortUrl);	//根据'短信接口'要求,拼接参数
			
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new SmsAccessException(e);
		}	
	}
	
	
	/**
	 * 生成短链并存到数据库中
	 * @param longUrl
	 * @param type 短链类型 4短信, 5二维码
	 * @return 6位数的短链ID
	 */
	private String genShortUrl(String longUrl, int type)	{
		ShortUrl su = new ShortUrl();
		su.setId(toShortUrl(longUrl));
		su.setInsertTime(new Date());
		su.setLongUrl(longUrl);
		su.setType(type);
		shortUrlDao.persist(su);
		return su.getId();
	}
	

	/**
	 * 生成短链
	 * @param url
	 * @return 六位区分大小写的字符
	 */
	public String toShortUrl(String longUrl) {
		String hex = Util.toMD5(longUrl);

		String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h",
				"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
				"u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
				"6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
				"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z"
		};
		String[] resUrl = new String[4];
		for (int i = 0; i < 4; i++) {
			// 把加密字符按照 8 位一组 16 进制与 0x3FFFFFFF 进行位与运算
			String sTempSubString = hex.substring(i * 8, i * 8 + 8);
			// 这里需要使用 long 型来转换，因为 Inteper .parseInt() 只能处理 31 位 , 首位为符号位 ,
			// 如果不用long ，则会越界
			long lHexLong = 0x3FFFFFFF & Long.parseLong(sTempSubString, 16);
			String outChars = "";
			for (int j = 0; j < 6; j++) {
				// 把得到的值与 0x0000003D 进行位与运算，取得字符数组 chars 索引
				long index = 0x0000003D & lHexLong;
				// 把取得的字符相加
				outChars += chars[(int) index];
				// 每次循环按位右移 5 位
				lHexLong = lHexLong >> 5;
			}
			// 把字符串存入对应索引的输出数组
			resUrl[i] = outChars;
		}
		return resUrl[new Random().nextInt(4)];
	}
	

	public void setSmsSender(SmsSender smsSender) {
		this.smsSender = smsSender;
	}

	public AppQuerier getAppQuerier() {
		return appQuerier;
	}

	public void setAppQuerier(AppQuerier appQuerier) {
		this.appQuerier = appQuerier;
	}

}

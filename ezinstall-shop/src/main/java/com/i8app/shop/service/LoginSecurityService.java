package com.i8app.shop.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.common.Util;
import com.i8app.shop.common.config.Config_i8;
import com.i8app.shop.dao.BusinessHallDao;
import com.i8app.shop.dao.EmployeeDao;
import com.i8app.shop.dao.HdDao;
import com.i8app.shop.dao.OperateLogDao;
import com.i8app.shop.dao.TerminalDao;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.OperateLog;
import com.i8app.shop.domain.Terminal;

/**
 * 进行安全性检查, 比如用户登录服务
 * @author jing
 *
 */
@Service
public class LoginSecurityService {
	
	/**
	 * 在遇到重复工号时, 是否选择使用BSS工号做为默认登录账号.
	 * 如果省内该工号只有一个, 则不需此项
	 * 注意工号重复有两种情况,一种是两个不同省之间有重复工号,第二种是自有省和BSS省之间重复. 这里只处理第2种情况
	 */
	public static final boolean USE_BSS_AS_DEFAULT = true;
	
	@Resource
	private HdDao hdDao;
	@Resource
	private OperateLogDao operateLogDao;
	@Resource
	private EmployeeDao employeeDao;
	@Resource
	private Config_i8 config_i8;
	@Resource
	private BusinessHallDao hallDao;
	@Resource
	private TerminalDao terminalDao;
	
	
	
	/**
	 * 用户名与密码的检查,先把输入的密码转换成MD5再进行检查, 返回值为各种错误码
	 * @param areaCode 不传此参数时, 代表旧接口或山西移动接口, 假定EMPNO唯一, 只根据EMPNO查出唯一对象. 
	 * 					否则根据AREACODE对应的PROVID, 联合USE_BSS_AS_DEFAULT, 取自有工号或BSS工号
	 * @param inputNo
	 * @param inputPwd
	 * @return
	 */
	public int checkEmpLogin(String areaCode, String inputNo, String inputPwd)	{
		Employee emp = null;
		if("13".equals(areaCode))	{	//山西移动, 还是根据工号查询唯一值
			emp = employeeDao.findByNo(inputNo);
		} else	{	//根据配置,选择BSS登录或自有工号登录
			int[] provIds = config_i8.getAreaProvMap().get(areaCode);
			if(provIds == null)
				throw new IllegalArgumentException("no matching province id of this areacode:" + areaCode);
			List<Employee> list = employeeDao.findByNoAndProvId(inputNo, provIds);
			emp = chooseEmp(list, provIds);
		}
		return empErrorCode(inputPwd, emp);
	}
	
	
	public Employee checkEmpLogin2(String areaCode, String inputNo)	{
		Employee emp = null;
		if("13".equals(areaCode))	{	//山西移动, 还是根据工号查询唯一值
			emp = employeeDao.findByNo(inputNo);
		} else	{	//根据配置,选择BSS登录或自有工号登录
			int[] provIds = config_i8.getAreaProvMap().get(areaCode);
			if(provIds == null)
				throw new IllegalArgumentException("no matching province id of this areacode:" + areaCode);
			List<Employee> list = employeeDao.findByNoAndProvId(inputNo, provIds);
			emp = chooseEmp(list, provIds);
		}
		return emp;
	}
	
	/**
	 * 根据登录策略, 判断使用哪个工号登录
	 * 根据"是否默认使用BSS"选项, 选择使用自有省或BSS省的工号
	 * @param list
	 * @param provIds
	 * @return
	 */
	private Employee chooseEmp(List<Employee> list, int[] provIds)	{
		Employee emp = null;
		if(list.size() == 1)	{	//该省中只有一个有效工号,无需再判断,直接登录 
			emp = list.get(0);
		} else if(list.size() > 1)	{	//该省里有重复工号
			int targetProvId = provIds[0];
			if(USE_BSS_AS_DEFAULT)	targetProvId = provIds[1];
			for(Employee e : list)	{
				if(e.getProvince().getId() == targetProvId)	{
					emp = e;
					break;
				}
			}
		} 
		return emp;
	}
	
	/**
	 * 定义登录错误码
	 * @param inputPwd
	 * @param persistEmp
	 * @return
	 */
	public int empErrorCode(String inputPwd, Employee persistEmp)	{
		String md5Pwd = Util.toMD5(inputPwd);
		if(persistEmp == null)	{
			return 1;	//无此工号
		} else if(!md5Pwd.equalsIgnoreCase(persistEmp.getEmpPwd()))	{
			return 2;	//密码错误
		} else if(persistEmp.getOrgLevel() == null || persistEmp.getOrgLevel() != 4)	{
			return 3;	//不是营业厅人员
		} else if(persistEmp.getIsDeleted() != null && persistEmp.getIsDeleted() == 1)	{
			return 4;	//已被删除
		} else if(persistEmp.getIsFreezed() != null && persistEmp.getIsFreezed() == 1)	{
			return 5;	//已被冻结
		} else 
			return 0;	//正确
	}
	
	/**
	 * 
	 * @param emp
	 * @param psn
	 * @param oper
	 */
	public void afterLogin(Employee emp, String psn, Integer hallId, int oper)	{
		if(emp != null && emp.getOrgLevel() == 4)	{
			hallDao.updateDeployTime(emp.getOrgId());
		}
		this.saveOperateLog(emp, oper);
		if(!terminalDao.isExist(psn))	{
			Terminal t = new Terminal();
			t.setMac(psn);
			t.setProvId(emp.getProvince().getId());
			t.setCityId(emp.getCity().getId());
			t.setCountyId(emp.getCounty().getId());
			t.setHallId(hallId);
			t.setDeployTime(new Date());
			terminalDao.persist(t);
		}
	}
	
	
	/**
	 * 检查手机号和运营商是否匹配
	 * @param phoneNumber
	 * @param operatorCode
	 * @return
	 */
	public boolean isPhoneMatch(String phoneNumber, Integer operatorCode)	{
		Integer code = hdDao.findOperByNum3(phoneNumber);
		if(operatorCode.equals(code))	//不要使用==
			return true;
		else
			return false;
	}
	
	public void afterLogin(Employee emp, int oper)	{
		if(emp != null && emp.getOrgLevel() == 4)	{
			hallDao.updateDeployTime(emp.getOrgId());
		}
		this.saveOperateLog(emp, oper);
	}
	
	/**
	 * 营业人员操作日志
	 * @param emp
	 * @param psn
	 * @param oper 1登陆 2退出
	 */
	public void saveOperateLog(Employee emp, int oper)	{
		if(oper != 1 && oper != 2)	
			throw new IllegalArgumentException(oper + "");
		if(emp != null)
			operateLogDao.saveOrUpdate(buildOperateLogFromEmp(emp, oper));
	}
	
	private OperateLog buildOperateLogFromEmp(Employee emp, int operateTag)	{
		OperateLog log = new OperateLog();
		log.setEmpId(emp.getId());
		log.setEmpName(emp.getName());
		log.setEmpno(emp.getEmpNo());
		log.setOrgIdStr(emp.getOrgIdStr());
		log.setOrgNameStr(emp.getOrgNameStr());
		log.setOperateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
		log.setOperateTag(operateTag + "");			//1登录, 2退出
		log.setClientType("1");			//3:统计平台 ; 2:手机客户端; 1:PC客户端;
		switch(emp.getOrgLevel().intValue())	{
			case 4:
				BusinessHall hall = hallDao.findById(emp.getOrgId());
				log.setHallId(hall.getId());
				log.setHallno(hall.getHallNo());
				log.setHallName(hall.getHallName());
				log.setBhid(hall.getBhid());
			case 3:
				log.setCountyId(emp.getCounty().getId());
				log.setCountyName(emp.getCounty().getName());
			case 2:
				log.setCityId(emp.getCity().getId());
				log.setCityName(emp.getCity().getName());
			case 1:
				log.setProvinceId(emp.getProvince().getId());
				log.setProvinceName(emp.getProvince().getName());
				break;
			default:
				throw new RuntimeException();
		}
		return log;
	}
	

	public void setHdDao(HdDao hdDao) {
		this.hdDao = hdDao;
	}


	public void setOperateLogDao(OperateLogDao operateLogDao) {
		this.operateLogDao = operateLogDao;
	}


	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}
}

package com.i8app.shop.service;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.dao.BusinessHallDao;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.Channeltype;
import com.i8app.shop.domain.Collaboratechannel;
import com.i8app.shop.domain.Reifiercollaboratechannel;

@Service
public class HallService {

	@Resource
	private BusinessHallDao hallDao;
	
	public BusinessHall findById(int id)	{
		return hallDao.findById(id);
	}
	
	public Channeltype findByCtid(int ctid)	{
		return hallDao.findByCtid(ctid);
	}
	
	public Collaboratechannel findByCcid(int ccid)	{
		return hallDao.findByCcid(ccid);
	}
	
	public Reifiercollaboratechannel findByRccid(int rccid)	{
		return hallDao.findByRccid(rccid);
	}
	
	public List<Channeltype> findCtList(){
		return hallDao.findCtList();
	}
	
	public List<Collaboratechannel> findCcList(Integer ctid){
		return hallDao.findCcList(ctid);
	}
	
	public List<Reifiercollaboratechannel> findRccByccId(Integer ccid) {
		return hallDao.findRccByccId(ccid);
	}
	
	public void addOrUpdateHall(BusinessHall hall) {
		hallDao.addOrUpdateHall(hall);
	}
}

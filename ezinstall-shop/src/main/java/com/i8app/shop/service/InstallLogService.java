package com.i8app.shop.service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.i8app.shop.common.TripleDES;
import com.i8app.shop.dao.AppInstallPackDao;
import com.i8app.shop.dao.BusinessHallDao;
import com.i8app.shop.dao.EmployeeDao;
import com.i8app.shop.dao.HdDao;
import com.i8app.shop.dao.InstallLogDao;
import com.i8app.shop.dao.ProvinceDao;
import com.i8app.shop.dao.WorkPortAccessLogDao;
import com.i8app.shop.domain.AppErrLog;
import com.i8app.shop.domain.AppInstallPack;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.Employee;
import com.i8app.shop.domain.InstallErrorLog;
import com.i8app.shop.domain.InstallLog;
import com.i8app.shop.domain.InstallLogzy;
import com.i8app.shop.domain.Province;
import com.i8app.shop.domain.WorkPortAccessLog;

@Service
public class InstallLogService {
	private static Logger logger = Logger.getLogger(InstallLogService.class);
	
	/**
	 * clientid对应的密钥,在spring文件中注入
	 */
	private static Map<String, String> clientKeyMap = new HashMap<String, String>();	
	@Resource 
	private InstallLogDao installLogDao;
	@Resource
	private EmployeeDao empDao;
	@Resource
	private AppInstallPackDao appInstallPackDao;
	@Resource
	private BusinessHallDao hallDao;
	@Resource
	private WorkPortAccessLogDao workPortAccessLogDao;
	@Resource
	private ProvinceDao provinceDao;
	@Resource
	private HdDao hdDao;
	
	@Resource
	public void setClientKeyMap(Map<String, String> clientKeyMap) {
		InstallLogService.clientKeyMap = clientKeyMap;
	}

	@SuppressWarnings("unchecked")
	public void saveInstallLog(InstallLog log) {
		installLogDao.saveOrUpdate(log);
	}
	
	@SuppressWarnings("unchecked")
	public void saveInstallLogzy(InstallLogzy logzy) {
		installLogDao.saveOrUpdate(logzy);
	}
	
	@SuppressWarnings("unchecked")
	public void saveInstallErrorLog(InstallErrorLog installErrorLog) {
		installLogDao.saveOrUpdate(installErrorLog);
	}
	

	/**
	 * 生成短信下载日志
	 * @param installType 安装类型
	 * @param empId 营业员ID
	 * @param installPackUuid 安装包UUID
	 * @param no 手机号
	 * @param optStatus 操作状态
	 * @param serviceSequence 服务的序列号
	 * @param imoduleType 页面模块
	 * @return 生成的日志id
	 */
	public int buildSmsInstallLog(int installType, int empId, String installPackUuid, String no, 
				int optStatus, String serviceSequence, String imoduleType)	{
		logger.debug(String.format("installType:%s, empId:%s, installPackUuid:%s, no:%s, optStatus:%s", 
				installType, empId, installPackUuid, no, optStatus));
		Employee emp = empDao.findById(empId);
		AppInstallPack pack = appInstallPackDao.findById(installPackUuid);
		if(emp == null)	{
			throw new IllegalArgumentException("无效的empid:" + empId);
		}
		if(pack == null)	{
			throw new IllegalArgumentException("无效的安装包ID:" + installPackUuid);
		}
		
		BusinessHall hall = hallDao.findById(emp.getOrgId());
		
		InstallLog log = new InstallLog();
		log.setAppUuid(pack.getApp().getUuid());
		log.setCityId(emp.getCity().getId());
		log.setCityName(emp.getCity().getName());
		log.setCountyId(emp.getCounty().getId());
		log.setCountyName(emp.getCounty().getName());
		log.setEmpId(empId+"");
		log.setEmpName(emp.getName());
		log.setEmpNo(emp.getEmpNo());
//		log.setFilterrule(filterrule);
		log.setImei("");	//设成空串
		log.setImsi("");	//设成空串	
		log.setImoduletype(imoduleType);
		log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		log.setInstallType(installType);
		log.setIsAutoOpen(0);
//		log.setIsGuide(isGuide);
		log.setIsOtherNet(0);
		log.setIsSmsVal(0);
//		log.setManuName(manuName);
		log.setMarkerType(pack.getApp().getMarkerType());
//		log.setModelName(modelName);
		log.setOptStatus(optStatus);	//70001:用户已点击短信链接, 70002:短信链接已下载完成
		log.setOstype(this.toOsType(pack.getOs()));
//		log.setOsVersion(osVersion);
		log.setPhoneNumber(no);
		log.setProvinceId(emp.getProvince().getId());
		log.setProvinceName(emp.getProvince().getName());
		log.setPackUuid(installPackUuid);
//		log.setPsn(psn);
		log.setRscid(pack.getOriginalPackId());
		log.setRscName(pack.getApp().getName());
		log.setRscVersion(pack.getAppVer());
		log.setSeqNo("");	//设成空串
		log.setSource(this.toSource(pack.getCp().getId()));
		log.setSourceType(pack.getApp().getRscType().getId());	//?
//		log.setTypeid(typeid);
		log.setUuid(serviceSequence);
//		log.setVersionId(versionId);
		
		if(hall != null){
			log.setBhid(hall.getBhid());	//?
			log.setCcid(hall.getCcid());
			log.setChanGrade(hall.getChanGrade());
			log.setChanLevel(hall.getChanLevel());
			log.setChanType(hall.getChanType());
			log.setCtid(hall.getCtid());
			log.setHallId(hall.getId());
			log.setHallName(hall.getHallName());
			log.setHallno(hall.getHallNo());
			log.setRccid(hall.getRccid());
		}
		
		installLogDao.persist(log);
		return log.getId();
	}
	
	private int toOsType(String os)	{
		if("os1".equals(os))	{
			return 200;
		} else if("os2".equals(os))	{
			return 300;
		} else if("os3".equals(os))	{
			return 300;
		} else if("os4".equals(os))	{
			return 100;
		} else if("os5".equals(os))	{
			return 500;
		} else if("os6".equals(os))	{
			return 400;
		} else 
			throw new IllegalArgumentException("os:" + os);
	}
	
	private int toSource(String cpId)	{
		if("cp01".equals(cpId) || "cp02".equals(cpId) || "cp09".equals(cpId))	{
			return 0;
		} else if("cp03".equals(cpId))	{
			return 2;
		} else if("cp04".equals(cpId))	{
			return 3;
		} else if("cp05".equals(cpId))	{
			return 1;
		} else if("cp06".equals(cpId))	{
			return 5;
		} else
			throw new IllegalArgumentException("cpId:" + cpId);
	}
	
	public void persistWorkPortAccessLog(WorkPortAccessLog log)	{
		workPortAccessLogDao.persist(log);
	}
	
	/**
	 * 根据clientid选择密钥,
	 * @param encryptParam
	 * @param clientId
	 * @return
	 * @throws BadPaddingException
	 */
	public String decryptParam(byte[] encryptParam, String clientId) throws BadPaddingException	{
		String key = clientKeyMap.get(clientId); 
		if(key == null)	{
			throw new IllegalArgumentException("不识别的clientid:" + clientId);
		}
		try {
			return new String(TripleDES.decrypt(encryptParam, key), "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * 根据工作量接口中上传的installLog, 补齐剩余的字段并入库
	 * @param log
	 */
	public void completeInstallLogFromWorkport(InstallLog log)	{
		Province prov = provinceDao.findById(log.getProvinceId());
		Employee emp = empDao.findByNoAndProId(log.getEmpNo(), log.getProvinceId());
		BusinessHall hall = hallDao.findByNoAndProvId(log.getHallno(), log.getProvinceId());
		
		if(prov == null)
			throw new IllegalArgumentException("省编码无效:" + log.getProvinceId());
		if(emp == null)
			throw new IllegalArgumentException("工号无效:" + log.getEmpNo());
		if(hall == null)
			throw new IllegalArgumentException("渠道编码无效:" + log.getHallno());
		
//		log.setAppUuid(appUuid);	//不需要
		log.setBhid(hall.getBhid());
		log.setCcid(hall.getCcid());
		log.setChanGrade(hall.getChanGrade());
		log.setChanLevel(hall.getChanLevel());
		log.setChanType(hall.getChanType());
		log.setCityId(hall.getCity().getId());
		log.setCityName(hall.getCity().getName());
		log.setCountyId(hall.getCounty().getId());
		log.setCountyName(hall.getCounty().getName());
//		log.setCpId(cpId);//不需要
		log.setCtid(hall.getCtid());
		log.setEmpId(emp.getId() + "");
		log.setEmpName(emp.getName());
//		log.setEmpNo(empNo);	//已赋值
//		log.setFilterrule(filterrule);//不需要
		log.setHallId(hall.getId());
		log.setHallName(hall.getHallName());
		log.setHallno(hall.getHallNo());
//		log.setImei(imei);//已赋值
//		log.setImoduletype(imoduletype);//不需要
//		log.setImsi(imsi);//已赋值
//		log.setInstallTime(installTime);//已赋值
//		log.setInstallType(installType);//已赋值
//		log.setIsAutoOpen(isAutoOpen);	//不需要
//		log.setIsGuide(isGuide);	//不需要
		int operator = hdDao.findOperByNum3(log.getPhoneNumber());
		log.setIsOtherNet(operator == 1 ? 0 : 1);	
//		log.setIsSmsVal(isSmsVal);	//不需要
//		log.setManuName(manuName);	//不需要
//		log.setMarkerType(markerType);	//不需要
//		log.setModelName(modelName);	//不需要
		log.setOptStatus(70000);
//		log.setOstype(ostype);	//已赋值
//		log.setOsVersion(osVersion);	//已赋值
//		log.setPackUuid(packUuid);	//不需要
//		log.setPhoneNumber(phoneNumber);	//已赋值
//		log.setProvinceId(provinceId);	//已赋值
		log.setProvinceName(prov.getName());
//		log.setPsn(psn);	//不需要
		log.setRccid(hall.getRccid());
//		log.setRscid(rscid);	//已赋值
//		log.setRscName(rscName);	//已赋值
//		log.setRscVersion(rscVersion);	//不需要
//		log.setSeqNo(seqNo);//已赋值
//		log.setSource(source);//不需要
//		log.setSourceType(sourceType);//不需要
//		log.setTypeid(typeid);	//已赋值
//		log.setUuid(uuid);	//已赋值
//		log.setVersionId(versionId);	//不需要
	}
	
	@SuppressWarnings("unchecked")
	public void saveAppErrLog (AppErrLog errLog){
		installLogDao.saveOrUpdate(errLog);
	}
}

package com.i8app.shop.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.shop.common.Pager;
import com.i8app.shop.dao.AddressbookrunlogDao;
import com.i8app.shop.dao.AnnounceDao;
import com.i8app.shop.dao.AppActiveDao;
import com.i8app.shop.dao.BusinessHallDao;
import com.i8app.shop.dao.CityCustomizeDao;
import com.i8app.shop.dao.CityDao;
import com.i8app.shop.dao.ClientDeployInfoDao;
import com.i8app.shop.dao.ClientExceptDao;
import com.i8app.shop.dao.CountyDao;
import com.i8app.shop.dao.EmployeeDao;
import com.i8app.shop.dao.HdDao;
import com.i8app.shop.dao.InstantMsgDao;
import com.i8app.shop.dao.MailDao;
import com.i8app.shop.dao.ManuInfoDao;
import com.i8app.shop.dao.MobileDao;
import com.i8app.shop.dao.ProvinceDao;
import com.i8app.shop.dao.TerminalDao;
import com.i8app.shop.dao.TypeInfoDao;
import com.i8app.shop.dao.VersionInfoDao;
import com.i8app.shop.domain.Addressbookrunlog;
import com.i8app.shop.domain.Announcement;
import com.i8app.shop.domain.AppActive;
import com.i8app.shop.domain.BusinessHall;
import com.i8app.shop.domain.City;
import com.i8app.shop.domain.CityCustomize;
import com.i8app.shop.domain.ClientDeployInfo;
import com.i8app.shop.domain.ClientExcept;
import com.i8app.shop.domain.County;
import com.i8app.shop.domain.InstantMsg;
import com.i8app.shop.domain.Mail;
import com.i8app.shop.domain.ManuInfo;
import com.i8app.shop.domain.Mobile;
import com.i8app.shop.domain.Province;
import com.i8app.shop.domain.Terminal;
import com.i8app.shop.domain.TypeInfo;
import com.i8app.shop.domain.VersionInfo;

/**
 * 该类存放对一些不常用的表的简单查询
 * @author jing
 *
 */
@Service
public class SimpleQueryService {

	@Resource
	private HdDao hdDao;
	@Resource
	private MailDao mailDao;
	@Resource
	private AppActiveDao appActiveDao;
	@Resource
	private ManuInfoDao manuInfoDao;
	@Resource
	private TypeInfoDao typeInfoDao;
	@Resource
	private EmployeeDao employeeDao;
	@Resource
	private InstantMsgDao instantMsgDao;
	@Resource
	private AnnounceDao announceDao;
	@Resource
	private BusinessHallDao businessHallDao;
	@Resource
	private AddressbookrunlogDao addressbookrunlogDao;
	@Resource
	private ClientDeployInfoDao clientDeployInfoDao; 
	@Resource
	private VersionInfoDao versionInfoDao;
	@Resource
	private ProvinceDao provinceDao;
	@Resource
	private CityDao cityDao;
	@Resource
	private CountyDao countyDao;
	@Resource
	private CityCustomizeDao cityCustomizeDao;
	@Resource
	private MobileDao mobileDao;
	@Resource
	private TerminalDao terminalDao;
	@Resource
	private ClientExceptDao clientExceptDao;
	
	
	/**
	 * 根据手机号查找运营商
	 * @param phoneNumber
	 * @return	运营商代号
	 * @throws IllegalArgumentException 
	 */
	public Integer getMNO(String phoneNumber) {
		if((phoneNumber.length() != 11) || (phoneNumber == null))
			throw new IllegalArgumentException("不是有效手机号码");

		Integer result = hdDao.findOperByNum3(phoneNumber);
		if(result == null)
			throw new IllegalArgumentException("未找到该手机号对应的运营商");
			
		return result; 
	}
	
	
	/**
	 * 根据ID查询营业厅
	 * @param id
	 * @return
	 */
	public BusinessHall findBusinessHallById(int id)	{
		return businessHallDao.findById(id);
	}
	
	/**
	 * 查询mailInfo表里的所有记录
	 * @return
	 */
	public List<Mail> mailList()	{
		return mailDao.list();
	}
	
	/**
	 * 查询manuInfo表里的所有厂商记录
	 * @return
	 */
	public List<ManuInfo> manuList()	{
		return manuInfoDao.list();
	}
	
	/**
	 * 分页查询所有厂商
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<ManuInfo> manuList(int offset, int pageSize)	{
		return manuInfoDao.list(offset, pageSize);
	}
	
	/**
	 * 根据区域码查询已发布的公告
	 * @param areaCode
	 * @return
	 */
	public List<Announcement> findAnnounce(String areaCode)	{
		return this.announceDao.findByAreaCode(areaCode);
	}
	
	/**
	 * 根据TYPENAME, MANUNAME, 得到对应的TYPEINFO
	 * @param typeName
	 * @param manuName
	 * @return
	 */
	public TypeInfo getSymbianTypeInfo(String typeName, String manuName)	{
		List<Integer> list = typeInfoDao.list4Symbian(typeName, manuName);
		if(list.isEmpty())
			return null;
		else	{
			return typeInfoDao.findById(list.get(0));
		}
	}
	
	/**
	 * 根据TYPENAME, MANUNAME, 得到对应的TYPEINFO
	 * @param typeName
	 * @param manuName
	 * @return
	 */
	public TypeInfo getJavaTypeInfo(String typeName, String manuName)	{
		List<Integer> list = typeInfoDao.list4Java(typeName, manuName);
		if(list.isEmpty())
			return null;
		else
			return typeInfoDao.findById(list.get(0));
	}
	
	public void saveAddressbookLog(Addressbookrunlog a)	{
		addressbookrunlogDao.saveOrUpdate(a);
	}
	
	public void saveClientDeploy(ClientDeployInfo c)	{
		clientDeployInfoDao.saveOrUpdate(c);
	}
	
	public ClientDeployInfo findClientDeploy(String psn)	{
		return clientDeployInfoDao.findByPsn(psn);
	}
	
	public void saveVersionInfo(VersionInfo v)		{
		versionInfoDao.saveOrUpdate(v);
	}
	
	public VersionInfo findVersionInfo(String versions)	{
		return versionInfoDao.findByVersions(versions);
	}
	
	public Province findProvinceById(int id)	{
		return provinceDao.findById(id);
	}

	public Province findProvinceByBsscode(String bsscode)	{
		return provinceDao.findByBssprov(bsscode);
	}
	
	public City findCityById(int id)	{
		return cityDao.findById(id);
	}
	
	public County findCountyById(int id)	{
		return countyDao.findById(id);
	}
	
	public List<CityCustomize> findCityCustomize(int cityId, int operator)	{
		return cityCustomizeDao.findByCityId(cityId, operator);
	}
	
	public AppActive findAppActive(int source, int appId)	{
		return appActiveDao.find(source, appId);
	}
	
	public List<InstantMsg> findInstantMsg(long fromTime, String areaCode, Integer queryCount)	{
		List<InstantMsg> list;
		if(queryCount != null)	{
			list = instantMsgDao.find(fromTime, areaCode, queryCount);
		} else
			list = instantMsgDao.find(fromTime, areaCode);
		return list;
	}
	
	public Pager<InstantMsg> findInstantMsg(String areaCode, int offset, int pageSize)	{
		return instantMsgDao.find(areaCode, offset, pageSize);
	}
	
	public void persistMobile(Mobile mobile)	{
		mobileDao.persist(mobile);
	}
	
	public void persistTerminal(Terminal terminal)	{
		terminalDao.persist(terminal);
	}
	
	public void persistClientExcept(ClientExcept clientExcept)	{
		clientExceptDao.persist(clientExcept);
	}
	
}

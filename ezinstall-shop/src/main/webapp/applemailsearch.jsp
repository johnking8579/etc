<%@page import="org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String mail = request.getParameter("appmail");
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());     
	AppleMailInfoDao appledao =  context.getBean(AppleMailInfoDao.class);
    Applemailinfo apple = null;
    if (mail == null) {
    	mail = "";
    	apple = appledao.getAppleMailInfo();
    } else if (!"".equals(mail)){
    	apple = appledao.getAppleMailInfo(mail);
    }
    if(apple != null) {
        out.print(apple.getId()+",");
        out.print(apple.getMac()+",");
        out.print(apple.getGuid()+",");
        out.print(apple.getMail());
    } else {
        out.print(",,,,");
    }
%>

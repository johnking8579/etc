<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*,org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.common.*,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>

<%-- 如果传入memo参数, 则需要根据memo参数查询, 否则查询全部记录 --%>
<%
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
	String memo = request.getParameter("memo");
	AreaInfoDao aidao = context.getBean(AreaInfoDao.class);
	List<Areainfo> list;
	if(memo == null)
		list = aidao.listAreaInfo();
	else
		list = aidao.findByMemo(memo);
    for (Areainfo ai : list) 
        out.print(ai.getAreaCode()+","+ai.getAreaName()+";");
%>

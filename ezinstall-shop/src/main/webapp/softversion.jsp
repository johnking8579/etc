<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.common.*,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
try {
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    String version = request.getParameter("version");
    String type = request.getParameter("type");
    UpdateProgramDao upd = context.getBean(UpdateProgramDao.class);
    request.setAttribute("listup",upd.softVersion(version,type));
%>
    <c:forEach items="${listup}" var="up" varStatus="lid">
     ${up.address};
    </c:forEach>
    
    <% } catch (Exception e) {
        e.printStackTrace();
    } %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.common.*,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
try {
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    String version = request.getParameter("version");
    UpdateDriverDao upd = context.getBean(UpdateDriverDao.class);
    request.setAttribute("listupdatedrive", upd.driverVersion(version));
%>
    <c:forEach items="${listupdatedrive}" var="updatedrive">
     ${updatedrive.version},${updatedrive.address};
    </c:forEach>
    
    <% } catch (Exception e) {
        e.printStackTrace();
    } %>
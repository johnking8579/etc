<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils,com.i8app.shop.common.*,com.i8app.shop.dao.*,com.i8app.shop.domain.*,org.springframework.context.*"%>
<%
	ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
	AreaInfoDao aidao = context.getBean(AreaInfoDao.class);
	String id = request.getParameter("id");
	Areainfo a = aidao.findById(id);
	if(a != null)
		out.print(a.getOperator());
	else
		out.print(0);
%>

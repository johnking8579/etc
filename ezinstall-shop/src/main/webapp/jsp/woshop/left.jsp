<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>应用--软件列表</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/global2.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<%@ include file="/jsp/common2.jsp"%>
		
		<script type="text/javascript">
			$(function()	{
				$("a, input:button").focus(function()	{
					$(this).blur();
				});
				document.onselectstart = function()	{
					return false;
				}
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   			
	   			//初始化按钮的状态
	   			$('img').imgAlter();
	   			
	   			$("#shuju").html("<center><br/>请稍后正在加载内容……<br/><br/><center>");

				/**
				 * 复选框点击后发生的事件, 更新选中数和软件总大小
				*/ 
				$("input[id$='_checkAll']").change(function()	{
					
					//取得所有为true的复选框的ID集合, 并得到集合个数, 写入到checkCount中
					var checkCount = 0, totalSize = 0.00, id = this.id;
					var typeId = id.split("_checkAll")[0];
					$("input[id^='Check_" + typeId + "']").attr('checked', this.checked).each(function()	{
						if(this.checked == true) {
							checkCount ++;
							var fileSize = $("#FileSize_"+ typeId +"_"+ this.value + "").val();
							if(fileSize == null || "" == fileSize){
								fileSize = " 0.00 ";
							}
							totalSize += parseFloat(fileSize);
						}
					});
					
					$('#'+ typeId +'_checkCount').html(checkCount);
					$('#'+ typeId +'_totalSize').html((totalSize/1024.0).toFixed(2));
				});
				
				$("input[id^='Check_']").change(function()	{
					//取得所有为true的复选框的ID集合, 并得到集合个数, 写入到checkCount中
					var checkCount = 0;
					var totalSize = 0.00;
					var id = this.id;
					var typeId = id.split("_")[1];
					
					$("input[id^='Check_" + typeId + "']").each(function()	{
						if(this.checked == true) {
							checkCount ++;
							var fileSize = $("#FileSize_"+ typeId +"_"+ this.value + "").val();
							if(fileSize == null || "" == fileSize){
								fileSize = " 0.00 ";
							}
							totalSize += parseFloat(fileSize);
						}
					});
						
					$('#'+ typeId +'_checkCount').html(checkCount);
					$('#'+ typeId +'_totalSize').html((totalSize/1024.0).toFixed(2));
				});
				
				
				//鼠标事件
	   			$(".app").mouseover(
					function(){
						var b=$(this).find(".popUp").html();
						if(b){
							$("#tanchulei").html(b);
							downDisplay(this);
						}
						$(this).find(".downtimes").hide().end().find(".down").show()
					}
				).mouseout(
					function(){
						$("#tanchulei").hide();
						$(this).find(".downtimes").show().end().find(".down").hide()
					}
				);
			});
			//鼠标事件--弹出框
			function downDisplay(k) {
				var i = $(k).parents().offset().left;
				var n = $(k).offset().left + 75;
				var j = $(k).offset().top + 5;
				var m = n - i;
				var h = $(k).parents().width() - m;
				var l = $("#tanchulei").width();
				if (l > h) {
					var n = $(k).offset().left - 180;
					var j = $(k).offset().top + 5;
					//$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				} else {
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}
			}
			
			/*
			 * 各个类别的全选框单击事件  
			 * this.checked 是jquery的属性，不是js的属性 
			function selectAll(typeId){
				var isChecked = $("#"+ typeId +"_isChecked");
				if(isChecked.val() == "false"){
					$("input[id='" + typeId + "_checkAll']").attr('checked', true);
					$("input[id^='Check_" + typeId + "']").attr('checked', true);
					isChecked.val("true");
				}else if(isChecked.val() == "true"){
					$("input[id='" + typeId + "_checkAll']").attr('checked', false);
					$("input[id^='Check_" + typeId + "']").attr('checked', false);
					isChecked.val("false");
				}
			}
			 */
			
			
			
			//应用宝库--更多  勾选安装处理
			function multiApp()	{
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 0;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							//alert(arr[0]+"  ,  "+arr[1]);
							var url = "${pageContext.request.contextPath}/woshop/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=10&ids=" + ids ;
							if(num == 0){
								num = 1;
								myweb.MyOwnFunc(1,arr[0],arr[1],url);
							}else{
								myweb.MyOwnFunc(0,arr[0],arr[1],url);
							}
						}
					}
				}
			}
			
			//单个的下载安装
			function downApp(appName, rscTypeId, ids){
				var url = "${pageContext.request.contextPath}/woshop/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&imoduletype=10&ids=" + ids;
				myweb.MyOwnFunc(1,appName, rscTypeId, url);
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
		
		</script>
		<STYLE type=text/css>
BODY {
	FONT-SIZE: 14px;
	FONT-FAMILY: "微软雅黑";
	background-color: #FFFFFF;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	top: 10px;
}

#con {
	FONT-SIZE: 14px;
	BORDER-RIGHT: #aecbd4 0px solid;
	BORDER-LEFT: #aecbd4 0px solid;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
	background-repeat: repeat-x;
	height: 35px;
}

#tagContent {
	BORDER-RIGHT: #aecbd4 0px solid;
	BORDER-LEFT: #aecbd4 0px solid;
	BORDER-TOP: #aecbd4 0px solid;
	BORDER-BOTTOM: #aecbd4 0px solid;
	margin-left: 0px;
	margin-right: 0px;
}

p.hj {
	margin-top: 6px;
	line-height: 28px;
}

a:link {
	color: #666666;
	text-decoration: none;
}

body,td,th {
	color: #333333;
	z-index: auto;
}

a:visited {
	text-decoration: none;
	color: #000000;
}

a:hover {
	text-decoration: none;
	color: #FF0000;
}

a:active {
	text-decoration: none;
	color: #003399;
}

.STYLE9 {
	font-size: 12px;
}

.STYLE13 {
	font-family: "微软雅黑";
	color: #000000;
	font-size: 14px;
}

.STYLE14 {
	color: #FF0000;
	font-weight: bold;
}

.STYLE15 {
	color: #333333;
}

.STYLE17 {
	font-size: 14px;
	font-weight: bold;
	height: 31px;
	line-height: 31px;
	overflow: hidden;
}

.STYLE11 {
	font-size: 14px;
	font-family: "微软雅黑";
	color: #333333;
}

.STYLE18 {
	font-size: 9px
}

.STYLE19 {
	font-size: 10px
}

.c1 {
	width: 100px;
	height: 800px;
	line-height: 75px;
	text-align: center;
	border: 0px solid #909;
	position: absolute;
	top: 50px;
	left: 850px;
}
</STYLE>
	</head>

	<body>
		<table width="100%" border="0">
			<td width="99%" valign="top">
				<a name="top" id="top"></a>
				<div id=tagContent>
					<!-- 循环开始 -->
					<c:if test="${empty typeMap }"> <center> <p> 对不起, 没有找到相关的软件类别! </p> </center> </c:if>
					<c:forEach items="${typeMap }" var="map" varStatus="i">
						<div class="tagContent selectTag" id=tagContent0>
							<div id="con">
								<span class="STYLE17">&nbsp;&nbsp;<input type="hidden" id="${map.key.id }_isChecked" value="false"/>
									<input type="checkbox" onclick="selectAll('${map.key.id }');" id="${map.key.id }_checkAll" value="${map.key.name }" name="${app.uuid}_${app.packUuid }_${app.discriminator}"/>
									<span class="STYLE13">&nbsp;<a name="type${map.key.id }" id="type${map.key.id }" >${map.key.name }</a></span>
									<span class="STYLE11">&nbsp;&nbsp;&nbsp;&nbsp;已选择<span class="STYLE14"  id="${map.key.id }_checkCount" > 0 </span>
									款，共<span id="${map.key.id }_totalSize" class="STYLE14"> 0.00 </span>MB </span>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									<span class="STYLE11"> <a target="_top" href="woshop/secondAppList.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&typeId=${map.key.id }&imoduletype=10">更多&gt;&gt;&gt;</a> </span>
								</span>
								<br />
							</div>
							
							<div class="softindex"  align="center">
								<ul id="bodyul">
									<c:if test="${empty map.value }"> <center> 对不起, 没有找到相关的软件! </center> </c:if>
									<c:forEach items="${map.value }" var="app">
										<li class="app">
											<div class="tpc" id="tpc">
												<span class="gxk">
													<input type="checkbox" id="Check_${map.key.id }_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}" />
													<input type="hidden" id="FileSize_${map.key.id }_${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.packFileSize}"/>
									            	<input type="hidden" name="isIOS"  value="${isIOS }"/>
									            	<input type="hidden" id="discriminator" value="${app.discriminator}"/>
									            	<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }_${app.rscTypeName }_${app.originalPackId }"/>
												</span>
												
												<a target="_top" href="woshop/appInfo.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=10">
													<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
													<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
														<span class="tj"></span>
													</c:if>
													<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
														<span class="rm"></span>
													</c:if>
													<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
														<span class="jl"></span>
													</c:if>
													<c:if test="${app.markerType == null || '' eq app.markerType }">
														<span class=""></span>
													</c:if>
													<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" width="50" height="50" align="baseline" />
												</a>
												<span class="downtimes STYLE9 STYLE15" style="display: block;">
													${fnx:abbreviate(app.name,12, "...")}
												</span>
												<br />
												
												<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
												<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}','${app. rscTypeName}','${app.originalPackId }');"> 
				                					<c:if test="${installType == 1 }"><!-- pc端直连 -->
					                					<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                				</c:if>
					                				<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
					                					<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                				</c:if>
					                				<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
					                					<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                				</c:if>
					                			</a>
												<div class="popUp">
													<font> ${fnx:abbreviate(app.name,20, "...")} </font>
													<span>
														${app.packAppVer } &nbsp;&nbsp;&nbsp; ${app.downCount }次<br /> 
								                    	<span class="floatleft"> ${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB</span> &nbsp;&nbsp;&nbsp; 
								                    	<c:if test="${app.cpUpdateTime == null }"></c:if>
								                    	<c:if test="${app.cpUpdateTime != null }">${app.cpUpdateTime }</c:if><br />
								                    	简介：${fnx:abbreviate(app.info,90, "...")}
													</span>
												</div>
											</div>
										</li>
									</c:forEach>
								</ul>
							</div>
							<hr size="1">
						</div>
					</c:forEach>
					<!-- 循环结束 -->
					<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
					<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
					<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
				</div>
			</td>
		</table>
		<div class="tanchubj popUp" id="tanchulei"
				style="position: absolute; left: 620.5px; top: 745px; display: none;">
		</div>
		<div style="position:fixed; right:2px; bottom:0px;"><a href="javascript:scroll(0,0)"><img src="img_woshop/top-dd.png" width="32" height="30" /></a></div>
	</body>
</html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>应用活动--软件列表</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/hot0406.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/global2.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript">
		
			$(function()	{
				document.onselectstart = function()	{
					return false;
				}
	   			
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   			
	   			//初始化按钮的状态
	   			$('img').imgAlter();
	   			
	   			
	   			//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.myweb.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				}); 
	   			
	   			// 软件宝库--更多页面全选  appimg
				$('#checkAll').click(function()	{
					$("input[id^='softCheck_']").attr('checked', this.checked);
					calcChecked();
				});
	   				
				/**
				 * 每个复选框变化后, 页面发生的联动变化
				 */
				$(':checkbox').change(function()	{
					calcChecked();
				});
   				
   				//鼠标事件--弹出框
   				$(document).ready(function () {
					$(".app").mouseover(function () {
						var b = $(this).find(".popUp").html();
						if (b) {
							$("#tanchulei").html(b);
							downDisplay(this);
						}
						$(this).find(".downtimes").hide().end().find(".down").show();
					}).mouseout(function () {
						$("#tanchulei").hide();
						$(this).find(".downtimes").show().end().find(".down").hide();
					});
				});
				
				$("#addPkg").hide();
				$("#delPkg").hide();
				
				//取消添加
				$("#qxAdd").click(function(){
					$("#addPkg").hide();
				});
				
				//取消删除
				$("#qxDel").click(function(){
				  	$("#delPkg").hide();
				});
				
				//初始化广告幻灯片
				changepic();
				//鼠标在图片上悬停让切换暂停 
				$("div[name='adPic']").hover(function () { clearInterval(t); }); 
				//鼠标离开图片切换继续 
				$("div[name='adPic']").mouseleave(function () { changepic(); }); 
				
   			});
			//鼠标事件--弹出框
			function downDisplay(k) {
				var i = $(k).parents().offset().left;
				var n = $(k).offset().left + 75;
				var j = $(k).offset().top + 5;
				var m = n - i;
				var h = $(k).parents().width() - m + 40;
				var l = $("#tanchulei").width();
				if (l > h) {
					var n = $(k).offset().left + 80;
					var j = $(k).offset().top + 5;
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				} else {
					$("#tanchulei").css({left:n + "px", top:j + "px"}).show();
				}
			}
			
			function calcChecked()	{
				var checkedCount = 0 ;
				var totalSize = 0.0 ;
				$("input[id^='softCheck_']").each(function()	{
					if(this.checked == true) {
						checkedCount ++;
						var fileSize = $("#fileSize_" + this.value + "").val();
						if(fileSize == null || "" == fileSize){
							fileSize = " 0.00 ";
						}
						totalSize += parseFloat(fileSize);
					}
				});
				$('#checkCount').html(checkedCount);
				$('#totalSize').html((totalSize/1024.0).toFixed(2));
			}
			
			//广告幻灯片
			var t; 
			var speed = 2;//图片切换速度 
			var nowlan = 0;//图片开始时间 
			function changepic() { 
				var imglen = $("div[name='adPic']").find("div").length; 
				$("div[name='adPic']").find("div").hide(); 
				$("div[name='adPic']").find("div").eq(nowlan).show(); 
				nowlan = nowlan+1 == imglen ?0:nowlan + 1; 
				t = setTimeout("changepic()", speed * 1000); 
			}
			
			//勾选安装
			function multiApp()	{
				var pkgId = $("#pkgId").val();
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 1;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							
							var url = "${pageContext.request.contextPath}/woplus/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&imoduletype=${imoduletype}&pkgId="+pkgId+"&ids=" + ids ;
							if(num == 1){
								num = 0;
								window.myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								window.myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
			
			//单个下载安装
			function downApp(appName, rscTypeId, ids){
				var pkgId = $("#pkgId").val();
				alert(pkgId);
				var url = "${pageContext.request.contextPath}/woplus/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pkgId="+pkgId+"&imoduletype=${imoduletype}&ids=" + ids;
				window.myweb.MyOwnFunc(1, appName, rscTypeId, url);
			}
			
			function searchApp(){
				if($("#keyword").val() == null || $("#keyword").val() == ""){
					window.myweb.SearchFunc("请输入软件名称进行搜索！");
				}else{
					document.searchForm.submit(); 
				}
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
   			
   			
   			//弹出添加新包的div
			function addNewPkg(){
				$("#addPkg").show();
			}
			
			function qdAddHide(){
				$("#addPkg").hide();
			}
			
			//弹出删除包的div
			function delePkg(){
			   	$("#delPkg").show();
			}
			
		</script>
		
		<STYLE type=text/css>
BODY {
	FONT-SIZE: 14px;
	FONT-FAMILY: "宋体";
	background-color: #FFFFFF;
	background-repeat: no-repeat;
}

UL {
	LIST-STYLE-TYPE: none
}

#con {
	FONT-SIZE: 14px;
	BORDER-RIGHT: #aecbd4 1px solid;
	BORDER-LEFT: #aecbd4 1px solid;
	margin-top: 0px;
	margin-right: 10px;
	margin-bottom: 0px;
	margin-left: 10px;
	background-image: url(img_woshop/qbj.png);
}

a:link {
	color: #000000;
	text-decoration: none;
}

body,td,th {
	color: #333333;
}

a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}
.STYLE9 {
	font-size: 12px
}

.STYLE11 {
	font-size: 14px;
	font-family: "微软雅黑";
	color: #333333;
}

.STYLE13 {
	font-family: "微软雅黑";
	color: #000000;
	font-size: 14px;
}

.STYLE14 {
	color: #FF0000;
	font-weight: bold;
}

.STYLE15 {
	color: #333333
}

.STYLE18 {
	font-family: "微软雅黑";
	color: #000000;
	font-size: 14px;
	font-weight: bold;
}

p.hj {
	line-height: 35px
}

.tstab{  
	width:320px; 
	height:190px; 
	border:0px; 
	background-image:url(img_woshop/T-box.png); 
	background-repeat:no-repeat;
}
.tsbt{font-family:"宋体"; font-size:14px;}
.tsbt3{ height:27px; color:#FFFFFF; padding-top:10px; font-weight:bold;}
.tsbt1{ height:35px; padding-top:16px;}
.tsbt2{ height:20px;}
.STYLE23{border:solid 1px rgb(159,159,159); height:21px; width:120px; padding-bottom:4px;}
</STYLE>

	</head>

	<body style="overflow-y: scroll">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<form id="searchForm" name="searchForm" action="woplus/search.action" method="get">
				<tbody>
					<tr>
						<td width="72%" height="28" bgcolor="RGB(225,225,225)">
							<table width="100%" border="0">
								<tr>
									<td width="2%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(-1);"> 
											<img src="img_woshop/houtui.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(1);">
											<img src="img_woshop/qianjin.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="woplus/activityAppList.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pkg.id=${pkg.id}&pkg.name=${pkg.name}&bizType=${bizType }">	
											<img src="img_woshop/shuaxin.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="62%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td align="right" valign="middle" bgcolor="RGB(225,225,225)">
							<input type="hidden" name="imoduletype" value="10"/>
								
							<input type="hidden" name="empId" value="${empId}"/>
							<input type="hidden" name="appOsType" value="${appOsType}"/>
							<input type="hidden" name="version" value="${version}"/>
							<input type="hidden" name="isIOS" value="${isIOS}"/>
							<input type="hidden" name="installType" value="${installType}"/>
							<input type="hidden" name="pageSize" value="${pageSize}"/>
								
							<input type="text" name="keyword" id="keyword" class="STYLE23" 
								onclick="javascript:$('#keyword').val('');" maxlength="255" value="${keyword }"/>
						</td>
						<td width="2.5%" valign="middle" bgcolor="RGB(225,225,225)">
							<div style="position:absolute; right:0px; top: 0px;">
								<a href="javascript:searchApp()">
									<img src="img_woshop/fdj.jpg" width="39" height="27" align="absmiddle" />
								</a>
							</div>
						</td>
					</tr>
				</tbody>
			</form>
		</table>
		
		<table width="100%" border="0" align="center">
				<tr>
					<td height="20" background="img_woshop/bd.png">
						<table width="100%" border="0">
							<tr>
								<td width="5%"></td>
								<td width="10%" height="30" valign="top" align="center" background="img_woshop/select-ht.png" >
									<input type="hidden" name="pkgId" id="pkgId" value="${pkg.id }"/>
									<input type="hidden" name="pkg.id" id="pkg.id" value="${pkg.id }"/>
									<div align="center" class="STYLE11">
										<strong> 
											<c:if test="${pkg != null}">
												${pkg.name } 
											</c:if>
										</strong>
									</div>
								</td>
								<td width="65%" valign="middle" align="left">
									&nbsp;&nbsp;
									<input type="checkbox" name="checkbox" id="checkAll" />
									<label for="checkAll"><span class="STYLE13">全选</span></label>
									<span class="STYLE13"> 
										&nbsp;&nbsp;&nbsp;已选择
										<span id="checkCount" class="STYLE14">0&nbsp;</span>款， 共
										<span id="totalSize" class="STYLE14">0.00&nbsp;</span>MB 
									</span>
								</td>
								<td width="15%">
									<div align="right">
										<a href="javascript:multiApp();" target="_self">
											<img src="img_woshop/yjanz.png" width="89" height="32" />
										</a>
									</div>
								</td>
								<td width="5%"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		
		<c:if test="${empty appPager.list}">
			<center style="font-size:14px;">对不起, 没有找到相关的软件!</center>
		</c:if>
		
		<c:if test="${!empty appPager.list}">
			<pg:pager items="${appPager.totalCount}" maxPageItems="${pageSize}" maxIndexPages="10" isOffset="true" 
			 	export="offset,currentPageNumber=pageNumber" scope="request" url="woplus/appLeft.action"> 
				<pg:param name="pkg.id" value="${param.pkg.id}"/>
		 		<pg:param name="imoduletype" value="${param.imoduletype}"/>
				
				<pg:param name="empId" value="${param.empId}"/>
		 		<pg:param name="appOsType" value="${param.appOsType}"/>
		 		<pg:param name="version" value="${param.version}"/>
		 		<pg:param name="isIOS" value="${param.isIOS}"/>
		 		<pg:param name="installType" value="${param.installType}"/>
		 		<pg:param name="pageSize" value="${param.pageSize}"/>
		 		
			<table>
				<tr>
					<td >
						<div class="softindex-1">
							<ul id="bodyul">
								<c:forEach items="${appPager.list}" var="app">
									<pg:item>
									<li class="app">
										<div class="tpc" id="tpc">
											<span class="gxk">
												&nbsp;&nbsp;
												<input type="checkbox" id="softCheck_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}" />
					               				<input type="hidden" id="fileSize_${app.uuid}_${app.packUuid }_${app.discriminator}"  value="${app.packFileSize}"/>
					               				<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
					               				<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
											</span>
											
											<a target="_top" href="woplus/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=${imoduletype }">
					            				<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
												<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
													<span class="tj"></span>
												</c:if>
												<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
													<span class="rm"></span>
												</c:if>
												<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
													<span class="jl"></span>
												</c:if>
												<c:if test="${app.markerType == null || '' eq app.markerType }">
													<span class=""></span>
												</c:if>
					            				<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="50" height="50">
											</a>
											<span class="downtimes STYLE9 STYLE15" style="display: block;">
												${fnx:abbreviate(app.name,12, "...")}
											</span>
											<br />
											<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
				                			<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"> 
				                				<c:if test="${installType == 1 }"><!-- pc端直连 -->
					                				<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                			</c:if>
					                			<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
					                				<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                			</c:if>
					                			<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
					                				<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
					                			</c:if>
					                		</a>
											<div class="popUp">
												<font> ${fnx:abbreviate(app.name,20, "...")} </font>
												<span>
													${app.packAppVer } &nbsp;&nbsp;&nbsp; ${app.downCount }次<br /> 
						                    		<span class="floatleft"> ${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB</span> 
						                    		<c:if test="${app.cpUpdateTime == null }"></c:if>
						                    		<c:if test="${app.cpUpdateTime != null }">${app.cpUpdateTime }</c:if>
						                    		简介：${fnx:abbreviate(app.info,90, "...")}
												</span>
											</div>
										</div>
									</li>
									</pg:item>
								</c:forEach>
							</ul>
						</div>
						
					</td>
				</tr>
				<tr>
					<td>
						<center>
							<pg:index>
					  			<font class="STYLE13">
									<pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
					  				<pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
							   		<pg:pages>
								    	<c:choose>
									    	<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b></c:when>
									    	<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a></c:otherwise>
								    	</c:choose>
								    </pg:pages>
								    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
								    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
					   				<br/>
					   			</font>
							</pg:index>
						</center>
					</td>
				</tr>
			</table>
		</pg:pager>
		</c:if>
		
		<div class="tanchubj popUp" id="tanchulei" style="position: absolute; left: 620.5px; top: 745px; display: none;"> </div>
		
	</body>
</html>
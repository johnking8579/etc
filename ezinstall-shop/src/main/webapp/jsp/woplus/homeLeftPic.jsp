<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>精品推荐-猜你喜欢</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all.css">
		<%@ include file="/jsp/common2.jsp" %>
		<script type=text/javascript>
			$(function()	{
				//初始化广告幻灯片
				changepic();
				//鼠标在图片上悬停让切换暂停 
				$("div[name='adPic']").hover(function () { clearInterval(t); }); 
				//鼠标离开图片切换继续 
				$("div[name='adPic']").mouseleave(function () { changepic(); }); 
   			});
			
			//广告幻灯片
			var t; 
			var speed = 5;//图片切换速度 
			var nowlan = 0;//图片开始时间 
			function changepic() { 
				var imglen = $("div[name='adPic']").find("div").length; 
				$("div[name='adPic']").find("div").hide(); 
				$("div[name='adPic']").find("div").eq(nowlan).show(); 
				nowlan = nowlan+1 == imglen ?0:nowlan + 1; 
				t = setTimeout("changepic()", speed * 1000); 
			}
		</script>
	</head>

	<body>
		<div name="adPic" class="div2" style="z-index: 1">
	  		<!-- 后台配置广告业务的动态广告 -->
	  		<c:if test="${empty pkgList }">
				<div><img src="img_adPics/ad_x1.jpg" class="img2" ></div>
				<div><img src="img_adPics/together_x.jpg" class="img2" ></div>
			</c:if>
			<c:if test="${!empty pkgList }">
				<c:forEach items="${pkgList }" var="pkg">
					<div>
						<c:if test="${pkg.memo != null && pkg.memo != '' }">
							<a target="_top" href="${pkg.memo }">
						</c:if>
						<c:if test="${pkg.memo == null || pkg.memo == '' }">
							<a>
						</c:if>
							<c:if test="${pkg.largeIcon == null || pkg.largeIcon == ''}">
								<img src="img_adPics/qmyx.jpg" class="img2" >
							</c:if>
							<c:if test="${pkg.largeIcon != null && pkg.largeIcon != ''}">
								<img src="${fileserverUrl}${pkg.largeIcon}" class="img2" />
							</c:if>
						</a>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</body>
</html>

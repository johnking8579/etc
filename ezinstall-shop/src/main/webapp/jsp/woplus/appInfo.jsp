<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>应用详情</title>
		<base href="<%=basePath%>">
		<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
	    <LINK rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/more.css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		<%@ include file="/jsp/common2.jsp" %>
		<script src="js/js_woshop/click_toggle.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function()	{
				$("a, input:button").focus(function()	{
					$(this).blur();
				});
				document.onselectstart = function()	{
					return false;
				}
				
				//切换文字和详情
				if($("#appInfo")[0].scrollHeight > 250)	{
					$("#switchInfo").show().toggle(function()	{
						$(this).html('&#171; 收起');
						$("#more").hide();
						$("#appInfo").height($("#appInfo")[0].scrollHeight);
					}, function(){
						$(this).html('展开&#187;');
						$("#more").show();
						$("#appInfo").height(250);
					});
				}
				
				$("img[id^='pic_']").lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/noPic.png"
	   			});
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   			
	   			$('img').imgAlter({
					map : {
						"1": "img_woshop/zzxz_info.png",
						"2": "img_woshop/yxz_info.png",
						"3": "img_woshop/zzaz_info.png",
						"4": "img_woshop/yaz_info.png",
						"5": "img_woshop/zzsc_info.png",
						"6": "img_woshop/ysc_info.png"
		  			}
				});
	   			
	   			//初始化按钮的状态
	   			$('img').imgAlter();
	   			
	   			//键盘按下事件
	   			$(document).keydown(function(event) {
					if (event.keyCode == 13) {
						$('form').each(function() {
							//搜索的代码
							if($("#keyword").val() == null || $("#keyword").val() == ""){
								window.external.SearchFunc("请输入软件名称进行搜索！");
							}else{
								document.searchForm.submit(); 
							}
						});
					}
				}); 
	   		});
	   		
	   		//单个的下载安装 
			function downApp(appName, rscTypeId, ids){
				var url = "${pageContext.request.contextPath}/woplus/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&imoduletype=${imoduletype }&ids=" + ids;
				window.parent.myweb.MyOwnFunc(1,appName, rscTypeId, url);
			}
	   		
	   		function searchApp(){
				if($("#keyword").val() == null || $("#keyword").val() == ""){
					window.parent.myweb.SearchFunc("请输入软件名称进行搜索！");
				}else{
					document.searchForm.submit(); 
				}
			}
			
			/**
			* 按钮状态变化
			*/
			function clientInvoke(u_sStr){
				//尹工打算将多个uuid和状态值一次性传过来，我这边可能需要做个循环处理
				var u_sArr = u_sStr.split(",");
				for(i=0; i<u_sArr.length; i++){
					var u_s = u_sArr[i].split(":");
					var uuid = u_s[0];
					var status = u_s[1];
	   				$("img").imgAlterTrigger(uuid, status);
				}
   			}
	   	</script>
		<style type="text/css">
			.STYLE11 {
				font-size: 14px;
				font-family: "微软雅黑";
				color: #333333;
			}
			.STYLE12 {
				font-size: 14px;
				font-weight: bold;
				color: #333333;
			}
			p.small
		  	{
		  		line-height: 12px
		  	}
			p.big
		  	{
		  		line-height: 25px
		  	}
		  	a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}
			.STYLE23{border:solid 1px rgb(159,159,159); height:24px; width:120px; padding-bottom:4px;}
		</style>
	</head>
	
	<body onLoad="clickMenu('outer','div','more')">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<form id="searchForm" name="searchForm" action="woplus/search.action" method="get">
				<tbody>
					<tr height="28">
						<td width="90%" bgcolor="RGB(225,225,225)">
							<table width="100%" border="0">
								<tr>
									<td width="2%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(-1);"> 
											<img src="img_woshop/houtui.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="javascript:history.go(1);">
											<img src="img_woshop/qianjin.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="1%">
										&nbsp;
									</td>
									<td width="2%">
										<a href="woplus/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=${imoduletype}">	
											<img src="img_woshop/shuaxin.jpg" width="35" height="22" /> 
										</a>
									</td>
									<td width="62%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td width="7%" align="right" valign="middle" bgcolor="RGB(225,225,225)">
							<input type="hidden" name="imoduletype" value="${imoduletype }"/>
								
							<input type="hidden" name="empId" value="${empId}"/>
							<input type="hidden" name="appOsType" value="${appOsType}"/>
							<input type="hidden" name="version" value="${version}"/>
							<input type="hidden" name="isIOS" value="${isIOS}"/>
							<input type="hidden" name="installType" value="${installType}"/>
							<input type="hidden" name="pageSize" value="${pageSize}"/>
									
							<input type="text" name="keyword" id="keyword" class="STYLE23" 
								onclick="javascript:$('#keyword').val('');" maxlength="255" value="${keyword }"/>
						</td>
						<td width="3%" align="left" valign="middle" bgcolor="RGB(225,225,225)">
							<div style="position:absolute; right:4px; top: 1px;">
								<a href="javascript:searchApp()">
									<img src="img_woshop/fdj.jpg" width="39" height="25"  align="absmiddle" />
								</a>
							</div>
						</td>
					</tr>
				</tbody>
			</form>
		</table>
			
			<table width="100%" border="0">
			  <tr>
			    <td height="36" background="img_woshop/bd.png">
				 <table width="100%" border="0">
			      <tr>
			        <td width="3%">&nbsp;</td>
			        <td width="10%" height="30" valign="top" background="img_woshop/select-ht.png"><div align="center" class="STYLE11">应用详情</div></td>
			        <td width="87%">&nbsp;</td>
			      </tr>
			     </table>
				</td>
			  </tr>
			</table>
			
			<table width="100%" border="0" cellspacing="15">
				<tr>
				    <td width="13%" height="82">
			    		<div class="tpc" id="div6">
					    	<div align="center">
					    		<!-- 如果是推荐的，要标明出来  <span class="tj2">推荐</span> 标签：【${app.markerType }】-->
									<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
										<span class="tj"></span>
									</c:if>
									<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
										<span class="rm"></span>
									</c:if>
									<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
										<span class="jl"></span>
									</c:if>
									<c:if test="${app.markerType == null || '' eq app.markerType }">
										<span class=""></span>
									</c:if>
									<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="52" height="52" />
				    		</div>
				    	</div>
				    </td>
				    <td width="16%">
				    	<div align="center">
				    		<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
			                <a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"> 
	                			<c:if test="${installType == 1 }"><!-- pc端直连 -->
		                			<img src="img_woshop/yjanz.png" width="89" height="32" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }">
		                		</c:if>
		                		<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
		                			<img src="img_woshop/sms.png" width="89" height="32" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }">
		                		</c:if>
		                		<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
		                			<img src="img_woshop/yjanz.png" width="89" height="32" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }">
		                		</c:if>
		                	</a>
				    	</div>
				    </td>
				    <td width="2%" rowspan="2"><p>&nbsp;</p></td>
				    <td width="48%" rowspan="2" valign="top">
				    	<span class="STYLE12">  ${app.name}   ${app.packAppVer} </span><br>
				    	<div id="outer"><div class="more">
				    		<i id="switchInfo" class="show" style="display: none">展开&#187;</i>
				    	</div></div>
				    	<div id="appInfo" style="overflow: hidden;height: 250" >
					    	<p class="big">${fnx:replaceAll(app.info,"\\n","<br/>")}</p> 
				    	</div>
					</td>
		    		<td width="1%" rowspan="2">&nbsp;</td>
		    		<td width="20%" rowspan="2">
		    			<div align="center">
			      			<!-- <p><img src="img_woshop/wyxewm.png" width="103" height="102"></p>
			      			<div align="center"> 
			      				使用手机扫描上方<br><br>
			        			二维码下载 
			        		</div> -->
		      			</div>
		      		</td>
		  		</tr>
		  		<tr>
				    <td height="141" colspan="2" valign="top">
				    	<!-- 通过PackUuid查询的详情将展示一下隐藏信息：CP商名称（app.cpName）；原始Id（app.originalPackId） -->
						<c:if test="${imoduletype != 10 && imoduletype != 9 && packUuid != null && packUuid != ''}">
							<input type="hidden" name="CP商名称" value="${app.cpName }"/>
							<input type="hidden" name="原始Id" value="${app.originalPackId }"/>
						</c:if>
						
						
				    	&nbsp;大小：${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB <br><br>
				    	&nbsp;下载量：
				    	<c:choose>
							<c:when test="${app.downCount eq null}">0</c:when>
							<c:otherwise>${app.downCount}</c:otherwise>
						</c:choose> &nbsp; 次 <br><br>
				       	&nbsp;类型：${app.rscTypeName } <br><br>
				       	&nbsp;更新于: ${app.packCpUpdateTime} <br><br>
				       	&nbsp;适用于: 
						<c:if test="${app.packOs == 'os1'}">
		                	ANDROID
		                </c:if>
		                <c:if test="${app.packOs == 'os2'}">
		                	IPHONE
		                </c:if>
		                <c:if test="${app.packOs == 'os3'}">
		                	破解IPHONE
		                </c:if>
		                <c:if test="${app.packOs == 'os4'}">
		                	SYMBIAN
		               	</c:if>
		                <c:if test="${app.packOs == 'os5'}">
		                	JAVA
		             	</c:if>
		         		<c:if test="${app.packOs == 'os6'}">
		                	WINMOBILE
		             	</c:if>	 
		             	<c:if test="${app.packOsMinVer == null }">
		             		2.0
		             	</c:if>
		             	<c:if test="${app.packOsMinVer != null }">
		             		${app.packOsMinVer }
		             	</c:if>
		             	 以上系统 <br><br>
		             	 &nbsp;开发商： ${app.developer }<br><br>
				    </td>
			  	</tr>
			</table>
			<table width="100%" border="0">
			  <tr align="center">
			  	<td width="10%" height="403">&nbsp;</td>
			  	<c:forEach items="${app.pics}" var="pic" varStatus="varStat">
				    <c:if test="${(varStat.index < 4)}">
				    	<td width="20%">
					    	<div align="center"><img name="icon" src="img_woshop/noPic.png" data-original="${fileserverUrl}${pic}" id="pic_${app.uuid }"  width="239" height="400"></div>
					    </td>
				    	<td width="1%">&nbsp;</td>
					    <c:if test="${(varStat.index+1) % 2 eq 0}"> </c:if>
				    </c:if>
				</c:forEach>
			    <td width="10%">&nbsp;</td>
			  </tr>
			</table>
			<p>&nbsp;</p>
	</body>
</html>

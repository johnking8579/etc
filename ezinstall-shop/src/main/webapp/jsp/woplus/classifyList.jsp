<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>无标题文档</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all2.css">
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all.css">
		<%@ include file="/jsp/common2.jsp" %>
		<script type=text/javascript>
			$(function()	{
				document.onselectstart = function()	{
					return false;
				}
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/pkg_def.png"
	   			});
	   			
	   			
				$("#checkAll").change(function()	{
					if($(this).attr('checked'))	{
						$(':checkbox').attr('checked', true);
					} else	{
						$(':checkbox').removeAttr('checked');
					}
					calcChecked();
				});
	   		});
	   		
	   		//勾选安装
			function multiApp()	{
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 1;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							
							var url = "${pageContext.request.contextPath}/woplus/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&imoduletype=19&pkgId=${pkgId }&ids=" + ids ;
							if(num == 1){
								num = 0;
								window.parent.myweb.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								window.parent.myweb.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
	   		
	   	</script>	
	</head>

	<body>
		<!-- 分页  -->
		<c:if test="${empty appPager.list}">
			<div>
				<div class="div11">
					<img src="img_woshop/11.jpg" />
				</div>
				<div class="div44">
					<img src="img_woshop/44.jpg" />
					&nbsp;&nbsp;换一批
				</div>
				<div class="div22">
					<img src="img_woshop/22.jpg" />
				</div>
				<div class="div33"></div>
				<div class="div55">
					<input type="checkbox" id="checkAll" value="checkbox" />
					全选
				</div>
				
				<div id="nav" class="nav">
					<center style="font-size:14px;">对不起, 没有找到相关的软件。</center>
				</div>
			</div>
			
		</c:if>
		
		<c:if test="${!empty appPager.list}">
		<pg:pager items="${appPager.totalCount}" maxPageItems="${pageSize}" maxIndexPages="10" isOffset="true" 
	 		 	export="offset,currentPageNumber=pageNumber" scope="request" url="woplus/classifyList.action" > 
		 		
		 		<pg:param name="pkgId" value="${param.pkgId}"/>
		 		
		 		<pg:param name="empId" value="${param.empId}"/>
		 		<pg:param name="appOsType" value="${param.appOsType}"/>
		 		<pg:param name="version" value="${param.version}"/>
		 		<pg:param name="isIOS" value="${param.isIOS}"/>
		 		<pg:param name="installType" value="${param.installType}"/>
		 		<pg:param name="pageSize" value="${param.pageSize}"/>
		 		<pg:param name="pageNumber" value="${param.pageNumber}"/>
		 		
			
			<div>
				<div class="div11">
					<img src="img_woshop/11.jpg" />
				</div>
				<div class="div44">
					<img src="img_woshop/44.jpg" />
					<a href="woplus/classifyList.action?empId=${empId}&appOsType=${appOsType}&version=${version}&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&pageNumber=${pageNumber }&pkgId=${pkgId }&offset=${pageNumber*pageSize } ">换一批 </a>
				</div>
				<div class="div22">
					<img src="img_woshop/22.jpg" />
				</div>
				<div class="div33"></div>
				<div class="div55">
					<input type="checkbox" id="checkAll" value="checkbox" />
					全选
				</div>
			</div>
			
			<div id="nav" class="nav">
				<ul>
					<c:forEach items="${appPager.list}" var="app">
					<pg:item>
						<li>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td valign="top">
										<input type="checkbox" id="softCheck_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}" />
										<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
										<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
									</td>
									<td class="jj">
										<div class="tpc2">
										<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
										<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
											<span class="tj2"></span>
										</c:if>
										<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
											<span class="rm2"></span>
										</c:if>
										<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
											<span class="jl2"></span>
										</c:if>
										<c:if test="${app.markerType == null || '' eq app.markerType }">
											<span class=""></span>
										</c:if>
							       		<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" width="50" height="50" align="baseline" /> 
										</div>
										<div >
											${fnx:abbreviate(app.name,6, "...")}
										</div>
									</td>
								</tr>
							</table>
						</li>
					</pg:item>
					</c:forEach>
				</ul>
			</div>
			
			</pg:pager>
		</c:if>
		
		<div align="center" id="con" style="position:fixed; left:250px; height:30px; bottom:0px;">
			<a href="javascript:void(0);" onclick="javascript:multiApp();" target="_self" >
				<img src="img_woshop/yjanz.png" />
			</a>
		</div>
	</body>
</html>

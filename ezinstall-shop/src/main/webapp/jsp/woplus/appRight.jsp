<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>游戏排行</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all.css">
		<%@ include file="/jsp/common2.jsp"%>
		
		<script type="text/javascript" language="javascript">
			
			$(function()	{
				$('img').lazyload({
					failure_limit : 100,
					failImg		: "img_woshop/zwtp.png"
	   			});
	   			
	   			//鼠标事件--弹出框
   				$(".app").mouseover(function () {
					$(this).find(".tdimg").hide().end().find(".tdrgh").hide().end().find(".downBoutten").show();
				}).mouseout(function () {
					$(this).find(".tdimg").show().end().find(".tdrgh").show().end().find(".downBoutten").hide();
				});
				
	   		});
	   		
			//单个下载安装
			function downApp(appName, rscTypeId, ids, pkgId){
				var url = "${pageContext.request.contextPath}/woplus/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&imoduletype=8&pkgId="+pkgId+"&ids=" + ids;
				window.parent.parent.myweb.MyOwnFunc(1, appName, rscTypeId, url);
			}
			
		</script>
	</head>
	
	<body class="bbbody">
		<div>
			<table class="righttab" cellpadding="0" cellspacing="0">
				<c:if test="${pkg == null}">
					<tr>
						<td class="tdph" align="center">
							没有相关配置！
						</td>
					</tr>
				</c:if>
				<c:if test="${pkg != null}">
					<tr>
						<td class="tdph" align="center">
							${pkg.name }
						</td>
					</tr>
				</c:if>
				
				<c:if test="${empty appPager.list}">
					<tr>
						<td>
							<center style="font-size:14px;">对不起, 没有找到相关的软件!</center>
						</td>
					</tr>
				</c:if>
				<c:forEach items="${appPager.list }" var="app" varStatus="i">
					<tr>
						<td>
							<table width="149" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="tdkk">
										<a target="_top" href="woplus/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=8">
											<div class="tpc2">
						            			<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
												<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
													<span class="tj2"></span>
												</c:if>
												<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
													<span class="rm2"></span>
												</c:if>
												<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
													<span class="jl2"></span>
												</c:if>
												<c:if test="${app.markerType == null || '' eq app.markerType }">
													<span class=""></span>
												</c:if>
						            			<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" width="40" height="40">
											</div>
										</a>
									</td>
									<td width="89" height="25" class="app">
										<div class="tdimg">
											<c:if test="${i.index<=2 }">
												&nbsp;&nbsp;&nbsp;&nbsp;
												<font style="color: red;font-weight: bold;">${i.index+1 }</font>
											</c:if>
											<c:if test="${i.index>2 }">
												&nbsp;&nbsp;&nbsp;&nbsp;
												<font style="font-weight: bold;">${i.index+1 }</font>
											</c:if>
										</div>
										<div class="tdrgh" >
											${fnx:abbreviate(app.name,12, "...")}
										</div>
										<div class="downBoutten" style="display: none">
											<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
				                			<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}','${pkg.id }');"> 
				                				<c:if test="${installType == 1 }"><!-- pc端直连 -->
					                				<img src="img_woshop/anzhuang.jpg" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" width="69" height="30"> 
					                			</c:if>
					                			<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
					                				<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" width="69" height="30"> 
					                			</c:if>
					                			<c:if test="${installType == 2 }"><!-- 蓝牙下载  -->
					                				<img src="img_woshop/anzhuang.jpg" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" width="69" height="30"> 
					                			</c:if>
					                		</a>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</c:forEach>
				
			</table>
		</div>
	</body>
</html>


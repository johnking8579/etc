<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>应用页面-左侧</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all.css">
		<%@ include file="/jsp/common2.jsp"%>
		<script type="text/javascript">
			
			$(function(){
				$("#addPkg").hide();
				$("#delPkg").hide();
				hidde('GameUp','GameDown','Game');
				hidde('AppUp','AppDown','App');
				hidde('CustomUp','CustomDown','Custom');
				
				$("#selected_").css("background","url(img_woshop/bj5.jpg)");
				
				//取消添加
				$("#qxAdd").click(function(){
					$("#addPkg").hide();
				});
				
				//取消删除
				$("#qxDel").click(function(){
				  	$("#delPkg").hide();
				});
				
			});
			
			function changeAppList(trId, typeId, imoduletype){
				$("tr[id^='selected_']").css("background","");
				$("#"+trId+"").css("background","url(img_woshop/bj5.jpg)");
				self.parent.appIndex.location.href="${pageContext.request.contextPath}/woplus/appIndex.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&typeId="+typeId+"&imoduletype="+imoduletype;
			}
			
			function show(up, down, obj) {
				//up,down表示图片标识，obj表示表格标识
				$("#yc"+obj).show();
				$("#images"+up).hide();
				$("#images"+down).show();
			}

			function hidde(up, down, obj) {
				//up,down表示图片标识，obj表示表格标识
				$("#yc"+obj).hide();
				$("#images"+up).show();
				$("#images"+down).hide();
			}
			
			
			//弹出添加新包的div
			function addNewPkg(){
				$("#addPkg").show();
			}
			
			function qdAdd(newPkgName){
				$("#addPkg").hide();
				var newPkgHtml = "<tr background='img_woshop/bj5.jpg'><td width='79' class='tdlb2 tdls'><img src='img_woshop/xgzdymc.jpg' /></td>"
								+"<td width='125' class='tdlb3 tdls'><input type='text' name='zdybbj' id='zdybbj' class='STYLE28' value='"+
								newPkgName +"' readonly='readonly' ondblclick='editPkgName()' /></td></tr>";
				$("#lastTr").before(newPkgHtml);
			}
			
			//弹出删除包的div
			function delePkg(){
			   	$("#delPkg").show();
			}
			
			//确定删除
			function qdDel(){
				$("#delPkg").hide();
			}
			
			//双击文本框事件
			function editPkgName(){
			  $("#zdybbj").removeAttr("readonly").css("background-color","white").css("border-color","white");
			}
			
		</script>
	</head>

	<body class="bbbody">
		<div class="leftdiv">
			<div>
				<table width="204" cellpadding="0" cellspacing="0" id="tabyy">
					<tr>
						<td class="tdyy">
							<img src="img_woshop/jptj.jpg" width="23" height="21" />
						</td>
						<td class="tdyy1">
							精品推荐
						</td>
						<td width="37" class="tdyy2">
							<img id="imagesJPDown" src="img_woshop/xxjt.jpg" onclick="hidde('JPUp','JPDown','JP')" />
							<img id="imagesJPUp" src="img_woshop/xsjt.jpg" width="26" height="21"
								onclick="show('JPUp','JPDown','JP')" style="display: none" />
						</td>
					</tr>
				</table>
				
				<table width="204" id="ycJP" border="0" cellpadding="0" cellspacing="0">
					<tr id="selected_${typeId }" >
						<td width="35">
							&nbsp;
						</td>
						<td class="tdlb tdls">
							<a target="appIndex" href="javascript:void(0);" onclick="javascript:changeAppList('selected_${typeId }','${typeId }',21);" 
								style="text-decoration: none;color: black;" >
								&nbsp;精品推荐
							</a>
						</td>
					</tr>
				</table>
			</div>

			<div>
				<table width="204" cellpadding="0" cellspacing="0" id="tabyy">
					<tr>
						<td class="tdyy">
							<img src="img_woshop/yxfl.jpg" width="23" height="22" />
						</td>
						<td class="tdyy1">
							游戏分类
						</td>
						<td width="37" class="tdyy2">
							<img id="imagesGameDown" src="img_woshop/xxjt.jpg" onclick="hidde('GameUp','GameDown','Game')" />
							<img id="imagesGameUp" src="img_woshop/xsjt.jpg" width="26" height="21"
								onclick="show('GameUp','GameDown','Game')" style="display: none" />
						</td>
					</tr>
				</table>

				<table width="204" id="ycGame" border="0" cellpadding="0" cellspacing="0" >
					
					<c:if test="${empty appTypeMap }"> <center style="font-size:14px;"> 对不起, 没有找到相关的 游戏类别! </center> </c:if>
					<c:forEach items="${appTypeMap}" var="map" >
						<c:if test="${map.key == 102 }">
							<tr id="selected_102">
								<td width="35">
									&nbsp;
								</td>
								<td class="tdlb tdls">
									<!--  这种方式也可以将内容显示到指定的框架，同时还能执行本页面的js函数。
									<a target="appIndex" onclick="javascript:changeAppList('selected_102','102',10);"  
									href="woplus/appIndex.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&typeId=102&imoduletype=10" style="text-decoration: none;color: black;">
									-->
									<a target="appIndex" href="javascript:void(0);" onclick="javascript:changeAppList('selected_102','102',10);" 
										style="text-decoration: none;color: black;" > 
										&nbsp;全部游戏
									</a>
								</td>
							</tr>
							<c:forEach items="${map.value }" var="type">
								<tr id="selected_${type.id }">
									<td width="35">
										&nbsp;
									</td>
									<td class="tdlb tdls" >
										<a target="appIndex" href="javascript:void(0);" onclick="javascript:changeAppList('selected_${type.id }','${type.id }',10);" 
											style="text-decoration: none;color: black;" > 
											&nbsp;${type.name }
										</a> 
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
				</table>
			</div>
			
			<div>
				<table width="204" cellpadding="0" cellspacing="0" id="tabyy">
					<tr>
						<td class="tdyy">
							<img src="img_woshop/appType.jpg" width="23" height="25" />
						</td>
						<td class="tdyy1">
							应用分类
						</td>
						<td width="37" class="tdyy2">
							<img id="imagesAppDown" src="img_woshop/xxjt.jpg" onclick="hidde('AppUp','AppDown','App')" />
							<img id="imagesAppUp" src="img_woshop/xsjt.jpg" width="26" height="21"
								onclick="show('AppUp','AppDown','App')" style="display: none" />
						</td>
					</tr>
				</table>
				
				<table width="204" id="ycApp" border="0" cellpadding="0" cellspacing="0">
					
					<c:if test="${empty appTypeMap }"> <center style="font-size:14px;"> 对不起, 没有找到相关的 应用类别! </center> </c:if>
					<c:forEach items="${appTypeMap}" var="map" >
						<c:if test="${map.key == 101 }">
							<tr id="selected_101">
								<td width="35">
									&nbsp;
								</td>
								<td class="tdlb tdls">
									<a target="appIndex" href="javascript:void(0);" onclick="javascript:changeAppList('selected_101','101',10);" 
										style="text-decoration: none;color: black;" > 
										&nbsp;全部应用
									</a>
								</td>
							</tr>
							<c:forEach items="${map.value }" var="type">
								<tr id="selected_${type.id }">
									<td width="35">
										&nbsp;
									</td>
									<td class="tdlb tdls">
										<a target="appIndex" href="javascript:void(0);" onclick="javascript:changeAppList('selected_${type.id }','${type.id }',10);" 
										style="text-decoration: none;color: black;" >
											&nbsp;${type.name }
										</a>
									</td>
								</tr>
							</c:forEach>
						</c:if>
					</c:forEach>
				</table>
			</div>
			
			
			<!-- 
			<div>
				<table width="204" cellpadding="0" cellspacing="0" id="tabyy">
					<tr>
						<td valign="middle" class="tdyy">
							<img src="img_woshop/zdy.jpg" width="27" height="23" />
						</td>
						<td class="tdyy3">
							自定义包&nbsp;
						</td>
						<td width="42" class="tdyy2">
							<img id="imagesCustomDown" src="img_woshop/xxjt.jpg" onclick="hidde('CustomUp','CustomDown','Custom')" />
							<img id="imagesCustomUp" src="img_woshop/xsjt.jpg" width="26" height="21"
								onclick="show('CustomUp','CustomDown','Custom')" style="display: none" />
						</td>
					</tr>
				</table>
				

				<table width="204" border="0" cellpadding="0" cellspacing="0" id="ycCustom">
					<tr id="lastTr">
						<td>
							&nbsp;
						</td>
						<td class="tdlb3 tdls" >
							<a href="javascript:void(0);" onclick="javascript:self.parent.appIndex.left.addNewPkg();" target="left" >
								<img src="img_woshop/tjxb.jpg" align="absmiddle" />
							</a>	
							添加新包
						</td>
					</tr>
					<!-- 
					<tr>
						<td>
							&nbsp;
						</td>
						<td class="tdlb3 tdls">
							本月推广
						</td>
					</tr>

					<tr background="img_woshop/bj5.jpg">
						<td class="tdlb2 tdls">
							&nbsp;&nbsp;
							<img src="img_woshop/sczdyb.jpg" onclick="delePkg()" />
						</td>
						<td class="tdlb3 tdls">
							用户必装包
						</td>
					</tr> - ->
				</table>
			</div>
		</div> -->
		
		<!-- 添加新包 弹出框 
		<div id="addPkg" style="position:absolute; left: 504px; top: 170px;">
			<table cellpadding="0" cellspacing="0" class="tstab">
				<tr>
		  			<td width="318" class="tsbt tsbt3">&nbsp;&nbsp;&nbsp;自定义-添加新包</td>
				</tr>
				<tr>
		  			<td class="tsbt1 tsbt">
		  				&nbsp;&nbsp;新包名称：<input type="text" name="newPkgName" value="用户必装包" style="color:#cccccc" onclick="this.value=''"//>
		  			</td>
				</tr>
				<tr>
		  			<td class="tsbt2 tsbt" >&nbsp;&nbsp; <font color="gray">名称长度限30字以内</font> </td>
				</tr>
				<tr>
		  			<td align="center">
			  			<img id="qxAdd" src="img_woshop/quxiao.jpg" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  			<img id="qdAdd" src="img_woshop/queding.jpg" />
		  			</td>
				</tr>
			</table>
		</div>-->
		
		<!-- 删除包提示 框
		<div id="delPkg" style="position:absolute; left: 504px; top: 170px; z-index: 100;">
			<table cellpadding="0" cellspacing="0" class="tstab">
				<tr>
		  			<td width="318" class="tsbt tsbt3">&nbsp;&nbsp;&nbsp;提示</td>
				</tr>
				<tr>
		  			<td class="tsbt1 tsbt">&nbsp;&nbsp;是否确定要删除[用户必装包]，删除后包中内容</td>
				</tr>
				<tr>
		  			<td class="tsbt2 tsbt" >&nbsp;&nbsp;将被删除？</td>
				</tr>
				<tr>
		  			<td align="center"><img id="qxDel" src="img_woshop/quxiao.jpg" onclick="qxDel()" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  			<img id="qdDel" src="img_woshop/queding.jpg" onclick="qdDel()" /></td>
				</tr>
			</table>
		</div>  -->
		</div>
	</body>
</html>


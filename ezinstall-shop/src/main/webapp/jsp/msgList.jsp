<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
  	<link rel="stylesheet" type="text/css"	href="css/soft.css">
  </head>
  
  <body>
  	<div style="margin-top:10px;">
		  <center><h2 class="STYLE5 STYLE9">消息记录</h2></center>
		<table width="80%" align="center" border="1" cellspacing="0" cellpadding="0" bordercolor="#c8c8c8">
		<pg:pager items="${msgPager.totalCount}" maxPageItems="${pageSize}" maxIndexPages="10" isOffset="true"	
			export="offset,currentPageNumber=pageNumber" scope="request" url="${pageContext.request.contextPath}/extra/msgList.action"> 
			<pg:param name="areaCode" value="${param.areaCode}" />
			<tr height="32">
				<th width="20%" background="img/tj2.png">标题</th>
				<th width="60%" background="img/tj2.png">内容</th>
				<th width="20%" background="img/tj2.png">时间</th>
			</tr>
			<c:if test="${empty msgPager.list}">
				<tr><td colspan="3" align="center" height="50">
					<font color="grey">对不起, 还没有相关消息!</font></td>
				</tr>
			</c:if>
			<c:forEach items="${msgPager.list}" var="msg">
			<pg:item>
				<tr height="30">
					<td align="center">&nbsp;${msg.title}</td>
					<td align="center">&nbsp;${msg.content}</td>
					<td align="center">&nbsp;${msg.uploadTime}</td>
				</tr>
			</pg:item>
			</c:forEach>
		
		<tr><td colspan="3" height="50">
				<center>
				<pg:index>
			    <font face="Helvetica" size=2>
				<pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
			    <pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
			    <pg:pages>
			    	<c:choose>
				    	<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b></c:when>
				    	<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a></c:otherwise>
			    	</c:choose>
			    </pg:pages>
			    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
			    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
			    <br/>
			    </font>
			 </pg:index>
			</center>
			</pg:pager>
		
		</table>
		</div>
  </body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>"/>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>文档树</title>
		<link href="css/style_dang.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="css/zTreeStyle/zTreeStyle.css"/>
		<%@ include file="/jsp/common2.jsp" %>
		<script type="text/javascript" src="js/jquery.ztree.all-3.1.min.js"></script>
		<script type="text/javascript">
			var setting = {	
				async: {
					enable: true,
					url:"${pageContext.request.contextPath}/dang/asyncSubFolder.action",
					autoParam:["id", "name=n", "level=lv", "filePath"]
				},
				callback: {
					onDblClick: function(event, treeId, treeNode)	{
						if(treeNode != null)
							window.parent.frames[1].location = 
								encodeURI("${pageContext.request.contextPath}/dang/docMain.action?filePath=" + treeNode.filePath);
					}
				}
		    };
 
			$(function(){
				$.fn.zTree.init($("#tree"), setting);
			});
		</script>
	</head>
	
	<body>
		<div class="bar"></div>
		<div style="color:#ff0000; margin-top:5px; margin-left:15px;">注:暂不支持IOS系统的下载</div>
		<div class="left">
		<ul id="tree" class="ztree"></ul>
		</div>
	</body>
</html>

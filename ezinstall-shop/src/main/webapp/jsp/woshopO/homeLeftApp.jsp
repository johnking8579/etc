<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>精品推荐-猜你喜欢</title>
		<base href="<%=basePath%>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<Link rel="stylesheet" type="text/css" href="css/css_woshop/all.css">
		<%@ include file="/jsp/common2.jsp" %>
		<script type=text/javascript>
			
			$(function()	{
				$('img').lazyload({
					failure_limit : 100,
					failImg		: "img_woshop/zwtp.png"
	   			});
	   			
	   			//鼠标事件--弹出框
   				$(".app").mouseover(function () {
					$(this).find(".down").hide().end().find(".downBoutten").show();
				}).mouseout(function () {
					$(this).find(".down").show().end().find(".downBoutten").hide();
				});
				
   			});
			
			//单个下载安装
			function downApp(appName, rscTypeId, ids){
				var pkgId = $("#pkgId").val();
				var url = "${pageContext.request.contextPath}/woshopO/downLoad.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&imoduletype=${imoduletype}&pkgId="+pkgId+"&ids=" + ids;
				window.parent.parent.myweb.MyOwnFunc(1, appName, rscTypeId, url);
			}
			
		</script>
	</head>

	<body class="bbbody">
		<div style="z-index: 1; position:fixed;" > 
			<table cellpadding="0" cellspacing="0" class="tab1">
    			<tr>
      				<td class="td1" align="center">
      					&nbsp;&nbsp;<img src="img_woshop/rj.jpg" class="img3" align="absmiddle" />
      					<c:if test="${pkg == null}">
      						&nbsp;<strong>没有相关配置！</strong>
      					</c:if>
      					<c:if test="${pkg != null}">
      						&nbsp;<strong>${pkg.name }</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      					</c:if>
      					<input type="hidden" id="pkgId" value="${pkg.id }"/>
      					<!-- 更多 -->
      				</td>
    			</tr>
    		</table>
    	</div>
		<div class="div1" style="position:absolute; top: 30px;">
			<table align="center" cellpadding="0" cellspacing="0" class="tab1"> 
    			<c:if test="${empty appPager.list}"> <tr> <td align="center" valign="middle" class="tds"> <center style="font-size:14px;"> 对不起, 没有找到相关的软件!</center></td> </tr> </c:if>
    			<c:forEach items="${appPager.list}" var="app" varStatus="i">
    				<tr class="tr2">
				      	<td class="tpc" style="z-index: 0" width="84" height="60">
				      		<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
				      		<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
			       			<a target="_top" href="woshopO/appInfo.action?empId=${empId}&appOsType=${appOsType}&version='${version}'&isIOS=${isIOS}&installType=${installType}&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=${imoduletype }" style="text-decoration: none;">
					       		<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
								<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
									<span class="tj"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
									<span class="rm"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
									<span class="jl"></span>
								</c:if>
								<c:if test="${app.markerType == null || '' eq app.markerType }">
									<span class=""></span>
								</c:if>
					       		&nbsp;&nbsp;<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" class="img" align="baseline" > 
					      	</a>
				      	</td>
				      	<td width="118" class="app">
				      		<div class="divname" > ${fnx:abbreviate(app.name,12, "...")} </div>
				         	<div class="down"> ${app.downCount } 次下载 </div>
							<div class="downBoutten" style="display:none">
								<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
					          	<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"> 
					           		<c:if test="${installType == 1 }"><!-- pc端直连 -->
						      			<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }" > 
						          	</c:if>
						           	<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
						          		<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
						          	</c:if>
						           	<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
						               	<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
						          	</c:if>
					         	</a>
							</div>  
				    	</td>
				    </tr>
    			</c:forEach>
			</table>
	  		
		</div>
	</body>
</html>

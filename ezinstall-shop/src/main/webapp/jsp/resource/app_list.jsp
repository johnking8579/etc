<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()	+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="css/zizhu2.css" rel="stylesheet" type="text/css" />
		<link href="css/jpages.css" rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp" %>
  		<script type="text/javascript" src="js/jPages.js"></script>
		<script type="text/javascript">
			$(function()	{
				changeGouxuan();
				$('img').lazyload({
					threshold : 5000
				});
				$("a, input:button").focus(function()	{
					$(this).blur();
				});
				$("input[type='checkbox']").change(changeGouxuan);
			});
			
			//改变勾选安装的颜色
			function changeGouxuan()	{
				if($("input:checked").length > 0)	{
					$("#downall").attr('class', "btn-gouxuan");
				} else	{
					$("#downall").attr('class', "btn-gouxuan2");
				}
			}
			
			//单个下载安装
			function downApp(appName, rscTypeId, ids){
				var url = "${pageContext.request.contextPath}/resource/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&imoduletype=${imoduletype}&pkgId=${pkgId}&ids=" + ids;
				window.external.MyOwnFunc(1, appName, rscTypeId, url);
			}
			
			//新的下载安
			function multiApp()	{
				var selectFlags = document.getElementsByName("selectFlag");
				var num = 1;
				for(var i=0; i<selectFlags.length; i++)	{
					if(selectFlags[i].checked)	{
						if(i<selectFlags.length){
							//每得到一个软件的ids就调用一次这个方法，将相关信息发给客户端。
							var ids = selectFlags[i].value;
							var name_typeId = $("#"+ids).val(); //这个得到的是该软件的名称_类别,必须是名称在前面,类别在后面！！！
							var arr=new Array();
							arr=name_typeId.split('_');
							var url = "${pageContext.request.contextPath}/resource/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pkgId=${pkgId}&imoduletype=${imoduletype}&ids=" + ids ;
							if(num == 1){
								num = 0;
								window.external.MyOwnFunc(1,arr[0],arr[1], url);
							}else{
								window.external.MyOwnFunc(0,arr[0],arr[1], url);
							}
						}
					}
				}
			}
		
		</script>
	</head>

	<body onselectstart="return false">
		<div class="gouxuananzhuang">
			<input type="button" id="downall" onclick="multiApp()"  value="" class="btn-gouxuan2" />
		</div>
		<div style="display : none;" class="holder"></div>
	
		<%--根据传入的action名, 判断传向详细信息页面的action值是什么 --%>
		<c:choose>
			<c:when	test="${action eq 'softList'}">
				<c:set value="softInfo" var="info" scope="page"></c:set>
			</c:when>
			<c:when	test="${action eq 'gameList'}">
				<c:set value="gameInfo" var="info" scope="page"></c:set>
			</c:when>
			<c:otherwise>
				<c:set value="appInfo" var="info" scope="page"></c:set>
			</c:otherwise>
		</c:choose>
		
		<div class="newsoft">
			<div class="newsoft-nav">
				<table border="0" cellpadding="0" cellspacing="0" align="center" >
					<tr >
						<td>
						<c:choose>
						<%-- 宝库用的分类 --%>
						<c:when test="${action eq 'softList' or action eq 'gameList'}">
							<ul id="jpagesContainer" style="margin-top:0px; margin-left:-40px;">
								<c:forEach items="${appTypeList}" var="type">
									<c:choose>
										<c:when test="${type.id eq typeId}">
											<li class="newsoft-nav-kid-sel">
												<a href="resource/resource.action?action=${action}&empId=${empId }&typeId=${type.id}&isIOS=${isIOS }&installType=${installType }&pageSize=${pageSize }&appOsType=${appOsType }&version=${version }&imoduletype=${imoduletype}">
													${type.name}
												</a>
											</li>
										</c:when>
										<c:otherwise><%-- 选中效果 --%>
											<li class="newsoft-nav-kid">
												<a href="resource/resource.action?action=${action}&empId=${empId }&typeId=${type.id}&isIOS=${isIOS }&installType=${installType }&pageSize=${pageSize }&appOsType=${appOsType }&version=${version }&imoduletype=${imoduletype}">
													${type.name}
												</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul>
							<c:if test="${!empty appTypeList }">
								<div id="prev" class="page-left1">
									<a href="javascript:void(0);"><img src="img_zizhu2/empty.png" width="37" height="40" /></a>
								</div>
								<div id="next" class="page-right1">
									<a href="javascript:void(0);"><img src="img_zizhu2/empty.png" width="37" height="40" /></a>
								</div>
								<script type="text/javascript">
									$('.holder').jPages({
						    			containerID : "jpagesContainer",
						    			perPage : 8,	//每页多少个
						    			previous : "#prev",
						    			next : "#next",
						    			delay : 10
							    	});
								</script>
							</c:if>
						</c:when>
						<c:otherwise>
							<div class="newsoft-t"> &nbsp; </div>
						</c:otherwise>
						</c:choose>
						</td>
					</tr>
				</table>
			</div>
			<div class="newsoft-nei">
				<c:if test="${empty(appPager.list)}">
					<center><font color="red" size="3">没有找到相关结果</font></center>
				</c:if>
				<pg:pager items="${appPager.totalCount}" maxPageItems="${pageSize}"
					maxIndexPages="10" isOffset="true" scope="request" export="offset,currentPageNumber=pageNumber" url="resource/resource.action">
					<pg:param name="action" value="${action}" />
					<pg:param name="empId" value="${empId}" />
					<pg:param name="typeId" value="${typeId}" />
					<pg:param name="pkgId" value="${pkgId}" />
					<pg:param name="isIOS" value="${isIOS}" />
					<pg:param name="installType" value="${installType}" />
					<pg:param name="imoduletype" value="${imoduletype}" />
					<pg:param name="appOsType" value="${appOsType}" />
					<pg:param name="version" value="${version}" />
					<pg:param name="pageSize" value="${pageSize}" />

					<c:forEach items="${appPager.list}" var="app" >
						<div class="newsoft-kid">
							<div class="newsoft-icon">
								<div class="newsoft-check">
									<input type="checkbox" class="inputcheck" id="softCheck_${app.uuid}" name="selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}" />
									<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
								</div>
							    <a title="${app.name}" href="resource/app_info.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=${imoduletype}">
								<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
								<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
									<span class="tj2"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
									<span class="rm2"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
									<span class="jl2"></span>
								</c:if>
								<c:if test="${app.markerType == null || '' eq app.markerType }">
									<span class=""></span>
								</c:if>
					       			<img src="img_woshop/loading.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" alt="${app.name}" width="50" height="50" align="baseline" > 
					      		</a>
								<div class="newsoft-icon-shade"></div>
							</div>
							<div class="newsoft-kid-name">${app.name}</div>
							<div class="newsoft-kid-lei">${app.rscTypeName}</div>
							<div class="newsoft-down">
								<input type="button" class="btn-newsoft-down" 
									onclick="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"
									onmousedown="$(this).addClass('btn-newsoft-down-sel')"
									onmouseup="$(this).removeClass('btn-newsoft-down-sel')"/>
								<div class="newsoft-down-shade"></div>
							</div>
						</div>
					</c:forEach>
					<div class="clear"></div>
						
					<pg:index>
						<%-- pg的页数标签放在<pg:index>里面 --%>
						<pg:prev>&nbsp;<div class="page-left"><a href="${pageUrl}"><img src="img_zizhu2/empty.png" width="37" height="40"/></a></div></pg:prev>
						<pg:next>&nbsp;<div class="page-right"><a href="${pageUrl}"><img src="img_zizhu2/empty.png" width="37" height="40"/></a></div></pg:next>
					</pg:index>
				</pg:pager>
			</div>
		</div>
	</body>
</html>

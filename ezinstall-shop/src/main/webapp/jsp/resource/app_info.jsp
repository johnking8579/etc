<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>应用详情</title>
		<base href="<%=basePath%>">
		<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
	    <LINK rel="stylesheet" type="text/css" href="css/css_woshop/soft.css">
		<link rel="stylesheet" type="text/css" href="css/css_woshop/more.css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/css_woshop/style.120517.css">
		<link rel="stylesheet" type="text/css" href="css/zizhu2.css" />
		<%@ include file="/jsp/common2.jsp" %>
		<script src="js/js_woshop/click_toggle.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function()	{
				$("img[name='icon']").lazyload();
				$("a, input:button").focus(function()	{
					$(this).blur();
				});
				
				$("img[id^='pic_']").lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/noPic.png"
	   			});
				$('img').lazyload({
					failure_limit : 100,
					failImg			: "img_woshop/zwtp.png"
	   			});
	   		});
	   		
	   		//单个的下载安装  
			function downApp(appName, rscTypeId, ids){
				var url = "${pageContext.request.contextPath}/resource/downLoad.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&pkgId=${pkgId}&imoduletype=${imoduletype }&ids=" + ids;
				window.external.MyOwnFunc(1,appName, rscTypeId, url);
			}
	   		
	   	</script>
		<style type="text/css">
			.STYLE11 {
				font-size: 14px;
				font-family: "微软雅黑";
				color: #333333;
			}
			.STYLE12 {
				font-size: 14px;
				font-weight: bold;
				color: #333333;
			}
			p.small
		  	{
		  		line-height: 12px
		  	}
			p.big
		  	{
		  		line-height: 25px
		  	}
		  	a:visited {
				text-decoration: none;
				color: #000000;
			}
			a:hover {
				text-decoration: none;
				color: #FF0000;
			}
			a:active {
				text-decoration: none;
				color: #0000cc
			}
		</style>
	</head>
	
	<body onLoad="clickMenu('outer','div','more')">
		<div class="info-back">
			<input type="button" value=" " class="btn-info-back"
				onclick="window.history.back()"
				onmousedown="$(this).addClass('btn-info-back-sel')"
				onmouseup="$(this).removeClass('btn-info-back-sel')" />
		</div>
		
		<table width="100%" border="0">
			<tr>
		    	<td height="36">
			 		
				</td>
		  	</tr>
		</table>
			
		<table width="100%" border="0" cellspacing="15">
			<tr>
			    <td width="13%" height="82">
		    		<div class="tpc" id="div6">
				    	<div align="center">
				    		<!-- 如果是推荐的，要标明出来  <span class="tj2">推荐</span> 标签：【${app.markerType }】-->
								<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
									<span class="tj"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
									<span class="rm"></span>
								</c:if>
								<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
									<span class="jl"></span>
								</c:if>
								<c:if test="${app.markerType == null || '' eq app.markerType }">
									<span class=""></span>
								</c:if>
								<img src="img_woshop/loading.gif" data-original="${fileserverUrl}${app.icon}" width="52" height="52" />
			    		</div>
			    	</div>
			    </td>
			    <td width="16%">
			    	<div align="center" class="info-jiben-xiazai">
			    		<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
						<input type="button" value=" " class="btn-info-xiazai"
							onclick="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}');"
							onmousedown="$(this).addClass('btn-info-xiazai-sel')"
							onmouseup="$(this).removeClass('btn-info-xiazai-sel')" />
			    	</div>
			    </td>
			    <td width="2%" rowspan="2"><p>&nbsp;</p></td>
			    <td width="48%" rowspan="2" valign="top">
			    	<span class="STYLE12">  ${app.name}   ${app.packAppVer} </span><br>
			    	<p class="big">
			    		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${fnx:abbreviate(app.info,200, "...")} 
			    	</p>
			    	<div id="outer">
			    		<div class="more">
			    			<p class="big">
			    				【更新内容】<br>
								${app.info }
							</p>
			    			<i class="show"><font size="3">展开&#187;</font></i>
			    			<i class="hide"><font size="3">&#171; 收起</font></i>
			    		</div>	
					</div>
				</td>
		   		<td width="1%" rowspan="2">&nbsp;</td>
		   		<td width="20%" rowspan="2">
		   			<div align="center">
		      			<!-- <p><img src="img_woshop/wyxewm.png" width="103" height="102"></p>
		      			<div align="center"> 
		      				使用手机扫描上方<br><br>
		        			二维码下载 
		        		</div> -->
		     			</div>
		     		</td>
		 		</tr>
				<tr>
				    <td height="141" colspan="2">
			    	<!-- 通过PackUuid查询的详情将展示一下隐藏信息：CP商名称（app.cpName）；原始Id（app.originalPackId） -->
					<c:if test="${imoduletype != 10 && imoduletype != 9 && packUuid != null && packUuid != ''}">
						<input type="hidden" name="CP商名称" value="${app.cpName }"/>
						<input type="hidden" name="原始Id" value="${app.originalPackId }"/>
					</c:if>
					
				    	
			    	&nbsp;大小：${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB <br><br>     
			    	&nbsp;下载量：
			    	<c:choose>
						<c:when test="${app.downCount eq null}">0</c:when>
						<c:otherwise>${app.downCount}</c:otherwise>
					</c:choose> &nbsp; 次 <br><br>
			       	&nbsp;类型：${app.rscTypeName }   <br><br>
			       	&nbsp;更新于: ${app.packCpUpdateTime} <br><br>
			       	&nbsp;适用于: 
					<c:if test="${app.packOs == 'os1'}">
	                	ANDROID
	                </c:if>
	                <c:if test="${app.packOs == 'os2'}">
	                	IPHONE
	                </c:if>
	                <c:if test="${app.packOs == 'os3'}">
	                	破解IPHONE
	                </c:if>
	                <c:if test="${app.packOs == 'os4'}">
	                	SYMBIAN
	               	</c:if>
	                <c:if test="${app.packOs == 'os5'}">
	                	JAVA
	             	</c:if>
	         		<c:if test="${app.packOs == 'os6'}">
	                	WINMOBILE
	             	</c:if>	 
	             	<c:if test="${app.packOsMinVer == null }">
	             		2.0
	             	</c:if>
	             	<c:if test="${app.packOsMinVer != null }">
	             		${app.packOsMinVer }
	             	</c:if>
	             	
	             	 以上系统 <br><br>
			    	&nbsp; ${app.developer }
			    </td>
		  	</tr>
		</table>
		<table width="100%" border="0">
		  <tr align="center">
		  	<td width="10%" height="403">&nbsp;</td>
		  	<c:forEach items="${app.pics}" var="pic" varStatus="varStat">
			    <c:if test="${(varStat.index < 4)}">
			    	<td width="20%">
				    	<div align="center"><img name="icon" src="img_woshop/noPic.png" data-original="${fileserverUrl}${pic}" id="pic_${app.uuid }" width="239" height="400"></div>
				    </td>
			    	<td width="1%">&nbsp;</td>
				    <c:if test="${(varStat.index+1) % 2 eq 0}"> </c:if>
			    </c:if>
			</c:forEach>
		    <td width="10%">&nbsp;</td>
		  </tr>
		</table>
		<p>&nbsp;</p>
	</body>
</html>

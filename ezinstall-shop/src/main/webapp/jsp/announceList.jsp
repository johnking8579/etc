<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
  	<link rel="stylesheet" type="text/css"	href="${pageContext.request.contextPath}/css/soft.css">
  </head>
  
  <body>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="${pageContext.request.contextPath}/img/xk.png">
		<tr >
        	<td width="2%" height="37"></td>
        	<td width="5%" >
        		<!-- <a href="javascript:history.go(-1)">
        			<img alt="返回" src="${pageContext.request.contextPath}/img/an3.png" width="30" height="24" border="0"/>
        		</a> -->
        	</td>
        	<td width="55%" ></td>
        	<td width="28%"  align="right"></td>
        	<td width="2%" ></td>
      	</tr>
    </table>
 	
 	<center>
  		<div style="margin-top:10px;">
			<center><h2 class="STYLE5 STYLE9">公告中心</h2></center>
			<table width="80%" align="center" border="1" cellspacing="0" cellpadding="0" bordercolor="#c8c8c8">
			<pg:pager maxPageItems="10"	maxIndexPages="10" isOffset="false"
				export="offset,currentPageNumber=pageNumber" scope="request" url="${pageContext.request.contextPath}/extra/announceList.action"> 
			<pg:param name="keyword" value="${param.areaCode}" />
				<tr height="32">
					<th width="20%" background="${pageContext.request.contextPath}/img/tj2.png">标题</th>
					<th width="60%" background="${pageContext.request.contextPath}/img/tj2.png">内容</th>
					<th width="20%" background="${pageContext.request.contextPath}/img/tj2.png">时间</th>
				</tr>
			<c:if test="${empty announceList}">
				<tr ><td colspan="3" align="center" height="50">对不起, 还没有相关公告!</td></tr>
			</c:if>
			<c:forEach items="${announceList}" var="announce">
			<pg:item>
				<tr height="30">
					<td align="center">${announce.title}</td>
					<td align="center">${announce.content}</td>
					<td align="center">${announce.uploadTime}</td>
				</tr>
			</pg:item>
			</c:forEach>
		
				<tr>
					<td colspan="3" height="50">
						<center>
						<pg:index>
			    		<font face="Helvetica" size=2>
							<pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
				    		<pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
						    <pg:pages>
						    	<c:choose>
							    	<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b></c:when>
							    	<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a></c:otherwise>
						    	</c:choose>
						    </pg:pages>
						    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
						    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
						    <br/>
			    		</font>
			 			</pg:index>
						</center>
					</td>
				</tr>
			</pg:pager>
			</table>
		</div>
	</center>
  </body>
</html>

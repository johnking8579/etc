<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<base href="<%=basePath%>"/>
		<link href="${pageContext.request.contextPath}/css/1.css" rel="stylesheet" type="text/css" />
		<script src="${pageContext.request.contextPath}/js/jquery-1.7.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.KinSlideshow-1.2.1.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function() {
				$("#KinSlideshow").KinSlideshow();
			})
		</script>
	</head>

	<body>
		<div class="divv">
			<div id="KinSlideshow" style="visibility: hidden;">
				<a><img src="${pageContext.request.contextPath}/images/1.jpg" width="1000" height="760" /></a>
				<a><img src="${pageContext.request.contextPath}/images/2.jpg" width="1000" height="760" /></a>
				<a><img src="${pageContext.request.contextPath}/images/3.jpg" width="1000" height="760" /></a>
				<a><img src="${pageContext.request.contextPath}/images/4.jpg" width="1000" height="760" /></a>
			</div>
		</div>
	</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<link href="css/soft.css" rel="stylesheet" type="text/css">
	<%@ include file="/jsp/common2.jsp" %>
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript">
		$(function()	{
			utilInit();
		});
	</script>
	
  </head>
  
  <body>
    	<pg:pager maxPageItems="40" maxIndexPages="10" isOffset="false"	export="offset,currentPageNumber=pageNumber" 
    		 scope="request" url="extra/phoneSel.action">
  	<table width="100%" border="0" cellpadding="0" cellspacing="0"  >
  	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
			    <tr>
			      <td width="100%" height="34" background="img/tj2.png">
			      &nbsp;<span align="right"> <a href="javascript:history.go(-1)">返回</a></span>
			      &nbsp;&nbsp;&nbsp;&nbsp;<span class="STYLE5 STYLE9">机型选择</span>
			      </td>
			      </tr>
			  </table>
		</td>
	</tr>
    <tr>
    <td align="center">
  <!-- 每页显示50条数据 -->
    
			<pg:param name="manuId" value="${param.manuId}"/>
		<c:if test="${empty typeInfoList}">
			<center><p style="margin-top:30px;"><font color="red" size="6">对不起，没有找到该厂商的相关机型。</font></p></center>
		</c:if>	
	   	<c:forEach items="${typeInfoList}" var="typeInfo"><pg:item>
        <div class="xinghao">
        	<div class="xinghao_img">
                <a href="print/phoneSel.action?typeId=${typeInfo.id}">
		   		<img height="130" width="80" data-original="${fileserverUrl}${typeInfo.bigPic}" src="img/blank_phone.png"></img></a>
            </div>
            <div class="xinghao_name">${typeInfo.typeName}</div>
        </div>
	   	</pg:item></c:forEach>
	   	
   </td>
   </tr>
   <tr>
   	<td>
   		<center><pg:index><font face="Helvetica" size=2>
			<pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
		    <pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
		    <pg:pages>
		    	<c:choose>
			    	<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b></c:when>
			    	<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a></c:otherwise>
		    	</c:choose>
		    </pg:pages>
		    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
		    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
		    <br/></font></pg:index></center>
   	</td>
   </tr>
   </table>
	   		
			 </pg:pager> 
</body>
</html>

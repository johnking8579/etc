<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
	<link rel="stylesheet" type="text/css" href="css/soft.css">
    <%@ include file="/jsp/common2.jsp" %>
    <script type="text/javascript" src="js/util.js"></script>
    <script type="text/javascript">
    	$(function()	{
    		utilInit();
    	});
    </script>
    <STYLE type=text/css>
			BODY {
				FONT-SIZE: 14px;
				FONT-FAMILY: "微软雅黑";
				background-color: #FFFFFF;
				background-repeat: no-repeat;
				background-image: url(img_woshop/bd.png);
			}
	</STYLE>
  </head>
  
 <body>
  	<center>
  	<pg:pager maxPageItems="40" maxIndexPages="10" isOffset="false"
			export="offset,currentPageNumber=pageNumber" scope="request" url="extra/manuSel2.action">
  	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
	<tr>
		<td>
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
			    <tr>
			      <td width="100%" height="34" background="img/tj2.png">
			      &nbsp;<span align="right"> <a href="javascript:history.go(-1)">返回</a></span>
			      &nbsp;&nbsp;&nbsp;&nbsp;<span class="STYLE5 STYLE9">机型选择</span>
			      </td>
			      </tr>
			  </table>
		</td>
	</tr>
    <tr>
    <td align="center">
    	<!-- 每页显示30条数据 -->
	   	<c:forEach items="${manuList}" var="manu">
	   	<pg:item>
        <div class="changshang" style="border:1px solid gray">
        	<div class="changshang_img">
		   		<a href="extra/modelSel.action?manuId=${manu.id}">
		   		<img width="100" height="100" data-original="${fileserverUrl}${manu.logo}" src="img/border.gif"></img></a>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="changshang_name">${manu.manuName}</div>
        </div>
	   	</pg:item>
	   	</c:forEach>
	</td>
   </tr>
   <tr>
   	<td height="50">
   		<center>
   			<pg:index><font face="Helvetica" size=2>
			<pg:first><a href="${pageUrl}"><nobr>[首页]</nobr></a></pg:first>
		    <pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a></pg:prev>
		    <pg:pages>
		    	<c:choose>
			    	<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b></c:when>
			    	<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a></c:otherwise>
		    	</c:choose>
		    </pg:pages>
		    <pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a></pg:next>
		    <pg:last>&nbsp;<a href="${pageUrl}"><nobr>[末页]</nobr></a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
		    <br/></font>
		    </pg:index>
   		</center>
   	</td>
   </tr>
   </table>
	</pg:pager>
	</center>
</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>



<table width="100%" border="0" align="center" cellspacing="12">
	<tr>
		<td width="66%" valign="middle">
			&nbsp;&nbsp;
			<input type="checkbox" name="bk_checkbox" id="bk_checkAll"/>
			<span class="STYLE13">全选</span>
			<span class="STYLE13"> 
				&nbsp;&nbsp;&nbsp;已选择
				<span id="bk_checkCount" class="STYLE14">0&nbsp;</span>款， 
				共<span id="bk_totalSize" class="STYLE14">0.00&nbsp;</span>MB 
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="woplusJ/appMain.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&pkgId=${pkgId }&typeId=${typeId }&imoduletype=10">更多>>></a>
			</span>
		</td>
		<td width="8%">
			<div align="left">
				<a href="javascript:bk_multiApp();" target="_self">
					<img src="img_woshop/yjanz.png" width="89" height="32" />
				</a>
			</div>
		</td>
		<td width="16%"></td>
	</tr>
</table>
					
<ul id="bodyul">
	<div align="center">
		<%-- 软件宝库下面的软件列表 --%>
		<c:if test="${empty appPager.list}"><center>对不起, 没有找到相关的软件!</center></c:if>
		<c:forEach items="${appPager.list}" var="app">
			<li class="app">
				<div class="tpc" id="tpc">
					<span class="gxk">
						<input type="checkbox" id="bkCheck_${app.uuid}" name="bk_selectFlag" value="${app.uuid}_${app.packUuid }_${app.discriminator}"/>
						<input type="hidden" id="bkFileSize_${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.packFileSize}"/>
	       				<input type="hidden" id="app.discriminator" value="${app.discriminator}"/>
	       				<input type="hidden" id="${app.uuid}_${app.packUuid }_${app.discriminator}" value="${app.name}_${app.rscTypeId }"/>
					</span>
					<a  href="woplusJ/appInfo.action?empId=${empId}&isIOS=${isIOS}&installType=${installType}&appOsType=${appOsType}&version='${version}'&pageSize=${pageSize}&pageSize2=${pageSize2}&app.uuid=${app.uuid}&packUuid=${app.packUuid }&imoduletype=10">
						<!-- 如果是推荐的，要标明出来  <span class="tj">推荐</span> 标签：【${app.markerType }】-->
						<c:if test="${app.markerType != null && app.markerType == 'mark01'}">
							<span class="tj"></span>
						</c:if>
						<c:if test="${app.markerType != null && app.markerType == 'mark02'}">
							<span class="rm"></span>
						</c:if>
						<c:if test="${app.markerType != null && app.markerType == 'mark03'}">
							<span class="jl"></span>
						</c:if>
						<c:if test="${app.markerType == null || '' eq app.markerType }">
							<span class=""></span>
						</c:if>
						<img src="img/border.gif" data-original="${fileserverUrl}${app.icon}" id="appimg_${app.uuid}" width="50" height="50" align="baseline" >
					</a>
					<span class="downtimes STYLE9 STYLE15" style="display: block;">
						<!-- 这个地方有个异常，可能名称的长度压根儿不足12位 -->
						${fnx:abbreviate(app.name,12, "...")}
					</span>
					<br />
	                	
                	<!-- ids:${app.uuid}_${app.packUuid }_${app.discriminator} 这个顺序不能变，一定是 appUuid_packUuid_discriminator -->
   					<a href="javascript:downApp('${app.name }','${app.rscTypeId }','${app.uuid}_${app.packUuid }_${app.discriminator}',10);"> 
  						<c:if test="${installType == 1 }"><!-- pc端直连 -->
       						<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
       					</c:if>
	  					<c:if test="${installType == 4 }"><!-- pc客户端短信下载 -->
	   						<img src="img_woshop/InstallButton-dx.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
	   					</c:if>
	   					<c:if test="${installType == 2 }"><!-- 蓝牙下载 -->
	   						<img src="img_woshop/InstallButton-a.png" imgAlterId1="${app.uuid }" imgAlterId2="${app.packUuid }"> 
	   					</c:if>
	   				</a>
					<div class="popUp">
						<font> ${fnx:abbreviate(app.name,20, "...")} </font>
						<span>
							${fnx:abbreviate(app.packAppVer,10, "...")}
							&nbsp;&nbsp;&nbsp; ${app.downCount }次<br /> 
			             	<span class="floatleft"> ${fnx:abbreviate(app.packFileSize/1024.0 ,4,"")} MB</span> &nbsp;&nbsp;&nbsp; 
			              	<c:if test="${app.cpUpdateTime == null }"></c:if>
			            	<c:if test="${app.cpUpdateTime != null }">${app.cpUpdateTime }</c:if>
			             	简介：${fnx:abbreviate(app.info,90, "...")}
						</span>
					</div>
				</div>
			</li>
		</c:forEach>
	</div>
</ul>

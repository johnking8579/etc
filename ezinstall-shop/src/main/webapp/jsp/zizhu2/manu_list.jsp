<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>"/>
		<link href="css/zizhu2.css"	rel="stylesheet" type="text/css" />
		<link href="css/flexslider-zizhu2.css" rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp" %>
		<script	src="js/jquery.flexslider-min.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(function()	{
				$('#main').fadeIn(500);
				//$('#changshang').fadeIn('slow');
				$('img').lazyload();
				
				$('.flexslider').flexslider({
					animation:'slide',
					slideshow: false,
					animationDuration: 500
				});
				
			});
			
			function locateTo(x)	{
				$('#main').fadeOut('normal', function()	{
					window.location = '${pageContext.request.contextPath}/terminal/phoneList.action?manuId=' + x;
				});
			}
			
		</script>
		<style></style>
	</head>
	<body onselectstart="return false">
		<div id="main" style="display: none">

			<div class="changshang">
				<div class="changshang-t">
					<img src="img_zizhu2/changshang-t.png" />
				</div>
				<div class="changshang-tt">
					<img src="img_zizhu2/changshang-tt.png" />
				</div>
				
				<div class="flexslider">
				<div class="changshang-nei">
					<ul class="slides">
						<c:forEach items="${manuLists}" var="list">
							<li>
								<div >
									<c:forEach items="${list}" var="manu">
										<div class="changshang-kid ">
											<div class="changshang-pic">
												<a href="touch.action?action=phoneList&manuId=${manu.id}">
													<img src="${fileserverUrl}${manu.logo}" alt="${manu.manuName}" />
												</a>
											</div>
											<div class="changshang-shade"></div>
										</div>
									</c:forEach>
									<div class="clear"></div>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
				</div>
			</div>
		</div>

	</body>
</html>

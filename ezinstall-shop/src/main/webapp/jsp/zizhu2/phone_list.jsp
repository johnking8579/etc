<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/jsp/common1.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>"/>
		<link href="css/zizhu2.css" rel="stylesheet" type="text/css" />
		<link href="css/jpages.css" rel="stylesheet" type="text/css" />
		<%@ include file="/jsp/common2.jsp" %>
		<script type="text/javascript" src="js/jPages.js"></script>
		<script type="text/javascript">
    	$(function()	{
    		$("a, button").focus(function()	{
    			$(this).blur();
    		});
    		
    		$(".jixing-zimu-pic, .jixing-zimu-pic-sel").click(function()	{
    			$(".jixing-zimu-pic-sel").attr("class", "jixing-zimu-pic");
   				$(this).attr("class", "jixing-zimu-pic-sel");
    		});
    		
    	});
    	
    	//选择首字母后, 机型列表联动变化
    	function find(manuId ,initial)	{
    		$.post("${pageContext.request.contextPath}/touch.action?action=phoneListMain&manuId=" + manuId + "&initial=" + initial, function(data){
    			$("#jpagesContainer1").html(data);
    			
    			//回调手机列表后绑定JPAGES
    			 $('.holder1').jPages({
		    			containerID : "jpagesContainer1",
		    			perPage : 12,	//每页多少个
		    			previous : "#prev1",
		    			next : "#next1"
		    		});
    			
    			
    			//回调手机列表后绑定单击
    			$(".jixing-xinghao-pic").click(function()	{
    				id = $(this).attr("typeId");
    				manuName = $(this).attr("manuName");
    				typeName = $(this).attr("typeName");
    				picPath = $(this).attr("pic");
    				show(id, manuName, typeName, picPath);
    			});
    		});
    	}
    	
    	//选择机型后, 右侧详细信息联动变化
    	function show(id, manuName, typeName, picPath)	{
			$('.jixing-tu-name').html(manuName +"-"+ typeName);
			$('#bigPic').attr('src', picPath);
			$('#chosenTypeId').val(id);
    	}	
    	
    	//带参数typeId传向后台进行查询
    	function href()	{
    		var typeId = $('#chosenTypeId').val();
			if(typeId != null && typeId != '')
				window.parent.location = "${pageContext.request.contextPath}/print/phoneSel.action?typeId=" + typeId;
    	}
    	
    </script>
	</head>

	<body onselectstart="return false">
		<div style="display: none" class="holder"></div>
		<div style="display: none" class="holder1"></div>
		<div class="info-back">
			<input type="button" value=" " class="btn-info-back"
				onclick="window.history.back()"
				onmousedown="$(this).addClass('btn-info-back-sel')"
				onmouseup="$(this).removeClass('btn-info-back-sel')" />
		</div>


		<div class="jixing">
			<div class="jixing-t">
				<img src="img_zizhu2/jixing-t.png" width="350" height="34" />
			</div>
			<div class="jixing-nei">
				<div class="jixing-zimu">
					<div class="jixing-zimu-nei">
						<ul id="jpagesContainer">
							<c:forEach items="${initialMap}" var="entry" >
								<li class="jixing-zimu-kid"
									onclick="find(${manuId}, '${entry.key}')">
									<div class="jixing-zimu-pic">
										<img src="${fileserverUrl}${entry.value}" width="352" height="572" />
										<div class="jixing-zimu-name">${entry.key}开头</div>
									</div>
								</li>
							</c:forEach>
						</ul>
						<div class="clear"></div>
						<div id="prev" class="page-left">
							<a><img src="img_zizhu2/empty.png" width="37" height="40" /></a>
						</div>
						<div id="next" class="page-right">
							<a><img src="img_zizhu2/empty.png" width="37" height="40" /></a>
						</div>
					</div>
				</div>

			<script type="text/javascript">
	            $('.holder').jPages({
	    			containerID : "jpagesContainer",
	    			perPage : 6,	//每页多少个
	    			previous : "#prev",
	    			next : "#next"
		    	});

            </script>


				<!-- =================================== -->

				<div class="jixing-xia">

					<div class="jixing-xinghao">
						<ul id="jpagesContainer1">
							<c:forEach items="${typeInfoList}" var="typeInfo">
									<c:set var="picPath" value="${fileserverUrl}${typeInfo.compressPic}" scope="page"/>
								<c:if test="${fn:length(typeInfo.compressPic) <= 0}">
									<c:set var="picPath" value="images/blank_phone.png"/>
								</c:if>
								<li class="jixing-xinghao-kid">
									<div class="jixing-xinghao-pic"
										onclick="show(${typeInfo.id}, '${typeInfo.manuInfo.manuName}', '${typeInfo.typeName}', '${picPath}')">
										<img src="${picPath}" width="352" height="572" />
										<div class="jixing-xinghao-name">
											${typeInfo.manuInfo.manuName}&nbsp; ${typeInfo.typeName}
										</div>
									</div>
								</li>
							</c:forEach>
						</ul>
						<div class="clear"></div>
						<div id="prev1" class="page-left"><a><img src="img_zizhu2/empty.png"	width="37" height="40"/></a></div>
						<div id="next1" class="page-right"><a><img src="img_zizhu2/empty.png" width="37" height="40"/></a></div>
				<script type="text/javascript">
	                $('.holder1').jPages({
		    			containerID : "jpagesContainer1",
		    			perPage : 12,	//每页多少个
		    			previous : "#prev1",
		    			next : "#next1"
		    		});
                </script>
					</div>

					<!-- =================================== -->

					<div class="jixing-tu">
						<div class="jixing-tu-pic" onclick="href()">
							<img id="bigPic" src="img_zizhu2/zhanwei.png"  />
						</div>
						<div class="jixing-tu-name"></div>
						<div class="jixing-tu-queding">
							<input type="button" value=" " class="btn-jixing-queding" onclick="href()" onmousedown="$(this).addClass('btn-jixing-queding-sel')"
									onmouseup="$(this).removeClass('btn-jixing-queding-sel')" onfocus="this.blur()"/>
							<input type="hidden" id="chosenTypeId" />
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</body>
</html>

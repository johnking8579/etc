工程简介:
功能:web网页,wap网页,手机端接口,对系统外部提供WEB服务.
接口访问地址都在index_index.html中


运行环境配置:
1. 服务器需要支持中文文件访问, 方法, 在Tomcat的server.xml中更改如下内容
		 <Connector port="8080" protocol="HTTP/1.1" 
               connectionTimeout="20000" 
               redirectPort="8443"
			   URIEncoding="utf-8"/>
			   
2. 数据库连接, 资源路径, 在这里配置:config.properties
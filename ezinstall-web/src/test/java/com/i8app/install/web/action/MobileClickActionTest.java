package com.i8app.install.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.opensymphony.xwork2.ActionProxy;

public class MobileClickActionTest extends StrutsSpringTestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}


	/**
	 *   添加订阅服务接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testaddSubscription() throws Exception	{
		String par = "{\"pid\":\"HY012013103000011\",\"eid\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/mobileClick/addSubscription");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	/**
	 * 查询搜索企业服务
	 * @throws Exception
	 */
	@Test
	public void testqueryEnterprise() throws Exception	{
		String par = "{\"name\":\"\",\"index\":\"0\",\"size\":\"10\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/mobileClick/queryEnterprise");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	/**
	 * 查询已订阅企业服务号
	 * @throws Exception
	 */
	@Test
	public void testquerySubscription() throws Exception	{
		String par = "{\"pid\":\"HY012013111400039\",\"index\":\"0\",\"size\":\"10\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/mobileClick/querySubscription");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	@Test
	public void testdelSubscription() throws Exception	{
		String par = "{\"pid\":\"HY012013111400039\",\"id\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/mobileClick/delSubscription");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
}

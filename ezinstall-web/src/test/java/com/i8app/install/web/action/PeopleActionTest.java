package com.i8app.install.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.opensymphony.xwork2.ActionProxy;

public class PeopleActionTest extends StrutsSpringTestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}

	@Test
	public void testgetProvAndCityID() throws Exception {
		String param = "{\"proname\":\"北京\",\"cityname\":\"北京\"}";
		request.setContent(param.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/getProvAndCityID");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}
	
	
	/**
	 * 登陆
	 * @throws Exception
	 */
	@Test
	public void testlogin() throws Exception	{
		String par = "{\"userNumber\":\"15901031592\",\"userPassword\":\"234234\",\"peopleID\":\"21341234\",\"phoneIMEI\":\"222222\",\"phoneIMSI\":\"22435234\",\"manuName\":\"asd\",\"modelName\":\"\",\"phoneOS\":\"49\",\"phoneOSversion\":\"9000451\",\"privinceName\":\"北京\",\"cityName\":\"北京\",\"deptID\":\"01\",\"deptName\":\"联通\"}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/login");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	/**
	 * 登陆
	 * @throws Exception
	 */
	@Test
	public void testsaveClientActivityLog() throws Exception	{
		String par = "{\"userNumber\":\"15901031592\",\"peopleID\":\"21341234\",\"phoneIMEI\":\"222222\",\"phoneIMSI\":\"22435234\",\"phoneNumber\":\"15901031592\",\"activityType\":\"1\"}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/saveClientActivityLog");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	/**
	 * 首次身份确认
	 * @throws Exception
	 */
	@Test
	public void testInitialAuthentication() throws Exception	{
		String par = "{\"phoneNo\":\"15901031592\",\"imsi\":\"234234\",\"imei\":\"21341234\",\"phoneOs\":\"android\",\"phoneOsVer\":\"2.2\",\"manuName\":\"asd\",\"modelName\":\"\",\"privinceID\":\"49\",\"cityID\":\"9000451\",\"privinceName\":\"北京\",\"cityName\":\"北京\",\"deptID\":\"01\",\"deptName\":\"联通\"}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/InitialAuthentication");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}

	/**
	 * 用户下载
	 * @throws Exception
	 */
	@Test
	public void testpeopledown() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"userNumber\":\"yangadfa\",\"phoneNo\":\"15901031592\",\"imsi\":\"234234\",\"imei\":\"21341234\",\"phoneOs\":\"android\",\"phoneOsVer\":\"2.2\",\"manuName\":\"asd\",\"modelName\":\"\",\"privinceID\":\"49\",\"cityID\":\"9000451\",\"privinceName\":\"北京\",\"cityName\":\"北京\",\"deptID\":\"01\",\"deptName\":\"联通\",\"appinfo\":[{\"packUUID\":\"aweawe\",\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"columnName\":\"\",\"columnID\":\"\",\"operTime\":\"2013-10-22 12:21:00\",\"oper\":\"7\"},{\"packUUID\":\"aweawe\",\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"columnName\":\"\",\"columnID\":\"\",\"operTime\":\"2013-10-22 12:21:00\",\"oper\":\"7\"},{\"packUUID\":\"aweawe\",\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"columnName\":\"\",\"columnID\":\"\",\"operTime\":\"2013-10-22 12:21:00\",\"oper\":\"7\"}]}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/peopledown");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	/**
	 * 用户流量
	 * @throws Exception
	 */
	@Test
	public void testpeopleflowdown() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"userNumber\":\"yangadfa\",\"phoneNo\":\"15901031592\",\"imsi\":\"234234\",\"imei\":\"21341234\",\"phoneOs\":\"android\",\"phoneOsVer\":\"2.2\",\"manuName\":\"asd\",\"modelName\":\"\",\"privinceID\":\"49\",\"cityID\":\"9000451\",\"privinceName\":\"北京\",\"cityName\":\"北京\",\"deptID\":\"01\",\"deptName\":\"联通\",\"flowinfo\":[{\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"},{\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"},{\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"}]}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/peopleflowdown");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	/**
	 * 应用流量
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testappFlowdown() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"userNumber\":\"yangadfa\",\"phoneNo\":\"15901031592\",\"imsi\":\"234234\",\"imei\":\"21341234\",\"phoneOs\":\"android\",\"phoneOsVer\":\"2.2\",\"manuName\":\"asd\",\"modelName\":\"\",\"privinceID\":\"49\",\"cityID\":\"9000451\",\"privinceName\":\"北京\",\"cityName\":\"北京\",\"deptID\":\"01\",\"deptName\":\"联通\",\"flowinfo\":[{\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"},{\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"},{\"packName\":\"\",\"appVersion\":\"\",\"appName\":\"\",\"appTypeId\":\"\",\"appTypeName\":\"\",\"startTime\":\"2013-10-21 13:20:25\",\"endTime\":\"2013-10-21 13:20:25\",\"flowValue\":\"\",\"wifiFlowValue\":\"\",\"mobileFlowValue\":\"\"}]}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/appFlowdown");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	/**
	 * 卸载接口
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testUninstall() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"phoneNo\":\"15901031592\",\"imsi\":\"234234\",\"imei\":\"21341234\",\"packname\":\"com.xx.xx\",\"os\":\"os1\"}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/Uninstall");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	/**
	 * 意见反馈
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testfeedback() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"userNumber\":\"15901031592\",\"phoneNo\":\"15901031592\",\"privinceID\":\"19\",\"cityID\":\"19999\",\"feeType\":\"zan\",\"content\":\"asdfasdaf\"}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/feedback");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	/**
	 * 配置文件接口
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testconfiguration() throws Exception	{
		String par = "{\"jsonStr\": {\"timing\":2}}";
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/configuration");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	
	/**
	 * 信号质量
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testsignalqualitydata() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"username\":\"15901031592\",\"phoneno\":\"15901031592\",\"cid\":\"8253\",\"cityname\":\"北京\",\"deptId\":\"01\",\"deptname\":\"联通\",\"imei\":\"12013102202222\",\"imsi\":\"15901032341592\",\"lac\":\"4426\",\"latitude\":\"31.404924\",\"location\":\"中国江苏省南京市滨江大道\",\"longitude\":\"120.660296\",\"manuname\":\"HTC\",\"modelname\":\"A6390\",\"mcc\":\"460\",\"mnc\":\"1\",\"nettype\":\"12\",\"os\":\"os1\",\"osversion\":\"2.2\",\"provname\":\"北京\",\"sigQua\":\"-70\",\"provId\":\"19\",\"cityID\":\"100\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/signalqualitydata");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	/**
	 * 信号投诉
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testsignalrequest() throws Exception	{
		String par = "{\"peopleID\":\"HY012013102202222\",\"username\":\"15901031592\",\"phoneno\":\"15901031592\",\"cid\":\"8253\",\"cityname\":\"北京\",\"deptId\":\"01\",\"deptname\":\"联通\",\"imei\":\"12013102202222\",\"imsi\":\"15901032341592\",\"lac\":\"4426\",\"latitude\":\"31.404924\",\"location\":\"中国江苏省南京市滨江大道\",\"longitude\":\"120.660296\",\"manuname\":\"HTC\",\"modelname\":\"A6390\",\"mcc\":\"460\",\"mnc\":\"1\",\"nettype\":\"12\",\"os\":\"os1\",\"osversion\":\"2.2\",\"provname\":\"北京\",\"sigQua\":\"-70\",\"provId\":\"19\",\"cityID\":\"100\",\"phoneIp\":\"192.168.1.110\",\"conent\":\"撒大大通\",\"question\":\"无信号\",\"scene\":\"通话\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/signalrequest");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	
	/**
	 * 信号质量
	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testupdatenew() throws Exception	{
		String par = "{\"apkPkgName\":\"com.iyd.reader.book57257\",\"version\":\"3.1.1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/updatenew");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	
	
	/**
	 *   批量软件更新
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testbulupdatenew() throws Exception	{
		String par = "[{\"apkPkgName\":\"com.iyd.reader.book57257\",\"version\":\"9.9.9.9.9\"},{\"apkPkgName\":\"com.iyd.reader.book57257\",\"version\":\"9.9.9.9.9\"}]";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/bulupdatenew");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	
	/**
	 *   批量软件更新
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testupdatePeople() throws Exception	{
		String par = "{\"userNumber\":\"18636153223\",\"userPassword\":\"123456\",\"imgType\":\"123456\",\"imgData\":\"123456\",\"userNickname\":\"123456\",\"userGender\":\"123456\",\"phoneIMEI\":\"123456\",\"phoneIMSI\":\"123456\",\"manuName\":\"123456\",\"modelName\":\"123456\",\"phoneOS\":\"123456\",\"phoneOSversion\":\"123456\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/updatePeople");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
	
	
	
	/**
	 *   短信备份接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testsms_backup() throws Exception	{
		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/sms_backup");
		proxy.execute();
		String s = response.getContentAsString();
//		assertEquals("{\"success\":0}", s);
		System.out.println(s);
//		assertEquals("{\"success\":0}", response.getContentAsString());
	}
}

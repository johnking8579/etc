package com.i8app.install.web.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsSpringTestCase;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.opensymphony.xwork2.ActionProxy;

public class CopyActionTest extends StrutsSpringTestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		SessionFactory sessionFactory = lookupSessionFactory(request);
		Session hibernateSession= getSession(sessionFactory);
		TransactionSynchronizationManager.bindResource(sessionFactory, 
		                                               new SessionHolder(hibernateSession));
	}
	
	 private Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
	     Session session = SessionFactoryUtils.getSession(sessionFactory, true);
	     FlushMode flushMode = FlushMode.NEVER;
	     if (flushMode != null) {
	        session.setFlushMode(flushMode);
	     }
	      return session;
	 }
	 private SessionFactory lookupSessionFactory(HttpServletRequest request) {
	     return (SessionFactory)this.applicationContext.getBean(SessionFactory.class);
	}


	/**
	 *   软件存在查询接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testapp_query_exist() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"uid\":\"HY012013112700001\",\"app\":[{\"pname\":\"com.hk.app\",\"versioncode\":\"102\"},{\"pname\":\"com.hk.app\",\"versioncode\":\"102\"}]}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/app_query_exist");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   软件名备份接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testapp_backup_name() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"peopleid\":\"HY012013112800001\",\"app\":[{\"uid\":\"HY012013112700001\",\"pname\":\"com.hk.app\",\"versioncode\":\"102\",\"name\":\"惠客通\",\"version\":\"1.02\",\"number\":\"15901031592\",\"size\":\"253.25\"},{\"uid\":\"HY012013112700001\",\"pname\":\"com.hk.app\",\"versioncode\":\"102\",\"name\":\"惠客通\",\"version\":\"1.02\",\"number\":\"15901031592\",\"size\":\"253.25\"}]}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/app_backup_name");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   软件备份查询接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testapp_query() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"uid\":\"HY012013112700001\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/app_query");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   软件备份删除接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testapp_delete() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"id\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/app_delete");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   软件备份查询接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testsms_query() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"uid\":\"HY012013111900051\",\"thread_id\":\"28\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/sms_query");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   短信回话删除接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testsms_delete_thread() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"uid\":\"HY012013112400001\",\"ids\":[{\"id\":\"1\"},{\"id\":\"1\"}]}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/sms_delete_thread");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	/**
	 *   图片删除接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testimage_delete() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"id\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/image_delete");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   图片查询接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testimage_query() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"id\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/image_query");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	/**
	 *   图片备份接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testimage_backup() throws Exception	{
//		String par = "{\"thread\":{\"uid\":\"18636153223\",\"number\":\"123456\",\"persons\":\"123456\",\"count\":\"123456\",\"snippet\":\"123456\",\"time\":\"2013-11-22 15:15:10\",\"read\":\"0\",\"type\":\"1\",\"thread_id\":\"1\"},\"smsArray\":[{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"},{\"sms_id\":\"1\",\"address\":\"22\",\"person\":\"asda\",\"time\":\"2013-11-15 14:12:14\",\"protocol\":\"1\",\"read\":\"1\",\"status\":\"1\",\"type\":\"1\",\"present\":\"1\",\"subject\":\"1\",\"body\":\"1\",\"center\":\"1\",\"locked\":\"1\",\"code\":\"1\",\"seen\":\"1\"}]}";
		String par = "{\"id\":\"1\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/image_backup");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   评论接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testPinglun() throws Exception	{
		String par = "{\"uuid\":\"056c6841-825c-4268-b998-ddb627ba5f72\",\"content\":\"很好用的软件应用！\",\"nickname\":\"冬日的阳光\",\"peopleId\":\"HY012014012200001\",\"phoneno\":\"15901031592\",\"rating\":\"5.0\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/appcomment");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   猜你喜欢接口
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testguessyoulike() throws Exception	{
		String par = "{\"phone\":\"15901031592\",\"index\":\"0\",\"size\":\"10\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/guessyoulike");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void test() throws Exception	{
		String par = "{\"uuid\":\"18636153223\",\"peopleId\":\"123456\"}";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/searchcomment");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	/**
	 *   
	 *	 * http://192.168.1.70:8080/wap
	 * @throws Exception
	 */
	@Test
	public void testbulupdatenew() throws Exception	{
		String par = "[{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}},{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}},{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}}]";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/bulupdatenew");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   
	 *	批量垃圾短信测试
	 * @throws Exception
	 */
	@Test
	public void testbulsmsspam() throws Exception	{
		String par = "[{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":[{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}]},{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":[{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}]},{\"thread\":{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\"},\"smsArray\":[{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}]}]";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/smsspam");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
	
	
	/**
	 *   
	 *	垃圾短信测试
	 * @throws Exception
	 */
	@Test
	public void testsmsspam() throws Exception	{
		String par = "[{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\",\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\",\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"},{\"uid\":\"HY012013102800002\",\"number\":\"18636153223\",\"content\":\"adsfasd\",\"time\":\"2014-02-14 10:10:00\"}]";
		System.out.println(par);
		request.setContent(par.getBytes("utf-8"));
		ActionProxy proxy = getActionProxy("/people/smsspam");
		proxy.execute();
		String s = response.getContentAsString();
		System.out.println(s);
	}
}

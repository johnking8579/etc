package com.i8app.install.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Biz {
	private Integer id;
	private String name, info;

	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}

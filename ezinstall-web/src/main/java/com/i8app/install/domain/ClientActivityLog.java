package com.i8app.install.domain;

import javax.persistence.*;

@Entity
public class ClientActivityLog {
	private int id;
	private String userNumber;
	private String phoneNumber;
	private String phoneIMEI;
	private String phoneIMSI;
	private String activityDateTime;
	private String activityType;
	private String peopleId;
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserNumber() {
		return userNumber;
	}
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneIMEI() {
		return phoneIMEI;
	}
	public void setPhoneIMEI(String phoneIMEI) {
		this.phoneIMEI = phoneIMEI;
	}
	public String getPhoneIMSI() {
		return phoneIMSI;
	}
	public void setPhoneIMSI(String phoneIMSI) {
		this.phoneIMSI = phoneIMSI;
	}
	public String getActivityDateTime() {
		return activityDateTime;
	}
	public void setActivityDateTime(String activityDateTime) {
		this.activityDateTime = activityDateTime;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
}

package com.i8app.install.domain;

import javax.persistence.*;

@Entity
public class BookInfo {
	private int id;
	private String title;
	private String author;
	private BookType bookType;
	private int fileSize;
	private String publishDate;
	private String addDate;
	private String intro;
	private String coverPic;
	private String url;
	private String wapUrl;
	private SoftSuffix suffix;
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(length = 64)
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(length = 64)
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "bookTypeId", nullable=true)
	public BookType getBookType() {
		return bookType;
	}
	public void setBookType(BookType bookType) {
		this.bookType = bookType;
	}
	
	public int getFileSize() {
		return fileSize;
	}
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}
	@Column(length = 64)
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	@Column(length = 64)
	public String getAddDate() {
		return addDate;
	}
	public void setAddDate(String addDate) {
		this.addDate = addDate;
	}
	@Column(length = 5000)
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	
	public String getCoverPic() {
		return coverPic;
	}
	public void setCoverPic(String coverPic) {
		this.coverPic = coverPic;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getWapUrl() {
		return wapUrl;
	}
	public void setWapUrl(String wapUrl) {
		this.wapUrl = wapUrl;
	}
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "suffixId" ,nullable=true)
	public SoftSuffix getSuffix() {
		return suffix;
	}
	public void setSuffix(SoftSuffix suffix) {
		this.suffix = suffix;
	}
}

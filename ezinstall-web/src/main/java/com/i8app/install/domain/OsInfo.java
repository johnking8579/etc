package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class OsInfo {
	private Integer id;
	private Integer osType;
	private String osVersion;
	private SoftSuffix suffix1;
	private SoftSuffix suffix2;
	private SoftSuffix suffix3;
	private SoftSuffix suffix4;
	private SoftSuffix suffix5;
	private SoftSuffix suffix6;
	
	@Id
	@Column(name="osId")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	
	@Column(length=128)
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	@ManyToOne
	@JoinColumn(name="suffix1")
	public SoftSuffix getSuffix1() {
		return suffix1;
	}
	public void setSuffix1(SoftSuffix suffix1) {
		this.suffix1 = suffix1;
	}
	
	@ManyToOne
	@JoinColumn(name="suffix2")
	public SoftSuffix getSuffix2() {
		return suffix2;
	}
	public void setSuffix2(SoftSuffix suffix2) {
		this.suffix2 = suffix2;
	}
	
	@ManyToOne
	@JoinColumn(name="suffix3")
	public SoftSuffix getSuffix3() {
		return suffix3;
	}
	public void setSuffix3(SoftSuffix suffix3) {
		this.suffix3 = suffix3;
	}
	
	@ManyToOne
	@JoinColumn(name="suffix4")
	public SoftSuffix getSuffix4() {
		return suffix4;
	}
	public void setSuffix4(SoftSuffix suffix4) {
		this.suffix4 = suffix4;
	}
	
	@ManyToOne
	@JoinColumn(name="suffix5")
	public SoftSuffix getSuffix5() {
		return suffix5;
	}
	public void setSuffix5(SoftSuffix suffix5) {
		this.suffix5 = suffix5;
	}
	
	@ManyToOne
	@JoinColumn(name="suffix6")
	public SoftSuffix getSuffix6() {
		return suffix6;
	}
	public void setSuffix6(SoftSuffix suffix6) {
		this.suffix6 = suffix6;
	}
	

}

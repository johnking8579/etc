package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 游戏信息表
 * @author JingYing
 * V1.1将主键的字段名更改为suffixId
 */
@Entity
public class GameInfo {

	private Integer id;
    private String softName;
    private String kaifamanu;
    private String softVersion;
    private Integer fileSize;
    private Integer softFee;
    private GameType gameType;
    private String shangjiadate;
    private String softInfo;
    private Integer osType;
    private OsInfo osInfo;
    private SoftSuffix softSuffix;
    private String iconFileName;
    private String picFileName1;
    private String picFileName2;
    private String picFileName3;
    private String softFileName;
    private Integer hotRecmd;
    private Integer downNum;
    private Integer installCount;
    private Integer weekInstallCount;
    private Integer monthInstallCount;
    private Integer userRemarkNum;
    private Float userRemark;
    private Integer softFlag;
    private String softUrl;
    private String legalFileName;
	
	@Id
	@GeneratedValue
	@Column(name="gid")
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="gameName")
	public String getSoftName() {
		return softName;
	}
	public void setSoftName(String softName) {
		this.softName = softName;
	}
	public String getKaifamanu() {
		return kaifamanu;
	}
	public void setKaifamanu(String kaifamanu) {
		this.kaifamanu = kaifamanu;
	}
	
	@Column(name="gameVersion", length=128)
	public String getSoftVersion() {
		return softVersion;
	}
	public void setSoftVersion(String softVersion) {
		this.softVersion = softVersion;
	}
	public Integer getFileSize() {
		return fileSize;
	}
	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}
	
	@Column(name="gameFee")
	public Integer getSoftFee() {
		return softFee;
	}
	public void setSoftFee(Integer softFee) {
		this.softFee = softFee;
	}

	public String getShangjiadate() {
		return shangjiadate;
	}
	public void setShangjiadate(String shangjiadate) {
		this.shangjiadate = shangjiadate;
	}
	
	@Column(name="gameInfo", length=5000)
	public String getSoftInfo() {
		return softInfo;
	}
	public void setSoftInfo(String softInfo) {
		this.softInfo = softInfo;
	}
	public Integer getOsType() {
		return osType;
	}
	public void setOsType(Integer osType) {
		this.osType = osType;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="osId")
	public OsInfo getOsInfo() {
		return osInfo;
	}
	public void setOsInfo(OsInfo osInfo) {
		this.osInfo = osInfo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="suffixId")
	public SoftSuffix getSoftSuffix() {
		return softSuffix;
	}
	public void setSoftSuffix(SoftSuffix softSuffix) {
		this.softSuffix = softSuffix;
	}
	public String getIconFileName() {
		return iconFileName;
	}
	public void setIconFileName(String iconFileName) {
		this.iconFileName = iconFileName;
	}
	public String getPicFileName1() {
		return picFileName1;
	}
	public void setPicFileName1(String picFileName1) {
		this.picFileName1 = picFileName1;
	}
	public String getPicFileName2() {
		return picFileName2;
	}
	public void setPicFileName2(String picFileName2) {
		this.picFileName2 = picFileName2;
	}
	public String getPicFileName3() {
		return picFileName3;
	}
	public void setPicFileName3(String picFileName3) {
		this.picFileName3 = picFileName3;
	}
	
	@Column(name="gameFileName")
	public String getSoftFileName() {
		return softFileName;
	}
	public void setSoftFileName(String softFileName) {
		this.softFileName = softFileName;
	}
	public String getLegalFileName() {
		return legalFileName;
	}
	public void setLegalFileName(String legalFileName) {
		this.legalFileName = legalFileName;
	}
	
	@Column(name="gameUrl", length=128)
	public String getSoftUrl() {
		return softUrl;
	}
	public void setSoftUrl(String softUrl) {
		this.softUrl = softUrl;
	}
	public Integer getHotRecmd() {
		return hotRecmd;
	}
	public void setHotRecmd(Integer hotRecmd) {
		this.hotRecmd = hotRecmd;
	}
	public Integer getDownNum() {
		return downNum;
	}
	public void setDownNum(Integer downNum) {
		this.downNum = downNum;
	}
	public Integer getInstallCount() {
		return installCount;
	}
	public void setInstallCount(Integer installCount) {
		this.installCount = installCount;
	}
	public Integer getUserRemarkNum() {
		return userRemarkNum;
	}
	public void setUserRemarkNum(Integer userRemarkNum) {
		this.userRemarkNum = userRemarkNum;
	}
	public Float getUserRemark() {
		return userRemark;
	}
	public void setUserRemark(Float userRemark) {
		this.userRemark = userRemark;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="gameType")
	public GameType getGameType() {
		return gameType;
	}
	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}
	public Integer getWeekInstallCount() {
		return weekInstallCount;
	}
	public void setWeekInstallCount(Integer weekInstallCount) {
		this.weekInstallCount = weekInstallCount;
	}
	public Integer getMonthInstallCount() {
		return monthInstallCount;
	}
	public void setMonthInstallCount(Integer monthInstallCount) {
		this.monthInstallCount = monthInstallCount;
	}
	
	@Column(name="gameFlag")
	public Integer getSoftFlag() {
		return softFlag;
	}
	public void setSoftFlag(Integer softFlag) {
		this.softFlag = softFlag;
	}
	
     
}
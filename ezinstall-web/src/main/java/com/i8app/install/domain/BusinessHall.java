package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
public class BusinessHall {
	private Integer hallID;
	private String hallName, hallNo;
	private Countyinfo county;
	private Cityinfo city;
	private Provinceinfo province;
	private int ctid;
	private int ccid;
	private int rccid;
	private Integer bhid;
	private String chanGrade, chanLevel, chanType;
	
	
	@Id
	@GeneratedValue
	public Integer getHallID() {
		return hallID;
	}

	public void setHallID(Integer hallID) {
		this.hallID = hallID;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cityID") 
	public Cityinfo getCity() {
		return city;
	}

	public void setCity(Cityinfo city) {
		this.city = city;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "provinceid") 
	public Provinceinfo getProvince() {
		return province;
	}
	public void setProvince(Provinceinfo province) {
		this.province = province;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "countyID") 
	public Countyinfo getCounty() {
		return county;
	}

	public void setCounty(Countyinfo county) {
		this.county = county;
	}

	public int getCtid() {
		return ctid;
	}

	public void setCtid(int ctid) {
		this.ctid = ctid;
	}

	public int getCcid() {
		return ccid;
	}

	public void setCcid(int ccid) {
		this.ccid = ccid;
	}

	public int getRccid() {
		return rccid;
	}

	public void setRccid(int rccid) {
		this.rccid = rccid;
	}

	public String getHallNo() {
		return hallNo;
	}

	public void setHallNo(String hallNo) {
		this.hallNo = hallNo;
	}

	public Integer getBhid() {
		return bhid;
	}

	public void setBhid(Integer bhid) {
		this.bhid = bhid;
	}

	public String getChanGrade() {
		return chanGrade;
	}

	public void setChanGrade(String chanGrade) {
		this.chanGrade = chanGrade;
	}

	public String getChanLevel() {
		return chanLevel;
	}

	public void setChanLevel(String chanLevel) {
		this.chanLevel = chanLevel;
	}

	public String getChanType() {
		return chanType;
	}

	public void setChanType(String chanType) {
		this.chanType = chanType;
	}
	
	
}

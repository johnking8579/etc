package com.i8app.install.domain;

import javax.persistence.*;


@Entity
@Table(name="mobile_flow_log")
public class Phonewizard {

     private Integer id;
     private String empno;
     private String imei;
     private String imsi;
     private String phoneNo;
     private String deviceNo;
     private String rscid;
     private String rscname;
     private String versionName;
     private Integer versionCode;
     private String packageName;
     private Integer flowSize;
     private Integer flowType;
     private String sourcetype;
     private String manuName;
     private String ostype;
     private Integer provid;
     private Integer cityid;
     private Integer countyid;
     private Integer hallid;
     private String provname;
     private String cityname;
     private String countyname;
     private String hallname;
     private String empname;
     private Integer ctid;
     private Integer ccid;
     private Integer rccid;
	private String newtime;
	private int detectType;
	private int gflow;
	private int wifiFlow;
    public String getNewtime() {
		return newtime;
	}

	public void setNewtime(String newtime) {
		this.newtime = newtime;
	}

	@Id
    @GeneratedValue
    @Column(name="id")
    public Integer getId() {
		return id;
	}

	public String getEmpno() {
		return empno;
	}

	public void setEmpno(String empno) {
		this.empno = empno;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}


	public String getRscid() {
		return rscid;
	}

	public void setRscid(String rscid) {
		this.rscid = rscid;
	}

	public String getRscname() {
		return rscname;
	}

	public void setRscname(String rscname) {
		this.rscname = rscname;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public Integer getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Integer getFlowType() {
		return flowType;
	}

	public Integer getFlowSize() {
		return flowSize;
	}

	public void setFlowSize(Integer flowSize) {
		this.flowSize = flowSize;
	}

	public void setFlowType(Integer flowType) {
		this.flowType = flowType;
	}

	public String getSourcetype() {
		return sourcetype;
	}

	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}

	public String getManuName() {
		return manuName;
	}

	public void setManuName(String manuName) {
		this.manuName = manuName;
	}

	public String getOstype() {
		return ostype;
	}

	public void setOstype(String ostype) {
		this.ostype = ostype;
	}

	public Integer getProvid() {
		return provid;
	}

	public void setProvid(Integer provid) {
		this.provid = provid;
	}

	public Integer getCityid() {
		return cityid;
	}

	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	public Integer getCountyid() {
		return countyid;
	}

	public void setCountyid(Integer countyid) {
		this.countyid = countyid;
	}

	public Integer getHallid() {
		return hallid;
	}

	public void setHallid(Integer hallid) {
		this.hallid = hallid;
	}

	public String getProvname() {
		return provname;
	}

	public void setProvname(String provname) {
		this.provname = provname;
	}

	public String getCityname() {
		return cityname;
	}

	public void setCityname(String cityname) {
		this.cityname = cityname;
	}

	public String getCountyname() {
		return countyname;
	}

	public void setCountyname(String countyname) {
		this.countyname = countyname;
	}

	public String getHallname() {
		return hallname;
	}

	public void setHallname(String hallname) {
		this.hallname = hallname;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public Integer getCtid() {
		return ctid;
	}

	public void setCtid(Integer ctid) {
		this.ctid = ctid;
	}

	public Integer getCcid() {
		return ccid;
	}

	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

	public Integer getRccid() {
		return rccid;
	}

	public void setRccid(Integer rccid) {
		this.rccid = rccid;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public int getDetectType() {
		return detectType;
	}

	public void setDetectType(int detectType) {
		this.detectType = detectType;
	}

	public int getGflow() {
		return gflow;
	}

	public void setGflow(int gflow) {
		this.gflow = gflow;
	}

	public int getWifiFlow() {
		return wifiFlow;
	}

	public void setWifiFlow(int wifiFlow) {
		this.wifiFlow = wifiFlow;
	}



}
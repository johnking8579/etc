package com.i8app.install.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 临时驱动上传表, 一条记录里面只有一个VID和PID, 不存放文件路径, 只存放驱动文件名
 * 
 * @author JingYing
 * 
 */
@Entity(name = "driverInfo_temp")
public class TempDriverInfo {
	private int id;
	private String vid, pid, fileName;

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}

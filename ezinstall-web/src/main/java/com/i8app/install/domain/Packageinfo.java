package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mobile_installed_list")
public class Packageinfo {
	private Integer id;
	private String deviceNo, apkPkgName,ipaBundleId,installTime;
	@Id
	@Column(name="id" )
	@GeneratedValue(strategy = GenerationType.AUTO)  
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDeviceNo() {
		return deviceNo;
	}
	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}
	public String getApkPkgName() {
		return apkPkgName;
	}
	public void setApkPkgName(String apkPkgName) {
		this.apkPkgName = apkPkgName;
	}
	public String getIpaBundleId() {
		return ipaBundleId;
	}
	public void setIpaBundleId(String ipaBundleId) {
		this.ipaBundleId = ipaBundleId;
	}
	public String getInstallTime() {
		return installTime;
	}
	public void setInstallTime(String installTime) {
		this.installTime = installTime;
	}
}

package com.i8app.install.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * 2014-03-24 15:57 企业服务号订阅表
 * @author lixiangyang
 * @version 1.0
 *
 */
@Entity
@Table(name="subscription")
public class Subscription {
	private Integer id;
	private EnterpriseInfo eid;
	private PeopleInfo peopleID;
	private String takeTime;
	
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name = "eid") 
	public EnterpriseInfo getEid() {
		return eid;
	}
	public void setEid(EnterpriseInfo eid) {
		this.eid = eid;
	}
	@ManyToOne
	@JoinColumn(name = "peopleID") 
	public PeopleInfo getPeopleID() {
		return peopleID;
	}
	public void setPeopleID(PeopleInfo peopleID) {
		this.peopleID = peopleID;
	}
	public String getTakeTime() {
		return takeTime;
	}
	public void setTakeTime(String takeTime) {
		this.takeTime = takeTime;
	}
}

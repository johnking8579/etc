package com.i8app.install.domain;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
/**
 * 通讯录备份
 * @author lixiangyang
 *
 */
@Entity
public class ContactBakData {
	private int id;
	private String peopleId;
	private String username;
	private File vcardFile;
	private String vcfVersion,vcfTime,memo;
	private int count;
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public File getVcardFile() {
		return vcardFile;
	}
	public void setVcardFile(File vcardFile) {
		this.vcardFile = vcardFile;
	}
	public String getVcfVersion() {
		return vcfVersion;
	}
	public void setVcfVersion(String vcfVersion) {
		this.vcfVersion = vcfVersion;
	}
	public String getVcfTime() {
		return vcfTime;
	}
	public void setVcfTime(String vcfTime) {
		this.vcfTime = vcfTime;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}

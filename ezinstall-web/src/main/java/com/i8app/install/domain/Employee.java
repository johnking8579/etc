package com.i8app.install.domain;

import javax.persistence.*;

/**
 * <<驱动信息表>>的实体类
 *  表格式: id--主键
 *  vid--表示生产厂商, 一张表里面应该只有一个VID.
 *  pid--表示手机型号,所有PID以逗号隔开放在一条记录中
 *  filePath--表示驱动包的网络地址
 *  osType--表示操作系统类型
 * 
 * @author JingYing
 * 
 */
@Entity
@Table(name = "employeeinfo") 
public class Employee {
	private Integer empID;
	private String empName;
	private String empno,empPwd;
	private Integer isDeleted,isFreezed;
	private BusinessHall hall;
	private Countyinfo county;
	private Cityinfo city;
	private Provinceinfo province;
	@Id
	@GeneratedValue
	public Integer getEmpID() {
		return empID;
	}
	public void setEmpID(Integer empID) {
		this.empID = empID;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpno() {
		return empno;
	}
	public void setEmpno(String empno) {
		this.empno = empno;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OrgID") 
	public BusinessHall getHall() {
		return hall;
	}
	public void setHall(BusinessHall hall) {
		this.hall = hall;
	}
	@ManyToOne
	@JoinColumn(name = "countyID") 
	public Countyinfo getCounty() {
		return county;
	}
	public void setCounty(Countyinfo county) {
		this.county = county;
	}
	@ManyToOne
	@JoinColumn(name = "cityID") 
	public Cityinfo getCity() {
		return city;
	}
	public void setCity(Cityinfo city) {
		this.city = city;
	}
	@ManyToOne
	@JoinColumn(name = "provid") 
	public Provinceinfo getProvince() {
		return province;
	}
	public void setProvince(Provinceinfo province) {
		this.province = province;
	}
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Integer getIsFreezed() {
		return isFreezed;
	}
	public void setIsFreezed(Integer isFreezed) {
		this.isFreezed = isFreezed;
	}
	public String getEmpPwd() {
		return empPwd;
	}
	public void setEmpPwd(String empPwd) {
		this.empPwd = empPwd;
	}

}

package com.i8app.install.domain;

import java.util.Set;

import javax.persistence.*;

/**
 * <<视频分类信息表>>的实体类
 * 
 */
@Entity
public class VideoType {
	private Integer videoTypeId, num;
	private String typeName;
	private VideoType parent;
	private Set<VideoInfo> videoInfoSet;

	@Id
	@GeneratedValue
	public Integer getVideoTypeId() {
		return videoTypeId;
	}

	public void setVideoTypeId(Integer videoTypeId) {
		this.videoTypeId = videoTypeId;
	}

	@Column(length=128)
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="parentVideoTypeID")
	public VideoType getParent() {
		return parent;
	}

	public void setParent(VideoType parent) {
		this.parent = parent;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@OneToMany(mappedBy="videoType")
	@JoinColumn(name="videoTypeID")
	public Set<VideoInfo> getVideoInfoSet() {
		return videoInfoSet;
	}

	public void setVideoInfoSet(Set<VideoInfo> videoInfoSet) {
		this.videoInfoSet = videoInfoSet;
	}

}

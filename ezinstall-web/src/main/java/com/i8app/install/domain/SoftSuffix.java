package com.i8app.install.domain;

import javax.persistence.*;

@Entity
public class SoftSuffix {
	private Integer id;
	private String suffixName;
	
	@Id
	@GeneratedValue
	@Column(name="suffixId")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSuffixName() {
		return suffixName;
	}
	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}
	
	

}

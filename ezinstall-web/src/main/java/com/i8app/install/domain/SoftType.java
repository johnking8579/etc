package com.i8app.install.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.JsonObject;
import com.i8app.install.JsonSerializable;

@Entity
public class SoftType implements JsonSerializable	{
	private Integer id;
	private String typeName;
	private Integer num;

	@Id
	@GeneratedValue
	@Column(name="softType")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	public JsonObject toJsonObject() {
		JsonObject json = new JsonObject();
		json.addProperty("id", id);
		json.addProperty("name", typeName);
		return json;
	}
}

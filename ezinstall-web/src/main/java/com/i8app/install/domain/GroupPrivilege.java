package com.i8app.install.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class GroupPrivilege {
	
	private Integer id;
	
	private UserGroup groupId; 

	private PrivilegeInfo privilegeId;
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name = "groupId") 
	public UserGroup getGroupId() {
		return groupId;
	}

	public void setGroupId(UserGroup groupId) {
		this.groupId = groupId;
	}
	@ManyToOne(optional=false)
	@JoinColumn(name = "privilegeId", nullable=false)
	public PrivilegeInfo getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(PrivilegeInfo privilegeId) {
		this.privilegeId = privilegeId;
	}
	
}
package com.i8app.install.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class App {
	private String uuid, name, pinyin, developer, info, infoHtml,
			markerType, cpId, originalAppId, originalTypeId;
	private boolean isAreaCustom, isDel;
	private int downCount, installCount;
	private String rscTypeId,point;
	private String icon;
	private Date updateTime;

	@Id
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public boolean getIsAreaCustom() {
		return isAreaCustom;
	}

	public void setIsAreaCustom(boolean isAreaCustom) {
		this.isAreaCustom = isAreaCustom;
	}

	public boolean getIsDel() {
		return isDel;
	}

	public void setIsDel(boolean isDel) {
		this.isDel = isDel;
	}

	public int getDownCount() {
		return downCount;
	}

	public void setDownCount(int downCount) {
		this.downCount = downCount;
	}

	public int getInstallCount() {
		return installCount;
	}

	public void setInstallCount(int installCount) {
		this.installCount = installCount;
	}

	public String getInfoHtml() {
		return infoHtml;
	}

	public void setInfoHtml(String infoHtml) {
		this.infoHtml = infoHtml;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOriginalAppId() {
		return originalAppId;
	}

	public void setOriginalAppId(String originalAppId) {
		this.originalAppId = originalAppId;
	}

	public String getOriginalTypeId() {
		return originalTypeId;
	}

	public void setOriginalTypeId(String originalTypeId) {
		this.originalTypeId = originalTypeId;
	}
	
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	@Override
	public String toString() {
		return "App [uuid=" + uuid + ", name=" + name + ", pinyin=" + pinyin
				+ ", developer=" + developer + ", info=" + info + ", infoHtml="
				+ infoHtml + ", markerType=" + markerType + ", cpId=" + cpId
				+ ", originalAppId=" + originalAppId + ", originalTypeId="
				+ originalTypeId + ", isAreaCustom=" + isAreaCustom
				+ ", isDel=" + isDel + ", downCount=" + downCount
				+ ", installCount=" + installCount + ", rscTypeId=" + rscTypeId
				+ ", icon=" + icon + ", updateTime=" + updateTime + "]";
	}
}

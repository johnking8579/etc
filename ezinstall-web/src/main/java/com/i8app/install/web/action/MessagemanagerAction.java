package com.i8app.install.web.action;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.Os;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.install.Config;
import com.i8app.install.Pager;
import com.i8app.install.domain.EnterpriseInfo;
import com.i8app.install.domain.Messagelog;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.Tag;
import com.i8app.install.jpush.ErrorCodeEnum;
import com.i8app.install.jpush.MessageResult;
import com.i8app.install.service.MobileClickService;
import com.i8app.install.service.PeopleService;

@SuppressWarnings("serial")
@Controller("messagemanagerAction")
@Scope("prototype")
public class MessagemanagerAction extends BaseAction{
	private static Logger logger = Logger.getLogger(MessagemanagerAction.class);
	@Resource
	public MobileClickService mobileClickService;
	@Resource
	public PeopleService peopleService;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	public Config config;
	private int eid,sid, aid, mid, isdelete,tid;
	private String title,content,pid,imgurlFileFileName, startTime, endTime,uuid,url;
	private Pager<EnterpriseInfo> dataList;
	private Pager<Tag> data;
	private Pager<Messagelog> datalog;
	private EnterpriseInfo en = new EnterpriseInfo();
	private Tag tag = new Tag();
	private Messagelog sms;
	private File imgurlFile;
	private String[] enIds;
	private List<EnterpriseInfo> enterList;
	private List<Tag> tagList;
	private AppDTO appDTO;
	public String servicemanager(){
		dataList = mobileClickService.pagerAlllist(en.getEnterpriseName(), this.getOffset(), 20);
		return SUCCESS;
	}
	public String addmanager(){
		return SUCCESS;
	}
	public String input(){
		int error = 0;
		try{
			String imgPath ="";
			if(imgurlFile !=null && imgurlFileFileName !=null){
				imgPath = mobileClickService.writeFile(imgurlFileFileName, imgurlFile);
			}
			en.setImg(imgPath);
			mobileClickService.addOrUpdatePrise(en);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String editmanager(){
		en = mobileClickService.findEnterpriseInfoById(en.getId());
		return SUCCESS;
	}
	public String eiit(){
		int error = 0;
		try{
			String imgPath ="";
			if(imgurlFile !=null && imgurlFileFileName !=null){
				imgPath = mobileClickService.writeFile(imgurlFileFileName, imgurlFile);
			}
			en.setImg(imgPath);
			mobileClickService.addOrUpdatePrise(en);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	
	public String deletemanager(){
		int error = 0;
		try{
			en = mobileClickService.findEnterpriseInfoById(en.getId());
			en.setStatus(2);
			mobileClickService.addOrUpdatePrise(en);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String bulkDel(){
		int error = 0;
		try{
			for(String enId:enIds){
				en = mobileClickService.findEnterpriseInfoById(Integer.parseInt(enId));
				en.setStatus(2);
				mobileClickService.addOrUpdatePrise(en);
			}
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String tagmanager(){
		data = mobileClickService.pagerTaglist(tag.getTagname(),this.offset, 20);
		return SUCCESS;
	}
	public String addtag(){
		tagList = mobileClickService.findTagList(0);
		return SUCCESS;
	}
	public String inputtag(){
		int error = 0;
		try{
			mobileClickService.addOrUpdateTag(tag);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String edittag(){
		tagList = mobileClickService.findTagList(0);
		tag = mobileClickService.findTagById(tag.getId());
		return SUCCESS;
	}
	public String updatetag(){
		int error = 0;
		try{
			mobileClickService.addOrUpdateTag(tag);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String deletetag(){
		int error = 0;
		try{
			mobileClickService.delTag(tag.getId());
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	public String bulkDeltag(){
		int error = 0;
		try{
			mobileClickService.buldelTag(enIds);
		}catch(Exception e){
			error=1;
		}
		this.setMessage("{\"error\":\""+error+"\"}");
		return AJAX;
	}
	
	public String querylog(){
		if(startTime !=null && endTime !=null){
			datalog = mobileClickService.pagerMessageloglist(sid, aid, mid, isdelete, startTime, endTime, this.offset, 20);
		}
		return SUCCESS;
	}
	public String systempush(){
		return SUCCESS;
	}
	
	public String dailyads(){
		enterList = mobileClickService.findEnterpriseList();
		tagList = mobileClickService.findTagList(0);
		return SUCCESS;
	}
	//系统推送
	public String pushallsms(){
		getResponse().setCharacterEncoding("UTF-8");
		int error = 0;
		String desc = "成功";
		try{
			String imgPath ="";
			if(imgurlFile !=null && imgurlFileFileName !=null){
				imgPath = mobileClickService.writeFile(imgurlFileFileName, imgurlFile);
			}
			sms.setFile_url(imgPath);
			MessageResult result = mobileClickService.sendSysTemMess(title,sms);
			if (null != result) {
				if (result.getErrcode() == ErrorCodeEnum.NOERROR.value()) {
					error=0;
					desc = "成功";
					logger.info("发送成功， sendNo=" + result.getSendno());
				} else {
					error=1;
					desc = errorcodetomsg(result.getErrcode());
					logger.error("发送失败， 错误代码=" + result.getErrcode() + ", 错误消息=" + result.getErrmsg());
				}
			} else {
				error=1;
				desc ="无法获取推送返回数据";
				logger.error("无法获取推送返回数据");
			}
		}catch(Exception e){
			error=1;
			desc ="网络错误";
			logger.error(e.getMessage());
		}
		this.setMessage("{\"error\":\""+error+"\",\"desc\":\""+desc+"\"}");
//		this.setMessage(desc);
		return AJAX;
	}
	public String pushsms(){
		getResponse().setCharacterEncoding("UTF-8");
		int error = 0;
		String desc = "成功";
		try{
			String imgPath ="";
			if(imgurlFile !=null && imgurlFileFileName !=null){
				imgPath = mobileClickService.writeFile(imgurlFileFileName, imgurlFile);
			}
			sms.setFile_url(imgPath);
			if(pid !=null && !"".equals(pid)){
				PeopleInfo p = peopleService.findByPhoneNumber(pid);
				if(p !=null){
					pid=p.getId();
					sms.setAcceptedid(pid);
					sms.setRecipients(p.getUserNumber());
				}else{
					pid=null;
				}
			}else{
				sms.setAcceptedid(sms.getSenderid());
			}
			MessageResult result = mobileClickService.sendAllMessage(pid,title,sms);
			if (null != result) {
				if (result.getErrcode() == ErrorCodeEnum.NOERROR.value()) {
					error=0;
					desc = "成功";
					logger.info("发送成功， sendNo=" + result.getSendno());
				} else {
					error=1;
					desc = errorcodetomsg(result.getErrcode());
					logger.error("发送失败， 错误代码=" + result.getErrcode() + ", 错误消息=" + result.getErrmsg());
				}
			} else {
				error=1;
				desc ="无法获取推送返回数据";
				logger.error("无法获取推送返回数据");
			}
		}catch(Exception e){
			error=1;
			desc ="网络错误";
			logger.error(e.getMessage());
		}
		this.setMessage("{\"error\":\""+error+"\",\"desc\":\""+desc+"\"}");
//		this.setMessage(desc);
		return AJAX;
	}
	public String phoneisexits(){
		int error = 0;
		String desc = "不存在";
		PeopleInfo p = peopleService.findByPhoneNumber(pid);
		if(p != null &&!"".equals(p)){
			error=1;
			desc="存在";
		}
		this.setMessage("{\"error\":\""+error+"\",\"desc\":\""+desc+"\"}");
		return AJAX;
	}
	public String errorcodetomsg(int errorcode){
		String desc="";
		switch(errorcode){
		case 10:
		desc = "系统内部错误";
		break;
		case 1001:
			desc = "不支持GET请求";
			break;
		case 1002:
			desc="缺少必须参数";
			break;
		case 1003:
			desc="参数值不合法";
			break;
		case 1004:
			desc="验证失败";
			break;
		case 1005:
			desc ="消息体太大";
			break;
		case 1007:
			desc="IMEI不合法";
			break;
		case 1008:
			desc="appkey不合法";
			break;
		case 1010:
			desc="msg_content不合法";
			break;
		case 1011:
			desc="没有满足条件的推送目标";
			break;
		default:
			desc="网络错误";
			break;
	}
		return desc;
	}
	public String detail(){
		if(uuid !=null){
//			appDTO = appQuerier.findSinglePack(uuid);
			appDTO = appQuerier.findSingle(uuid,Os.ANDROID);
			if(appDTO !=null){
				try {
					String packuuid = appQuerier.confirmPack(uuid,Os.ANDROID,null);
					url = appQuerier.getPackUrl(packuuid);
				} catch (CpServerAccessException e) {
					logger.error("uuid不正确");
				}
			}else{
				return "noapp";
			}
		}
		return SUCCESS;
	}
	public Pager<EnterpriseInfo> getDataList() {
		return dataList;
	}
	public void setDataList(Pager<EnterpriseInfo> dataList) {
		this.dataList = dataList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public EnterpriseInfo getEn() {
		return en;
	}
	public void setEn(EnterpriseInfo en) {
		this.en = en;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public Pager<Tag> getData() {
		return data;
	}
	public void setData(Pager<Tag> data) {
		this.data = data;
	}
	public Tag getTag() {
		return tag;
	}
	public void setTag(Tag tag) {
		this.tag = tag;
	}
	public String getImgurlFileFileName() {
		return imgurlFileFileName;
	}
	public void setImgurlFileFileName(String imgurlFileFileName) {
		this.imgurlFileFileName = imgurlFileFileName;
	}
	public File getImgurlFile() {
		return imgurlFile;
	}
	public void setImgurlFile(File imgurlFile) {
		this.imgurlFile = imgurlFile;
	}
	public String[] getEnIds() {
		return enIds;
	}
	public void setEnIds(String[] enIds) {
		this.enIds = enIds;
	}
	public Pager<Messagelog> getDatalog() {
		return datalog;
	}
	public void setDatalog(Pager<Messagelog> datalog) {
		this.datalog = datalog;
	}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getIsdelete() {
		return isdelete;
	}
	public void setIsdelete(int isdelete) {
		this.isdelete = isdelete;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Messagelog getSms() {
		return sms;
	}
	public void setSms(Messagelog sms) {
		this.sms = sms;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public List<EnterpriseInfo> getEnterList() {
		return enterList;
	}
	public void setEnterList(List<EnterpriseInfo> enterList) {
		this.enterList = enterList;
	}
	public List<Tag> getTagList() {
		return tagList;
	}
	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}
	public AppDTO getAppDTO() {
		return appDTO;
	}
	public void setAppDTO(AppDTO appDTO) {
		this.appDTO = appDTO;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}

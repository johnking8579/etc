package com.i8app.install.web.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sun.misc.BASE64Decoder;

import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.install.Config;
import com.i8app.install.MD5;
import com.i8app.install.Util;
import com.i8app.install.domain.App;
import com.i8app.install.domain.AppFlowLog;
import com.i8app.install.domain.AppInstallPack;
import com.i8app.install.domain.App_Comment;
import com.i8app.install.domain.App_backup;
import com.i8app.install.domain.ClientActivityLog;
import com.i8app.install.domain.ClientConfigInfo;
import com.i8app.install.domain.ContactBakData;
import com.i8app.install.domain.FeedbackInfo;
import com.i8app.install.domain.Guessyoulike;
import com.i8app.install.domain.Image_backup;
import com.i8app.install.domain.Mobile_installed_list;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.Signalqualitydata;
import com.i8app.install.domain.Signalrequest;
import com.i8app.install.domain.Sms_backup;
import com.i8app.install.domain.Smsspam;
import com.i8app.install.domain.SoftwareInstallLog;
import com.i8app.install.domain.Threads;
import com.i8app.install.domain.UserFlowLog;
import com.i8app.install.domain.User_uninstall_log;
import com.i8app.install.service.App_backupService;
import com.i8app.install.service.PeopleService;
import com.i8app.install.service.PeopleService.FileType;
import com.i8app.install.service.SmsHessianService;
import com.i8app.install.service.SoftwareInstallLogService;
import com.i8app.install.wsclient.AppAssemblier;


@SuppressWarnings("serial")
@Component("peopleAction")
@Scope("prototype")
public class PeopleAction extends BaseAction {
	private static Logger logger = Logger.getLogger(PeopleAction.class);
	@Resource
	private PeopleService peopleService;
	@Resource
	private SmsHessianService smsHessianService;
	@Resource
	private SoftwareInstallLogService softwareInstallLogService;
	@Resource
	private AppQuerier appQuerier;
	@Resource
	private AppAssemblier appAssemblier;
	@Resource
	private App_backupService app_backupService;
	private PeopleInfo people;
	private ClientConfigInfo client;
	private String jsonStr,contentLength,fileName;
	@Resource
	private Config config;
	//通讯录备份
	private File vcardFile,appFile,picFile,content;
	private String peopleId,username,vcfVersion,vcardFileFileName,memo,id,appFileFileName,picFileFileName,time,contentFileName;
	private int count,versioncode;
	
	//软件
	private String version,uid,name,pname,number,icon;
	
	private Float size;
	
	
	/**
	 * 用户注册
	 * json传入以下参数
	 * @param userNumber 用户账号、手机号码（一致）
	 * @param userPassword 登录密码
	 * @param userNickname 昵称
	 * @param phoneIMEI IMEI号
	 * @param phoneIMSI IMSI号
	 * @param manuName 手机厂商
	 * @param modelName 手机型号
	 * @param phoneOS 操作系统
	 * @param phoneOSversion 操作系统版本
	 * 
	 * @return  JSONObject
	 */
	public String register() {
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					//查询该账号是否已经存在
					if(jsnb.getString("userNumber") != null){
						people = peopleService.findByPhoneNumber(jsnb.getString("userNumber"));
						if(people != null){
//							//保存账号
//							jsnb.getString("userNumber");
//							PeopleInfo p = new PeopleInfo();
//							p.setUserNumber(jsnb.getString("userNumber"));
//							p.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
//							p.setUserNickname(jsnb.getString("userNickname"));
//							p.setPhoneNumber(jsnb.getString("userNumber"));//传入的是手机号码
//							p.setPhoneIMEI(jsnb.getString("phoneIMEI"));
//							p.setPhoneIMSI(jsnb.getString("phoneIMSI"));
//							p.setManuName(jsnb.getString("manuName"));
//							p.setModelName(jsnb.getString("modelName"));
//							p.setPhoneOS(jsnb.getString("phoneOS"));
//							p.setPhoneOSversion(jsnb.getString("phoneOSversion"));
//							peopleService.saveOrUpdatePeople(p);
							if(people.getIsRegistered() ==0){
								people.setUserNumber(jsnb.getString("userNumber"));
								if(jsnb.getString("userPassword") !=null && !"".equals(jsnb.getString("userPassword"))){
								people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
								}
								people.setUserNickname(jsnb.getString("userNickname"));
								people.setPhoneNumber(jsnb.getString("userNumber"));
								people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
								people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
								people.setManuName(jsnb.getString("manuName"));
								people.setModelName(jsnb.getString("modelName"));
								people.setPhoneOS(jsnb.getString("phoneOS"));
								people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
								people.setIsRegistered(1);
//								people.setRegDate(jsnb.getString("regDate"));
								people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
								peopleService.saveOrUpdatePeople(people);
								
								
								
								ClientActivityLog caLog = new ClientActivityLog();
								caLog.setPeopleId(jsnb.getString("peopleID"));
								caLog.setUserNumber(jsnb.getString("userNumber"));
								caLog.setPhoneNumber(jsnb.getString("userNumber"));
								caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
								caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
								caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
								caLog.setActivityType(1+"");
								peopleService.saveClientActivityLog(caLog);
								
								
								
								peopleJson.put("peopleID", people.getId());
								errorCode = 0;//注册注册成功
								desc = "该手机号注册成功";
							}else{
								peopleJson.put("peopleID", people.getId());
								errorCode = 1;//已注册
								desc = "该手机号已注册";
							}
						}else{
							if(jsnb.getString("phoneIMSI") !=null){
								people = peopleService.findByPhoneIMSI(jsnb.getString("phoneIMSI"));
								if(people !=null){
									if(people.getIsRegistered()==0){
										people.setUserNumber(jsnb.getString("userNumber"));
										if(jsnb.getString("userPassword") !=null  && !"".equals(jsnb.getString("userPassword"))){
											people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
										}
										people.setUserNickname(jsnb.getString("userNickname"));
										people.setPhoneNumber(jsnb.getString("userNumber"));
										people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
										people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
										people.setManuName(jsnb.getString("manuName"));
										people.setModelName(jsnb.getString("modelName"));
										people.setPhoneOS(jsnb.getString("phoneOS"));
										people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
										people.setIsRegistered(1);
										people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
										peopleService.saveOrUpdatePeople(people);
										
										ClientActivityLog caLog = new ClientActivityLog();
										caLog.setPeopleId(jsnb.getString("peopleID"));
										caLog.setUserNumber(jsnb.getString("userNumber"));
										caLog.setPhoneNumber(jsnb.getString("userNumber"));
										caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
										caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
										caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
										caLog.setActivityType(1+"");
										peopleService.saveClientActivityLog(caLog);
										
										peopleJson.put("peopleID", people.getId());
										errorCode = 0;//成功
										desc = "该IMSI注册成功";
									}else{
										peopleJson.put("peopleID", people.getId());
										errorCode = 1;//账号已经存在
										desc = "该IMSI已注册";
									}
								}else{
									if(jsnb.getString("phoneIMEI") != null){
										people = peopleService.findByPhoneIMEI(jsnb.getString("phoneIMEI"));
										if(people !=null){
											if(people.getIsRegistered()==0){
												people.setUserNumber(jsnb.getString("userNumber"));
												if(jsnb.getString("userPassword") !=null  && !"".equals(jsnb.getString("userPassword"))){
													people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
												}
												people.setUserNickname(jsnb.getString("userNickname"));
												people.setPhoneNumber(jsnb.getString("userNumber"));
												people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
												people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
												people.setManuName(jsnb.getString("manuName"));
												people.setModelName(jsnb.getString("modelName"));
												people.setPhoneOS(jsnb.getString("phoneOS"));
												people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
												people.setIsRegistered(1);
												people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
												peopleService.saveOrUpdatePeople(people);
												
												ClientActivityLog caLog = new ClientActivityLog();
												caLog.setPeopleId(jsnb.getString("peopleID"));
												caLog.setUserNumber(jsnb.getString("userNumber"));
												caLog.setPhoneNumber(jsnb.getString("userNumber"));
												caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
												caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
												caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
												caLog.setActivityType(1+"");
												peopleService.saveClientActivityLog(caLog);
												
												peopleJson.put("peopleID", people.getId());
												errorCode = 0;//成功
												desc = "该IMEI注册成功";
											}else{
												peopleJson.put("peopleID", people.getId());
												errorCode = 1;//账号已经存在
												desc = "该IMEI已注册";
											}
										}else{
											if(jsnb.getString("peopleID") != null){
												people = peopleService.findByPeopleID(jsnb.getString("peopleID"));
												if(people !=null){
													if(people.getIsRegistered()==0){
														people.setUserNumber(jsnb.getString("userNumber"));
														if(jsnb.getString("userPassword") !=null  && !"".equals(jsnb.getString("userPassword"))){
															people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
														}
														people.setUserNickname(jsnb.getString("userNickname"));
														people.setPhoneNumber(jsnb.getString("userNumber"));
														people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
														people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
														people.setManuName(jsnb.getString("manuName"));
														people.setModelName(jsnb.getString("modelName"));
														people.setPhoneOS(jsnb.getString("phoneOS"));
														people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
														people.setIsRegistered(1);
														people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
														peopleService.saveOrUpdatePeople(people);
														
														ClientActivityLog caLog = new ClientActivityLog();
														caLog.setPeopleId(jsnb.getString("peopleID"));
														caLog.setUserNumber(jsnb.getString("userNumber"));
														caLog.setPhoneNumber(jsnb.getString("userNumber"));
														caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
														caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
														caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
														caLog.setActivityType(1+"");
														peopleService.saveClientActivityLog(caLog);
														
														peopleJson.put("peopleID", people.getId());
														errorCode = 0;//成功
														desc = "该用户注册成功";
													}else{
														peopleJson.put("peopleID", people.getId());
														errorCode = 1;//账号已经存在
														desc = "该用户已注册";
													}
												}else{
													PeopleInfo people = new PeopleInfo();
													String peopleID = peopleService.findMaxPeopleID();
													if(peopleID ==null){
														peopleID = "00001";
													}
//													int d = Integer.valueOf(peopleID);
													int peopleintId = Integer.parseInt(peopleID);
													peopleintId++;
													String str = String.valueOf(peopleintId);
													if(str.length()==1){
														str = "0000" + str;
													}else if(str.length() ==2){
														str = "000" +str;
													}else if (str.length() ==3){
														str = "00" + str;
													}else if(str.length() ==4){
														str = "0" + str;
													}
													String  peopleid= "HY01" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + str;
													people.setId(peopleid);
													people.setUserNumber(jsnb.getString("userNumber"));
													if(jsnb.getString("userPassword") !=null  && !"".equals(jsnb.getString("userPassword"))){
														people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
													}
													people.setUserNickname(jsnb.getString("userNickname"));
													people.setPhoneNumber(jsnb.getString("userNumber"));
													people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
													people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
													people.setManuName(jsnb.getString("manuName"));
													people.setModelName(jsnb.getString("modelName"));
													people.setPhoneOS(jsnb.getString("phoneOS"));
													people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
													people.setIsRegistered(1);
													people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
													people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
													peopleService.saveOrUpdatePeople(people);
													
													ClientActivityLog caLog = new ClientActivityLog();
													caLog.setPeopleId(jsnb.getString("peopleID"));
													caLog.setUserNumber(jsnb.getString("userNumber"));
													caLog.setPhoneNumber(jsnb.getString("userNumber"));
													caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
													caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
													caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
													caLog.setActivityType(1+"");
													peopleService.saveClientActivityLog(caLog);
													
													peopleJson.put("peopleID", peopleid);
													errorCode = 0;//成功
													desc = "该用户添加并注册成功";
												}
											}else{
												PeopleInfo people = new PeopleInfo();
												String peopleID = peopleService.findMaxPeopleID();
												if(peopleID ==null){
													peopleID = "00001";
												}
//												int d = Integer.valueOf(peopleID);
												int peopleintId = Integer.parseInt(peopleID);
												peopleintId++;
												String str = String.valueOf(peopleintId);
												if(str.length()==1){
													str = "0000" + str;
												}else if(str.length() ==2){
													str = "000" +str;
												}else if (str.length() ==3){
													str = "00" + str;
												}else if(str.length() ==4){
													str = "0" + str;
												}
												String  peopleid= "HY01" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + str;
												people.setId(peopleid);
												people.setUserNumber(jsnb.getString("PeopleStr"));
												people.setUserNumber(jsnb.getString("userNumber"));
												if(jsnb.getString("userPassword") !=null  && !"".equals(jsnb.getString("userPassword"))){
													people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
												}
												people.setUserNickname(jsnb.getString("userNickname"));
												people.setPhoneNumber(jsnb.getString("userNumber"));
												people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
												people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
												people.setManuName(jsnb.getString("manuName"));
												people.setModelName(jsnb.getString("modelName"));
												people.setPhoneOS(jsnb.getString("phoneOS"));
												people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
												people.setIsRegistered(1);
												people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
												people.setRegDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
												peopleService.saveOrUpdatePeople(people);
												
												ClientActivityLog caLog = new ClientActivityLog();
												caLog.setPeopleId(jsnb.getString("peopleID"));
												caLog.setUserNumber(jsnb.getString("userNumber"));
												caLog.setPhoneNumber(jsnb.getString("userNumber"));
												caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
												caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
												caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
												caLog.setActivityType(1+"");
												peopleService.saveClientActivityLog(caLog);
												
												peopleJson.put("peopleID", peopleid);
												errorCode = 0;//成功
												desc = "该用户添加并注册成功";
											}
										}
									}
								}
							}
						}
					}else{
						
					}
					
				}else{
					errorCode = 500;
					desc = "上传信息失败";
				}
				
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
				errorCode = 500;
				desc = "不支持编码异常：";
				e.printStackTrace();
			} catch (IOException e) {
				logger.error(e);
				errorCode = 500;
				desc = "IO异常：";
				e.printStackTrace();
			} catch (JSONException e) {
				logger.error(e);
				errorCode = 500;
				desc = "Json异常：";
				e.printStackTrace();
			}
			
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}

	/**
	 * 用户登录
	 * @param userNumber 用户账号、手机号码（一致）
	 * @param userPassword 登录密码
	 * @param phoneIMEI
	 * @param phoneIMSI
	 * @param manuName
	 * @param modelName
	 * @param phoneOS
	 * @param phoneOSversion
	 * 
	 * @return
	 */
	public String login() {
		JSONObject peopleJson = new JSONObject();
		JSONObject peoplejo = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				
				//查询该账号是否已经存在
				people = peopleService.findByUserNumber(jsnb.getString("userNumber"));
				if(people == null){
					errorCode = 1;//无此账号
					desc="无此账号";
				}else{
					if(people.getUserPassword() != null && !"".equals(people.getUserPassword())){
						people = peopleService.findByUserNumberAndPassword(jsnb.getString("userNumber"), new MD5().toMD5(jsnb.getString("userPassword")));
						if(people == null || !people.getUserPassword().equalsIgnoreCase(new MD5().toMD5(jsnb.getString("userPassword")))){
							errorCode = 2;//登录密码错误
							desc="登录密码错误";
						}else{
							boolean update = false;//false 状态代表不修改任何东西；true 状态代表修改了内容
							
							//添加用户活跃度
							ClientActivityLog caLog = new ClientActivityLog();
							caLog.setUserNumber(jsnb.getString("userNumber"));
							caLog.setPhoneNumber(jsnb.getString("userNumber"));
							caLog.setPeopleId(jsnb.getString("peopleID"));
							
							//修改用户的信息
							if(jsnb.getString("phoneIMEI") != null && !"".equals(jsnb.getString("phoneIMEI")) 
									&& !people.getPhoneIMEI().equalsIgnoreCase(jsnb.getString("phoneIMEI"))){
								caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
								people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
								update = true;
							}
							if(jsnb.getString("phoneIMSI") != null && !"".equals(jsnb.getString("phoneIMSI"))
									&& !people.getPhoneIMSI().equalsIgnoreCase(jsnb.getString("phoneIMSI"))){
								caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
								people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
								update = true;
							}
							if(jsnb.getString("manuName") != null && !"".equals(jsnb.getString("manuName"))
									&& !people.getManuName().equalsIgnoreCase(jsnb.getString("manuName"))){
								
								people.setManuName(jsnb.getString("manuName"));
								update = true;
							}
							if(jsnb.getString("modelName") != null && !"".equals(jsnb.getString("modelName"))
									&& !people.getModelName().equalsIgnoreCase(jsnb.getString("modelName"))){
								
								people.setModelName(jsnb.getString("modelName"));
								update = true;
							}
							if(jsnb.getString("phoneOS") != null && !"".equals(jsnb.getString("phoneOS"))
									&& !people.getPhoneOS().equalsIgnoreCase(jsnb.getString("phoneOS"))){
								
								people.setPhoneOS(jsnb.getString("phoneOS"));
								update = true;
							}
							if(jsnb.getString("phoneOSversion") != null && !"".equals(jsnb.getString("phoneOSversion"))
									&& !people.getPhoneOSversion().equalsIgnoreCase(jsnb.getString("phoneOSversion"))){
								
								people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
								update = true;
							}
							if(update){
								peopleService.saveOrUpdatePeople(people);
							}
						//保存活跃度
							caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
							caLog.setActivityType(1+"");
							peopleService.saveClientActivityLog(caLog);
							
							
							peoplejo.put("id", people.getId());
							peoplejo.put("userNumber", people.getUserNumber());
							peoplejo.put("userNickname", people.getUserNickname());
							peoplejo.put("userAge", people.getUserAge());
							peoplejo.put("userGender", people.getUserGender());
							peoplejo.put("phoneNumber", people.getPhoneNumber());
							peoplejo.put("phoneIMEI", people.getPhoneIMEI());
							peoplejo.put("phoneIMSI", people.getPhoneIMSI());
							peoplejo.put("manuName", people.getManuName());
							peoplejo.put("modelName", people.getModelName());
							peoplejo.put("phoneOS", people.getPhoneOS());
							peoplejo.put("phoneOSversion", people.getPhoneOSversion());
							peoplejo.put("img", HTTP_PATH + people.getImg());
							peoplejo.put("provId", people.getPrivinceId());
							errorCode = 0;//成功
							desc = "成功";
						}
					}else{
						boolean update = false;//false 状态代表不修改任何东西；true 状态代表修改了内容
						
						//添加用户活跃度
						ClientActivityLog caLog = new ClientActivityLog();
						caLog.setUserNumber(jsnb.getString("userNumber"));
						caLog.setPhoneNumber(jsnb.getString("userNumber"));
						caLog.setPeopleId(jsnb.getString("peopleID"));
						
						//修改用户的信息
						if(jsnb.getString("phoneIMEI") != null && !"".equals(jsnb.getString("phoneIMEI")) 
								&& !people.getPhoneIMEI().equalsIgnoreCase(jsnb.getString("phoneIMEI"))){
							caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
							people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
							update = true;
						}
						if(jsnb.getString("phoneIMSI") != null && !"".equals(jsnb.getString("phoneIMSI"))
								&& !people.getPhoneIMSI().equalsIgnoreCase(jsnb.getString("phoneIMSI"))){
							caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
							people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
							update = true;
						}
						if(jsnb.getString("manuName") != null && !"".equals(jsnb.getString("manuName"))
								&& !people.getManuName().equalsIgnoreCase(jsnb.getString("manuName"))){
							
							people.setManuName(jsnb.getString("manuName"));
							update = true;
						}
						if(jsnb.getString("modelName") != null && !"".equals(jsnb.getString("modelName"))
								&& !people.getModelName().equalsIgnoreCase(jsnb.getString("modelName"))){
							
							people.setModelName(jsnb.getString("modelName"));
							update = true;
						}
						if(jsnb.getString("phoneOS") != null && !"".equals(jsnb.getString("phoneOS"))
								&& !people.getPhoneOS().equalsIgnoreCase(jsnb.getString("phoneOS"))){
							
							people.setPhoneOS(jsnb.getString("phoneOS"));
							update = true;
						}
						if(jsnb.getString("phoneOSversion") != null && !"".equals(jsnb.getString("phoneOSversion"))
								&& !people.getPhoneOSversion().equalsIgnoreCase(jsnb.getString("phoneOSversion"))){
							
							people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
							update = true;
						}
						if(update){
							peopleService.saveOrUpdatePeople(people);
						}
					//保存活跃度
						caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
						caLog.setActivityType(1+"");
						peopleService.saveClientActivityLog(caLog);
						
						
						peoplejo.put("id", people.getId());
						peoplejo.put("userNumber", people.getUserNumber());
						peoplejo.put("userNickname", people.getUserNickname());
						peoplejo.put("userAge", people.getUserAge());
						peoplejo.put("userGender", people.getUserGender());
						peoplejo.put("phoneNumber", people.getPhoneNumber());
						peoplejo.put("phoneIMEI", people.getPhoneIMEI());
						peoplejo.put("phoneIMSI", people.getPhoneIMSI());
						peoplejo.put("manuName", people.getManuName());
						peoplejo.put("modelName", people.getModelName());
						peoplejo.put("phoneOS", people.getPhoneOS());
						peoplejo.put("phoneOSversion", people.getPhoneOSversion());
						peoplejo.put("img", HTTP_PATH + people.getImg());
						errorCode = 0;//成功
						desc = "成功";
					}
				}
				
			} catch (UnsupportedEncodingException e) {
				logger.error(e);
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				logger.error(e);
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				logger.error(e);
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			}
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			peopleJson.put("result", peoplejo);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}

	/**
	 * 找回密码--发送验证码
	 * @param phoneNumber 接收验证码的手机号
	 * @param userNumber 用户账号、手机号码（一致）
	 * @param userPassword 登录密码
	 * @return 
	 */
	public String phone_sendAuthcode() {
		JSONObject peopleJson = new JSONObject();
		Integer errorCode = null;
		String desc = "";
		String authcode = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				
				//是否为找回密码发送验证码,0：找回密码；1：用户验证
				String isFindPass = jsnb.getString("isFindPass");
				//查询该账号是否已经存在
				String phoneNumber = jsnb.getString("phoneNumber");
				people = peopleService.findByUserNumber(jsnb.getString("userNumber"));
				
				if("1".equals(isFindPass)){
					if(people != null){
						errorCode = 1;
						desc = "账号已存在！";
					}else{
						authcode = Util.randStr(6);
						smsHessianService.sendTest(phoneNumber, authcode);
						errorCode = 0;
						desc = "成功";
					}
					
				}else{
					if(people == null){
						errorCode = 1;
						desc = "无此账号";
					}else{
						authcode = Util.randStr(6);
						smsHessianService.sendTest(phoneNumber, authcode);
						errorCode = 0;
						desc = "成功";
					}
				}
			} catch (UnsupportedEncodingException e) {
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			} catch (Exception e) {
				errorCode = 500;
				desc = e.getMessage();
				e.printStackTrace();
			}
			
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			peopleJson.put("authcode", authcode);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());

	}
	
	/**
	 * 找回密码,直接将密码做修改
	 * @param userNumber 用户账号、手机号码（一致）
	 * @param userPassword 新登录密码
	 * @return
	 */
	public String getBackPassword() {
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				
				//查询该账号是否已经存在
				people = peopleService.findByUserNumber(jsnb.getString("userNumber"));
				if(people == null){
					errorCode = 1;//账号不存在
				}else{
					people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
					peopleService.saveOrUpdatePeople(people);
					
					errorCode = 0;//成功
				}
			} catch (UnsupportedEncodingException e) {
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			}
			
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 修改密码
	 * @param userNumber 用户账号、手机号码（一致）
	 * @param oldPassword 原登录密码
	 * @param userPassword 新登录密码
	 * @return
	 */
	public String updatePassword() {
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				
				//查询该账号是否已经存在
				people = peopleService.findByUserNumber(jsnb.getString("userNumber"));
				if(people == null){
					errorCode = 1;//账号不存在
					desc ="账号不存在";
				}else{
					if(people.getUserPassword() != null && !"".equals(people.getUserPassword())){
						people = peopleService.findByUserNumberAndPassword(jsnb.getString("userNumber"), new MD5().toMD5(jsnb.getString("oldPassword")));
						if(people == null){
							errorCode = 2;//登录密码错误
							desc = "登陆密码错误";
						}else{
							if(jsnb.getString("userPassword") !=null && !"".equals(jsnb.getString("userPassword"))){
								people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
							}else{
								people.setUserPassword("");
							}
							peopleService.saveOrUpdatePeople(people);
							errorCode = 0;//成功
							desc = "修改成功";
						}
					}else{
						people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
						peopleService.saveOrUpdatePeople(people);
						errorCode = 0;//成功
						desc = "设置成功";
					}
				}
				
			} catch (UnsupportedEncodingException e) {
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			}
			
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 修改用户基本信息
	 * @param userNumber
	 * @param userPassword
	 * @param userNickname
	 * @param phoneIMEI
	 * @param phoneIMSI
	 * @param manuName
	 * @param modelName
	 * @param phoneOS
	 * @param phoneOSversion
	 * @param imgType
	 * @param imgData
	 * 
	 * @return
	 */
	public String updatePeople() {
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			String picturePath = "";
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				
				//查询该账号是否已经存在
				people = peopleService.findByUserNumber(jsnb.getString("userNumber"));
				if(people == null){
					errorCode = 1;//账号不存在
					desc = "账号不存在";
				}else{
					if(people.getUserPassword() != null && !"".equals(people.getUserPassword())){
						people = peopleService.findByUserNumberAndPassword(jsnb.getString("userNumber"), new MD5().toMD5(jsnb.getString("userPassword")));
						if(people == null || !people.getUserPassword().equalsIgnoreCase(new MD5().toMD5(jsnb.getString("userPassword")))){
							errorCode = 2;//登录密码错误
							desc = "密码错误";
						}else{
							//先将img上传于服务器，存于 people/img/ 目录下
							String ImgType = jsnb.getString("imgType");
							String imgData = jsnb.getString("imgData");
							picturePath = this.upLoadFile(ImgType, imgData);
							
							people.setImg(picturePath);
							people.setUserNumber(jsnb.getString("userNumber"));
							people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
							people.setUserNickname(jsnb.getString("userNickname"));
							people.setUserGender(jsnb.getInt("userGender"));
							people.setPhoneNumber(jsnb.getString("userNumber"));//传入的是手机号码
							people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
							people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
							people.setManuName(jsnb.getString("manuName"));
							people.setModelName(jsnb.getString("modelName"));
							people.setPhoneOS(jsnb.getString("phoneOS"));
							people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
							
							peopleService.saveOrUpdatePeople(people);
							errorCode = 0;//成功
							picturePath = HTTP_PATH+  picturePath;
							desc = "修改成功";
						}
					}else{
						//先将img上传于服务器，存于 people/img/ 目录下
						String ImgType = jsnb.getString("imgType");
						String imgData = jsnb.getString("imgData");
						picturePath = this.upLoadFile(ImgType, imgData);
						
						people.setImg(picturePath);
						people.setUserNumber(jsnb.getString("userNumber"));
						people.setUserPassword(new MD5().toMD5(jsnb.getString("userPassword")));
						people.setUserNickname(jsnb.getString("userNickname"));
						people.setUserGender(jsnb.getInt("userGender"));
						people.setPhoneNumber(jsnb.getString("userNumber"));//传入的是手机号码
						people.setPhoneIMEI(jsnb.getString("phoneIMEI"));
						people.setPhoneIMSI(jsnb.getString("phoneIMSI"));
						people.setManuName(jsnb.getString("manuName"));
						people.setModelName(jsnb.getString("modelName"));
						people.setPhoneOS(jsnb.getString("phoneOS"));
						people.setPhoneOSversion(jsnb.getString("phoneOSversion"));
						
						peopleService.saveOrUpdatePeople(people);
						errorCode = 0;//成功
						picturePath = HTTP_PATH+  picturePath;
						desc = "修改成功";
					}
				}
				
			} catch (UnsupportedEncodingException e) {
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			}catch (Exception e){
				logger.error(e);
				errorCode = 500;
				desc = "系统异常：" + e.getMessage();
				e.printStackTrace();
			}
			
			peopleJson.put("result", picturePath);
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 根据用户传入的文件流、文件的后缀名 对字节数组字符串进行Base64解码并生成图片
	 * @param ImgType  文件的后缀名（如 jpg\png）
	 * @param imgData 文件流
	 * @return picturePath 保存于数据库的那段路径
	 * @throws IOException 
	 * @throws Exception
	 */
	@SuppressWarnings("restriction")
	public String upLoadFile( String ImgType, String imgData) throws IOException {
		if (imgData == null) // 图像数据为空
			return null;
		BASE64Decoder decoder = new BASE64Decoder();
		// Base64解码
		byte[] bytes = decoder.decodeBuffer(imgData);
		for (int i = 0; i < bytes.length; ++i) {
			if (bytes[i] < 0) {// 调整异常数据
				bytes[i] += 256;
			}
		}
		// 生成jpeg图片
		String pictureDir = "people/img/";
		File f = new File(DISK_PATH  + pictureDir);
        if (!f.exists())
        {
        	f.mkdirs();
        }
        String picturePath = pictureDir + new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())+"."+ImgType;
		OutputStream out = new FileOutputStream(new File(DISK_PATH  + picturePath));
		out.write(bytes);
		out.flush();
		out.close();
		return picturePath.replace("\\", "/");
	}
	
	/**
	 * 客户端的活跃记录--看看要不要换一个action
	 * @return
	 */
	public String saveClientActivityLog() {
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br;
			try {
				br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				JSONObject jsnb = new JSONObject(s);
				ClientActivityLog caLog = new ClientActivityLog();
				caLog.setPeopleId(jsnb.getString("peopleID"));
				caLog.setUserNumber(jsnb.getString("userNumber"));
				caLog.setPhoneNumber(jsnb.getString("phoneNumber"));
				caLog.setPhoneIMEI(jsnb.getString("phoneIMEI"));
				caLog.setPhoneIMSI(jsnb.getString("phoneIMSI"));
				caLog.setActivityDateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(new Date()));
				caLog.setActivityType(jsnb.getString("activityType"));
				int id = peopleService.saveClientActivityLog(caLog);
				errorCode = 0;//成功
				desc = "修改成功";
			} catch (UnsupportedEncodingException e) {
				errorCode = 500;
				desc = "不支持编码异常：" + e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorCode = 500;
				desc = "IO异常：" + e.getMessage();
				e.printStackTrace();
			} catch (JSONException e) {
				errorCode = 500;
				desc = "Json异常：" + e.getMessage();
				e.printStackTrace();
			}
			
			peopleJson.put("errorCode", errorCode);
			peopleJson.put("desc", desc);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return this.doPrint(peopleJson.toString());
	}

	/*
	 * {"phoneNo":"15901031592","imsi":"234234","imei":"21341234","phoneOs":"android","phoneOsVer":"2.2","manuName":"asd","modelName":"","privinceID":"49","cityID":"9000451","privinceName":"北京","cityName":"北京","deptID":"01","deptName":"联通"}
	 * 首次身份确认接口
	 */
	public String InitialAuthentication() {
			JSONObject peopleJson = new JSONObject();
			try {
				BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s != null){
					JSONObject jsnb = new JSONObject(s);
					PeopleInfo people = null;
					if(jsnb.getString("phoneNo") !=null){
						people = peopleService.findByPhoneNumber(jsnb.getString("phoneNo"));
						if(people !=null){
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "手机号已存在");
							peopleJson.put("peopleID", people.getId());
							peopleJson.put("status", people.getIsRegistered());
							if(people.getUserPassword() !=null && !"".equals(people.getUserPassword())){
								peopleJson.put("isNull", 0);
							}else{
								peopleJson.put("isNull", 1);
							}
						}else{
							people = new PeopleInfo();
							String peopleID = peopleService.findMaxPeopleID();
							if(peopleID ==null){
								peopleID = "00001";
							}
//							int d = Integer.valueOf(peopleID);
							int peopleintId = Integer.parseInt(peopleID);
							peopleintId++;
							String str = String.valueOf(peopleintId);
							if(str.length()==1){
								str = "0000" + str;
							}else if(str.length() ==2){
								str = "000" +str;
							}else if (str.length() ==3){
								str = "00" + str;
							}else if(str.length() ==4){
								str = "0" + str;
							}
							
							String PeopleStr = "HY01" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + str;
							people.setId(PeopleStr);
							people.setPhoneNumber(jsnb.getString("phoneNo"));
							if(jsnb.getString("imsi") !=null){
								people.setPhoneIMSI(jsnb.getString("imsi"));
							}
							if(jsnb.getString("imei") !=null){
								people.setPhoneIMEI(jsnb.getString("imei"));
							}
							if(jsnb.getString("phoneOs") !=null){
								people.setPhoneOS(jsnb.getString("phoneOs"));
							}
							if(jsnb.getString("phoneOsVer") !=null){
								people.setPhoneOSversion(jsnb.getString("phoneOsVer"));
							}
							if(jsnb.getString("manuName") !=null){
								people.setManuName(jsnb.getString("manuName"));
							}
							if(jsnb.getString("modelName") !=null){
								people.setModelName(jsnb.getString("modelName"));
							}
							if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
								people.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
							}
							if(jsnb.getString("cityID") !=null  && !"".equals(jsnb.getString("cityID"))){
								people.setCityId(Integer.parseInt(jsnb.getString("cityID")));
							}
							if(jsnb.getString("privinceName") !=null){
								people.setProvName(jsnb.getString("privinceName"));
							}
							if(jsnb.getString("cityName") !=null){
								people.setCityName(jsnb.getString("cityName"));
							}
							if(jsnb.getString("deptID") !=null){
								people.setDeptId(jsnb.getString("deptID"));
							}
							if(jsnb.getString("deptName") !=null){
								people.setDeptName(jsnb.getString("deptName"));
							}
							if(jsnb.getString("phoneSource") !=null){
								people.setPhoneSource(Integer.parseInt(jsnb.getString("phoneSource")));
							}
							
							people.setIsRegistered(0);
							people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							peopleService.saveOrUpdatePeople(people);
//							people = peopleService.findByPhoneNumber(jsnb.getString("phoneNo"));
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "该手机号添加成功");
							peopleJson.put("peopleID", PeopleStr);
							peopleJson.put("status", 0);
							peopleJson.put("isNull", 1);
						}
					}else if(jsnb.getString("imsi") !=null){
						people = peopleService.findByPhoneIMSI(jsnb.getString("imsi"));
						if(people !=null){
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "该IMSI卡已存在");
							peopleJson.put("peopleID", people.getId());
							peopleJson.put("status", people.getIsRegistered());
							if(people.getUserPassword() !=null && !"".equals(people.getUserPassword())){
								peopleJson.put("isNull", 0);
							}else{
								peopleJson.put("isNull", 1);
							}
						}else{
							people = new PeopleInfo();
							
							String peopleID = peopleService.findMaxPeopleID();
							if(peopleID ==null){
								peopleID = "00001";
							}
							int peopleintId = Integer.parseInt(peopleID);
							peopleintId++;
							String str = String.valueOf(peopleintId);
							if(str.length()==1){
								str = "0000" + str;
							}else if(str.length() ==2){
								str = "000" +str;
							}else if (str.length() ==3){
								str = "00" + str;
							}else if(str.length() ==4){
								str = "0" + str;
							}
							
							String PeopleStr = "HY01" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + str;
							people.setId(PeopleStr);
							
							people.setPhoneNumber(jsnb.getString("imsi"));
							if(jsnb.getString("imei") !=null){
								people.setPhoneIMEI(jsnb.getString("imei"));
							}
							
							if(jsnb.getString("phoneOs") !=null){
								people.setPhoneOS(jsnb.getString("phoneOs"));
							}
							if(jsnb.getString("phoneOsVer") !=null){
								people.setPhoneOSversion(jsnb.getString("phoneOsVer"));
							}
							if(jsnb.getString("manuName") !=null){
								people.setManuName(jsnb.getString("manuName"));
							}
							if(jsnb.getString("modelName") !=null){
								people.setModelName(jsnb.getString("modelName"));
							}
							if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
								people.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
							}
							if(jsnb.getString("cityID") !=null && !"".equals(jsnb.getString("cityID"))){
								people.setCityId(Integer.parseInt(jsnb.getString("cityID")));
							}
							if(jsnb.getString("privinceName") !=null){
								people.setProvName(jsnb.getString("privinceName"));
							}
							if(jsnb.getString("cityName") !=null){
								people.setCityName(jsnb.getString("cityName"));
							}
							if(jsnb.getString("deptID") !=null){
								people.setDeptId(jsnb.getString("deptID"));
							}
							if(jsnb.getString("deptName") !=null){
								people.setDeptName(jsnb.getString("deptName"));
							}
							
							if(jsnb.getString("phoneSource") !=null){
								people.setPhoneSource(Integer.parseInt(jsnb.getString("phoneSource")));
							}
							
							people.setIsRegistered(0);
							people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							peopleService.saveOrUpdatePeople(people);
//							people = peopleService.findByPhoneIMSI(jsnb.getString("imsi"));
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "该IMSI卡添加成功");
							peopleJson.put("peopleID", PeopleStr);
							peopleJson.put("status", 0);
							peopleJson.put("isNull", 1);
						}
					}else if(jsnb.getString("imei") !=null){
						people = peopleService.findByPhoneIMEI(jsnb.getString("imei"));
						if(people !=null){
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "该IMEI卡已存在");
							peopleJson.put("peopleID", people.getId());
							peopleJson.put("status", people.getIsRegistered());
							if(people.getUserPassword() !=null && !"".equals(people.getUserPassword())){
								peopleJson.put("isNull", 0);
							}else{
								peopleJson.put("isNull", 1);
							}
						}else{
							people = new PeopleInfo();
							
							String peopleID = peopleService.findMaxPeopleID();
							if(peopleID ==null){
								peopleID = "00001";
							}
							int peopleintId = Integer.parseInt(peopleID);
							peopleintId++;
							String str = String.valueOf(peopleintId);
							if(str.length()==1){
								str = "0000" + str;
							}else if(str.length() ==2){
								str = "000" +str;
							}else if (str.length() ==3){
								str = "00" + str;
							}else if(str.length() ==4){
								str = "0" + str;
							}
							
							String PeopleStr = "HY01" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + str;
							people.setId(PeopleStr);
							people.setIsRegistered(0);
							people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							people.setPhoneNumber(jsnb.getString("imei"));
							if(jsnb.getString("phoneOs") !=null){
								people.setPhoneOS(jsnb.getString("phoneOs"));
							}
							if(jsnb.getString("phoneOsVer") !=null){
								people.setPhoneOSversion(jsnb.getString("phoneOsVer"));
							}
							if(jsnb.getString("manuName") !=null){
								people.setManuName(jsnb.getString("manuName"));
							}
							if(jsnb.getString("modelName") !=null){
								people.setModelName(jsnb.getString("modelName"));
							}
							if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
								people.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
							}
							if(jsnb.getString("cityID") !=null && !"".equals(jsnb.getString("cityID"))){
								people.setCityId(Integer.parseInt(jsnb.getString("cityID")));
							}
							if(jsnb.getString("privinceName") !=null){
								people.setProvName(jsnb.getString("privinceName"));
							}
							if(jsnb.getString("cityName") !=null){
								people.setCityName(jsnb.getString("cityName"));
							}
							if(jsnb.getString("deptID") !=null){
								people.setDeptId(jsnb.getString("deptID"));
							}
							if(jsnb.getString("deptName") !=null){
								people.setDeptName(jsnb.getString("deptName"));
							}
							
							if(jsnb.getString("phoneSource") !=null){
								people.setPhoneSource(Integer.parseInt(jsnb.getString("phoneSource")));
							}
							
							peopleService.saveOrUpdatePeople(people);
//							people = peopleService.findByPhoneIMSI(jsnb.getString("imei"));
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "该IMEI卡添加成功");
							peopleJson.put("peopleID", PeopleStr);
							peopleJson.put("status",0);
							peopleJson.put("isNull", 1);
						}
					}else{
						people = new PeopleInfo();
						
						String peopleID = peopleService.findMaxPeopleID();
						if(peopleID ==null){
							peopleID = "00001";
						}
						int peopleintId = Integer.parseInt(peopleID);
						peopleintId++;
						String str = String.valueOf(peopleintId);
						if(str.length()==1){
							str = "0000" + str;
						}else if(str.length() ==2){
							str = "000" +str;
						}else if (str.length() ==3){
							str = "00" + str;
						}else if(str.length() ==4){
							str = "0" + str;
						}
						
						String PeopleStr = "HY01" + new SimpleDateFormat("yyyyMMDD").format(new Date()) + str;
						people.setId(PeopleStr);
						people.setIsRegistered(0);
						if(jsnb.getString("phoneOs") !=null){
							people.setPhoneOS(jsnb.getString("phoneOs"));
						}
						if(jsnb.getString("phoneOsVer") !=null){
							people.setPhoneOSversion(jsnb.getString("phoneOsVer"));
						}
						if(jsnb.getString("manuName") !=null){
							people.setManuName(jsnb.getString("manuName"));
						}
						if(jsnb.getString("modelName") !=null){
							people.setModelName(jsnb.getString("modelName"));
						}
						if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
							people.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
						}
						if(jsnb.getString("cityID") !=null && !"".equals(jsnb.getString("cityID"))){
							people.setCityId(Integer.parseInt(jsnb.getString("cityID")));
						}
						if(jsnb.getString("privinceName") !=null){
							people.setProvName(jsnb.getString("privinceName"));
						}
						if(jsnb.getString("cityName") !=null){
							people.setCityName(jsnb.getString("cityName"));
						}
						if(jsnb.getString("deptID") !=null){
							people.setDeptId(jsnb.getString("deptID"));
						}
						if(jsnb.getString("deptName") !=null){
							people.setDeptName(jsnb.getString("deptName"));
						}
						if(jsnb.getString("phoneSource") !=null){
							people.setPhoneSource(Integer.parseInt(jsnb.getString("phoneSource")));
						}
						people.setInsert_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						peopleService.saveOrUpdatePeople(people);
						
						peopleJson.put("success", "20000");
						peopleJson.put("desc", "该未知用户添加成功");
						peopleJson.put("peopleID", PeopleStr);
						peopleJson.put("status", 0);
						peopleJson.put("isNull", 1);
					}
					
				}else{
					peopleJson.put("success", "20400");
					peopleJson.put("desc", "上传数据失败");
				}
			} catch (Exception e) {
				try {
					logger.error(e);
					peopleJson.put("success", "20400");
					peopleJson.put("desc", "系统错误");
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
			} 
			
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * {"phoneNo":"15901031592","imsi":"234234","imei":"21341234","phoneOs":"android","phoneOsVer":"2.2","manuName":"asd","modelName":"","privinceID":"49","cityID":"9000451","privinceName":"北京","cityName":"北京","deptID":"01","deptName":"联通","appinfo":[{"packUUID":"aweawe","packName":"","appVersion":"","appName":"","appTypeId":"","appTypeName":"","columnName":"","columnID":"","operTime":"","oper":""},{"packUUID":"aweawe","packName":"","appVersion":"","appName":"","appTypeId":"","appTypeName":"","columnName":"","columnID":"","operTime":"","oper":""},{"packUUID":"aweawe","packName":"","appVersion":"","appName":"","appTypeId":"","appTypeName":"","columnName":"","columnID":"","operTime":"","oper":""}]}
	 * 用户下载安装接口
	 */
	public String peopledown() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject jsnb = new JSONObject(s);
				if(jsnb.getString("appinfo") !=null){
					String appinfo = jsnb.getString("appinfo");
					JSONArray jsar = new JSONArray(appinfo);
					if(jsar !=null && jsar.length()>0){
						for(int i=0;i<jsar.length();i++){
							int count = softwareInstallLogService.findSoftwareInstallLog(jsnb.getString("peopleID"), jsar.getJSONObject(i).getString("packUUID"));
							if(count ==0){
								SoftwareInstallLog softApp = new SoftwareInstallLog();
								if(jsnb.getString("peopleID") !=null){
									softApp.setPeopleId(jsnb.getString("peopleID"));
								}
								if(jsnb.getString("userNumber") !=null){
									softApp.setUserNumber(jsnb.getString("userNumber"));
								}
								if(jsnb.getString("phoneNo") !=null){
									softApp.setPhoneNumber(jsnb.getString("phoneNo"));
								}
								if(jsnb.getString("imsi") !=null){
									softApp.setPhoneIMSI(jsnb.getString("imsi"));
								}
								if(jsnb.getString("imei") !=null){
									softApp.setPhoneIMEI(jsnb.getString("imei"));
								}
								if(jsnb.getString("phoneOs") !=null){
									softApp.setPhoneOS(jsnb.getString("phoneOs"));
								}
								if(jsnb.getString("phoneOsVer") !=null){
									softApp.setPhoneOSversion(jsnb.getString("phoneOsVer"));
								}
								if(jsnb.getString("manuName") !=null){
									softApp.setManuName(jsnb.getString("manuName"));
								}
								if(jsnb.getString("modelName") !=null){
									softApp.setModelName(jsnb.getString("modelName"));
								}
								if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
									softApp.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
								}
								if(jsnb.getString("cityID") !=null  && !"".equals(jsnb.getString("cityID"))){
									softApp.setCityId(Integer.parseInt(jsnb.getString("cityID")));
								}
								if(jsnb.getString("privinceName") !=null){
									softApp.setProvName(jsnb.getString("privinceName"));
								}
								if(jsnb.getString("cityName") !=null){
									softApp.setCityName(jsnb.getString("cityName"));
								}
								if(jsnb.getString("deptID") !=null){
									softApp.setDeptId(jsnb.getString("deptID"));
								}
								if(jsnb.getString("deptName") !=null){
									softApp.setDeptName(jsnb.getString("deptName"));
								}
								if(jsar.getJSONObject(i).getString("packUUID") !=null){
									softApp.setPackUUID(jsar.getJSONObject(i).getString("packUUID"));
								}
								if(jsar.getJSONObject(i).getString("packName") != null){
									softApp.setBundleID(jsar.getJSONObject(i).getString("packName"));
								}
								if(jsar.getJSONObject(i).getString("appVersion") !=null){
									softApp.setAppVersion(jsar.getJSONObject(i).getString("appVersion"));
								}
								if(jsar.getJSONObject(i).getString("appName") !=null){
									softApp.setAppName(jsar.getJSONObject(i).getString("appName"));
								}
								if(jsar.getJSONObject(i).getString("appTypeId") !=null){
									softApp.setAppTypeId(jsar.getJSONObject(i).getString("appTypeId"));
								}
								if(jsar.getJSONObject(i).getString("appTypeName") !=null){
									softApp.setAppTypeName(jsar.getJSONObject(i).getString("appTypeName"));
								}
								if(jsar.getJSONObject(i).getString("netType") !=null){
									softApp.setNetType(jsar.getJSONObject(i).getString("netType"));
								}
								if(jsar.getJSONObject(i).getString("columnName") !=null){
									softApp.setColumnName(jsar.getJSONObject(i).getString("columnName"));
								}
								if(jsar.getJSONObject(i).getString("columnID") !=null){
									softApp.setColumnID(jsar.getJSONObject(i).getString("columnID"));
								}
								if(jsar.getJSONObject(i).getString("operTime") !=null){
									softApp.setOperTime(jsar.getJSONObject(i).getString("operTime"));
								}
								if(jsar.getJSONObject(i).getString("oper") !=null){
									softApp.setOper(jsar.getJSONObject(i).getString("oper"));
								}
								softwareInstallLogService.saveOrUpdateSoftwareInstallLog(softApp);
								peopleJson.put("success", "20000");
								peopleJson.put("desc", "上传成功");
							}else{
								peopleJson.put("success", "20000");
								peopleJson.put("desc", "上传成功");
							}
						}
					}
				}
			}else{
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "上传失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		} 
		
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 用户流量接口
	 * @return
	 */
	public String peopleflowdown() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject jsnb = new JSONObject(s);
				if(jsnb.getString("flowinfo") !=null){
					String appinfo = jsnb.getString("flowinfo");
					JSONArray jsar = new JSONArray(appinfo);
					if(jsar !=null && jsar.length()>0){
						for(int i=0;i<jsar.length();i++){
							UserFlowLog softApp = new UserFlowLog();
							if(jsnb.getString("peopleID") !=null){
								softApp.setPeopleId(jsnb.getString("peopleID"));
							}
							if(jsnb.getString("userNumber") !=null){
								softApp.setUserNumber(jsnb.getString("userNumber"));
							}
							if(jsnb.getString("phoneNo") !=null){
								softApp.setPhoneNumber(jsnb.getString("phoneNo"));
							}
							if(jsnb.getString("imsi") !=null){
								softApp.setPhoneIMSI(jsnb.getString("imsi"));
							}
							if(jsnb.getString("imei") !=null){
								softApp.setPhoneIMEI(jsnb.getString("imei"));
							}
							if(jsnb.getString("phoneOs") !=null){
								softApp.setOs(jsnb.getString("phoneOs"));
							}
							if(jsnb.getString("phoneOsVer") !=null){
								softApp.setOsver(jsnb.getString("phoneOsVer"));
							}
							if(jsnb.getString("privinceID") !=null  && !"".equals(jsnb.getString("privinceID"))){
								softApp.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
							}
							if(jsnb.getString("cityID") !=null  && !"".equals(jsnb.getString("cityID"))){
								softApp.setCityId(Integer.parseInt(jsnb.getString("cityID")));
							}
							if(jsnb.getString("privinceName") !=null){
								softApp.setProvName(jsnb.getString("privinceName"));
							}
							if(jsnb.getString("cityName") !=null){
								softApp.setCityName(jsnb.getString("cityName"));
							}
							softApp.setPeriodTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
							if(jsnb.getString("manuName") !=null){
								softApp.setManuName(jsnb.getString("manuName"));
							}
							if(jsnb.getString("modelName") !=null){
								softApp.setModelName(jsnb.getString("modelName"));
							}
							softApp.setDeptId(jsnb.getString("deptID"));
							softApp.setDeptName(jsnb.getString("deptName"));
							
							
							if(jsar.getJSONObject(i).getString("startTime") != null){
								softApp.setStartTime(jsar.getJSONObject(i).getString("startTime"));
							}
							if(jsar.getJSONObject(i).getString("endTime") !=null){
								softApp.setEndTime(jsar.getJSONObject(i).getString("endTime"));
							}
				
							if(jsar.getJSONObject(i).getString("flowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("flowValue"))){
								softApp.setFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("flowValue")));
							}else{
								softApp.setFlowValue(0);
							}
							if(jsar.getJSONObject(i).getString("wifiFlowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("wifiFlowValue"))){
								softApp.setWifiFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("wifiFlowValue")));
							}else{
								softApp.setWifiFlowValue(0);
							}
							if(jsar.getJSONObject(i).getString("mobileFlowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("mobileFlowValue"))){
								softApp.setMobileFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("mobileFlowValue")));
							}else{
								softApp.setMobileFlowValue(0);
							}
							softwareInstallLogService.saveOrUpdateUserFlowLog(softApp);
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "成功");
						}
					}
				}
			}else{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数上传失败");
			}
		} catch (Exception e) {
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		} 
		
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 应用流量接口
	 * @return
	 */
	public String appFlowdown() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			System.out.println(s);
			if(s != null){
				JSONObject jsnb = new JSONObject(s);
				if(jsnb.getString("flowinfo") !=null){
					String appinfo = jsnb.getString("flowinfo");
					JSONArray jsar = new JSONArray(appinfo);
					if(jsar !=null && jsar.length()>0){
						for(int i=0;i<jsar.length();i++){
							AppFlowLog softApp = new AppFlowLog();
							if(jsnb.getString("peopleID") !=null){
								softApp.setPeopleId(jsnb.getString("peopleID"));
							}
							if(jsnb.getString("userNumber") !=null){
								softApp.setUserNumber(jsnb.getString("userNumber"));
							}
							if(jsnb.getString("phoneNo") !=null){
								softApp.setPhoneNumber(jsnb.getString("phoneNo"));
							}
							if(jsnb.getString("imsi") !=null){
								softApp.setPhoneIMSI(jsnb.getString("imsi"));
							}
							if(jsnb.getString("imei") !=null){
								softApp.setPhoneIMEI(jsnb.getString("imei"));
							}
							if(jsnb.getString("manuName") !=null){
								softApp.setManuName(jsnb.getString("manuName"));
							}
							if(jsnb.getString("modelName") !=null){
								softApp.setModelName(jsnb.getString("modelName"));
							}
							if(jsnb.getString("phoneOs") !=null){
								softApp.setOs(jsnb.getString("phoneOs"));
							}
							if(jsnb.getString("phoneOsVer") !=null){
								softApp.setOsver(jsnb.getString("phoneOsVer"));
							}
							if(jsnb.getString("privinceID") !=null && !"".equals(jsnb.getString("privinceID"))){
								softApp.setPrivinceId(Integer.parseInt(jsnb.getString("privinceID")));
							}
							if(jsnb.getString("cityID") !=null && !"".equals(jsnb.getString("cityID"))){
								softApp.setCityId(Integer.parseInt(jsnb.getString("cityID")));
							}
							if(jsnb.getString("privinceName") !=null){
								softApp.setProvName(jsnb.getString("privinceName"));
							}
							if(jsnb.getString("cityName") !=null){
								softApp.setCityName(jsnb.getString("cityName"));
							}
							softApp.setDeptId(jsnb.getString("deptID"));
							softApp.setDeptName(jsnb.getString("deptName"));
							softApp.setPeriodTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
							System.out.println(jsar.getJSONObject(i).toString());
							if(jsar.getJSONObject(i).getString("packName") != null){
								softApp.setBundleID(jsar.getJSONObject(i).getString("packName"));
							}
							if(jsar.getJSONObject(i).getString("appVersion") !=null){
								softApp.setAppVersion(jsar.getJSONObject(i).getString("appVersion"));
							}
							if(jsar.getJSONObject(i).getString("appName") !=null){
								softApp.setAppName(jsar.getJSONObject(i).getString("appName"));
							}
							if(jsar.getJSONObject(i).getString("appTypeId") !=null){
								softApp.setAppTypeId(jsar.getJSONObject(i).getString("appTypeId"));
							}
							if(jsar.getJSONObject(i).getString("appTypeName") !=null){
								softApp.setAppTypeName(jsar.getJSONObject(i).getString("appTypeName"));
							}
							if(jsar.getJSONObject(i).getString("startTime") != null){
								softApp.setStartTime(jsar.getJSONObject(i).getString("startTime"));
							}
							if(jsar.getJSONObject(i).getString("endTime") !=null){
								softApp.setEndTime(jsar.getJSONObject(i).getString("endTime"));
							}
							if(jsar.getJSONObject(i).getString("flowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("flowValue"))){
								softApp.setFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("flowValue")));
							}else{
								softApp.setFlowValue(0);
							}
							if(jsar.getJSONObject(i).getString("wifiFlowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("wifiFlowValue"))){
								softApp.setWifiFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("wifiFlowValue")));
							}else{
								softApp.setWifiFlowValue(0);
							}
							if(jsar.getJSONObject(i).getString("mobileFlowValue") !=null && !"".equals(jsar.getJSONObject(i).getString("mobileFlowValue"))){
								softApp.setMobileFlowValue(Double.parseDouble(jsar.getJSONObject(i).getString("mobileFlowValue")));
							}else{
								softApp.setMobileFlowValue(0);
							}
							softwareInstallLogService.saveOrUpdateAppFlowLog(softApp);
							peopleJson.put("success", "20000");
							peopleJson.put("desc", "上传成功");
						}
					}
				}
			}else{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "上传失败");
			}
		} catch (Exception e) {
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		} 
		
		return this.doPrint(peopleJson.toString());
	}
	
	public String getProvAndCityID() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			int provid = 0;
			int cityid = 0;
			if(s != null){
				JSONObject json = new JSONObject(s);
				provid = peopleService.provinceID(json.getString("proname"));
				cityid = peopleService.cityID(json.getString("cityname"));
				peopleJson.put("success", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("provID", provid);
				peopleJson.put("provName", json.getString("proname"));
				peopleJson.put("cityID", cityid);
				peopleJson.put("cityName", json.getString("cityname"));
			}else
			{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数错误");
			}
		}catch(Exception e){
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	/*
	 * 意见反馈接口
	 * */
	public String feedback() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				FeedbackInfo fee = new FeedbackInfo();
				fee.setPeopleId(json.getString("peopleID"));
				fee.setUserNumber(json.getString("userNumber"));
				fee.setPhoneNumber(json.getString("phoneNo"));
				if(json.getString("privinceID") !=null && !"".equals(json.getString("privinceID"))){
				fee.setProvId(Integer.parseInt(json.getString("privinceID")));
				}
				if(json.getString("cityID") !=null && "".equals(json.getString("cityID"))){
					fee.setCityId(Integer.parseInt(json.getString("cityID")));
				}
				fee.setFeedType(json.getString("feeType"));
				fee.setFeedTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				fee.setContent(json.getString("content"));
				peopleService.saveOrUpdateFeeback(fee);
				peopleJson.put("success", "20000");
				peopleJson.put("desc", "成功");
			}else
			{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数错误");
			}
		}catch(Exception e){
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/*
	 * 卸载接口
	 * */
	public String Uninstall() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				User_uninstall_log fee = new User_uninstall_log();
				fee.setPeopleId(json.getString("peopleID"));
				fee.setPhoneNumber(json.getString("phoneNo"));
				fee.setImei(json.getString("imei"));
				fee.setImsi(json.getString("imsi"));
				fee.setOperTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				fee.setBundleId(json.getString("packname"));
				fee.setOs(json.getString("os"));
				peopleService.saveOrUpdateUninstall(fee);
				peopleJson.put("success", "20000");
				peopleJson.put("desc", "成功");
			}else
			{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数错误");
			}
		}catch(Exception e){
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}

	public String getConfiginfo(){
		JSONObject pJson = new JSONObject();
		try {
				ClientConfigInfo client = peopleService.findClientConfigInfo();
				if(client !=null && client.getJsonStr() !=null && !"".equals(client.getJsonStr())){
					pJson.put("success", 20000);
					pJson.put("desc", "获取成功");
					JSONObject j = new JSONObject(client.getJsonStr());
					pJson.put("jsonStr",j);
				}else{
					pJson.put("success", 20001);
					pJson.put("desc", "无配置信息");
				}
		}catch(Exception e){
			try{
			pJson.put("success", 20400);
			pJson.put("desc", "系统错误");
			}catch(Exception e1){
				e1.printStackTrace();
			}
		}
		return this.doPrint(pJson.toString());
	}
	
	/*
	 * 配置
	 * */
	public String configuration(){
		JSONObject pJson = new JSONObject();
		try{
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject pJsonparse = new JSONObject(s);
				ClientConfigInfo client = peopleService.findClientConfigInfo();
				if(client !=null){
					client.setJsonStr(pJsonparse.getString("jsonStr"));
					peopleService.saveOrUpdateClientConfigInfo(client);
					pJson.put("success", 20000);
					pJson.put("desc", "修改成功");
				}else{
					ClientConfigInfo newclient = new ClientConfigInfo();
					newclient.setJsonStr(pJsonparse.getString("jsonStr"));
					peopleService.saveOrUpdateClientConfigInfo(newclient);
					pJson.put("success", 20000);
					pJson.put("desc", "添加成功");
				}
			}
		}catch(Exception e){
			try{
			pJson.put("success", 20400);
			pJson.put("desc", "系统错误");
			}catch(Exception e1){
				e1.printStackTrace();
			}
		}
		return this.doPrint(pJson.toString());
	}
	
	
	public String configurationPage(){
		client = peopleService.findClientConfigInfo();
		if(client ==null){
			client = new ClientConfigInfo();
		}
		return SUCCESS;
	}
	
	public String configurationSaveOrUpdate(){
		try{
			client = peopleService.findClientConfigInfo();
			if(client ==null){
				client = new ClientConfigInfo();
			}
			client.setJsonStr(jsonStr);
			peopleService.saveOrUpdateClientConfigInfo(client);
			this.setMessage("{\"errorCode\":0}");
		}catch(Exception e){
			this.setMessage("{\"errorCode\":1}");	
		}
		return AJAX;
	}
	
	
	
	/*
	 * 信号质量接口
	 * */
	public String signalqualitydata() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				Signalqualitydata fee = new Signalqualitydata();
				fee.setPeopleId(json.getString("peopleID"));
				fee.setUsername(json.getString("username"));
				fee.setPhonenum(json.getString("phoneno"));
				fee.setCid(json.getString("cid"));
				fee.setCityName(json.getString("cityname"));
				fee.setDeptId(json.getString("deptId"));
				fee.setDeptname(json.getString("deptname"));
				fee.setImei(json.getString("imei"));
				fee.setImsi(json.getString("imsi"));
				fee.setInster_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				fee.setLac(json.getString("lac"));
				fee.setLatitude(json.getString("latitude"));
				fee.setLocation(json.getString("location"));
				fee.setLongitude(json.getString("longitude"));
				fee.setManuName(json.getString("manuname"));
				fee.setModelName(json.getString("modelname"));
				fee.setMcc(json.getString("mcc"));
				fee.setMnc(json.getString("mnc"));
				fee.setNetType(json.getString("nettype"));
				fee.setOs(json.getString("os"));
				fee.setOsVersion(json.getString("osversion"));
				fee.setProvName(json.getString("provname"));
				fee.setSignalQuality(json.getString("sigQua"));
				if(json.getString("provId") !=null && !"".equals(json.getString("provId"))){
					fee.setProvId(Integer.parseInt(json.getString("provId")));
				}
				if(json.getString("cityID") !=null && !"".equals(json.getString("cityID"))){
					fee.setCityId(Integer.parseInt(json.getString("cityID")));
				}
				peopleService.saveOrUpdateSignalqualitydata(fee);
				peopleJson.put("success", "20000");
				peopleJson.put("desc", "成功");
			}else
			{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数错误");
			}
		}catch(Exception e){
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	/*
	 * 信号投诉接口
	 * */
	public String signalrequest() {
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				Signalrequest fee = new Signalrequest();
				fee.setPeopleId(json.getString("peopleID"));
				fee.setUsername(json.getString("username"));
				fee.setPhonenum(json.getString("phoneno"));
				fee.setCid(json.getString("cid"));
				fee.setCityName(json.getString("cityname"));
				fee.setDeptId(json.getString("deptId"));
				fee.setDeptname(json.getString("deptname"));
				fee.setImei(json.getString("imei"));
				fee.setImsi(json.getString("imsi"));
				fee.setInster_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				fee.setLac(json.getString("lac"));
				fee.setLatitude(json.getString("latitude"));
				fee.setLocation(json.getString("location"));
				fee.setLongitude(json.getString("longitude"));
				fee.setManuName(json.getString("manuname"));
				fee.setModelName(json.getString("modelname"));
				fee.setMcc(json.getString("mcc"));
				fee.setMnc(json.getString("mnc"));
				fee.setNetType(json.getString("nettype"));
				fee.setOs(json.getString("os"));
				fee.setOsVersion(json.getString("osversion"));
				fee.setProvName(json.getString("provname"));
				fee.setSignalQuality(json.getString("sigQua"));
				if(json.getString("provId") !=null && !"".equals(json.getString("provId"))){
					fee.setProvId(Integer.parseInt(json.getString("provId")));
				}
				if(json.getString("cityID") !=null && !"".equals(json.getString("cityID"))){
					fee.setCityId(Integer.parseInt(json.getString("cityID")));
				}
				fee.setPhoneIp(json.getString("phoneIp"));
				fee.setComplaints(json.getString("content"));
				fee.setComplaintsType(json.getString("question"));
				fee.setScene(json.getString("scene"));
				peopleService.saveOrUpdateSignalrequest(fee);
				peopleJson.put("success", "20000");
				peopleJson.put("desc", "成功");
			}else
			{
				peopleJson.put("success", "20002");
				peopleJson.put("desc", "参数错误");
			}
		}catch(Exception e){
			try {
				peopleJson.put("success", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 通讯录备份
	 * @return
	 */
	public String contactsBackup(){
		JSONObject peopleJson = new JSONObject();
		try{
			if(memo !=null && memo.length()>120){
				peopleJson.put("result", "20002");
				peopleJson.put("desc", "描述不能太长");
				return this.doPrint(peopleJson.toString());
			}
//			peopleService.findvcardFile
			peopleService.saveOrUpdateContactBakData1(peopleId, memo, username, vcfVersion, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), vcardFile,count);
			peopleJson.put("result", "20000");
			peopleJson.put("desc", "成功");
		}catch(Exception e){
			try {
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	

	
	public String ContactBakDataList(){
		JSONObject json = new JSONObject();
		try{
			List<ContactBakData> cList = peopleService.ContactBakDataList(peopleId);
		
			if(cList !=null){
				JSONArray a = new JSONArray();
				for(int i=0;i<cList.size();i++){
					JSONObject j = new JSONObject();
					j.put("id", cList.get(i).getId());
					j.put("username", cList.get(i).getUsername());
					j.put("vcfVersion", cList.get(i).getVcfVersion());
					j.put("vcfTime", cList.get(i).getVcfTime());
					j.put("Memo", cList.get(i).getMemo());
					j.put("count", cList.get(i).getCount());
					a.put(j);
				}
				json.put("result", 20000);
				json.put("desc", "成功");
				json.put("data", a);
			}else{
				json.put("result", 20001);
				json.put("desc", "没有数据");
			}
		}catch(Exception e){
			try {
				json.put("result", 20400);
				json.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(json.toString());
	}
	
	public String findvcardFile(){
		return SUCCESS;
	}
	public InputStream getInputStream()throws Exception{
		InputStream str = peopleService.findvcardFile(id);
		contentLength = str.available()+"";
		fileName = new SimpleDateFormat("yyyyMMdd").format(new Date()) +".rar";
		return str;
	}
	
	
	/**
	 * 软件更新接口
	 * @return
	 */
	public String updatenew(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
					List<String> strList = peopleService.findnewVersion(json.getString("apkPkgName"));
					if(strList !=null && !"".equals(strList) && strList.size()>0){
						if(strList.get(1) !=null){
//							long maxresult =  Util.softVerToInt2(strList.get(1));
//							long clientresult =  Util.softVerToInt2(json.getString("version"));
							int maxresult =  Integer.parseInt(strList.get(3));
							int clientresult =  Integer.parseInt(json.getString("version"));
							if(maxresult>clientresult){
								peopleJson.put("result", "20000");
								peopleJson.put("desc", "成功");
								peopleJson.put("version", strList.get(1));
								String url = appQuerier.getPackUrl(strList.get(0));
								peopleJson.put("downurl", url);
							}else{
								peopleJson.put("result", "20001");
								peopleJson.put("desc", "无更新");
							}
						}else{
							peopleJson.put("result", "20001");
							peopleJson.put("desc", "无更新");
						}
					}else{
						peopleJson.put("result", "20001");
						peopleJson.put("desc", "无更新");
					}
				}
		}catch(Exception e){
			try {
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}  
	
	
	/**
	 * 软件更新接口
	 * @return
	 */
	public String bulupdatenew(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONArray json = new JSONArray(s);
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				JSONArray arrs = new JSONArray();
				for(int i=0;i<json.length();i++){
					json.getJSONObject(i);
					List<String> strList = peopleService.findnewVersion(json.getJSONObject(i).getString("apkPkgName"));
					if(strList !=null && !"".equals(strList) && strList.size()>0){
						if(strList.get(1) !=null){
//							int maxresult =  Util.softVerToInt2(strList.get(3));
//							int clientresult =  Util.softVerToInt2(json.getJSONObject(i).getString("version"));
							int maxresult =  Integer.parseInt(strList.get(3));
							int clientresult = Integer.parseInt(json.getJSONObject(i).getString("version"));
							if(maxresult>clientresult){
								JSONObject jsonSing = new JSONObject();
								jsonSing.put("apkPkgName", strList.get(2));
								jsonSing.put("version", strList.get(1));
								String url = appQuerier.getPackUrl(strList.get(0));
								jsonSing.put("downurl", url);
								arrs.put(jsonSing);
							}
						}
					}
					Mobile_installed_list mobile = peopleService.findByDeviceNoAndApkName(json.getJSONObject(i).getString("imei"), json.getJSONObject(i).getString("apkPkgName"));
					if(mobile ==null){
						Mobile_installed_list mobile1 = new Mobile_installed_list();
						mobile1.setApkPkgName(json.getJSONObject(i).getString("apkPkgName"));
						mobile1.setDeviceNo(json.getJSONObject(i).getString("imei"));
						mobile1.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						peopleService.saveOrUpdateMobile_installed_list(mobile1);
					}
				}
				peopleJson.put("data", arrs);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 软件卸载接口
	 * @return
	 */
	public String uninstall(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				Mobile_installed_list mobile = peopleService.findByDeviceNoAndApkName(json.getString("imei"), json.getString("apkPkgName"));
				if(mobile !=null){
					peopleService.delteleMobile_installed_list(mobile);
				}
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 垃圾短信
	 * @return
	 */
	public String smsspam(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONArray json = new JSONArray(s);
				if(json !=null && json.length()>0){
					for(int j=0;j<json.length();j++){
						Smsspam thr = peopleService.findByPeopleIdAndsms(json.getJSONObject(j).getString("uid"),json.getJSONObject(j).getString("content"),json.getJSONObject(j).getString("time"));
						if(thr==null){
							Smsspam sms = new Smsspam();
							sms.setPeopleId(json.getJSONObject(j).getString("uid"));
							sms.setPhoneno(json.getJSONObject(j).getString("number"));
							sms.setBackuptime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							sms.setContent(json.getJSONObject(j).getString("content"));
							sms.setReceivetime(json.getJSONObject(j).getString("time"));
							peopleService.saveOrUpdateSmsspam(sms);
						}
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 批量垃圾短信
	 * @return
	 */
	public String bulsmsspam(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONArray json1 = new JSONArray(s);
				if(json1 !=null && json1.length()>0){
					for(int i=0;i<json1.length();i++){
						JSONObject json = new JSONObject(json1.getJSONObject(i).getString("thread"));
						JSONArray array = new JSONArray(json1.getJSONObject(i).getString("smsArray"));
						if(array !=null && array.length()>0){
							for(int j=0;j<array.length();j++){
								Smsspam thr = peopleService.findByPeopleIdAndsms(json.getString("uid"),array.getJSONObject(j).getString("content"),array.getJSONObject(j).getString("time"));
								if(thr==null){
									Smsspam sms = new Smsspam();
									sms.setPeopleId(json.getString("uid"));
									sms.setPhoneno(json.getString("number"));
									sms.setBackuptime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
									sms.setContent(array.getJSONObject(j).getString("content"));
									sms.setReceivetime(array.getJSONObject(j).getString("time"));
									peopleService.saveOrUpdateSmsspam(sms);
								}
							}
						}
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 短信备份
	 * @return
	 */
	public String sms_backup(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json1 = new JSONObject(s);

				JSONObject json = new JSONObject(json1.getString("thread"));
				JSONArray array = new JSONArray(json1.getString("smsArray"));
				Threads thr = peopleService.findBythrid(Integer.parseInt(json.getString("thread_id")));
				if(thr !=null){
					thr.setPeopleId(json.getString("uid"));
					thr.setPhoneno(json.getString("number"));
					thr.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					thr.setIds(json.getString("persons"));
					
					
					
					for(int i=0;i<array.length();i++){
						int num = peopleService.findByuidandsmsid(json.getString("uid"),Integer.parseInt(array.getJSONObject(i).getString("sms_id")),Integer.parseInt(json.getString("thread_id")));
						if(num==0){
							Sms_backup sms = new Sms_backup();
							sms.setPeopleId(json.getString("uid"));
							sms.setPhoneno(json.getString("number"));
							sms.setSms_id(Integer.parseInt(array.getJSONObject(i).getString("sms_id")));
							sms.setThreadId(Integer.parseInt(json.getString("thread_id")));
							sms.setAddress(array.getJSONObject(i).getString("address"));
							sms.setPerson(array.getJSONObject(i).getString("person"));
							sms.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//备份时间
							sms.setThread_date(array.getJSONObject(i).getString("time"));//回话时间
							sms.setProtocol(Integer.parseInt(array.getJSONObject(i).getString("protocol")));
							sms.setIsread(Integer.parseInt(array.getJSONObject(i).getString("read")));
							sms.setSms_status(Integer.parseInt(array.getJSONObject(i).getString("status")));
							sms.setSms_type(Integer.parseInt(array.getJSONObject(i).getString("type")));
							sms.setPresnet(Integer.parseInt(array.getJSONObject(i).getString("present")));
							sms.setSubject(array.getJSONObject(i).getString("subject"));
							sms.setSms_body(array.getJSONObject(i).getString("body"));
							sms.setColumncenter(array.getJSONObject(i).getString("center"));
							sms.setLocked(Integer.parseInt(array.getJSONObject(i).getString("locked")));
							sms.setError_code(Integer.parseInt(array.getJSONObject(i).getString("code")));
							sms.setSeen(Integer.parseInt(array.getJSONObject(i).getString("seen")));
							sms.setLong_date(array.getJSONObject(i).getString("longtime"));
							peopleService.saveOrUpdateSms(sms);
							int count = thr.getMessage_count();
							count++;
							thr.setMessage_count(count);
						}
					}
					
					thr.setSnippet(json.getString("snippet"));
					thr.setSnippet_cs(0);
					thr.setThread_date(json.getString("time"));
					thr.setIsread(Integer.parseInt(json.getString("read")));
					thr.setType(Integer.parseInt(json.getString("type")));
					thr.setThread_id(Integer.parseInt(json.getString("thread_id")));
	//				thr.setAttachment(Integer.parseInt(json.getString("present")));
					peopleService.saveOrUpdateThreads(thr);
				}else{
					thr = new Threads();
					thr.setPeopleId(json.getString("uid"));
					thr.setPhoneno(json.getString("number"));
					thr.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					thr.setIds(json.getString("persons"));
					
					int count =0;
					
					for(int i=0;i<array.length();i++){
						int num = peopleService.findByuidandsmsid(json.getString("uid"),Integer.parseInt(array.getJSONObject(i).getString("sms_id")),Integer.parseInt(json.getString("thread_id")));
						if(num==0){
							Sms_backup sms = new Sms_backup();
							sms.setPeopleId(json.getString("uid"));
							sms.setPhoneno(json.getString("number"));
							sms.setSms_id(Integer.parseInt(array.getJSONObject(i).getString("sms_id")));
							sms.setThreadId(Integer.parseInt(json.getString("thread_id")));
							sms.setAddress(array.getJSONObject(i).getString("address"));
							sms.setPerson(array.getJSONObject(i).getString("person"));
							sms.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));//备份时间
							sms.setThread_date(array.getJSONObject(i).getString("time"));//回话时间
							sms.setProtocol(Integer.parseInt(array.getJSONObject(i).getString("protocol")));
							sms.setIsread(Integer.parseInt(array.getJSONObject(i).getString("read")));
							sms.setSms_status(Integer.parseInt(array.getJSONObject(i).getString("status")));
							sms.setSms_type(Integer.parseInt(array.getJSONObject(i).getString("type")));
							sms.setPresnet(Integer.parseInt(array.getJSONObject(i).getString("present")));
							sms.setSubject(array.getJSONObject(i).getString("subject"));
							sms.setSms_body(array.getJSONObject(i).getString("body"));
							sms.setColumncenter(array.getJSONObject(i).getString("center"));
							sms.setLocked(Integer.parseInt(array.getJSONObject(i).getString("locked")));
							sms.setError_code(Integer.parseInt(array.getJSONObject(i).getString("code")));
							sms.setSeen(Integer.parseInt(array.getJSONObject(i).getString("seen")));
							sms.setLong_date(array.getJSONObject(i).getString("longtime"));
							peopleService.saveOrUpdateSms(sms);
							count++;
						}
					}
					thr.setMessage_count(count);
					thr.setSnippet(json.getString("snippet"));
					thr.setSnippet_cs(0);
					thr.setThread_date(json.getString("time"));
					thr.setIsread(Integer.parseInt(json.getString("read")));
					thr.setType(Integer.parseInt(json.getString("type")));
					thr.setThread_id(Integer.parseInt(json.getString("thread_id")));
	//				thr.setAttachment(Integer.parseInt(json.getString("present")));
					peopleService.saveOrUpdateThreads(thr);
				}
				
			
				
			
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	
	/**
	 * 短信查询接口
	 * @return
	 */
	public String sms_query(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<Sms_backup> smsList = peopleService.sms_backupList(json.getString("uid"), Integer.parseInt(json.getString("thread_id")),0);
				JSONArray jsarray = new JSONArray();
				if(smsList !=null){
					for(int i=0;i<smsList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("thread_id", smsList.get(i).getThreadId());
						jsObj.put("id", smsList.get(i).getId());
						jsObj.put("sender", smsList.get(i).getPhoneno());
						jsObj.put("address", smsList.get(i).getAddress());
						jsObj.put("time", smsList.get(i).getThread_date());
						jsObj.put("body", smsList.get(i).getSms_body());
						jsObj.put("read", smsList.get(i).getIsread());
						jsObj.put("locked", smsList.get(i).getLocked());
						jsObj.put("type", smsList.get(i).getSms_type());
						jsObj.put("backup_date", smsList.get(i).getBackup_date());
						jsObj.put("center", smsList.get(i).getColumncenter());
						jsObj.put("code", smsList.get(i).getError_code());
						jsObj.put("peopleId", smsList.get(i).getPeopleId());
						jsObj.put("person", smsList.get(i).getPerson());
						jsObj.put("present", smsList.get(i).getPresnet());
						jsObj.put("protocol", smsList.get(i).getProtocol());
						jsObj.put("seen", smsList.get(i).getSeen());
						jsObj.put("sms_id", smsList.get(i).getSms_id());
						jsObj.put("status", smsList.get(i).getSms_status());
						jsObj.put("subject", smsList.get(i).getSubject());
						jsObj.put("longtime", smsList.get(i).getLong_date());
						jsarray.put(jsObj);
						
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("thread_id", json.getString("thread_id"));
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 短信回话查询接口
	 * @return
	 */
	public String sms_query_thread(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<Threads> smsList = peopleService.findBypeopleId(json.getString("uid"));
				JSONArray jsarray = new JSONArray();
				if(smsList !=null){
					for(int i=0;i<smsList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("id", smsList.get(i).getId());
						jsObj.put("thread_id", smsList.get(i).getThread_id());
						jsObj.put("persons", smsList.get(i).getIds());
						jsObj.put("time", smsList.get(i).getThread_date());
						jsObj.put("snippet", smsList.get(i).getSnippet());
						jsObj.put("count", smsList.get(i).getMessage_count());
						jsObj.put("read", smsList.get(i).getIsread());
						jsObj.put("type", smsList.get(i).getType());
						jsarray.put(jsObj);
						
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 短信回话删除接口
	 * @return
	 */
	public String sms_delete_thread(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				JSONArray arr = json.getJSONArray("ids");
				for(int j=0;j<arr.length();j++){
					Threads smsList = peopleService.findBythreadid(Integer.parseInt(arr.getJSONObject(j).getString("id")));
					if(smsList !=null && smsList.getThread_id() !=null){
						List<Sms_backup> listsms = peopleService.findBythread_id(smsList.getThread_id());
						for(int i=0;i<listsms.size();i++){
							peopleService.delSms(listsms.get(i));
						}
						peopleService.delThreads(smsList);
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 软件存在查询接口
	 * @return
	 */
	public String app_query_exist(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				
				JSONArray arr = json.getJSONArray("app");
				JSONArray jsarray = new JSONArray();
				if(arr !=null){
					for(int i=0;i<arr.length();i++){
						JSONObject jsObj = new JSONObject();
						AppInstallPack app = app_backupService.findMaxAppByPname(arr.getJSONObject(i).getString("pname"));
						jsObj.put("pname", arr.getJSONObject(i).getString("pname"));
						String exist = "0";
						if(app !=null){
							exist = "1";
						}else{
							App_backup app_back = app_backupService.findAppByUPV(json.getString("uid"),arr.getJSONObject(i).getString("pname"),"");
							if(app_back !=null){
								exist = "1";
							}else{
								exist = "0";
							}
						}
						jsObj.put("exist",exist);
						jsarray.put(jsObj);
						
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 软件名备份接口
	 * @return
	 */
	public String app_backup_name(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json1 = new JSONObject(s);
//				JSONArray array = new JSONArray(json1.getString("app"));
				JSONArray json = json1.getJSONArray("app");
				for(int i=0;i<json.length();i++){
					App_backup app =  app_backupService.findapp_backup(json.getJSONObject(i).getString("uid"), json.getJSONObject(i).getString("pname"), json.getJSONObject(i).getString("version"));
					if(app !=null){
						app.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						app_backupService.saveOrUpdateApp_backup(app);
					}else{
						app = new App_backup();
						app.setApp_version(json.getJSONObject(i).getString("version"));
						app.setAppname(json.getJSONObject(i).getString("name"));
						app.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
						app.setPackname(json.getJSONObject(i).getString("pname"));
						app.setPeopleId(json.getJSONObject(i).getString("uid"));
						app.setPhoneno(json.getJSONObject(i).getString("number"));
						app.setSize(Float.parseFloat(json.getJSONObject(i).getString("size")));
						app.setVersioncode(Integer.parseInt(json.getJSONObject(i).getString("versioncode")));
						app_backupService.saveOrUpdateApp_backup(app);
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	/**
	 * 软件体备份接口
	 * @return
	 */
	public String app_backup(){
		JSONObject peopleJson = new JSONObject();
		try {
			
//			boolean s = appAssemblier.receivePkgname("cp09", "os1", pname);
			App_backup app =  app_backupService.findapp_backup(uid, pname, version);
			if(app !=null){
				app.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				app_backupService.saveOrUpdateApp_backup(app);
			}else{
				app = new App_backup();
				app.setApp_version(version);
				app.setAppname(name);
				app.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				app.setPackname(pname);
				app.setPeopleId(uid);
				app.setPhoneno(number);
				logger.error("软件体 " + appFileFileName +" : " +appFile);
				String fileurl = peopleService.writeFile(appFileFileName, appFile, FileType.软件);
				app.setFile_url(fileurl);
				app.setIcon_url(picFileFileName);
				app.setSize(size);
				app.setVersioncode(versioncode);
				app_backupService.saveOrUpdateApp_backup(app);
			}
			peopleJson.put("result", "20000");
			peopleJson.put("desc", "成功");
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 备份软件查询接口
	 * @return
	 */
	public String app_query(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<App_backup> smsList = app_backupService.findAppByPeopleId(json.getString("uid"));
				JSONArray jsarray = new JSONArray();
				if(smsList !=null){
					
					for(int i=0;i<smsList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("id", smsList.get(i).getId());
						jsObj.put("name", smsList.get(i).getAppname());
						jsObj.put("pname", smsList.get(i).getPackname());
						jsObj.put("version", smsList.get(i).getApp_version());
						jsObj.put("versioncode", smsList.get(i).getVersioncode());
						jsObj.put("size", smsList.get(i).getSize());
						if(smsList.get(i).getFile_url() !=null){
							jsObj.put("url", config.getFileServerUrl() + smsList.get(i).getFile_url());
						}else{
							AppInstallPack app = app_backupService.findAppByPnameAndVer(smsList.get(i).getPackname(),smsList.get(i).getVersioncode());
							String packurl = "";
							if(app !=null){
								packurl = appQuerier.getPackUrl(app.getUuid());
							}else{
								AppInstallPack app1 = app_backupService.findMaxAppByPname(smsList.get(i).getPackname());
								if(app1 !=null){
									packurl = appQuerier.getPackUrl(app1.getUuid());
								}else{
									App_backup a = app_backupService.findAppByPe(json.getString("uid"), smsList.get(i).getPackname());
									if(a !=null){
										packurl = config.getFileServerUrl() + a.getFile_url();
									}
								}
							}
							jsObj.put("url", packurl);
						}
						if(smsList.get(i).getIcon_url() !=null){
							jsObj.put("url_icon",  smsList.get(i).getIcon_url());
						}else{
							jsObj.put("url_icon", config.getFileServerUrl() + app_backupService.app_backupIcon(smsList.get(i).getPackname()));
						}
						jsarray.put(jsObj);
						
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 软件备份删除接口
	 * @return
	 */
	public String app_delete(){
		JSONObject peopleJson = new JSONObject();

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				App_backup app = app_backupService.findById(Integer.parseInt(json.getString("id")));
				
				if(app.getFile_url() !=null){
					File file = new File(config.getFileServerDir() + app.getFile_url());
					if(file.exists()){
						file.delete();
					}
				}
				if(app.getIcon_url() !=null){
					File file = new File(config.getFileServerDir() + app.getIcon_url());
					if(file.exists()){
						file.delete();
					}
				}
				app_backupService.delApp_backup(app);
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 图片备份接口
	 * @return
	 */
	public String image_backup(){
		JSONObject peopleJson = new JSONObject();
		System.out.println(name);
		try {
			Image_backup image = app_backupService.findImageByPidAndName(uid, name);
			if(image !=null){
				image.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				app_backupService.saveOrUpdateApp_backup(image);
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "已备份");
			}else{
				image = new Image_backup();
				
				image.setImagename(name);
				image.setBackup_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				image.setCreate_date(time);
				image.setPeopleId(uid);
				image.setPhoneno(number);
				String fileurl = peopleService.writeFile(contentFileName, content, FileType.图片);
				image.setImage_url(fileurl);
				String icon_url = peopleService.writeIcon(contentFileName, content);//stringtoIcon
//				String icon_url = peopleService.stringtoIcon(icon);//
				image.setIcon_url(icon_url);
				image.setSize(size);
				app_backupService.saveOrUpdateApp_backup(image);
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	/**
	 * 图片查询接口
	 * @return
	 */
	public String image_query(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<Image_backup> smsList = app_backupService.findImageByPeopleId(json.getString("uid"));
				JSONArray jsarray = new JSONArray();
				if(smsList !=null){
					for(int i=0;i<smsList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("id", smsList.get(i).getId());
						jsObj.put("name", smsList.get(i).getImagename());
						jsObj.put("size", smsList.get(i).getSize());
						jsObj.put("time", smsList.get(i).getCreate_date());
						jsObj.put("backup_time", smsList.get(i).getBackup_date());
						jsObj.put("url_icon",  config.getFileServerUrl() + smsList.get(i).getIcon_url());
						jsObj.put("url", config.getFileServerUrl() + smsList.get(i).getImage_url());
						jsarray.put(jsObj);
					}
				}	
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}		
	
	/**
	 * 图片删除接口
	 * @return
	 */
	public String image_delete(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				JSONArray arr = json.getJSONArray("ids");
				for(int j=0;j<arr.length();j++){
					Image_backup image = app_backupService.findimageById(Integer.parseInt(arr.getJSONObject(j).getString("id")),json.getString("uid"));
					if(image !=null){
						if(image.getImage_url() !=null){
							File file = new File(config.getFileServerDir() + image.getImage_url());
							if(file.exists()){
								file.delete();
							}
						}
						if(image.getIcon_url() !=null){
							File file = new File(config.getFileServerDir() + image.getIcon_url());
							if(file.exists()){
								file.delete();
							}
						}
						app_backupService.delImage_backup(image);
					}
				}
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	
	
	/**
	 * 评论接口
	 * @return
	 */
	public String appcomment(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
//				if(json.getString("content").length()<=200){
//					json.getString("content").contains("");
					App_Comment app = new App_Comment();
					app.setApp_uuid(json.getString("uuid"));
					app.setComment_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
					app.setContent(json.getString("content"));
					app.setNickname(json.getString("nickname"));
					app.setPeopleId(json.getString("peopleId"));
					app.setPhoneno(json.getString("phoneno"));
					app.setRating(Float.parseFloat(json.getString("rating")));
					
					//int point = peopleService.findByAppUuid(json.getString("uuid"));
					App a = peopleService.findByuuid(json.getString("uuid"));
					if(a !=null){
						int point =1;
						if(a.getPoint() !=null){
							point  = Integer.parseInt(a.getPoint());
						}else{
							point = 1;
						}
						int rating = (int)Float.parseFloat(json.getString("rating"));
						int count = peopleService.findByPidAndUuid(json.getString("uuid"));
						int newpoint = (point*count+rating)/(count+1);
						a.setPoint(newpoint+"");
						peopleService.saveOrUpdateApp(a);
					}
					peopleService.saveOrUpdateApp_Comment(app);
					peopleJson.put("result", "20000");
					peopleJson.put("desc", "成功");
//				}else{
//					peopleJson.put("result", "20000");
//					peopleJson.put("desc", "评论不能太长");
//				}
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 查看评论接口
	 * @return
	 */
	public String searchcomment(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<App_Comment> appList = peopleService.findByPidAndUuid(json.getString("uuid"),Integer.parseInt(json.getString("index")),Integer.parseInt(json.getString("size")));
				JSONArray jsarray = new JSONArray();
				if(appList !=null){
					for(int i=0;i<appList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("uuid", appList.get(i).getApp_uuid());
						jsObj.put("content", appList.get(i).getContent());
						jsObj.put("size", appList.size());
						jsObj.put("time", appList.get(i).getComment_date());
						jsObj.put("peopleId",  appList.get(i).getPeopleId());
						jsObj.put("phoneno", appList.get(i).getPhoneno());
						jsObj.put("nickname", appList.get(i).getNickname());
						jsObj.put("rating", appList.get(i).getRating());
						PeopleInfo p = peopleService.findByPeopleID(appList.get(i).getPeopleId());
						if(p!=null && p.getImg() !=null && !"".equals(p.getImg())){
							jsObj.put("icon", config.getFileServerUrl() +  p.getImg());
						}else{
							jsObj.put("icon", "");
						}
						jsarray.put(jsObj);
					}
				}	
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	/**
	 * 猜你喜欢接口
	 * @return
	 */
	public String guessyoulike(){
		JSONObject peopleJson = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(ServletActionContext.getRequest()
							.getInputStream(), "UTF-8"));
			String s = br.readLine();
			if(s != null){
				JSONObject json = new JSONObject(s);
				List<Guessyoulike> appList = peopleService.findByPid(json.getString("phone"), Integer.parseInt(json.getString("index")), Integer.parseInt(json.getString("size")));
				JSONArray jsarray = new JSONArray();
				if(appList !=null && appList.size()>0){
					for(int i=0;i<appList.size();i++){
						JSONObject jsObj = new JSONObject();
						jsObj.put("uuid", appList.get(i).getApp_uuid());
						jsObj.put("phoneno", appList.get(i).getPhoneno());
						jsObj.put("appname", appList.get(i).getAppname());
						jsObj.put("icon",config.getFileServerUrl() +  appList.get(i).getIcon());
						jsObj.put("peopleId", appList.get(i).getPeopleId());
						jsObj.put("size", appList.get(i).getFile_size());
						jsObj.put("friends_count", appList.get(i).getFriends_count());
						jsObj.put("version", appList.get(i).getVersions());
						String url = appQuerier.getPackUrl(appList.get(i).getApp_uuid());
						if(url!=null){
							jsObj.put("downurl", url);
						}else{
							jsObj.put("downurl", "");
						}
						jsarray.put(jsObj);
					}
				}else{
					List<Guessyoulike> appList1 = peopleService.findByPidNoBackup(Integer.parseInt(json.getString("index")), Integer.parseInt(json.getString("size")));
					if(appList1 !=null){
						for(int i=0;i<appList1.size();i++){
							JSONObject jsObj = new JSONObject();
							jsObj.put("uuid", appList1.get(i).getApp_uuid());
							jsObj.put("appname", appList1.get(i).getAppname());
							jsObj.put("icon", config.getFileServerUrl() + appList1.get(i).getIcon());
							jsObj.put("file_size", appList1.get(i).getFile_size());
							jsObj.put("friends_count", appList1.get(i).getFriends_count());
							jsObj.put("version", appList1.get(i).getVersions());
							jsObj.put("phoneno", "");
							jsObj.put("peopleId", "");
							String url = appQuerier.getPackUrl(appList1.get(i).getApp_uuid());
							if(url!=null){
								jsObj.put("downurl", url);
							}else{
								jsObj.put("downurl", "");
							}
							jsarray.put(jsObj);
						}
					}
				}	
				peopleJson.put("result", "20000");
				peopleJson.put("desc", "成功");
				peopleJson.put("data", jsarray);
			}
		}catch(Exception e){
			e.printStackTrace();
			try {
				logger.error(e);
				peopleJson.put("result", "20400");
				peopleJson.put("desc", "系统错误" + e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	
	
	
	/***************************************************************************
	 * 以下是访问器
	 **************************************************************************/
	
	public PeopleInfo getPeople() {
		return people;
	}
	public void setPeople(PeopleInfo people) {
		this.people = people;
	}
	public void setPeopleService(PeopleService peopleService) {
		this.peopleService = peopleService;
	}
	public void setSmsHessianService(SmsHessianService smsHessianService) {
		this.smsHessianService = smsHessianService;
	}
	public ClientConfigInfo getClient() {
		return client;
	}
	public void setClient(ClientConfigInfo client) {
		this.client = client;
	}

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public String getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVcfVersion() {
		return vcfVersion;
	}

	public void setVcfVersion(String vcfVersion) {
		this.vcfVersion = vcfVersion;
	}


	public File getVcardFile() {
		return vcardFile;
	}

	public void setVcardFile(File vcardFile) {
		this.vcardFile = vcardFile;
	}

	public String getVcardFileFileName() {
		return vcardFileFileName;
	}

	public void setVcardFileFileName(String vcardFileFileName) {
		this.vcardFileFileName = vcardFileFileName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public AppQuerier getAppQuerier() {
		return appQuerier;
	}

	public void setAppQuerier(AppQuerier appQuerier) {
		this.appQuerier = appQuerier;
	}

	public File getAppFile() {
		return appFile;
	}

	public void setAppFile(File appFile) {
		this.appFile = appFile;
	}

	public String getAppFileFileName() {
		return appFileFileName;
	}

	public void setAppFileFileName(String appFileFileName) {
		this.appFileFileName = appFileFileName;
	}

	public File getPicFile() {
		return picFile;
	}

	public void setPicFile(File picFile) {
		this.picFile = picFile;
	}

	public String getPicFileFileName() {
		return picFileFileName;
	}

	public void setPicFileFileName(String picFileFileName) {
		this.picFileFileName = picFileFileName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getVersioncode() {
		return versioncode;
	}

	public void setVersioncode(int versioncode) {
		this.versioncode = versioncode;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getSize() {
		return size;
	}

	public void setSize(Float size) {
		this.size = size;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public File getContent() {
		return content;
	}
	public void setContent(File content) {
		this.content = content;
	}
	public String getContentFileName() {
		return contentFileName;
	}
	public void setContentFileName(String contentFileName) {
		this.contentFileName = contentFileName;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}

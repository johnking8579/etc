package com.i8app.install.web.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.install.Config;
import com.i8app.install.Globals;
import com.i8app.install.Pager;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.EnterpriseInfo;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.PrivilegeInfo;
import com.i8app.install.domain.Subscription;
import com.i8app.install.domain.Tag;
import com.i8app.install.service.EmployeeService;
import com.i8app.install.service.MobileClickService;
import com.i8app.install.service.PeopleService;

@SuppressWarnings("serial")
@Controller("mobileClickAction")
@Scope("prototype")
public class MobileClickAction extends BaseAction {
	private static Logger logger = Logger.getLogger(MobileClickAction.class);
	@Resource
	public MobileClickService mobileClickService;
	@Resource
	public PeopleService peopleService;
	@Resource
	public Config config;
	@Resource
	public EmployeeService employeeService;
	
	public String pass,empNo;
	public int provId;
	/**
	 * 订阅企业服务号
	 * @return
	 */
	public String saveSubscription(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					int count = mobileClickService.findByPersion(Integer.parseInt(jsnb.getString("eid")),jsnb.getString("pid"));
					if(count ==0){
						Subscription sub = new Subscription();
						EnterpriseInfo e = mobileClickService.findEnterpriseInfoById(Integer.parseInt(jsnb.getString("eid")));
						PeopleInfo p =  peopleService.findByPeopleID(jsnb.getString("pid"));
						if(e !=null && p!=null){
							sub.setEid(mobileClickService.findEnterpriseInfoById(Integer.parseInt(jsnb.getString("eid"))));
							sub.setPeopleID(peopleService.findByPeopleID(jsnb.getString("pid")));
							sub.setTakeTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							mobileClickService.addOrUpdateSub(sub);
							System.out.println(sub.getId());
							errorCode=20000;
							desc="已经成功";
							peopleJson.put("id", sub.getId());
						}else{
							errorCode=20400;
							desc="无法订阅";
						}
					}else{
						errorCode=20000;
						desc="已经订阅";
					}
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="无法订阅";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
//		this.setMessage(peopleJson.toString());
		return this.doPrint(peopleJson.toString());
	}
	
	
	public String queryEnterprise(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					Pager<EnterpriseInfo> count = mobileClickService.pagerlist(jsnb.getString("name"),Integer.parseInt(jsnb.getString("index")),Integer.parseInt(jsnb.getString("size")));
					if(count.getTotal() !=0){
						JSONArray array = new JSONArray();
						for(EnterpriseInfo e : count.getList()){
							JSONObject json = new JSONObject();
							json.put("eid", e.getId());
							json.put("enterprisename", e.getEnterpriseName());
							json.put("nickname", e.getNickname());
							if(e.getImg() !=null && !"".equals(e.getImg())){
								json.put("img", config.getFileServerUrl() + e.getImg());
							}else{
								json.put("img", "");
							}
							json.put("status", e.getStatus());
							json.put("intro", e.getIntro());
							array.put(json);
						}
						peopleJson.put("data", array);
						errorCode=20000;
						desc="查询成功";
					}else{
						errorCode=20001;
						desc="暂时无该企业服务";
					}
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="无企业服务";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
//		this.setMessage(peopleJson.toString());
		return this.doPrint(peopleJson.toString());
	}
	
	/**
	 * 查询已经订阅企业服务号
	 * @return
	 */
	public String querySubscription(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					Pager<Subscription> count = mobileClickService.pagerSubscribedlist(jsnb.getString("pid"),Integer.parseInt(jsnb.getString("index")),Integer.parseInt(jsnb.getString("size")));
					if(count.getTotal() !=0){
						JSONArray array = new JSONArray();
						for(Subscription e : count.getList()){
							JSONObject json = new JSONObject();
							json.put("id", e.getId());
							json.put("eid", e.getEid().getId());
							json.put("peopleId", e.getPeopleID().getId());
							json.put("taketime", e.getTakeTime());
							json.put("enterprisename", e.getEid().getEnterpriseName());
							json.put("nickname", e.getEid().getNickname());
							if(e.getEid() != null && !"".equals(e.getEid()) && e.getEid().getImg() != null && !"".equals(e.getEid().getImg())){
							json.put("img", config.getFileServerUrl() + e.getEid().getImg());
							}else{
								json.put("img", "");
							}
							json.put("status", e.getEid().getStatus());
							json.put("intro", e.getEid().getIntro());
							array.put(json);
						}
						peopleJson.put("data", array);
						errorCode=20000;
						desc="查询成功";
					}else{
						errorCode=20001;
						desc="暂时没有订阅企业服务";
					}
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="无订阅";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
//		this.setMessage(peopleJson.toString());
		return this.doPrint(peopleJson.toString());
	}
	/**
	 * 删除订阅服务接口
	 * @return
	 */
	public String delSubscription(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					Subscription sub = mobileClickService.findByPidAndEid(Integer.parseInt(jsnb.getString("eid")),jsnb.getString("pid"));
					if(sub !=null){
						mobileClickService.delSubscription(sub.getId());
						errorCode=20000;
						desc="删除成功";
					}else{
						errorCode=20000;
						desc="该服务号无订阅";
					}
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="删除失败";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
//		this.setMessage(peopleJson.toString());
		return this.doPrint(peopleJson.toString());
	}
	public String findEnterpriseInfo(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					EnterpriseInfo count = mobileClickService.findEnterpriseInfoById(Integer.parseInt(jsnb.getString("eid")));
					if(count !=null){
							JSONObject json = new JSONObject();
							json.put("eid", count.getId());
							json.put("enterpriseName", count.getEnterpriseName());
							json.put("nickname", count.getNickname());
							if(count.getImg() !=null && !"".equals(count.getImg())){
								json.put("img", config.getFileServerUrl() + count.getImg());
							}else{
								json.put("img", "");
							}
							json.put("status", count.getStatus());
							json.put("intro", count.getIntro());
						peopleJson.put("data", json);
						errorCode=20000;
						desc="查询成功";
					}else{
						errorCode=20001;
						desc="暂时无该企业服务";
					}
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="无企业服务";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
	
//		this.setMessage(peopleJson.toString());
		return this.doPrint(peopleJson.toString());
	}
	public String findtag(){
		JSONObject peopleJson = new JSONObject();
		int errorCode = 0;
		String desc = "";
		try {
			BufferedReader br = new BufferedReader(
						new InputStreamReader(ServletActionContext.getRequest()
								.getInputStream(), "UTF-8"));
				String s = br.readLine();
				if(s !=null){
					JSONObject jsnb = new JSONObject(s);
					Pager<Tag> sub = mobileClickService.pagerTaglist(Integer.parseInt(jsnb.getString("index")),Integer.parseInt(jsnb.getString("size")));
					JSONArray array = new JSONArray();
					for(Tag e : sub.getList()){
						JSONObject json = new JSONObject();
						json.put("id", e.getId());
						json.put("tagname", e.getTagname());
						json.put("tagDesc", e.getTagDesc());
						array.put(json);
					}
					peopleJson.put("data", array);
					errorCode=20000;
					desc="查询成功";
				}else{
					errorCode=20002;
					desc="参数不符合要求";
				}
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", desc);
		}catch(Exception e){
			errorCode=20400;
			desc="删除失败";
			try {
				peopleJson.put("result", errorCode);
				peopleJson.put("desc", e.toString());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		return this.doPrint(peopleJson.toString());
	}
	
	public String index(){
		return SUCCESS;
	}
	public String login(){
		return SUCCESS;
	}
	public String loginto(){
		try{
			List<Employee> empList = employeeService.findEmpListByEmpnoAndProvidLogin(empNo, provId);
			if(empList !=null && !"".equals(empList) && empList.size()>0){
				if(empList.size()==1){
					if(pass !=null){
						if(empList.get(0).getIsDeleted()==1){
							this.setMessage("抱歉，此工号已锁定，不能登陆");
						}else if(empList.get(0).getIsFreezed()==1){
							this.setMessage("抱歉，此工号已冻结，不能登陆");
	//					}else if(!empList.get(0).getEmpPwd().equalsIgnoreCase(new MD5().toMD5(pass))){
						}else if(!empList.get(0).getEmpPwd().equalsIgnoreCase(pass)){						
							this.setMessage("抱歉，密码错误");
						}else{
							List<PrivilegeInfo> privilegeList = employeeService.findPrivilegeByEmpId(empList.get(0).getEmpID());
							if(privilegeList.size()>0){
								getSession().setAttribute(Globals.CURRENT_USER, empList.get(0));
								getSession().setAttribute(Globals.URL_SET, privilegeList);
								this.setMessage("0");
							}else{
								this.setMessage("抱歉！，您无操作权限");
							}
						}
					}else{
						this.setMessage("抱歉！，密码不能为空");
					}
				}else{
					this.setMessage("抱歉，此工号存在多个，请联系管理员");
				}
				
			}else{
				this.setMessage("抱歉！，无此工号或者省份不正确");
			}
		}catch(Exception e){
			logger.error(e.getMessage());
			this.setMessage("系统错误");
		}
		return AJAX;
	}
	public String top(){
		return SUCCESS;
	}
	public String left(){
		return SUCCESS;
	}
	public String mainfra(){
		return SUCCESS;
	}
	public String bottom(){
		return SUCCESS;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public int getProvId() {
		return provId;
	}
	public void setProvId(int provId) {
		this.provId = provId;
	}
}

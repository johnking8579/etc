package com.i8app.install.web;

import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

@SuppressWarnings("serial")
public class SecurityInterceptor implements Interceptor {

	/**
	 * 过滤参数中的特殊字符, 不能过滤,;-
	 */
	String sensitiveInput = "[<>&$%@'|()\\+\"]";

	/**
	 * 过滤URL地址中的特殊字符, 不要过滤.
	 */
	String sensitiveUrl = "[%<>]";

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		HttpServletRequest req = (HttpServletRequest) invocation
				.getInvocationContext().get(StrutsStatics.HTTP_REQUEST);
		Matcher m = Pattern.compile(sensitiveUrl).matcher(
				req.getRequestURL().toString());
		if (m.find())
			return Action.ERROR;

		Map<String, Object> map = invocation.getInvocationContext()
				.getParameters();
		for (Entry e : map.entrySet()) {
			String[] ss = (String[]) e.getValue();
			for (int i = 0; i < ss.length; i++) {
				ss[i] = ss[i].replaceAll(sensitiveInput, "");
			}
		}
		invocation.getInvocationContext().setParameters(map);
		return invocation.invoke();
	}

}

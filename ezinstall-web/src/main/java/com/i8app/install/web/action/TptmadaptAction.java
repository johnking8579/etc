package com.i8app.install.web.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.install.domain.Tptmadapt;
import com.i8app.install.service.TptmadaptService;

@SuppressWarnings("serial")
@Controller("tptmadaptAction")
@Scope("prototype")
public class TptmadaptAction extends BaseAction {
	@Resource
	public TptmadaptService tptmadaptService;
	private int proid;
	private String empno,phoneno;
	/**
	 * http://localhost:8080/wap-phone/tptmadapt/tptmadapt.action?proid=41&empno=snld&phoneno=15901031592
	 * @return
	 */
	public String tptmadapt(){
		int error = 0;
		String desc ="修改成功";
		try{
			Tptmadapt tpt = tptmadaptService.findByPEP(proid, empno, phoneno);
			if(tpt !=null){
				tpt.setUpdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				tptmadaptService.addOrUpdateTptmadapt(tpt);
			}else{
				tpt = new Tptmadapt();
				tpt.setEmpno(empno);
				tpt.setPhonenumber(phoneno);
				tpt.setProvinceid(proid);
				tpt.setUpdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				tptmadaptService.addOrUpdateTptmadapt(tpt);
				desc ="添加成功";
			}
		}catch(Exception e){
			error=1;
			desc ="添加修改失败";
		}
		this.doPrint("{\"error\":\""+error+"\",\"desc\":\""+desc+"\"}");
		return null;
	}
	public int getProid() {
		return proid;
	}
	public void setProid(int proid) {
		this.proid = proid;
	}
	public String getEmpno() {
		return empno;
	}
	public void setEmpno(String empno) {
		this.empno = empno;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
}

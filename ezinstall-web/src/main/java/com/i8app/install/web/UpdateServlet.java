package com.i8app.install.web;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.JsonObject;
import com.i8app.install.InvalidOsVersionException;
import com.i8app.install.PhoneOs;
import com.i8app.install.domain.LingDongClient;
import com.i8app.install.service.LingDongClientService;

/**
 * 从UpdateCenter中迁过来的类.
 * 为了和前工程一致, 本类也使用了servlet, 并且把原参数做了下适配处理
 */
@SuppressWarnings("serial")
public class UpdateServlet extends HttpServlet  {
 
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			/*throws ServletException, IOException*/ {
		try{
		int softId = Integer.parseInt(request.getParameter("softId"));
		int osType = Integer.parseInt(request.getParameter("osType"));
//		int subType = Integer.parseInt(request.getParameter("subType"));
		String osVersion = request.getParameter("osVersion");
		
		//把osType转换成Phone.getInstance()所需的参数
		int paramOsType;
		try {
			if(osType == 1)	{
				paramOsType = 31;
			} else if(osType == 2)	{
				paramOsType = 2;
			} else
				throw new InvalidOsVersionException("osType值:1iphone,2android");
			PhoneOs os = PhoneOs.getInstance(paramOsType, osVersion);
			
			//获取ioc容器
			WebApplicationContext w = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
			LingDongClientService service = w.getBean(LingDongClientService.class);
			
			List<LingDongClient> list = service.find(softId, 0, 0, os.getOsid());
			LingDongClient l = service.latestVersion(list);
			if(l == null)	{
				doPrint(response, "null");
			} else	{
				JsonObject json = new JsonObject();
				json.addProperty("softVersion", l.getSoftVersion());
				json.addProperty("url", l.getUrl());
				json.addProperty("memo", l.getMemo());
				System.out.println("打印版本号");
				json.addProperty("versionCode",l.getVersionCode());
				doPrint(response, json.toString());
			}
		} catch (InvalidOsVersionException e) {
			e.printStackTrace();
			doPrint(response, e.getMessage());
		}
		}
		catch(Exception e1){
			System.out.println("走不走");
			e1.printStackTrace();
		}

			
	}
 
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
	
	private void doPrint(ServletResponse response, Object obj)	{
		response.setContentType("text/html;charset=utf-8");
		PrintWriter pw;
		try {
			pw = response.getWriter();
			pw.print(obj.toString());	//不要用println,防止请求端接收到/r/n等换行符
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

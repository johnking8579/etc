package com.i8app.install.web.action;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.i8app.install.domain.ShortUrl;
import com.i8app.install.service.SimpleService;

/**
 * 短链解析
 * 
 * @author JingYing 2013-6-7
 */
@Controller
@Scope("prototype")
public class UrlAction extends BaseAction{
	
	private static Logger logger = Logger.getLogger(UrlAction.class);
	
	@Resource
	private SimpleService simpleService;
	private String location;
	
	/**
	 * 获取短链对应的长链, 在struts.xml里进行客户端跳转
	 * @return
	 */
	public String parse()	{
		String uri = getRequest().getRequestURI();
		String code = uri.substring(uri.lastIndexOf("/")+1);
		if(code.length() == 6)	{
			ShortUrl su = simpleService.findShortUrl(code);
			if(su == null)	return "404";
			location = su.getLongUrl();
			return SUCCESS;		//struts.xml里进行redirect
		} else	{
			logger.error(uri);
			return null;
		}
	}
	
	
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}

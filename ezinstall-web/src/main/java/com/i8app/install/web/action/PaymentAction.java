package com.i8app.install.web.action;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.i8app.install.Globals;
import com.i8app.install.Pager;
import com.i8app.install.domain.CommisionWithdrawal;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Requisition;
import com.i8app.install.service.CommisionHessionService;
import com.i8app.install.service.PaymentService;
import com.i8app.install.service.SmsHessianService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
public class PaymentAction extends BaseAction{
	
	public static final Logger logger = Logger.getLogger(PaymentAction.class);

	@Resource
	private PaymentService paymentService;
	@Resource
	private SmsHessianService SmsHessianService;
	@Resource
	private CommisionHessionService commisionHessionService;

	private String empNo,pass,year,month,phoneNumber,autocode,money;
	private int pro,pageNo;
	private Requisition req;
	private CommisionWithdrawal cw;


	public String notifyUrl(){
		return SUCCESS;
	}
	public String index(){
		return SUCCESS;
	}
	public String verification(){
		List<Employee> emplist = paymentService.findByEmpno(empNo);
		int flag =0;
		if(emplist !=null && emplist.size()>0){
			if(emplist.size()==1){
				flag=2;
			}else{
				flag=3;
			}
		}else{
			flag=1;
		}
		String result="{\"flag\":\""+flag+"\"}";
		this.setMessage(result);
		return AJAX;
	}
	public void loginout(){
		ServletActionContext.getRequest().getSession().removeAttribute(Globals.CURRENT_USER);
		ServletActionContext.getRequest().getSession().removeAttribute("req");
		ServletActionContext.getRequest().getSession().removeAttribute("authcode");
		this.doPrint("1");
	}
	public String login(){
		int flag =0;
		Employee emp = null;
		if(pro !=0){
			emp = paymentService.findByEmpno(empNo,pro);
			if(emp!=null){
				if(pass !=null){
					if(emp.getEmpPwd().equalsIgnoreCase(pass)){
						req = paymentService.findByPersonnum(emp.getEmpID());
						if(req != null){
							flag=4;//密码正确
							ServletActionContext.getRequest().getSession().setAttribute(Globals.CURRENT_USER, emp);
							ServletActionContext.getRequest().getSession().setAttribute("req", req);
						}else{
							flag=5;
							ServletActionContext.getRequest().getSession().setAttribute(Globals.CURRENT_USER, emp);
						}
					}else{
						flag=3;//密码错误
					}
				}else{
					flag=2;//密码不能为空
				}
			}else{
				flag=1;//工号不存在
			}
		}else{
			emp = paymentService.findByEmp(empNo);
			if(emp!=null){
				if(pass !=null){
					if(emp.getEmpPwd().equalsIgnoreCase(pass)){
						if(emp.getIsDeleted()==1){
							flag = 6;//帐号状态为删除
						}else if(emp.getIsFreezed() == 1){
							flag = 7;//帐号状态为冻结
						}else{
							req = paymentService.findByPersonnum(emp.getEmpID());
							if(req != null){
								flag=4;//密码正确
								ServletActionContext.getRequest().getSession().setAttribute(Globals.CURRENT_USER, emp);
								ServletActionContext.getRequest().getSession().setAttribute("req", req);
							}else{
								ServletActionContext.getRequest().getSession().setAttribute(Globals.CURRENT_USER, emp);
								flag=5;
							}
						}
					}else{
						flag=3;//密码错误
					}
				}else{
					flag=2;//密码不能为空
				}
			}else{
				flag=1;//工号不存在
			}
		}
		String result="{\"flag\":"+flag+"}";
		this.setMessage(result);
		return AJAX;
	}
	public String exchangeBonus(){
		Employee emp = (Employee)ServletActionContext.getRequest().getSession()
			.getAttribute(Globals.CURRENT_USER);
		req = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
//		CommisionBalance commisionBalance = paymentService.findBalanceByEmpId(emp.getEmpID());
//		req = paymentService.findByPersion(emp.getEmpID());
//		money = commisionBalance==null?0:commisionBalance.getBalance().intValue();
//		pro = commisionBalance==null?0:commisionBalance.getStatus();
		
		/*页面余额，冻结金额，帐号状态*/
		String json = commisionHessionService.getBalance(emp.getEmpID());
		JsonElement je = new JsonParser().parse(json);
		if(je.isJsonObject()){
			JsonObject jo = je.getAsJsonObject();
			money = jo.get("money").getAsString();
			pro = jo.get("status").getAsInt();
			month = jo.get("freeze").getAsString();
		}
		if(req.getApproveStatus()==0){
			pro = 3;
		}
		
		return SUCCESS;
	}
	public String exchangeQuery(){
		return SUCCESS;
	}
	
	public String queryRequst(){
		Employee emp = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
		Pager<CommisionWithdrawal> pager = paymentService.pagerlist(emp.getEmpID(), year, month, pageNo, 2);
//		Pager<CommisionAccount> pager = paymentService.pagerlist(emp.getEmpID(), year, month, pageNo, 2);		
//		pager.setList(CommisionHessionService.listAccounts(emp.getEmpID()));
		//分页，计算出总页数
		int pageSize = (pager.getTotal()+2-1)/2;
		StringBuffer sbf = new StringBuffer();
		if(pager.getTotal()!=0){
			sbf.append("<table width=\"96%\" class=\"hovertable\" >");
			sbf.append("<tr>");
			sbf.append("<th><span class=\"STYLE26\">序号</span></th>");
			sbf.append("<th><span class=\"STYLE26\">提交时间</span></th>");
			sbf.append("<th><span class=\"STYLE26\">操作金额</span></th>");
			sbf.append("<th><span class=\"STYLE26\">手续费</span></th>");
			sbf.append("<th><span class=\"STYLE26\">个人所得税</span></th>");
			sbf.append("<th><span class=\"STYLE26\">备注</span></th>");
			sbf.append("</tr>");
			for(int i = 0;i<pager.getList().size();i++){
				sbf.append("<tr onMouseOver=\"this.style.backgroundColor='#E1E1E1';\" " +
						"onMouseOut=\"this.style.backgroundColor='#FFFFFF';\">");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
						/*pager.getList().get(i).getId()*/
						i+1
						+"</div></td>");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
						pager.getList().get(i).getApplyTime().toString().substring(0, 16)+"</div></td>");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
						pager.getList().get(i).getApply_amount()+"</div></td>");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
						pager.getList().get(i).getApply_amount_fee()+"</div></td>");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
					pager.getList().get(i).getApply_amount_tax()+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
//						(pager.getList().get(i).getBookkeepingTime()==null?"????"
//								:pager.getList().get(i).getBookkeepingTime().toString().substring(0, 16))+"</div></td>");
				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+
						(getBookkeepingStatusNotes(pager.getList().get(i).getBookkeepingStatus()))+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+pager.getList().get(i).getOriginalAmount().intValue()+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+pager.getList().get(i).getOperAmount().intValue()+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+pager.getList().get(i).getRemainAmount().intValue()+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+pager.getList().get(i).getFinancialTime().toString().substring(0, 16)+"</div></td>");
//				sbf.append("<td><div align=\"center\" class=\"STYLE27\">"+pager.getList().get(i).getSummary()+"</div></td>");
				sbf.append("</tr>");
			}
	         sbf.append("</table>");
	         sbf.append("<input type=\"hidden\" id=\"pageNo\" name=\"pageNo\" value=\""+pageNo+"\"/>");
	         sbf.append("<input type=\"hidden\" id=\"pageSize\" name=\"pageSize\" value=\""+pageSize+"\"/>");
		}else{
			 sbf.append("<div style=\"LINE-HEIGHT: 56px; HEIGHT: 56px\" class=\"ztd1\">");
	         sbf.append("<center>");
	         sbf.append("<font size=\"2\" face=\"Helvetica\"> ");
	         sbf.append("&nbsp;&nbsp;<span class=\"STYLE30\">暂时无记录</span></font>");
	         sbf.append("</center>");
	         sbf.append("</div>");
		}
		this.setMessage(sbf.toString());
		return AJAX;
	}
	
	private String getBookkeepingStatusNotes(Integer bookkeepingStatus) {
		String note = null;
		switch (bookkeepingStatus) {
		case 0:note="支取申请审核中";break;
		case 1:note="已记账支付宝转账中";break;
		case 2:note="已记账支付宝已确认";break;
		case 3:note="余额不足记账失败";break;
		case 4:note="信息不全记账失败";break;
		case 5:note="支付宝转账失败记账失败";break;
		default:note="尚未审核";
		}
		return note;
	}
	public String modify(){
		return SUCCESS;
	}
	public String person(){
		req = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
		if(req == null || req.getEmpid() ==null){
			return "newperson";
		}
		return SUCCESS;
	}
	
	public void savePhoneNo(){
		Requisition requisition = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
		String authcode = (String)ServletActionContext.getRequest().getSession().getAttribute("authcode");
		if((autocode !="" && authcode !="" && autocode.equals(authcode))){
		}else{
			this.doPrint("验证码错误！");
			return;
		}
		if((phoneNumber!=null && phoneNumber.equals(req.getPhoneNo()))){
		}else{
			this.doPrint("新号码填写不一致！");
			return;
		}
		requisition.setPhoneNo(req.getPhoneNo());
		requisition.setIsAppeal(0);
		requisition.setApplyTime(new Timestamp(System.currentTimeMillis()));
		if(pass.equals("0")){
			//非申诉
		}else{
			//申诉
			requisition.setIsAppeal(1);
			requisition.setApproveStatus(0);
		}
		
		if(paymentService.saveOrUpdate(requisition)>0){
			this.doPrint("1");
			ServletActionContext.getRequest().getSession().setAttribute("req", requisition);
		}else{
			this.doPrint("保存失败请重新操作！");
		}
	}
	public void saveAlipayInfo(){
		Requisition requisition = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
		if((year !="" && year.equals(requisition.getAlipayUser()))
				&&(month!="" && month.equals(requisition.getAlipayAccount()))
				&&(req.getPhoneNo()!="" && req.getPhoneNo().equals(requisition.getPhoneNo()))
				&&(req.getAlipayPwd() !="" && req.getAlipayPwd().equalsIgnoreCase(requisition.getAlipayPwd()))){
		}else{
			this.doPrint("原始信息验证错误！");
			return;
		}
		if((empNo!=null && empNo.equals(req.getAlipayAccount()))
				&&(autocode != null && autocode.equals(req.getAlipayUser()))){
		}else{
			this.doPrint("新信息填写不一致！");
			return;
		}
		requisition.setAlipayAccount(req.getAlipayAccount());
		requisition.setAlipayUser(req.getAlipayUser());
		requisition.setApplyTime(new Timestamp(System.currentTimeMillis()));
		if(paymentService.saveOrUpdate(requisition)>0){
			this.doPrint("1");
			ServletActionContext.getRequest().getSession().setAttribute("req", requisition);
		}else{
			this.doPrint("保存失败请重新操作！");
		}
	}
	
	public String newperson(){
//		Employee emp1 = (Employee)ServletActionContext.getRequest().getSession().getAttribute(Globals.CURRENT_USER);
//		req = paymentService.findByPerson(emp1.getEmpID());
		return SUCCESS;
	}
	public void saveNewPerson(){
		String authcode = (String)ServletActionContext.getRequest().getSession().getAttribute("authcode");
		if(autocode!="" && autocode.equals(authcode)){
		}else{
			this.doPrint("验证码错误！请重新验证！");
			return;
		}
		
		if(req.getPhoneNo()==""||req.getAlipayUser()==""){
			this.doPrint("输入信息不能为空！");
			return;
		}
		if(month!="" && month.equals(req.getAlipayAccount())){
		}else{
			this.doPrint("帐号信息输入不一致！请修改！");
			return;
		}
		if(pass !="" && pass.equalsIgnoreCase(req.getAlipayPwd())){
		}else{
			this.doPrint("密码信息输入不一致！请修改！");
			return;
		}
		
		Employee emp1=(Employee)ServletActionContext.getRequest().getSession()
			.getAttribute(Globals.CURRENT_USER);
		
		if(paymentService.findByPersonnum(emp1.getEmpID())==null){
			req.setEmpid(emp1);
			req.setApplyTime(new Timestamp(System.currentTimeMillis()));
			req.setApproveStatus(0);
			if(paymentService.saveOrUpdate(req)>0){
				ServletActionContext.getRequest().getSession().setAttribute("req", req);
				this.doPrint("1");
			}else{
				this.doPrint("保存失败！");
			}
		}else{
			this.doPrint("请勿重复保存！");
		}
	}
	
	public void password() {
		Requisition requisition = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
		if (requisition.getAlipayPwd().equals(req.getAlipayPwd())) {
			this.doPrint("1");
		}else{
			this.doPrint("密码错误！");
		}
	}

	public void saveNewPassword(){
		if (!req.getAlipayAccount().equals(req.getAlipayUser())) {
			this.doPrint("新密码两次输入不一致！");
			return;
		}
		
		Requisition requisition = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
		if(requisition.getAlipayPwd().equals((req.getPhoneNo()))) {
			requisition.setAlipayPwd((req.getAlipayAccount()));
			requisition.setApplyTime(new Timestamp(System.currentTimeMillis()));
			paymentService.updatePassword(requisition);
			this.doPrint("1");
			ServletActionContext.getRequest().getSession().setAttribute("req", requisition);
		}else{
			this.doPrint("原密码输入错误！");
		}
	}
	
	public void newWithdrawal(){
		String authcode = (String)ServletActionContext.getRequest().getSession().getAttribute("authcode");
		if(autocode!="" && autocode.equals(authcode)){
		}else{
			this.doPrint("验证码错误！请重新验证！");
			return;
		}
		
		Requisition requisition = (Requisition) ServletActionContext.getRequest()
																	.getSession().getAttribute("req");
		
		if(pass!=""&& pass.equalsIgnoreCase(requisition.getAlipayPwd())){
		}else{
			this.doPrint("密码错误！");
			return;
		}
		if(requisition.getApproveStatus()==0){
			this.doPrint("营业员信息未审核！");
			return;
		}
		Employee emp1 = (Employee) ServletActionContext.getRequest().getSession()
																	.getAttribute(Globals.CURRENT_USER);
		if(paymentService.checkWithdrawal(emp1, money) != null){
			this.doPrint("2");
			return;
		}
		cw.setApply_amount(new BigDecimal(money));
		cw.setApply_amount_fee(new BigDecimal(year));
		cw.setApply_amount_tax(new BigDecimal(month));
		cw.setBookkeepingRemaining(new BigDecimal(phoneNumber));
		int i = paymentService.insertWithdrawal(emp1, cw);
		this.doPrint(i);
	}
	
	public String newVerfysms(){
		if(autocode.equalsIgnoreCase("0")){
			//非申诉
			req = (Requisition) ServletActionContext.getRequest().getSession().getAttribute("req");
			phoneNumber = req.getPhoneNo();
		}else{
			//申诉
			phoneNumber = year;
		}
		month = String.valueOf(System.currentTimeMillis()).substring(0, 8);
		int flag=1;
		Pattern p = Pattern.compile("^1[3,5,4,8]{1}\\d{9}$"); 
		if(phoneNumber !=null && !"".equals(phoneNumber)){
			Matcher m = p.matcher(phoneNumber.trim()); 
			if(m.matches()){
				String smsTimeStamp = (String) ServletActionContext.getRequest().getSession()
					.getAttribute("smsTimeStamp");
				if(smsTimeStamp!=null && smsTimeStamp.equals(month)){
					flag = 5;
				}else{
					try{
						String authcode;
//						短信端口IP限制，测试时使用固定验证码123456
						authcode = SmsHessianService.sendAuthcode(null, phoneNumber.trim());
//						authcode = "123456";
						ServletActionContext.getRequest().getSession().setAttribute("authcode", authcode);
						ServletActionContext.getRequest().getSession().setAttribute("smsTimeStamp", 
								String.valueOf(System.currentTimeMillis()).substring(0, 8));
						flag=3;
					}catch (Exception e){
						logger.error("发送手机验证码时出错："+e.getMessage());
						flag = 6;
					}
				}
			}else{
				flag=4;
			}
		}else{
			flag=2;
		}
		String result="{\"flag\":"+flag+"}";
		this.setMessage(result);
		return AJAX;
	}
	public String verifysms(){
		month = String.valueOf(System.currentTimeMillis()).substring(0, 8);
		int flag=1;
		Pattern p = Pattern.compile("^1[3,5,4,8]{1}\\d{9}$"); 
		if(phoneNumber !=null && !"".equals(phoneNumber)){
			Matcher m = p.matcher(phoneNumber.trim()); 
			if(m.matches()){
				String smsTimeStamp = (String) ServletActionContext.getRequest().getSession()
					.getAttribute("smsTimeStamp");
				if(smsTimeStamp!=null && month.equals(smsTimeStamp)){
					flag = 5;
				}else{
					try{
						String authcode;
//						短信端口IP限制，测试时使用固定验证码123456
						authcode = SmsHessianService.sendAuthcode(null, phoneNumber.trim());
//						authcode = "123456";
						ServletActionContext.getRequest().getSession().setAttribute("authcode", authcode);
						ServletActionContext.getRequest().getSession().setAttribute("smsTimeStamp", 
								String.valueOf(System.currentTimeMillis()).substring(0, 8));
						flag=3;
					}catch (Exception e){
						logger.error("发送手机验证码时出错："+e.getMessage());
						flag = 6;
					}
				}
			}else{
				flag=4;
			}
		}else{
			flag=2;
		}
		String result="{\"flag\":"+flag+"}";
		this.setMessage(result);
		return AJAX;
	}
	
	public String verifyautocode(){
		int flag=1;
		try{
			if(autocode !=null && !"".equals(autocode)){
				String authcode =(String)ServletActionContext.getRequest().getSession()
																		.getAttribute("authcode");
				if(authcode !=null){
					if(autocode.equals(authcode)){
						flag=3;
					}else{
						flag=4;
					}
				}else{
					flag=5;
				}
			}else{
				flag=2;
			}
		}catch (Exception e){
			logger.error("验证手机验证码时出错："+e.getMessage());
		}
		String result="{\"flag\":"+flag+"}";
		this.setMessage(result);
		return AJAX;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public int getPro() {
		return pro;
	}
	public void setPro(int pro) {
		this.pro = pro;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public Requisition getReq() {
		return req;
	}
	public void setReq(Requisition req) {
		this.req = req;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAutocode() {
		return autocode;
	}
	public void setAutocode(String autocode) {
		this.autocode = autocode;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public CommisionWithdrawal getCw() {
		return cw;
	}
	public void setCw(CommisionWithdrawal cw) {
		this.cw = cw;
	}
}

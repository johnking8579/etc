package com.i8app.install;

public class PagerContext {
	private PagerContext() {}

	private static ThreadLocal<Integer> offsetThreadLocal = new ThreadLocal<Integer>();
	
	private static ThreadLocal<Integer> pageSizeThreadLocal = new ThreadLocal<Integer>(); 
	
	public static void setOffset(int offset) {
		offsetThreadLocal.set(offset);
	}

	public static void setPageSize(int pageSize) {
		pageSizeThreadLocal.set(pageSize);
	}
	
	public static int getOffset() {
		
		return offsetThreadLocal.get();
	}
	
	public static int getPageSize() {
		return pageSizeThreadLocal.get();
	} 
	
	public static void removeOffset() {
		offsetThreadLocal.remove();	
	}
	
	public static void removePageSize() {
		offsetThreadLocal.remove();
	}
}

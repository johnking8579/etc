package com.i8app.install;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * PAGER-TAGLIB用分页模型
 * @author jing
 *
 * @param <T>
 */
@XmlRootElement
public class Pager <T> {
	
	private int total;
	
	private List<T> list;
	
	public Pager() {}
	
	public Pager(List<T> list, int total)	{
		this.list = list;
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
}

package com.i8app.install.wsclient;

import java.util.List;
import java.util.Map;


/**
 * 支付宝项目支取单接口. 发布的hessian接口
 * 
 * @author YangJunli 2014-03-12
 */
public interface Commision {
	
	/**
	 * 支取单处理
	 * @param str 支取单号数组
	 * @return 支取链接，单号处理结果
	 */
	public String verifyCommision(String[] str);

	/**
	 * 入账单处理
	 */
	public void inputDeposit();

	/**
	 * 支付宝回调接口
	 * @param params 回调链接参数Map
	 */
	public void alipayNofity(Map<String, String> params);
	
	/**
	 * 验证支付宝参数
	 * @param params
	 * @return
	 */
	public boolean alipayVerify(Map<String, String> params);

	/**
	 * web中营业员支取界面帐号余额状态详情
	 * @param empId 营业员编码
	 * @return JSON字符串
	 */
	public String getBalanceByEmpId(Integer empId);

	/**
	 * LDMS中管理员查询营业员奖金记录单列表
	 * @param empIds 营业员编码列表
	 * @return 佣金流水JSON字符串
	 */
	public String getBalanceByEmpId(String approver, String tiaojian, Integer pageIndex, int rows);
	public String ListAccountByEmpIds(Integer empId);
	/**
	 * LDMS中管理员查询营业员奖金记录单列表
	 * @param empIds 营业员编码列表
	 * @return 佣金流水JSON字符串
	 */
	public String ListBalanceByEmpIds(List<Integer> empIds);
	/**
	 * 解冻冻结操作
	 * @param empId 
	 * @param managerId
	 * @param status
	 * @return
	 */
	public String updateBalanceStatus(Integer empId, Integer managerId, Integer status , String note);

}

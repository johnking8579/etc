package com.i8app.install.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.i8app.ezinstall.common.Util;
import com.i8app.install.dao.AppleDao;
import com.i8app.install.domain.AppleUser;
import com.i8app.install.domain.Applelog;

/**
 * 在spring容器中配置成prototype模式, 不要配成单例模式
 * 
 * @author Lixiangyang 2014-05-13
 */
@Component
@Scope("prototype")
public class ChannelcommitThread implements Runnable {
	private static Logger logger = Logger.getLogger(AppleService.class);
	public static final String 
	BASE_URL 	= 	"http://api1.apptree.com.cn/other/hengyuan/channelcommit.jsp?";//
	@Resource
	public AppleDao appleDao;
	private String callback;
	private String mac;
	@Override
	public void run() {
		logger.info("开始访问葫芦接口进行激活");
//		appleDao.updateadudid();//
		List<AppleUser> userList = appleDao.findAppleUser();
		for(AppleUser u:userList){
			try{
				String callbackurl = String.format("%slogid=%s", callback,u.getUser_id());
				String callbackurlencoder = URLEncoder.encode(callbackurl, "utf-8");
				String url = String.format("%ssignid=713&idfa=%s&mac=%s&adudid=%s&callback=%s&openudid=%s&phoneversion=%s", BASE_URL,u.getIdfa(),u.getMac(),u.getSign(),callbackurlencoder,u.getOpenudid(),u.getVersion());
				JsonObject result = doGet(url);
				if(result !=null && "000000".equals(result.get("resultcode").getAsString()) && result.get("isok").getAsInt()==1)	{
					Applelog log = appleDao.findApplelogbyid(u.getUser_id());
					if(log !=null){
						log.setIsOk(1);
						appleDao.saveOrUpdateApplelog(log);
					}
					logger.info("苹果树推广软件接口返回成功");
				}else{
					Applelog log = appleDao.findApplelogbyid(u.getUser_id());
					if(log !=null){
						log.setIsOk(2);
						appleDao.saveOrUpdateApplelog(log);
					}
					logger.error(String.format("苹果树推广软件%s在设备%s接口返回错误结果码%s", u.getSign(),u.getMac(),result.get("resultcode").getAsString()));
				}
			}catch(Exception e){
				logger.error(String.format("苹果树推广软件%s在设备%s接口返回错误信息%s", u.getSign(),u.getMac(),e.getMessage()));
			}
		}
	}
	private JsonObject doGet(String url) 	{
		InputStream is = null;
		JsonObject json = null;
		try {
			HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
			is = conn.getInputStream();
			json = new JsonParser().parse(new InputStreamReader(is,"UTF-8")).getAsJsonObject();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally	{
			Util.closeStream(is);
		}
		return json;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
}

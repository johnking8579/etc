package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.PhonewizardDao;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Mobile;
import com.i8app.install.domain.Packageinfo;
import com.i8app.install.domain.Phonewizard;

@Component("phonewizardService")
public class PhonewizardService{

	private PhonewizardDao phonewizardDao;

	public void addOrUpdate(Phonewizard ur) {
		phonewizardDao.addOrUpdate(ur);
	}
	public void addOrUpdate(Packageinfo ur1) {
		phonewizardDao.addOrUpdate(ur1);
	}
	public void addOrUpdate(Mobile ur2) {
		phonewizardDao.addOrUpdate(ur2);
	}
	public Phonewizard findById(int id) {
		return phonewizardDao.findById(id);
	}

	public Employee findByIdandOstype(String empno,int provinceId){
		return phonewizardDao.findByIdandOstype(empno, provinceId);
	}
	@Resource
	public void setPhonewizardDao(PhonewizardDao phonewizardDao) {
		this.phonewizardDao = phonewizardDao;
	}
	
	public List<Packageinfo> findByDevice(String empno){
		return phonewizardDao.findByDevice(empno);
	}
	
	public Packageinfo findByPackage(String deviceNo,String packageName){
		return phonewizardDao.findByPackage(deviceNo, packageName);
	}
	public Mobile findMobileByDevice(String device){
		return phonewizardDao.findMobileByDevice(device);
	}
}

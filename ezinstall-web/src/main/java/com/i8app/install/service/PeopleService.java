package com.i8app.install.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import sun.misc.BASE64Decoder;

import com.i8app.install.Config;
import com.i8app.install.ImageUtils;
import com.i8app.install.Util;
import com.i8app.install.dao.PeopleDao;
import com.i8app.install.domain.App;
import com.i8app.install.domain.App_Comment;
import com.i8app.install.domain.ClientActivityLog;
import com.i8app.install.domain.ClientConfigInfo;
import com.i8app.install.domain.ContactBakData;
import com.i8app.install.domain.FeedbackInfo;
import com.i8app.install.domain.Guessyoulike;
import com.i8app.install.domain.Mobile_installed_list;
import com.i8app.install.domain.PeopleInfo;
import com.i8app.install.domain.Signalqualitydata;
import com.i8app.install.domain.Signalrequest;
import com.i8app.install.domain.Sms_backup;
import com.i8app.install.domain.Smsspam;
import com.i8app.install.domain.Threads;
import com.i8app.install.domain.User_uninstall_log;

@Component("UserService")
public class PeopleService {
	public static enum FileType{软件,截图,图片};
	public static final String SPACE_FOLDER = "space";
	public static final String PID_FOLDER = "pid";
	public static final String APP_FOLDER = "soft";
	public static final String PIC_FOLDER = "pic";
	public static final String IMAGE_FOLDER = "image";
	public static final String ICON_FOLDER = "icon";
	@Resource
	private PeopleDao peopleDao;
	@Resource
	private Config config;
	public void setPeopleDao(PeopleDao peopleDao) {
		this.peopleDao = peopleDao;
	}

	public void saveOrUpdatePeople(PeopleInfo p) {
		peopleDao.saveOrUpdatePeople(p);
	}
	
	public void saveOrUpdateFeeback(FeedbackInfo p){
		peopleDao.saveOrUpdateFeeback(p);
	}

	public void saveOrUpdateUninstall(User_uninstall_log p)	{
		peopleDao.saveOrUpdateUninstall(p);
	}
	
	public void saveOrUpdateSignalrequest(Signalrequest p){
		peopleDao.saveOrUpdateSignalrequest(p);
	}
	public void saveOrUpdateApp_Comment(App_Comment p){
		peopleDao.saveOrUpdateApp_Comment(p);
	}
	public void saveOrUpdateSignalqualitydata(Signalqualitydata p)	{
		peopleDao.saveOrUpdateSignalqualitydata(p);
	}
	public void saveOrUpdateApp(App p)	{
		peopleDao.saveOrUpdateApp(p);
	}
	public App findByuuid(String uuid) {
		return peopleDao.findByuuid(uuid);
	}
	public List<ContactBakData> ContactBakDataList(String peopleID) {
		return peopleDao.ContactBakDataList(peopleID);
	}
	
	public InputStream findvcardFile(String id) {
		return peopleDao.findvcardFile(id);
	}
	public void saveOrUpdateContactBakData(ContactBakData p)	{
		peopleDao.saveOrUpdateContactBakData(p);
	}
	
	public void saveOrUpdateSms(Sms_backup p)	{
		peopleDao.saveOrUpdateSms(p);
	}
	public void saveOrUpdateThreads(Threads p)	{
		peopleDao.saveOrUpdateThreads(p);
	}
	public Threads findBythreadid(int id) {
		return peopleDao.findBythreadid(id);
	}
	public Threads findBythrid(int id) {
		return peopleDao.findBythrid(id);
	}
	public List<Threads> findBypeopleId(String peopleid) {
		return peopleDao.findBypeopleId(peopleid);
	}
	public void delSms(Sms_backup p)	{
		peopleDao.delSms(p);
	}
	public void delThreads(Threads p)	{
		peopleDao.delThreads(p);
	}
	public void saveOrUpdateMobile_installed_list(Mobile_installed_list p)	{
		peopleDao.saveOrUpdateMobile_installed_list(p);
	}
	public void saveOrUpdateSmsspam(Smsspam p)	{
		peopleDao.saveOrUpdateSmsspam(p);
	}
	public void delteleMobile_installed_list(Mobile_installed_list mobile){
		peopleDao.delteleMobile_installed_list(mobile);
	}
	public Mobile_installed_list findByDeviceNoAndApkName(String deviceNo, String apkName) {
		return peopleDao.findByDeviceNoAndApkName(deviceNo, apkName);
	}
	public Smsspam findByPeopleIdAndsms(String peopleId, String content,String time) {
		return peopleDao.findByPeopleIdAndsms(peopleId, content, time);
	}
	public List<Sms_backup> findBythread_id(int id) {
		return peopleDao.findBythread_id(id);
	}
	public List<Sms_backup> sms_backupList(String uid,int threadId,int count) {
		return peopleDao.sms_backupList(uid,threadId,count);
	}
	public void saveOrUpdateContactBakData1(String peo,String memo,String name,String cardVer,String time,File cardfile,int count) {
		peopleDao.saveOrUpdateContactBakData1(peo, memo, name, cardVer, time, cardfile,count);
	}
	public PeopleInfo findByUserNumber(String userNumber) {
		return peopleDao.findByUserNumber(userNumber);
	}
	
	public PeopleInfo findByUserNumberAndPassword(String userNumber, String userPassword) {
		return peopleDao.findByUserNumberAndPassword(userNumber, userPassword);
	}
	
	public Integer saveClientActivityLog(ClientActivityLog caLog)	{
		return peopleDao.saveClientActivityLog(caLog);
	}
	public PeopleInfo findByPhoneNumber(String phoneNumber) {
		return peopleDao.findByPhoneNumber(phoneNumber);
	}
	public PeopleInfo findByPhoneIMSI(String imsi) {
		return peopleDao.findByPhoneIMSI(imsi);
	}
	public PeopleInfo findByPhoneIMEI(String imei) {
		return peopleDao.findByPhoneIMEI(imei);
	}
	public PeopleInfo findByPeopleID(String id) {
		return peopleDao.findByPeopleID(id);
	}
	public PeopleInfo findByPhonenoandimeiandimsi(String phoneNumber,String imei,String imsi) {
		return peopleDao.findByPhonenoandimeiandimsi(phoneNumber, imei, imsi);
	}
	public String findMaxPeopleID(){
		return peopleDao.findMaxPeopleID();
	}
	public int provinceID(String provName){
		return peopleDao.provinceID(provName);
	}
	public int cityID(String cityName){
		return peopleDao.cityID(cityName);
	}
	public ClientConfigInfo findClientConfigInfo(){
		return peopleDao.findClientConfigInfo();
	}
	public void saveOrUpdateClientConfigInfo(ClientConfigInfo p)	{
		peopleDao.saveOrUpdateClientConfigInfo(p);
	}
	public List<String> findnewVersion(String apkPkgName) {
		return peopleDao.findnewVersion(apkPkgName);
	}
	
	public int findByuidandsmsid(String uid,int smsId,int threadId) {
		return peopleDao.findByuidandsmsid(uid, smsId,threadId);
	}
	
	private void writeToDisk(File fileRead, File fileWrite)	{
		if(!fileWrite.getParentFile().exists())	{
			fileWrite.getParentFile().mkdirs();
		}
		int b=0;
		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel fcIn = null, fcOut = null;
		try {
			fis = new FileInputStream(fileRead);
			fos = new FileOutputStream(fileWrite);
			fcIn = fis.getChannel();
			fcOut = fos.getChannel();
			fcIn.transferTo(0, fcIn.size(), fcOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fcIn.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (fos != null) {
				try {
					fcOut.close();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public String writeFile(String fileName,
			File fileRead, FileType fileType) {
	String childPath = null;
		if(FileType.软件.equals(fileType))
			childPath = SPACE_FOLDER + Util.SPLITTER + PID_FOLDER +
							 "/"+ APP_FOLDER
								+ "/" + Util.createTimestamp() + Util.getSuffixName(fileName);
		else if(FileType.截图.equals(fileType))
			childPath = SPACE_FOLDER + Util.SPLITTER + PID_FOLDER +
							 "/"+ PIC_FOLDER
								+ "/" + Util.createTimestamp() + Util.getSuffixName(fileName);
		
		else if(FileType.图片.equals(fileType)){
			childPath = SPACE_FOLDER + Util.SPLITTER + PID_FOLDER +
			 "/"+ IMAGE_FOLDER
				+ "/" + Util.createTimestamp() + Util.getSuffixName(fileName);

		}
		File fileWrite = new File(config.getFileServerDir() + childPath);
		System.out.println(config.getFileServerDir() + childPath);
		this.writeToDisk(fileRead, fileWrite);
		return  childPath;
	}
	
	public String writeIcon(String fileName,File file) {
		String childPath = null;
			childPath = SPACE_FOLDER + Util.SPLITTER + PID_FOLDER +
							 "/"+ ICON_FOLDER
								+ "/" + Util.createTimestamp() + Util.getSuffixName(fileName);
		File fileWrite = new File(config.getFileServerDir() + childPath);
//		System.out.println(config.getFileServerDir() + childPath);
//		this.writeToDisk(fileRead, fileWrite);
		if(!fileWrite.getParentFile().exists())	{
			fileWrite.getParentFile().mkdirs();
		}
		ImageUtils.scale3(file, config.getFileServerDir() + childPath, 200, 200, false);
		return  childPath;
	}
	
	public String stringtoIcon(String imgStr) {
		String childPath = null;
		childPath = SPACE_FOLDER + Util.SPLITTER + PID_FOLDER +
						 "/"+ ICON_FOLDER
							+ "/" + Util.createTimestamp() + ".jpg";
		File fileWrite = new File(config.getFileServerDir() + childPath);
	//	System.out.println(config.getFileServerDir() + childPath);
	//	this.writeToDisk(fileRead, fileWrite);
		if(!fileWrite.getParentFile().exists())	{
			fileWrite.getParentFile().mkdirs();
		}
		if(!stringtoIcon(imgStr,config.getFileServerDir() + childPath)){
			childPath="";
		}
		return  childPath;
	}
	
	public boolean stringtoIcon(String imgStr,String imgFile) {
		  // 对字节数组字符串进行Base64解码并生成图片
	    if (imgStr == null){ // 图像数据为空
	        return false;
	    }
	    @SuppressWarnings("restriction")
		BASE64Decoder decoder = new BASE64Decoder();
	    try {
	        // Base64解码
	        @SuppressWarnings("restriction")
			byte[] b = decoder.decodeBuffer(imgStr);
	        for (int i = 0; i < b.length; ++i) {
	            if (b[i] < 0) {// 调整异常数据
	                b[i] += 256;
	            }
	        }
	        // 生成jpeg图片
	        String imgFilePath = imgFile;// 新生成的图片
	        OutputStream out = new FileOutputStream(imgFilePath);
	        out.write(b);
	        out.flush();
	        out.close();
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return true;
	}
	public List<App_Comment> findByPidAndUuid(String uid,int index,int size) {
		return peopleDao.findByPidAndUuid(uid,index,size);
	}
	
	public int findByPidAndUuid(String uid) {
		return peopleDao.findByPidAndUuid(uid);
	}
	public int findByAppUuid(String uid) {
		return peopleDao.findByAppUuid(uid);
	}
	/**
	 * 图片是否符合 jpg gjf png格式
	 * @param imgStr
	 * @return
	 */
	public static boolean isRightFormat(String format){
	    return (format.equals("jpg") || format.equals("gif") || format.equals("png"))?true:false;
	}
	
	
	public int  findByvcf(String id) {
		int count = 0;
		String in = peopleDao.findvcard(id);
		if(in != null){
			int ins = in.lastIndexOf("END");
			System.out.println(ins);
		}
		return  count;
	}
	
	public List<Guessyoulike> findByPid(String uid,int index,int size) {
		return peopleDao.findByPid(uid, index, size);
	}
	public List<Guessyoulike> findByPidNoBackup(int index,int size) {
		return peopleDao.findByPidNoBackup(index, size);
	}
}

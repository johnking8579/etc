package com.i8app.install.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.i8app.install.Config;
import com.i8app.install.Pager;
import com.i8app.install.PhoneOs;
import com.i8app.install.dao.SoftInfoDao;
import com.i8app.install.dao.SoftInstallLogDao;
import com.i8app.install.dao.SoftTypeDao;
import com.i8app.install.domain.SoftInfo;
import com.i8app.install.domain.SoftInstallLog;
import com.i8app.install.domain.SoftType;

@Component
public class SoftInfoService extends AbstractJsonService implements IAppService	{
	
	@Resource
	private SoftInfoDao softInfoDao;
	@Resource
	private SoftTypeDao softTypeDao;
	@Resource
	private SoftInstallLogDao softInstallLogDao;
	
	private String HTTP_PATH;
	
	/**
	 * 根据软件内容做下载日志
	 * @param s
	 */
	@Override
	public <T> void persistInstallLog(T app)	{
		if(app != null)	{
			SoftInfo s = (SoftInfo)app;
			SoftInstallLog log = new SoftInstallLog();
			log.setInstallType(6);
			log.setSource(0);
			log.setSoftId(s.getId());
			log.setSoftName(s.getSoftName());
			log.setOsType(s.getOsType());
			log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:MM:ss").format(new Date()));
			softInstallLogDao.saveOrUpdate(log);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SoftInfo findById(int id) {
		return softInfoDao.findById(id);
	}
	
	@Override
	public <T> void updateDownnum(T app) {
		SoftInfo s = (SoftInfo)app;
		softInfoDao.updateDownNum(s);
	}

	public String getSetupFilePath(SoftInfo softInfo, int osType) {
		if(osType == 33)
			return HTTP_PATH+ softInfo.getLegalFileName();
		return HTTP_PATH + softInfo.getSoftFileName();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftInfo> find(PhoneOs phoneOs) {
		return softInfoDao.findByCriteria(phoneOs.createCriteria(SoftInfo.class));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftInfo> find(PhoneOs phoneOs, int pageNo, int pageSize)	{
		DetachedCriteria dc = phoneOs.createCriteriagongyong(SoftInfo.class);
		dc.addOrder(Order.desc("shangjiadate"));
		return this.softInfoDao.findByCriteria(dc, pageNo, pageSize);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftInfo> search(PhoneOs phoneOs, String keyword)	{
		DetachedCriteria dc = phoneOs.createCriteria(SoftInfo.class);
		dc.add(Restrictions.like("softName", keyword, MatchMode.ANYWHERE));
		return this.softInfoDao.findByCriteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftInfo> search(PhoneOs phoneOs, String keyword, int pageNo, int pageSize)	{
		DetachedCriteria dc = phoneOs.createCriteria(SoftInfo.class);
		dc.add(Restrictions.like("softName", keyword, MatchMode.ANYWHERE));
		return this.softInfoDao.findByCriteria(dc, pageNo, pageSize);
	}
//	public List<Tongyong> searchappandgame(PhoneOs phoneOs, String keyword, int pageno, int pagesize){
//		return this.softInfoDao.findByCriteria(dc);
//	}
	@SuppressWarnings("unchecked")
	@Override
	public Pager<SoftInfo> byTypeid(PhoneOs phoneOs, int appTypeId, int offset, int pageSize)	{
		DetachedCriteria dc = phoneOs.createCriteria(SoftInfo.class);
			dc.createAlias("softType", "st")
			.add(Restrictions.eq("st.id", appTypeId));
		return softInfoDao.queryPager(dc, offset, pageSize);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftType> typeList()	{
		return softTypeDao.findAll();
	}
	
	@Resource
	public void setConfig(Config config)	{
		this.HTTP_PATH = config.getFileServerUrl();
	}
	
	/**
	 * 将SoftInfo类展示为Json格式, json的key名采用数据库模型中<<软件信息表>>的命名
	 */
	@Override
	public <T> JsonObject appToJson(T app)	{
		SoftInfo s = (SoftInfo)app;
		JsonObject json = new JsonObject();
		json.addProperty("softid", s.getId() + "");
		json.addProperty("softname", s.getSoftName());
		json.addProperty("kaifamanu", s.getKaifamanu() == null ? "" : s.getKaifamanu());
		json.addProperty("softVersion", s.getSoftVersion() == null ? "" : s.getSoftVersion());
		json.addProperty("fileSize", s.getFileSize() == null ? "" : s.getFileSize() + "");
		json.addProperty("softFee", s.getSoftFee() == null ? "" : s.getSoftFee() + "");
		json.addProperty("softtype", s.getSoftType() == null ? "" : s.getSoftType().getTypeName() + "");
		json.addProperty("shangjiadate", s.getShangjiadate() == null ? "" : s.getShangjiadate());
		json.addProperty("softinfo", s.getSoftInfo() == null ? "" : s.getSoftInfo());
		json.addProperty("ostype", s.getOsType() + "");
		json.addProperty("osid", s.getOsInfo() == null ? "" : s.getOsInfo().getId() + "");
		json.addProperty("suffix", s.getSoftSuffix() == null ? "" : s.getSoftSuffix().getSuffixName());
		
		String s1 = s.getIconFileName();
		if(s1 == null || "".equals(s1.trim()))
			json.addProperty("iconfilename", "");
		else
			json.addProperty("iconfilename", HTTP_PATH + s1.replace("\\", "/"));
		
		String s2 = s.getPicFileName1();
		if(s2 == null || "".equals(s2.trim()))
			json.addProperty("picfilename1", "");
		else
			json.addProperty("picfilename1", HTTP_PATH + s2.replace("\\", "/"));
		
		String s3 = s.getPicFileName2();
		if(s3 == null || "".equals(s3.trim()))
			json.addProperty("picfilename2", "");
		else
			json.addProperty("picfilename2", HTTP_PATH + s3.replace("\\", "/"));
		
		String s4 = s.getPicFileName3();
		if(s4 == null || "".equals(s4.trim()))
			json.addProperty("picfilename3", "");
		else
			json.addProperty("picfilename3", HTTP_PATH +s4.replace("\\", "/"));
		
		String s5 = s.getSoftFileName();
		if(s5 == null || "".equals(s5.trim()))
			json.addProperty("softfilename", "");
		else
			json.addProperty("softfilename", HTTP_PATH + s5.replace("\\", "/"));

		String s6 = s.getLegalFileName();
		if(s6 == null || "".equals(s6.trim()))
			json.addProperty("legalfilename", "");
		else
			json.addProperty("legalfilename", HTTP_PATH + s6.replace("\\", "/"));
		
		json.addProperty("softurl", s.getSoftUrl() == null ? "" : s.getSoftUrl());
		json.addProperty("hotrecmd", s.getHotRecmd() == null ? "" : s.getHotRecmd() + "");
		json.addProperty("downnum", s.getDownNum() == null ? "" : s.getDownNum() + "");
		json.addProperty("installCount", s.getInstallCount() == null ? "" : s.getInstallCount() + "");
		json.addProperty("userRemarkNum", s.getUserRemarkNum() == null ? "" : s.getUserRemarkNum()+"");
		json.addProperty("userRemark", s.getUserRemark() + "");
		return json;
	}
}

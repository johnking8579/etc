package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.i8app.install.dao.BookInfoDao;
import com.i8app.install.domain.BookInfo;
@Component("bookInfoService")
public class BookInfoService{
	private BookInfoDao bookInfoDao;
	private String httpRootPath;
	public String getHttpRootPath() {
		return httpRootPath;
	}
	@Resource
	public void setBookInfoDao(BookInfoDao bookInfoDao) {
		this.bookInfoDao = bookInfoDao;
	}

	public void addOrUpdate(BookInfo bookInfo) {
		bookInfoDao.addOrUpdate(bookInfo);
	}

	public void del(int id) {
		bookInfoDao.del(id);
	}

	public BookInfo findbookById(int id) {
		return bookInfoDao.findBookById(id);
	}

	public List<BookInfo> list() {
		return bookInfoDao.list();
	}

	public List<BookInfo> list(String title, String author, int bookTypeId) {
		return bookInfoDao.list(title, author, bookTypeId);
	}

	public List<BookInfo> list(int pageNo, int pageSize) {
		return bookInfoDao.list(pageNo, pageSize);
	}
	public String listJson()	{
		return this.changeToJson(this.list());
	}
	
	public String listJson(int pageNo, int pageSize)	{
		return this.changeToJson(this.list(pageNo, pageSize));
	}
	
	public int count()	{
		return bookInfoDao.count();
	}
	public String changeToJson(List<BookInfo> list)	{
		JsonArray jsonArray = new JsonArray();
		for(BookInfo b : list)	{
			JsonObject json = new JsonObject();
			json.addProperty("id", b.getId() + "");
			json.addProperty("title", b.getTitle() + "");
			json.addProperty("author", b.getAuthor() + "");
			json.addProperty("bookType", b.getBookType() == null ? "" : b.getBookType().getTypeName() + "");
			json.addProperty("fileSize", b.getFileSize() + "");
			json.addProperty("publishDate",b.getPublishDate() + "");
			json.addProperty("addDate", b.getAddDate() + "");
			json.addProperty("intro", b.getIntro() + "");
			json.addProperty("coverPic", b.getCoverPic() + "");
			json.addProperty("url", b.getUrl() + "");
			json.addProperty("wapUrl", b.getWapUrl() + "");
			json.addProperty("suffix", b.getSuffix() == null ? "" : b.getSuffix().getSuffixName() + "");
			jsonArray.add(json);
		}
		return jsonArray.toString().replace("null", "");
	}


}

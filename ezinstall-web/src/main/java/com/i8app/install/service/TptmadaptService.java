package com.i8app.install.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.install.dao.TptmadaptDao;
import com.i8app.install.domain.Tptmadapt;

@Service
public class TptmadaptService {
	@Resource
	public TptmadaptDao tptmadaptDao;
	public Tptmadapt findByPEP(Integer pid,String empno,String phone){
		return tptmadaptDao.findByPEP(pid, empno, phone);
	}
	public void addOrUpdateTptmadapt(Tptmadapt tpt) {
		tptmadaptDao.addOrUpdateTptmadapt(tpt);
	}
}

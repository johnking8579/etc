package com.i8app.install.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.wsclient.Commision;

@Component("commisionHessionService")
public class CommisionHessionService {
	
	@Resource
	private Commision commision;
	
	public String getBalance(Integer empId){
		return commision.getBalanceByEmpId(empId);
	}
	
//	public List<CommisionAccount> listAccounts(Integer empId){
//		String result = commision.listAccountByEmpId(empId);
//		JsonParser parser = new JsonParser();
//		JsonElement el = parser.parse(result);
//		JsonObject res = el.getAsJsonObject();
//		if(res.get("status").getAsInt()==0){
//			return null;
//		}
//
////		System.out.println(res.get("data").isJsonPrimitive());
//		Gson gson = new Gson();
//		List<CommisionAccount> list = new ArrayList<CommisionAccount>();
//		JsonArray ja = parser.parse(res.get("data").getAsString()).getAsJsonArray();
//		for (JsonElement jsonElement : ja) {
////			JsonObject o = jsonElement.getAsJsonObject();
//			CommisionAccount ca = gson.fromJson(jsonElement, CommisionAccount.class);
//			list.add(ca);
//		}
//		System.out.println(parser.parse(res.get("data").getAsString()).getAsJsonArray());
////		Json
//		return list;
//	}
}

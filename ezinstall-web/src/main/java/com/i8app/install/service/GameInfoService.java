package com.i8app.install.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.i8app.install.Pager;
import com.i8app.install.Config;
import com.i8app.install.PhoneOs;
import com.i8app.install.dao.GameInfoDao;
import com.i8app.install.dao.GameInstallLogDao;
import com.i8app.install.dao.GameTypeDao;
import com.i8app.install.domain.GameInfo;
import com.i8app.install.domain.GameInstallLog;
import com.i8app.install.domain.GameType;

@Component
public class GameInfoService extends AbstractJsonService implements IAppService	{
	
	@Resource
	private GameInfoDao gameInfoDao;
	@Resource
	private GameTypeDao gameTypeDao;
	@Resource
	private GameInstallLogDao gameInstallLogDao;
	
	@SuppressWarnings("unused")
	private String HTTP_PATH, DISK_PATH;
	
	/**
	 * 根据软件内容做下载日志
	 * @param s
	 */
	@Override
	public <T> void persistInstallLog(T app)	{
		if(app != null)	{
			GameInfo g = (GameInfo)app;
			GameInstallLog log = new GameInstallLog();
			log.setInstallType(6);
			log.setSource(0);
			log.setGid(g.getId());
			log.setGameName(g.getSoftName());
			log.setOsType(g.getOsType());
			log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:MM:ss").format(new Date()));
			gameInstallLogDao.saveOrUpdate(log);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public GameInfo findById(int id) {
		return gameInfoDao.findById(id);
	}
	
	@Override
	public <T> void updateDownnum(T app) {
		// TODO Auto-generated method stub
		
	}

	public String getSetupFilePath(GameInfo gameInfo, int osType) {
		if(osType == 33)	
			return HTTP_PATH + gameInfo.getLegalFileName();
		return HTTP_PATH + gameInfo.getSoftFileName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GameInfo> find(PhoneOs phoneOs) {
		DetachedCriteria dc = phoneOs.createCriteria(GameInfo.class);
		return gameInfoDao.findByCriteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GameInfo> find(PhoneOs phoneOs, int pageNo, int pageSize) {
		DetachedCriteria dc = phoneOs.createCriteriagongyong(GameInfo.class);
		dc.addOrder(Order.desc("shangjiadate"));
		return gameInfoDao.findByCriteria(dc, pageNo, pageSize);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GameInfo> search(PhoneOs phoneOs, String keyword) {
		DetachedCriteria dc = phoneOs.createCriteria(GameInfo.class);
		dc.add(Restrictions.like("softName", keyword, MatchMode.ANYWHERE));
		return this.gameInfoDao.findByCriteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GameInfo> search(PhoneOs phoneOs, String keyword, int pageNo, int pageSize) {
		DetachedCriteria dc = phoneOs.createCriteria(GameInfo.class);
		dc.add(Restrictions.like("softName", keyword, MatchMode.ANYWHERE));
		return this.gameInfoDao.findByCriteria(dc, pageNo, pageSize);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public <T> Pager<T> byTypeid(PhoneOs phoneOs, int appTypeid, int offset, int pagesize) {
		DetachedCriteria dc = phoneOs.createCriteria(GameInfo.class);
		dc.createAlias("gameType", "gt")
			.add(Restrictions.eq("gt.id", appTypeid));
		return gameInfoDao.queryPager(dc, offset, pagesize);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GameType> typeList()	{
		return gameTypeDao.findAll();
	}
	
	@Resource
	public void setConfig(Config pathConfig)	{
		this.HTTP_PATH = pathConfig.getFileServerUrl();
	}
	
	@Override
	public <T> JsonObject appToJson(T app)	{
		GameInfo g = (GameInfo)app;
		JsonObject json = new JsonObject();
		json.addProperty("softid", g.getId() + "");
		json.addProperty("softname", g.getSoftName() + "");
		json.addProperty("kaifamanu", g.getKaifamanu() + "");
		json.addProperty("softVersion", g.getSoftVersion() + "");
		json.addProperty("fileSize", g.getFileSize() + "");
		json.addProperty("softFee", g.getSoftFee() + "");
		json.addProperty("softtype", g.getGameType() == null ? "" : g.getGameType().getTypeName() + "");
		json.addProperty("shangjiadate", g.getShangjiadate() + "");
		json.addProperty("softinfo", g.getSoftInfo() + "");
		json.addProperty("ostype", g.getOsType() + "");
		json.addProperty("osid", g.getOsInfo() == null ? "" : g.getOsInfo().getId() + "");
		json.addProperty("suffix", g.getSoftSuffix() == null ? "" : g.getSoftSuffix().getSuffixName() + "");
		
		String s1 = g.getIconFileName();
		if(s1 == null || "".equals(s1.trim()))
			json.addProperty("iconfilename", "");
		else
			json.addProperty("iconfilename",HTTP_PATH + s1.replace("\\", "/"));
		
		String s2 = g.getPicFileName1();
		if(s2 == null || "".equals(s2.trim()))
			json.addProperty("picfilename1", "");
		else
			json.addProperty("picfilename1", HTTP_PATH + s2.replace("\\", "/"));
		
		String s3 = g.getPicFileName2();
		if(s3 == null || "".equals(s3.trim()))
			json.addProperty("picfilename2", "");
		else
			json.addProperty("picfilename2", HTTP_PATH + s3.replace("\\", "/"));
		
		String s4 = g.getPicFileName3();
		if(s4 == null || "".equals(s4.trim()))
			json.addProperty("picfilename3", "");
		else
			json.addProperty("picfilename3", HTTP_PATH + s4.replace("\\", "/"));
		
		String s5 = g.getSoftFileName();
		if(s5 == null || "".equals(s5.trim()))
			json.addProperty("softfilename", "");
		else
			json.addProperty("softfilename", HTTP_PATH + s5.replace("\\", "/"));

		String s6 = g.getLegalFileName();
		if(s6 == null || "".equals(s6.trim()))
			json.addProperty("legalfilename", "");
		else
			json.addProperty("legalfilename", HTTP_PATH + s6.replace("\\", "/"));
		
		json.addProperty("softurl", g.getSoftUrl() + "");
		json.addProperty("hotrecmd", g.getHotRecmd() + "");
		json.addProperty("downnum", g.getDownNum() + "");
		json.addProperty("installCount", g.getInstallCount() + "");
		json.addProperty("userRemarkNum", g.getUserRemarkNum() == null ? "" : g.getUserRemarkNum() + "");
		json.addProperty("userRemark", g.getUserRemark() == null ? "" : g.getUserRemark() + "");
		
		return json;
	}
}

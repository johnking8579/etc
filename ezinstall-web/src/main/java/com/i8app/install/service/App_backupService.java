package com.i8app.install.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.App_backupDao;
import com.i8app.install.domain.AppInstallPack;
import com.i8app.install.domain.App_backup;
import com.i8app.install.domain.Image_backup;
@Component
public class App_backupService {
	@Resource
	private App_backupDao app_backupDao;
	public App_backup findById(int id){
		return app_backupDao.findById(id);
	}
	public void saveOrUpdateApp_backup(App_backup app){
		app_backupDao.saveOrUpdateApp_backup(app);
	}
	public void delApp_backup(App_backup app){
		app_backupDao.delApp_backup(app);
	}
	
	public Image_backup findimageById(int id,String uid){
		return app_backupDao.findimageById(id,uid);
	}
	public void saveOrUpdateApp_backup(Image_backup app){
		app_backupDao.saveOrUpdateApp_backup(app);
	}
	public void delImage_backup(Image_backup app){
		app_backupDao.delImage_backup(app);
	}
	
	public App_backup findAppByUPV(String peopleId,String pname,String version){
		return app_backupDao.findAppByUPV(peopleId, pname, version);
	}
	public AppInstallPack findAppByPnameAndVer(String pname,int version){
		return app_backupDao.findAppByPnameAndVer(pname, version);
	}
	public AppInstallPack findMaxAppByPname(String pname){
		return app_backupDao.findMaxAppByPname(pname);
	}
	public List<App_backup> findAppByPeopleId(String peopleId){
		return app_backupDao.findAppByPeopleId(peopleId);
	}
	
	public List<Image_backup> findImageByPeopleId(String peopleId){
		return app_backupDao.findImageByPeopleId(peopleId);
	}
	public String app_backupIcon(String pname) {
		return app_backupDao.app_backupIcon(pname);
	}
	
	public App_backup findapp_backup(String uid,String pname,String version){
		return app_backupDao.findapp_backup(uid, pname, version);
	}
	public Image_backup findImageByPidAndName(String peopleId,String name){
		return app_backupDao.findImageByPidAndName(peopleId, name);
	}
	public App_backup findAppByPe(String peopleId,String pname){
		return app_backupDao.findAppByPe(peopleId, pname);
	}
}

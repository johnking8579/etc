package com.i8app.install.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.EmpDao;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.GroupPrivilege;
import com.i8app.install.domain.PrivilegeInfo;
import com.i8app.install.domain.UserGroupInfo;
import com.i8app.install.domain.Userdatarestrict;
@Component("EmployeeService")
public class EmployeeService {
	private EmpDao empDao;
	@Resource
	public void setEmpDao(EmpDao empDao) {
		this.empDao = empDao;
	}
	public Employee findByEmpnoAndProvidLogin(String empNo, Integer provId) {
		return empDao.findByEmpnoAndProvidLogin(empNo, provId);
	}
	public List<Employee> findEmpListByEmpnoAndProvidLogin(String empNo, Integer provId) {
		return empDao.findEmpListByEmpnoAndProvidLogin(empNo, provId);
	}
	public List<Userdatarestrict> findUserdatarestrictByEmpId(Integer empId) {
		return empDao.findUserdatarestrictByEmpId(empId);
	}
	public List<UserGroupInfo> findUserGroupInfoByEmpId(Integer empId) {
		return empDao.findUserGroupInfoByEmpId(empId);
	}
	public List<PrivilegeInfo> findPrivilegeByEmpId(Integer empId) {
		List<PrivilegeInfo> privilegeInfoList = new ArrayList<PrivilegeInfo>();
		List<UserGroupInfo> usergroup = empDao.findUserGroupInfoByEmpId(empId);
		for(UserGroupInfo u :usergroup){
			List<GroupPrivilege> groupPrivilegeList = empDao.findGroupPrivilegeBygroupId(u.getGroupId());
			for(GroupPrivilege g:groupPrivilegeList){
				if(g.getPrivilegeId() !=null){
					privilegeInfoList.add(g.getPrivilegeId());
				}
			}
		}
		return privilegeInfoList;
	}
}

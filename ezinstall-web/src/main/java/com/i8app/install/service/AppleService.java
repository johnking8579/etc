package com.i8app.install.service;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.i8app.ezinstall.common.Util;
import com.i8app.install.dao.AppleDao;
import com.i8app.install.domain.AppleUser;
import com.i8app.install.domain.Applelog;
/**
 * @author LiXiangyang 2014-05-08
 */
@Service
public class AppleService implements ApplicationContextAware{
	private static Logger logger = Logger.getLogger(AppleService.class);
	public static final String 
	BASE_URL 	= 	"http://api1.apptree.com.cn/other/hengyuan/channelcommit.jsp?";//
	@Resource
	public AppleDao appleDao;
	private ApplicationContext applicationContext;
	public void saveOrUpdateAppleUser(AppleUser user)	{
		appleDao.saveOrUpdateAppleUser(user);
	}
	public void saveOrUpdateApplelog(Applelog log)	{
		appleDao.saveOrUpdateApplelog(log);
	}
	public void updateadudid(){
		appleDao.updateadudid();
	}
	public List<Applelog> findApplelog(String mac,Integer rscid,String adudid){
		return appleDao.findApplelog(mac, rscid, adudid);
	}
	public Applelog findApplelogbyid(Integer id){
		return appleDao.findApplelogbyid(id);
	}
	public void asynccheckActivate(String callback,String mac){
		ChannelcommitThread chanel= applicationContext.getBean(ChannelcommitThread.class);
		chanel.setCallback(callback);
		chanel.setMac(mac);
		new Thread(chanel).start();
	}
	public String findAdudid(String uuid){
		return appleDao.findAdudid(uuid);
	}
	public String checkActivate(String callback) throws UnsupportedEncodingException{
		logger.info("开始访问葫芦接口");
//		appleDao.updateadudid();//
		List<AppleUser> userList = appleDao.findAppleUser();
		for(AppleUser u:userList){
			try{
				callback = String.format("%slogid=%s", callback,u.getUser_id());
				String callbackurlencoder = URLEncoder.encode(callback, "utf-8");
				String url = String.format("%ssignid=713&idfa=%s&mac=%s&adudid=%s&callback=%s&openudid=%s&phoneversion=%s", BASE_URL,u.getIdfa(),u.getMac(),u.getSign(),callbackurlencoder,u.getOpenudid(),u.getVersion());
				JsonObject result = doGet(url);
				if(result !=null && "000000".equals(result.get("resultcode").getAsString()) && result.get("isok").getAsInt()==1)	{
//					List<Applelog> logList = appleDao.findApplelog(u.getMac(), u.getApp_id(), u.getSign());
//					for(Applelog a :logList){
//						a.setIsOk(1);//isOk==1为成功,isOk==0为初始状态,isOk==2为错误
//						appleDao.saveOrUpdateApplelog(a);
//					}
					Applelog log = appleDao.findApplelogbyid(u.getUser_id());
					if(log !=null){
						log.setIsOk(1);
						appleDao.saveOrUpdateApplelog(log);
					}
					logger.info("苹果树推广软件接口返回成功");
				}else{
					Applelog log = appleDao.findApplelogbyid(u.getUser_id());
					if(log !=null){
						log.setIsOk(2);
						appleDao.saveOrUpdateApplelog(log);
					}
					logger.error("苹果树推广软件接口返回错误");
				}
			}catch(Exception e){
				
			}
		}
		return null;
	}
	private JsonObject doGet(String url) 	{
		InputStream is = null;
		JsonObject json = null;
		try {
			HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
			is = conn.getInputStream();
			json = new JsonParser().parse(new InputStreamReader(is,"UTF-8")).getAsJsonObject();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally	{
			Util.closeStream(is);
		}
		return json;
	}
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;		
	}
}

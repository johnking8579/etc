package com.i8app.install.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.DriverDao;
import com.i8app.install.domain.DriverInfo;
import com.i8app.install.service.DriverService;

@Component("driverService")
public class DriverService{

	private DriverDao driverDao;
	
	//PID写入数据库表时应变成如下格式：　,xxxxx,,yyyyy,,zzzzzz,
	private static final String SPLITTER= ",";

	public void addOrUpdate(DriverInfo ur) {
		driverDao.addOrUpdate(ur);
	}

	public DriverInfo findById(int id) {
		return driverDao.findById(id);
	}

	/**
	 * 处理上传的文件, 生成网络可访问地址, 生成本地磁盘地址.
	 * 同时写入磁盘文件并更新数据库记录
	 * 
	 */
	public String saveAndWriteToDisk(String vid, String pid,
			String httpRootPath, String realRootPath, InputStream inputStream) {
		
		//TODO fileChidPath根据业务需要来定
		String fileChildPath = "DriverUpload";  
		//文件名的自动生成规则 : vid+pid+时间戳+.zip
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		String timestamp = sdf.format(new Date());
		String fileName = String.format("%s_%s_%s.zip", vid, pid, timestamp);
		String realfileFullName = String.format("%s%s\\%s", realRootPath, fileChildPath, fileName);
		String httpFileFullName = String.format("%s%s/%s", httpRootPath, fileChildPath, fileName);
		File file = new File(realfileFullName);
		if(!file.getParentFile().exists())
			file.getParentFile().mkdirs();
		
		String message = null;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		int i;
		bis = new BufferedInputStream(inputStream);
		
		try {
			bos = new BufferedOutputStream(new FileOutputStream(realfileFullName));
			while((i = bis.read()) != -1)	{
				bos.write(i);
			}
			
			//存入数据库
			DriverInfo driverInfo = new DriverInfo();
			driverInfo.setPids(pid);
			driverInfo.setVid(vid);
			driverInfo.setFilePath(httpFileFullName);
			this.addOrUpdate(driverInfo);

			message = "{\"status\":\"success\"}";
		} catch (Exception e) {
			message = "{\"status\":\"failed\"}";
			e.printStackTrace();
		} finally	{
			if(bis != null)	{
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(bos != null)	{
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return message;
	}
	
	@Resource
	public void setDriverDao(DriverDao driverDao) {
		this.driverDao = driverDao;
	}

	public void del(int id) {
		driverDao.del(id);
		
	}

	public List<DriverInfo> list() {
		return driverDao.list();
	}

	public List<DriverInfo> list(String vid, String pid) {
		return driverDao.list(vid, SPLITTER + pid + SPLITTER);
	}
	
	public DriverInfo getUnique(String vid, String pid)	{
		return this.driverDao.getUnique(vid, SPLITTER + pid + SPLITTER);
	}

	public List<String> getPidList(String vid) {
		// TODO Auto-generated method stub
		return null;
	}

	public DriverInfo getUnique(String vid, String pid, int osType) {
		return driverDao.getUnique(vid, pid, osType);
	}

	public List<DriverInfo> list(String vid, String pid, int osType) {
		return this.driverDao.list(vid, pid, osType);
	}

	public DriverDao getDriverDao() {
		return driverDao;
	}


}

package com.i8app.install.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.i8app.install.dao.AppInstallPackDao;
import com.i8app.install.dao.EmpDao;
import com.i8app.install.dao.InstallLogDao;
import com.i8app.install.dao.MobileActivateLogDao;
import com.i8app.install.dao.SoftInfoDao;
import com.i8app.install.domain.AppInstallPack;
import com.i8app.install.domain.BusinessHall;
import com.i8app.install.domain.Employee;
import com.i8app.install.domain.InstallLog;
import com.i8app.install.domain.MobileActivateLog;
import com.i8app.install.domain.SoftInfo;

/**
 * 日志service
 * 
 * @author JingYing 2013-6-7
 */
@Service
public class LogService {
	
	@Resource
	private InstallLogDao installLogDao;
	@Resource
	private EmpDao empDao;
	@Resource
	private SoftInfoDao softInfoDao;
	@Resource
	private AppInstallPackDao appInstallPackDao;
	@Resource
	private MobileActivateLogDao mobileActivateLogDao;
	

	/**
	 * 生成短信下载日志
	 * @param installType
	 * @param empId
	 * @param installPackUuid
	 * @param no 
	 * @return 生成的日志id
	 */
	public int buildSmsInstallLog(int installType, int empId, String installPackUuid, String no, int optStatus)	{
		Employee emp = empDao.findById(empId);
		AppInstallPack pack = appInstallPackDao.findById(installPackUuid);
		if(emp == null)	{
			throw new IllegalArgumentException("无效的empid:" + empId);
		}
		if(pack == null)	{
			throw new IllegalArgumentException("无效的安装包ID:" + installPackUuid);
		}
		
		BusinessHall hall = emp.getHall();
		
		InstallLog log = new InstallLog();
		log.setAppUuid(pack.getApp().getUuid());
		log.setCityId(emp.getCity().getCityID());
		log.setCityName(emp.getCity().getCityName());
		log.setCountyId(emp.getCounty().getCountyID());
		log.setCountyName(emp.getCounty().getCountyName());
		log.setEmpId(empId+"");
		log.setEmpName(emp.getEmpName());
		log.setEmpNo(emp.getEmpno());
//		log.setFilterrule(filterrule);
		log.setImei("");
		log.setImsi("");
		log.setInstallTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		log.setInstallType(installType);
		log.setIsAutoOpen(0);
//		log.setIsGuide(isGuide);
		log.setIsOtherNet(0);
		log.setIsSmsVal(0);
//		log.setManuName(manuName);
		log.setMarkerType(pack.getApp().getMarkerType());
//		log.setModelName(modelName);
		log.setOptStatus(optStatus);	//70001:用户已点击短信链接, 70002:短信链接已下载完成,70003:手机获取验证码状态,70004:短信下发量日志
		log.setOstype(this.toOsType(pack.getOs()));
//		log.setOsVersion(osVersion);
		log.setPhoneNumber(no);
		log.setProvinceId(emp.getProvince().getProvinceID());
		log.setProvinceName(emp.getProvince().getProvinceName());
		log.setPackUuid(installPackUuid);
//		log.setPsn(psn);
		log.setRscid(pack.getOriginalPackId());
		log.setRscName(pack.getApp().getName());
		log.setRscVersion(pack.getAppVer());
		log.setSeqNo("");
		log.setSource(this.toSource(pack.getCpId()));
		log.setSourceType(pack.getApp().getRscTypeId());	//?
//		log.setTypeid(typeid);
		log.setUuid(UUID.randomUUID().toString());
//		log.setVersionId(versionId);
		
		if(hall != null){
			log.setBhid(hall.getBhid());	//?
			log.setCcid(hall.getCcid());
			log.setChanGrade(hall.getChanGrade());
			log.setChanLevel(hall.getChanLevel());
			log.setChanType(hall.getChanType());
			log.setCtid(hall.getCtid());
			log.setHallId(hall.getHallID());
			log.setHallName(hall.getHallName());
			log.setHallno(hall.getHallNo());
			log.setRccid(hall.getRccid());
		}
		
		installLogDao.persist(log);
		return log.getId();
	}
	
	
	private int toOsType(String os)	{
		if("os1".equals(os))	{
			return 200;
		} else if("os2".equals(os))	{
			return 300;
		} else if("os3".equals(os))	{
			return 300;
		} else if("os4".equals(os))	{
			return 100;
		} else if("os5".equals(os))	{
			return 500;
		} else if("os6".equals(os))	{
			return 400;
		} else 
			throw new IllegalArgumentException("os:" + os);
	}
	
	private int toSource(String cpId)	{
		if("cp01".equals(cpId) || "cp02".equals(cpId) || "cp07".equals(cpId) || "cp08".equals(cpId) || "cp09".equals(cpId))	{
			return 0;
		} else if("cp03".equals(cpId))	{
			return 2;
		} else if("cp04".equals(cpId))	{
			return 3;
		} else if("cp05".equals(cpId))	{
			return 1;
		} else if("cp06".equals(cpId))	{
			return 5;
		} else
			throw new IllegalArgumentException("cpId:" + cpId);
	}
	
	/**
	 * 拼接日志字段,存入数据库
	 * @param log
	 */
	public void completeMobileActivateLog(MobileActivateLog log)	{
		Employee emp = empDao.findByEmpnoAndProvid(log.getEmpno(), log.getProvid());
		
		BusinessHall hall = emp.getHall();
		log.setCcid(hall.getCcid());
		log.setCityid(hall.getCity().getCityID());
		log.setCityname(hall.getCity().getCityName());
		log.setCountyid(hall.getCounty().getCountyID());
		log.setCountyname(hall.getCounty().getCountyName());
		log.setCtid(hall.getCtid());
		log.setDetectType(log.getDetectType());
		log.setDeviceNo(log.getDeviceNo());
		log.setEmpname(emp.getEmpName());
		log.setHallid(hall.getHallID());
		log.setHallname(hall.getHallName());
		log.setManuName(log.getManuName());
		log.setNewtime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		log.setOstype(log.getOstype());
		log.setPhoneNo(log.getPhoneNo());
		log.setProvname(hall.getProvince().getProvinceName());
		log.setRccid(hall.getRccid());
			
		Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(log.getRscid());
        if(!isNum.matches()){
        	String name = softInfoDao.findSoftByuuid(log.getRscid());
        	log.setRscname(name);
        }else{
        	if(log.getRscid().length()<10){
	        	SoftInfo s = softInfoDao.findById(Integer.parseInt(log.getRscid()));
	        	log.setRscname(s.getSoftName());
        	}
        }

		
		log.setSourcetype("101");	//暂时默认softinfo
//		log.setVersionCode(versionCode);
//		log.setVersionName(versionName);
		
		mobileActivateLogDao.persist(log);
	}
	
	/**
	 * 存下载日志
	 * @param installLog
	 */
	public void persistInstallLog(InstallLog installLog)	{
		installLogDao.persist(installLog);
	}
	
	/**
	 * 查找下载日志
	 * @param logId
	 * @return
	 */
	public InstallLog findInstallLogById(int logId)	{
		return installLogDao.findById(logId);
	}
	
	/**
	 * 更新下载日志的OPTSTATUS记录
	 * @param logId
	 * @param optStatus
	 */
	public void updateInstallLogOptStat(int logId, int optStatus)	{
		installLogDao.updateOptStat(logId, optStatus);
	}
	
	/**
	 * 手机客户端安装日志
	 * @param installType
	 * @param empId
	 * @param installPackUuid
	 * @param no
	 * @param optStatus
	 * @return
	 */
	public void buildPhoneInstallLog(InstallLog log,String installPackUuid)	{
		AppInstallPack pack = appInstallPackDao.findById(installPackUuid);
		if(pack == null)	{
			throw new IllegalArgumentException("无效的安装包ID:" + installPackUuid);
		}
		
		log.setAppUuid(pack.getApp().getUuid());
		log.setMarkerType(pack.getApp().getMarkerType());
		log.setOstype(this.toOsType(pack.getOs()));
		log.setPackUuid(installPackUuid);
		log.setRscid(pack.getOriginalPackId());
		log.setRscName(pack.getApp().getName());
		log.setRscVersion(pack.getAppVer());
		log.setSource(this.toSource(pack.getCpId()));
		log.setSourceType(pack.getApp().getRscTypeId());	//?
		
		installLogDao.persist(log);
	}
}

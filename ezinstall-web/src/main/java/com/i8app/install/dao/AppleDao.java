package com.i8app.install.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.AppleUser;
import com.i8app.install.domain.Applelog;
@SuppressWarnings("rawtypes")
@Repository
public class AppleDao extends BaseDao {
	public void saveOrUpdateAppleUser(AppleUser user)	{
		getHibernateTemplate().saveOrUpdate(user);
	}
	public void saveOrUpdateApplelog(Applelog log)	{
		getHibernateTemplate().saveOrUpdate(log);
	}
	public void updateadudid(){
		String hql = "UPDATE applelog SET adudid=IF((SELECT adudid FROM appletree WHERE appid=rscid) IS NOT NULL,(SELECT adudid FROM appletree WHERE appid=rscid),'') WHERE adudid IS NULL AND isOk=0;";
		Session s = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
		@SuppressWarnings("deprecation")
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public List<AppleUser> findAppleUser(){
		String hql = "SELECT u.idfa,u.mac,l.adudid,u.openudid,l.rscid,l.id,u.mobile_client_version FROM appleuser u LEFT JOIN applelog l ON u.mac=l.mac WHERE adudid<>'' AND isOk=0 ;";
		List<AppleUser> cList  =new ArrayList<AppleUser>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			res = pstat.executeQuery();
			while (res.next()) {
				AppleUser c = new AppleUser();
				c.setIdfa(res.getString("idfa"));
				c.setMac(res.getString("mac"));
				c.setOpenudid(res.getString("openudid"));
				c.setSign(res.getString("adudid"));
//				c.setApp_id(res.getInt("rscid"));
				c.setUser_id(res.getInt("id"));
				c.setVersion(res.getString("mobile_client_version"));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	public List<AppleUser> findAppleUser(String mac){
		String hql = "SELECT u.idfa,u.mac,l.adudid,u.openudid,l.rscid,l.id,u.mobile_client_version FROM appleuser u LEFT JOIN applelog l ON u.mac=l.mac WHERE l.adudid<>'' AND l.isOk=0 AND l.mac=?;";
		List<AppleUser> cList  =new ArrayList<AppleUser>() ;
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		@SuppressWarnings("deprecation")
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, mac);
			res = pstat.executeQuery();
			while (res.next()) {
				AppleUser c = new AppleUser();
				c.setIdfa(res.getString("idfa"));
				c.setMac(res.getString("mac"));
				c.setOpenudid(res.getString("openudid"));
				c.setSign(res.getString("adudid"));
//				c.setApp_id(res.getInt("rscid"));
				c.setUser_id(res.getInt("id"));
				c.setVersion(res.getString("mobile_client_version"));
				cList.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cList;
	}
	
	
	public String findAdudid(String uuid){
		String str = null;
		String hql = "SELECT a.adudid FROM appletree a JOIN i8app i ON i.sourceid=a.appid JOIN app_install_pack p ON CONVERT(i.id,CHAR(255))=p.originalPackId   WHERE p.uuid=?;";
		Session s = this.getHibernateTemplate().getSessionFactory()
		.getCurrentSession();
		@SuppressWarnings("deprecation")
		Connection conn = s.connection();
		PreparedStatement pstat = null;
		ResultSet res = null;
		try {
			pstat = conn.prepareStatement(hql);
			pstat.setString(1, uuid);
			res = pstat.executeQuery();
			while (res.next()) {
				str = res.getString("adudid");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (res != null)
					res.close();
				if (pstat != null)
					pstat.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return str;
	}
	public List<Applelog> findApplelog(String mac,Integer rscid,String adudid){
		String hql ="FROM Applelog a WHERE a.mac=? AND a.rscid=? AND a.adudid=? AND a.isOk=0";
		@SuppressWarnings("unchecked")
		List<Applelog> logList = getHibernateTemplate().find(hql,mac,rscid,adudid);
//		return logList.isEmpty()?null:logList.get(0);
		return logList;
	}
	
	public Applelog findApplelogbyid(Integer id){
		return getHibernateTemplate().get(Applelog.class, id);
	}
}

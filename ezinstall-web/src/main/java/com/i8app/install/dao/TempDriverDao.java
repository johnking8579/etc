package com.i8app.install.dao;

import org.springframework.stereotype.Component;

import com.i8app.install.dao.TempDriverDao;
import com.i8app.install.domain.TempDriverInfo;

@SuppressWarnings("rawtypes")
@Component("tempDriverDao")
public class TempDriverDao extends BaseDao{

	public void addOrUpdate(TempDriverInfo tempDriverInfo) {
		this.getHibernateTemplate().saveOrUpdate(tempDriverInfo);
	}

//	public void save(TempDriverInfo td) throws SQLException, ClassNotFoundException	{
//		String sql = "insert into TempDriverInfo(vid, pid, httpPath) values(?,?,?)";
//		Connection conn =  DataBase.getConnection();
//
//		PreparedStatement ps = conn.prepareStatement(sql);
//		ps.setString(1, td.getVid());
//		ps.setString(2, td.getPid());
//		ps.setString(3, td.getHttpPath());
//
//		ps.executeUpdate();
//
//		DataBase.close(ps);
//		DataBase.close(conn);
//	}
	

	



}

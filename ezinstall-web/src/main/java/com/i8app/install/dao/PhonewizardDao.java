package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.Employee;
import com.i8app.install.domain.Mobile;
import com.i8app.install.domain.Packageinfo;
import com.i8app.install.domain.Phonewizard;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Component("phonewizardDao")
public class PhonewizardDao extends BaseDao{

	public void addOrUpdate(Phonewizard ur) {
		this.getHibernateTemplate().saveOrUpdate(ur);
	}
	public void addOrUpdate(Packageinfo ur1) {
		this.getHibernateTemplate().saveOrUpdate(ur1);
	}
	public void addOrUpdate(Mobile ur2) {
		this.getHibernateTemplate().saveOrUpdate(ur2);
	}
	public Phonewizard findById(int id) {
		return (Phonewizard) this.getHibernateTemplate().get(Phonewizard.class,
				id);
	}
	public void del(int id) {
		this.getHibernateTemplate().delete(this.findById(id));
	}
	public List<Phonewizard> list() {
		return (List<Phonewizard>) this.getHibernateTemplate().find(
				"from Phonewizard");
	}
	public Employee findByIdandOstype(final String empno,final int provinceId){
		return (Employee)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("select s from Employee s where s.empno=? and s.province.provinceID=?")
				.setString(0, empno).setInteger(1, provinceId).uniqueResult();
			}
		});
	}
	public Mobile findMobileByDevice(final String device){
		return (Mobile)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("select s from Mobile s where s.deviceNo=? and s.sourcetype=1")
				.setString(0, device).list().size()==0?null:session.createQuery("select s from Mobile s where s.deviceNo=? and s.sourcetype=1")
						.setString(0, device).list().get(0);
			}
		});
	}
	public List<Packageinfo> findByDevice(final String empno){
		return (List<Packageinfo>)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				return session.createQuery("select s from Packageinfo s where s.deviceNo=? order by s.installTime desc")
				.setString(0, empno).list();
			}
		});
	}
	
	public Packageinfo findByPackage(final String deviceNo,final String packageName){
		return (Packageinfo)getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session)throws HibernateException,SQLException{
				List<Packageinfo> pageList = session.createQuery("from Packageinfo s where s.deviceNo=? and s.apkPkgName=?")
				.setString(0, deviceNo).setString(1, packageName).list();
				return pageList.isEmpty()?null:pageList.get(0);
						
			}
		});
	}
}

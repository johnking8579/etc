package com.i8app.install.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.install.domain.GameType;

@SuppressWarnings("rawtypes")
@Component
public class GameTypeDao extends BaseDao	{
	
	@SuppressWarnings("unchecked")
	public List<GameType> findAll()	{
		String hql = "from GameType order by num, id";
		return this.getHibernateTemplate().find(hql);
	}

}

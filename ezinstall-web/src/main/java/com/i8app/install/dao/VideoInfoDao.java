package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.domain.VideoInfo;
/**
 * VideoInfo的Dao类
 * @author JingYing
 *
 */
@SuppressWarnings("rawtypes")
@Component("videoInfoDao")
public class VideoInfoDao extends BaseDao{

	public void del(int id) {
		this.getHibernateTemplate().delete(findById(id));
	}

	public VideoInfo findById(int id) {
		return (VideoInfo) this.getHibernateTemplate().get(VideoInfo.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<VideoInfo> list() {
		return this.getHibernateTemplate().find("from VideoInfo");
	}
	
	@SuppressWarnings("unchecked")
	public List<VideoInfo> list(final String videoName, final int videoTypeId)	{
		return this.getHibernateTemplate().executeFind(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				StringBuilder sb = new StringBuilder();
				sb.append("from VideoInfo v where 1=1 ");
				if(videoName != null && !"".equals(videoName))
					sb.append(String.format(" and v.videoName like '%%%s%%'", videoName));
				if(videoTypeId != 0)
					sb.append(String.format(" and v.videoType.videoTypeId = %d ", videoTypeId));
				return session.createQuery(sb.toString()).list();
			}
		});
	}

	public void addOrUpdate(VideoInfo videoInfo) {
		this.getHibernateTemplate().saveOrUpdate(videoInfo);
	}

	@SuppressWarnings("unchecked")
	public List<VideoInfo> list(int pageNo, final int pageSize) {
		final int begin = (pageNo-1) * pageSize;
		return (List<VideoInfo>) this.getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery("from VideoInfo").setFirstResult(begin).setMaxResults(pageSize).list();
			}
		});
	}
	public int count()	{
		return list().size();
	}

	public void del(int[] ids) {
		for (int i = 0; i < ids.length; i++) {
			this.getHibernateTemplate().delete(findById(ids[i]));
		}
	}

	public void del(String[] ids) {
		for (int i = 0; i < ids.length; i++) {
			this.getHibernateTemplate().delete(findById(Integer.parseInt(ids[i])));
		}
	}
}

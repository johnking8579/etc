package com.i8app.install.dao;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import com.i8app.install.domain.ShortUrl;

@Repository
public class ShortUrlDao extends BaseDao<ShortUrl>{
	
	public void persist(ShortUrl shortUrl)	{
		try {
			getHibernateTemplate().persist(shortUrl);
			getHibernateTemplate().flush();
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();	//主键冲突时不向外抛异常
		}
	}
	
	public ShortUrl findById(String id)	{
		return (ShortUrl)getHibernateTemplate().get(ShortUrl.class, id);
	}

}

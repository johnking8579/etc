package com.i8app.install.dao;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.MobileActivateLog;

@Repository
public class MobileActivateLogDao extends BaseDao{

	public void persist(MobileActivateLog log)	{
		getHibernateTemplate().saveOrUpdate(log);
	}
	
}

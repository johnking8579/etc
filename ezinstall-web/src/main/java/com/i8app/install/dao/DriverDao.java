package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.dao.DriverDao;
import com.i8app.install.domain.DriverInfo;

@SuppressWarnings("rawtypes")
@Component("driverDao")
public class DriverDao extends BaseDao{

	public void addOrUpdate(DriverInfo ur) {
		this.getHibernateTemplate().saveOrUpdate(ur);
	}

	public DriverInfo findById(int id) {
		return (DriverInfo) this.getHibernateTemplate().get(DriverInfo.class,
				id);
	}

	public void del(int id) {
		this.getHibernateTemplate().delete(this.findById(id));
	}

	@SuppressWarnings("unchecked")
	public List<DriverInfo> list() {
		return (List<DriverInfo>) this.getHibernateTemplate().find(
				"from DriverInfo");
	}

	/**
	 * @deprecated 需要修改以适应数据库表的形式
	 */
	@SuppressWarnings("unchecked")
	public List<DriverInfo> list_deprecated(final String vid, final String pid) {
		return (List<DriverInfo>) this.getHibernateTemplate().execute(
				new HibernateCallback() {

					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						StringBuilder sb = new StringBuilder(
								"from DriverInfo where 1=1 ");
						if ((vid != null) && (!"".equals(vid)))
							sb.append(String.format(" and vid like '%%%s%%' ",
									vid));
						if ((pid != null) && (!"".equals(pid)))
							sb.append(String.format(" and pid like '%%%s%%' ",
									pid));
						return session.createQuery(sb.toString()).list();
					}

				});
	}

	@SuppressWarnings("unchecked")
	public List<DriverInfo> list(final String vid, final String pid) {
		return (List<DriverInfo>)this.getHibernateTemplate().execute(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery(String.format( 
							"from DriverInfo d where d.vid = '%s' and d.pids like '%%%s%%'",
										vid, pid)).list();
			}
		});
	}
	
	public DriverInfo getUnique(String vid, String pid)	{
		List<DriverInfo> list = this.list(vid, pid);
		if(list.size() == 0)
			return null;
		else
			return list.get(0);
	}

	public String getPids(int id) {
		return this.findById(id).getPids();
	}

	@SuppressWarnings("unchecked")
	public String getPids(final String vid) {

		return (String) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						return session.createQuery(
								"from DriverInfo d where d.vid = " + vid)
								.uniqueResult();
					}

				});
	}

	public boolean isExist(String vid, String pid) {
		return false;
	}

	public DriverInfo getUnique(String vid, String pid, int osType) {
		List<DriverInfo> list = this.list(vid, pid, osType);
		if(list.size() == 0)
			return null;
		else
			return list.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<DriverInfo> list(final String vid, final String pid, final int osType) {
		return (List<DriverInfo>)this.getHibernateTemplate().execute(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				return session.createQuery(String.format( 
							"from DriverInfo d where d.vid = '%s' and d.pids like '%%%s%%' and d.osType = %d",
										vid, pid, osType)).list();
			}
		});
	}

}

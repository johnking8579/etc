package com.i8app.install.dao;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.GameInstallLog;

@SuppressWarnings("rawtypes")
@Repository("gameInstallLogDao")
public class GameInstallLogDao extends BaseDao {
	
	public void saveOrUpdate(GameInstallLog log)	{
		this.getHibernateTemplate().saveOrUpdate(log);
	}

}

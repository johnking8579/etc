package com.i8app.install.dao;

import org.springframework.stereotype.Repository;

import com.i8app.install.domain.InstallLog;

@Repository
public class InstallLogDao extends BaseDao{

	public void persist(InstallLog installLog)	{
		getHibernateTemplate().persist(installLog);
		getHibernateTemplate().flush();
	}
	
	public InstallLog findById(int id)	{
		return (InstallLog)getHibernateTemplate().get(InstallLog.class, id);
	}

	public void updateOptStat(int logId, int optStatus) {
		String hql = "update InstallLog set optStatus = ? where id = ?";
		getHibernateTemplate().bulkUpdate(hql, optStatus, logId);
	}
}

package com.i8app.install.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Component;

import com.i8app.install.dao.GameInfoDao;
import com.i8app.install.domain.GameInfo;

@SuppressWarnings("rawtypes")
@Component("gameInfoDao")
public class GameInfoDao extends BaseDao	{

	@SuppressWarnings("unchecked")
	public List<GameInfo> findByCriteria(DetachedCriteria dc) {
		return this.getHibernateTemplate().findByCriteria(dc);
	}

	public GameInfo findById(int id) {
		return (GameInfo) this.getHibernateTemplate().get(GameInfo.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<GameInfo> list() {
		return this.getHibernateTemplate().find("from GameInfo");
	}

	@SuppressWarnings({ "unchecked" })
	public List<GameInfo> list(final int pageNo, final int pageSize) {
		final int begin = (pageNo - 1) * pageSize;
		return (List<GameInfo>) this.getHibernateTemplate().execute(
				new HibernateCallback() {
					public Object doInHibernate(Session session)
							throws HibernateException, SQLException {
						return session.createQuery("from GameInfo")
								.setFirstResult(begin).setMaxResults(pageSize)
								.list();
					}
				});
	}
	
	public int count()	{
		return list().size();
	}

	@SuppressWarnings("unchecked")
	public List<GameInfo> findByCriteria(DetachedCriteria dc, int pageNo,
			int pageSize) {
		int begin = (pageNo - 1) * pageSize;
		return this.getHibernateTemplate().findByCriteria(dc, begin, pageSize); //起始查询数为0, 不是1
	}

}

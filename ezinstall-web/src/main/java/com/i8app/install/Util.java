package com.i8app.install;

import java.io.Closeable;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.i8app.install.domain.Tongyong;

public class Util {

	/**
	 * 网络地址的分隔符
	 */
	public static final String SPLITTER = "/";
	
	/**
	 * 过滤字符串, 防止json拼串时出现特殊字符
	 * 
	 * @param s
	 * @return
	 */
	public static String jsonHandler(String s) {
		if (s != null)
			return s.replace(",", "，").replace(":", "：").replace("{", "『")
					.replace("}", "』").replace("[", "【").replace("]", "】")
					.replace("\"", "“").replace("\r", "").replace("\t", "")
					.replace("\n", "").trim();
		else
			return null;
	}

	public static String createTimestamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return sdf.format(new Date());
	}

	/**
	 * 参考格式"yyyyMMddHHmmssSSS"、"yyyy-MM-dd HH:mm:ss"、"yyyyMMdd";
	 * @param formatString 拼接时间的格式
	 * @return 获得对应的时间
	 */
	public static String createTimes(String formatString) {
		return new SimpleDateFormat(formatString).format(new Date());
	}
	/**
	 * 
	 * @param formatString 
	 * <br>0: yyyy-MM-dd HH:mm:ss, 
	 * <br>1: yyyyMMddHHmmss, 
	 * <br>2: yyyyMMddHHmm, 
	 * <br>3: yyyyMMdd, 
	 * <br>default: yyyyMMddHHmmssSSS
	 * @return
	 */
	public static String createTimes(int formatString) {
		switch (formatString) {
		case 0:
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		case 1:
			return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		case 2:
			return new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
		case 3:
			return new SimpleDateFormat("yyyyMMdd").format(new Date());
		default:
			return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
		}
	}

	/**
	 * 从文件名中截取出后缀名
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getSuffixName(String fileName) {
		String[] s = fileName.split("[.]");
		return "." + s[s.length - 1];
	}

	public static long softVerToInt(String version) {
		int[] ints = toIntArray(version.split("[.]"));
		long total = 0L;
		for (int i = 0, j = ints.length - 1; i < ints.length; i++, j--) {
			total += ints[i] * Math.pow(255, j);
		}
		return total;
	}
	
	
	public static int softVerToInt2(String version) {
		int num = version.split("[.]").length;
		for (int i = 0; i < 5 - num; i++) {
			version += ".0";
		}
		String[] strings = version.split("[.]");
		int sum = 0;
		try	{
			for (int i = 0, j = strings.length - 1; i < strings.length; i++, j--) {
				sum += Integer.parseInt(strings[i]) * Math.pow(50, j);
			}
		} catch(NumberFormatException e)	{
			e.printStackTrace();
		}
		return sum;
	}

	/*
	 * 将String数组转换为Int数组
	 */
	public static int[] toIntArray(String[] s) {
		int[] ints = new int[s.length];
		for (int i = 0; i < s.length; i++) {
			ints[i] = Integer.parseInt(s[i]);
		}
		return ints;
	}

	public static Tongyong latestVersionsoft(List<Tongyong> list)
			throws InvalidOsVersionException {
		if (list == null || list.isEmpty())
			return null;
		int flag = 0;
		long result = Util.softVerToInt(list.get(0).getTongyong4());
		for (int i = 0; i < list.size(); i++) {
			long intVal = Util.softVerToInt(list.get(i).getTongyong4());
			if (intVal > result) {
				flag = i;
				result = intVal;
			}
		}
		return list.get(flag);
	}
	
	/** 
	 * MD5加密
	 * @param str 要加密的字符串 
	 * @return    加密后的字符串 
	 */  
	public static String toMD5(String str){  
	    try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(str.getBytes());  
	        byte[] byteDigest = md.digest();  
	        int i;  
	        StringBuffer buf = new StringBuffer("");  
	        for (int offset = 0; offset < byteDigest.length; offset++) {  
	            i = byteDigest[offset];  
	            if (i < 0)  
	                i += 256;  
	            if (i < 16)  
	                buf.append("0");  
	            buf.append(Integer.toHexString(i));  
	        }  
	         
//	        return buf.toString().substring(8, 24);		//16位加密     
	        return buf.toString();						//32位加密    
	    } catch (NoSuchAlgorithmException e) {  
	        e.printStackTrace();  
	        return null;  
	    }
	}
	
	public static void closeStream(Closeable... closeable){
		for(Closeable c : closeable)	{
			try {
				if(c != null) c.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 生成一串随机数组成的字符串
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randStr(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
}

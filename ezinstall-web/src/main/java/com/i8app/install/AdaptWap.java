package com.i8app.install;

import com.i8app.ezinstall.common.app.query.Os;

public class AdaptWap {
	public static Os adaptWapUc (String mobtype) {
		Os os = Os.ANDROID;
		String osType = "";
		mobtype = mobtype.toLowerCase();
		if(mobtype.indexOf("uc") >= 0) {  /// uc浏览器
			if (mobtype.indexOf("(") > 0 && mobtype.indexOf(")") > 0){
				mobtype = mobtype.substring(mobtype.indexOf("(")+1, mobtype.indexOf(")"));
			}
			if (mobtype.indexOf("linux") >= 0 || mobtype.indexOf("nux") >= 0 || mobtype.indexOf("android") >= 0) {
				osType = "200";
			} else if (mobtype.indexOf("ios") >= 0 || mobtype.indexOf("apple") >= 0 || mobtype.indexOf("iphone") >= 0 || mobtype.indexOf("ipod") >= 0) {
				osType = "300";
			} else if (mobtype.indexOf("symbian") > 0 || mobtype.indexOf("nokia") > 0 || mobtype.indexOf("symbianos") > 0 ) {
				osType = "100";
			}
		} else if(mobtype.indexOf("opera") >= 0) {  /// opear手机浏览器
			if (mobtype.indexOf("(") > 0 && mobtype.indexOf(")") > 0){
				mobtype = mobtype.substring(mobtype.indexOf("(")+1, mobtype.indexOf(")"));
			}
			if (mobtype.indexOf("linux") >= 0 || mobtype.indexOf("nux") >= 0 || mobtype.indexOf("android") >= 0) {
				osType = "200";
			} else if (mobtype.indexOf("ios") >= 0 || mobtype.indexOf("apple") >= 0 || mobtype.indexOf("iphone") >= 0 || mobtype.indexOf("ipod") >= 0) {
				osType = "300";
			} else if (mobtype.indexOf("symbian") > 0 || mobtype.indexOf("nokia") > 0 || mobtype.indexOf("symbianos") > 0 ) {
				osType = "100";
			}
		} else if(mobtype.indexOf("mozilla") >= 0 || mobtype.indexOf("symbian") >= 0 ) {   /// mozilla 浏览器
			if (mobtype.indexOf("(") > 0 && mobtype.indexOf(")") > 0 && mobtype.indexOf("symbian") < 0){
				mobtype = mobtype.substring(mobtype.indexOf("(")+1, mobtype.indexOf(")"));
			}
			if (mobtype.indexOf("linux") >= 0 || mobtype.indexOf("nux") >= 0 || mobtype.indexOf("android") >= 0) {
				osType = "200";
				
			} else if ((mobtype.indexOf("ios") >= 0 || mobtype.indexOf("apple") >= 0 || mobtype.indexOf("iphone") >= 0 || mobtype.indexOf("ipod") >= 0) && mobtype.indexOf("symbian") < 0) {
				osType = "300";
				
			} else if (mobtype.indexOf("symbian") > 0 || mobtype.indexOf("nokia") > 0 || mobtype.indexOf("iphone") > 0 ) {
				osType = "100";
			}
		}
		if(osType==""){
			os = Os.ANDROID;
		}else if(osType=="200"){
			os = Os.ANDROID;
		}else if(osType=="300"){
			os = Os.IOS;
		}else if(osType=="100"){
			os = Os.SYMBIAN;
		}
		return os;
	}

	
	public static void main(String[] args) {
		AdaptWap aw = new AdaptWap();
//		String ucandroid = "JUC (Linux; U; 2.2.1; zh-cn; XT800; 480*854)";
//		String ucApple = "IUC(U;iOS 3.1.3;Zh-cn;320*480;)/UCWEB7.9.0.94/39/800";
		String ipod = "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_2 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5";
//		String androidliulan = "Mozilla/5.0 (Linux; U; Android 2.1-update1; zh-cn; ZTE-U X850 Build/ERE27) " +
//				"AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17";
//		String iphoneliulan = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_5 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8L1 Safari/6533.18.5";
//		String osliulan = "Nokia5700/4.21 (SymbianOS/9.2; U; Series60/3.1 Nokia5700/4.21; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) Mozilla/5.0 AppleWebKi";
//		String osliulan2 = "NokiaN97mini/SymbianOS/9.1 Series60/3.0";
		//		String opearandroid = "Opera/9.80 (Android 2.3.3; linux; Opera Mobi/ADR-1110071847; U; en) presto/2.9.201 Version/11.50";
		System.out.println(aw.adaptWapUc(ipod));
	}

}

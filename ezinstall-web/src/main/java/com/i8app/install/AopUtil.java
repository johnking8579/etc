package com.i8app.install;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class AopUtil	{
	
	private static Logger logger = Logger.getLogger(AopUtil.class);
	
	/**
	 * 捕捉到异常后进行记录
	 * @param method
	 * @param args
	 * @param target
	 * @param ex
	 */
	public void errorLog(Exception e)	{
		logger.error(e.getMessage(), e);
	}
}

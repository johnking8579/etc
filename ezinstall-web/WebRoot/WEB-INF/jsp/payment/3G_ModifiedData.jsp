<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<title>3G加油站_佣金兑换</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath }/js/jquery-1.7.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath }/js/common.js" type="text/javascript"></script>

<style>
body{ width:1024px; padding:0px; margin:0 auto; font-size:12px; color:#555; font-family: "微软雅黑";}
.bottom{
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: auto;
	background-image: url(../img/bottom.png);
	background-repeat: repeat-x;
}
.header {padding: 30px 48px 32px 48px; background-color: #EEEEEE;}
.marquee {
	font-size: 14px;
	color: #000;
	padding: 0px 120px 0px 120px;
}
.STYLE3 {font-size: 13px; color: #F00;}
.STYLE4 {font-size: 14px; color: #000}
.STYLE8 {
	font-size: 14px;
	color: #000;
	background-color: #eee;
}
.STYLE14 {color: #FF0000; font-size: 25px; font-style: italic; font-weight: bold;}
.STYLE24 {font-size: 16px; font-weight: bold;}
.STYLE25 {font-size: 21px; font-weight: bold;}
.STYLE26 {color: #FF0000}
.STYLE27 {font-size: 15px}
#bj{
	padding-right:80px;
	padding-top: 15px;
}
#dl{
	background-image: url(../img/zhxx.png);
	background-repeat: no-repeat;
	background-position:center;
	padding: 8px 78px 8px 78px;
	margin: 15px 0px 0px 0px;
}
#gz{
	padding: 5px 20px 15px 20px;
}
#zygn—left{
	width:168px;
	position:relative;
	left:0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}
#zygn—center{
	width:168px;
	position:relative;
	left:0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}
#zygn—right{
	width:332px;
	position:relative;
	left:0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}
a:link, a:visited ,a:active {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color: #FF0000;
}
</style>
<script type="text/javascript">
function password(){
	if($.trim($("#passwords").val())==''){
		$("#passwords").focus();
		alert("密码不能为空！");
		return;
	}
	$.post(
			"${pageContext.request.contextPath }/index/password.action",
			{'req.alipayPwd' : hex_md5($.trim($("#passwords").val()))},
			function(data){
				if(data==1){
					window.location.href="${pageContext.request.contextPath }/pay/person.action";
				}else{
					alert(data);
				}
			});
}
</script>
</head>


<body>
	<table style="margin: 10px auto;width:1024px;">
		<tr><td><div class="header" align="left">
			<img src="../img/ltlogo.png" alt="logo" width="156" height="43" align="absbottom" class="miui_logo" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class="STYLE25"> 3G加油站 </span> <span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span>
		</div></td></tr>
		<tr>
			<td>
			<div align="center" class="marquee"><marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee></div>
			</td>
		</tr>
	</table>
	<table style="margin: 10px auto;width:1024px;text-align: center;">
		<tr>
		  <td >
			<a href="${pageContext.request.contextPath }/index/index.action"  class="STYLE24">
			<img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; 首 页
			</a>
		  </td>
		  <td >
			<a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">
			<img src="../img/Commission.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; 兑换奖金
			</a>
	      </td>
		  <td >
			<a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">
			<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">奖金查询
			</a>
	      </td>
		</tr>
	</table>
<table width="1024" border="0" align="center" cellspacing="0">
<%--<div class="header" align="left">
  <img src="../img/ltlogo.png" alt="logo" width="156" height="43" align="absbottom" class="miui_logo" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class="STYLE25"> 3G加油站 </span> <span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span></div>
<table width="1024" border="0" align="center" cellspacing="0"><br/>
      <div align="center" class="marquee"><marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee></div>
	  <td >
	  <div id="zygn—left" class="STYLE3"><img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/index/index.action"  class="STYLE24">首 页</a></div></td>
	  <td >
      <div id="zygn—center" class="STYLE3"><img src="../img/Commission.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">兑换奖金</a></div></td>
	  <td >
      <div id="zygn—right" class="STYLE3"><img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">兑奖查询</a></div></td>
--%><tr>
   <td colspan="3">
   <div id="div" class="STYLE8" align="center">
     <br/><br/><span><strong>请输入兑换密码以修改个人资料，如忘记兑换密码，请拨打客服电话进行重置。
	 <br/><br/>客服电话：010-84450077</strong></span>
	 <br/><br/><hr></hr><br/><br/>
	 <span><strong>填输入兑换密码：</strong></span>&nbsp; 
       <input placeholder=' 请输入兑换密码 ' name='conversion password' type="password" maxlength='16' id='passwords'/>
       <br/>
       <br/>
	   <br/>
       <input class='conversion' type="button" onclick="javascript:password();" value='  确  定  '>
       <br/>
       <br/>
	   <br/>
	   <br/>
   </div>
     </td>
</tr>
</table>
<div class="bottom">版权所有 &copy; 北京桓源创新科技有限公司</div>
</body></html>
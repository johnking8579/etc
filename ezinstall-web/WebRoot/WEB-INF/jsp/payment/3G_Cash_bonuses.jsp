<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="">
		<title>3G加油站_佣金兑换</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link href="${pageContext.request.contextPath }/css/style.css"
			rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath }/js/JQueryUI/css/smoothness/jquery-ui-1.10.4.custom.min.css"></link>
		<!--  -->
		<script type="text/javascript"
			src="${pageContext.request.contextPath }/js/jquery-1.7.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath }/js/jquery-ui-1.8.17.custom.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath }/js/common.js"></script>

		<style>
body {
	width: 1024px;
	padding: 0px;
	margin: 0 auto;
	font-size: 12px;
	color: #555;
	font-family: "微软雅黑";
}

.bottom {
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: auto;
	background-image: url(../img/bottom.png);
	background-repeat: repeat-x;
}

.header {
	padding: 30px 48px 32px 48px;
	background-color: #EEEEEE;
}

.marquee {
	font-size: 14px;
	color: #000;
	padding: 0;
}

.STYLE3 {
	font-size: 13px;
	color: #F00;
}

.STYLE4 {
	font-size: 14px;
	color: #000
}

.STYLE8 {
	font-size: 14px;
	color: #000;
	background-color: #eee;
}

.STYLE14 {
	color: #FF0000;
	font-size: 25px;
	font-style: italic;
	font-weight: bold;
}

.STYLE24 {
	font-size: 16px;
	font-weight: bold;
}

.STYLE25 {
	font-size: 21px;
	font-weight: bold;
}

.STYLE26 {
	color: #FF0000;
	font-size: 15px;
}

.STYLE27 {
	font-size: 15px
}

#bj {
	padding-right: 80px;
	padding-top: 15px;
}

#dl {
	background-image: url(../img/zhxx.png);
	background-repeat: no-repeat;
	background-position: center;
	padding: 8px 110px;
	margin: 15px 0px 0px 0px;
}

#gz {
	padding: 5px 20px 15px 20px;
}

#zygn—left {
	width: 168px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

#zygn—center {
	width: 168px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

#zygn—right {
	width: 240px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

a:link,a:visited,a:active {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
	color: #FF0000;
}

#bonus {
	cursor: help;
	text-decoration: underline;
	color: #FF0000;
}

input[readonly] {
	background-color: #DCDCE2;
}

input[type=text] {
	text-align: right;
}

.ui-dialog-title {
	font-size: 16px;
}
.ModifiedData{width:80px}
</style>
<script type="text/javascript">
$(function(){
	$("#apply_amount").keyup(function(){
		var temp = $.trim($("#apply_amount").val());
		if(temp =='' ){
			temp = 0;
		}
		if(!valid_Money(temp)){
			alert("不能为空或金额格式不对！");
			return ;
		}
		var temp = new Number(temp).toFixed(2);
		$("#money").val(temp);
		$("#originalAmount").val($.trim($("#allMoney").text()));
		$("#tax").val(getTax(temp));
		$("#fee").val(getFee(temp));
		$("#sumAll").val(sumAllMoney());
		$("#sum").val(temp);
		$("#remainAmount").val(new Number($("#originalAmount").val()-$("#sumAll").val()).toFixed(2));
		if($("#remainAmount").val()< 0){
			temp = getAllMoney();
			$("#apply_amount").val(temp);
			$("#money").val(temp);
			$("#tax").val(getTax(temp));
			$("#fee").val(getFee(temp));
			$("#sumAll").val(sumAllMoney());
			$("#sum").val(temp);
			$("#remainAmount").val(new Number($("#originalAmount").val()-$("#sumAll").val()).toFixed(2));
		}

	});
	$("#withdrawalDialog").dialog({
		autoOpen: false,
		height: 500,
		width:600,
		modal: true,
		disabled:true,
		resizable:false,
		stack:true,
        buttons: {
	          "确认": function() {
					withdrawal();
	          },
	          "取消": function() {
		            $( this ).dialog( "close" );
	          }
	        }
	});
	Number.prototype.toFixed = function(scale)
	{  
	     var s = this + "";  
	     if (!scale) scale = 0;  
	     if (s.indexOf(".") == -1) s += ".";  
	     s += new Array(scale + 1).join("0");  
	     if (new RegExp("^(-|\\+)?(\\d+(\\.\\d{0," + (scale + 1) + "})?)\\d*$").test(s))  
	     {  
	         var s = "0" + RegExp.$2, pm = RegExp.$1, a = RegExp.$3.length, b = true;  
	         if (a == scale + 2)  
	         {  
	             a = s.match(/\d/g);  
	             if (parseInt(a[a.length - 1]) > 4)  
	             {  
	                 for (var i = a.length - 2; i >= 0; i--)  
	                 {  
	                     a[i] = parseInt(a[i]) + 1;  
	                     if (a[i] == 10)  
	                     {  
	                         a[i] = 0;  
	                         b = i != 1;  
	                     }  
	                     else  
	                         break;  
	                 }  
	             }  
	             s = a.join("").replace(new RegExp("(\\d+)(\\d{" + scale + "})\\d$"), "$1.$2");  
	         }  
	         if (b) s = s.substr(1);  
	         return (pm + s).replace(/\.$/, "");  
	     }  
	     return this + "";  
	};
});
function getAllMoney(){
	var all = new Number($.trim($("#allMoney").text()));
	if(all <= 201){
		return all - 1;
	}else if(all<= 800){
		return new Number(all/1.005).toFixed(2);
	}else{
		return new Number(800.00);
	}
	
}
function valid_Money(Money) {
	var patten = new RegExp(/^\d+(\.)?(\d*){0,2}$/);
	return patten.test(Money);
}
function withdrawal(){
	var apply_amount = new Number($.trim($("#apply_amount").val()));
	var tax = new Number($.trim($("#tax").val()));
	var fee = new Number($.trim($("#fee").val()));
	var _ = new Date().getMilliseconds();
	var allmoney = new Number($.trim($("#allMoney").text()));
	var __ = hex_md5(apply_amount.toString()+tax.toString()+fee.toString()+allmoney.toString()+_.toString());
	var money = apply_amount + tax + fee;
//	if(money<30){
//		alert("金额不足30元！");
//		return ;
//	}
	if(money >allmoney){
		alert("兑取金额与手续费合计为："+ money+"\n奖金金额不足！");
		return;
	}
	if(money>=800){
		alert("当月支取金额超出800，支付宝将代扣支取金额的20%的个人所得税！\n"+
				"请详细阅读 首页 奖金兑换说明。\n"+
				"暂不支持支取>800的金额");
		return ;
	}
	var i = Number($.trim($("#accountStatus").val()));
	switch(i){
	case 0:
			if(!confirm("每月限制支取一次。\n确认后提交支取请求，取消返回！")){
				return;
			}
		  break;
	case 1:
		$("#msg").text("帐号转账中。请确认奖金到账后再进行支取。");
		$("#msg").show();
		return;
	case 2:
		$("#msg").text("帐号异常，完善支付宝信息或者联系管理员！");
		$("#msg").show();
		return;
	case 3:
		$("#msg").text("帐号审核中，请联系相关审核人员！");
		$("#msg").show();
		return;
	default:
		$("#msg").text("系统错误！");
		$("#msg").show();
	  	return;
	}
	$.post(
			"../index/newWithdrawal.action",
			{
				"money": apply_amount.toString(),
				"month": tax.toString(),
				"year": fee.toString(),
				"phoneNumber": allmoney.toString(),
				"autocode": $.trim($("#autocode").val()),
				"pass": hex_md5($.trim($("#pass").val())),
				"cw.empid": _,
				"cw.approveNote": __
			},
			function(data){
				if(data == 1){
					if(confirm("支取单创建成功，\n\n点击确定进入查询界面。")){
						window.location.href="../pay/query.action";
					}
				}else if(data==2){
					alert("本月已进行过支取。");
					window.location.href="../pay/query.action";
				}else{
					alert(data);
				}
			});
}
function loginout(){
	$.get(
		"../index/loginout.action",
		function(data){
			if(data==1){
				window.location.href="../index/index.action";
			}else{
				alert(data);
			}
		});
}
function getFee(op){
	var fee = op * 0.005;
	if(fee==0){
		fee = 0;
	}else if(fee <= 1){
		fee = 1;
	}else if( fee >= 25){
		fee = 25;
	}
	return new Number(fee).toFixed(2);
}
function getTax(op){
	var tax ;
	if(op==0){
		tax = 0;
	}else if(op <= 800){
		tax = 0;
	}else if( op <= 4000){
		tax = op * 0.2;
	}else{
		tax = op * 0.4;
	}
	return new Number(tax).toFixed(2);
}
function getMoney(){
	var money = new Number($.trim($("#money").val()));
	if(money =='' || !valid_Money(money)){
		alert("金额不能为空或格式不对！");
		return ;
	}
	$("#apply_amount").val(new Number(money).toFixed(2));
	$("#apply_amount").keyup();
	$('#withdrawalDialog').dialog('open');
}
function sumAllMoney(){
	var s = new Number($.trim($("#money").val()));
	s = s + new Number($.trim($("#tax").val()));
	s = s + new Number($.trim($("#fee").val()));
	return new Number(s).toFixed(2);
}
function getSmsCode(obj){
	countDown(obj, 120);
	sms_verify();
}
function goModify(){
	window.location.href="../pay/modify.action";
}
function sms_verify(){
    $.ajax({
		url:"../pay/newVerfysms.action",
		data:{
			"year":$("#phoneNumber").val(),
			"autocode":"0"
			},
		type:"post",
		dataType:"json",
		success:function(data){
			switch(data.flag){
			case 1:
				alert("抱歉，请检查短信网关!");
				break;
			case 2:
				alert("手机号码不能为空");
				break;	
			case 3:
				alert("发送成功");
				break;	
			case 4:
				alert("请输入正确的手机号码");
				break;	
			case 5:
				alert("请等2分钟后再发送验证码");
				break;	
			default:
				alert("抱歉，获取验证失败！");
				break;
			}
		}
	});   	
}
</script>
	</head>
	<body>
		<table style="margin: 10px auto; width: 1024px;">
			<tr>
				<td>
					<div class="header" align="left">
						<img src="${pageContext.request.contextPath }/img/ltlogo.png"
							alt="logo" width="156" height="43" align="absbottom"
							class="miui_logo" />
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<span class="STYLE25"> 3G加油站 </span>
						<span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;
						<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！</span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="center" class="marquee">
						<marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee>
					</div>
				</td>
			</tr>
		</table>
		<table style="margin: 10px auto; width: 1024px; text-align: center;">
			<tr>
				<td>
					<a href="${pageContext.request.contextPath }/index/index.action" class="STYLE24">
					<img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>首 页</span> 
					</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">
					<img src="../img/Commission.png" width="64"	height="64" hspace="8" vspace="8" align="middle">
					<span>兑换奖金</span>
					</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">
					<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>奖金查询</span>
					</a>
				</td>
			</tr>
		</table>
		<table width="1024" border="0" align="center" cellspacing="0">
			<tr>
				<td colspan="2" style="padding-left: 70px;">
					<div id="div" class="STYLE8" align="center">
						<br />
						<br />
						<span><strong>填写兑换金额：</strong>
						</span>&nbsp;
						<input placeholder=' 请输入兑换金额 ' name='bonus[money]' id='money'
							type=text maxlength='6' />
						&nbsp; 元 / 人民币
						<br />
						<br />
						<input class='conversion' type='button' onclick="getMoney();"
							value='  兑  换  '>
						<br />
						<br />
						<br />
					</div>
					<br />
					<span class="STYLE26">注： 每次兑换金额最低30元，最高5000元；兑换可能产生的各项费用请查看"<a
						id="bonus" href="../index/index.action">奖金兑换说明</a>
						"页面；申请兑换后，将在2个工作日内进行转账，我们将通过支付宝平台向申请兑换的营业员发放奖金，请确保您输入的正确性。</span>
				</td>
				<td width="45%" height="auto" valign="top">
					<div id="gz">
						<div id="dl" class="STYLE4">
							<div align="left">
								<br />
								<br />
								<span><strong><span class="STYLE27">3G加油站-营业员帐号信息</span>
								</strong>
								</span>
								<br />
								<br />
								<span>&emsp;&emsp;<strong>省&emsp;&emsp;分：</strong>
								</span> ${emp.province.provinceName }
								<br />
								<span>&emsp;&emsp;<strong>工&emsp;&emsp;号：</strong>
								</span> ${emp.empno }
								<br />
								<span>&emsp;&emsp;<strong>帐号余额：</strong>
								</span><span id="allMoney">${money }</span>元
								<br />
								<span>&emsp;&emsp;<strong>冻结金额：</strong>
								</span><span>${month }</span>元
								<br />
								<span>&emsp;&emsp;<strong>帐号状态：</strong>
								</span>
								<span>
									<input type="hidden" value="${pro}"	id="accountStatus" />
									<c:if test="${pro==0 }">正常</c:if>
									<c:if test="${pro==1 }">正在支付中</c:if>
									<c:if test="${pro==2 }">帐号异常，请联系管理员</c:if>
									<c:if test="${pro==3 }">帐号未审核</c:if>
								</span>
								<br />
								<br />
								<div align="left">
									<input class='ModifiedData'	type='button' value='修改资料' onclick="goModify()">
									&nbsp; &nbsp; &nbsp; &nbsp;
									<input class='ModifiedData' type='button' onclick='loginout()' value='退  出'>
								</div>
								<br />
								<br />
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div id="withdrawalDialog" title="兑换明细">
			<fieldset style="background-color: #E9E9E9;">
				<table style="font-size: 16px; margin: auto;">
					<tr>
						<td style="text-align: right;">
							<label>帐号原金额：</label>
						</td>
						<td>
							<input type="text" id="originalAmount" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>支取金额：</label>
						</td>
						<td>
							<input type="text" id="apply_amount" maxlength="6" />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>代扣个人所得税：</label>
						</td>
						<td>
							<input type="text" id="tax" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>转账手续费：</label>
						</td>
						<td>
							<input type="text" id="fee" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>实际扣除金额：</label>
						</td>
						<td>
							<input type="text" id="sumAll" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>实际到账金额：</label>
						</td>
						<td>
							<input type="text" id="sum" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>支付成功后余额：</label>
						</td>
						<td>
							<input type="text" id="remainAmount" readonly />
							<span>元</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>请输入验证码：</label>
						</td>
						<td>
							<input placeholder='请输入验证码  ' type="text" id="autocode" />
						</td>
					</tr>
					<tr>
						<td style="text-align: right;"></td>
						<td align="center">
							<input type="button" value="发送验证码" onclick="getSmsCode(this)"
								style="width: 150px; margin-left: 10px;" />
							<span>&emsp;</span>
						</td>
					</tr>
					<tr>
						<td style="text-align: right;">
							<label>
								兑换密码：
							</label>
						</td>
						<td>
							<input placeholder='请输入兑换密码  ' type="password" id="pass" />
							<span></span>
						</td>
					</tr>
				</table>
			</fieldset>
			<span id='msg' style="color: red;font-size: 24px;display: block;width: 100%;text-align: center;""></span>
		</div>

		<div class="bottom">
			版权所有 &copy; 北京桓源创新科技有限公司
		</div>

	</body>
</html>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<title>3G加油站_佣金兑换</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath }/images/favicon.ico" />
<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath }/css/pay_person_data.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath }/js/JQueryUI/css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath }/js/jquery-1.7.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath }/js/JQueryUI/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath }/js/jquery.blockUI.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath }/js/common.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath }/js/pay_person_data.js" type="text/javascript"></script>

<script type="text/javascript">
$("document").ready(function(){
	$( "#pwd-modify" ).dialog({
		autoOpen: false,
		resizable: false,
		draggable: false,
		height:300,
		width:340,
	    modal: true,
	    open:function(){
			$("#errorPwd").hide();
	    },
	    buttons: {
	        "确认": function() {
	        	saveNewPass();
	        },
	        "取消": function() {
	          $( this ).dialog( "close" );
	          clearInput();
	      }
	   }
	});
	$( "#phoneNo-confirm" ).dialog({
		autoOpen: false,
		resizable: false,
		draggable: false,
		height:240,
		width:340,
	    modal: true,
	    buttons: {
	        "确认": function() {
	          $( this ).dialog( "close" );
	          $( "#phoneNo-modify" ).dialog("open");
	        },
	        "取消": function() {
	          $( this ).dialog( "close" );
	          clearInput();
	      }
	   }
	});

	$( "#phoneNo-modify" ).dialog({
		autoOpen: false,
		resizable: false,
		draggable: false,
		height:400,
		width:340,
	    modal: true,
	    open: function( event, ui ) {
	    	$("#errorphoneNo").hide();
	    	if($(":radio:checked").val()=="isAppeal"){
	    		$("#SmsCodeFieldset legend").html("<span>新手机号码接收验证码</sapn>");
	    		$("#SmsCodeNotice").show();
	    	}else{
	    		$("#SmsCodeFieldset legend").html("<span>原注册手机号码接收验证码</sapn>");
	    		$("#SmsCodeNotice").hide();
	    	}
	    },
	    buttons: {
	        "确认": function() {
	       		saveNewPhoneNo();
	        },
	        "取消": function() {
	          $( this ).dialog( "close" );
	          clearInput();
	      }
	   }
	});
	$( "#alipay-modify" ).dialog({
		autoOpen: false,
		resizable: false,
		draggable: false,
		height:490,
		width:430,
	    modal: true,
	    buttons: {
	        "确认": function() {
	       		saveNewAlipay();
	        },
	        "取消": function() {
	          $( this ).dialog( "close" );
	          clearInput();
	      }
	   }
	});

	$("#submitform").click(function(){
		saveNewPerson();
	});
	checkSame("number", "number2", "errorphoneNo");
	checkSame("newPassword", "newPassword2", "errorPwd");
	checkSame("newName", "newName2", "errorNew");
	checkSame("newAlipay", "newAlipay2", "errorNew");
});
</script>
</head>


<body>
	<table style="margin: 10px auto;width:1024px;">
		<tr><td><div class="header" align="left">
			<img src="../img/ltlogo.png" alt="logo" width="156" height="43" align="absbottom" class="miui_logo" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class="STYLE25"> 3G加油站 </span> <span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span>
		</div></td></tr>
		<tr>
			<td>
			<div align="center" class="marquee"><marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee></div>
			</td>
		</tr>
	</table>
	<table style="margin: 10px auto;width:1024px;text-align: center;">
		<tr>
			<td>
				<a href="${pageContext.request.contextPath }/index/index.action" class="STYLE24">
				<img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle">
				<span>首 页</span> 
				</a>
			</td>
			<td>
				<a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">
				<img src="../img/Commission.png" width="64"	height="64" hspace="8" vspace="8" align="middle">
				<span>兑换奖金</span>
				</a>
			</td>
			<td>
				<a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">
				<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">
				<span>奖金查询</span>
				</a>
			</td>
		</tr>
	</table>
<table width="1024" border="0" align="center" cellspacing="0">
<tr>
   <td colspan="3">
   <div id="div" class="STYLE8" align="center">
	<table border="1"  style="margin:0 auto;width:1024px;background-color: #EEEEEE;line-height: 30px;border-style: none;">
		<tr>
			<td colspan="4" style="text-align: center;">
				<span class="STYLE24">修改个人资料</span>
			</td>
		</tr>
		<c:if test="${req.approveStatus==0}">
		<tr>
			<td colspan="4" style="text-align: center;">
				<span class="STYLE24">该用户信息在审核中</span>
			</td>
		</tr>
		</c:if>
		<tr>
			<td align="right" width="15%"><span><strong>省分：</strong></span></td>
			<td><input id='1' type=text value='${emp.province.provinceName }' readonly/></td>
			<td align="right" width="15%"><span><strong>手机号码：</strong></span></td>
			<td>
		       <input class="inputright" placeholder=' 手机号码 ' id='6' type="hidden" value='${req.phoneNo }' maxlength='11' readonly/>
		       <%--<input class='conversion' type='button' onClick='amend_phone(event)' value='  修改手机号码  '>--%>
		       <input class='conversion' type='button' onClick='phoneModify()' value='  修改手机号码  '>
		    </td>
		</tr>
		<tr>
			<td align="right"><span><strong>地市：</strong></span></td>
			<td><input id='2' type=text value='${emp.city.cityName }' readonly/></td>
			<td align="right"><span><strong>佣金支取密码：</strong></span></td>
			<td><input class='conversion' type='button' onClick='pwdModify()' value='  修改兑换密码  '></td>
		</tr>
		<tr>
			<td align="right"><span><strong>区县：</strong></span></td>
			<td><input id='3' type=text value='${emp.county.countyName }' readonly/></td>
			<td align="right"><span><strong>帐号信息修改：</strong></span></td>
			<td><input class='conversion'  type="button" value="  修改帐号信息  " onClick="alipayModify()"/></td>
		</tr>
		<tr>
			<td align="right"><span><strong>帐号/工号：</strong></span></td>
			<td >
		       <input placeholder=' 帐号/工号 ' id='5' type=text value='${emp.empno }' readonly/>
			<td></td>
			<td></td>
			</td>
		</tr>
		<tr>
		<td colspan="4" align="center">
		<a href="${pageContext.request.contextPath }/pay/bonus.action"><input class='back' type='button' value=' 返回 '></a>
		</td>
		</tr>
		</table>
    </td>
</tr>
</table>
<div class="bottom">版权所有 &copy; 北京桓源创新科技有限公司</div>

<div id="pwd-modify" title="修改密码">
	<fieldset>
		<legend><span>原密码</sapn></legend>
		<input id='oldpassword' type="password" />
		</fieldset>
		<fieldset>
		<legend><span>新密码</sapn></legend>
		<input id='newPassword' type="password"><br>
		<label for='newPassword2'>请再输入一次密码</label><br>
		<input id='newPassword2' type="password"><span id='errorPwd'style="color: red;">两次输入不一致</span>
	</fieldset>
</div>

<div id="phoneNo-confirm" title="修改手机号码：第一步">
	<fieldset>
		<legend><span>原注册手机号码 可以使用</sapn></legend>
		<input id='isAppeal0' type="radio" value='0' name="shouji" checked>
		<label for='isAppeal0'>通过手机验证码修改</label>
		</fieldset>
		<fieldset>
		<legend><span>原注册手机号码 不能使用</sapn></legend>
		<input id='isAppeal' type="radio" value='isAppeal' name="shouji">
		<label for='isAppeal'>向管理员提交修改申请</label>
	</fieldset>
</div>

<div id="phoneNo-modify" title="修改手机号码：第二步">
<span id='errorphoneNo'style="color: red;">两次输入不一致</span>
	<fieldset>
		<legend><span>新手机号码</sapn></legend>
		<input id='number' placeholder=' 请输入手机号码 ' type='text' maxlength='11'/><br>
		<label>再次输入新号码：</label><br>
		<input id='number2' placeholder=' 请输入手机号码' type='text' maxlength='11'/>
	</fieldset>
	<fieldset id="SmsCodeFieldset">
		<legend></legend>
		<input placeholder=' 请输入验证码 ' id='autoCode' maxlength='6'/><br>
		<input type="button" onclick="getSmsCode(this);" value='获取短信验证码'>
	</fieldset>
	<fieldset id="SmsCodeNotice">
		<legend><span>注意：</sapn></legend>
		<span>本次修改将提交管理员审核，审核通过后方可生效</span>
	</fieldset>
</div>
<div id="alipay-modify" title="修改帐号信息">
	<span id='erroralipay' style="color: red; display:none;"></span>
	<fieldset>
		<legend><span>原注册信息</sapn></legend>
		<label>原帐号用户名：</label>
		<input id='oldName' placeholder=' 请输入原帐号用户名 ' type='text'/><br>
		<label>原支付宝帐号：</label>
		<input id='oldAlipay' placeholder=' 请输入原支付宝帐号 ' type='text'/><br>
		<label>原注册手机号：</label>
		<input id='oldPhoneNo' placeholder=' 请输入手机号码 ' type='text'/><br>
		<label>输入佣金密码：</label>
		<input id='oldPass' placeholder=' 请输入支取密码 ' type='password'/><br>
		
	</fieldset>
	<fieldset>
		<legend><span>新注册信息</sapn></legend>
		<label>帐号用户名：</label><input id='newName' placeholder=' 请输入帐号用户名 ' type='text'/><br>
		<label>帐号用户名：</label><input id='newName2' placeholder=' 请再次输入帐号用户名 ' type='text'/><br>
		<label>支付宝帐号：</label><input id='newAlipay' placeholder=' 请输入支付宝帐号 ' type='text'/><br>
		<label>支付宝帐号：</label><input id='newAlipay2' placeholder=' 请再次输入支付宝帐号 ' type='text'/>
		<span id='errorNew' style="color: red; display:none;">两次输入不一致！</span>
	</fieldset>
</div>
</body></html>
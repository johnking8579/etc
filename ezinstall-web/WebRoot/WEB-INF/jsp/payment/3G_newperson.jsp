<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="">
<title>3G加油站_佣金兑换</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css"/>
<link rel="stylesheet" type="text/css" 
href="${pageContext.request.contextPath }/js/JQueryUI/css/smoothness/jquery-ui-1.10.4.custom.min.css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.7.min.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/amend.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/amend_phone.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/common.js"></script>

<style>
body{ width:1024px; padding:0px; margin:0 auto; font-size:12px; color:#555; font-family: "微软雅黑";}
.bottom{
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: auto;
	background-image: url(../img/bottom.png);
	background-repeat: repeat-x;
}
.header {padding: 30px 48px 32px 48px; background-color: #EEEEEE;}
.marquee {
	font-size: 14px;
	color: #000;
	padding: 0px 120px 0px 120px;
	margin:20px;
}
.data {
    position:relative;
	padding: 0px 10px 10px 10px;
	text-align: left;
	width: 350px;
	left:-188px;
	top: 30px;
}
.data-1 {
    position:relative;
	padding: 0px 10px 10px 10px;
	text-align: left;
	width: 450px;
	left:188px;
	top: -144px;
}

.STYLE3 {font-size: 13px; color: #F00;}
.STYLE8 {
	font-size: 14px;
	color: #000;
	background-color: #eee;
}
.STYLE14 {color: #FF0000; font-size: 25px; font-style: italic; font-weight: bold;}
.STYLE24 {font-size: 16px; font-weight: bold;}
.STYLE25 {font-size: 21px; font-weight: bold;}
.STYLE26 {color: #FF0000}
#bj{
	padding-right:80px;
	padding-top: 15px;
}
#dl{
	background-image: url(../img/zhxx.png);
	background-repeat: no-repeat;
	background-position:center;
	padding: 8px 78px 8px 78px;
	margin: 15px 0px 0px 0px;
}
#gz{
	padding: 5px 20px 15px 20px;
}
.mesWindow{border:#666 1px solid;background:#fff; width:400px; position:absolute; top:300px; text-align:center;}
.mesWindowTop{border-bottom:#999 2px solid;padding:3px 3px 3px 12px;font-weight:bold;text-align:left;font-size:16px;height:32px; background-color:#ddd;}
.mesWindowContent{margin:4px;font-size:13px;padding:20px 35px 20px 35px;text-align:left}
.mesWindow .close{ text-align:center;font-size:15px; font-weight:bold;color:#666;height:20px;width:21px;background:#eee; border:0px; margin-right:3px; padding:2px 1px 1px 1px}
.verify {
    position:absolute;
    left:138px;
    top:240px;
    text-align: center;
	border:0px;
	background-image:url(../img/Ltd-button-bh.png);
	width:73px;
	height:35px;
	background-color:#FFFFFF;
	background-position:center;
	font-size: 14px;   
}
.verify:hover {border:0px;background-image:url(../img/Ltd-button-bule.png);width:73px;height:35px;background-position:center;color:#fff;font-size: 14px;}
.submit {
    position:absolute;
    left:138px;
    top:200px;
    text-align: center;
	border:0px;
	background-image:url(img/Ltd-button-bh.png);
	width:73px;
	height:35px;
	background-color:#FFFFFF;
	background-position:center;
	font-size: 14px;   
}
.submit:hover {border:0px;background-image:url(../img/Ltd-button-bule.png);width:73px;height:35px;background-position:center;color:#fff;font-size: 14px;}
a:link, a:visited , a:active{
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
	color: #FF0000;
}
.inputright{
	width: 160px;
}
input{
	line-height: 25px;
	padding-left:5px;
}
input[readonly]{
	background-color: #DCDCE2;
}
.shengming{
text-align:left; display:block; width: 520px; height: 350px; border-style : solid; border-color: black;
padding: 10px;
}
</style>
<script type="text/javascript">
$("document").ready(function(){
	checkSame("10", "14","errorSame");
	checkSame("11", "13","errorSame");
	$("#button").attr("disabled", "disabled");
	$("#xieyi").click(function() {
		if ($("#xieyi").prop("checked") == true) {
			$("#button").removeAttr("disabled");
		} else {
			$("#button").attr("disabled", "disabled");
		}
	});
	$( "#declaration" ).dialog({
		autoOpen: true,
		height: 530,
		width:600,
		modal: true,
		disabled:true,
		resizable:false,
		stack:true,
		close: function( event, ui ) {
			$.get(
					"../index/loginout.action",
					function(data){
						if(data==1){
							window.location.href="../index/index.action";
						}else{
							alert(data);
						}
					});
		}
	});
	$("#6").blur(function(){
		//手机号码
		if($.trim($("#6").val()).length!=11){
			$("#6").css("background-color","#F00");
			$("#errorSame span").text("手机号码长度不对！");
			$("#errorSame").show();
		}else{
			$("#6").css("background-color","#FFF");
			$("#errorSame").hide();
			$("#errorSame span").text("两次输入不一致！");
		}
	});
	$("#11").blur(function(){
		if($("#11").val().length<6){
			$("#11").css("background-color","#F00");
			$("#errorSame span").text("密码长度少于6个字符！");
			$("#errorSame").show();
		}else{
			$("#11").css("background-color","#FFF");
			$("#errorSame").hide();
			$("#errorSame span").text("两次输入不一致！");
		}
	});
	$("#submitform").click(function(){
		if(!confirm("本次提交为重要信息！\n\n请确认内容准确性并牢记密码。\n\n确认进行提交？")){
			return;
		}
		$.post(
				"${pageContext.request.contextPath }/index/saveNewPerson.action",
				{
					"autocode" 			: $.trim($("#4").val()),
					'req.phoneNo'    	: $.trim($("#6").val()),
					'req.alipayUser' 	: $.trim($("#9").val()),
					'req.alipayAccount' : $.trim($("#10").val()),
					"month" 			: $.trim($("#14").val()),
					'req.alipayPwd'  	: hex_md5($.trim($("#11").val())),
					"pass" 				: hex_md5($.trim($("#13").val())),
					'req.isAppeal'  	: 1
				},
				function(data){
					if(data==1){
						window.location.href="../pay/bonus.action";
					}else{
						alert(data);
					}
				});
	});
});
function perfect() {
	if ($("#xieyi").prop("checked") == true) {
		$( "#declaration" ).dialog( "destroy" );
	}
}
function loginout(){
	if(confirm("您还没有完善信息将不能领取奖金\n\n确认退出么？")){
		$.get(
			"../index/loginout.action",
			function(data){
				if(data==1){
					window.location.href="../index/index.action";
				}else{
					alert(data);
				}
			});
	}
}
function getSmsCode(obj){
	if($("#6").val()==""){
		$("#6").css("background-color","#F00");
		$("#errorSame span").text("请输入手机号！");
		$("#errorSame").show();
		return;
	}else{
		$("#6").css("background-color","#FFF");
		$("#errorSame").hide();
		$("#errorSame span").text("两次输入不一致！");
	}
	countDown(obj, 120);	
	sms_verify();
}
function sms_verify(){
	if($.trim($('#6').val())==null || $.trim($('#6').val())==""){
		alert("请输入手机号码！");
		return;
	}
	var h = /^1[3,5,4,8]{1}\d{9}$/;
	if(!h.test($.trim($('#6').val()))){
		alert("请输入正确的手机号码！");
		return;	 			
	} 
    $.ajax({
		url:"../pay/verifysms.action",
		data:{
			"phoneNumber":$.trim($('#6').val()),
			"month"	:	new String(event.timeStamp).substring(0,8)
			},
		type:"post",
		dataType:"json",
		success:function(data){
			switch(data.flag){
			case 1:
				alert("抱歉，请检查短信网关!");
				break;
			case 2:
				alert("手机号码不能为空");
				break;	
			case 3:
				alert("发送成功");
				break;	
			case 4:
				alert("请输入正确的手机号码");
				break;	
			case 5:
				alert("请等2分钟后再发送验证码");
				break;	
			default:
				alert("抱歉，获取验证失败！");
				break;
			}
		}
	});   	
}
</script>
</head>
<body>
	<table style="margin: 10px auto;width:1024px;">
		<tr><td><div class="header" align="left">
			<img src="../img/ltlogo.png" alt="logo" width="156" height="43" align="absbottom" class="miui_logo" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span class="STYLE25"> 3G加油站 </span> <span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span>
		</div></td></tr>
		<tr>
			<td>
			<div align="center" class="marquee"><marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee></div>
			</td>
		</tr>
	</table>
	<table style="margin: 10px auto;width:1024px;text-align: center;">
		<tr>
		  <td >
			<a href="javascript:loginout();"  class="STYLE24">
			<img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle">退 出
			</a>
		  </td>
		  <td >
			<a href="javascript:alert('请完善个人信息并提交！');" class="STYLE24">
			<img src="../img/Commission.png" width="64" height="64" hspace="8" vspace="8" align="middle">兑换奖金
			</a>
	      </td>
		  <td >
			<a href="javascript:alert('请完善个人信息并提交！');" class="STYLE24">
			<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">奖金查询
			</a>
	      </td>
		</tr>
	</table>
	<table border="1"  style="margin:0 auto;width:1024px;background-color: #EEEEEE;line-height: 30px;border-style: none;">
		<tr>
			<td colspan="4" style="text-align: center;">
				<span class="STYLE24">完善个人资料</span>
			</td>
		</tr>
		<tr>
			<td align="right" width="15%"><span><strong>省分：</strong></span></td>
			<td><input id='1' type=text value='${emp.province.provinceName }' maxlength='' readonly/></td>
			<td align="right"><span><strong>手机号码：</strong></span></td>
			<td width="45%">
				<input class="inputright" placeholder='请输入手机号码' id='6' type=text value='' maxlength='11'/>
				<%--
				<input class='conversion' type='button' id='phonebutton' onClick='amend_phone2(event)' value='绑定'>&nbsp;&nbsp;<span class="STYLE26">* 必填</span>
				--%>
			</td>
		</tr>
		<tr>
			<td align="right"><span><strong>地市：</strong></span></td>
			<td><input id='2' type=text value='${emp.city.cityName }' readonly/></td>
			<td align="right"><span><strong>验证码：</strong></span></td>
			<td><input class="inputright" placeholder=' 请输入验证码 ' id='4' type=text />&nbsp;&nbsp;&nbsp;<input type="button" value="获取验证码" onclick='getSmsCode(this);' ></td>
			<%--
			<td align="right"><span><strong>营业员姓名：</strong></span></td>
			<td><input class="inputright"  id='7' type=text value='${emp.empName }' readonly/></td>
			--%>
		</tr>
		<tr>
			<td align="right"><span><strong>区县：</strong></span></td>
			<td><input id='3' type=text value='${emp.county.countyName }' maxlength='' readonly/></td>
			<td align="right"><span><strong>支付宝姓名：</strong></span></td>
			<td><input class="inputright" placeholder=' 请输入支付宝姓名 ' id='9' type=text value='' maxlength='16'/>&nbsp;&nbsp;<span class="STYLE26">* 必填（支付宝真实姓名且与支付宝帐号匹配）</span></td>
		</tr>
		<tr>
			<td align="right"><span><strong>帐号/工号：</strong></span></td>
			<td><input type=text value='${emp.empno }' maxlength='' readonly/></td>
			<td align="right"><span><strong>支付宝帐号：</strong></span></td>
			<td><input class="inputright" placeholder=' 请输入支付宝帐号 ' id='10' type=text value='' maxlength=''/>&nbsp;&nbsp;<span class="STYLE26">* 必填（需通过实名认证，否则不能接受转账）</span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="right"><span><strong>确认支付宝帐号：</strong></span></td>
			<td><input class="inputright" placeholder=' 请再次输入支付宝帐号 ' id='14' type=text value='' />&nbsp;&nbsp;<span class="STYLE26">* 必填 <a style="color:red;" type="button" target="_blank" href="https://www.alipay.com/">( 点这里确认支付宝准确可用 )</a> </span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="right"><span><strong>本站佣金支取密码：</strong></span></td>
			<td><input class="inputright" placeholder=' 请输入佣金支取密码 ' id='11' type="password" value='' maxlength=''/>&nbsp;&nbsp;<span class="STYLE26">* 必填</span></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="right"><span><strong>确认佣金支取密码：</strong></span></td>
			<td><input class="inputright" placeholder=' 请再次输入佣金支取密码 ' id='13' type="password" value='' maxlength=''/>&nbsp;&nbsp;<span class="STYLE26">* 必填</span></td>
		</tr>
		<tr>
		<td colspan="4" align="center">
			<input class='submit-1' type="button" value=' 重填 ' id="resetform">
			<input class='submit-1' type='button' value=' 提交 ' id='submitform'>
		</td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: center;" id="errorSame">
				<span class="STYLE24" style="color:red;">两次输入不一致！</span>
			</td>
		</tr>
		</table>
<div class="bottom">版权所有 &copy; 北京桓源创新科技有限公司</div>
		<div id="declaration" style="display: none" title="声明" >
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="center">
					<div class="shengming" >
						<p>百度提醒您：</p><p>&emsp;&emsp;在使用百度搜索引擎（以下简称百度）前，请您务必仔细阅读并透彻理解本声明。您可以选择不使用百度，但如果您使用百度，您的使用行为将被视为对本声明全部内容的认可。鉴于百度以非人工检索方式、根据您键入的关键字自动生成到第三方网页的链接，除百度注明之服务条款外，其他一切因使用百度而可能遭致的意外、疏忽、侵权及其造成的损失（包括因下载被搜索链接到的第三方网站内容而感染电脑病毒），百度对其概不负责，亦不承担任何法律责任。任何通过使用百度而搜索链接到的第三方网页均系他人制作或提供，您可能从该第三方网页上获得资讯及享用服务，百度对其合法性概不负责，亦不承担任何法律责任。百度搜索结果根据您键入的关键字自动搜索获得并生成，不代表百度赞成被搜索链接到的第三方网页上的内容或立场。您应该对使用搜索引擎的结果自行承担风险。百度不做任何形式的保证：不保证搜索结果满足您的要求，不保证搜索服务不中断，不保证搜索结果的安全性、正确性、及时性、合法性。因网络状况、通讯线路、第三方网站等任何原因而导致您不能正常使用百度，百度不承担任何法律责任。百度尊重并保护所有使用百度用户的个人隐私权，您注册的用户名、电子邮件地址等个人资料，非经您亲自许可或根据相关法律、法规的强制性规定，百度不会主动地泄露给第三方。百度提醒您：您在使用搜索引擎时输入的关键字将不被认为是您的个人隐私资料。任何网站如果不想被百度收录（即不被搜索到），应该及时向百度反映，或者在其网站页面中根据拒绝蜘蛛协议（Robots Exclusion Protocol）加注拒绝收录的标记，否则，百度将依照惯例视其为可收录网站。任何单位或个人认为通过百度搜索链接到的第三方网页内容可能涉嫌侵犯其信息网络传播权，应该及时向百度提出书面权利通知，并提供身份证明、权属证明及详细侵权情况证明。百度在收到上述法律文件后，将会依法尽快断开相关链接内容。详情参见特定频道的著作权保护声明。</p>
					</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div style="margin: 10px;">
							<label>
								<input name="xieyi" id="xieyi" type="checkbox" value="" />
								我已阅读并接受《奖金兑换协议》
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div style="margin: 10px;">
							<input type='button' id="button" onclick="perfect()" value='同意'>
						</div>
					</td>
				</tr>
			</table>
		</div>
</body></html>
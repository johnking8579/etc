<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="">
		<title>3G加油站_佣金兑换</title>
		<link rel="shortcut icon" href="images/favicon.ico" />
		<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
		<script src="${pageContext.request.contextPath }/js/jquery-1.7.min.js" type="text/javascript"></script>
		<script src="${pageContext.request.contextPath }/js/page.js" type="text/javascript"></script>
		<style>
body {
	width: 1024px;
	padding: 0px;
	margin: 0 auto;
	font-size: 12px;
	color: #555;
	font-family: "微软雅黑";
}

.bottom {
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: auto;
	background-image: url(../img/bottom.png);
	background-repeat: repeat-x;
}

.header {
	padding: 30px 48px 32px 48px;
	background-color: #EEEEEE;
}

table.hovertable {
	font-size: 14px;
	color: #333333;
	border-width: 1px;
	border-color: #999999;
	border-collapse: collapse;
}

table.hovertable th {
	border-width: 1px;
	padding: 5px;
	border-style: solid;
	border-color: #603939;
	background-color: #333333;
}

table.hovertable tr {
	background-color: #FFFFFF;
}

table.hovertable td {
	border-width: 1px;
	padding: 3px;
	border-style: solid;
	border-color: #a9c6c9;
}

.marquee {
	font-size: 14px;
	color: #000;
	padding: 0px 120px 0px 120px;
}

.STYLE3 {
	font-size: 13px;
	color: #F00;
}

.STYLE4 {
	font-size: 14px;
	color: #000
}

.STYLE8 {
	font-size: 14px;
	color: #000;
	background-color: #eee;
	border: 1;
}

.STYLE14 {
	color: #FF0000;
	font-size: 25px;
	font-style: italic;
	font-weight: bold;
}

.STYLE24 {
	font-size: 16px;
	font-weight: bold;
}

.STYLE25 {
	font-size: 21px;
	font-weight: bold;
}

.STYLE26 {
	color: #FFFFFF
}

.STYLE27 {
	font-size: 15px
}

#bj {
	padding-right: 80px;
	padding-top: 15px;
}

#dl {
	background-image: url(../img/zhxx.png);
	background-repeat: no-repeat;
	background-position: center;
	padding: 8px 80px 8px 80px;
	margin: 15px 0px 0px 0px;
}

#gz {
	padding: 5px 20px 15px 20px;
}

#zygn—left {
	width: 168px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

#zygn—center {
	width: 168px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

#zygn—right {
	width: 332px;
	position: relative;
	left: 0px;
	top: 0px;
	height: auto;
	padding: 5px 0px 0px 88px;
}

.cancel {
	position: absolute;
	left: 200px;
	top: 200px;
	text-align: center;
	border: 0px;
	background-image: url(../img/Ltd-button-bh.png);
	width: 73px;
	height: 35px;
	background-color: #FFFFFF;
	background-position: center;
	font-size: 14px;
}

.cancel:hover {
	border: 0px;
	background-image: url(../img/Ltd-button-bule.png);
	width: 73px;
	height: 35px;
	background-position: center;
	color: #fff;
	font-family: "微软雅黑";
	font-size: 14px;
}

.login {
	position: absolute;
	left: 76px;
	top: 200px;
	text-align: center;
	border: 0px;
	background-image: url(../img/Ltd-button-hs.png);
	width: 73px;
	height: 35px;
	background-color: #FFFFFF;
	background-position: center;
	font-size: 14px;
}

.login:hover {
	border: 0px;
	background-image: url(../img/Ltd-button-back.png);
	width: 73px;
	height: 35px;
	background-position: center;
	color: #fff;
	font-size: 14px;
}

a:hover {
	text-decoration: underline;
	color: #FF0000;
}

a:link, a:visited , a:active {
	text-decoration: none;
}
#page td{
	padding: 0 5px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#select").focus;
	});
</script>
	</head>


	<body>
		<table style="margin: 10px auto; width: 1024px;">
			<tr>
				<td>
					<div class="header" align="left">
						<img src="../img/ltlogo.png" alt="logo" width="156" height="43"
							align="absbottom" class="miui_logo" />
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<span class="STYLE25"> 3G加油站 </span>
						<span class="STYLE14"> 装软件 赚奖金 </span>&nbsp;
						<span class="STYLE25"> 亲，马上有财宝，赶紧来抢咯！ </span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="center" class="marquee">
						<marquee>
							您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。
						</marquee>
					</div>
				</td>
			</tr>
		</table>
		<table style="margin: 10px auto; width: 1024px; text-align: center;">
			<tr>
				<td>
					<a href="${pageContext.request.contextPath }/index/index.action" class="STYLE24">
					<img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>首 页</span> 
					</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">
					<img src="../img/Commission.png" width="64"	height="64" hspace="8" vspace="8" align="middle">
					<span>兑换奖金</span>
					</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">
					<img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle">
					<span>奖金查询</span>
					</a>
				</td>
			</tr>
		</table>
		<table width="1024" border="0" align="center" cellspacing="0">
			<%--<table width="1024" border="0" align="center" cellspacing="0"><br/>
      <div align="center" class="marquee"><marquee>您好!欢迎您参与3G加油站"装软件、赚奖金"的活动。</marquee></div>
	  <td >
	  <div id="zygn—left" class="STYLE3"><img src="../img/home.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/index/index.action"  class="STYLE24">首 页</a></div></td>
	  <td >
      <div id="zygn—center" class="STYLE3"><img src="../img/Commission.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/pay/bonus.action" class="STYLE24">兑换奖金</a></div></td>
	  <td >
      <div id="zygn—right" class="STYLE3"><img src="../img/numbers64.png" width="64" height="64" hspace="8" vspace="8" align="middle"> &nbsp; <a href="${pageContext.request.contextPath }/pay/query.action" class="STYLE24">兑奖查询</a></div></td>
--%>
			<tr>
				<td colspan="3">
					<div id="div" class="STYLE8" align="center">
						<br />
						<span><strong>省分 ：</strong>
						</span> ${emp.province.provinceName } &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp;
						<span><strong>帐号/工号 ：</strong>
						</span> ${emp.empno }&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp;
						<span><strong>营业员姓名 ：</strong>
						</span> ${emp.empName } &nbsp;
						<br />
						<hr></hr>
						<br />
						<label>
							<strong>选择年份：</strong>&nbsp;
						</label>
						<select name="select" class="STYLE21">
							<option selected="selected">
								--请选择--
							</option>
							<option>
								&nbsp;&nbsp; 全部&nbsp;
							</option>
							<option>
								&nbsp;2014年
							</option>
							<option>
								&nbsp;2015年
							</option>
							<option>
								&nbsp;2016年
							</option>
							<option>
								&nbsp;2017年
							</option>
							<option>
								&nbsp;2018年
							</option>
						</select>
						<!-- 
         -->
						<%--<input type="text" readonly="readonly" class="STYLE21" id="year" name="year" placeholder="请选择年份" onFocus="if(value==defaultValue){value='';this.style.color='#000'};WdatePicker({isShowToday:false,readOnly:true,dateFmt:'yyyy'})" onblur="if(!value){value=defaultValue;this.style.color='#999';}" style="color:#999999"/>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		 --%>
						<label>
							<strong>选择月份：</strong>&nbsp;
						</label>
						<select name="month" id="month" class="STYLE21">
							<option value="" selected="selected">
								--请选择--
							</option>
							<option value="">
								&nbsp;&nbsp; 全部&nbsp;
							</option>
							<option value="01">
								&nbsp;&nbsp; 1月
							</option>
							<option value="02">
								&nbsp;&nbsp; 2月
							</option>
							<option value="03">
								&nbsp;&nbsp; 3月
							</option>
							<option value="04">
								&nbsp;&nbsp; 4月
							</option>
							<option value="05">
								&nbsp;&nbsp; 5月
							</option>
							<option value="06">
								&nbsp;&nbsp; 6月
							</option>
							<option value="07">
								&nbsp;&nbsp; 7月
							</option>
							<option value="08">
								&nbsp;&nbsp; 8月
							</option>
							<option value="09">
								&nbsp;&nbsp; 9月
							</option>
							<option value="10">
								&nbsp;&nbsp; 10月
							</option>
							<option value="11">
								&nbsp;&nbsp; 11月
							</option>
							<option value="12">
								&nbsp;&nbsp; 12月
							</option>
						</select>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						<input class='conversion' type='button' value='  查  询  '
							onclick="query(1);">
						<br />
						<br />

						<div id="loading" style="display: none">
							<table width="96%" class="hovertable">
								<img src="${pageContext.request.contextPath}/img/loading50.gif" />
							</table>
						</div>
						<div id="shuju"></div>

						<div style="LINE-HEIGHT: 56px; HEIGHT: 56px; display: none"	id="page">
							<table>
								<tr>
									<td><a href="javascript:query(1);">[首页]</a></td>
									<td><a href="javascript:query(2);">[&lt;&lt;上一页]</a></td>
									<td><a href="javascript:void(0);"><span class="STYLE30" id="page1"></span></a></td>
									<td><a href="javascript:query(3);">[下一页&gt;&gt;]</a></td>
									<td><a href="javascript:query(4);">[末页]</a></td>
									<td>共 <span class="STYLE30" id="size"></span> 页</td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div class="bottom">
			<span>版权所有 &copy; 北京桓源创新科技有限公司<span>
		</div>
	</body>
</html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns=" http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript">
</script>
</head>
<body>
	<div id="containner">
		<div id="main">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="80" rowspan="3">
					<input type="hidden" id="uuid" name="uuid" value="${appDTO.uuid }"/>
						<div class="info_icon"><img src="${fileserverUrl}${appDTO.icon}" alt="" /></div>
					</td>
					<td>
						<div class="info_name">${appDTO.name}</div>
					</td>
					<td>
						<div class="info_down">
							<a href="${url }" class="info_btn_down">直接下载</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="info_size">大小： <span id="sizeSpan"></span>${appDTO.packFileSize }K</div>
					</td>
					<td width="100">
						<div class="info_star">
							<c:forEach begin="0" end="4">
								<img src="${pageContext.request.contextPath}/img/star-on.png" width="16" height="16" />
							</c:forEach>
						</div>
					</td>
				</tr>
				<tr>
					<td>
					<input type="hidden" name="dis" id="dis" value="${appDTO.discriminator}" ></input>
					<input type="hidden" name="packuuid" id="packuuid" value="${appDTO.packUuid}"></input>
						<div class="info_time">上架时间：${appDTO.packCpUpdateTime}</div>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			
			<div class="info_img">
				<c:forEach items="${appDTO.pics}" var="li"	begin="0" end="0">
					<img src="${fileserverUrl}${li}" alt="Fish" />
				</c:forEach>
			</div>
	
			<div class="info_file">系统： 
				<c:if test="${appDTO.packOs eq 'os1'}">Android</c:if>
				<c:if test="${appDTO.packOs eq 'os2'}">IOS</c:if>
				<c:if test="${appDTO.packOs eq 'os3'}">越狱IOS</c:if>
				<c:if test="${appDTO.packOs eq 'os4'}">SYMBIAN</c:if>
				<c:if test="${appDTO.packOs eq 'os5'}">JAVA</c:if>
				<c:if test="${appDTO.packOs eq 'os6'}">WINMOBILE</c:if>
			</div>
			
			<div class="info_file">版本：${appDTO.packAppVer }</div>
			<div class="info_file">下载： ${appDTO.downCount}</div>
			
			
			<div class="info_file"  id="fxiangqing">简介：
			<font>${appDTO.info}</font>
			</div>
		</div>
		<div class="fo"></div>
	<div class="bottom">&copy; 北京桓源创新科技有限公司 版权所有</div>
	</div>
</body>
</html>
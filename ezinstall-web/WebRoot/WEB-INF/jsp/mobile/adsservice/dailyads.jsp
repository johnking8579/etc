<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>每日一笑</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css1/style.css">
	<link href="${pageContext.request.contextPath}/css1/common.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(function() {
		var maxl=120;//总长
		$("#desc").keyup(function(){
		   var s=$("#desc").val().length;
		   if(s>maxl){
			   $("#desc").val($("#desc").val().substr(0,maxl));
		   }else{ 
			   $("#descnum").html(s+"/"+maxl+" 字符");
		    };
		});
		$("#mtype").change(function() {
			if($("#mtype").val()==2){
				$("#jump").hide();
				$("#file").show();
			}
			if($("#mtype").val()==3){
				$("#file").hide();
				$("#jump").show();
			}
			if($("#mtype").val()==1 || $("#mtype").val()==""){
				$("#file").hide();
				$("#jump").hide();
			}
		});
	});			
function push(){
	if($("#stype").val()=="" ||$("#atype").val()=="" || $("#mtype").val()=="" || $("#desc").val()==""|| $("#eid").val()==""){
		if($("#stype").val()==""){
			alert("发送类型不能为空");
		}else if($("#atype").val()==""){
			alert("接收类型不能为空");
		}else if($("#mtype").val()==""){
			alert("消息类型不能为空");
		}else if($("#eid").val()==""){
			alert("服务不能为空");
		}
	}else{
		if($("#mtype").val()==2){
			if($("#imgurlFile").val()==""){
				alert("图片不能为空");
			}else{
				submit();
			}
		}else if($("#mtype").val()==3){
			if($("#jumpurl").val()==""){
				alert("目标地址不能为空");
			}else{
				submit();
			}
		}else{
			submit();
		}
	}
}
function submit(){
	var ajax_option={
			dataType:"json",
			success:function(data){
				if(data.error==0){
					alert(data.desc);
					window.location.reload(true);
				}else{
					alert(data.desc);
				}
			}
		};
		$('#condForm').ajaxSubmit(ajax_option);
}
function isexits(){
	if($("#pid").val()!=""){
		$.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/push/phoneisexits.action",
			data:{"pid":$("#pid").val()},
			cache:false,
			dataType:"json",
			success:function(data){
				if(data.error==0){
					alert("该手机号无法推送");
					$("#pid").val("");
				}
			}
		});
	}
}
	</script>
  </head>
  <body>
<div class="main-title">公告管理>>消息推送</div>
<form name="condForm" id="condForm" action="${pageContext.request.contextPath}/push/pushsms.action" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="inquire">
  <tr>
    <td class="querytitle">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
        	<tr>
          		<td width="200" height="25" class="bold">推送范围</td>
        	</tr>
        	<tr>
          		<td width="20%" align="right">发送类型： </td>
          		 <td><select name="sms.sendtype" class="input-most" id="stype">
                <option value="1" selected>企业</option>
                  </select></td>
          		<td width="20%" align="right">接收类型： </td>
          		 <td><select name="sms.acceptedtype" class="input-most" id="atype">
                <option value="2" selected>个人</option>
                  </select></td>
          		<td width="20%" align="right">消息类型： </td>
          		<td><select name="sms.messagetype" class="input-most" id="mtype">
	                <option value="">---全部---</option>
	                <option value="1" ${mid eq 1 ? "selected" : ""}>文本</option>
	                <option value="2" ${mid eq 2 ? "selected" : ""}>图片</option>
	                <option value="3" ${mid eq 3 ? "selected" : ""}>链接</option>
                  	</select>
                </td>       
          	</tr>
          	<tr>
          		<td width="20%" align="right">选择服务： </td>
          		<td><select name="sms.senderid" class="input-most" id="eid">
	                <option value="">---全部---</option>
	                <c:forEach items="${enterList }" var="enter">
	                	<option value="${enter.id }" >${enter.enterpriseName }</option>
	                </c:forEach>
                  	</select>
                </td>
                <!-- 
          		<td width="20%" align="right">选择标签： </td>
          		<td><select name="sms.acceptedid" class="input-most" id="tid">
	                <option value="">---全部---</option>
	                <c:forEach items="${tagList }" var="tag">
	                	<option value="${tag.id }" >${tag.tagname }</option>
	                </c:forEach>
                  	</select>
                </td>
                 -->
          		<td width="20%" align="right">输入手机号： </td>
          		<td>
					<input type="text" class="input-most"  name="pid" id="pid" onblur="isexits();">
                </td>                   
          	</tr>          	
          	<tr>
          		<td width="20%" align="right">消息名称： </td>
				<td colspan="5" style="width:100%;">
					<input type="text" class="input-most"  name="title">
				</td>          	
          	</tr>
          	<tr id="file" style='display: none;'>
          		<td width="20%" align="right">选择文件： </td>
				<td colspan="5" style="width:100%;">
					<input type="file" class="input-most" id="imgurlFile" name="imgurlFile">
				</td>          	
          	</tr>
          	<tr id="jump" style='display: none;'>
          		<td width="20%" align="right">目标地址： </td>
				<td colspan="5" style="width:100%;">
					<input type="text" class="input-most"  id="jumpurl" name="sms.jump_url">
				</td>          	
          	</tr>
          	<tr>
          		<td width="20%" align="right">消息内容： </td>
				<td colspan="5" style="width:100%;">
					<textarea rows="10" cols="100%" style="resize:none;" id="desc" name="sms.messagecontent"></textarea>
				</td>          	
          	</tr>
			 <tr>
			 	<td width="20%"  align="right">已输入字符:</td>
			 	<td><div id="descnum"></div></td>
			 </tr>	          	
          	<tr>
			    <td width="20%" align="right">
			    	<input name="button" type="button" class="submit-button" value=" 推    送 " onclick="push();">
			    </td>                  	
          	</tr>
    	</table>
	</td>
  </tr>
</table>
</form>
  </body>
</html>

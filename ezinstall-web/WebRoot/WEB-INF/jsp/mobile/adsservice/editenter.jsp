<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改服务</title>
<link href="${pageContext.request.contextPath}/css1/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript">
	$(function() {
		
		var maxl=500;//总长
		$("#desc").keyup(function(){
		   var s=$("#desc").val().length;
		   if(s>maxl){
			   $("#desc").val($("#desc").val().substr(0,maxl));
		   }else{ 
			   $("#descnum").html(s+"/"+maxl+" 字符");
		    };
		});
		
		$("#empName").focus();
		$("#empName").blur(function() {
			if ($.trim(this.value) == "") {
				$("#empNamespan").html("<font color='red'>企业名不能为空</font>");	
				$("#empName").focus();
			}else {
				$("#empNamespan").html("");
			}			
		}); 
		var options = {
			beforeSubmit: function() {
				$.ajaxSetup({
					async: false //同步
				});	
				$(":input.input-most").blur();
				var flag = false;
				$.each($("span"), function() {
					if (this.innerHTML != "") {
						flag = true;
						return false; //return false表示退出循环，等同break，return true等同continue
					}
				});
				$.ajaxSetup({
					async: true //异步
				});					
				if (flag) {
					alert("数据不合法，请验证！");
				}
				return !flag;
			},
			dataType:"json",
			success: function(data) { 
				if(data.error==0){
					alert("修改成功！");
					window.opener.location.reload(true);
					window.close();
				}else{
					alert("修改失败！");
				}
			},
			error: function() {
				alert("修改企业服务失败！");			
			},
			dataType: 'json'
		};
		$('#eiitForm').ajaxForm(options); 				
	});	

</script>
</head>
<body>
<form action="${pageContext.request.contextPath}/push/eiit.action" method="post" id="eiitForm">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="inquire">
  <tr>
    <td height="400" class="con">
		<table width="95%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#5480A2" class="tab-padding">
        	<!-- 表头信息 -->
        	<tr>
		    	<td colspan="4" class="bg3 bold" >服务信息</td>
		    	<td><input type="hidden" name="en.id" value="${en.id }"/></td>
        	</tr>
	  		<tr>
				 <td class="bg3" align="right"><font color="#FF0000">*</font>企业名称：</td>
        		 <td class="bg1"><input type="text"  class="input-most" name="en.enterpriseName" id="empName" value="${en.enterpriseName }"><span id="empNamespan"></span></td>
  		  	</tr>
			 <tr>
				<td class="bg3" align="right">企业别名：</td>
				<td class="bg1"><input type="text" class="input-most"  name="en.nickname" value="${en.nickname }"></td>
			 </tr>
			 <tr>
				<td class="bg3" align="right"><font color="#FF0000">*</font>是否激活：</td>
				<td class="bg1">
				<select name="en.status" class="input-most">
                  <option value="0">---请选择---</option>
                  <option value="0" ${en.status eq 0 ? "selected":"" }>是</option>
                  <option value="1" ${en.status eq 1 ? "selected":"" }>否</option>
                </select></td>
			 </tr>
			 <tr>
				<td class="bg3" align="right">企业简介：</td>
				<td class="bg1"><textarea cols="40" rows="10" style="resize:none" name="en.intro" class="input-most" id="desc">${en.intro }</textarea>
			 </tr>
			 <tr>
			 	<td class="bg3" align="right">已输入字符:</td>
			 	<td class="bg1"><div id="descnum"></div></td>
			 </tr>					 
			 <tr>
				<td class="bg3" align="right">企业缩影：</td>
				<td class="bg1"><input type="file" class="input-most"  name="imgurlFile" value="${en.nickname }"></td>
			 </tr>
	  	     <!-- 按钮 -->
     		 <tr align="center">
       			 <td height="26" colspan="4" style="padding-top:20px;" class="bg1">
				 	<input type="submit" class="submit-button" value="修改服务">
				 	&nbsp;
					<input type="button" class="submit-button" onclick="window.close()" value="关闭窗口" >			   </td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
</form>
</body>
</html>
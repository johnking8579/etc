<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>员工维护</title>
<link href="${pageContext.request.contextPath}/css1/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script language="javascript">
function del(empId) {
	if (window.confirm("确认删除？")) {
		$.ajax({
			url: "${pageContext.request.contextPath}/push/deletetag.action",
			data: {"tag.id":empId},
			cache: false,
			success: function() {
				window.location.reload(true);
				alert("删除服务成功！");
			},
			error: function() {
				alert("删除失败！");
			}
		});
	}	
}

function bulkDel() {
	$.ajaxSettings.traditional=true;//解决struts2不能给数组赋值的bug，juery1.3以上都存在这个问题，设置此属性便好。
	if ($("input[type=checkbox]:checked").length == 0) {
		alert("请选择需要删除的数据？");
	}else {
		if (window.confirm("确认删除？")) {
			var empIds = new Array();
			$.each($("input[type=checkbox]:checked"), function() {
				empIds.push(this.value);
			});
			$.ajax({
				url: "${pageContext.request.contextPath}/push/bulkDeltag.action",
				data: {"enIds": empIds},
				cache: false,
				success: function() {
					window.location.reload(true);
					alert("删除成功！");
				},
				error: function() {
					alert("删除失败！");
				}
			});
		}
	}	
}

function checkAll(field) {
	$("input[name='empId']").attr("checked", field.checked);
}
$(document).ready(function(){
	$("#allSize").html($("#total").val());
	$("#pSize").html($("#pageSize").val());
});
</script>
</head>
<body>
<div class="main-title">标签管理>>标签维护</div>
<form name="condForm" id="condForm" action="${pageContext.request.contextPath}/push/tagmanager.action">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="inquire">
  <tr>
    <td class="querytitle">
		<!-- 查询标题和查询按钮 -->
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
        	<tr>
          		<td width="200" height="25" class="bold">查询条件</td>
          		<td width="20%" align="right">标签名称： </td>
			  	<td width="36%"><input name="tag.tagname" type="text" size="25" class="input-most" value="${tag.tagname }"></td>
          		<td width="750" align="right"><input name="submit" type="submit" class="submit-button" value="查询"></td>
        	</tr>
    	</table>
    	
	</td>
  </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="list">
	<!-- 增删改按钮 -->
  <tr>
	<td valign="bottom" class="topbg">
			<a href="javascript:void(0);" onclick="openWin('${pageContext.request.contextPath}/push/addtag.action','addemp',600,500);">
			<img class="addimg"  title="添加" src="${pageContext.request.contextPath}/images/add.png"/></a>&nbsp;
			<a href="javascript:void(0);" onclick="bulkDel()"><img class="addimg"  title="批量删除" src="${pageContext.request.contextPath}/images/buldelete.png"/></a>
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
      		<tr>
        		<td class="title">
					<!-- 表头 -->
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
          				<tr>
            				<td width="6%" align="center">
            				<input type="checkbox" name="checkbox" onclick="checkAll(this)">全选</td>
            				<td width="8%" align="center">标签名称</td>
            				<td width="19%" align="center">标签介绍</td>
						    <td width="5%" align="center">操作</td>
          				</tr>
   					</table>				
				</td>
   			</tr>
      		<tr>
        		<td>
					<!-- 表体 -->
					<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#758795">
						<c:choose>
							<c:when test="${empty data.list}">
								<tr>
									<td colspan="11">没有符合条件的数据</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${data.list}" var="tag" varStatus="vs">
			          				<tr class="bg${vs.count % 2 != 0 ? 1 : 2}">
										<td width="6%" align="center">
											<input type="checkbox" name="empId" value="${tag.id}">
										</td>
			            				<td width="8%" align="center">${tag.tagname }</td>
						            	<td width="19%" align="center">${tag.tagDesc }</td>
									    <td width="5%" align="center">
									    	<a href="javascript:void(0)" onclick="openWin('${pageContext.request.contextPath}/push/edittag.action?tag.id=${tag.id}','editemp',600,500);">
									    	<img class="addimg"  title="修改" src="${pageContext.request.contextPath}/images/edit.png"/>
									    	</a>&nbsp;
									    	<a href="javascript:void(0)" onclick="del('${tag.id}')"><img class="addimg"  title="删除" src="${pageContext.request.contextPath}/images/delete.png"/></a>
									    </td>
			          				</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
        			</table>				
				</td>
      		</tr>
    	</table>	
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			   <!-- 分页处理 -->	
			  <td align="right">[共<font color="red" id="allSize"></font>条/<font color="red" id="pSize"></font>页]&nbsp; <!-- maxIndexPages="5" isOffset="false"	export="pageOffset,currentPageNumber=pageNumber" scope="request" -->
				<pg:pager items="${data.total}" maxPageItems="20" url="${pageContext.request.contextPath}/push/tagmanager.action" export="currentPageNumber=pageNumber">
					<pg:param name="tag.tagname" value="${tag.tagname}"/>
					<input type="hidden" id="total" value="${data.total}"/>
					<input type="hidden" id="pageSize" value="${fn:substringBefore(data.total/20+1,'.')}"/>
										
					<pg:first>
						<a href="${pageUrl}">第一页</a>
					</pg:first>
					<pg:prev>
						<a href="${pageUrl}">上一页</a>
					</pg:prev>
					<pg:pages>
						<c:choose>
							<c:when test="${currentPageNumber eq pageNumber}">
								<font color="red">${pageNumber}</font>
							</c:when>
							<c:otherwise>
								<a href="${pageUrl}">${pageNumber}</a>	
							</c:otherwise>
						</c:choose>
					</pg:pages>
					<pg:next>
						<a href="${pageUrl}">下一页</a>
					</pg:next>
				 	<pg:last>
				 		<a href="${pageUrl}">最后一页</a>
				 	</pg:last>
				</pg:pager>
				<!-- 
				  <a href="#" onClick="submitPage(1)">转到</a>
				  <input name="pageNo" type="text"  id="pageNo" size="3"  value="1" >
				 -->
				  页&nbsp;页面大小
				  <input name="pagesize" type="text" size="2" value="20" readonly>
			 </td>
			</tr>
		</table>	
	</td>
  </tr>
</table>
</body>
</html>
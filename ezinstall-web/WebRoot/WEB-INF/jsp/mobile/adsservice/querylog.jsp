<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>员工维护</title>
<link href="${pageContext.request.contextPath}/css1/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/My97DatePicker/WdatePicker.js"></script>
<script language="javascript">
function yanzheng(){
    if($("#startTime").val()=="" || $("#endTime").val()==""){
        alert("请选择开始日期和截止日期!");
        return false; 
    }else{
		var date = new Date(new Date().getTime());
		var year=date.getFullYear();
		var month = date.getMonth();
		var day = date.getDate();
		month++;
		if (month < 10) {
			month = "0" + month;
		}
		if (day < 10) {
			day = "0" + day;
		}
    	var d=new Date(Date.parse($("#startTime").val().replace(/-/g,"/")));
    	var e=new Date(Date.parse($("#endTime").val().replace(/-/g,"/")));
    	var curDate=new Date();
    	if(d >curDate){
    		$("#startTime").val(year+"-"+month+"-"+day);
    		alert("开始日期不能大于今天的时间！");
    		return;
    	}else if(e >curDate){
    		$("#endTime").val(year+"-"+month+"-"+day);
    		alert("截止日期不能大于今天的时间！");
    		return;
    	}else{
    		if(d>e){
    			$("#startTime").val($("#endTime").val());
    			alert("开始日期不能大于截止日期！");
    			return;
    		}else{
    			$("#condForm").submit();
    		}
    	}
    }
}


$(function() {
	if($("#endTime").val()=="" || $("#startTime").val()==""){
		var date = new Date(new Date().getTime());
		var year=date.getFullYear();
		var month = date.getMonth();
		var day = date.getDate();
		month++;
		if (month < 10) {
			month = "0" + month;
		}
		if (day < 10) {
			day = "0" + day;
		}
		$("#startTime").val(year+"-"+month+"-"+day);
		$("#endTime").val(year+"-"+month+"-"+day);
	}
});

$(document).ready(function(){
	$("#allSize").html($("#total").val());
	$("#pSize").html($("#pageSize").val());
});
</script>
</head>
<body>
<div class="main-title">消息管理>>消息日志</div>
<form name="condForm" id="condForm" action="${pageContext.request.contextPath}/push/querylog.action">
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="inquire">
  <tr>
    <td class="querytitle">
		<!-- 查询标题和查询按钮 -->
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
        	<tr>
          		<td width="200" height="25" class="bold">查询条件</td>
        	</tr>
        	<tr>
          		<td width="20%" align="right">发送类型： </td>
          		 <td><select name="sid" class="input-most">
                <option value="">---全部---</option>
                <option value="1" ${sid eq 1 ? "selected" : ""}>企业</option>
                <option value="2" ${sid eq 2 ? "selected" : ""}>个人</option>
                  </select></td>
          		<td width="20%" align="right">接收类型： </td>
          		 <td><select name="aid" class="input-most">
                <option value="">---全部---</option>
                <option value="1" ${aid eq 1 ? "selected" : ""}>企业</option>
                <option value="2" ${aid eq 2 ? "selected" : ""}>个人</option>
                  </select></td>
          		<td width="20%" align="right">消息类型： </td>
          		<td><select name="mid" class="input-most">
	                <option value="">---全部---</option>
	                <option value="1" ${mid eq 1 ? "selected" : ""}>文本</option>
	                <option value="2" ${mid eq 2 ? "selected" : ""}>图片</option>
	                <option value="3" ${mid eq 3 ? "selected" : ""}>链接</option>
                  	</select>
                </td>                    
          	</tr>
          	<tr>
		        <td width="20%" align="right"><font color="red">*</font>开始时间 </td>
		        <td>
					<input type="text"  readonly style="width:120px;" id="startTime" name="startTime" value="${startTime }"  onfocus="WdatePicker({isShowToday:false,readOnly:false,dateFmt:'yyyy-MM-dd'})" />
			    </td>
			    <td width="20%" align="right"><font color="red">*</font>结束时间  </td>
			    <td>
					<input type="text" readonly style="width:120px;" id="endTime" name="endTime"  value="${endTime }" onfocus="WdatePicker({isShowToday:false,readOnly:false,dateFmt:'yyyy-MM-dd'})" />
			    </td>
			    <td width="20%" align="right">
			    	<input name="button" type="button" class="submit-button" value=" 查  询 " onclick="yanzheng();">
			    </td>
          	</tr>
    	</table>
    	
	</td>
  </tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="list">
	<!-- 增删改按钮 -->
  <tr>
	<td valign="bottom" class="topbg">
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
      		<tr>
        		<td class="title">
					<!-- 表头 -->
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
          				<tr>
            				<td width="6%" align="center">发送类型</td>
            				<td width="8%" align="center">发送者</td>
            				<td width="6%" align="center">接收类型</td>
						    <td width="8%" align="center">接收者</td>
						    <td width="6%" align="center">消息类型</td>
						    <td width="10%" align="center">文本内容</td>
						    <td width="10%" align="center">文件地址</td>
						    <td width="10%" align="center">链接地址</td>
						    <td width="5%" align="center">是否删除</td>
						    <td width="10%" align="center">发送时间</td>
          				</tr>
   					</table>				
				</td>
   			</tr>
      		<tr>
        		<td>
					<!-- 表体 -->
					<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#758795">
						<c:choose>
							<c:when test="${empty datalog.list}">
								<tr>
									<td colspan="11" align="center" style="color:red;">
										<c:choose>
										<c:when test="${startTime !=null && endTime !=null }">
										没有符合条件的数据
										</c:when>
										<c:otherwise>
										请选择查询条件
										</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${datalog.list}" var="log" varStatus="vs">
			          				<tr class="bg${vs.count % 2 != 0 ? 1 : 2}">
				          				<c:choose>
				          					<c:when test="${log.sendtype eq 1 }"><td width="6%" align="center" title="企业">企业</td></c:when>
				          					<c:otherwise><td width="6%" align="center"  title="个人">个人</td></c:otherwise>
				          				</c:choose>
						            	<td width="8%" align="center"  title="${log.sender }">${fn:substring(log.sender,0,8) }</td>
				          				<c:choose>
				          					<c:when test="${log.acceptedtype eq 1 }"><td width="6%" align="center"  title="企业">企业</td></c:when>
				          					<c:otherwise><td width="6%" align="center"  title="个人">个人</td></c:otherwise>
				          				</c:choose>						            	
						            	<td width="8%" align="center" title="${log.recipients }">${fn:substring(log.recipients,0,8) }</td>
				          				<c:choose>
				          					<c:when test="${log.messagetype eq 1 }"><td width="6%" align="center"  title="文本">文本</td></c:when>
				          					<c:when test="${log.messagetype eq 1 }"><td width="6%" align="center"  title="图片">图片</td></c:when>
				          					<c:otherwise><td width="6%" align="center"  title="链接">链接</td></c:otherwise>
				          				</c:choose>			            				
						            	<td width="10%" align="center"  title="${log.messagecontent }">${fn:substring(log.messagecontent,0,10) }</td>
			            				<td width="10%" align="center"  title="${log.file_url }">${fn:substring(log.file_url,0,10) }</td>
						            	<td width="10%" align="center"  title="${log.jump_url }">${fn:substring(log.jump_url,0,10) }</td>
				          				<c:choose>
				          					<c:when test="${log.isdelete eq 1 }"><td width="5%" align="center"  title="否">否</td></c:when>
				          					<c:otherwise><td width="5%" align="center"  title="是">是</td></c:otherwise>
				          				</c:choose>						            	
						            	<td width="10%" align="center" title="${log.sendtime }">${fn:substring(log.sendtime,0,10) }</td>
			          				</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
        			</table>				
				</td>
      		</tr>
    	</table>	
	</td>
  </tr>
  <tr>
    <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			   <!-- 分页处理 -->	
			  <td align="right">[共<font color="red" id="allSize"></font>条/<font color="red" id="pSize"></font>页]&nbsp; <!-- maxIndexPages="5" isOffset="false"	export="pageOffset,currentPageNumber=pageNumber" scope="request" -->
				<pg:pager items="${datalog.total}" maxPageItems="20" url="${pageContext.request.contextPath}/push/querylog.action" export="currentPageNumber=pageNumber">
					<pg:param name="sid" value="${sid}"/>
					<pg:param name="aid" value="${aid}"/>
					<pg:param name="mid" value="${mid}"/>
					<pg:param name="isdelete" value="${isdelete}"/>
					<pg:param name="startTime" value="${startTime}"/>
					<pg:param name="endTime" value="${endTime}"/>
					<input type="hidden" id="total" value="${datalog.total}"/>
					<input type="hidden" id="pageSize" value="${fn:substringBefore(datalog.total/20+1,'.')}"/>
										
					<pg:first>
						<a href="${pageUrl}">第一页</a>
					</pg:first>
					<pg:prev>
						<a href="${pageUrl}">上一页</a>
					</pg:prev>
					<pg:pages>
						<c:choose>
							<c:when test="${currentPageNumber eq pageNumber}">
								<font color="red">${pageNumber}</font>
							</c:when>
							<c:otherwise>
								<a href="${pageUrl}">${pageNumber}</a>	
							</c:otherwise>
						</c:choose>
					</pg:pages>
					<pg:next>
						<a href="${pageUrl}">下一页</a>
					</pg:next>
				 	<pg:last>
				 		<a href="${pageUrl}">最后一页</a>
				 	</pg:last>
				</pg:pager>
				<!-- 
				  <a href="#" onClick="submitPage(1)">转到</a>
				  <input name="pageNo" type="text"  id="pageNo" size="3"  value="1" >
				 -->
				  页&nbsp;页面大小
				  <input name="pagesize" type="text" size="2" value="20" readonly>
			 </td>
			</tr>
		</table>	
	</td>
  </tr>
</table>
</body>
</html>
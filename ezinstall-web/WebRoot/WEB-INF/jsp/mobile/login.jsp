<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>手机加油站--后台管理系统</title>
<link href="${pageContext.request.contextPath}/css1/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<link href="${pageContext.request.contextPath}/css1/login.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript">
$(function() {
	$("#empNo").focus();
	if (window.self != window.top) {
		window.top.location = "${pageContext.request.contextPath}/pushLogin/login.action";
	}
	$("body").keydown(function()	{
		if(window.event.keyCode == 13  || window.event.which == 13)	{
			if($("#empNo").val()=="" || $("#empNo").val()==null){
				alert("请输入用户名");
				$("#empNo").focus();
				return false;
			}else if($("#pass").val()==""){
	  	  		alert("请输入密码");
	  	  		$("#pass").focus();
	  	  		return false;
	  	  	}else{	  
	  	  		denglu();
	  	  	}
		}
	});
});

function denglu(){
	showBlock("${pageContext.request.contextPath}/images/loading.gif");
    $.ajax({
		url:"${pageContext.request.contextPath}/pushLogin/loginto.action",
		data:{"empNo":$.trim($("#empNo").val()),"provId":$.trim($("#provId").val()),"pass":hex_md5($.trim($("#pass").val()))},
		type:"post",
		dataType: "html",
		cache:false,
	//	beforeSend:function(){
	//		$('#loading').show();
	//	},
		success:function(data){
			hideBlock();
			if (data==0) {
				window.location = "${pageContext.request.contextPath}/push/index.action";
			}else{
				alert(data);
			}
		}
	});
};
</script>
</head>
<body>
<div class="logo"></div>
<div class="login">
  <table width="100%%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="490" height="105">&nbsp;</td>
      <td width="72">&nbsp;</td>
      <td width="140">&nbsp;</td>
      <td width="268">&nbsp;</td>
    </tr>
    <tr>
      <td height="22">&nbsp;</td>
      <td>&nbsp;</td>
      <td>
      <select name="provId" id="provId" style="width:130px; border:0px ; padding:0;margin:0;">
      	<option value="1000">全国</option>
      	<option value="9">上海</option>
      	<option value="19">广东</option>
      	<option value="23">陕西</option>
      	<option value="18">湖南</option>
      	<option value="4">山西</option>
      	<option value="15">山东</option>
      	<option value="22">甘肃</option>
      	<option value="24">新疆</option>
		<option value="21">广西</option>
		<option value="36">内蒙</option>
      </select></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="18">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="22">&nbsp;</td>
      <td>&nbsp;</td>
      <td><label for="textfield"></label>
      <input name="empNo" id="empNo" type="text" class="text" placeholder="请输入用户名" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="18">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="22">&nbsp;</td>
      <td>&nbsp;</td>
      <td><label for="textfield2"></label>
      <input name="pass" id="pass" type="password" class="text" placeholder=' 请输入密码 '/></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="30">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="26">&nbsp;</td>
      <td colspan="2" align="center"><input name="Submit2" type="submit" class="btn-submit" id="button" onClick="denglu();" value="" />
      <input name="button2" type="reset" class="btn-reset" id="button2"  value="" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</div>
</body>
</html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns=" http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () { 
		$("#ostype").change(function(){
				    $.ajax({
						url:"${pageContext.request.contextPath}/wap/sessionOstype.action",
						data:{"ostype":$('#ostype').val(),_:new Date().getTime()},
						type:"post",
						dataType:"json",
						success:function(data){
							if(data.success=1){
								window.location=document.URL;
							}
						}
					});  		
					// location.reload();
					
		});
		//alert(document.URL);
	});
	function search(){
		document.forms["softInfo"].submit();
	}
</script>
</head>
<body>
		<div id="head">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="140" align="center">
						<img src="${pageContext.request.contextPath}/img/sh-logo.png" />
					</td>
					<td width="180">
						<select id="ostype" class="ss_sss">
							<option value="1" ${ostype eq 1?"selected":"" }>Android</option>
							<option value="2" ${ostype eq 2?"selected":"" }>IOS</option>
							<option value="3" ${ostype eq 3?"selected":"" }>IOSX</option>
							<option value="4" ${ostype eq 4?"selected":"" }>Symbian</option>
							<option value="5" ${ostype eq 5?"selected":"" }>Windows</option>
							<option value="6" ${ostype eq 6?"selected":"" }>JAVA</option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="320" align="center">
					<form action="${pageContext.request.contextPath}/wap/findsoftByName.action" name="softInfo" method="post">
						<div class="ss">
							<input type="text" class="ss_text" id="softName" value="${softName }" name="softName" />
							<div class="ss_ss"><input type="button" class="ss_btn" onclick="search()" ;/></div>
						</div>
					</form>
					</td>
				</tr>				
			</table>
			
		</div>
		<c:choose>
				<c:when test="${fn:contains(checkPage,emp.province.provinceID) }">
					<div class="nav">
						<ul>
							<li><a href="${pageContext.request.contextPath}/wap/findDesignationAppByPage.action">必备</a></li>
							<li><a href="${pageContext.request.contextPath}/wap/findAllSoftInfoByPage.action">排行</a></li>
						</ul>
					</div>
				</c:when>
		</c:choose>
		<!-- 
		 -->
</body>
</html>
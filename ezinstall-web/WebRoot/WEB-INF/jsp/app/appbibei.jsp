<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/css/style.css"  rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div id="containner">
			<jsp:include page="hearder.jsp" flush="true">
			   	<jsp:param value="" name=""/>
			</jsp:include>
			<div id="main">
				<div class="tui2">
					<c:forEach items="${map}" var="speType" varStatus="vs">
						<div class="title">
							${speType.key}
						</div>
						<div class="tui2_nei">
							<table width="100%" border="1" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<c:forEach items="${speType.value}" var="st" end="5">
											<div class="tui2_kid">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td width="60" rowspan="2">
															<div class="tui2_kid_icon">
																<a
																	href="${pageContext.request.contextPath}/app/findsoftInfo.action?softInfo.softid=${st.softid}">
																	<img src="${fileserverUrl}${st.iconfilename}" />
																</a>
															</div>
														</td>
														<td>
															<div class="tui2_kid_name">
																${st.softname}
															</div>
														</td>
													</tr>
													<tr>
														<td >
															<div class="tui2_kid_star">
															<c:forEach begin="0" end="4">
																<img src="${pageContext.request.contextPath}/img/star-on.png" width="16" height="16" />
															</c:forEach>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</c:forEach>
									</td>
								</tr>
							</table>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="fo"></div>
		</div>
	</body>
</html>

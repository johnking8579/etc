<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/css/style.css"
			rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div id="containner">
<jsp:include page="hearder.jsp" flush="true">
   	<jsp:param value="" name=""/>
</jsp:include>
			<div id="main">
				<div class="tui2">
					<!--  
						<div class="title">
							必备
						</div>
					-->
					<div class="tui2_nei">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<pg:pager maxPageItems="${application.pageFenLeiSize}"
										maxIndexPages="2" isOffset="false"
										export="offset,currentPageNumber=pageNumber" scope="request"
										url="${pageContext.request.contextPath }/app/findsoftTypeInfo.action">
										
										<pg:param name="softy" value="${param.softy}"/>
										<div class="tui2_nei">
										<c:forEach items="${list1}" var="soft" varStatus="vs">
											<pg:item>
												<div class="tui2_kid">
													<table width="100%" border="0" cellspacing="0"
														cellpadding="0">
														<tr>
															<td width="60" rowspan="2">
																<div class="tui2_kid_icon">
																	<a
																		href="${pageContext.request.contextPath}/app/findsoftInfo.action?softInfo.softid=${soft.softid}">
																		<img
																			src="${fileserverUrl}${soft.iconfilename}" />
																	</a>
																</div>
															</td>
															<td>
																<div class="tui2_kid_name">
																	${soft.softname} 
																</div>
															</td>
														</tr>
														<tr>
															<td>
																<div class="tui2_kid_star">
																	<img
																		src="${pageContext.request.contextPath}/img/star-on.png"
																		width="16" height="16" />
																	<img
																		src="${pageContext.request.contextPath}/img/star-on.png"
																		width="16" height="16" />
																	<img
																		src="${pageContext.request.contextPath}/img/star-on.png"
																		width="16" height="16" />
																	<img
																		src="${pageContext.request.contextPath}/img/star-on.png"
																		width="16" height="16" />
																	<img
																		src="${pageContext.request.contextPath}/img/star-on.png"
																		width="16" height="16" />
																</div>
															</td>
														</tr>
													</table>
												</div>
											</pg:item>
										</c:forEach>
										<div class="clear"></div>
										</div>
										<div class="page">
										<pg:index>
											<pg:first>
												<a href="${pageUrl}">[首页]</a>
											</pg:first>
											<pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a>
											</pg:prev>
											<pg:pages>
												<c:choose>
													<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b>
													</c:when>
													<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a>
													</c:otherwise>
												</c:choose>
											</pg:pages>
											<pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a>
											</pg:next>
											<pg:last>&nbsp;<a href="${pageUrl}">[末页]</a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
											<br />
										</pg:index>
										</div>
									</pg:pager>
								</td>
							</tr>
						</table>
					</div>
					
					<div class="tui2">
					</div>
					<div class="tui2">
						<div class="title">
							精品推荐
						</div>
						<div class="info_tui_nei">
							<c:forEach items="${list}" var="li" varStatus="vs">
								<div class="info_tui_kid">
									<div class="info_tui_kid_icon">
										<a
											href="${pageContext.request.contextPath}/app/findsoftInfo.action?softInfo.softid=${li.softid}">
											<img
												src="${httpPath }${li.iconfilename}" />
										</a>
									</div>
									<div class="info_tui_kid_name">
										${li.softname}
									</div>
									<div class="info_tui_kid_star">
										<img src="${pageContext.request.contextPath}/img/star-on.png"
											width="16" height="16" />
										<img src="${pageContext.request.contextPath}/img/star-on.png"
											width="16" height="16" />
										<img src="${pageContext.request.contextPath}/img/star-on.png"
											width="16" height="16" />
										<img src="${pageContext.request.contextPath}/img/star-on.png"
											width="16" height="16" />
										<img src="${pageContext.request.contextPath}/img/star-on.png"
											width="16" height="16" />
									</div>
								</div>
							</c:forEach>
							<div class="clear"></div>
						</div>

					</div>
				</div>
				<div class="fo"></div>
			</div>
	</body>
</html>

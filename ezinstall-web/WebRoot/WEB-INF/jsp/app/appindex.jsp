<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.7.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function () { 
	
		});
		/*
function query(obj){
	window.location='${pageContext.request.contextPath}/wap/findDesignationAppByPackage.action?packageid='+obj+'&ostype='+$("#ostype").val();
}
*/
		</script>
	</head>
	<body>
		<div id="containner">
			<jsp:include page="hearder.jsp" flush="true">
			   	<jsp:param value="" name=""/>
			</jsp:include>
			<div id="main">
				<div class="title">
				<!-- 
					<input type="checkbox"" id="checkboxall" name="checkboxall">全选</input>
				 -->
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					必备软件包
				</div>
					
				<table width="100%" border="1" cellpadding="0" cellspacing="0">
					<tr>
						<td>

<pg:pager maxPageItems="20"
								maxIndexPages="2" isOffset="false"
								export="offset,currentPageNumber=pageNumber" scope="request"
								url="${pageContext.request.contextPath }/wap/findDesignationAppByPage.action">
														
								<div class="tui2_nei">
								<c:forEach items="${pkgDTOList}" var="soft" varStatus="vs">
									<pg:item>
										<div class="tui2_kid">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
												<!-- 
													<td>
														<input type="checkbox"" id="checkbox${vs.count }" name="checkbox" value="${soft.id }"/>
													</td>												
												 -->
													<td width="60" rowspan="2">
														<div class="tui2_kid_icon">
															<a href="${pageContext.request.contextPath}/wap/findDesignationAppByPackage.action?packageid=${soft.id}"><img src="${fileserverUrl}${soft.icon}" /></a>
														</div>
													</td>
													<td>
														<div class="tui2_kid_name">${soft.name}</div>
													</td>
													<td>
														<div class="tui2_kid_star">
														</div>
													</td>
												</tr>
											</table>
										</div>
										</pg:item>
								</c:forEach>
								<div class="clear"></div>
								</div>
								<div class="page">
							<pg:index>
									<pg:first>
										<a href="${pageUrl}">[首页]</a>
									</pg:first>
									<pg:prev>&nbsp;<a href="${pageUrl}">[&lt;&lt;上一页]</a>
									</pg:prev>
									<pg:pages>
										<c:choose>
											<c:when test="${pageNumber eq currentPageNumber}">&nbsp;&nbsp;<b>${pageNumber}</b>
											</c:when>
											<c:otherwise>&nbsp;&nbsp;<a href="${pageUrl}">[${pageNumber}]</a>
											</c:otherwise>
										</c:choose>
									</pg:pages>
									<pg:next>&nbsp;<a href="${pageUrl}">[下一页&gt;&gt;]</a>
									</pg:next>
									<pg:last>&nbsp;<a href="${pageUrl}">[末页]</a>&nbsp;&nbsp;共${pageNumber}页</pg:last>
									<br />
								</pg:index>								 
								</div>
				</pg:pager>
						</td>
					</tr>
				</table>
			</div>
			<div class="fo"></div>
		</div>
	</body>
</html>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>My JSP 'FromPost.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">

  </head>
  
  <body>
  <form action="${pageContext.request.contextPath}/people/image_backup.action" method="post" enctype="multipart/form-data">
  	<table>
  		<tr>
  			<td>
  				I D：<input type="text" id="uid" name="uid" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				手机：<input type="text" id="number" name="number" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				图片名称：<input type="text" id="name" name="name" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				时间：<input type="text" id="time" name="time" />
  			</td>
  		</tr>  		  		
  		<tr>
  			<td>
  				文件：<input type="file" id="content" name="content" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				大小：<input type="text" id="size" name="size" />
  			</td>
  		</tr>   		
  		<tr>
  			<td>
  				<input type="submit" id="submit" name="submit" value="保存"/>
  			</td>
  		</tr>   		
  	</table>
  </form>
  </body>
</html>

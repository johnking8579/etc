<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>My JSP 'FromPost.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">

  </head>
  
  <body>
  <form action="${pageContext.request.contextPath}/people/contactsBackup.action" method="post" enctype="multipart/form-data">
  	<table>
  		<tr>
  			<td>
  				I D：<input type="text" id="peopleId" name="peopleId" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				名称：<input type="text" id="username" name="username" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				版本：<input type="text" id="vcfVersion" name="vcfVersion" />
  			</td>
  		</tr>
  		<tr>
  			<td>
  				文件：<input type="file" id="vcardFile" name="vcardFile" />
  			</td>
  		</tr>  		
  		<tr>
  			<td>
  				数量：<input type="text" id="count" name="count" />
  			</td>
  		</tr>   		
  		<tr>
  			<td>
  				描述：<textarea rows="10" cols="30" id="memo" name="memo"></textarea>
  			</td>
  		</tr>
  		<tr>
  			<td>
  				<input type="submit" id="submit" name="submit" value="保存"/>
  			</td>
  		</tr>   		
  	</table>
  </form>
  </body>
</html>

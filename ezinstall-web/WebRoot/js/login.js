function left(){
	if($("#LoginForm_email").val()==''){
		return;
	}
	$("#loadImage").show();
	$.get("../index/verification.action", {empNo:$("#LoginForm_email").val(),_:new Date().getTime()}, 
	function(data) {
		$("#loadImage").hide();
		if(data.flag==1){
			alert("无此工号");
		}else if(data.flag==2){
			$("#prodiv").hide();
		}else if(data.flag==3){
			$("#prodiv").show();
		}else{
			alert("抱歉，请检查网络链接！");
		}
	},"json");
}
function verify(){
	if($.trim($('#LoginForm_email').val()) == ""){
		alert("请输入账号");
	 	return;
	}
	if($.trim($('#LoginForm_password').val()) == ""){
		alert("请输入密码");
	 	return;
	}	
	if($.trim($('#pro').val()) == 0 && document.getElementById("prodiv").style.display!='none'){
		alert("请选择省份");
	 	return;
	}	
	showBlock();
    $.ajax({
		url:"../index/login.action",
		data:{
			"empNo":$.trim($('#LoginForm_email').val()),
			"pass":hex_md5($.trim($('#LoginForm_password').val())),
			"pro":$.trim($('#pro').val())
			},
		type:"post",
		dataType:"json",
		success:function(data){
			hideBlock();
			switch(data.flag){
			case 1:
				if($.trim($('#pro').val())!=0){
					alert("密码错误！");
					break;
				}
				alert("工号不存在");
				break;
			case 2:
				alert("密码不能为空");
				break;	
			case 3:
				alert("密码错误");
				break;	
			case 6:
				alert("该帐号状态为删除");
				break;	
			case 7:
				alert("该帐号状态为冻结");
				break;	
			case 4:
				$( "#loginDialog" ).dialog("close");
				window.location.href="../pay/bonus.action";
				break;			
			case 5:
				$( "#loginDialog" ).dialog("close");
				window.location.href="../pay/person.action";
				break;					
			default:
				alert("抱歉，请检查网络链接！");
				break;
			}
		}
	});   	
}

function loginon(){
	$("#loginDialog").dialog("open");
}

function loginout() {
	$.get(
			"../index/loginout.action",
			function(data) {
				if (data == 1) {
					window.location.href = "../index/index.action";
				} else {
					alert(data);
				}
			});
}

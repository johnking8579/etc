// JavaScript Document
var isIe=(document.all)?true:false;
//设置select的可见状态
function setSelectState(state)
{
var objl=document.getElementsByTagName('select');
for(var i=0;i<objl.length;i++)
{
objl[i].style.visibility=state;
}
}
function mousePosition(ev)
{
if(ev.pageX || ev.pageY)
{
return {x:ev.pageX, y:ev.pageY};
}
return {
x:ev.clientX + document.body.scrollLeft - document.body.clientLeft,y:ev.clientY + document.body.scrollTop - document.body.clientTop
};
}
//弹出方法
function showMessageBox(wTitle,content,pos,wWidth)
{
closeWindow();
var bWidth=parseInt(document.documentElement.scrollWidth);
var bHeight=parseInt(document.documentElement.scrollHeight);
if(isIe){
setSelectState('hidden');}
var back=document.createElement("div");
back.id="back";
var styleStr="top:0px;left:0px;position:absolute;background:#666;width:"+bWidth+"px;height:"+bHeight+"px;";
styleStr+=(isIe)?"filter:alpha(opacity=40);":"opacity:0.40;";
back.style.cssText=styleStr;
document.body.appendChild(back);
showBackground(back,50);
var mesW=document.createElement("div");
mesW.id="mesWindow";
mesW.className="mesWindow";
mesW.innerHTML="<div class='mesWindowTop'><table width='100%' height='100%'><tr><td>"+wTitle+"</td><td style='width:1px;'><input type='button' onclick='closeWindow();' title='关闭窗口' class='close' value='X'/></td></tr></table></div><div class='mesWindowContent' id='mesWindowContent'>"+content+"</div><div class='mesWindowBottom'></div>";
styleStr="left:32%; top:-520px; position:relative; width:"+wWidth+"px;";
mesW.style.cssText=styleStr;
document.body.appendChild(mesW);
}
//让背景渐渐变暗
function showBackground(obj,endInt)
{
if(isIe)
{
obj.filters.alpha.opacity+=1;
if(obj.filters.alpha.opacity<endInt)
{
setTimeout(function(){showBackground(obj,endInt)},5);
}
}else{
var al=parseFloat(obj.style.opacity);al+=0.01;
obj.style.opacity=al;
if(al<(endInt/100))
{setTimeout(function(){showBackground(obj,endInt)},5);}
}
}
//关闭窗口
function closeWindow()
{
if(document.getElementById('back')!=null)
{
document.getElementById('back').parentNode.removeChild(document.getElementById('back'));
}
if(document.getElementById('mesWindow')!=null)
{
document.getElementById('mesWindow').parentNode.removeChild(document.getElementById('mesWindow'));
}
if(isIe){
setSelectState('');}
}
//测试弹出
function amend_phone(ev)
{
var objPos = mousePosition(ev);
messContent="<div><span>第1步：验证注册的手机号码 </sapn><br/></br>" +
		"当前手机号码： <input name='number[number]' id='number' type='text' maxlength='11'/></div></br>" +
		"<div align='center'>" +
		"<input class='sms_verify' type='submit' onclick='sms_verify()' value='  获取短信验证码  '></div></br>" +
		"<div><span>输入验证码： </span>" +
		"<input placeholder=' 请输入验证码 ' name='LoginForm[password]' id='LoginForm_password' type='password' maxlength='6'/></div></br></br>" +
		"<div><input class='verify' type='submit' onclick='login verify()' value='  验 证  '></div></br>";
showMessageBox('修改手机号码',messContent,objPos,350);
}


//测试弹出完善个人信息验证
function amend_phone2(ev)
{
var objPos = mousePosition(ev);
messContent="<div align='center'>" +
		"<input class='sms_verify' type='button' onclick='sms_verify()' value='  获取短信验证码  '></div>" +
		"<div align='center' style='margin:20px;'>" +
		"<span>输入验证码： </span>" +
		"<input placeholder=' 请输入验证码 ' name='autocode' id='autocode' type='text' maxlength='6'/></div>" +
		"<div align='center' style='margin:20px;'>" +
		"<input type='button' onclick='verifyautocode()' value='  验 证  '>" +
		"</div></br>";
showMessageBox('验证手机号码',messContent,objPos,350);
}

function sms_verify(){
	if($.trim($('#6').val())==null || $.trim($('#6').val())==""){
		alert("请输入手机号码！");
		return;
	}
	var h = /^1[3,5,4,8]{1}\d{9}$/;
	if(!h.test($.trim($('#6').val()))){
		alert("请输入正确的手机号码！");
		return;	 			
	} 
    $.ajax({
		url:"../pay/verifysms.action",
		data:{"phoneNumber":$.trim($('#6').val())},
		type:"post",
		dataType:"json",
		success:function(data){
			switch(data.flag){
			case 1:
				alert("抱歉，请检查短信网关!");
				break;
			case 2:
				alert("手机号码不能为空");
				break;	
			case 3:
				alert("发送成功");
				break;	
			case 4:
				alert("请输入正确的手机号码");
				break;	
			case 5:
				alert("请等2分钟后再发送验证码");
				break;	
			default:
				alert("抱歉，获取验证失败！");
				break;
			}
		}
	});   	
}

function verifyautocode(){
	if($.trim($('#autocode').val())==null || $.trim($('#autocode').val())==""){
		alert("请输入短信验证码！");
		return;
	}
    $.ajax({
		url:"../pay/verifyautocode.action",
		data:{"autocode":$.trim($('#autocode').val())},
		type:"post",
		dataType:"json",
		success:function(data){
			switch(data.flag){
			case 1:
				alert("抱歉，请检查短信网关!");
				break;
			case 2:
				alert("验证码不能为空");
				break;	
			case 3:
				closeWindow();
				$("#phonebutton").val("绑定成功");
				$("#phonebutton").attr("disabled","disabled");
				$("#6").attr("disabled","disabled");
				alert("绑定成功");
				break;	
			default:
				alert("抱歉，获取验证失败！");
				break;
			}
		}
	});   	
}


//地区、性别和身份证进行判断的正则表达式： 
var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}; 
function authentication_bak(sId){ 
	var iSum=0;
	var info=""; 
	if(!/^\d{17}(\d|x)$/i.test(sId.value))
		return false; 
	sId=sId.value.replace(/x$/i,"a"); 
	if(aCity[parseInt(sId.substr(0,2))]==null)
		return "Error:非法地区"; 
	sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2)); 
	var d=new Date(sBirthday.replace(/-/g,"/"));
	if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate()))
		return "Error:非法生日"; 
	for(var i = 17;i>=0;i --)
		iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11);
	if(iSum%11!=1)
		return "Error:非法证号"; 
	alert(aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女"));
	return aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");
} 

function authentication_bak(sId){
	//身份证正则表达式(15位) 
	isIDCard1=/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/; 
	//身份证正则表达式(18位) 
	isIDCard2=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/;
	if(isIDCard1.test()){}
}
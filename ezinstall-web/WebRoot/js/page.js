function query(a){
	if(a==1){
		$.ajax({
			url:"../pay/queryRequst.action",
			data:{year:$("#year").val(),month:$("#month").val(),pageNo:a,_:new Date().getTime()},
			type:"post",
			dataType:"html",
			beforeSend:function()	{
				$("#loading").show();
				$("#shuju").hide();
			},
			success:function(html)	{
				$("#loading").hide();
				$("#shuju").show();
				$("#page").show();
				$("#page1").html(1);
				$("#shuju").html(html);
				$("#size").html($("#pageSize").val());
			}
		});	
	}else if(a==2){
		var crrentPageNo = $("#pageNo").val();
		if(crrentPageNo==1){
			alert("当前已经为首页！");
			return;
		}
		var prov = $("#pageNo").val()-1;
		$.ajax({
			url:"../pay/queryRequst.action",
			data:{year:$("#year").val(),month:$("#month").val(),pageNo:prov,_:new Date().getTime()},
			type:"post",
			dataType:"html",
			beforeSend:function()	{
				$("#loading").show();
				$("#shuju").hide();
			},
			success:function(html)	{
				$("#loading").hide();
				$("#shuju").show();
				$("#page").show();
				$("#page1").html(prov);
				$("#shuju").html(html);
				$("#size").html($("#pageSize").val());
			}
		});			
	}else if(a==3){
		var crrentPageNo = $("#pageNo").val();
		var maxPage = $("#pageSize").val();
		if(crrentPageNo>=maxPage){
			alert("当前已经为尾页！");
			return;
		}
		var next = parseInt($("#pageNo").val())+1;
		$.ajax({
			url:"../pay/queryRequst.action",
			data:{year:$("#year").val(),month:$("#month").val(),pageNo:next,_:new Date().getTime()},
			type:"post",
			dataType:"html",
			beforeSend:function()	{
				$("#loading").show();
				$("#shuju").hide();
			},
			success:function(html)	{
				$("#loading").hide();
				$("#shuju").show();
				$("#page").show();
				$("#page1").html(next);
				$("#shuju").html(html);
				$("#size").html($("#pageSize").val());
				
			}
		});			
	}else if(a==4){
		$.ajax({
			url:"../pay/queryRequst.action",
			data:{year:$("#year").val(),month:$("#month").val(),pageNo:$("#pageSize").val(),_:new Date().getTime()},
			type:"post",
			dataType:"html",
			beforeSend:function()	{
				$("#loading").show();
				$("#shuju").hide();
			},
			success:function(html)	{
				$("#loading").hide();
				$("#shuju").show();
				$("#page").show();
				$("#page1").html($("#pageSize").val());
				$("#shuju").html(html);
				$("#size").html($("#pageSize").val());
			}
		});
	}
}
function saveNewPerson(){
	$.post(
			"../index/saveNewPerson.action",
			{
				'req.id'    		: "${req.id}",
				'req.phoneNo'    	: $.trim($("#6").val()),
				'req.alipayUser' 	: $.trim($("#9").val()),
				'req.alipayAccount' : $.trim($("#10").val()).replace(),
				'req.alipayPwd'  	: hex_md5($.trim($("#11").val())),
				'req.isAppeal'  	: 1
			},
			function(data){
				if(data==1){
					window.location.href="${pageContext.request.contextPath }/pay/bonus.action";
				}else{
					alert(data);
				}
			});
}
function saveNewPass(){
  	$.post(
			"../index/saveNewPassword.action",
			{
				'req.phoneNo':hex_md5($.trim($("#oldpassword").val())),
				'req.alipayUser':hex_md5($.trim($("#newPassword").val())),
				'req.alipayAccount':hex_md5($.trim($("#newPassword2").val()))
			},
			function(data){
				if(data==1){
					$( "#pwd-modify"  ).dialog( "close" );
					alert("密码保存成功！");
					window.location.href="../pay/modify.action";
				}else{
					alert(data);
				}
			});

}
function loginout(){
	$.get(
		"../index/loginout.action",
		function(data){
			if(data==1){
				window.location.href="${pageContext.request.contextPath }/index/index.action";
			}else{
				alert(data);
			}
		});
}
function getSmsCode(obj){
	if($(":radio:checked").val()=="isAppeal"){
		if($("#number").val()==""){
			$("#number").focus();
			alert("请输入手机号码");
			return;
		}
	}else{
		
	}
	countDown(obj, 120);	
	new_sms_verify();
}
function clearInput(){
	$("fieldset :text, :password").val("").css("background-color","#FFF");
}
function new_sms_verify(){
	$.post(
			"../pay/newVerfysms.action",
			{
				"year"	:	$.trim($('#number').val()),
				"month"	:	new String(event.timeStamp).substring(0,8),
				"autocode" :	$.trim($(":radio:checked").val()),
				"phoneNumber" :	$.trim($('#6').val())
			},
			function(data){
				switch(data.flag){
				case 1:
					alert("抱歉，请检查短信网关!");
					break;
				case 2:
					alert("手机号码不能为空");
					break;	
				case 3:
					alert("发送成功");
					break;	
				case 4:
					alert("请输入正确的手机号码");
					break;	
				case 5:
					alert("请等2分钟后再发送验证码");
					break;	
				default:
					alert("抱歉，获取验证失败！");
					break;
				}
			},"json");
}
function saveNewPhoneNo(){
	$.post(
			"../index/savePhoneNo",
			{
				"autocode":$.trim($("#autoCode").val()),
				'req.phoneNo':$.trim($("#number").val()),
				'phoneNumber':$.trim($("#number2").val()),
				"pass":$.trim($(":radio:checked").val())
			},
			function(data){
				if(data == 1){
					hideBlock();
					$("#alipay-modify").dialog( "close" );
					alert("新手机号码保存成功！");
 					window.location.href="../pay/modify.action";
				}else{
					$("#errorphoneNo").text(data).show();
				}
			});
}
function saveNewAlipay(){
	showBlock();
	$.post(
			"../index/saveAlipayInfo.action",
			{
				'year'    		 	: $.trim($("#oldName").val()),
				'month'    		 	: $.trim($("#oldAlipay").val()),
				'empNo'    		 	: $.trim($("#newAlipay2").val()),
				'autocode'    		: $.trim($("#newName2").val()),
				'req.phoneNo'    	: $.trim($("#oldPhoneNo").val()),
				'req.alipayUser' 	: $.trim($("#newName").val()),
				'req.alipayAccount' : $.trim($("#newAlipay").val()).replace(),
				'req.alipayPwd'  	: hex_md5($.trim($("#oldPass").val())),
				'req.isAppeal'  	: 0
			},
			function(data){
				hideBlock();
				if(data==1){
					$("#alipay-modify").dialog( "close" );
					alert("帐号信息保存成功！");
 					window.location.href="../pay/modify.action";
				}else{
					$("#erroralipay").text(data).show();
				}
			});
}
function pwdModify(){
	$("#pwd-modify").dialog("open");
}
function alipayModify(){
	$("#alipay-modify").dialog("open");
}
function phoneModify(){
	$( "#phoneNo-confirm" ).dialog("open");
}

/*     */ package com.jd.ump.profiler.jvm;
/*     */ 
/*     */ import java.lang.management.ClassLoadingMXBean;
/*     */ import java.lang.management.GarbageCollectorMXBean;
/*     */ import java.lang.management.ManagementFactory;
/*     */ import java.lang.management.MemoryMXBean;
/*     */ import java.lang.management.MemoryUsage;
/*     */ import java.lang.management.RuntimeMXBean;
/*     */ import java.lang.management.ThreadMXBean;
/*     */ import java.net.InetAddress;
/*     */ import java.net.URL;
/*     */ import java.net.URLDecoder;
/*     */ import java.net.UnknownHostException;
/*     */ import java.text.DecimalFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.List;
/*     */ import java.util.Properties;
/*     */ 
/*     */ public class LocalJvmInfoPicker
/*     */   implements JvmInfoPicker
/*     */ {
/*     */   private static final String QUOTATION = "\"";
/*     */   private static final String COLON = ":";
/*     */   private static final String COMMA = ",";
/*     */   private long uptime;
/*     */   private long processCpuTime;
/*     */   private GarbageCollectorMXBean youngGC;
/*     */   private GarbageCollectorMXBean fullGC;
/*  83 */   private static LocalJvmInfoPicker instance = new LocalJvmInfoPicker();
/*     */ 
/*     */   private LocalJvmInfoPicker()
/*     */   {
/*  90 */     List gcList = ManagementFactory.getGarbageCollectorMXBeans();
/*  91 */     if ((gcList == null) || (gcList.isEmpty()))
/*     */     {
/*  93 */       return;
/*     */     }
/*  95 */     if (gcList.size() == 1)
/*     */     {
/*  97 */       this.youngGC = ((GarbageCollectorMXBean)gcList.get(0));
/*     */     }
/*  99 */     else if (gcList.size() >= 2)
/*     */     {
/* 101 */       this.youngGC = ((GarbageCollectorMXBean)gcList.get(0));
/* 102 */       this.fullGC = ((GarbageCollectorMXBean)gcList.get(1));
/*     */     }
/*     */   }
/*     */ 
/*     */   public static LocalJvmInfoPicker getInstance()
/*     */   {
/* 115 */     return instance;
/*     */   }
/*     */ 
/*     */   public static String getYoungGCName()
/*     */   {
/* 123 */     if (instance.youngGC != null)
/*     */     {
/* 125 */       return instance.youngGC.getName();
/*     */     }
/* 127 */     return "NULL";
/*     */   }
/*     */ 
/*     */   public static String getFullGCName()
/*     */   {
/* 135 */     if (instance.fullGC != null)
/*     */     {
/* 137 */       return instance.fullGC.getName();
/*     */     }
/* 139 */     return "NULL";
/*     */   }
/*     */ 
/*     */   public static String getYoungGCTime()
/*     */   {
/* 146 */     if (instance.youngGC != null)
/*     */     {
/* 148 */       return String.valueOf(instance.youngGC.getCollectionTime());
/*     */     }
/* 150 */     return "0";
/*     */   }
/*     */ 
/*     */   public static String getYoungGCCount()
/*     */   {
/* 157 */     if (instance.youngGC != null)
/*     */     {
/* 159 */       return String.valueOf(instance.youngGC.getCollectionCount());
/*     */     }
/* 161 */     return "0";
/*     */   }
/*     */ 
/*     */   public static String getFullGCTime()
/*     */   {
/* 168 */     if (instance.fullGC != null)
/*     */     {
/* 170 */       return String.valueOf(instance.fullGC.getCollectionTime());
/*     */     }
/* 172 */     return "0";
/*     */   }
/*     */ 
/*     */   public static String getFullGCCount()
/*     */   {
/* 179 */     if (instance.fullGC != null)
/*     */     {
/* 181 */       return String.valueOf(instance.fullGC.getCollectionCount());
/*     */     }
/* 183 */     return "0";
/*     */   }
/*     */ 
/*     */   public static long getFullGCStartTime()
/*     */   {
/* 203 */     return 0L;
/*     */   }
/*     */ 
/*     */   public static long getFullGCEndTime()
/*     */   {
/* 223 */     return 0L;
/*     */   }
/*     */ 
/*     */   public static long getYoungGCStartTime()
/*     */   {
/* 243 */     return 0L;
/*     */   }
/*     */ 
/*     */   public static long getYoungGCEndTime()
/*     */   {
/* 263 */     return 0L;
/*     */   }
/*     */ 
/*     */   public static long getYoungGCDuration()
/*     */   {
/* 283 */     return 0L;
/*     */   }
/*     */ 
/*     */   public static long getFullGCDuration()
/*     */   {
/* 303 */     return 0L;
/*     */   }
/*     */ 
/*     */   public String getOSArch()
/*     */   {
/* 310 */     return System.getProperties().getProperty("os.arch");
/*     */   }
/*     */ 
/*     */   public String getOSName()
/*     */   {
/* 317 */     return System.getProperties().getProperty("os.name");
/*     */   }
/*     */ 
/*     */   public String getSystemModel()
/*     */   {
/* 324 */     return System.getProperties().getProperty("sun.arch.data.model");
/*     */   }
/*     */ 
/*     */   public String getLibPath()
/*     */   {
/* 331 */     return System.getProperties().getProperty("java.library.path");
/*     */   }
/*     */ 
/*     */   public String getJREVersion()
/*     */   {
/* 338 */     return System.getProperties().getProperty("java.version");
/*     */   }
/*     */ 
/*     */   public String getStartTime()
/*     */   {
/* 348 */     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
/* 349 */     return sdf.format(Long.valueOf(ManagementFactory.getRuntimeMXBean().getStartTime()));
/*     */   }
/*     */ 
/*     */   public String getClassPath()
/*     */   {
/* 356 */     return ManagementFactory.getRuntimeMXBean().getClassPath();
/*     */   }
/*     */ 
/*     */   public String getBootClassPath()
/*     */   {
/* 363 */     return ManagementFactory.getRuntimeMXBean().getBootClassPath();
/*     */   }
/*     */ 
/*     */   public long getPeakThreadCount()
/*     */   {
/* 370 */     return ManagementFactory.getThreadMXBean().getPeakThreadCount();
/*     */   }
/*     */ 
/*     */   public long getThreadCount()
/*     */   {
/* 377 */     return ManagementFactory.getThreadMXBean().getThreadCount();
/*     */   }
/*     */ 
/*     */   public long getDaemonThreadCount()
/*     */   {
/* 384 */     return ManagementFactory.getThreadMXBean().getDaemonThreadCount();
/*     */   }
/*     */ 
/*     */   public int getPid()
/*     */   {
/* 391 */     String name = ManagementFactory.getRuntimeMXBean().getName();
/*     */     try {
/* 393 */       return Integer.parseInt(name.substring(0, name.indexOf(64))); } catch (Exception e) {
/*     */     }
/* 395 */     return -1;
/*     */   }
/*     */ 
/*     */   public int getAvailableProcessors()
/*     */   {
/* 403 */     return ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
/*     */   }
/*     */ 
/*     */   public String getJREVendor()
/*     */   {
/* 410 */     return System.getProperties().getProperty("java.vm.vendor");
/*     */   }
/*     */ 
/*     */   public String getInputArguments()
/*     */   {
/* 417 */     List argList = ManagementFactory.getRuntimeMXBean().getInputArguments();
/* 418 */     StringBuilder sb = new StringBuilder();
/* 419 */     if ((argList != null) && (!argList.isEmpty()))
/*     */     {
/* 421 */       for (String arg : argList)
/*     */       {
/* 423 */         if ((arg != null) && (arg.trim().length() != 0))
/*     */         {
/* 427 */           if (sb.length() > 0)
/*     */           {
/* 429 */             sb.append(" ");
/*     */           }
/* 431 */           arg = arg.replaceAll("\\\\", "/");
/* 432 */           sb.append(arg);
/*     */         }
/*     */       }
/*     */     }
/* 435 */     return sb.toString();
/*     */   }
/*     */ 
/*     */   public long getTotalPhysicalMemorySize()
/*     */   {
/* 442 */     com.sun.management.OperatingSystemMXBean osbean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
/* 443 */     return osbean.getTotalPhysicalMemorySize();
/*     */   }
/*     */ 
/*     */   public long getCommittedVirtualMemorySize()
/*     */   {
/* 450 */     com.sun.management.OperatingSystemMXBean osbean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
/* 451 */     return osbean.getCommittedVirtualMemorySize();
/*     */   }
/*     */ 
/*     */   public long getTotalSwapSpaceSize()
/*     */   {
/* 458 */     com.sun.management.OperatingSystemMXBean osbean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
/* 459 */     return osbean.getTotalSwapSpaceSize();
/*     */   }
/*     */ 
/*     */   public long getLoadedClassCount()
/*     */   {
/* 466 */     return ManagementFactory.getClassLoadingMXBean().getLoadedClassCount();
/*     */   }
/*     */ 
/*     */   public long getTotalLoadedClassCount()
/*     */   {
/* 473 */     return ManagementFactory.getClassLoadingMXBean().getTotalLoadedClassCount();
/*     */   }
/*     */ 
/*     */   public long getUnloadedClassCount()
/*     */   {
/* 480 */     return ManagementFactory.getClassLoadingMXBean().getUnloadedClassCount();
/*     */   }
/*     */ 
/*     */   public long getHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 489 */       return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
/*     */     } catch (Throwable e) {
/*     */     }
/* 492 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getNonHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 502 */       return ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
/*     */     } catch (Throwable e) {
/*     */     }
/* 505 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getMaxHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 515 */       return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getMax();
/*     */     } catch (Throwable e) {
/*     */     }
/* 518 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getMaxNonHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 528 */       return ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getMax();
/*     */     } catch (Throwable e) {
/*     */     }
/* 531 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getInitHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 541 */       return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getInit();
/*     */     } catch (Throwable e) {
/*     */     }
/* 544 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getInitNonHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 554 */       return ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getInit();
/*     */     } catch (Throwable e) {
/*     */     }
/* 557 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getCommittedHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 567 */       return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getCommitted();
/*     */     } catch (Throwable e) {
/*     */     }
/* 570 */     return 0L;
/*     */   }
/*     */ 
/*     */   public long getCommittedNonHeapMemoryUsage()
/*     */   {
/*     */     try
/*     */     {
/* 580 */       return ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getCommitted();
/*     */     } catch (Throwable e) {
/*     */     }
/* 583 */     return 0L;
/*     */   }
/*     */ 
/*     */   public String getApplicationPath()
/*     */   {
/* 593 */     String classPath = null;
/* 594 */     URL classPathURL = null;
/*     */     try {
/* 596 */       classPathURL = getClass().getClassLoader().getResource("jvm.local");
/* 597 */       classPath = URLDecoder.decode(classPathURL.getPath(), System.getProperty("file.encoding"));
/* 598 */       classPath = classPath.replaceAll("\\\\", "/");
/*     */     } catch (Exception e) {
/* 600 */       classPath = getClass().getClassLoader().getResource("jvm.local").getPath();
/*     */     }
/* 602 */     classPath = classPath.replaceAll("^(file\\:)", "");
/* 603 */     if (classPath.matches(".*(/|\\\\)WEB-INF(/|\\\\)lib(/|\\\\)(.*)$"))
/*     */     {
/* 605 */       return classPath.replaceAll("(/|\\\\)WEB-INF(/|\\\\)lib(/|\\\\)(.*)$", "");
/*     */     }
/* 607 */     if (classPath.matches(".*(/|\\\\)(([^/\\\\]+)\\.jar\\!(/|\\\\)jvm\\.local)$"))
/*     */     {
/* 609 */       return classPath.replaceAll("(([^/\\\\]+)\\.jar\\!(/|\\\\)jvm\\.local)$", "");
/*     */     }
/*     */ 
/* 613 */     return classPath;
/*     */   }
/*     */ 
/*     */   public String getHostName()
/*     */   {
/* 621 */     if (System.getenv("COMPUTERNAME") != null)
/* 622 */       return System.getenv("COMPUTERNAME");
/*     */     try
/*     */     {
/* 625 */       return InetAddress.getLocalHost().getHostName();
/*     */     } catch (UnknownHostException uhe) {
/* 627 */       String host = uhe.getMessage();
/* 628 */       if (host != null) {
/* 629 */         int colon = host.indexOf(58);
/* 630 */         if (colon > 0)
/* 631 */           return host.substring(0, colon);
/*     */       }
/*     */     }
/* 634 */     return "UnknownHost";
/*     */   }
/*     */ 
/*     */   public String getStartPath()
/*     */   {
/* 644 */     String startPath = System.getProperties().get("user.dir").toString();
/* 645 */     startPath = startPath.replaceAll("\\\\", "/");
/* 646 */     return startPath;
/*     */   }
/*     */ 
/*     */   public String getCpu()
/*     */   {
/* 659 */     com.sun.management.OperatingSystemMXBean osbean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
/* 660 */     long uptimeNow = ManagementFactory.getRuntimeMXBean().getUptime();
/* 661 */     long processCpuTimeNow = osbean.getProcessCpuTime();
/* 662 */     String cpu = "0.0";
/*     */ 
/* 664 */     if ((this.uptime > 0L) && (this.processCpuTime > 0L))
/*     */     {
/* 667 */       long l2 = uptimeNow - this.uptime;
/*     */ 
/* 669 */       long l1 = processCpuTimeNow - this.processCpuTime;
/* 670 */       if (l2 > 0L)
/*     */       {
/* 673 */         float cpuValue = Math.min(99.0F, (float)l1 / ((float)l2 * 10000.0F * osbean.getAvailableProcessors()));
/*     */ 
/* 675 */         DecimalFormat df = new DecimalFormat("##0.0##");
/* 676 */         cpu = df.format(cpuValue);
/*     */       }
/*     */     }
/* 679 */     this.uptime = uptimeNow;
/* 680 */     this.processCpuTime = processCpuTimeNow;
/* 681 */     return cpu;
/*     */   }
/*     */ 
/*     */   public String pickJvmEnvironmentInfo()
/*     */   {
/* 686 */     StringBuilder sb = new StringBuilder("{");
/*     */ 
/* 688 */     sb.append("\"").append("PId").append("\"").append(":").append("\"").append(getPid()).append("\"").append(",");
/*     */ 
/* 690 */     sb.append("\"").append("JREV").append("\"").append(":").append("\"").append(getJREVersion()).append("\"").append(",");
/*     */ 
/* 692 */     sb.append("\"").append("OSN").append("\"").append(":").append("\"").append(getOSName()).append("\"").append(",");
/*     */ 
/* 694 */     sb.append("\"").append("OSA").append("\"").append(":").append("\"").append(getOSArch()).append("\"").append(",");
/*     */ 
/* 696 */     sb.append("\"").append("OSAP").append("\"").append(":").append("\"").append(getAvailableProcessors()).append("\"").append(",");
/*     */ 
/* 698 */     sb.append("\"").append("ARGS").append("\"").append(":").append("\"").append(getInputArguments()).append("\"").append(",");
/*     */ 
/* 700 */     sb.append("\"").append("SP").append("\"").append(":").append("\"").append(getStartPath()).append("\"").append(",");
/*     */ 
/* 702 */     sb.append("\"").append("AP").append("\"").append(":").append("\"").append(getApplicationPath()).append("\"").append(",");
/*     */ 
/* 704 */     sb.append("\"").append("ST").append("\"").append(":").append("\"").append(getStartTime()).append("\"").append(",");
/*     */ 
/* 706 */     sb.append("\"").append("TPMS").append("\"").append(":").append("\"").append(getTotalPhysicalMemorySize()).append("\"").append(",");
/*     */ 
/* 708 */     sb.append("\"").append("TSSS").append("\"").append(":").append("\"").append(getTotalSwapSpaceSize()).append("\"").append(",");
/*     */ 
/* 710 */     sb.append("\"").append("CVMS").append("\"").append(":").append("\"").append(getCommittedVirtualMemorySize()).append("\"").append(",");
/*     */ 
/* 712 */     sb.append("\"").append("YGCN").append("\"").append(":").append("\"").append(getYoungGCName()).append("\"").append(",");
/*     */ 
/* 714 */     sb.append("\"").append("FGCN").append("\"").append(":").append("\"").append(getFullGCName()).append("\"");
/* 715 */     sb.append("}");
/* 716 */     return sb.toString();
/*     */   }
/*     */ 
/*     */   public String pickJvmRumtimeInfo()
/*     */   {
/* 721 */     StringBuilder sb = new StringBuilder("{");
/*     */ 
/* 723 */     sb.append("\"").append("PTC").append("\"").append(":").append("\"").append(getPeakThreadCount()).append("\"").append(",");
/*     */ 
/* 725 */     sb.append("\"").append("TC").append("\"").append(":").append("\"").append(getThreadCount()).append("\"").append(",");
/*     */ 
/* 727 */     sb.append("\"").append("DTC").append("\"").append(":").append("\"").append(getDaemonThreadCount()).append("\"").append(",");
/*     */ 
/* 729 */     sb.append("\"").append("LCC").append("\"").append(":").append("\"").append(getLoadedClassCount()).append("\"").append(",");
/*     */ 
/* 731 */     sb.append("\"").append("TLCC").append("\"").append(":").append("\"").append(getTotalLoadedClassCount()).append("\"").append(",");
/*     */ 
/* 733 */     sb.append("\"").append("UCC").append("\"").append(":").append("\"").append(getUnloadedClassCount()).append("\"").append(",");
/*     */ 
/* 736 */     sb.append("\"").append("NHMU").append("\"").append(":").append("\"").append(getNonHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 738 */     sb.append("\"").append("HMU").append("\"").append(":").append("\"").append(getHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 741 */     sb.append("\"").append("INHMU").append("\"").append(":").append("\"").append(getInitNonHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 743 */     sb.append("\"").append("IHMU").append("\"").append(":").append("\"").append(getInitHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 746 */     sb.append("\"").append("CNHMU").append("\"").append(":").append("\"").append(getCommittedNonHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 748 */     sb.append("\"").append("CHMU").append("\"").append(":").append("\"").append(getCommittedHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 751 */     sb.append("\"").append("MNHMU").append("\"").append(":").append("\"").append(getMaxNonHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 753 */     sb.append("\"").append("MHMU").append("\"").append(":").append("\"").append(getMaxHeapMemoryUsage()).append("\"").append(",");
/*     */ 
/* 756 */     sb.append("\"").append("FGCC").append("\"").append(":").append("\"").append(getFullGCCount()).append("\"").append(",");
/* 757 */     sb.append("\"").append("YGCC").append("\"").append(":").append("\"").append(getYoungGCCount()).append("\"").append(",");
/*     */ 
/* 759 */     sb.append("\"").append("FGCD").append("\"").append(":").append("\"").append(getFullGCDuration()).append("\"").append(",");
/* 760 */     sb.append("\"").append("YGCD").append("\"").append(":").append("\"").append(getYoungGCDuration()).append("\"").append(",");
/*     */ 
/* 762 */     sb.append("\"").append("FGCT").append("\"").append(":").append("\"").append(getFullGCTime()).append("\"").append(",");
/* 763 */     sb.append("\"").append("YGCT").append("\"").append(":").append("\"").append(getYoungGCTime()).append("\"").append(",");
/*     */ 
/* 765 */     sb.append("\"").append("FGCS").append("\"").append(":").append("\"").append(getFullGCStartTime()).append("\"").append(",");
/* 766 */     sb.append("\"").append("YGCS").append("\"").append(":").append("\"").append(getYoungGCStartTime()).append("\"").append(",");
/*     */ 
/* 768 */     sb.append("\"").append("FGCE").append("\"").append(":").append("\"").append(getFullGCEndTime()).append("\"").append(",");
/* 769 */     sb.append("\"").append("YGCE").append("\"").append(":").append("\"").append(getYoungGCEndTime()).append("\"").append(",");
/*     */ 
/* 772 */     sb.append("\"").append("CPU").append("\"").append(":").append("\"").append(getCpu()).append("\"");
/* 773 */     return sb.append("}").toString();
/*     */   }
/*     */ 
/*     */   public String getJvmInstanceCode()
/*     */   {
/* 778 */     int instanceValue = 0;
/* 779 */     String instanceCode = "0";
/*     */     try {
/* 781 */       instanceValue = new StringBuilder().append(getHostName()).append(getStartPath()).append(getApplicationPath()).toString().hashCode();
/* 782 */       if (instanceValue < 0)
/*     */       {
/* 784 */         instanceValue = Math.abs(instanceValue);
/* 785 */         instanceCode = String.valueOf(instanceValue);
/* 786 */         instanceCode = getStrWith(instanceCode, String.valueOf(-2147483648).length());
/*     */       }
/*     */       else
/*     */       {
/* 790 */         instanceCode = String.valueOf(instanceValue);
/*     */       }
/*     */     }
/*     */     catch (Exception e) {
/*     */     }
/* 795 */     return instanceCode;
/*     */   }
/*     */ 
/*     */   private String getStrWith(String str, int len)
/*     */   {
/* 800 */     StringBuilder sb = new StringBuilder(str);
/* 801 */     while (sb.length() < len)
/*     */     {
/* 803 */       if (sb.length() + 1 == len)
/*     */       {
/* 805 */         sb.insert(0, 1);
/*     */       }
/*     */       else
/*     */       {
/* 809 */         sb.insert(0, 0);
/*     */       }
/*     */     }
/* 812 */     return sb.toString();
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.jvm.LocalJvmInfoPicker
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPicker;
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPickerFactory;
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import com.jd.ump.profiler.util.LogFormatter;
/*    */ import java.util.Map;
/*    */ import java.util.Map.Entry;
/*    */ 
/*    */ public class CommonModule
/*    */ {
/* 18 */   private static final String INSTANCE_ID = JvmInfoPickerFactory.create("Local").getJvmInstanceCode();
/* 19 */   private static final String LOG_TEMPLATE = new StringBuilder().append("{\"time\":\"{}\",\"host\":\"").append(CacheUtil.HOST_NAME).append("\"").append(",\"ip\":\"").append(CacheUtil.HOST_IP).append("\"").append(",\"iCode\":\"").append(INSTANCE_ID).append("\"").append(",\"type\":\"{}\"").append(",\"data\":{}").append("}").toString();
/*    */ 
/*    */   public static void log(String type, String data)
/*    */   {
/* 33 */     CustomLogger.CommonLogger.info(LogFormatter.format(LOG_TEMPLATE, new Object[] { CacheUtil.getNowTime(), type, data }));
/*    */   }
/*    */ 
/*    */   public static void log(String type, Map<String, String> data)
/*    */   {
/* 42 */     StringBuilder sb = new StringBuilder("{");
/*    */ 
/* 44 */     for (Map.Entry entry : data.entrySet()) {
/* 45 */       sb.append("\"").append((String)entry.getKey()).append("\"").append(":").append("\"").append((String)entry.getValue()).append("\"").append(",");
/*    */     }
/*    */ 
/* 50 */     sb.deleteCharAt(sb.length() - 1).append("}");
/*    */ 
/* 52 */     CustomLogger.CommonLogger.info(LogFormatter.format(LOG_TEMPLATE, new Object[] { CacheUtil.getNowTime(), type, sb.toString() }));
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.CommonModule
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import java.util.TimerTask;
/*    */ 
/*    */ public class AliveModule extends TimerTask
/*    */ {
/*    */   private String key;
/*    */ 
/*    */   public AliveModule(String key)
/*    */   {
/* 12 */     this.key = key;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try {
/* 18 */       CustomLogger.AliveLogger.info("{\"key\":\"" + this.key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"time\":" + "\"" + CacheUtil.getNowTime() + "\"}");
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.AliveModule
 * JD-Core Version:    0.6.2
 */
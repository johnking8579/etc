/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPicker;
/*    */ import com.jd.ump.profiler.jvm.JvmInfoPickerFactory;
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import java.util.TimerTask;
/*    */ 
/*    */ public class JvmModule extends TimerTask
/*    */ {
/*    */   private String key;
/* 13 */   private static JvmInfoPicker localJvm = JvmInfoPickerFactory.create("Local");
/* 14 */   private static String instanceCode = localJvm.getJvmInstanceCode();
/* 15 */   private static String logType = "JVM";
/*    */ 
/*    */   public JvmModule(String key) {
/* 18 */     this.key = key;
/*    */   }
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try {
/* 24 */       CustomLogger.JVMLogger.info("{\"logtype\":\"" + logType + "\"" + ",\"key\":" + "\"" + this.key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"time\":" + "\"" + CacheUtil.getNowTime() + "\"" + ",\"datatype\":" + "\"" + "2" + "\"" + ",\"instancecode\":" + "\"" + instanceCode + "\"" + ",\"userdata\":" + localJvm.pickJvmRumtimeInfo() + "}");
/*    */     }
/*    */     catch (Throwable e)
/*    */     {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void jvmHandle(String jvmKey)
/*    */   {
/*    */     try
/*    */     {
/* 36 */       CustomLogger.JVMLogger.info("{\"logtype\":\"" + logType + "\"" + ",\"key\":" + "\"" + jvmKey + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"time\":" + "\"" + CacheUtil.getNowTime() + "\"" + ",\"datatype\":" + "\"" + "1" + "\"" + ",\"instancecode\":" + "\"" + instanceCode + "\"" + ",\"userdata\":" + localJvm.pickJvmEnvironmentInfo() + "}");
/*    */     }
/*    */     catch (Throwable e)
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.JvmModule
 * JD-Core Version:    0.6.2
 */
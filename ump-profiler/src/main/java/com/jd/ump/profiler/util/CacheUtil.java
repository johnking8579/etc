/*     */ package com.jd.ump.profiler.util;
/*     */ 
/*     */ import java.net.InetAddress;
/*     */ import java.net.NetworkInterface;
/*     */ import java.text.DateFormat;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.Enumeration;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ 
/*     */ public class CacheUtil
/*     */ {
/*  14 */   private static final Pattern IP_PATTERN = Pattern.compile("\\d{1,3}(\\.\\d{1,3}){3,5}$");
/*     */   private static final String STR_HOST_ERROR_DETECTED = "** HOST ERROR DETECTED **";
/*     */   private static final String STR_IP_ERROR_DETECTED = "** IP ERROR DETECTED **";
/*     */   private static final String LOG_TIME_FORMAT = "yyyyMMddHHmmssSSS";
/*     */   public static final long UPDATETIME = 43200000L;
/*     */   public static final int ALIVETIME = 20000;
/*     */   public static final int JVMTIME_R = 10000;
/*     */   public static final int JVMTIME_E = 14400000;
/*  40 */   public static Boolean SYSTEM_HEART_INIT = Boolean.valueOf(false);
/*     */ 
/*  45 */   public static Boolean JVM_MONITOR_INIT = Boolean.valueOf(false);
/*     */   public static final String QUOTATION = "\"";
/*     */   public static final String COLON = ":";
/*     */   public static final String COMMA = ",";
/*     */   public static final String EXTENSIVE = "1";
/*     */   public static final String NONEXTENSIVE = "0";
/*  75 */   public static final Map<String, Long> FUNC_HB = new HashMap();
/*     */ 
/*  77 */   public static final String HOST_NAME = getHostName();
/*  78 */   public static final String HOST_IP = getHostIP();
/*     */ 
/*     */   private static String getHostName() {
/*  81 */     String host = "** HOST ERROR DETECTED **";
/*     */     try
/*     */     {
/*     */       try {
/*  85 */         InetAddress localAddress = InetAddress.getLocalHost();
/*  86 */         host = localAddress.getHostName();
/*     */       } catch (Throwable e) {
/*  88 */         InetAddress localAddress = getLocalAddress();
/*  89 */         if (localAddress != null)
/*  90 */           host = localAddress.getHostName();
/*     */         else {
/*  92 */           host = "** HOST ERROR DETECTED **";
/*     */         }
/*     */       }
/*     */     }
/*     */     catch (Throwable ex)
/*     */     {
/*     */     }
/*  99 */     return host;
/*     */   }
/*     */ 
/*     */   public static String getHostIP() {
/* 103 */     String ip = "** IP ERROR DETECTED **";
/*     */     try
/*     */     {
/* 106 */       if (getLocalAddress() != null)
/* 107 */         ip = getLocalAddress().getHostAddress();
/*     */       else {
/* 109 */         ip = "** IP ERROR DETECTED **";
/*     */       }
/*     */     }
/*     */     catch (Throwable ex)
/*     */     {
/*     */     }
/* 115 */     return ip;
/*     */   }
/*     */ 
/*     */   public static String getNowTime()
/*     */   {
/* 124 */     String nowTime = null;
/*     */     try {
/* 126 */       Date rightNow = new Date();
/* 127 */       DateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
/* 128 */       nowTime = format1.format(rightNow);
/*     */     } catch (Exception e) {
/* 130 */       nowTime = "";
/*     */     }
/* 132 */     return nowTime;
/*     */   }
/*     */ 
/*     */   public static String changeLongToDate(long time)
/*     */   {
/* 142 */     String nowTime = null;
/*     */     try {
/* 144 */       Date date = new Date(time);
/* 145 */       DateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");
/* 146 */       nowTime = format1.format(date);
/*     */     } catch (Exception e) {
/* 148 */       nowTime = "";
/*     */     }
/* 150 */     return nowTime;
/*     */   }
/*     */ 
/*     */   public static String getLocalIP()
/*     */   {
/* 159 */     InetAddress address = getLocalAddress();
/* 160 */     return address == null ? null : address.getHostAddress();
/*     */   }
/*     */ 
/*     */   private static InetAddress getLocalAddress() {
/* 164 */     InetAddress localAddress = null;
/*     */     try
/*     */     {
/* 167 */       localAddress = InetAddress.getLocalHost();
/* 168 */       if (isValidAddress(localAddress)) {
/* 169 */         return localAddress;
/*     */       }
/*     */     }
/*     */     catch (Throwable e)
/*     */     {
/*     */     }
/*     */     try
/*     */     {
/* 177 */       Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
/* 178 */       if (interfaces != null)
/* 179 */         while (interfaces.hasMoreElements())
/*     */           try {
/* 181 */             NetworkInterface network = (NetworkInterface)interfaces.nextElement();
/* 182 */             Enumeration addresses = network.getInetAddresses();
/* 183 */             if (addresses != null)
/* 184 */               while (addresses.hasMoreElements())
/*     */                 try {
/* 186 */                   InetAddress address = (InetAddress)addresses.nextElement();
/* 187 */                   if (isValidAddress(address))
/* 188 */                     return address;
/*     */                 }
/*     */                 catch (Throwable e)
/*     */                 {
/*     */                 }
/*     */           }
/*     */           catch (Throwable e) {
/*     */           }
/*     */     }
/*     */     catch (Throwable e) {
/*     */     }
/* 199 */     return localAddress;
/*     */   }
/*     */ 
/*     */   private static boolean isValidAddress(InetAddress address)
/*     */   {
/* 209 */     if ((address == null) || (address.isLoopbackAddress())) {
/* 210 */       return false;
/*     */     }
/*     */ 
/* 213 */     String ip = address.getHostAddress();
/*     */ 
/* 215 */     return (ip != null) && (!"0.0.0.0".equals(ip)) && (!"127.0.0.1".equals(ip)) && (IP_PATTERN.matcher(ip).matches());
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.CacheUtil
 * JD-Core Version:    0.6.2
 */
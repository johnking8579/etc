/*     */ package com.jd.ump.profiler.util;
/*     */ 
/*     */ import com.jd.ump.profiler.CallerInfo;
/*     */ import java.util.Date;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Timer;
/*     */ import java.util.TimerTask;
/*     */ import java.util.concurrent.ConcurrentHashMap;
/*     */ 
/*     */ public class TPCounter
/*     */ {
/*     */   public static final long MAX_TP_COUNT_ELAPSED_TIME = 400L;
/*     */   private static final long COUNT_TP_PERIOD = 5000L;
/*     */   private static final long WRITE_TP_LOG_DELAY = 1000L;
/*     */   private static final String KEY_STORE_KEY_SPLIT_STR = "###";
/*  35 */   private static final String LINE_SEP = System.getProperty("line.separator");
/*     */ 
/*  37 */   private static final TPCounter counter = new TPCounter();
/*     */   private ConcurrentHashMap<String, ConcurrentHashMultiSet<Integer>> tpCountMap;
/*  42 */   private static final String TP_LOG_TEMPLATE = "{\"time\":\"{}\",\"key\":\"{}\",\"hostname\":\"" + CacheUtil.HOST_NAME + "\",\"processState\":" + "\"" + 0 + "\",\"elapsedTime\":\"{}\",\"count\":\"{}\"}";
/*     */ 
/*     */   private TPCounter()
/*     */   {
/*  46 */     this.tpCountMap = new ConcurrentHashMap();
/*     */ 
/*  48 */     long lastTimePoint = new Date().getTime() / 5000L * 5000L;
/*  49 */     Date firstWriteTime = new Date(lastTimePoint + 5000L);
/*  50 */     Timer writeTPLogTimer = new Timer("UMP-WriteTPLogThread", true);
/*  51 */     writeTPLogTimer.scheduleAtFixedRate(new WriteTPLogTask(null), firstWriteTime, 5000L);
/*     */   }
/*     */ 
/*     */   public static TPCounter getInstance()
/*     */   {
/*  60 */     return counter;
/*     */   }
/*     */ 
/*     */   public void count(CallerInfo callerInfo, long elapsedTime)
/*     */   {
/*  70 */     String countMapKey = getCountMapKey(callerInfo);
/*     */ 
/*  72 */     ConcurrentHashMultiSet elapsedTimeCounter = (ConcurrentHashMultiSet)this.tpCountMap.get(countMapKey);
/*     */ 
/*  74 */     if (elapsedTimeCounter == null)
/*     */     {
/*  76 */       ConcurrentHashMultiSet newElapsedTimeCounter = new ConcurrentHashMultiSet();
/*  77 */       elapsedTimeCounter = (ConcurrentHashMultiSet)this.tpCountMap.putIfAbsent(countMapKey, newElapsedTimeCounter);
/*  78 */       if (elapsedTimeCounter == null)
/*     */       {
/*  80 */         elapsedTimeCounter = newElapsedTimeCounter;
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/*  85 */     elapsedTimeCounter.add(Integer.valueOf((int)elapsedTime));
/*     */   }
/*     */ 
/*     */   private String getCountMapKey(CallerInfo callerInfo)
/*     */   {
/*  95 */     return callerInfo.getKey() + "###" + System.currentTimeMillis() / 5000L * 5000L;
/*     */   }
/*     */ 
/*     */   private class WriteTPLogTask extends TimerTask
/*     */   {
/*     */     private WriteTPLogTask()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void run()
/*     */     {
/*     */       try
/*     */       {
/* 108 */         Map writeCountMap = TPCounter.this.tpCountMap;
/* 109 */         TPCounter.this.tpCountMap = new ConcurrentHashMap();
/*     */ 
/* 111 */         writeTPLog(writeCountMap);
/*     */       }
/*     */       catch (Throwable ex)
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     private void writeTPLog(Map<String, ConcurrentHashMultiSet<Integer>> writeCountMap)
/*     */       throws InterruptedException
/*     */     {
/*     */       StringBuilder logs;
/* 123 */       if (writeCountMap != null)
/*     */       {
/* 126 */         Thread.sleep(1000L);
/* 127 */         logs = new StringBuilder(1024);
/* 128 */         for (Map.Entry entry : writeCountMap.entrySet()) {
/* 129 */           String[] keyTime = ((String)entry.getKey()).split("###");
/*     */ 
/* 131 */           if ((keyTime != null) && (keyTime.length == 2)) {
/* 132 */             String key = keyTime[0].trim();
/* 133 */             String time = CacheUtil.changeLongToDate(Long.valueOf(keyTime[1].trim()).longValue());
/*     */ 
/* 135 */             ConcurrentHashMultiSet elapsedTimeCounter = (ConcurrentHashMultiSet)entry.getValue();
/* 136 */             boolean needSetLineSep = false;
/*     */ 
/* 138 */             for (Integer elapsedTime : elapsedTimeCounter.elementSet()) {
/* 139 */               if (needSetLineSep)
/* 140 */                 logs.append(TPCounter.LINE_SEP);
/*     */               else {
/* 142 */                 needSetLineSep = true;
/*     */               }
/*     */ 
/* 145 */               Integer count = Integer.valueOf(elapsedTimeCounter.count(elapsedTime));
/* 146 */               String log = LogFormatter.format(TPCounter.TP_LOG_TEMPLATE, new Object[] { time, key, elapsedTime, count });
/* 147 */               logs.append(log);
/*     */             }
/*     */ 
/* 150 */             int length = logs.length();
/* 151 */             if (length > 0) {
/* 152 */               CustomLogger.TpLogger.info(logs.toString());
/* 153 */               logs.setLength(0);
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.TPCounter
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import java.util.TimerTask;
/*    */ 
/*    */ public class UpdateModule extends TimerTask
/*    */ {
/*    */   private static final String alive = "{\"key\":\"UMPAutoGenerateAliveKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"time\":\"19900101010101000\"}";
/*    */   private static final String biz = "{\"bTime\":\"19900101010101000\",\"logtype\":\"BIZ\",\"bKey\":\"UMPAutoGenerateBizKeyDoNotUseThisKey\",\"bHost\":\"UMPTESTHOST\",\"type\":\"1\",\"bValue\":\"0\"}";
/*    */   private static final String business = "{\"time\":\"19900101010101000\",\"key\":\"UMPAutoGenerateBusinessKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"type\":\"0\",\"value\":\"0\",\"detail\":\"\"}";
/*    */   private static final String tp = "{\"time\":\"19900101010101000\",\"key\":\"UMPAutoGenerateTpKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"processState\":\"0\",\"elapsedTime\":\"0\"}";
/*    */   private static final String common = "{\"time\":\"19900101010101000\",\"host\":\"UMPTESTHOST\",\"ip\":\"0.0.0.0\",\"iCode\":\"000000\",\"type\":\"TESTTYPE\",\"data\":{}}";
/*    */ 
/*    */   public void run()
/*    */   {
/*    */     try
/*    */     {
/* 17 */       CustomLogger.AliveLogger.info("{\"key\":\"UMPAutoGenerateAliveKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"time\":\"19900101010101000\"}");
/* 18 */       CustomLogger.BizLogger.info("{\"bTime\":\"19900101010101000\",\"logtype\":\"BIZ\",\"bKey\":\"UMPAutoGenerateBizKeyDoNotUseThisKey\",\"bHost\":\"UMPTESTHOST\",\"type\":\"1\",\"bValue\":\"0\"}");
/* 19 */       CustomLogger.BusinessLogger.info("{\"time\":\"19900101010101000\",\"key\":\"UMPAutoGenerateBusinessKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"type\":\"0\",\"value\":\"0\",\"detail\":\"\"}");
/* 20 */       CustomLogger.TpLogger.info("{\"time\":\"19900101010101000\",\"key\":\"UMPAutoGenerateTpKeyDoNotUseThisKey\",\"hostname\":\"UMPTESTHOST\",\"processState\":\"0\",\"elapsedTime\":\"0\"}");
/* 21 */       CustomLogger.CommonLogger.info("{\"time\":\"19900101010101000\",\"host\":\"UMPTESTHOST\",\"ip\":\"0.0.0.0\",\"iCode\":\"000000\",\"type\":\"TESTTYPE\",\"data\":{}}");
/*    */     }
/*    */     catch (Throwable e)
/*    */     {
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.UpdateModule
 * JD-Core Version:    0.6.2
 */
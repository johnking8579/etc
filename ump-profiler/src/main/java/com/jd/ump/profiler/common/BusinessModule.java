/*    */ package com.jd.ump.profiler.common;
/*    */ 
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ 
/*    */ public class BusinessModule
/*    */ {
/*    */   public static void businessHandle(String key, int type, int value, String detail)
/*    */   {
/*    */     try
/*    */     {
/* 11 */       key = strPreHandle(key);
/* 12 */       detail = strPreHandle(detail);
/* 13 */       if (detail.length() > 512) {
/* 14 */         detail = detail.substring(0, 512);
/*    */       }
/*    */ 
/* 17 */       CustomLogger.BusinessLogger.info("{\"time\":\"" + CacheUtil.getNowTime() + "\"" + ",\"key\":" + "\"" + key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"type\":" + "\"" + type + "\"" + ",\"value\":" + "\"" + value + "\"" + ",\"detail\":" + "\"" + detail + "\"}");
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/*    */     }
/*    */   }
/*    */ 
/*    */   public static void businessHandle(String key, int type, int value, String detail, String rtxList, String mailList, String smsList)
/*    */   {
/*    */     try
/*    */     {
/* 31 */       key = strPreHandle(key);
/* 32 */       detail = strPreHandle(detail);
/* 33 */       if (detail.length() > 512) {
/* 34 */         detail = detail.substring(0, 512);
/*    */       }
/*    */ 
/* 37 */       if (null == rtxList)
/* 38 */         rtxList = "";
/*    */       else {
/* 40 */         rtxList = strPreHandle(rtxList);
/*    */       }
/* 42 */       if (null == mailList)
/* 43 */         mailList = "";
/*    */       else {
/* 45 */         mailList = strPreHandle(mailList);
/*    */       }
/* 47 */       if (null == smsList)
/* 48 */         smsList = "";
/*    */       else {
/* 50 */         smsList = strPreHandle(smsList);
/*    */       }
/*    */ 
/* 53 */       CustomLogger.BusinessLogger.info("{\"time\":\"" + CacheUtil.getNowTime() + "\"" + ",\"key\":" + "\"" + key + "\"" + ",\"hostname\":" + "\"" + CacheUtil.HOST_NAME + "\"" + ",\"type\":" + "\"" + type + "\"" + ",\"value\":" + "\"" + value + "\"" + ",\"detail\":" + "\"" + detail + "\"" + ",\"RTX\":" + "\"" + rtxList + "\"" + ",\"MAIL\":" + "\"" + mailList + "\"" + ",\"SMS\":" + "\"" + smsList + "\"}");
/*    */     }
/*    */     catch (Exception e)
/*    */     {
/*    */     }
/*    */   }
/*    */ 
/*    */   private static String strPreHandle(String str)
/*    */   {
/*    */     try
/*    */     {
/* 70 */       str = str.replace("\r\n", " ");
/* 71 */       str = str.replace("\r", " ");
/* 72 */       str = str.replace("\n", " ");
/* 73 */       str = str.replace("\\", "\\\\");
/* 74 */       str = str.replace("\"", "\\\"");
/* 75 */       str = str.trim();
/*    */     }
/*    */     catch (Exception e) {
/*    */     }
/* 79 */     return str;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.common.BusinessModule
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.profiler;
/*    */ 
/*    */ import com.jd.ump.profiler.util.CacheUtil;
/*    */ import com.jd.ump.profiler.util.CustomLogger;
/*    */ import com.jd.ump.profiler.util.LogFormatter;
/*    */ 
/*    */ public class CallerInfo
/*    */ {
/*    */   public static final int STATE_TRUE = 0;
/*    */   public static final int STATE_FALSE = 1;
/* 11 */   private static final String TP_LOG_TEMPLATE = "{\"time\":\"{}\",\"key\":\"{}\",\"hostname\":\"" + CacheUtil.HOST_NAME + "\",\"processState\":" + "\"{}\",\"elapsedTime\":\"{}\"}";
/*    */   private long startTime;
/*    */   private String key;
/*    */   private int processState;
/*    */   private long elapsedTime;
/*    */ 
/*    */   public CallerInfo(String key)
/*    */   {
/* 21 */     this.key = key;
/* 22 */     this.startTime = System.currentTimeMillis();
/* 23 */     this.elapsedTime = -1L;
/* 24 */     this.processState = 0;
/*    */   }
/*    */ 
/*    */   public int getProcessState() {
/* 28 */     return this.processState;
/*    */   }
/*    */ 
/*    */   public void error()
/*    */   {
/* 36 */     this.processState = 1;
/*    */   }
/*    */ 
/*    */   public long getStartTime() {
/* 40 */     return this.startTime;
/*    */   }
/*    */ 
/*    */   public String getKey() {
/* 44 */     return this.key;
/*    */   }
/*    */ 
/*    */   public long getElapsedTime() {
/* 48 */     if (this.elapsedTime == -1L) {
/* 49 */       this.elapsedTime = (System.currentTimeMillis() - this.startTime);
/*    */     }
/* 51 */     return this.elapsedTime;
/*    */   }
/*    */ 
/*    */   public void stop() {
/* 55 */     CustomLogger.TpLogger.info(toString());
/*    */   }
/*    */ 
/*    */   public String toString()
/*    */   {
/* 60 */     return LogFormatter.format(TP_LOG_TEMPLATE, new Object[] { CacheUtil.getNowTime(), this.key, Integer.valueOf(this.processState), Long.valueOf(getElapsedTime()) });
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.CallerInfo
 * JD-Core Version:    0.6.2
 */
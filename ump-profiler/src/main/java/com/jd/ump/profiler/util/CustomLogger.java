/*    */ package com.jd.ump.profiler.util;
/*    */ 
/*    */ import com.jd.ump.log4j.Logger;
/*    */ 
/*    */ public class CustomLogger
/*    */ {
/* 12 */   public static final CustomLogger TpLogger = new CustomLogger(CustomLogFactory.getLogger("tpLogger"));
/* 13 */   public static final CustomLogger AliveLogger = new CustomLogger(CustomLogFactory.getLogger("aliveLogger"));
/* 14 */   public static final CustomLogger BusinessLogger = new CustomLogger(CustomLogFactory.getLogger("businessLogger"));
/* 15 */   public static final CustomLogger BizLogger = new CustomLogger(CustomLogFactory.getLogger("bizLogger"));
/* 16 */   public static final CustomLogger JVMLogger = new CustomLogger(CustomLogFactory.getLogger("jvmLogger"));
/* 17 */   public static final CustomLogger CommonLogger = new CustomLogger(CustomLogFactory.getLogger("commonLogger"));
/*    */   private Logger logger;
/*    */ 
/*    */   public CustomLogger(Logger logger)
/*    */   {
/* 21 */     this.logger = logger;
/*    */   }
/*    */ 
/*    */   public void info(String message) {
/* 25 */     this.logger.info(message);
/*    */   }
/*    */ 
/*    */   public Logger getLogger() {
/* 29 */     return this.logger;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.CustomLogger
 * JD-Core Version:    0.6.2
 */
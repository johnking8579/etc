/*     */ package com.jd.ump.profiler.util;
/*     */ 
/*     */ import com.jd.ump.log4j.Category;
/*     */ import java.util.concurrent.ConcurrentLinkedQueue;
/*     */ import java.util.concurrent.atomic.AtomicInteger;
/*     */ 
/*     */ public class MessagesStore
/*     */ {
/*     */   private static final int MAX_MSG_PER_CATEGORY = 262144;
/*     */   private static final int BATCH_LOG_SIZE = 128;
/*  19 */   private static final String LINE_SEP = System.getProperty("line.separator");
/*     */   private final ConcurrentLinkedQueue<Object> messages;
/*     */   private final Category category;
/*     */   private final AtomicInteger counter;
/*     */ 
/*     */   public MessagesStore(Category category)
/*     */   {
/*  29 */     this.messages = new ConcurrentLinkedQueue();
/*  30 */     this.counter = new AtomicInteger(0);
/*  31 */     this.category = category;
/*     */ 
/*  33 */     int bufferSize = 8192;
/*  34 */     if (("tpLogger".equals(category.getName())) || ("bizLogger".equals(category.getName()))) {
/*  35 */       bufferSize = 32768;
/*     */     }
/*     */ 
/*  39 */     Thread writeThread = new Thread(new WriteLog2File(bufferSize), "UMP-WriteLog2FileThread-" + category.getName());
/*  40 */     writeThread.setDaemon(true);
/*  41 */     writeThread.start();
/*     */   }
/*     */ 
/*     */   public void storeMsg(Object message)
/*     */   {
/*  47 */     if (this.counter.get() < 262144) {
/*  48 */       this.messages.offer(message);
/*  49 */       this.counter.incrementAndGet();
/*     */     }
/*     */   }
/*     */ 
/*     */   class WriteLog2File
/*     */     implements Runnable
/*     */   {
/*     */     StringBuilder logBuffer;
/*     */     private int pollCount;
/*     */ 
/*     */     public WriteLog2File(int bufferSize)
/*     */     {
/*  67 */       this.pollCount = 0;
/*  68 */       this.logBuffer = new StringBuilder(bufferSize);
/*     */     }
/*     */ 
/*     */     public void run()
/*     */     {
/*     */       while (true)
/*     */         try {
/*  75 */           this.pollCount += 1;
/*  76 */           Object msg = MessagesStore.this.messages.poll();
/*     */ 
/*  78 */           if (msg != null)
/*     */           {
/*  80 */             MessagesStore.this.counter.decrementAndGet();
/*  81 */             this.logBuffer.append(msg.toString()).append(MessagesStore.LINE_SEP);
/*     */           }
/*     */           else {
/*     */             try {
/*  85 */               Thread.sleep(1L);
/*     */             }
/*     */             catch (InterruptedException e)
/*     */             {
/*     */             }
/*     */           }
/*  91 */           if (this.pollCount >= 128)
/*     */           {
/*  94 */             this.pollCount = 0;
/*  95 */             writeLogNow();
/*     */           }
/*     */         }
/*     */         catch (Throwable ex) {
/*     */           try {
/* 100 */             Thread.sleep(1L);
/*     */           }
/*     */           catch (InterruptedException e)
/*     */           {
/*     */           }
/*     */         }
/*     */     }
/*     */ 
/*     */     private void writeLogNow()
/*     */     {
/* 112 */       int logLength = this.logBuffer.length();
/*     */ 
/* 114 */       if (logLength > 0)
/*     */         try {
/* 116 */           MessagesStore.this.category.infoReal(this.logBuffer.toString());
/*     */         }
/*     */         finally {
/* 119 */           this.logBuffer.setLength(0);
/*     */         }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.util.MessagesStore
 * JD-Core Version:    0.6.2
 */
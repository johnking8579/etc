/*     */ package com.jd.ump.profiler.proxy;
/*     */ 
/*     */ import com.jd.ump.profiler.CallerInfo;
/*     */ import com.jd.ump.profiler.ProfilerImpl;
/*     */ import java.util.Map;
/*     */ 
/*     */ public class Profiler
/*     */ {
/*     */   public static CallerInfo registerInfo(String key)
/*     */   {
/*  21 */     CallerInfo callerInfo = null;
/*  22 */     if ((key != null) && (!"".equals(key.trim()))) {
/*  23 */       callerInfo = ProfilerImpl.scopeStart(key.trim(), true, false);
/*     */     }
/*  25 */     return callerInfo;
/*     */   }
/*     */ 
/*     */   public static CallerInfo registerInfo(String key, boolean enableHeartbeat, boolean enableTP)
/*     */   {
/*  41 */     CallerInfo callerInfo = null;
/*  42 */     if ((key != null) && (!"".equals(key.trim()))) {
/*  43 */       callerInfo = ProfilerImpl.scopeStart(key.trim(), enableHeartbeat, enableTP);
/*     */     }
/*  45 */     return callerInfo;
/*     */   }
/*     */ 
/*     */   public static void registerInfoEnd(CallerInfo callerInfo)
/*     */   {
/*  55 */     ProfilerImpl.scopeEnd(callerInfo);
/*     */   }
/*     */ 
/*     */   public static void businessAlarm(String key, String detail)
/*     */   {
/*  69 */     if ((key != null) && (!"".equals(key.trim())) && (detail != null) && (!"".equals(detail.trim())))
/*  70 */       ProfilerImpl.registerBusiness(key.trim(), 0, 0, detail.trim());
/*     */   }
/*     */ 
/*     */   public static void businessAlarm(String key, long time, String detail)
/*     */   {
/*  85 */     if ((key != null) && (!"".equals(key.trim())) && (detail != null) && (!"".equals(detail.trim())))
/*  86 */       ProfilerImpl.registerBusiness(key.trim(), 0, 0, detail.trim());
/*     */   }
/*     */ 
/*     */   public static void businessAlarm(String key, String detail, String rtxList, String mailList, String smsList)
/*     */   {
/* 106 */     if ((key != null) && (!"".equals(key.trim())) && (detail != null) && (!"".equals(detail.trim())))
/* 107 */       ProfilerImpl.registerBusiness(key.trim(), 5, 0, detail.trim(), rtxList, mailList, smsList);
/*     */   }
/*     */ 
/*     */   public static void businessAlarm(String key, long time, String detail, String rtxList, String mailList, String smsList)
/*     */   {
/* 125 */     if ((key != null) && (!"".equals(key.trim())) && (detail != null) && (!"".equals(detail.trim())))
/* 126 */       ProfilerImpl.registerBusiness(key.trim(), 5, 0, detail.trim(), rtxList, mailList, smsList);
/*     */   }
/*     */ 
/*     */   public static void InitHeartBeats(String key)
/*     */   {
/* 137 */     if ((key != null) && (!"".equals(key.trim())))
/* 138 */       ProfilerImpl.scopeAlive(key.trim());
/*     */   }
/*     */ 
/*     */   public static void functionError(CallerInfo callerInfo)
/*     */   {
/* 149 */     ProfilerImpl.scopeFunctionError(callerInfo);
/*     */   }
/*     */ 
/*     */   public static void valueAccumulate(String key, Number bValue)
/*     */   {
/* 163 */     if ((key != null) && (!"".equals(key.trim())) && (bValue != null))
/* 164 */       ProfilerImpl.registerBizData(key.trim(), 1, bValue);
/*     */   }
/*     */ 
/*     */   public static void countAccumulate(String key)
/*     */   {
/* 177 */     if ((key != null) && (!"".equals(key.trim())))
/* 178 */       ProfilerImpl.registerBizData(key.trim(), 2, Integer.valueOf(1));
/*     */   }
/*     */ 
/*     */   public static void sourceDataByNum(String key, Map<String, Number> dataMap)
/*     */   {
/* 193 */     if ((key != null) && (!"".equals(key.trim())) && (dataMap != null) && (!dataMap.isEmpty()))
/* 194 */       ProfilerImpl.registerBizData(key.trim(), 3, dataMap);
/*     */   }
/*     */ 
/*     */   public static void sourceDataByStr(String key, Map<String, String> dataMap)
/*     */   {
/* 208 */     if ((key != null) && (!"".equals(key.trim())) && (dataMap != null) && (!dataMap.isEmpty()))
/* 209 */       ProfilerImpl.registerBizData(key.trim(), 3, dataMap);
/*     */   }
/*     */ 
/*     */   public static void registerJVMInfo(String key)
/*     */   {
/* 220 */     if ((key != null) && (!"".equals(key.trim())))
/* 221 */       ProfilerImpl.registerJvmData(key.trim());
/*     */   }
/*     */ 
/*     */   public static void bizNode(String nodeID, Map<String, String> data)
/*     */   {
/*     */     try
/*     */     {
/* 233 */       if ((nodeID != null) && (!"".equals(nodeID = nodeID.trim())) && (data != null) && (!data.isEmpty()))
/*     */       {
/* 235 */         ProfilerImpl.bizNode(nodeID, data);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void bizNode(String nodeID, String data)
/*     */   {
/*     */     try
/*     */     {
/* 249 */       if ((nodeID != null) && (!"".equals(nodeID = nodeID.trim())) && (data != null) && (!"".equals(data = data.trim())))
/*     */       {
/* 251 */         ProfilerImpl.bizNode(nodeID, data);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void log(String type, String data)
/*     */   {
/*     */     try
/*     */     {
/* 264 */       if ((type != null) && (!"".equals(type = type.trim())) && (data != null) && (!"".equals(data = data.trim())))
/*     */       {
/* 267 */         ProfilerImpl.log(type, data);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   public static void log(String type, Map<String, String> data)
/*     */   {
/*     */     try
/*     */     {
/* 280 */       if ((type != null) && (!"".equals(type = type.trim())) && (data != null) && (!data.isEmpty()))
/*     */       {
/* 283 */         ProfilerImpl.log(type, data);
/*     */       }
/*     */     }
/*     */     catch (Throwable t)
/*     */     {
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.profiler.proxy.Profiler
 * JD-Core Version:    0.6.2
 */
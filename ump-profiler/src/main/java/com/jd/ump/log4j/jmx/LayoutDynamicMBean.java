/*     */ package com.jd.ump.log4j.jmx;
/*     */ 
/*     */ import com.jd.ump.log4j.Layout;
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.Priority;
/*     */ import com.jd.ump.log4j.helpers.OptionConverter;
/*     */ import com.jd.ump.log4j.spi.OptionHandler;
/*     */ import java.beans.BeanInfo;
/*     */ import java.beans.IntrospectionException;
/*     */ import java.beans.Introspector;
/*     */ import java.beans.PropertyDescriptor;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.lang.reflect.Constructor;
/*     */ import java.lang.reflect.InvocationTargetException;
/*     */ import java.lang.reflect.Method;
/*     */ import java.util.Hashtable;
/*     */ import java.util.Vector;
/*     */ import javax.management.Attribute;
/*     */ import javax.management.AttributeNotFoundException;
/*     */ import javax.management.InvalidAttributeValueException;
/*     */ import javax.management.MBeanAttributeInfo;
/*     */ import javax.management.MBeanConstructorInfo;
/*     */ import javax.management.MBeanException;
/*     */ import javax.management.MBeanInfo;
/*     */ import javax.management.MBeanNotificationInfo;
/*     */ import javax.management.MBeanOperationInfo;
/*     */ import javax.management.MBeanParameterInfo;
/*     */ import javax.management.ReflectionException;
/*     */ import javax.management.RuntimeOperationsException;
/*     */ 
/*     */ public class LayoutDynamicMBean extends AbstractDynamicMBean
/*     */ {
/*  54 */   private MBeanConstructorInfo[] dConstructors = new MBeanConstructorInfo[1];
/*  55 */   private Vector dAttributes = new Vector();
/*  56 */   private String dClassName = getClass().getName();
/*     */ 
/*  58 */   private Hashtable dynamicProps = new Hashtable(5);
/*  59 */   private MBeanOperationInfo[] dOperations = new MBeanOperationInfo[1];
/*  60 */   private String dDescription = "This MBean acts as a management facade for log4j layouts.";
/*     */ 
/*  64 */   private static Logger cat = Logger.getLogger(LayoutDynamicMBean.class);
/*     */   private Layout layout;
/*     */ 
/*     */   public LayoutDynamicMBean(Layout layout)
/*     */     throws IntrospectionException
/*     */   {
/*  70 */     this.layout = layout;
/*  71 */     buildDynamicMBeanInfo();
/*     */   }
/*     */ 
/*     */   private void buildDynamicMBeanInfo() throws IntrospectionException
/*     */   {
/*  76 */     Constructor[] constructors = getClass().getConstructors();
/*  77 */     this.dConstructors[0] = new MBeanConstructorInfo("LayoutDynamicMBean(): Constructs a LayoutDynamicMBean instance", constructors[0]);
/*     */ 
/*  82 */     BeanInfo bi = Introspector.getBeanInfo(this.layout.getClass());
/*  83 */     PropertyDescriptor[] pd = bi.getPropertyDescriptors();
/*     */ 
/*  85 */     int size = pd.length;
/*     */ 
/*  87 */     for (int i = 0; i < size; i++) {
/*  88 */       String name = pd[i].getName();
/*  89 */       Method readMethod = pd[i].getReadMethod();
/*  90 */       Method writeMethod = pd[i].getWriteMethod();
/*  91 */       if (readMethod != null) {
/*  92 */         Class returnClass = readMethod.getReturnType();
/*  93 */         if (isSupportedType(returnClass))
/*     */         {
/*     */           String returnClassName;
/*     */           String returnClassName;
/*  95 */           if (returnClass.isAssignableFrom(Level.class))
/*  96 */             returnClassName = "java.lang.String";
/*     */           else {
/*  98 */             returnClassName = returnClass.getName();
/*     */           }
/*     */ 
/* 101 */           this.dAttributes.add(new MBeanAttributeInfo(name, returnClassName, "Dynamic", true, writeMethod != null, false));
/*     */ 
/* 107 */           this.dynamicProps.put(name, new MethodUnion(readMethod, writeMethod));
/*     */         }
/*     */       }
/*     */     }
/*     */ 
/* 112 */     MBeanParameterInfo[] params = new MBeanParameterInfo[0];
/*     */ 
/* 114 */     this.dOperations[0] = new MBeanOperationInfo("activateOptions", "activateOptions(): add an layout", params, "void", 1);
/*     */   }
/*     */ 
/*     */   private boolean isSupportedType(Class clazz)
/*     */   {
/* 123 */     if (clazz.isPrimitive()) {
/* 124 */       return true;
/*     */     }
/*     */ 
/* 127 */     if (clazz == String.class) {
/* 128 */       return true;
/*     */     }
/* 130 */     if (clazz.isAssignableFrom(Level.class)) {
/* 131 */       return true;
/*     */     }
/*     */ 
/* 134 */     return false;
/*     */   }
/*     */ 
/*     */   public MBeanInfo getMBeanInfo()
/*     */   {
/* 141 */     cat.debug("getMBeanInfo called.");
/*     */ 
/* 143 */     MBeanAttributeInfo[] attribs = new MBeanAttributeInfo[this.dAttributes.size()];
/* 144 */     this.dAttributes.toArray(attribs);
/*     */ 
/* 146 */     return new MBeanInfo(this.dClassName, this.dDescription, attribs, this.dConstructors, this.dOperations, new MBeanNotificationInfo[0]);
/*     */   }
/*     */ 
/*     */   public Object invoke(String operationName, Object[] params, String[] signature)
/*     */     throws MBeanException, ReflectionException
/*     */   {
/* 159 */     if ((operationName.equals("activateOptions")) && ((this.layout instanceof OptionHandler)))
/*     */     {
/* 161 */       OptionHandler oh = this.layout;
/* 162 */       oh.activateOptions();
/* 163 */       return "Options activated.";
/*     */     }
/* 165 */     return null;
/*     */   }
/*     */ 
/*     */   protected Logger getLogger()
/*     */   {
/* 170 */     return cat;
/*     */   }
/*     */ 
/*     */   public Object getAttribute(String attributeName)
/*     */     throws AttributeNotFoundException, MBeanException, ReflectionException
/*     */   {
/* 180 */     if (attributeName == null) {
/* 181 */       throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), "Cannot invoke a getter of " + this.dClassName + " with null attribute name");
/*     */     }
/*     */ 
/* 187 */     MethodUnion mu = (MethodUnion)this.dynamicProps.get(attributeName);
/*     */ 
/* 189 */     cat.debug("----name=" + attributeName + ", mu=" + mu);
/*     */ 
/* 191 */     if ((mu != null) && (mu.readMethod != null)) {
/*     */       try {
/* 193 */         return mu.readMethod.invoke(this.layout, null);
/*     */       } catch (InvocationTargetException e) {
/* 195 */         if (((e.getTargetException() instanceof InterruptedException)) || ((e.getTargetException() instanceof InterruptedIOException)))
/*     */         {
/* 197 */           Thread.currentThread().interrupt();
/*     */         }
/* 199 */         return null;
/*     */       } catch (IllegalAccessException e) {
/* 201 */         return null;
/*     */       } catch (RuntimeException e) {
/* 203 */         return null;
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 210 */     throw new AttributeNotFoundException("Cannot find " + attributeName + " attribute in " + this.dClassName);
/*     */   }
/*     */ 
/*     */   public void setAttribute(Attribute attribute)
/*     */     throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException
/*     */   {
/* 223 */     if (attribute == null) {
/* 224 */       throw new RuntimeOperationsException(new IllegalArgumentException("Attribute cannot be null"), "Cannot invoke a setter of " + this.dClassName + " with null attribute");
/*     */     }
/*     */ 
/* 229 */     String name = attribute.getName();
/* 230 */     Object value = attribute.getValue();
/*     */ 
/* 232 */     if (name == null) {
/* 233 */       throw new RuntimeOperationsException(new IllegalArgumentException("Attribute name cannot be null"), "Cannot invoke the setter of " + this.dClassName + " with null attribute name");
/*     */     }
/*     */ 
/* 241 */     MethodUnion mu = (MethodUnion)this.dynamicProps.get(name);
/*     */ 
/* 243 */     if ((mu != null) && (mu.writeMethod != null)) {
/* 244 */       Object[] o = new Object[1];
/*     */ 
/* 246 */       Class[] params = mu.writeMethod.getParameterTypes();
/* 247 */       if (params[0] == Priority.class) {
/* 248 */         value = OptionConverter.toLevel((String)value, (Level)getAttribute(name));
/*     */       }
/*     */ 
/* 251 */       o[0] = value;
/*     */       try
/*     */       {
/* 254 */         mu.writeMethod.invoke(this.layout, o);
/*     */       }
/*     */       catch (InvocationTargetException e) {
/* 257 */         if (((e.getTargetException() instanceof InterruptedException)) || ((e.getTargetException() instanceof InterruptedIOException)))
/*     */         {
/* 259 */           Thread.currentThread().interrupt();
/*     */         }
/* 261 */         cat.error("FIXME", e);
/*     */       } catch (IllegalAccessException e) {
/* 263 */         cat.error("FIXME", e);
/*     */       } catch (RuntimeException e) {
/* 265 */         cat.error("FIXME", e);
/*     */       }
/*     */     } else {
/* 268 */       throw new AttributeNotFoundException("Attribute " + name + " not found in " + getClass().getName());
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.jmx.LayoutDynamicMBean
 * JD-Core Version:    0.6.2
 */
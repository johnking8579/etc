/*    */ package com.jd.ump.log4j;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public class SimpleLayout extends Layout
/*    */ {
/*    */   public void activateOptions()
/*    */   {
/*    */   }
/*    */ 
/*    */   public String format(LoggingEvent event)
/*    */   {
/* 57 */     return event.getRenderedMessage();
/*    */   }
/*    */ 
/*    */   public boolean ignoresThrowable()
/*    */   {
/* 68 */     return true;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.SimpleLayout
 * JD-Core Version:    0.6.2
 */
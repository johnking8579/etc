package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.Appender;
import com.jd.ump.log4j.Category;

public abstract interface HierarchyEventListener
{
  public abstract void addAppenderEvent(Category paramCategory, Appender paramAppender);

  public abstract void removeAppenderEvent(Category paramCategory, Appender paramAppender);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.HierarchyEventListener
 * JD-Core Version:    0.6.2
 */
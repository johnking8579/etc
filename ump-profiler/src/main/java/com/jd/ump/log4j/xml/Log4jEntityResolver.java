/*    */ package com.jd.ump.log4j.xml;
/*    */ 
/*    */ import com.jd.ump.log4j.helpers.LogLog;
/*    */ import java.io.ByteArrayInputStream;
/*    */ import java.io.InputStream;
/*    */ import org.xml.sax.EntityResolver;
/*    */ import org.xml.sax.InputSource;
/*    */ 
/*    */ public class Log4jEntityResolver
/*    */   implements EntityResolver
/*    */ {
/*    */   private static final String PUBLIC_ID = "-//APACHE//DTD LOG4J 1.2//EN";
/*    */ 
/*    */   public InputSource resolveEntity(String publicId, String systemId)
/*    */   {
/* 39 */     if ((systemId.endsWith("log4j.dtd")) || ("-//APACHE//DTD LOG4J 1.2//EN".equals(publicId))) {
/* 40 */       Class clazz = getClass();
/* 41 */       InputStream in = clazz.getResourceAsStream("/org/apache/log4j/xml/log4j.dtd");
/* 42 */       if (in == null) {
/* 43 */         LogLog.warn("Could not find [log4j.dtd] using [" + clazz.getClassLoader() + "] class loader, parsed without DTD.");
/*    */ 
/* 45 */         in = new ByteArrayInputStream(new byte[0]);
/*    */       }
/* 47 */       return new InputSource(in);
/*    */     }
/* 49 */     return null;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.xml.Log4jEntityResolver
 * JD-Core Version:    0.6.2
 */
package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.or.ObjectRenderer;
import com.jd.ump.log4j.or.RendererMap;

public abstract interface RendererSupport
{
  public abstract RendererMap getRendererMap();

  public abstract void setRenderer(Class paramClass, ObjectRenderer paramObjectRenderer);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.RendererSupport
 * JD-Core Version:    0.6.2
 */
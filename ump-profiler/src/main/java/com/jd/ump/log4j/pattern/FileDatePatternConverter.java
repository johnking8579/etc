/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ public final class FileDatePatternConverter
/*    */ {
/*    */   public static PatternConverter newInstance(String[] options)
/*    */   {
/* 41 */     if ((options == null) || (options.length == 0)) {
/* 42 */       return DatePatternConverter.newInstance(new String[] { "yyyy-MM-dd" });
/*    */     }
/*    */ 
/* 48 */     return DatePatternConverter.newInstance(options);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.FileDatePatternConverter
 * JD-Core Version:    0.6.2
 */
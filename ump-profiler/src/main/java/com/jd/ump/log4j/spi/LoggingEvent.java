/*     */ package com.jd.ump.log4j.spi;
/*     */ 
/*     */ import com.jd.ump.log4j.Category;
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.MDC;
/*     */ import com.jd.ump.log4j.NDC;
/*     */ import com.jd.ump.log4j.Priority;
/*     */ import com.jd.ump.log4j.helpers.Loader;
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import java.io.IOException;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.io.ObjectInputStream;
/*     */ import java.io.ObjectOutputStream;
/*     */ import java.io.Serializable;
/*     */ import java.lang.reflect.InvocationTargetException;
/*     */ import java.lang.reflect.Method;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.Hashtable;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ 
/*     */ public class LoggingEvent
/*     */   implements Serializable
/*     */ {
/*  57 */   private static long startTime = System.currentTimeMillis();
/*     */   public final transient String fqnOfCategoryClass;
/*     */ 
/*     */   /** @deprecated */
/*     */   private transient Category logger;
/*     */ 
/*     */   /** @deprecated */
/*     */   public final String categoryName;
/*     */ 
/*     */   /** @deprecated */
/*     */   public transient Priority level;
/*     */   private String ndc;
/*     */   private Hashtable mdcCopy;
/* 109 */   private boolean ndcLookupRequired = true;
/*     */ 
/* 115 */   private boolean mdcCopyLookupRequired = true;
/*     */   private transient Object message;
/*     */   private String renderedMessage;
/*     */   private String threadName;
/*     */   private ThrowableInformation throwableInfo;
/*     */   public final long timeStamp;
/*     */   private LocationInfo locationInfo;
/*     */   static final long serialVersionUID = -868428216207166145L;
/* 142 */   static final Integer[] PARAM_ARRAY = new Integer[1];
/*     */   static final String TO_LEVEL = "toLevel";
/* 144 */   static final Class[] TO_LEVEL_PARAMS = { Integer.TYPE };
/* 145 */   static final Hashtable methodCache = new Hashtable(3);
/*     */ 
/*     */   public LoggingEvent(String fqnOfCategoryClass, Category logger, Priority level, Object message, Throwable throwable)
/*     */   {
/* 159 */     this.fqnOfCategoryClass = fqnOfCategoryClass;
/* 160 */     this.logger = logger;
/* 161 */     this.categoryName = logger.getName();
/* 162 */     this.level = level;
/* 163 */     this.message = message;
/* 164 */     if (throwable != null) {
/* 165 */       this.throwableInfo = new ThrowableInformation(throwable, logger);
/*     */     }
/* 167 */     this.timeStamp = System.currentTimeMillis();
/*     */   }
/*     */ 
/*     */   public LoggingEvent(String fqnOfCategoryClass, Category logger, long timeStamp, Priority level, Object message, Throwable throwable)
/*     */   {
/* 184 */     this.fqnOfCategoryClass = fqnOfCategoryClass;
/* 185 */     this.logger = logger;
/* 186 */     this.categoryName = logger.getName();
/* 187 */     this.level = level;
/* 188 */     this.message = message;
/* 189 */     if (throwable != null) {
/* 190 */       this.throwableInfo = new ThrowableInformation(throwable, logger);
/*     */     }
/*     */ 
/* 193 */     this.timeStamp = timeStamp;
/*     */   }
/*     */ 
/*     */   public LoggingEvent(String fqnOfCategoryClass, Category logger, long timeStamp, Level level, Object message, String threadName, ThrowableInformation throwable, String ndc, LocationInfo info, Map properties)
/*     */   {
/* 222 */     this.fqnOfCategoryClass = fqnOfCategoryClass;
/* 223 */     this.logger = logger;
/* 224 */     if (logger != null)
/* 225 */       this.categoryName = logger.getName();
/*     */     else {
/* 227 */       this.categoryName = null;
/*     */     }
/* 229 */     this.level = level;
/* 230 */     this.message = message;
/* 231 */     if (throwable != null) {
/* 232 */       this.throwableInfo = throwable;
/*     */     }
/*     */ 
/* 235 */     this.timeStamp = timeStamp;
/* 236 */     this.threadName = threadName;
/* 237 */     this.ndcLookupRequired = false;
/* 238 */     this.ndc = ndc;
/* 239 */     this.locationInfo = info;
/* 240 */     this.mdcCopyLookupRequired = false;
/* 241 */     if (properties != null)
/* 242 */       this.mdcCopy = new Hashtable(properties);
/*     */   }
/*     */ 
/*     */   public LocationInfo getLocationInformation()
/*     */   {
/* 252 */     if (this.locationInfo == null) {
/* 253 */       this.locationInfo = new LocationInfo(new Throwable(), this.fqnOfCategoryClass);
/*     */     }
/* 255 */     return this.locationInfo;
/*     */   }
/*     */ 
/*     */   public Level getLevel()
/*     */   {
/* 262 */     return (Level)this.level;
/*     */   }
/*     */ 
/*     */   public String getLoggerName()
/*     */   {
/* 270 */     return this.categoryName;
/*     */   }
/*     */ 
/*     */   public Category getLogger()
/*     */   {
/* 279 */     return this.logger;
/*     */   }
/*     */ 
/*     */   public Object getMessage()
/*     */   {
/* 293 */     if (this.message != null) {
/* 294 */       return this.message;
/*     */     }
/* 296 */     return getRenderedMessage();
/*     */   }
/*     */ 
/*     */   public String getNDC()
/*     */   {
/* 307 */     if (this.ndcLookupRequired) {
/* 308 */       this.ndcLookupRequired = false;
/* 309 */       this.ndc = NDC.get();
/*     */     }
/* 311 */     return this.ndc;
/*     */   }
/*     */ 
/*     */   public Object getMDC(String key)
/*     */   {
/* 332 */     if (this.mdcCopy != null) {
/* 333 */       Object r = this.mdcCopy.get(key);
/* 334 */       if (r != null) {
/* 335 */         return r;
/*     */       }
/*     */     }
/* 338 */     return MDC.get(key);
/*     */   }
/*     */ 
/*     */   public void getMDCCopy()
/*     */   {
/* 347 */     if (this.mdcCopyLookupRequired) {
/* 348 */       this.mdcCopyLookupRequired = false;
/*     */ 
/* 351 */       Hashtable t = MDC.getContext();
/* 352 */       if (t != null)
/* 353 */         this.mdcCopy = ((Hashtable)t.clone());
/*     */     }
/*     */   }
/*     */ 
/*     */   public String getRenderedMessage()
/*     */   {
/* 360 */     if ((this.renderedMessage == null) && (this.message != null)) {
/* 361 */       this.renderedMessage = ((String)this.message);
/*     */     }
/*     */ 
/* 364 */     return this.renderedMessage;
/*     */   }
/*     */ 
/*     */   public static long getStartTime()
/*     */   {
/* 371 */     return startTime;
/*     */   }
/*     */ 
/*     */   public String getThreadName()
/*     */   {
/* 376 */     if (this.threadName == null)
/* 377 */       this.threadName = Thread.currentThread().getName();
/* 378 */     return this.threadName;
/*     */   }
/*     */ 
/*     */   public ThrowableInformation getThrowableInformation()
/*     */   {
/* 391 */     return this.throwableInfo;
/*     */   }
/*     */ 
/*     */   public String[] getThrowableStrRep()
/*     */   {
/* 400 */     if (this.throwableInfo == null) {
/* 401 */       return null;
/*     */     }
/* 403 */     return this.throwableInfo.getThrowableStrRep();
/*     */   }
/*     */ 
/*     */   private void readLevel(ObjectInputStream ois)
/*     */     throws IOException, ClassNotFoundException
/*     */   {
/* 411 */     int p = ois.readInt();
/*     */     try {
/* 413 */       String className = (String)ois.readObject();
/* 414 */       if (className == null) {
/* 415 */         this.level = Level.toLevel(p);
/*     */       } else {
/* 417 */         Method m = (Method)methodCache.get(className);
/* 418 */         if (m == null) {
/* 419 */           Class clazz = Loader.loadClass(className);
/*     */ 
/* 426 */           m = clazz.getDeclaredMethod("toLevel", TO_LEVEL_PARAMS);
/* 427 */           methodCache.put(className, m);
/*     */         }
/* 429 */         PARAM_ARRAY[0] = new Integer(p);
/* 430 */         this.level = ((Level)m.invoke(null, PARAM_ARRAY));
/*     */       }
/*     */     } catch (InvocationTargetException e) {
/* 433 */       if (((e.getTargetException() instanceof InterruptedException)) || ((e.getTargetException() instanceof InterruptedIOException)))
/*     */       {
/* 435 */         Thread.currentThread().interrupt();
/*     */       }
/* 437 */       LogLog.warn("Level deserialization failed, reverting to default.", e);
/* 438 */       this.level = Level.toLevel(p);
/*     */     } catch (NoSuchMethodException e) {
/* 440 */       LogLog.warn("Level deserialization failed, reverting to default.", e);
/* 441 */       this.level = Level.toLevel(p);
/*     */     } catch (IllegalAccessException e) {
/* 443 */       LogLog.warn("Level deserialization failed, reverting to default.", e);
/* 444 */       this.level = Level.toLevel(p);
/*     */     } catch (RuntimeException e) {
/* 446 */       LogLog.warn("Level deserialization failed, reverting to default.", e);
/* 447 */       this.level = Level.toLevel(p);
/*     */     }
/*     */   }
/*     */ 
/*     */   private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException
/*     */   {
/* 453 */     ois.defaultReadObject();
/* 454 */     readLevel(ois);
/*     */ 
/* 457 */     if (this.locationInfo == null)
/* 458 */       this.locationInfo = new LocationInfo(null, null);
/*     */   }
/*     */ 
/*     */   private void writeObject(ObjectOutputStream oos)
/*     */     throws IOException
/*     */   {
/* 465 */     getThreadName();
/*     */ 
/* 468 */     getRenderedMessage();
/*     */ 
/* 472 */     getNDC();
/*     */ 
/* 476 */     getMDCCopy();
/*     */ 
/* 479 */     getThrowableStrRep();
/*     */ 
/* 481 */     oos.defaultWriteObject();
/*     */ 
/* 484 */     writeLevel(oos);
/*     */   }
/*     */ 
/*     */   private void writeLevel(ObjectOutputStream oos)
/*     */     throws IOException
/*     */   {
/* 490 */     oos.writeInt(this.level.toInt());
/*     */ 
/* 492 */     Class clazz = this.level.getClass();
/* 493 */     if (clazz == Level.class) {
/* 494 */       oos.writeObject(null);
/*     */     }
/*     */     else
/*     */     {
/* 499 */       oos.writeObject(clazz.getName());
/*     */     }
/*     */   }
/*     */ 
/*     */   public final void setProperty(String propName, String propValue)
/*     */   {
/* 515 */     if (this.mdcCopy == null) {
/* 516 */       getMDCCopy();
/*     */     }
/* 518 */     if (this.mdcCopy == null) {
/* 519 */       this.mdcCopy = new Hashtable();
/*     */     }
/* 521 */     this.mdcCopy.put(propName, propValue);
/*     */   }
/*     */ 
/*     */   public final String getProperty(String key)
/*     */   {
/* 535 */     Object value = getMDC(key);
/* 536 */     String retval = null;
/* 537 */     if (value != null) {
/* 538 */       retval = value.toString();
/*     */     }
/* 540 */     return retval;
/*     */   }
/*     */ 
/*     */   public final boolean locationInformationExists()
/*     */   {
/* 550 */     return this.locationInfo != null;
/*     */   }
/*     */ 
/*     */   public final long getTimeStamp()
/*     */   {
/* 561 */     return this.timeStamp;
/*     */   }
/*     */ 
/*     */   public Set getPropertyKeySet()
/*     */   {
/* 576 */     return getProperties().keySet();
/*     */   }
/*     */ 
/*     */   public Map getProperties()
/*     */   {
/* 591 */     getMDCCopy();
/*     */     Map properties;
/*     */     Map properties;
/* 593 */     if (this.mdcCopy == null)
/* 594 */       properties = new HashMap();
/*     */     else {
/* 596 */       properties = this.mdcCopy;
/*     */     }
/* 598 */     return Collections.unmodifiableMap(properties);
/*     */   }
/*     */ 
/*     */   public String getFQNOfLoggerClass()
/*     */   {
/* 608 */     return this.fqnOfCategoryClass;
/*     */   }
/*     */ 
/*     */   public Object removeProperty(String propName)
/*     */   {
/* 621 */     if (this.mdcCopy == null) {
/* 622 */       getMDCCopy();
/*     */     }
/* 624 */     if (this.mdcCopy == null) {
/* 625 */       this.mdcCopy = new Hashtable();
/*     */     }
/* 627 */     return this.mdcCopy.remove(propName);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.LoggingEvent
 * JD-Core Version:    0.6.2
 */
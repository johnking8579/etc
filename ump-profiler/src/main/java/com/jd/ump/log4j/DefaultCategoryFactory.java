/*    */ package com.jd.ump.log4j;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LoggerFactory;
/*    */ 
/*    */ class DefaultCategoryFactory
/*    */   implements LoggerFactory
/*    */ {
/*    */   public Logger makeNewLoggerInstance(String name)
/*    */   {
/* 29 */     return new Logger(name);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.DefaultCategoryFactory
 * JD-Core Version:    0.6.2
 */
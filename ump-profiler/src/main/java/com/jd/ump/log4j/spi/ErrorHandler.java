package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.Appender;
import com.jd.ump.log4j.Logger;

public abstract interface ErrorHandler extends OptionHandler
{
  public abstract void setLogger(Logger paramLogger);

  public abstract void error(String paramString, Exception paramException, int paramInt);

  public abstract void error(String paramString);

  public abstract void error(String paramString, Exception paramException, int paramInt, LoggingEvent paramLoggingEvent);

  public abstract void setAppender(Appender paramAppender);

  public abstract void setBackupAppender(Appender paramAppender);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.ErrorHandler
 * JD-Core Version:    0.6.2
 */
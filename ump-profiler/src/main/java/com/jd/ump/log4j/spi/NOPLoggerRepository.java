/*     */ package com.jd.ump.log4j.spi;
/*     */ 
/*     */ import com.jd.ump.log4j.Appender;
/*     */ import com.jd.ump.log4j.Category;
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Vector;
/*     */ 
/*     */ public final class NOPLoggerRepository
/*     */   implements LoggerRepository
/*     */ {
/*     */   public void addHierarchyEventListener(HierarchyEventListener listener)
/*     */   {
/*     */   }
/*     */ 
/*     */   public boolean isDisabled(int level)
/*     */   {
/*  43 */     return true;
/*     */   }
/*     */ 
/*     */   public void setThreshold(Level level)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void setThreshold(String val)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void emitNoAppenderWarning(Category cat)
/*     */   {
/*     */   }
/*     */ 
/*     */   public Level getThreshold()
/*     */   {
/*  68 */     return Level.OFF;
/*     */   }
/*     */ 
/*     */   public Logger getLogger(String name)
/*     */   {
/*  75 */     return new NOPLogger(this, name);
/*     */   }
/*     */ 
/*     */   public Logger getLogger(String name, LoggerFactory factory)
/*     */   {
/*  82 */     return new NOPLogger(this, name);
/*     */   }
/*     */ 
/*     */   public Logger getRootLogger()
/*     */   {
/*  89 */     return new NOPLogger(this, "root");
/*     */   }
/*     */ 
/*     */   public Logger exists(String name)
/*     */   {
/*  96 */     return null;
/*     */   }
/*     */ 
/*     */   public void shutdown()
/*     */   {
/*     */   }
/*     */ 
/*     */   public Enumeration getCurrentLoggers()
/*     */   {
/* 109 */     return new Vector().elements();
/*     */   }
/*     */ 
/*     */   public Enumeration getCurrentCategories()
/*     */   {
/* 116 */     return getCurrentLoggers();
/*     */   }
/*     */ 
/*     */   public void fireAddAppenderEvent(Category logger, Appender appender)
/*     */   {
/*     */   }
/*     */ 
/*     */   public void resetConfiguration()
/*     */   {
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.NOPLoggerRepository
 * JD-Core Version:    0.6.2
 */
package com.jd.ump.log4j.spi;

public abstract interface ThrowableRenderer
{
  public abstract String[] doRender(Throwable paramThrowable);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.ThrowableRenderer
 * JD-Core Version:    0.6.2
 */
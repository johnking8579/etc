package com.jd.ump.log4j.spi;

public abstract interface RepositorySelector
{
  public abstract LoggerRepository getLoggerRepository();
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.RepositorySelector
 * JD-Core Version:    0.6.2
 */
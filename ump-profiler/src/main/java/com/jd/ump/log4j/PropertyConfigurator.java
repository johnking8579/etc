/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.config.PropertySetter;
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import com.jd.ump.log4j.helpers.OptionConverter;
/*     */ import com.jd.ump.log4j.or.RendererMap;
/*     */ import com.jd.ump.log4j.spi.Configurator;
/*     */ import com.jd.ump.log4j.spi.ErrorHandler;
/*     */ import com.jd.ump.log4j.spi.Filter;
/*     */ import com.jd.ump.log4j.spi.LoggerFactory;
/*     */ import com.jd.ump.log4j.spi.LoggerRepository;
/*     */ import com.jd.ump.log4j.spi.OptionHandler;
/*     */ import com.jd.ump.log4j.spi.RendererSupport;
/*     */ import com.jd.ump.log4j.spi.ThrowableRenderer;
/*     */ import com.jd.ump.log4j.spi.ThrowableRendererSupport;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.net.URL;
/*     */ import java.net.URLConnection;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Hashtable;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Properties;
/*     */ import java.util.Set;
/*     */ import java.util.StringTokenizer;
/*     */ import java.util.Vector;
/*     */ 
/*     */ public class PropertyConfigurator
/*     */   implements Configurator
/*     */ {
/*  98 */   protected Hashtable registry = new Hashtable(11);
/*     */   private LoggerRepository repository;
/* 100 */   protected LoggerFactory loggerFactory = new DefaultCategoryFactory();
/*     */   static final String CATEGORY_PREFIX = "log4j.category.";
/*     */   static final String LOGGER_PREFIX = "log4j.logger.";
/*     */   static final String FACTORY_PREFIX = "log4j.factory";
/*     */   static final String ADDITIVITY_PREFIX = "log4j.additivity.";
/*     */   static final String ROOT_CATEGORY_PREFIX = "log4j.rootCategory";
/*     */   static final String ROOT_LOGGER_PREFIX = "log4j.rootLogger";
/*     */   static final String APPENDER_PREFIX = "log4j.appender.";
/*     */   static final String RENDERER_PREFIX = "log4j.renderer.";
/*     */   static final String THRESHOLD_PREFIX = "log4j.threshold";
/*     */   private static final String THROWABLE_RENDERER_PREFIX = "log4j.throwableRenderer";
/*     */   private static final String LOGGER_REF = "logger-ref";
/*     */   private static final String ROOT_REF = "root-ref";
/*     */   private static final String APPENDER_REF_TAG = "appender-ref";
/*     */   public static final String LOGGER_FACTORY_KEY = "log4j.loggerFactory";
/*     */   private static final String RESET_KEY = "log4j.reset";
/*     */   private static final String INTERNAL_ROOT_NAME = "root";
/*     */ 
/*     */   public void doConfigure(String configFileName, LoggerRepository hierarchy)
/*     */   {
/* 369 */     Properties props = new Properties();
/* 370 */     FileInputStream istream = null;
/*     */     try {
/* 372 */       istream = new FileInputStream(configFileName);
/* 373 */       props.load(istream);
/* 374 */       istream.close();
/*     */     }
/*     */     catch (Exception e) {
/* 377 */       if (((e instanceof InterruptedIOException)) || ((e instanceof InterruptedException))) {
/* 378 */         Thread.currentThread().interrupt();
/* 380 */       }LogLog.error("Could not read configuration file [" + configFileName + "].", e);
/* 381 */       LogLog.error("Ignoring configuration file [" + configFileName + "].");
/*     */       return; } finally {
/* 384 */       if (istream != null) {
/*     */         try {
/* 386 */           istream.close();
/*     */         } catch (InterruptedIOException ignore) {
/* 388 */           Thread.currentThread().interrupt();
/*     */         }
/*     */         catch (Throwable ignore)
/*     */         {
/*     */         }
/*     */       }
/*     */     }
/* 395 */     doConfigure(props, hierarchy);
/*     */   }
/*     */ 
/*     */   public static void configure(String configFilename)
/*     */   {
/* 403 */     new PropertyConfigurator().doConfigure(configFilename, LogManager.getLoggerRepository());
/*     */   }
/*     */ 
/*     */   public static void configure(URL configURL)
/*     */   {
/* 415 */     new PropertyConfigurator().doConfigure(configURL, LogManager.getLoggerRepository());
/*     */   }
/*     */ 
/*     */   public static void configure(Properties properties)
/*     */   {
/* 428 */     new PropertyConfigurator().doConfigure(properties, LogManager.getLoggerRepository());
/*     */   }
/*     */ 
/*     */   public static void configureAndWatch(String configFilename)
/*     */   {
/* 443 */     configureAndWatch(configFilename, 60000L);
/*     */   }
/*     */ 
/*     */   public static void configureAndWatch(String configFilename, long delay)
/*     */   {
/* 461 */     PropertyWatchdog pdog = new PropertyWatchdog(configFilename);
/* 462 */     pdog.setDelay(delay);
/* 463 */     pdog.start();
/*     */   }
/*     */ 
/*     */   public void doConfigure(Properties properties, LoggerRepository hierarchy)
/*     */   {
/* 474 */     this.repository = hierarchy;
/* 475 */     String value = properties.getProperty("log4j.debug");
/* 476 */     if (value == null) {
/* 477 */       value = properties.getProperty("log4j.configDebug");
/* 478 */       if (value != null) {
/* 479 */         LogLog.warn("[log4j.configDebug] is deprecated. Use [log4j.debug] instead.");
/*     */       }
/*     */     }
/* 482 */     if (value != null) {
/* 483 */       LogLog.setInternalDebugging(OptionConverter.toBoolean(value, true));
/*     */     }
/*     */ 
/* 489 */     String reset = properties.getProperty("log4j.reset");
/* 490 */     if ((reset != null) && (OptionConverter.toBoolean(reset, false))) {
/* 491 */       hierarchy.resetConfiguration();
/*     */     }
/*     */ 
/* 494 */     String thresholdStr = OptionConverter.findAndSubst("log4j.threshold", properties);
/*     */ 
/* 496 */     if (thresholdStr != null) {
/* 497 */       hierarchy.setThreshold(OptionConverter.toLevel(thresholdStr, Level.ALL));
/*     */ 
/* 499 */       LogLog.debug("Hierarchy threshold set to [" + hierarchy.getThreshold() + "].");
/*     */     }
/*     */ 
/* 502 */     configureRootCategory(properties, hierarchy);
/* 503 */     configureLoggerFactory(properties);
/* 504 */     parseCatsAndRenderers(properties, hierarchy);
/*     */ 
/* 506 */     LogLog.debug("Finished configuring.");
/*     */ 
/* 509 */     this.registry.clear();
/*     */   }
/*     */ 
/*     */   public void doConfigure(URL configURL, LoggerRepository hierarchy)
/*     */   {
/* 517 */     Properties props = new Properties();
/* 518 */     LogLog.debug("Reading configuration from URL " + configURL);
/* 519 */     InputStream istream = null;
/* 520 */     URLConnection uConn = null;
/*     */     try {
/* 522 */       uConn = configURL.openConnection();
/* 523 */       uConn.setUseCaches(false);
/* 524 */       istream = uConn.getInputStream();
/* 525 */       props.load(istream);
/*     */     }
/*     */     catch (Exception e) {
/* 528 */       if (((e instanceof InterruptedIOException)) || ((e instanceof InterruptedException))) {
/* 529 */         Thread.currentThread().interrupt();
/* 531 */       }LogLog.error("Could not read configuration file from URL [" + configURL + "].", e);
/*     */ 
/* 533 */       LogLog.error("Ignoring configuration file [" + configURL + "].");
/*     */       return;
/*     */     } finally {
/* 537 */       if (istream != null)
/*     */         try {
/* 539 */           istream.close();
/*     */         } catch (InterruptedIOException ignore) {
/* 541 */           Thread.currentThread().interrupt();
/*     */         } catch (IOException ignore) {
/*     */         }
/*     */         catch (RuntimeException ignore) {
/*     */         }
/*     */     }
/* 547 */     doConfigure(props, hierarchy);
/*     */   }
/*     */ 
/*     */   protected void configureLoggerFactory(Properties props)
/*     */   {
/* 566 */     String factoryClassName = OptionConverter.findAndSubst("log4j.loggerFactory", props);
/*     */ 
/* 568 */     if (factoryClassName != null) {
/* 569 */       LogLog.debug("Setting category factory to [" + factoryClassName + "].");
/* 570 */       this.loggerFactory = ((LoggerFactory)OptionConverter.instantiateByClassName(factoryClassName, LoggerFactory.class, this.loggerFactory));
/*     */ 
/* 574 */       PropertySetter.setProperties(this.loggerFactory, props, "log4j.factory.");
/*     */     }
/*     */   }
/*     */ 
/*     */   void configureRootCategory(Properties props, LoggerRepository hierarchy)
/*     */   {
/* 602 */     String effectiveFrefix = "log4j.rootLogger";
/* 603 */     String value = OptionConverter.findAndSubst("log4j.rootLogger", props);
/*     */ 
/* 605 */     if (value == null) {
/* 606 */       value = OptionConverter.findAndSubst("log4j.rootCategory", props);
/* 607 */       effectiveFrefix = "log4j.rootCategory";
/*     */     }
/*     */ 
/* 610 */     if (value == null) {
/* 611 */       LogLog.debug("Could not find root logger information. Is this OK?");
/*     */     } else {
/* 613 */       Logger root = hierarchy.getRootLogger();
/* 614 */       synchronized (root) {
/* 615 */         parseCategory(props, root, effectiveFrefix, "root", value);
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void parseCatsAndRenderers(Properties props, LoggerRepository hierarchy)
/*     */   {
/* 626 */     Enumeration enumeration = props.propertyNames();
/* 627 */     while (enumeration.hasMoreElements()) {
/* 628 */       String key = (String)enumeration.nextElement();
/* 629 */       if ((key.startsWith("log4j.category.")) || (key.startsWith("log4j.logger."))) {
/* 630 */         String loggerName = null;
/* 631 */         if (key.startsWith("log4j.category."))
/* 632 */           loggerName = key.substring("log4j.category.".length());
/* 633 */         else if (key.startsWith("log4j.logger.")) {
/* 634 */           loggerName = key.substring("log4j.logger.".length());
/*     */         }
/* 636 */         String value = OptionConverter.findAndSubst(key, props);
/* 637 */         Logger logger = hierarchy.getLogger(loggerName, this.loggerFactory);
/* 638 */         synchronized (logger) {
/* 639 */           parseCategory(props, logger, key, loggerName, value);
/* 640 */           parseAdditivityForLogger(props, logger, loggerName);
/*     */         }
/* 642 */       } else if (key.startsWith("log4j.renderer.")) {
/* 643 */         String renderedClass = key.substring("log4j.renderer.".length());
/* 644 */         String renderingClass = OptionConverter.findAndSubst(key, props);
/* 645 */         if ((hierarchy instanceof RendererSupport)) {
/* 646 */           RendererMap.addRenderer((RendererSupport)hierarchy, renderedClass, renderingClass);
/*     */         }
/*     */       }
/* 649 */       else if ((key.equals("log4j.throwableRenderer")) && 
/* 650 */         ((hierarchy instanceof ThrowableRendererSupport))) {
/* 651 */         ThrowableRenderer tr = (ThrowableRenderer)OptionConverter.instantiateByKey(props, "log4j.throwableRenderer", ThrowableRenderer.class, null);
/*     */ 
/* 656 */         if (tr == null) {
/* 657 */           LogLog.error("Could not instantiate throwableRenderer.");
/*     */         }
/*     */         else {
/* 660 */           PropertySetter setter = new PropertySetter(tr);
/* 661 */           setter.setProperties(props, "log4j.throwableRenderer.");
/* 662 */           ((ThrowableRendererSupport)hierarchy).setThrowableRenderer(tr);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   void parseAdditivityForLogger(Properties props, Logger cat, String loggerName)
/*     */   {
/* 675 */     String value = OptionConverter.findAndSubst("log4j.additivity." + loggerName, props);
/*     */ 
/* 677 */     LogLog.debug("Handling log4j.additivity." + loggerName + "=[" + value + "]");
/*     */ 
/* 679 */     if ((value != null) && (!value.equals(""))) {
/* 680 */       boolean additivity = OptionConverter.toBoolean(value, true);
/* 681 */       LogLog.debug("Setting additivity for \"" + loggerName + "\" to " + additivity);
/*     */ 
/* 683 */       cat.setAdditivity(additivity);
/*     */     }
/*     */   }
/*     */ 
/*     */   void parseCategory(Properties props, Logger logger, String optionKey, String loggerName, String value)
/*     */   {
/* 693 */     LogLog.debug("Parsing for [" + loggerName + "] with value=[" + value + "].");
/*     */ 
/* 695 */     StringTokenizer st = new StringTokenizer(value, ",");
/*     */ 
/* 700 */     if ((!value.startsWith(",")) && (!value.equals("")))
/*     */     {
/* 703 */       if (!st.hasMoreTokens()) {
/* 704 */         return;
/*     */       }
/* 706 */       String levelStr = st.nextToken();
/* 707 */       LogLog.debug("Level token is [" + levelStr + "].");
/*     */ 
/* 712 */       if (("inherited".equalsIgnoreCase(levelStr)) || ("null".equalsIgnoreCase(levelStr)))
/*     */       {
/* 714 */         if (loggerName.equals("root"))
/* 715 */           LogLog.warn("The root logger cannot be set to null.");
/*     */         else
/* 717 */           logger.setLevel(null);
/*     */       }
/*     */       else {
/* 720 */         logger.setLevel(OptionConverter.toLevel(levelStr, Level.DEBUG));
/*     */       }
/* 722 */       LogLog.debug("Category " + loggerName + " set to " + logger.getLevel());
/*     */     }
/*     */ 
/* 726 */     logger.removeAllAppenders();
/*     */ 
/* 730 */     while (st.hasMoreTokens()) {
/* 731 */       String appenderName = st.nextToken().trim();
/* 732 */       if ((appenderName != null) && (!appenderName.equals(",")))
/*     */       {
/* 734 */         LogLog.debug("Parsing appender named \"" + appenderName + "\".");
/* 735 */         Appender appender = parseAppender(props, appenderName);
/* 736 */         if (appender != null)
/* 737 */           logger.addAppender(appender);
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   Appender parseAppender(Properties props, String appenderName) {
/* 743 */     Appender appender = registryGet(appenderName);
/* 744 */     if (appender != null) {
/* 745 */       LogLog.debug("Appender \"" + appenderName + "\" was already parsed.");
/* 746 */       return appender;
/*     */     }
/*     */ 
/* 749 */     String prefix = "log4j.appender." + appenderName;
/* 750 */     String layoutPrefix = prefix + ".layout";
/*     */ 
/* 752 */     appender = (Appender)OptionConverter.instantiateByKey(props, prefix, Appender.class, null);
/*     */ 
/* 755 */     if (appender == null) {
/* 756 */       LogLog.error("Could not instantiate appender named \"" + appenderName + "\".");
/*     */ 
/* 758 */       return null;
/*     */     }
/* 760 */     appender.setName(appenderName);
/*     */ 
/* 762 */     if ((appender instanceof OptionHandler)) {
/* 763 */       if (appender.requiresLayout()) {
/* 764 */         Layout layout = (Layout)OptionConverter.instantiateByKey(props, layoutPrefix, Layout.class, null);
/*     */ 
/* 768 */         if (layout != null) {
/* 769 */           appender.setLayout(layout);
/* 770 */           LogLog.debug("Parsing layout options for \"" + appenderName + "\".");
/*     */ 
/* 772 */           PropertySetter.setProperties(layout, props, layoutPrefix + ".");
/* 773 */           LogLog.debug("End of parsing for \"" + appenderName + "\".");
/*     */         }
/*     */       }
/* 776 */       String errorHandlerPrefix = prefix + ".errorhandler";
/* 777 */       String errorHandlerClass = OptionConverter.findAndSubst(errorHandlerPrefix, props);
/* 778 */       if (errorHandlerClass != null) {
/* 779 */         ErrorHandler eh = (ErrorHandler)OptionConverter.instantiateByKey(props, errorHandlerPrefix, ErrorHandler.class, null);
/*     */ 
/* 783 */         if (eh != null) {
/* 784 */           appender.setErrorHandler(eh);
/* 785 */           LogLog.debug("Parsing errorhandler options for \"" + appenderName + "\".");
/* 786 */           parseErrorHandler(eh, errorHandlerPrefix, props, this.repository);
/* 787 */           Properties edited = new Properties();
/* 788 */           String[] keys = { errorHandlerPrefix + "." + "root-ref", errorHandlerPrefix + "." + "logger-ref", errorHandlerPrefix + "." + "appender-ref" };
/*     */ 
/* 793 */           for (Iterator iter = props.entrySet().iterator(); iter.hasNext(); ) {
/* 794 */             Map.Entry entry = (Map.Entry)iter.next();
/* 795 */             int i = 0;
/* 796 */             while ((i < keys.length) && 
/* 797 */               (!keys[i].equals(entry.getKey()))) {
/* 796 */               i++;
/*     */             }
/*     */ 
/* 799 */             if (i == keys.length) {
/* 800 */               edited.put(entry.getKey(), entry.getValue());
/*     */             }
/*     */           }
/* 803 */           PropertySetter.setProperties(eh, edited, errorHandlerPrefix + ".");
/* 804 */           LogLog.debug("End of errorhandler parsing for \"" + appenderName + "\".");
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/* 809 */       PropertySetter.setProperties(appender, props, prefix + ".");
/* 810 */       LogLog.debug("Parsed \"" + appenderName + "\" options.");
/*     */     }
/* 812 */     parseAppenderFilters(props, appenderName, appender);
/* 813 */     registryPut(appender);
/* 814 */     return appender;
/*     */   }
/*     */ 
/*     */   private void parseErrorHandler(ErrorHandler eh, String errorHandlerPrefix, Properties props, LoggerRepository hierarchy)
/*     */   {
/* 822 */     boolean rootRef = OptionConverter.toBoolean(OptionConverter.findAndSubst(errorHandlerPrefix + "root-ref", props), false);
/*     */ 
/* 824 */     if (rootRef) {
/* 825 */       eh.setLogger(hierarchy.getRootLogger());
/*     */     }
/* 827 */     String loggerName = OptionConverter.findAndSubst(errorHandlerPrefix + "logger-ref", props);
/* 828 */     if (loggerName != null) {
/* 829 */       Logger logger = this.loggerFactory == null ? hierarchy.getLogger(loggerName) : hierarchy.getLogger(loggerName, this.loggerFactory);
/*     */ 
/* 831 */       eh.setLogger(logger);
/*     */     }
/* 833 */     String appenderName = OptionConverter.findAndSubst(errorHandlerPrefix + "appender-ref", props);
/* 834 */     if (appenderName != null) {
/* 835 */       Appender backup = parseAppender(props, appenderName);
/* 836 */       if (backup != null)
/* 837 */         eh.setBackupAppender(backup);
/*     */     }
/*     */   }
/*     */ 
/*     */   void parseAppenderFilters(Properties props, String appenderName, Appender appender)
/*     */   {
/* 847 */     String filterPrefix = "log4j.appender." + appenderName + ".filter.";
/* 848 */     int fIdx = filterPrefix.length();
/* 849 */     Hashtable filters = new Hashtable();
/* 850 */     Enumeration e = props.keys();
/* 851 */     String name = "";
/* 852 */     while (e.hasMoreElements()) {
/* 853 */       String key = (String)e.nextElement();
/* 854 */       if (key.startsWith(filterPrefix)) {
/* 855 */         int dotIdx = key.indexOf(46, fIdx);
/* 856 */         String filterKey = key;
/* 857 */         if (dotIdx != -1) {
/* 858 */           filterKey = key.substring(0, dotIdx);
/* 859 */           name = key.substring(dotIdx + 1);
/*     */         }
/* 861 */         Vector filterOpts = (Vector)filters.get(filterKey);
/* 862 */         if (filterOpts == null) {
/* 863 */           filterOpts = new Vector();
/* 864 */           filters.put(filterKey, filterOpts);
/*     */         }
/* 866 */         if (dotIdx != -1) {
/* 867 */           String value = OptionConverter.findAndSubst(key, props);
/* 868 */           filterOpts.add(new NameValue(name, value));
/*     */         }
/*     */ 
/*     */       }
/*     */ 
/*     */     }
/*     */ 
/* 875 */     Enumeration g = new SortedKeyEnumeration(filters);
/* 876 */     while (g.hasMoreElements()) {
/* 877 */       String key = (String)g.nextElement();
/* 878 */       String clazz = props.getProperty(key);
/* 879 */       if (clazz != null) {
/* 880 */         LogLog.debug("Filter key: [" + key + "] class: [" + props.getProperty(key) + "] props: " + filters.get(key));
/* 881 */         Filter filter = (Filter)OptionConverter.instantiateByClassName(clazz, Filter.class, null);
/* 882 */         if (filter != null) {
/* 883 */           PropertySetter propSetter = new PropertySetter(filter);
/* 884 */           Vector v = (Vector)filters.get(key);
/* 885 */           Enumeration filterProps = v.elements();
/* 886 */           while (filterProps.hasMoreElements()) {
/* 887 */             NameValue kv = (NameValue)filterProps.nextElement();
/* 888 */             propSetter.setProperty(kv.key, kv.value);
/*     */           }
/* 890 */           propSetter.activate();
/* 891 */           LogLog.debug("Adding filter of type [" + filter.getClass() + "] to appender named [" + appender.getName() + "].");
/*     */ 
/* 893 */           appender.addFilter(filter);
/*     */         }
/*     */       } else {
/* 896 */         LogLog.warn("Missing class definition for filter: [" + key + "]");
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   void registryPut(Appender appender)
/*     */   {
/* 903 */     this.registry.put(appender.getName(), appender);
/*     */   }
/*     */ 
/*     */   Appender registryGet(String name) {
/* 907 */     return (Appender)this.registry.get(name);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.PropertyConfigurator
 * JD-Core Version:    0.6.2
 */
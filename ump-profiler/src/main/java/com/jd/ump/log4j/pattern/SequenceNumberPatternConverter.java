/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public class SequenceNumberPatternConverter extends LoggingEventPatternConverter
/*    */ {
/* 33 */   private static final SequenceNumberPatternConverter INSTANCE = new SequenceNumberPatternConverter();
/*    */ 
/*    */   private SequenceNumberPatternConverter()
/*    */   {
/* 40 */     super("Sequence Number", "sn");
/*    */   }
/*    */ 
/*    */   public static SequenceNumberPatternConverter newInstance(String[] options)
/*    */   {
/* 50 */     return INSTANCE;
/*    */   }
/*    */ 
/*    */   public void format(LoggingEvent event, StringBuffer toAppendTo)
/*    */   {
/* 57 */     toAppendTo.append("0");
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.SequenceNumberPatternConverter
 * JD-Core Version:    0.6.2
 */
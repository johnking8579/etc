/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.io.StringReader;
/*     */ import javax.swing.AbstractAction;
/*     */ import javax.swing.JFileChooser;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.xml.parsers.ParserConfigurationException;
/*     */ import javax.xml.parsers.SAXParser;
/*     */ import javax.xml.parsers.SAXParserFactory;
/*     */ import org.xml.sax.InputSource;
/*     */ import org.xml.sax.SAXException;
/*     */ import org.xml.sax.XMLReader;
/*     */ 
/*     */ class LoadXMLAction extends AbstractAction
/*     */ {
/*  45 */   private static final Logger LOG = Logger.getLogger(LoadXMLAction.class);
/*     */   private final JFrame mParent;
/*  54 */   private final JFileChooser mChooser = new JFileChooser();
/*     */   private final XMLReader mParser;
/*     */   private final XMLFileHandler mHandler;
/*     */ 
/*     */   LoadXMLAction(JFrame aParent, MyTableModel aModel)
/*     */     throws SAXException, ParserConfigurationException
/*     */   {
/*  56 */     this.mChooser.setMultiSelectionEnabled(false);
/*  57 */     this.mChooser.setFileSelectionMode(0);
/*     */ 
/*  77 */     this.mParent = aParent;
/*  78 */     this.mHandler = new XMLFileHandler(aModel);
/*  79 */     this.mParser = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
/*  80 */     this.mParser.setContentHandler(this.mHandler);
/*     */   }
/*     */ 
/*     */   public void actionPerformed(ActionEvent aIgnore)
/*     */   {
/*  88 */     LOG.info("load file called");
/*  89 */     if (this.mChooser.showOpenDialog(this.mParent) == 0) {
/*  90 */       LOG.info("Need to load a file");
/*  91 */       File chosen = this.mChooser.getSelectedFile();
/*  92 */       LOG.info("loading the contents of " + chosen.getAbsolutePath());
/*     */       try {
/*  94 */         int num = loadFile(chosen.getAbsolutePath());
/*  95 */         JOptionPane.showMessageDialog(this.mParent, "Loaded " + num + " events.", "CHAINSAW", 1);
/*     */       }
/*     */       catch (Exception e)
/*     */       {
/* 101 */         LOG.warn("caught an exception loading the file", e);
/* 102 */         JOptionPane.showMessageDialog(this.mParent, "Error parsing file - " + e.getMessage(), "CHAINSAW", 0);
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   private int loadFile(String aFile)
/*     */     throws SAXException, IOException
/*     */   {
/* 122 */     synchronized (this.mParser)
/*     */     {
/* 124 */       StringBuffer buf = new StringBuffer();
/* 125 */       buf.append("<?xml version=\"1.0\" standalone=\"yes\"?>\n");
/* 126 */       buf.append("<!DOCTYPE log4j:eventSet ");
/* 127 */       buf.append("[<!ENTITY data SYSTEM \"file:///");
/* 128 */       buf.append(aFile);
/* 129 */       buf.append("\">]>\n");
/* 130 */       buf.append("<log4j:eventSet xmlns:log4j=\"Claira\">\n");
/* 131 */       buf.append("&data;\n");
/* 132 */       buf.append("</log4j:eventSet>\n");
/*     */ 
/* 134 */       InputSource is = new InputSource(new StringReader(buf.toString()));
/*     */ 
/* 136 */       this.mParser.parse(is);
/* 137 */       return this.mHandler.getNumEvents();
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.LoadXMLAction
 * JD-Core Version:    0.6.2
 */
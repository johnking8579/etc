/*    */ package com.jd.ump.log4j.lf5.viewer.categoryexplorer;
/*    */ 
/*    */ import com.jd.ump.log4j.lf5.LogRecord;
/*    */ import com.jd.ump.log4j.lf5.LogRecordFilter;
/*    */ import java.util.Enumeration;
/*    */ 
/*    */ public class CategoryExplorerLogRecordFilter
/*    */   implements LogRecordFilter
/*    */ {
/*    */   protected CategoryExplorerModel _model;
/*    */ 
/*    */   public CategoryExplorerLogRecordFilter(CategoryExplorerModel model)
/*    */   {
/* 52 */     this._model = model;
/*    */   }
/*    */ 
/*    */   public boolean passes(LogRecord record)
/*    */   {
/* 66 */     CategoryPath path = new CategoryPath(record.getCategory());
/* 67 */     return this._model.isCategoryPathActive(path);
/*    */   }
/*    */ 
/*    */   public void reset()
/*    */   {
/* 74 */     resetAllNodes();
/*    */   }
/*    */ 
/*    */   protected void resetAllNodes()
/*    */   {
/* 82 */     Enumeration nodes = this._model.getRootCategoryNode().depthFirstEnumeration();
/*    */ 
/* 84 */     while (nodes.hasMoreElements()) {
/* 85 */       CategoryNode current = (CategoryNode)nodes.nextElement();
/* 86 */       current.resetNumberOfContainedRecords();
/* 87 */       this._model.nodeChanged(current);
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.viewer.categoryexplorer.CategoryExplorerLogRecordFilter
 * JD-Core Version:    0.6.2
 */
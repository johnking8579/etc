/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import com.jd.ump.log4j.helpers.QuietWriter;
/*     */ import com.jd.ump.log4j.spi.ErrorHandler;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ import java.io.IOException;
/*     */ import java.io.InterruptedIOException;
/*     */ import java.io.OutputStream;
/*     */ import java.io.OutputStreamWriter;
/*     */ import java.io.Writer;
/*     */ 
/*     */ public class WriterAppender extends AppenderSkeleton
/*     */ {
/*  57 */   protected boolean immediateFlush = true;
/*     */   protected String encoding;
/*     */   protected QuietWriter qw;
/*     */ 
/*     */   public WriterAppender()
/*     */   {
/*     */   }
/*     */ 
/*     */   public WriterAppender(Layout layout, OutputStream os)
/*     */   {
/*  85 */     this(layout, new OutputStreamWriter(os));
/*     */   }
/*     */ 
/*     */   public WriterAppender(Layout layout, Writer writer)
/*     */   {
/*  96 */     this.layout = layout;
/*  97 */     setWriter(writer);
/*     */   }
/*     */ 
/*     */   public void setImmediateFlush(boolean value)
/*     */   {
/* 116 */     this.immediateFlush = value;
/*     */   }
/*     */ 
/*     */   public boolean getImmediateFlush()
/*     */   {
/* 124 */     return this.immediateFlush;
/*     */   }
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/*     */   }
/*     */ 
/*     */   public void append(LoggingEvent event)
/*     */   {
/* 159 */     if (!checkEntryConditions()) {
/* 160 */       return;
/*     */     }
/* 162 */     subAppend(event);
/*     */   }
/*     */ 
/*     */   protected boolean checkEntryConditions()
/*     */   {
/* 173 */     if (this.closed) {
/* 174 */       return false;
/*     */     }
/*     */ 
/* 177 */     if (this.qw == null) {
/* 178 */       return false;
/*     */     }
/*     */ 
/* 181 */     if (this.layout == null) {
/* 182 */       return false;
/*     */     }
/* 184 */     return true;
/*     */   }
/*     */ 
/*     */   public synchronized void close()
/*     */   {
/* 199 */     if (this.closed)
/* 200 */       return;
/* 201 */     this.closed = true;
/* 202 */     writeFooter();
/* 203 */     reset();
/*     */   }
/*     */ 
/*     */   protected void closeWriter()
/*     */   {
/* 210 */     if (this.qw != null)
/*     */       try {
/* 212 */         this.qw.close();
/*     */       } catch (IOException e) {
/* 214 */         if ((e instanceof InterruptedIOException)) {
/* 215 */           Thread.currentThread().interrupt();
/*     */         }
/*     */ 
/* 219 */         LogLog.error("Could not close " + this.qw, e);
/*     */       }
/*     */   }
/*     */ 
/*     */   protected OutputStreamWriter createWriter(OutputStream os)
/*     */   {
/* 232 */     OutputStreamWriter retval = null;
/*     */ 
/* 234 */     String enc = getEncoding();
/* 235 */     if (enc != null) {
/*     */       try {
/* 237 */         retval = new OutputStreamWriter(os, enc);
/*     */       } catch (IOException e) {
/* 239 */         if ((e instanceof InterruptedIOException)) {
/* 240 */           Thread.currentThread().interrupt();
/*     */         }
/* 242 */         LogLog.warn("Error initializing output writer.");
/* 243 */         LogLog.warn("Unsupported encoding?");
/*     */       }
/*     */     }
/* 246 */     if (retval == null) {
/* 247 */       retval = new OutputStreamWriter(os);
/*     */     }
/* 249 */     return retval;
/*     */   }
/*     */ 
/*     */   public String getEncoding() {
/* 253 */     return this.encoding;
/*     */   }
/*     */ 
/*     */   public void setEncoding(String value) {
/* 257 */     this.encoding = value;
/*     */   }
/*     */ 
/*     */   public synchronized void setErrorHandler(ErrorHandler eh)
/*     */   {
/* 267 */     if (eh == null) {
/* 268 */       LogLog.warn("You have tried to set a null error-handler.");
/*     */     } else {
/* 270 */       this.errorHandler = eh;
/* 271 */       if (this.qw != null)
/* 272 */         this.qw.setErrorHandler(eh);
/*     */     }
/*     */   }
/*     */ 
/*     */   public synchronized void setWriter(Writer writer)
/*     */   {
/* 290 */     reset();
/* 291 */     this.qw = new QuietWriter(writer, this.errorHandler);
/*     */ 
/* 293 */     writeHeader();
/*     */   }
/*     */ 
/*     */   protected void subAppend(LoggingEvent event)
/*     */   {
/* 306 */     this.qw.write(this.layout.format(event));
/*     */ 
/* 308 */     if (shouldFlush(event))
/* 309 */       this.qw.flush();
/*     */   }
/*     */ 
/*     */   public boolean requiresLayout()
/*     */   {
/* 321 */     return true;
/*     */   }
/*     */ 
/*     */   protected void reset()
/*     */   {
/* 331 */     closeWriter();
/* 332 */     this.qw = null;
/*     */   }
/*     */ 
/*     */   protected void writeFooter()
/*     */   {
/* 342 */     if (this.layout != null) {
/* 343 */       String f = this.layout.getFooter();
/* 344 */       if ((f != null) && (this.qw != null)) {
/* 345 */         this.qw.write(f);
/* 346 */         this.qw.flush();
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void writeHeader()
/*     */   {
/* 356 */     if (this.layout != null) {
/* 357 */       String h = this.layout.getHeader();
/* 358 */       if ((h != null) && (this.qw != null))
/* 359 */         this.qw.write(h);
/*     */     }
/*     */   }
/*     */ 
/*     */   protected boolean shouldFlush(LoggingEvent event)
/*     */   {
/* 370 */     return this.immediateFlush;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.WriterAppender
 * JD-Core Version:    0.6.2
 */
/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ class NameValue
/*     */ {
/*     */   String key;
/*     */   String value;
/*     */ 
/*     */   public NameValue(String key, String value)
/*     */   {
/* 930 */     this.key = key;
/* 931 */     this.value = value;
/*     */   }
/*     */   public String toString() {
/* 934 */     return this.key + "=" + this.value;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.NameValue
 * JD-Core Version:    0.6.2
 */
/*     */ package com.jd.ump.log4j;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import java.io.IOException;
/*     */ import java.io.OutputStream;
/*     */ import java.io.PrintStream;
/*     */ 
/*     */ public class ConsoleAppender extends WriterAppender
/*     */ {
/*     */   public static final String SYSTEM_OUT = "System.out";
/*     */   public static final String SYSTEM_ERR = "System.err";
/*  38 */   protected String target = "System.out";
/*     */ 
/*  44 */   private boolean follow = false;
/*     */ 
/*     */   public ConsoleAppender()
/*     */   {
/*     */   }
/*     */ 
/*     */   public ConsoleAppender(Layout layout)
/*     */   {
/*  58 */     this(layout, "System.out");
/*     */   }
/*     */ 
/*     */   public ConsoleAppender(Layout layout, String target)
/*     */   {
/*  67 */     setLayout(layout);
/*  68 */     setTarget(target);
/*  69 */     activateOptions();
/*     */   }
/*     */ 
/*     */   public void setTarget(String value)
/*     */   {
/*  79 */     String v = value.trim();
/*     */ 
/*  81 */     if ("System.out".equalsIgnoreCase(v))
/*  82 */       this.target = "System.out";
/*  83 */     else if ("System.err".equalsIgnoreCase(v))
/*  84 */       this.target = "System.err";
/*     */     else
/*  86 */       targetWarn(value);
/*     */   }
/*     */ 
/*     */   public String getTarget()
/*     */   {
/*  98 */     return this.target;
/*     */   }
/*     */ 
/*     */   public final void setFollow(boolean newValue)
/*     */   {
/* 109 */     this.follow = newValue;
/*     */   }
/*     */ 
/*     */   public final boolean getFollow()
/*     */   {
/* 120 */     return this.follow;
/*     */   }
/*     */ 
/*     */   void targetWarn(String val) {
/* 124 */     LogLog.warn("[" + val + "] should be System.out or System.err.");
/* 125 */     LogLog.warn("Using previously set target, System.out by default.");
/*     */   }
/*     */ 
/*     */   public void activateOptions()
/*     */   {
/* 132 */     if (this.follow) {
/* 133 */       if (this.target.equals("System.err"))
/* 134 */         setWriter(createWriter(new SystemErrStream()));
/*     */       else {
/* 136 */         setWriter(createWriter(new SystemOutStream()));
/*     */       }
/*     */     }
/* 139 */     else if (this.target.equals("System.err"))
/* 140 */       setWriter(createWriter(System.err));
/*     */     else {
/* 142 */       setWriter(createWriter(System.out));
/*     */     }
/*     */ 
/* 146 */     super.activateOptions();
/*     */   }
/*     */ 
/*     */   protected final void closeWriter()
/*     */   {
/* 155 */     if (this.follow)
/* 156 */       super.closeWriter();
/*     */   }
/*     */ 
/*     */   private static class SystemOutStream extends OutputStream
/*     */   {
/*     */     public void close()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void flush()
/*     */     {
/* 204 */       System.out.flush();
/*     */     }
/*     */ 
/*     */     public void write(byte[] b) throws IOException {
/* 208 */       System.out.write(b);
/*     */     }
/*     */ 
/*     */     public void write(byte[] b, int off, int len) throws IOException
/*     */     {
/* 213 */       System.out.write(b, off, len);
/*     */     }
/*     */ 
/*     */     public void write(int b) throws IOException {
/* 217 */       System.out.write(b);
/*     */     }
/*     */   }
/*     */ 
/*     */   private static class SystemErrStream extends OutputStream
/*     */   {
/*     */     public void close()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void flush()
/*     */     {
/* 174 */       System.err.flush();
/*     */     }
/*     */ 
/*     */     public void write(byte[] b) throws IOException {
/* 178 */       System.err.write(b);
/*     */     }
/*     */ 
/*     */     public void write(byte[] b, int off, int len) throws IOException
/*     */     {
/* 183 */       System.err.write(b, off, len);
/*     */     }
/*     */ 
/*     */     public void write(int b) throws IOException {
/* 187 */       System.err.write(b);
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.ConsoleAppender
 * JD-Core Version:    0.6.2
 */
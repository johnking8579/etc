/*     */ package com.jd.ump.log4j.or;
/*     */ 
/*     */ import com.jd.ump.log4j.helpers.Loader;
/*     */ import com.jd.ump.log4j.helpers.LogLog;
/*     */ import com.jd.ump.log4j.helpers.OptionConverter;
/*     */ import com.jd.ump.log4j.spi.RendererSupport;
/*     */ import java.util.Hashtable;
/*     */ 
/*     */ public class RendererMap
/*     */ {
/*     */   Hashtable map;
/*  36 */   static ObjectRenderer defaultRenderer = new DefaultRenderer();
/*     */ 
/*     */   public RendererMap()
/*     */   {
/*  40 */     this.map = new Hashtable();
/*     */   }
/*     */ 
/*     */   public static void addRenderer(RendererSupport repository, String renderedClassName, String renderingClassName)
/*     */   {
/*  50 */     LogLog.debug("Rendering class: [" + renderingClassName + "], Rendered class: [" + renderedClassName + "].");
/*     */ 
/*  52 */     ObjectRenderer renderer = (ObjectRenderer)OptionConverter.instantiateByClassName(renderingClassName, ObjectRenderer.class, null);
/*     */ 
/*  56 */     if (renderer == null) {
/*  57 */       LogLog.error("Could not instantiate renderer [" + renderingClassName + "].");
/*  58 */       return;
/*     */     }
/*     */     try {
/*  61 */       Class renderedClass = Loader.loadClass(renderedClassName);
/*  62 */       repository.setRenderer(renderedClass, renderer);
/*     */     } catch (ClassNotFoundException e) {
/*  64 */       LogLog.error("Could not find class [" + renderedClassName + "].", e);
/*     */     }
/*     */   }
/*     */ 
/*     */   public String findAndRender(Object o)
/*     */   {
/*  78 */     if (o == null) {
/*  79 */       return null;
/*     */     }
/*  81 */     return get(o.getClass()).doRender(o);
/*     */   }
/*     */ 
/*     */   public ObjectRenderer get(Object o)
/*     */   {
/*  90 */     if (o == null) {
/*  91 */       return null;
/*     */     }
/*  93 */     return get(o.getClass());
/*     */   }
/*     */ 
/*     */   public ObjectRenderer get(Class clazz)
/*     */   {
/* 149 */     ObjectRenderer r = null;
/* 150 */     for (Class c = clazz; c != null; c = c.getSuperclass())
/*     */     {
/* 152 */       r = (ObjectRenderer)this.map.get(c);
/* 153 */       if (r != null) {
/* 154 */         return r;
/*     */       }
/* 156 */       r = searchInterfaces(c);
/* 157 */       if (r != null)
/* 158 */         return r;
/*     */     }
/* 160 */     return defaultRenderer;
/*     */   }
/*     */ 
/*     */   ObjectRenderer searchInterfaces(Class c)
/*     */   {
/* 166 */     ObjectRenderer r = (ObjectRenderer)this.map.get(c);
/* 167 */     if (r != null) {
/* 168 */       return r;
/*     */     }
/* 170 */     Class[] ia = c.getInterfaces();
/* 171 */     for (int i = 0; i < ia.length; i++) {
/* 172 */       r = searchInterfaces(ia[i]);
/* 173 */       if (r != null) {
/* 174 */         return r;
/*     */       }
/*     */     }
/* 177 */     return null;
/*     */   }
/*     */ 
/*     */   public ObjectRenderer getDefaultRenderer()
/*     */   {
/* 183 */     return defaultRenderer;
/*     */   }
/*     */ 
/*     */   public void clear()
/*     */   {
/* 189 */     this.map.clear();
/*     */   }
/*     */ 
/*     */   public void put(Class clazz, ObjectRenderer or)
/*     */   {
/* 197 */     this.map.put(clazz, or);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.or.RendererMap
 * JD-Core Version:    0.6.2
 */
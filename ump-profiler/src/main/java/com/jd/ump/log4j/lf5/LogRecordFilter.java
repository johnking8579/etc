package com.jd.ump.log4j.lf5;

public abstract interface LogRecordFilter
{
  public abstract boolean passes(LogRecord paramLogRecord);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.LogRecordFilter
 * JD-Core Version:    0.6.2
 */
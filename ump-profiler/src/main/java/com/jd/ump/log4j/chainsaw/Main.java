/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.PropertyConfigurator;
/*     */ import java.awt.Container;
/*     */ import java.awt.Dimension;
/*     */ import java.awt.event.WindowAdapter;
/*     */ import java.awt.event.WindowEvent;
/*     */ import java.io.IOException;
/*     */ import java.util.Properties;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.JFrame;
/*     */ import javax.swing.JMenu;
/*     */ import javax.swing.JMenuBar;
/*     */ import javax.swing.JMenuItem;
/*     */ import javax.swing.JOptionPane;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JScrollPane;
/*     */ import javax.swing.JSplitPane;
/*     */ import javax.swing.JTable;
/*     */ 
/*     */ public class Main extends JFrame
/*     */ {
/*     */   private static final int DEFAULT_PORT = 4445;
/*     */   public static final String PORT_PROP_NAME = "chainsaw.port";
/*  55 */   private static final Logger LOG = Logger.getLogger(Main.class);
/*     */ 
/*     */   private Main()
/*     */   {
/*  62 */     super("CHAINSAW - Log4J Log Viewer");
/*     */ 
/*  64 */     MyTableModel model = new MyTableModel();
/*     */ 
/*  67 */     JMenuBar menuBar = new JMenuBar();
/*  68 */     setJMenuBar(menuBar);
/*  69 */     JMenu menu = new JMenu("File");
/*  70 */     menuBar.add(menu);
/*     */     try
/*     */     {
/*  73 */       LoadXMLAction lxa = new LoadXMLAction(this, model);
/*  74 */       JMenuItem loadMenuItem = new JMenuItem("Load file...");
/*  75 */       menu.add(loadMenuItem);
/*  76 */       loadMenuItem.addActionListener(lxa);
/*     */     } catch (NoClassDefFoundError e) {
/*  78 */       LOG.info("Missing classes for XML parser", e);
/*  79 */       JOptionPane.showMessageDialog(this, "XML parser not in classpath - unable to load XML events.", "CHAINSAW", 0);
/*     */     }
/*     */     catch (Exception e)
/*     */     {
/*  85 */       LOG.info("Unable to create the action to load XML files", e);
/*  86 */       JOptionPane.showMessageDialog(this, "Unable to create a XML parser - unable to load XML events.", "CHAINSAW", 0);
/*     */     }
/*     */ 
/*  93 */     JMenuItem exitMenuItem = new JMenuItem("Exit");
/*  94 */     menu.add(exitMenuItem);
/*  95 */     exitMenuItem.addActionListener(ExitAction.INSTANCE);
/*     */ 
/*  98 */     ControlPanel cp = new ControlPanel(model);
/*  99 */     getContentPane().add(cp, "North");
/*     */ 
/* 102 */     JTable table = new JTable(model);
/* 103 */     table.setSelectionMode(0);
/* 104 */     JScrollPane scrollPane = new JScrollPane(table);
/* 105 */     scrollPane.setBorder(BorderFactory.createTitledBorder("Events: "));
/* 106 */     scrollPane.setPreferredSize(new Dimension(900, 300));
/*     */ 
/* 109 */     JPanel details = new DetailPanel(table, model);
/* 110 */     details.setPreferredSize(new Dimension(900, 300));
/*     */ 
/* 113 */     JSplitPane jsp = new JSplitPane(0, scrollPane, details);
/*     */ 
/* 115 */     getContentPane().add(jsp, "Center");
/*     */ 
/* 117 */     addWindowListener(new WindowAdapter() {
/*     */       public void windowClosing(WindowEvent aEvent) {
/* 119 */         ExitAction.INSTANCE.actionPerformed(null);
/*     */       }
/*     */     });
/* 123 */     pack();
/* 124 */     setVisible(true);
/*     */ 
/* 126 */     setupReceiver(model);
/*     */   }
/*     */ 
/*     */   private void setupReceiver(MyTableModel aModel)
/*     */   {
/* 135 */     int port = 4445;
/* 136 */     String strRep = System.getProperty("chainsaw.port");
/* 137 */     if (strRep != null) {
/*     */       try {
/* 139 */         port = Integer.parseInt(strRep);
/*     */       } catch (NumberFormatException nfe) {
/* 141 */         LOG.fatal("Unable to parse chainsaw.port property with value " + strRep + ".");
/*     */ 
/* 143 */         JOptionPane.showMessageDialog(this, "Unable to parse port number from '" + strRep + "', quitting.", "CHAINSAW", 0);
/*     */ 
/* 149 */         System.exit(1);
/*     */       }
/*     */     }
/*     */     try
/*     */     {
/* 154 */       LoggingReceiver lr = new LoggingReceiver(aModel, port);
/* 155 */       lr.start();
/*     */     } catch (IOException e) {
/* 157 */       LOG.fatal("Unable to connect to socket server, quiting", e);
/* 158 */       JOptionPane.showMessageDialog(this, "Unable to create socket on port " + port + ", quitting.", "CHAINSAW", 0);
/*     */ 
/* 163 */       System.exit(1);
/*     */     }
/*     */   }
/*     */ 
/*     */   private static void initLog4J()
/*     */   {
/* 175 */     Properties props = new Properties();
/* 176 */     props.setProperty("log4j.rootLogger", "DEBUG, A1");
/* 177 */     props.setProperty("log4j.appender.A1", "com.jd.ump.log4j.ConsoleAppender");
/*     */ 
/* 179 */     props.setProperty("log4j.appender.A1.layout", "com.jd.ump.log4j.TTCCLayout");
/*     */ 
/* 181 */     PropertyConfigurator.configure(props);
/*     */   }
/*     */ 
/*     */   public static void main(String[] aArgs)
/*     */   {
/* 190 */     initLog4J();
/* 191 */     new Main();
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.Main
 * JD-Core Version:    0.6.2
 */
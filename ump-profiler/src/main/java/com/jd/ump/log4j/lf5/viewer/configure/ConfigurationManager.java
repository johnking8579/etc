/*     */ package com.jd.ump.log4j.lf5.viewer.configure;
/*     */ 
/*     */ import com.jd.ump.log4j.lf5.LogLevel;
/*     */ import com.jd.ump.log4j.lf5.LogLevelFormatException;
/*     */ import com.jd.ump.log4j.lf5.viewer.LogBrokerMonitor;
/*     */ import com.jd.ump.log4j.lf5.viewer.LogTable;
/*     */ import com.jd.ump.log4j.lf5.viewer.LogTableColumn;
/*     */ import com.jd.ump.log4j.lf5.viewer.LogTableColumnFormatException;
/*     */ import com.jd.ump.log4j.lf5.viewer.categoryexplorer.CategoryExplorerModel;
/*     */ import com.jd.ump.log4j.lf5.viewer.categoryexplorer.CategoryExplorerTree;
/*     */ import com.jd.ump.log4j.lf5.viewer.categoryexplorer.CategoryNode;
/*     */ import com.jd.ump.log4j.lf5.viewer.categoryexplorer.CategoryPath;
/*     */ import java.awt.Color;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.io.PrintWriter;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ import javax.swing.JCheckBoxMenuItem;
/*     */ import javax.swing.tree.TreePath;
/*     */ import javax.xml.parsers.DocumentBuilder;
/*     */ import javax.xml.parsers.DocumentBuilderFactory;
/*     */ import org.w3c.dom.Document;
/*     */ import org.w3c.dom.NamedNodeMap;
/*     */ import org.w3c.dom.Node;
/*     */ import org.w3c.dom.NodeList;
/*     */ 
/*     */ public class ConfigurationManager
/*     */ {
/*     */   private static final String CONFIG_FILE_NAME = "lf5_configuration.xml";
/*     */   private static final String NAME = "name";
/*     */   private static final String PATH = "path";
/*     */   private static final String SELECTED = "selected";
/*     */   private static final String EXPANDED = "expanded";
/*     */   private static final String CATEGORY = "category";
/*     */   private static final String FIRST_CATEGORY_NAME = "Categories";
/*     */   private static final String LEVEL = "level";
/*     */   private static final String COLORLEVEL = "colorlevel";
/*     */   private static final String RED = "red";
/*     */   private static final String GREEN = "green";
/*     */   private static final String BLUE = "blue";
/*     */   private static final String COLUMN = "column";
/*     */   private static final String NDCTEXTFILTER = "searchtext";
/*  86 */   private LogBrokerMonitor _monitor = null;
/*  87 */   private LogTable _table = null;
/*     */ 
/*     */   public ConfigurationManager(LogBrokerMonitor monitor, LogTable table)
/*     */   {
/*  94 */     this._monitor = monitor;
/*  95 */     this._table = table;
/*  96 */     load();
/*     */   }
/*     */ 
/*     */   public void save()
/*     */   {
/* 103 */     CategoryExplorerModel model = this._monitor.getCategoryExplorerTree().getExplorerModel();
/* 104 */     CategoryNode root = model.getRootCategoryNode();
/*     */ 
/* 106 */     StringBuffer xml = new StringBuffer(2048);
/* 107 */     openXMLDocument(xml);
/* 108 */     openConfigurationXML(xml);
/* 109 */     processLogRecordFilter(this._monitor.getNDCTextFilter(), xml);
/* 110 */     processLogLevels(this._monitor.getLogLevelMenuItems(), xml);
/* 111 */     processLogLevelColors(this._monitor.getLogLevelMenuItems(), LogLevel.getLogLevelColorMap(), xml);
/*     */ 
/* 113 */     processLogTableColumns(LogTableColumn.getLogTableColumns(), xml);
/* 114 */     processConfigurationNode(root, xml);
/* 115 */     closeConfigurationXML(xml);
/* 116 */     store(xml.toString());
/*     */   }
/*     */ 
/*     */   public void reset() {
/* 120 */     deleteConfigurationFile();
/* 121 */     collapseTree();
/* 122 */     selectAllNodes();
/*     */   }
/*     */ 
/*     */   public static String treePathToString(TreePath path)
/*     */   {
/* 127 */     StringBuffer sb = new StringBuffer();
/* 128 */     CategoryNode n = null;
/* 129 */     Object[] objects = path.getPath();
/* 130 */     for (int i = 1; i < objects.length; i++) {
/* 131 */       n = (CategoryNode)objects[i];
/* 132 */       if (i > 1) {
/* 133 */         sb.append(".");
/*     */       }
/* 135 */       sb.append(n.getTitle());
/*     */     }
/* 137 */     return sb.toString();
/*     */   }
/*     */ 
/*     */   protected void load()
/*     */   {
/* 144 */     File file = new File(getFilename());
/* 145 */     if (file.exists())
/*     */       try {
/* 147 */         DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
/*     */ 
/* 149 */         DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
/* 150 */         Document doc = docBuilder.parse(file);
/* 151 */         processRecordFilter(doc);
/* 152 */         processCategories(doc);
/* 153 */         processLogLevels(doc);
/* 154 */         processLogLevelColors(doc);
/* 155 */         processLogTableColumns(doc);
/*     */       }
/*     */       catch (Exception e)
/*     */       {
/* 159 */         System.err.println("Unable process configuration file at " + getFilename() + ". Error Message=" + e.getMessage());
/*     */       }
/*     */   }
/*     */ 
/*     */   protected void processRecordFilter(Document doc)
/*     */   {
/* 172 */     NodeList nodeList = doc.getElementsByTagName("searchtext");
/*     */ 
/* 175 */     Node n = nodeList.item(0);
/*     */ 
/* 178 */     if (n == null) {
/* 179 */       return;
/*     */     }
/*     */ 
/* 182 */     NamedNodeMap map = n.getAttributes();
/* 183 */     String text = getValue(map, "name");
/*     */ 
/* 185 */     if ((text == null) || (text.equals(""))) {
/* 186 */       return;
/*     */     }
/* 188 */     this._monitor.setNDCLogRecordFilter(text);
/*     */   }
/*     */ 
/*     */   protected void processCategories(Document doc) {
/* 192 */     CategoryExplorerTree tree = this._monitor.getCategoryExplorerTree();
/* 193 */     CategoryExplorerModel model = tree.getExplorerModel();
/* 194 */     NodeList nodeList = doc.getElementsByTagName("category");
/*     */ 
/* 197 */     NamedNodeMap map = nodeList.item(0).getAttributes();
/* 198 */     int j = getValue(map, "name").equalsIgnoreCase("Categories") ? 1 : 0;
/*     */ 
/* 201 */     for (int i = nodeList.getLength() - 1; i >= j; i--) {
/* 202 */       Node n = nodeList.item(i);
/* 203 */       map = n.getAttributes();
/* 204 */       CategoryNode chnode = model.addCategory(new CategoryPath(getValue(map, "path")));
/* 205 */       chnode.setSelected(getValue(map, "selected").equalsIgnoreCase("true"));
/* 206 */       if (getValue(map, "expanded").equalsIgnoreCase("true"));
/* 207 */       tree.expandPath(model.getTreePathToRoot(chnode));
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void processLogLevels(Document doc)
/*     */   {
/* 213 */     NodeList nodeList = doc.getElementsByTagName("level");
/* 214 */     Map menuItems = this._monitor.getLogLevelMenuItems();
/*     */ 
/* 216 */     for (int i = 0; i < nodeList.getLength(); i++) {
/* 217 */       Node n = nodeList.item(i);
/* 218 */       NamedNodeMap map = n.getAttributes();
/* 219 */       String name = getValue(map, "name");
/*     */       try {
/* 221 */         JCheckBoxMenuItem item = (JCheckBoxMenuItem)menuItems.get(LogLevel.valueOf(name));
/*     */ 
/* 223 */         item.setSelected(getValue(map, "selected").equalsIgnoreCase("true"));
/*     */       }
/*     */       catch (LogLevelFormatException e) {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void processLogLevelColors(Document doc) {
/* 231 */     NodeList nodeList = doc.getElementsByTagName("colorlevel");
/* 232 */     LogLevel.getLogLevelColorMap();
/*     */ 
/* 234 */     for (int i = 0; i < nodeList.getLength(); i++) {
/* 235 */       Node n = nodeList.item(i);
/*     */ 
/* 238 */       if (n == null) {
/* 239 */         return;
/*     */       }
/*     */ 
/* 242 */       NamedNodeMap map = n.getAttributes();
/* 243 */       String name = getValue(map, "name");
/*     */       try {
/* 245 */         LogLevel level = LogLevel.valueOf(name);
/* 246 */         int red = Integer.parseInt(getValue(map, "red"));
/* 247 */         int green = Integer.parseInt(getValue(map, "green"));
/* 248 */         int blue = Integer.parseInt(getValue(map, "blue"));
/* 249 */         Color c = new Color(red, green, blue);
/* 250 */         if (level != null)
/* 251 */           level.setLogLevelColorMap(level, c);
/*     */       }
/*     */       catch (LogLevelFormatException e)
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void processLogTableColumns(Document doc)
/*     */   {
/* 261 */     NodeList nodeList = doc.getElementsByTagName("column");
/* 262 */     Map menuItems = this._monitor.getLogTableColumnMenuItems();
/* 263 */     List selectedColumns = new ArrayList();
/* 264 */     for (int i = 0; i < nodeList.getLength(); i++) {
/* 265 */       Node n = nodeList.item(i);
/*     */ 
/* 268 */       if (n == null) {
/* 269 */         return;
/*     */       }
/* 271 */       NamedNodeMap map = n.getAttributes();
/* 272 */       String name = getValue(map, "name");
/*     */       try {
/* 274 */         LogTableColumn column = LogTableColumn.valueOf(name);
/* 275 */         JCheckBoxMenuItem item = (JCheckBoxMenuItem)menuItems.get(column);
/*     */ 
/* 277 */         item.setSelected(getValue(map, "selected").equalsIgnoreCase("true"));
/*     */ 
/* 279 */         if (item.isSelected()) {
/* 280 */           selectedColumns.add(column);
/*     */         }
/*     */       }
/*     */       catch (LogTableColumnFormatException e)
/*     */       {
/*     */       }
/* 286 */       if (selectedColumns.isEmpty())
/* 287 */         this._table.setDetailedView();
/*     */       else
/* 289 */         this._table.setView(selectedColumns);
/*     */     }
/*     */   }
/*     */ 
/*     */   protected String getValue(NamedNodeMap map, String attr)
/*     */   {
/* 296 */     Node n = map.getNamedItem(attr);
/* 297 */     return n.getNodeValue();
/*     */   }
/*     */ 
/*     */   protected void collapseTree()
/*     */   {
/* 302 */     CategoryExplorerTree tree = this._monitor.getCategoryExplorerTree();
/* 303 */     for (int i = tree.getRowCount() - 1; i > 0; i--)
/* 304 */       tree.collapseRow(i);
/*     */   }
/*     */ 
/*     */   protected void selectAllNodes()
/*     */   {
/* 309 */     CategoryExplorerModel model = this._monitor.getCategoryExplorerTree().getExplorerModel();
/* 310 */     CategoryNode root = model.getRootCategoryNode();
/* 311 */     Enumeration all = root.breadthFirstEnumeration();
/* 312 */     CategoryNode n = null;
/* 313 */     while (all.hasMoreElements()) {
/* 314 */       n = (CategoryNode)all.nextElement();
/* 315 */       n.setSelected(true);
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void store(String s)
/*     */   {
/*     */     try {
/* 322 */       PrintWriter writer = new PrintWriter(new FileWriter(getFilename()));
/* 323 */       writer.print(s);
/* 324 */       writer.close();
/*     */     }
/*     */     catch (IOException e) {
/* 327 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ 
/*     */   protected void deleteConfigurationFile()
/*     */   {
/*     */     try {
/* 334 */       File f = new File(getFilename());
/* 335 */       if (f.exists())
/* 336 */         f.delete();
/*     */     }
/*     */     catch (SecurityException e) {
/* 339 */       System.err.println("Cannot delete " + getFilename() + " because a security violation occured.");
/*     */     }
/*     */   }
/*     */ 
/*     */   protected String getFilename()
/*     */   {
/* 345 */     String home = System.getProperty("user.home");
/* 346 */     String sep = System.getProperty("file.separator");
/*     */ 
/* 348 */     return home + sep + "lf5" + sep + "lf5_configuration.xml";
/*     */   }
/*     */ 
/*     */   private void processConfigurationNode(CategoryNode node, StringBuffer xml)
/*     */   {
/* 355 */     CategoryExplorerModel model = this._monitor.getCategoryExplorerTree().getExplorerModel();
/*     */ 
/* 357 */     Enumeration all = node.breadthFirstEnumeration();
/* 358 */     CategoryNode n = null;
/* 359 */     while (all.hasMoreElements()) {
/* 360 */       n = (CategoryNode)all.nextElement();
/* 361 */       exportXMLElement(n, model.getTreePathToRoot(n), xml);
/*     */     }
/*     */   }
/*     */ 
/*     */   private void processLogLevels(Map logLevelMenuItems, StringBuffer xml)
/*     */   {
/* 367 */     xml.append("\t<loglevels>\r\n");
/* 368 */     Iterator it = logLevelMenuItems.keySet().iterator();
/* 369 */     while (it.hasNext()) {
/* 370 */       LogLevel level = (LogLevel)it.next();
/* 371 */       JCheckBoxMenuItem item = (JCheckBoxMenuItem)logLevelMenuItems.get(level);
/* 372 */       exportLogLevelXMLElement(level.getLabel(), item.isSelected(), xml);
/*     */     }
/*     */ 
/* 375 */     xml.append("\t</loglevels>\r\n");
/*     */   }
/*     */ 
/*     */   private void processLogLevelColors(Map logLevelMenuItems, Map logLevelColors, StringBuffer xml) {
/* 379 */     xml.append("\t<loglevelcolors>\r\n");
/*     */ 
/* 381 */     Iterator it = logLevelMenuItems.keySet().iterator();
/* 382 */     while (it.hasNext()) {
/* 383 */       LogLevel level = (LogLevel)it.next();
/*     */ 
/* 385 */       Color color = (Color)logLevelColors.get(level);
/* 386 */       exportLogLevelColorXMLElement(level.getLabel(), color, xml);
/*     */     }
/*     */ 
/* 389 */     xml.append("\t</loglevelcolors>\r\n");
/*     */   }
/*     */ 
/*     */   private void processLogTableColumns(List logTableColumnMenuItems, StringBuffer xml)
/*     */   {
/* 394 */     xml.append("\t<logtablecolumns>\r\n");
/* 395 */     Iterator it = logTableColumnMenuItems.iterator();
/* 396 */     while (it.hasNext()) {
/* 397 */       LogTableColumn column = (LogTableColumn)it.next();
/* 398 */       JCheckBoxMenuItem item = this._monitor.getTableColumnMenuItem(column);
/* 399 */       exportLogTableColumnXMLElement(column.getLabel(), item.isSelected(), xml);
/*     */     }
/*     */ 
/* 402 */     xml.append("\t</logtablecolumns>\r\n");
/*     */   }
/*     */ 
/*     */   private void processLogRecordFilter(String text, StringBuffer xml)
/*     */   {
/* 408 */     xml.append("\t<").append("searchtext").append(" ");
/* 409 */     xml.append("name").append("=\"").append(text).append("\"");
/* 410 */     xml.append("/>\r\n");
/*     */   }
/*     */ 
/*     */   private void openXMLDocument(StringBuffer xml) {
/* 414 */     xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n");
/*     */   }
/*     */ 
/*     */   private void openConfigurationXML(StringBuffer xml) {
/* 418 */     xml.append("<configuration>\r\n");
/*     */   }
/*     */ 
/*     */   private void closeConfigurationXML(StringBuffer xml) {
/* 422 */     xml.append("</configuration>\r\n");
/*     */   }
/*     */ 
/*     */   private void exportXMLElement(CategoryNode node, TreePath path, StringBuffer xml) {
/* 426 */     CategoryExplorerTree tree = this._monitor.getCategoryExplorerTree();
/*     */ 
/* 428 */     xml.append("\t<").append("category").append(" ");
/* 429 */     xml.append("name").append("=\"").append(node.getTitle()).append("\" ");
/* 430 */     xml.append("path").append("=\"").append(treePathToString(path)).append("\" ");
/* 431 */     xml.append("expanded").append("=\"").append(tree.isExpanded(path)).append("\" ");
/* 432 */     xml.append("selected").append("=\"").append(node.isSelected()).append("\"/>\r\n");
/*     */   }
/*     */ 
/*     */   private void exportLogLevelXMLElement(String label, boolean selected, StringBuffer xml) {
/* 436 */     xml.append("\t\t<").append("level").append(" ").append("name");
/* 437 */     xml.append("=\"").append(label).append("\" ");
/* 438 */     xml.append("selected").append("=\"").append(selected);
/* 439 */     xml.append("\"/>\r\n");
/*     */   }
/*     */ 
/*     */   private void exportLogLevelColorXMLElement(String label, Color color, StringBuffer xml) {
/* 443 */     xml.append("\t\t<").append("colorlevel").append(" ").append("name");
/* 444 */     xml.append("=\"").append(label).append("\" ");
/* 445 */     xml.append("red").append("=\"").append(color.getRed()).append("\" ");
/* 446 */     xml.append("green").append("=\"").append(color.getGreen()).append("\" ");
/* 447 */     xml.append("blue").append("=\"").append(color.getBlue());
/* 448 */     xml.append("\"/>\r\n");
/*     */   }
/*     */ 
/*     */   private void exportLogTableColumnXMLElement(String label, boolean selected, StringBuffer xml) {
/* 452 */     xml.append("\t\t<").append("column").append(" ").append("name");
/* 453 */     xml.append("=\"").append(label).append("\" ");
/* 454 */     xml.append("selected").append("=\"").append(selected);
/* 455 */     xml.append("\"/>\r\n");
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.viewer.configure.ConfigurationManager
 * JD-Core Version:    0.6.2
 */
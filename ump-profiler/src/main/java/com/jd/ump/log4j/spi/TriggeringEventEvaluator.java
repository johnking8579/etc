package com.jd.ump.log4j.spi;

public abstract interface TriggeringEventEvaluator
{
  public abstract boolean isTriggeringEvent(LoggingEvent paramLoggingEvent);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.TriggeringEventEvaluator
 * JD-Core Version:    0.6.2
 */
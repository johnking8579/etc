package com.jd.ump.log4j.spi;

import java.net.URL;

public abstract interface Configurator
{
  public static final String INHERITED = "inherited";
  public static final String NULL = "null";

  public abstract void doConfigure(URL paramURL, LoggerRepository paramLoggerRepository);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.Configurator
 * JD-Core Version:    0.6.2
 */
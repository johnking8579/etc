package com.jd.ump.log4j.or;

public abstract interface ObjectRenderer
{
  public abstract String doRender(Object paramObject);
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.or.ObjectRenderer
 * JD-Core Version:    0.6.2
 */
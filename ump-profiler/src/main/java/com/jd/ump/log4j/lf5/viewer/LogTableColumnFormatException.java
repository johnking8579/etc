/*    */ package com.jd.ump.log4j.lf5.viewer;
/*    */ 
/*    */ public class LogTableColumnFormatException extends Exception
/*    */ {
/*    */   private static final long serialVersionUID = 6529165785030431653L;
/*    */ 
/*    */   public LogTableColumnFormatException(String message)
/*    */   {
/* 49 */     super(message);
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.viewer.LogTableColumnFormatException
 * JD-Core Version:    0.6.2
 */
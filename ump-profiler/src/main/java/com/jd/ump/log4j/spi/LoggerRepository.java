package com.jd.ump.log4j.spi;

import com.jd.ump.log4j.Appender;
import com.jd.ump.log4j.Category;
import com.jd.ump.log4j.Level;
import com.jd.ump.log4j.Logger;
import java.util.Enumeration;

public abstract interface LoggerRepository
{
  public abstract void addHierarchyEventListener(HierarchyEventListener paramHierarchyEventListener);

  public abstract boolean isDisabled(int paramInt);

  public abstract void setThreshold(Level paramLevel);

  public abstract void setThreshold(String paramString);

  public abstract void emitNoAppenderWarning(Category paramCategory);

  public abstract Level getThreshold();

  public abstract Logger getLogger(String paramString);

  public abstract Logger getLogger(String paramString, LoggerFactory paramLoggerFactory);

  public abstract Logger getRootLogger();

  public abstract Logger exists(String paramString);

  public abstract void shutdown();

  public abstract Enumeration getCurrentLoggers();

  public abstract Enumeration getCurrentCategories();

  public abstract void fireAddAppenderEvent(Category paramCategory, Appender paramAppender);

  public abstract void resetConfiguration();
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.LoggerRepository
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.log4j.pattern;
/*    */ 
/*    */ import com.jd.ump.log4j.helpers.LogLog;
/*    */ import com.jd.ump.log4j.helpers.MDCKeySetExtractor;
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ import java.util.Iterator;
/*    */ import java.util.Set;
/*    */ 
/*    */ public final class PropertiesPatternConverter extends LoggingEventPatternConverter
/*    */ {
/*    */   private final String option;
/*    */ 
/*    */   private PropertiesPatternConverter(String[] options)
/*    */   {
/* 51 */     super((options != null) && (options.length > 0) ? "Property{" + options[0] + "}" : "Properties", "property");
/*    */ 
/* 55 */     if ((options != null) && (options.length > 0))
/* 56 */       this.option = options[0];
/*    */     else
/* 58 */       this.option = null;
/*    */   }
/*    */ 
/*    */   public static PropertiesPatternConverter newInstance(String[] options)
/*    */   {
/* 69 */     return new PropertiesPatternConverter(options);
/*    */   }
/*    */ 
/*    */   public void format(LoggingEvent event, StringBuffer toAppendTo)
/*    */   {
/* 78 */     if (this.option == null) {
/* 79 */       toAppendTo.append("{");
/*    */       try
/*    */       {
/* 82 */         Set keySet = MDCKeySetExtractor.INSTANCE.getPropertyKeySet(event);
/* 83 */         if (keySet != null)
/* 84 */           for (i = keySet.iterator(); i.hasNext(); ) {
/* 85 */             Object item = i.next();
/* 86 */             Object val = event.getMDC(item.toString());
/* 87 */             toAppendTo.append("{").append(item).append(",").append(val).append("}");
/*    */           }
/*    */       }
/*    */       catch (Exception ex)
/*    */       {
/*    */         Iterator i;
/* 92 */         LogLog.error("Unexpected exception while extracting MDC keys", ex);
/*    */       }
/*    */ 
/* 95 */       toAppendTo.append("}");
/*    */     }
/*    */     else {
/* 98 */       Object val = event.getMDC(this.option);
/*    */ 
/* 100 */       if (val != null)
/* 101 */         toAppendTo.append(val);
/*    */     }
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.pattern.PropertiesPatternConverter
 * JD-Core Version:    0.6.2
 */
/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.Priority;
/*     */ import java.text.DateFormat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Comparator;
/*     */ import java.util.Date;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.SortedSet;
/*     */ import java.util.TreeSet;
/*     */ import javax.swing.table.AbstractTableModel;
/*     */ 
/*     */ class MyTableModel extends AbstractTableModel
/*     */ {
/*  43 */   private static final Logger LOG = Logger.getLogger(MyTableModel.class);
/*     */ 
/*  46 */   private static final Comparator MY_COMP = new Comparator()
/*     */   {
/*     */     public int compare(Object aObj1, Object aObj2)
/*     */     {
/*  50 */       if ((aObj1 == null) && (aObj2 == null))
/*  51 */         return 0;
/*  52 */       if (aObj1 == null)
/*  53 */         return -1;
/*  54 */       if (aObj2 == null) {
/*  55 */         return 1;
/*     */       }
/*     */ 
/*  59 */       EventDetails le1 = (EventDetails)aObj1;
/*  60 */       EventDetails le2 = (EventDetails)aObj2;
/*     */ 
/*  62 */       if (le1.getTimeStamp() < le2.getTimeStamp()) {
/*  63 */         return 1;
/*     */       }
/*     */ 
/*  66 */       return -1;
/*     */     }
/*  46 */   };
/*     */ 
/* 113 */   private static final String[] COL_NAMES = { "Time", "Priority", "Trace", "Category", "NDC", "Message" };
/*     */ 
/* 117 */   private static final EventDetails[] EMPTY_LIST = new EventDetails[0];
/*     */ 
/* 120 */   private static final DateFormat DATE_FORMATTER = DateFormat.getDateTimeInstance(3, 2);
/*     */ 
/* 124 */   private final Object mLock = new Object();
/*     */ 
/* 126 */   private final SortedSet mAllEvents = new TreeSet(MY_COMP);
/*     */ 
/* 128 */   private EventDetails[] mFilteredEvents = EMPTY_LIST;
/*     */ 
/* 130 */   private final List mPendingEvents = new ArrayList();
/*     */ 
/* 132 */   private boolean mPaused = false;
/*     */ 
/* 135 */   private String mThreadFilter = "";
/*     */ 
/* 137 */   private String mMessageFilter = "";
/*     */ 
/* 139 */   private String mNDCFilter = "";
/*     */ 
/* 141 */   private String mCategoryFilter = "";
/*     */ 
/* 143 */   private Priority mPriorityFilter = Priority.DEBUG;
/*     */ 
/*     */   MyTableModel()
/*     */   {
/* 151 */     Thread t = new Thread(new Processor(null));
/* 152 */     t.setDaemon(true);
/* 153 */     t.start();
/*     */   }
/*     */ 
/*     */   public int getRowCount()
/*     */   {
/* 163 */     synchronized (this.mLock) {
/* 164 */       return this.mFilteredEvents.length;
/*     */     }
/*     */   }
/*     */ 
/*     */   public int getColumnCount()
/*     */   {
/* 171 */     return COL_NAMES.length;
/*     */   }
/*     */ 
/*     */   public String getColumnName(int aCol)
/*     */   {
/* 177 */     return COL_NAMES[aCol];
/*     */   }
/*     */ 
/*     */   public Class getColumnClass(int aCol)
/*     */   {
/* 183 */     return aCol == 2 ? Boolean.class : Object.class;
/*     */   }
/*     */ 
/*     */   public Object getValueAt(int aRow, int aCol)
/*     */   {
/* 188 */     synchronized (this.mLock) {
/* 189 */       EventDetails event = this.mFilteredEvents[aRow];
/*     */ 
/* 191 */       if (aCol == 0)
/* 192 */         return DATE_FORMATTER.format(new Date(event.getTimeStamp()));
/* 193 */       if (aCol == 1)
/* 194 */         return event.getPriority();
/* 195 */       if (aCol == 2) {
/* 196 */         return event.getThrowableStrRep() == null ? Boolean.FALSE : Boolean.TRUE;
/*     */       }
/* 198 */       if (aCol == 3)
/* 199 */         return event.getCategoryName();
/* 200 */       if (aCol == 4) {
/* 201 */         return event.getNDC();
/*     */       }
/* 203 */       return event.getMessage();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setPriorityFilter(Priority aPriority)
/*     */   {
/* 218 */     synchronized (this.mLock) {
/* 219 */       this.mPriorityFilter = aPriority;
/* 220 */       updateFilteredEvents(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setThreadFilter(String aStr)
/*     */   {
/* 230 */     synchronized (this.mLock) {
/* 231 */       this.mThreadFilter = aStr.trim();
/* 232 */       updateFilteredEvents(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setMessageFilter(String aStr)
/*     */   {
/* 242 */     synchronized (this.mLock) {
/* 243 */       this.mMessageFilter = aStr.trim();
/* 244 */       updateFilteredEvents(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setNDCFilter(String aStr)
/*     */   {
/* 254 */     synchronized (this.mLock) {
/* 255 */       this.mNDCFilter = aStr.trim();
/* 256 */       updateFilteredEvents(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void setCategoryFilter(String aStr)
/*     */   {
/* 266 */     synchronized (this.mLock) {
/* 267 */       this.mCategoryFilter = aStr.trim();
/* 268 */       updateFilteredEvents(false);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void addEvent(EventDetails aEvent)
/*     */   {
/* 278 */     synchronized (this.mLock) {
/* 279 */       this.mPendingEvents.add(aEvent);
/*     */     }
/*     */   }
/*     */ 
/*     */   public void clear()
/*     */   {
/* 287 */     synchronized (this.mLock) {
/* 288 */       this.mAllEvents.clear();
/* 289 */       this.mFilteredEvents = new EventDetails[0];
/* 290 */       this.mPendingEvents.clear();
/* 291 */       fireTableDataChanged();
/*     */     }
/*     */   }
/*     */ 
/*     */   public void toggle()
/*     */   {
/* 297 */     synchronized (this.mLock) {
/* 298 */       this.mPaused = (!this.mPaused);
/*     */     }
/*     */   }
/*     */ 
/*     */   public boolean isPaused()
/*     */   {
/* 304 */     synchronized (this.mLock) {
/* 305 */       return this.mPaused;
/*     */     }
/*     */   }
/*     */ 
/*     */   public EventDetails getEventDetails(int aRow)
/*     */   {
/* 316 */     synchronized (this.mLock) {
/* 317 */       return this.mFilteredEvents[aRow];
/*     */     }
/*     */   }
/*     */ 
/*     */   private void updateFilteredEvents(boolean aInsertedToFront)
/*     */   {
/* 332 */     long start = System.currentTimeMillis();
/* 333 */     List filtered = new ArrayList();
/* 334 */     int size = this.mAllEvents.size();
/* 335 */     Iterator it = this.mAllEvents.iterator();
/*     */ 
/* 337 */     while (it.hasNext()) {
/* 338 */       EventDetails event = (EventDetails)it.next();
/* 339 */       if (matchFilter(event)) {
/* 340 */         filtered.add(event);
/*     */       }
/*     */     }
/*     */ 
/* 344 */     EventDetails lastFirst = this.mFilteredEvents.length == 0 ? null : this.mFilteredEvents[0];
/*     */ 
/* 347 */     this.mFilteredEvents = ((EventDetails[])filtered.toArray(EMPTY_LIST));
/*     */ 
/* 349 */     if ((aInsertedToFront) && (lastFirst != null)) {
/* 350 */       int index = filtered.indexOf(lastFirst);
/* 351 */       if (index < 1) {
/* 352 */         LOG.warn("In strange state");
/* 353 */         fireTableDataChanged();
/*     */       } else {
/* 355 */         fireTableRowsInserted(0, index - 1);
/*     */       }
/*     */     } else {
/* 358 */       fireTableDataChanged();
/*     */     }
/*     */ 
/* 361 */     long end = System.currentTimeMillis();
/* 362 */     LOG.debug("Total time [ms]: " + (end - start) + " in update, size: " + size);
/*     */   }
/*     */ 
/*     */   private boolean matchFilter(EventDetails aEvent)
/*     */   {
/* 373 */     if ((aEvent.getPriority().isGreaterOrEqual(this.mPriorityFilter)) && (aEvent.getThreadName().indexOf(this.mThreadFilter) >= 0) && (aEvent.getCategoryName().indexOf(this.mCategoryFilter) >= 0) && ((this.mNDCFilter.length() == 0) || ((aEvent.getNDC() != null) && (aEvent.getNDC().indexOf(this.mNDCFilter) >= 0))))
/*     */     {
/* 380 */       String rm = aEvent.getMessage();
/* 381 */       if (rm == null)
/*     */       {
/* 383 */         return this.mMessageFilter.length() == 0;
/*     */       }
/* 385 */       return rm.indexOf(this.mMessageFilter) >= 0;
/*     */     }
/*     */ 
/* 389 */     return false;
/*     */   }
/*     */ 
/*     */   private class Processor
/*     */     implements Runnable
/*     */   {
/*     */     private Processor()
/*     */     {
/*     */     }
/*     */ 
/*     */     public void run()
/*     */     {
/*     */       while (true)
/*     */       {
/*     */         try
/*     */         {
/*  81 */           Thread.sleep(1000L);
/*     */         }
/*     */         catch (InterruptedException e)
/*     */         {
/*     */         }
/*  86 */         synchronized (MyTableModel.this.mLock) {
/*  87 */           if (!MyTableModel.this.mPaused)
/*     */           {
/*  91 */             boolean toHead = true;
/*  92 */             boolean needUpdate = false;
/*  93 */             Iterator it = MyTableModel.this.mPendingEvents.iterator();
/*  94 */             while (it.hasNext()) {
/*  95 */               EventDetails event = (EventDetails)it.next();
/*  96 */               MyTableModel.this.mAllEvents.add(event);
/*  97 */               toHead = (toHead) && (event == MyTableModel.this.mAllEvents.first());
/*  98 */               needUpdate = (needUpdate) || (MyTableModel.this.matchFilter(event));
/*     */             }
/* 100 */             MyTableModel.this.mPendingEvents.clear();
/*     */ 
/* 102 */             if (needUpdate)
/* 103 */               MyTableModel.this.updateFilteredEvents(toHead);
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.MyTableModel
 * JD-Core Version:    0.6.2
 */
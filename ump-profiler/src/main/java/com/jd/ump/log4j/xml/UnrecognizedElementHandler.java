package com.jd.ump.log4j.xml;

import java.util.Properties;
import org.w3c.dom.Element;

public abstract interface UnrecognizedElementHandler
{
  public abstract boolean parseUnrecognizedElement(Element paramElement, Properties paramProperties)
    throws Exception;
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.xml.UnrecognizedElementHandler
 * JD-Core Version:    0.6.2
 */
/*     */ package com.jd.ump.log4j.chainsaw;
/*     */ 
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.Logger;
/*     */ import com.jd.ump.log4j.Priority;
/*     */ import java.awt.GridBagConstraints;
/*     */ import java.awt.GridBagLayout;
/*     */ import java.awt.event.ActionEvent;
/*     */ import java.awt.event.ActionListener;
/*     */ import javax.swing.BorderFactory;
/*     */ import javax.swing.JButton;
/*     */ import javax.swing.JComboBox;
/*     */ import javax.swing.JLabel;
/*     */ import javax.swing.JPanel;
/*     */ import javax.swing.JTextField;
/*     */ import javax.swing.event.DocumentEvent;
/*     */ import javax.swing.event.DocumentListener;
/*     */ import javax.swing.text.Document;
/*     */ 
/*     */ class ControlPanel extends JPanel
/*     */ {
/*  43 */   private static final Logger LOG = Logger.getLogger(ControlPanel.class);
/*     */ 
/*     */   ControlPanel(final MyTableModel aModel)
/*     */   {
/*  52 */     setBorder(BorderFactory.createTitledBorder("Controls: "));
/*  53 */     GridBagLayout gridbag = new GridBagLayout();
/*  54 */     GridBagConstraints c = new GridBagConstraints();
/*  55 */     setLayout(gridbag);
/*     */ 
/*  58 */     c.ipadx = 5;
/*  59 */     c.ipady = 5;
/*     */ 
/*  62 */     c.gridx = 0;
/*  63 */     c.anchor = 13;
/*     */ 
/*  65 */     c.gridy = 0;
/*  66 */     JLabel label = new JLabel("Filter Level:");
/*  67 */     gridbag.setConstraints(label, c);
/*  68 */     add(label);
/*     */ 
/*  70 */     c.gridy += 1;
/*  71 */     label = new JLabel("Filter Thread:");
/*  72 */     gridbag.setConstraints(label, c);
/*  73 */     add(label);
/*     */ 
/*  75 */     c.gridy += 1;
/*  76 */     label = new JLabel("Filter Logger:");
/*  77 */     gridbag.setConstraints(label, c);
/*  78 */     add(label);
/*     */ 
/*  80 */     c.gridy += 1;
/*  81 */     label = new JLabel("Filter NDC:");
/*  82 */     gridbag.setConstraints(label, c);
/*  83 */     add(label);
/*     */ 
/*  85 */     c.gridy += 1;
/*  86 */     label = new JLabel("Filter Message:");
/*  87 */     gridbag.setConstraints(label, c);
/*  88 */     add(label);
/*     */ 
/*  91 */     c.weightx = 1.0D;
/*     */ 
/*  93 */     c.gridx = 1;
/*  94 */     c.anchor = 17;
/*     */ 
/*  96 */     c.gridy = 0;
/*  97 */     Level[] allPriorities = { Level.FATAL, Level.ERROR, Level.WARN, Level.INFO, Level.DEBUG, Level.TRACE };
/*     */ 
/* 104 */     final JComboBox priorities = new JComboBox(allPriorities);
/* 105 */     Level lowest = allPriorities[(allPriorities.length - 1)];
/* 106 */     priorities.setSelectedItem(lowest);
/* 107 */     aModel.setPriorityFilter(lowest);
/* 108 */     gridbag.setConstraints(priorities, c);
/* 109 */     add(priorities);
/* 110 */     priorities.setEditable(false);
/* 111 */     priorities.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent aEvent) {
/* 113 */         aModel.setPriorityFilter((Priority)priorities.getSelectedItem());
/*     */       }
/*     */     });
/* 119 */     c.fill = 2;
/* 120 */     c.gridy += 1;
/* 121 */     final JTextField threadField = new JTextField("");
/* 122 */     threadField.getDocument().addDocumentListener(new DocumentListener() {
/*     */       public void insertUpdate(DocumentEvent aEvent) {
/* 124 */         aModel.setThreadFilter(threadField.getText());
/*     */       }
/*     */       public void removeUpdate(DocumentEvent aEvente) {
/* 127 */         aModel.setThreadFilter(threadField.getText());
/*     */       }
/*     */       public void changedUpdate(DocumentEvent aEvent) {
/* 130 */         aModel.setThreadFilter(threadField.getText());
/*     */       }
/*     */     });
/* 133 */     gridbag.setConstraints(threadField, c);
/* 134 */     add(threadField);
/*     */ 
/* 136 */     c.gridy += 1;
/* 137 */     final JTextField catField = new JTextField("");
/* 138 */     catField.getDocument().addDocumentListener(new DocumentListener() {
/*     */       public void insertUpdate(DocumentEvent aEvent) {
/* 140 */         aModel.setCategoryFilter(catField.getText());
/*     */       }
/*     */       public void removeUpdate(DocumentEvent aEvent) {
/* 143 */         aModel.setCategoryFilter(catField.getText());
/*     */       }
/*     */       public void changedUpdate(DocumentEvent aEvent) {
/* 146 */         aModel.setCategoryFilter(catField.getText());
/*     */       }
/*     */     });
/* 149 */     gridbag.setConstraints(catField, c);
/* 150 */     add(catField);
/*     */ 
/* 152 */     c.gridy += 1;
/* 153 */     final JTextField ndcField = new JTextField("");
/* 154 */     ndcField.getDocument().addDocumentListener(new DocumentListener() {
/*     */       public void insertUpdate(DocumentEvent aEvent) {
/* 156 */         aModel.setNDCFilter(ndcField.getText());
/*     */       }
/*     */       public void removeUpdate(DocumentEvent aEvent) {
/* 159 */         aModel.setNDCFilter(ndcField.getText());
/*     */       }
/*     */       public void changedUpdate(DocumentEvent aEvent) {
/* 162 */         aModel.setNDCFilter(ndcField.getText());
/*     */       }
/*     */     });
/* 165 */     gridbag.setConstraints(ndcField, c);
/* 166 */     add(ndcField);
/*     */ 
/* 168 */     c.gridy += 1;
/* 169 */     final JTextField msgField = new JTextField("");
/* 170 */     msgField.getDocument().addDocumentListener(new DocumentListener() {
/*     */       public void insertUpdate(DocumentEvent aEvent) {
/* 172 */         aModel.setMessageFilter(msgField.getText());
/*     */       }
/*     */       public void removeUpdate(DocumentEvent aEvent) {
/* 175 */         aModel.setMessageFilter(msgField.getText());
/*     */       }
/*     */       public void changedUpdate(DocumentEvent aEvent) {
/* 178 */         aModel.setMessageFilter(msgField.getText());
/*     */       }
/*     */     });
/* 183 */     gridbag.setConstraints(msgField, c);
/* 184 */     add(msgField);
/*     */ 
/* 187 */     c.weightx = 0.0D;
/* 188 */     c.fill = 2;
/* 189 */     c.anchor = 13;
/* 190 */     c.gridx = 2;
/*     */ 
/* 192 */     c.gridy = 0;
/* 193 */     JButton exitButton = new JButton("Exit");
/* 194 */     exitButton.setMnemonic('x');
/* 195 */     exitButton.addActionListener(ExitAction.INSTANCE);
/* 196 */     gridbag.setConstraints(exitButton, c);
/* 197 */     add(exitButton);
/*     */ 
/* 199 */     c.gridy += 1;
/* 200 */     JButton clearButton = new JButton("Clear");
/* 201 */     clearButton.setMnemonic('c');
/* 202 */     clearButton.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent aEvent) {
/* 204 */         aModel.clear();
/*     */       }
/*     */     });
/* 207 */     gridbag.setConstraints(clearButton, c);
/* 208 */     add(clearButton);
/*     */ 
/* 210 */     c.gridy += 1;
/* 211 */     final JButton toggleButton = new JButton("Pause");
/* 212 */     toggleButton.setMnemonic('p');
/* 213 */     toggleButton.addActionListener(new ActionListener() {
/*     */       public void actionPerformed(ActionEvent aEvent) {
/* 215 */         aModel.toggle();
/* 216 */         toggleButton.setText(aModel.isPaused() ? "Resume" : "Pause");
/*     */       }
/*     */     });
/* 220 */     gridbag.setConstraints(toggleButton, c);
/* 221 */     add(toggleButton);
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.chainsaw.ControlPanel
 * JD-Core Version:    0.6.2
 */
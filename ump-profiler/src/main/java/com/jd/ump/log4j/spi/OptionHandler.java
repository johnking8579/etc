package com.jd.ump.log4j.spi;

public abstract interface OptionHandler
{
  public abstract void activateOptions();
}

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.spi.OptionHandler
 * JD-Core Version:    0.6.2
 */
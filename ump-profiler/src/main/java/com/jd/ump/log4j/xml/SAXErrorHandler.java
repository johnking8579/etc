/*    */ package com.jd.ump.log4j.xml;
/*    */ 
/*    */ import com.jd.ump.log4j.helpers.LogLog;
/*    */ import org.xml.sax.ErrorHandler;
/*    */ import org.xml.sax.SAXParseException;
/*    */ 
/*    */ public class SAXErrorHandler
/*    */   implements ErrorHandler
/*    */ {
/*    */   public void error(SAXParseException ex)
/*    */   {
/* 29 */     emitMessage("Continuable parsing error ", ex);
/*    */   }
/*    */ 
/*    */   public void fatalError(SAXParseException ex)
/*    */   {
/* 34 */     emitMessage("Fatal parsing error ", ex);
/*    */   }
/*    */ 
/*    */   public void warning(SAXParseException ex)
/*    */   {
/* 39 */     emitMessage("Parsing warning ", ex);
/*    */   }
/*    */ 
/*    */   private static void emitMessage(String msg, SAXParseException ex) {
/* 43 */     LogLog.warn(msg + ex.getLineNumber() + " and column " + ex.getColumnNumber());
/*    */ 
/* 45 */     LogLog.warn(ex.getMessage(), ex.getException());
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.xml.SAXErrorHandler
 * JD-Core Version:    0.6.2
 */
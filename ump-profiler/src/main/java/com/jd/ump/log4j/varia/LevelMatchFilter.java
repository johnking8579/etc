/*     */ package com.jd.ump.log4j.varia;
/*     */ 
/*     */ import com.jd.ump.log4j.Level;
/*     */ import com.jd.ump.log4j.helpers.OptionConverter;
/*     */ import com.jd.ump.log4j.spi.Filter;
/*     */ import com.jd.ump.log4j.spi.LoggingEvent;
/*     */ 
/*     */ public class LevelMatchFilter extends Filter
/*     */ {
/*  45 */   boolean acceptOnMatch = true;
/*     */   Level levelToMatch;
/*     */ 
/*     */   public void setLevelToMatch(String level)
/*     */   {
/*  54 */     this.levelToMatch = OptionConverter.toLevel(level, null);
/*     */   }
/*     */ 
/*     */   public String getLevelToMatch()
/*     */   {
/*  59 */     return this.levelToMatch == null ? null : this.levelToMatch.toString();
/*     */   }
/*     */ 
/*     */   public void setAcceptOnMatch(boolean acceptOnMatch)
/*     */   {
/*  64 */     this.acceptOnMatch = acceptOnMatch;
/*     */   }
/*     */ 
/*     */   public boolean getAcceptOnMatch()
/*     */   {
/*  69 */     return this.acceptOnMatch;
/*     */   }
/*     */ 
/*     */   public int decide(LoggingEvent event)
/*     */   {
/*  86 */     if (this.levelToMatch == null) {
/*  87 */       return 0;
/*     */     }
/*     */ 
/*  90 */     boolean matchOccured = false;
/*  91 */     if (this.levelToMatch.equals(event.getLevel())) {
/*  92 */       matchOccured = true;
/*     */     }
/*     */ 
/*  95 */     if (matchOccured) {
/*  96 */       if (this.acceptOnMatch) {
/*  97 */         return 1;
/*     */       }
/*  99 */       return -1;
/*     */     }
/* 101 */     return 0;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.varia.LevelMatchFilter
 * JD-Core Version:    0.6.2
 */
/*    */ package com.jd.ump.log4j.varia;
/*    */ 
/*    */ import com.jd.ump.log4j.AppenderSkeleton;
/*    */ import com.jd.ump.log4j.spi.LoggingEvent;
/*    */ 
/*    */ public class NullAppender extends AppenderSkeleton
/*    */ {
/* 30 */   private static NullAppender instance = new NullAppender();
/*    */ 
/*    */   public void activateOptions()
/*    */   {
/*    */   }
/*    */ 
/*    */   /** @deprecated */
/*    */   public NullAppender getInstance()
/*    */   {
/* 47 */     return instance;
/*    */   }
/*    */ 
/*    */   public static NullAppender getNullAppender()
/*    */   {
/* 55 */     return instance;
/*    */   }
/*    */ 
/*    */   public void close()
/*    */   {
/*    */   }
/*    */ 
/*    */   public void doAppend(LoggingEvent event)
/*    */   {
/*    */   }
/*    */ 
/*    */   protected void append(LoggingEvent event)
/*    */   {
/*    */   }
/*    */ 
/*    */   public boolean requiresLayout()
/*    */   {
/* 77 */     return false;
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.varia.NullAppender
 * JD-Core Version:    0.6.2
 */
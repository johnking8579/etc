/*    */ package com.jd.ump.log4j.lf5;
/*    */ 
/*    */ import com.jd.ump.log4j.PropertyConfigurator;
/*    */ import com.jd.ump.log4j.spi.Configurator;
/*    */ import com.jd.ump.log4j.spi.LoggerRepository;
/*    */ import java.io.IOException;
/*    */ import java.net.URL;
/*    */ 
/*    */ public class DefaultLF5Configurator
/*    */   implements Configurator
/*    */ {
/*    */   public static void configure()
/*    */     throws IOException
/*    */   {
/* 78 */     String resource = "/org/apache/log4j/lf5/config/defaultconfig.properties";
/*    */ 
/* 80 */     URL configFileResource = DefaultLF5Configurator.class.getResource(resource);
/*    */ 
/* 83 */     if (configFileResource != null)
/* 84 */       PropertyConfigurator.configure(configFileResource);
/*    */     else
/* 86 */       throw new IOException("Error: Unable to open the resource" + resource);
/*    */   }
/*    */ 
/*    */   public void doConfigure(URL configURL, LoggerRepository repository)
/*    */   {
/* 97 */     throw new IllegalStateException("This class should NOT be instantiated!");
/*    */   }
/*    */ }

/* Location:           C:\Users\Administrator\Desktop\
 * Qualified Name:     com.jd.ump.log4j.lf5.DefaultLF5Configurator
 * JD-Core Version:    0.6.2
 */
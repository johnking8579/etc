/**
 * 数据字典-数据项列表页面JS
 * User: 
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;

//初始化页面
$(function() {
	bindValidate($('#taskParaRelaSearchForm'));
    
	var columns =  [
	                {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	                { "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
	                { "mData": "code","sTitle":"编码","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "name","sTitle":"名称","bSortable":true,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "url","sTitle":"链接","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
	                    return "<a href='#' title='修改' ><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'/ onclick='updateTaskParaRela(\""+data+"\");'></a>&nbsp;"+
	                    "<a href='#' title='删除' ><img src='"+springUrl+"/static/admin/skin/img/del.png' alt='删除' onclick='deleteTaskParaRela(\""+data+"\");'/></a>";
	                }}
	            ];
	var btns=[
	          {
	              "sExtends":    "text",
	              "sButtonText": "添加任务节点",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  //loadUrl(springUrl+"/supplier/taskParaRela_add);
	            	  addPara(1);
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "添加角色",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  //loadUrl(springUrl+"/supplier/taskParaRela_add);
	            	  addPara(2);
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "添加部门",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  //loadUrl(springUrl+"/supplier/taskParaRela_add);
	            	  addPara(3);
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "添加绑定",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  loadUrl(springUrl+"/supplier/taskParaRela_bindingIndex?taskId="+$("#taskId").val());
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "节点初始化",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  initNode();
	              }
	          }
	          ];
	
	table=$("#taskParaRelaTable").dataTable({
	"bProcessing": false,
    "bServerSide":true,
    "sPaginationType": "full_numbers",
    "sAjaxSource":springUrl+"/supplier/taskParaRela_page",
    "sServerMethod": "POST",
    "bAutoWidth": false,
    "bStateSave": false,
    "sScrollY":"100%",
    "bScrollCollapse": true,
    "bPaginate":true,
    "oLanguage": {
        "sLengthMenu": "每页显示 _MENU_ 条记录",
        "sZeroRecords": "抱歉， 没有找到",
        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
        "sInfoEmpty": "没有数据",
        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
        "oPaginate": {
            "sFirst": "首页",
            "sPrevious": "前页",
            "sNext": "后页",
            "sLast": "尾页"}
    },
    "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bJQueryUI": true,
    "bFilter":false,
	"fnServerData":function ( sSource, aoData, fnCallback ) {
			var taskId=$("input#taskId").val()==null?'':$("input#taskId").val();
			var name = $("#name").val();
			aoData.push( { "name": "taskId", "value":taskId },
						 { "name": "name", "value":name });
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","数据字典数据查询失败");
                        }
		    });
		},
	"fnDrawCallback": function( oSettings ){
		var that = this;
		this.$('td:first-child').each(function(i){
			that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
	    });          
	},
	"aaSorting":[],//默认排序
	"aoColumns":columns,
	"oTableTools":{
	    "aButtons":btns
	}


	});
	
	//查询
	$('#requirement_search').click(function() {
		if(validate($("#taskParaRelaSearchForm"))){
			table.fnDraw(); 
		} 
	});

});
	


function initNode(){
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/taskNode_init",
        data:{
        	taskId:$("#taskId").val()
        },
        success:function (data) {
        	if(typeof(data) != 'object')
        		data = eval("("+data+")");
        	if (data.operator == true){
        		Dialog.alert("初始化成功！",data.message);
        	}else{
            	Dialog.alert("失败！",data.message);
            }
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}

/**
 * 新增参数
 * @param type
 */	            	  
function addPara(type){
	Dialog.openRemote('taskPara_add','新增参数',springUrl+"/supplier/taskPara_add?type="+type,600,400,[
		{
		    "label": "取消",
		    "class": "btn-success",
		    "callback": function() {}
		},
		{
		    "label": "保存",
		    "class": "btn-success",
		    "callback": function() {
		    	if(!validate($('#taskParaAddForm'))){
						return false;
				}
		    	addSave(type);
		    }
		}
		]);                                                                                                               
}

/**
 * 保存参数
 * @param type
 */
function addSave(type){
	var code = $("#code_ad").val();
	var name = $("#name_ad").val();
	var url = $("#url_ad").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/taskPara_addSave",
        data:{
        	code:code,
        	name:name,
        	type:type,
        	url:url
        },
        success:function (data) {
            Dialog.hide();
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}


function binding(type){
	Dialog.openUrl("taskPara_binding", 
			"新增绑定", "/supplier/taskPara_addBinding?type="+type, 600, 400, 
			[
			 {
				 "label": "取消",
			     "class": "btn-success",
				 "callback": function() {}
			 },
			 {
				 "label": "保存",
				 "class": "btn-success",
			     "callback": function() {
			     if(!validate($('#taskParaAddForm'))){
							return false;
				 }
			     addBinding(type);
			     }
			 }
			 ]);
}

function addBinding(type){
	 var taskId = $("#taskId").val();
	 jQuery.ajax({
	        type:"POST",
	        cache:false,
	        url: springUrl+"/supplier/taskPara_saveBinding",
	        data:$('#taskParaAddForm').serialize(),
	        success:function (data) {
	            Dialog.hide();
	        },
	        error: function (data) {
	            Dialog.alert("失败","数据字典数据更新失败");
	        }
	    });
	 
}
/**
 * 删除单条预选信息
 */
function deleteTaskParaRela(id){
    Dialog.confirm("确认","确定删除数据字典数据项?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/taskParaRela_delete",
                data:{
                    id:id
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+"/supplier/taskParaRela_index?taskId="+$("input#taskId").val());
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}

/**
 * 进入预选修改界面
 * @param id
 */
function updateTaskParaRela(id){
    Dialog.openRemote('taskParaRela_update','供应商预选修改',springUrl+"/supplier/taskParaRela_update?id="+id,600,400,[
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function() {}
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function() {
            	if(!validate($('#taskParaRelaUpdateForm'))){
						return false;
					}
            	updateTaskParaRelaInfo();
            }
        }
    ]);
}

/**
 * 更新预选信息
 */
function updateTaskParaRelaInfo(){
    jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/taskParaRela_updateSave",
        data:$('#taskParaRelaUpdateForm').serialize(),
        success:function (data) {
            Dialog.hide();
            loadUrl(springUrl+"/supplier/taskParaRela_index?taskId="+$("input#taskId").val());
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}




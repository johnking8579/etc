$(function() {

	$("#winBidNotice_add_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			addWinBidNoticeRecord1();
		}
	});
	
	$("#winBidNotice_update_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			updateWinBidNoticeRecord();
		}
	});
	
	$("#publishdate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });

	//getBaseControlInfo();
	bindValidate($('#addPane'));
	findFinalSelectedByReqId();
});


function addWinBidNoticeRecord1() {
	$("#requirementName").val($("#requirementId option:selected").text());
	$("#supplierName").val($("#supplierId option:selected").text());
	var urlpara = "/supplier/winBidNotice_save";
	//把表单的数据进行序列化 
	var params = $("#insertWinBidNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/winBidNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}


function updateWinBidNoticeRecord() {
	$("#requirementName").val($("#requirementId option:selected").text());
	$("#supplierName").val($("#supplierId option:selected").text());
	var urlpara = "/supplier/winBidNotice_update";
	//把表单的数据进行序列化 
	var params = $("#updateWinBidNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/winBidNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

function findFinalSelectedByReqId(){
	var requirementId=$("#requirementId option:selected").val();
	var urlpara = "/supplier/findFinalSelectedByReqId?requirementId="+requirementId;
	var supplierId1=$("#supplierId1").val();
	//把表单的数据进行序列化 
	$.ajax({
		url : urlpara,
		type : "POST",
		//data : params,
		dataType : "json",
		success : function(data) {
			var length=data.length;
			if(length==0){
				$("#supplierId").find("option").remove();
			}else{
				for(var i=0;i<length;i++){
					if(supplierId1==data[i].id){
						$("#supplierId").append("<option value="+data[i].id+" selected>"+data[i].name+"</option>");
					}else{
						$("#supplierId").append("<option value="+data[i].id+">"+data[i].name+"</option>");
					}
				}
			}
			
		},
		error : function(xhr, st, err) {
			alert("查询失败!");
		}
	});
	
}







function save() {
	if(validate($("#form1"))){
		edit(); } 
	} 

function back() {
	loadUrl("/supplier/supplier_index");
}

function edit() {
	Dialog.confirm("确认","确定修改吗?","是","否",function(result){
		if(result){	
		var urlpara = springUrl+"/supplier/supplier_edit";
		var params = $("#form1").serialize();
		$.ajax({
			url : urlpara,
			type : "POST",
			data : params,
			dataType : "json",
			success : function(data) {
				if(data == 1){
				 alert("修改数据成功!");
				 loadUrl("/supplier/supplier_index");
				}else{
					alert("操作异常,请联系管理员!");
					return false;
				}
			}
		
		});
    }
  });
}
function addRow(){
		var i = jQuery("#i_hidden").val();
		var rowStr = "<tr id='tr_contact_"+i+"'>";
		rowStr += "<td ><input maxlength='50' type='text' class='input-text' name='contacts["+i+"].contactor'></td>";
		rowStr += "<td ><input maxlength='50' type='text' class='input-text' name='contacts["+i+"].position'></td>";
		rowStr += "<td ><input maxlength='50' type='text' class='input-text' name='contacts["+i+"].telphone'></td>";
		rowStr += "<td ><input maxlength='20' type='text' class='input-text' name='contacts["+i+"].mobile'></td>";
		rowStr += "<td ><input maxlength='20' type='text' class='input-text' name='contacts["+i+"].fax'></td>";
		rowStr += "<td ><input maxlength='50' type='text' class='input-text' name='contacts["+i+"].email'></td>";
		rowStr += "<td ><a href='javascript:delRow("+i+")'>删除</a></td>";
		rowStr += "</tr>";
		jQuery("#contact").append(rowStr);
		jQuery("#i_hidden").val(parseInt(i)+1);
	}
	
	function delRow(tr_id) {
		jQuery("#tr_contact_"+tr_id).remove();
	}
	
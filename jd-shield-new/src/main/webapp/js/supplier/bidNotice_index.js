var table;
var tableData;
$(function(){ 

//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='bidNotice' value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	            {"mData": "title","bSortable":false,"sDefaultContent": "","sTitle":"招标预告标题","sDefaultContent":""},
		        {"mData": "requirementName", "sTitle": "项目名称" ,"sDefaultContent": "", "bSortable": true},
		        {"mData": "biddate","bSortable":false,"sTitle":"预计招标时间","sDefaultContent":"","mRender":function(data){
		        	    return parseDate(data);
		        }},
		        {"mData": "publishdate","bSortable":false,"sTitle":"发布时间","sDefaultContent":"","mRender":function(data){
		        	    return parseDate(data);
		        }},
		        {"mData": "bidprincipal","bSortable":false,"sDefaultContent": "","sTitle":"招标负责人"},
		        {"mData": "phone","bSortable":false,"sDefaultContent":"","sTitle":"电话"},
		        {"mData": "telephone","bSortable":false,"sDefaultContent":"","sTitle":"手机"},
		        {"mData": "id", "sTitle": "操作", "bSortable": false,"mRender": function (data, type, full) {
		        	var id = data;
					return "&nbsp;&nbsp;<a href='#'  title='修改' onclick='editBidNotice(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'></a>" +
					"&nbsp;&nbsp;<a href='#' title='取消' onclick='delBidNotice(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/del.png' alt='删除'></a>";
					
		        }}
        
        
	        ];

var  tableBtns  =[
				];


 table = $('#hello').dataTable( {
	 		"sDefaultContent":"bidNotice",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/notice_page",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
                var type=$("#type").val();
				var searchTitleName=$("input#searchTitleName").val();
				if(searchTitleName==null) searchTitleName="";
				searchTitleName=searchTitleName.replace("_","\\_");
				aoData.push( 
						{ "name": "searchTitleName", "value":searchTitleName },
						{ "name": "type", "value":type }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );
 

  	function converReqStatus(data){
  		var reqStatus="";
  		if(data=="1"){
  			reqStatus="招标预告";
  		}else if(data=="2"){
  			reqStatus="招标预告招标预告";
  		}else if(data=="3"){
  			reqStatus="驳回";
  		}else if(data=="4"){
  			reqStatus="招标预告招标预告会";
  		}
  		return reqStatus;
  	}
  	
  	parseDate= function(data){
  		 if(data!="")
    	 return new Date(data).format('yyyy-MM-dd');
    	 else return "";
  	}
	

	$('#bidNotice_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); } 
		}
	);
	$('#searchTitleName').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	
	$("#bidNotice_add_btn").click(function(){
		var validateEl=$('#addPane');
		if(validate(validateEl)){
			addBidNoticeRecord1();
		}
	});
	
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#supplierSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}


function saveBidNoticePage(){
	loadUrl("/supplier/bidNotice_savepage");
}




//清空添加面板的数据
function clearAddPane(){
	setBidNoticeId("");
	setBidNoticeName("");
	setOrganization("","不限","不限");
	setPostion("","不限","不限");
	setUserName("","任何人");
	setLevel("","","");
	setRole("");
	setGroup("");
}




//获取要修改操作数据的值
function editBidNotice(id) {
	loadUrl("/supplier/bidNotice_editpage?id=" + id);
}


function delBidNotice(id) {
    Dialog.confirm("提示", "确定要删除改条记录吗?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                cache: false,
                url: springUrl+"/supplier/notice_delete",
                data: {
                    id: id
                },
                success: function (msg) {
                	alert("删除成功!");
                    table.fnDraw();
                },
                error: function (msg) {
                	alert("取消失败");
                }
            });
        }
    });
}



//判断两个map是否相同
function isEqualMap(map1,map2){
	var bool=true;
	for(var key in map1){
		if(map1[key]!=map2[key]){
			bool=false;
			return false;
		}
	}
	return bool;
}


bindValidate($('#assignmentAddForm'));
function addAssignment(){
	var taskId = $("#taskId").val();
	var nodeId = $("#nodeId").val();
	var userId = $("#userId").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/task_assignmentSave",
        data:{
        	taskId:taskId,
        	nodeId:nodeId,
        	userId:userId
        },
        success:function (data) {
        	if(typeof(data)!='object')
        		data = eval("("+data+")");
        	if(data.operator == true){
        		Dialog.alert("成功！",data.message);
        		loadUrl(springUrl+"/supplier/task_view?id="+taskId);
        	}else{
        		Dialog.alert("失败！",data.message);
                return false;
        	}
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}

bindValidate($('#inputReasonForm'));
//认领
function claim(){
	var taskId = $("#taskId").val();
	var nodeId = $("#nodeId").val();
	var roleCode = $("#roleCode").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/task_claimUpdate",
        data:{
        	taskId:taskId,
        	id:nodeId,
        	roleCode:roleCode
        },
        success:function (data) {
        	if(typeof(data)!='object')
        		data = eval("("+data+")");
        	if(data.operator == true){
        		Dialog.alert("成功！",data.message);
        		loadUrl(springUrl+"/supplier/task_view?id="+taskId);
        	}else{
        		Dialog.alert("失败",data.message);
                return false;
        	}
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}

//输入驳回原因
function inputReason(){
	Dialog.openRemote("", "输入驳回原因", springUrl+"/supplier/inputReason", 600, 400, [
	{
	    "label": "取消",
	    "class": "btn btn-green mr10",
	    "callback": function() {}
	},
	{
	    "label": "确认",
	    "class": "btn btn-green mr10",
	    "callback": function() {
	    	if(!validate($('#inputReasonForm'))){
					return false;
				}
	    	reject();
	}
}
	]);
}

//驳回
function reject(){
	var taskId = $("#taskId").val();
	var nodeId = $("#nodeId").val();
	var reason = $("#reason").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/task_claimReject",
        data:{
        	taskId:taskId,
        	id:nodeId,
        	reason:reason
        },
        success:function (data) {
        	if(typeof(data)!='object')
        		data = eval("("+data+")");
        	if(data.operator == true){
        		Dialog.alert("成功！",data.message);
        		loadUrl(springUrl+"/supplier/task_view?id="+taskId);
        	}else{
        		Dialog.alert("失败",data.message);
                return false;
        	}
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}
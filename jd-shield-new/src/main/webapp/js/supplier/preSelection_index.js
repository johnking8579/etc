/**
 * 数据字典-数据项列表页面JS
 * User: 
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;

//初始化页面
$(function() {
	bindValidate($('#preSelectionForm'));
    
	var columns =  [
	                {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	                { "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
	                { "mData": "enterpriseName","sTitle":"供应商全称","bSortable":false,"bVisible":true,"sDefaultContent":"","fnRender":function(data){
	                	if(data.aData.supplierId==null || data.aData.supplierId=='')
	                		return "<span style='color:red'>"+data.aData.enterpriseName+"</span>";
	                	else
	                		return data.aData.enterpriseName;
	                }},
	                { "mData": "funds","sTitle":"注册资金","bSortable":true,"sClass": "my_class","sDefaultContent":'0'},
	                { "mData": "beSampleQualified","sTitle":"样品是否合格","bSortable":true,"sClass": "my_class","fnRender":function(data){
	                	return getSelected(data,'beSampleQualified');
	                }},
	                { "mData": "beInvestigationQualified","sTitle":"考察是否合格" ,"bSortable":true,"fnRender":function(data){
	                	return getSelected(data,'beInvestigationQualified');
	                }},
	                { "mData": "beWillingTo","sTitle":"是否愿意投标","bSortable":true,"bVisible":true,"fnRender":function(data){
	                	return getSelected(data,'beWillingTo');
	                }},
	                { "mData": "remarks","sTitle":"备注","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "investigationReportName","sTitle":"考察报告","bSortable":false,"bVisible":true,"sDefaultContent":"","fnRender":function(data){
	                	var url;
	                	var name;
	                	if(data.aData.investigationReportId==null)
	                		url = 'javascript:void(0);';
	                	else
	                		url = springUrl+"/supplier/selection_download?id="+data.aData.id+"&fileType=1";
	                	if(data.aData.investigationReportName==null)
	                		name = 'file';
	                	else
	                		name = data.aData.investigationReportName;
	                	return "<a href='"+url+"' >"+name+"</a>";
//	                	return "<a href='javascript:void(0)' onclick='downloadFile("+data.aData.id+",1)'>"+name+"</a>";
	                }},
	                { "mData": "sampleTestReportName","sTitle":"样品测试报告","bSortable":false,"bVisible":true,"sDefaultContent":"","fnRender":function(data){
	                	var url;
	                	var name;
	                	if(data.aData.sampleTestReportId==null)
	                		url = 'javascript:void(0);';
	                	else
	                		url = springUrl+"/supplier/selection_download?id="+data.aData.id+"&fileType=2";
	                	if(data.aData.sampleTestReportName==null)
	                		name = 'file';
	                	else
	                		name = data.aData.sampleTestReportName;
	                	return "<a href='"+url+"' >"+name+"</a>";
	                }},
	                { "mData": "beFinanceQualified","sTitle":"财务审查是否合格","bSortable":true,"bVisible":true,"fnRender":function(data){
	                	return getSelected(data,'beFinanceQualified');
	                }},
	                {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
	                    return "<a href='#' title='修改'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'/ onclick='updatePreSelection(\""+data+"\");'></a>&nbsp;"+
	                    "<a href='#' title='删除'><img src='"+springUrl+"/static/admin/skin/img/del.png' alt='删除' onclick='deletePreSelection(\""+data+"\");'/></a>";
	                }}
	            ];
	var btns=[
	          {
	              "sExtends":    "text",
	              "sButtonText": "添加预选供应商",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  //Dialog.hide();
	            	  loadUrl(springUrl+"/supplier/preSelection_add?taskId="+$("input#taskId").val()+"&nodeId="+$("#nodeId").val());
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "完成预选",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  //Dialog.hide();
	            	  checkFinish();
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "返回",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  //Dialog.hide();
	            	  back2TaskView();
	              }
	          }
	          ];
	
	table=$("#preSuppSelectionTable").dataTable({
	"bProcessing": false,
    "bServerSide":true,
    "sPaginationType": "full_numbers",
    "sAjaxSource":springUrl+"/supplier/preSelection_page",
    "sServerMethod": "POST",
    "bAutoWidth": false,
    "bStateSave": false,
    "sScrollY":"100%",
    "bScrollCollapse": true,
    "bPaginate":true,
    "oLanguage": {
        "sLengthMenu": "每页显示 _MENU_ 条记录",
        "sZeroRecords": "抱歉， 没有找到",
        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
        "sInfoEmpty": "没有数据",
        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
        "oPaginate": {
            "sFirst": "首页",
            "sPrevious": "前页",
            "sNext": "后页",
            "sLast": "尾页"}
    },
    "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bJQueryUI": true,
    "bFilter":false,
	"fnServerData":function ( sSource, aoData, fnCallback ) {
			var taskId=$("input#taskId").val()==null?'':$("input#taskId").val();
			var enterpriseName = $("#searchEnterpriseName").val();
			var funds = $("#searchFunds").val();
			aoData.push( { "name": "taskId", "value":taskId },
						 { "name": "funds", "value":funds },
						 { "name": "enterpriseName", "value":enterpriseName });
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","数据字典数据查询失败");
                        }
		    });
		},
	"fnDrawCallback": function( oSettings ){
		var that = this;
		this.$('td:first-child').each(function(i){
			that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
	    });          
	},
	"aaSorting":[],//默认排序
	"aoColumns":columns,
	"oTableTools":{
	    "aButtons":btns
	}


	});
	
	//查询
	$('#requirement_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	});

});
	
	
function getSelected(data,o){
	var a;
	var s = 'data.aData.' + o;
	if(eval('(' +s+ ')'))
		a = '是';
	else
		a = '否';
	return a;
}

function getSelected_0605(data,o){
	var a;
	var s = 'data.aData.' + o;
	if(eval('(' +s+ ')'))
		a = '<option value=\'true\' selected=\'selected\' >是</option><option value=\'false\'>否</option>';
	else
		a = '<option value=\'true\'>是</option><option value=\'false\' selected=\'selected\'>否</option>';
	return '<select class=\"span1\" disabled=\'disabled\'>'+a+'</select>';
}

/**
 * 删除单条预选信息
 */
function deletePreSelection(id){
	var taskId = $("#taskId").val(); 
	var nodeId = $("#nodeId").val();
    Dialog.confirm("确认","确定删除吗?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/preSelection_delete",
                data:{
                    id:id
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+"/supplier/preSelection_index?taskId="+taskId+"&nodeId="+nodeId);
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}

/**
 * 进入预选修改界面
 * @param id
 */
function updatePreSelection(id){
    Dialog.openRemote('preSelection_update','供应商预选修改',springUrl+"/supplier/preSelection_update?id="+id,600,400,[
        {
            "label": "取消",
            "class": "btn btn-green mr10",
            "callback": function() {}
        },
        {
            "label": "保存",
            "class": "btn btn-green mr10",
            "callback": function() {
            	if(!validate($('#preSelectionUpdateForm'))){
						return false;
					}
            	updatePreSelectionInfo();
            }
        }
    ]);
}

/**
 * 更新预选信息
 */
function updatePreSelectionInfo(){
	var taskId = $("#taskId").val(); 
	var nodeId = $("#nodeId").val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/preSelection_updateSave",
        data:$('#preSelectionUpdateForm').serialize(),
        success:function (data) {
            Dialog.hide();
            loadUrl(springUrl+"/supplier/preSelection_index?taskId="+taskId+"&nodeId="+nodeId);
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}

//下载文件
function downloadFile(id,type){
	Dialog.confirm("确认","确定下载该文件吗?","是","否",function(result){
        if(result){
        	jQuery.ajax({
        		type:"POST",
        		cache:false,
        		url: springUrl+"/supplier/selection_download",
        		data:{
        			id:id,
        			fileType:type
        		},
        		success:function (data) {
        			if(typeof(data) != 'object')
                		data = eval("("+data+")");
        			if(data.operator==true){
        				Dialog.alert("成功","下载成功，默认的下载路径："+data.tempDir);
        			}else{
        				Dialog.alert("失败","下载失败，请重新尝试！");
        			}
        		},
        		error: function (data) {
        			Dialog.alert("失败","系统异常");
        		}
        	});
        }
	});
}

//检查是否已做预选
function checkFinish(){
	var taskId = $("#taskId").val();
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/preSelection_checkFinish",
        data:{
        	taskId:taskId
        },
        success:function (data) {
        	if(typeof(data) != 'object')
        		data = eval("("+data+")");
        	if(data.count>0){
        		var nodeId = $("#nodeId").val();
        		taskNode_compelete(nodeId,"");
        	}else{
        		Dialog.alert("提示","请添加预选数据！");
        	}
        },
        error: function (data) {
            Dialog.alert("失败","系统异常");
        }
    });
}


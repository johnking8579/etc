$(function() {

	$("#requirement_add_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			addRequirementRecord1();
		}
	});
	
	$("#requirement_update_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			updateRequirementRecord();
		}
	});

	
	$("#projectDate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });
	
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	
});

	

function addRequirementRecord1() {
	var urlpara = "/supplier/requirement_save";
	//把表单的数据进行序列化 
	var params = $("#insertRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/requirement_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}


function updateRequirementRecord() {
	var urlpara = "/supplier/requirement_update";
	//把表单的数据进行序列化 
	var params = $("#updateRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/requirement_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

function requirement_back_btn(){
	loadUrl("/supplier/requirement_index");
}

function showRequirementYear(){
	if($("#isYear option:selected").text()=='是'){
		$("#requirementYearIdDiv").attr("style","display:block");
		$("#outsideReasonDiv").attr("style","display:none");
	}else if($("#isYear option:selected").text()=='否'){
		$("#requirementYearIdDiv").attr("style","display:none");
		$("#outsideReasonDiv").attr("style","display:block");
	}else{
		$("#requirementYearIdDiv").attr("style","display:none");
		$("#outsideReasonDiv").attr("style","display:none");
	}
}

function copyProjectName(){
	var annualRequirementId=$("#requirementYearId option:selected").val();
	if(annualRequirementId!=null&&annualRequirementId!=""){
		$("#projectName").val($("#requirementYearId option:selected").text());
		var urlpara = "/supplier/getAnnualRequirement?id="+annualRequirementId;
		//把表单的数据进行序列化 
		$.ajax({
			url : urlpara,
			type : "POST",
			dataType : "json",
			success : function(data) {
				$("#budgetAmount").val(data.tenderBudget);
			},
			error : function(xhr, st, err) {
				alert("查询年度计划预算金额失败!");
			}
		});
	}
	
}



var j=1;
function uploadifyUpload(i){
		$('#uploadify'+i).uploadifySettings('scriptData');
		$('#uploadify'+i).uploadifyUpload();
		j=i;
}

function showUpload(i){
	if($("#fileType"+i).attr("checked")=="checked"){
		//$("#uploadify"+i+"Uploader").attr("style","visibility:visible;");
		/*$('#uploadifyStart'+i).attr("style","display:block");
		$('#uploadifyCancel'+i).attr("style","display:block");*/
	}else{
		//$("#uploadify"+i+"Uploader").attr("style","visibility:hidden;");
		/*$('#uploadifyStart'+i).attr("style","display:none");
		$('#uploadifyCancel'+i).attr("style","display:none");*/
	}
	
}

$(function (){
	var i=1;
	var filename1="";
	var filename2="";
	var filename3="";
	var filename4="";
	var filename5="";
	for (i;i<=5;i++){
		$("#uploadify"+i).uploadify({
		    'uploader'       : '/static/supplier/upload/uploadify.swf',
		    //'script'         : '/servlet/fileUpload',
		    'script'         : '/servlet/fileUpload',
		    'cancelImg'      : '/static/supplier/images/cancel.png',
		    'queueID'        : 'fileQueue',
		    'auto'           : false,	
		    'multi'          : true,
		    'fileDesc'		:'*.*',//上传文件类型说明  
		   'fileExt'		:'*.*',
		    //'buttonText'     : 'Browse file',
		   'buttonImg'      : '/static/supplier/images/btn_upload.png',
		                
		    onComplete: function (evt, queueID, fileObj, response, data) {
		    	if(j==1){
		    		filename1+=response+",";
			    	$("#fileName1").val(filename1);
		    	}else if(j==2){
		    		filename2+=response+",";
			    	$("#fileName2").val(filename2);
		    	}else if(j==3){
		    		filename3+=response+",";
			    	$("#fileName3").val(filename3);
		    	}else if(j==4){
		    		filename4+=response+",";
			    	$("#fileName4").val(filename4);
		    	}else if(j==5){
		    		filename5+=response+",";
			    	$("#fileName5").val(filename5);
		    	}
		    	
				//$('<li></li>').appendTo('.files').text(filename1);
			},
			onError: function(a, b, c, d) {
		         alert("文件上传失败");
				},
			onAllComplete : function (evt ,data){
					alert("文件上传成功");
					$('#uploadifyStart'+j).attr("style","display:none");
					$('#uploadifyCancel'+j).attr("style","display:none");
					//alert($("#fileName1").val());
			}
		});
		
		//$("#uploadify"+i+"Uploader").attr("style","visibility:hidden;");
	};
	
	
})


delAffix =function (reqAffixId,reqId,flag,fileType){
	
	if(confirm("确定删除吗?")){
		var urlpara = "/supplier/AffixFile_delete?ids="+reqAffixId;
		//把表单的数据进行序列化 
		$.ajax({
			url : urlpara,
			type : "POST",
			//data : params,
			dataType : "json",
			success : function(data) {
				alert(data.message);
				
				//loadUrl("/supplier/requirement_editpage?id=" + reqId);
				
				var urlpara = "/supplier/queryAffixList?requirementId="+reqId+"&fileType="+fileType;
				//把表单的数据进行序列化 
				$.ajax({
					url : urlpara,
					type : "POST",
					//data : params,
					dataType : "json",
					success : function(data) {
						$("#"+flag).html("");
						//重新加载数据
						if(data.length!=0){
							var info="";
							for(var i=0;i<data.length;i++){
								info+="<a href='/supplier/downFilename?filename="+data[i].fileName+"&filekey="+data[i].fileKey+"'>"+data[i].fileName+"</a>" +
								" &nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='delAffix("+data[i].id+","+reqId+",\""+flag+"\","+fileType+")'>删除</a>&nbsp ";
							}
							$("#"+flag).html(info);
						}
						
					},
					error : function(xhr, st, err) {
						alert("重新加载附件失败!");
					}
				});
				
				
			},
			error : function(xhr, st, err) {
				alert("删除失败!");
			}
		});
	}
	
	
	
}


$(function() {

	$("#requirementReject_add_btn").click(function() {
		var validateEl = $('#addPaneRequirementReject');
		if (validate(validateEl)) {
			addRequirementRejectRecord();
		}
	});
	
	
	bindValidate($('#addPaneRequirementReject'));
	
	
});


saveRequirementReject  =function (requirementId){
	var urlpara = "/supplier/requirementReject_save?requirementId="+requirementId;
	//把表单的数据进行序列化 
	var params = $("#insertRequirementRejectForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("驳回成功!");
			Dialog.hide();
			loadUrl("/supplier/requirementAduit_index");
		},
		error : function(xhr, st, err) {
			alert("驳回失败!");
		}
	});
}

	
	


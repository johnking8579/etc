function back_btn() {
	loadUrl("/supplier/supplier_index");
}
function examine_btn() {
	if(validate($("#form1"))){
		saveExamine(); } 
	}
function reject_btn() {
	if($("#auditReason").val()!=''){
		rejectExamine();
	}else{
		$("#auditReason").focus();
		return false;
	}
 }
function saveExamine() {
	var urlpara = "/supplier/supplier_saveExamine";
	var params = $("#form1").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			if(data == 1){
			 alert("审核成功!");
			 loadUrl("/supplier/supplier_index");
			}else{
				alert("操作异常,请联系管理员!");
				return false;
			}
		}
	
	});
}
function rejectExamine() {
	var urlpara = "/supplier/supplier_rejectExamine";
	var params = $("#form1").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			if(data == 1){
			 alert("驳回成功!");
			 loadUrl("/supplier/supplier_index");
			}else{
				alert("操作异常,请联系管理员!");
				return false;
			}
		}
	
	});
}
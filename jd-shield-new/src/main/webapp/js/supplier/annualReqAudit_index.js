var table;
var tableData;
$(function(){ 
//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='requirement' value=\""+data.aData.reqNumber+"\" approvalStatus=\""+data.aData.annualRequirementAudit.approvalStatus+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sDefaultContent":"","sWidth": "50px","bSortable": true},
	            {"mData": "reqNumber", "sDefaultContent":"","sTitle": "需求编号" , "bSortable": true},
	            {"mData": "firstDepName","bSortable":false,"sDefaultContent":"","sTitle":"需求部门"},
	            {"mData": "projectManager","bSortable":false,"sDefaultContent":"","sTitle":"项目负责人"},
		        {"mData": "tenderName","bSortable":false,"sDefaultContent":"","sTitle":"招标项目"},
		        {"mData": "tenderDetailName","bSortable":false,"sDefaultContent":"","sTitle":"明细项目名称"},
		        {"mData": "originalContractDate","bSortable":false,"sDefaultContent":"","sTitle":"原合同到期日","mRender": function (data) {
		        	return parseDate(data);
		        }},
		        {"mData": "tenderFinishDate","bSortable":false,"sDefaultContent":"","sTitle":"招标完成时间","mRender": function (data) {
		        	return parseDate(data);
		        }},
		        {"mData": "tenderBudget","bSortable":false,"sDefaultContent":"","sTitle":"预算金额"},
		        {"mData": "mark","bSortable":false,"sDefaultContent":"","sTitle":"备注"},
		        {"mData": "annualRequirementAudit.approvalStatus","bSortable":false,"sDefaultContent":"","sTitle":"状态","mRender": function (data) {
		        	switch(data)
		        	{
		        	case '1':return "待审核";break;
		        	case '2':return "驳回";break;
		        	case '3':return "已审核";break;
		        	case '0':return "草稿";break;
		        	}
		        }},
		        {"mData": "annualRequirementAudit.approvalReply","sDefaultContent": "","bSortable":false,"sTitle":"审批回复"},
		        {"mData": "id", "sDefaultContent":"","sTitle": "操作", "bSortable": false,"fnRender": function (obj) {
		        	var id = obj.aData.id;
		        	var approvalStatus=obj.aData.annualRequirementAudit.approvalStatus;
		        	if(approvalStatus==1)
					return "&nbsp;&nbsp;<a href='#' title='审核' onclick='auditRequirement(\""+id+"\");' style='font-size:12px;'>审核</a>";
					else if(approvalStatus==3){
		        		return "&nbsp;&nbsp;<a href='#'  title='详情' onclick='showRequirement(\""+id+"\");'><img src='"+springUrl+"/static/admin/skin/img/search.png' alt='详情'></a>";
		        	} else return null;
		        	
		        }
		        }
		        
        
        
	        ];

var  tableBtnsAudit  =[
					{
				    "sExtends":    "text",
				    "sButtonText": "批量驳回",
					"sToolTip": "驳回",
					
					"fnClick": function ( nButton, oConfig, oFlash ) {}
					},
					];




 table = $('#hello').dataTable( {
	 		"sDefaultContent":"requirement",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/annualReqAuditPage",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
                
				var reqNumber=$("input#reqNumber").val();
				if(reqNumber==null) reqNumber="";
				reqNumber=reqNumber.replace("_","\\_");
				var tenderName=$("input#tenderName").val();
				if(tenderName==null) tenderName="";
				tenderName=tenderName.replace("_","\\_");
				var status=$("select#status").val();
				if(status==null) status="";
				status=status.replace("_","\\_");
				aoData.push( 
						{ "name": "reqNumber", "value":reqNumber },
						{ "name": "tenderName", "value":tenderName },
						{ "name": "status", "value":status }
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":""
                
            }
        } );

	function saveRequirementPage(){
		loadUrl("/supplier/annualRequirement_savepage");
	}

	$('#expression_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); } 
		}
	);
	
	$('#all_Audit').click(function() {
		if(validate($("#supplierSearchForm"))){

			var approvalReply = $("#approvalReply").val();
			var reqNumber="";
			$('input[name=requirement]').each(function() {
				var approvalStatus=$(this).attr('approvalStatus');
	            if(approvalStatus!='1'){
	            	$(this).attr('checked',false);
	            }
	            	
				if ($(this).attr('checked')=="checked") {
					reqNumber+=$(this).val()+",";
				}
				});
			if(reqNumber.length==0){
				Dialog.alert("消息提示","没有选中项","确定");
			}else{
				Dialog.openRemote('all_Audit','审核信息',springUrl+"/supplier/allAudit_Page?reqNumber="+reqNumber,1000,600); 
			}
	    	} 
		}
	);
	
	$('#all_reject').click(function() {
		if(validate($("#supplierSearchForm"))){

			var approvalReply = $("#approvalReply").val();
			var ids="";
			$('input[name=requirement]').each(function() {
				var approvalStatus=$(this).attr('approvalStatus');
	            if(approvalStatus!='1'){
	            	$(this).attr('checked',false);
	            }
	            	
				if ($(this).attr('checked')=="checked") {
						ids+=$(this).val()+",";
				}
				});
			if(ids.length==0){
				Dialog.alert("消息提示","没有选中项","确定");
			}else{
				 if(approvalReply.length==0){
					 Dialog.alert("消息提示","驳回原因不能为空！","确定");
	                }else{
	                	rejectAnnualReqs(ids,approvalReply);
	                }
	                return false;
			}
	    	} 
		}
	);
	
	
	$('#reqNumber').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	
	$('#tenderName').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );

	$('#status').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
	$("#requirement_add_btn").click(function(){
		var validateEl=$('#addPane');
		if(validate(validateEl)){
			addRequirementRecord1();
		}
	});
	
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#supplierSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}

function showRequirement(id,reback){
	loadUrl("/supplier/annualReqAudit_detail?id=" + id+"&reback=admin");
}



//显示表达式对应的用户串
function showUsers(supplierId){
	var map = getUsers(supplierId);
//	$("#disp").html(map.usersName);	
//	$("#disp").show();
	if(map==null){
		Dialog.alert("提示","没有用户信息");
	}else{
		Dialog.alert("用户信息如下：",map.usersName);
	}
}

//隐藏表达式对应的用户串
function hideUsers(supplierId){
	$("#disp").hide();
}

//获得表达式对应的用户串Ajax
function getUsers(supplierId){
	var usersMap ;
    var data={
    		"supplierId":supplierId	
    };
	Dialog.post(springUrl+"/supplier/users_by_requirements_search_str",data,function(result){
		usersMap=result;
	});
	return usersMap;
}

//回到列表页面
function clearAddPane(){
	loadUrl("/supplier/annualReqAudit_index");
}


parseDate= function(data){
		 if(data!="")
	 return new Date(data).format('yyyy-MM-dd');
	 else return "";
	};



$('#searchProjectName').keyup(function() {
	if(validate($("#requirementSearchForm"))){
		table.fnDraw(); 
	} 
} );






//判断两个map是否相同
function isEqualMap(map1,map2){
	var bool=true;
	for(var key in map1){
		if(map1[key]!=map2[key]){
			bool=false;
			return false;
		}
	}
	return bool;
}

//打开编辑页面
//获取要修改操作数据的值
function auditRequirement(id) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "确认",
            "class": "btn-success",
            "callback": function () {
                if(!validate($("#editPane"))){
                    return false;
                }else{
                	editRequirementRecord1();
                }
                return false;
            }
        }
    ];
    loadUrl("/supplier/annualRequirement_audit?id=" + id);
}


function editRequirementRecord1() {
	
	var urlpara = "/supplier/annualRequirement_save";
	//把表单的数据进行序列化 
	var params = $("#addRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/annualRequirement_index");
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

//批量驳回操作
function rejectAnnualReqs(ids,approvalReply){
	 Dialog.confirm("批量驳回操作提示","确定进行批量驳回操作?","是","否",function(result){
			
			if(result){
	            jQuery.ajax({
	                type:"POST",
	                cache:false,
	                url: springUrl+"/supplier/annualReqAudit_reject",
	                data:{
	                    "ids":ids,
				       "approvalReply":approvalReply
	                },
	                success:function (data) {
	                	Dialog.alert("驳回操作提示","操作成功","确定");
	        			table.fnDraw();
	                },
	                error: function (data) {
	                    Dialog.alert("失败","批量驳回失败");
	                }
	            });
	        }
				

		});
}




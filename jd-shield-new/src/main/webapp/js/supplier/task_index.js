/**
 * 数据字典-数据项列表页面JS
 * User: 
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;

//初始化页面
$(function() {
	bindValidate($('#taskForm'));
    
	var columns =  [
	                {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	                { "mData": "id","sTitle":"任务单id","bSortable":true,"bVisible":false},
	                { "mData": "taskStatus","sTitle":"状态","bSortable":false,"bVisible":true,"sDefaultContent":"","fnRender":function(data){
	                	return "<a href='javascript:void(0);' title='' onclick='getView("+data.aData.id+");'>"+data.aData.statusName+"</a>"
	                }},
	                { "mData": "projectNum","sTitle":"申请编号","bSortable":false,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "projectName","sTitle":"项目名称","bSortable":false,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "budgetAmount","sTitle":"预算金额","bSortable":true,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "applicant","sTitle":"申请人","bSortable":false,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "applicantDep","sTitle":"申请部门","bSortable":false,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "applicantTel","sTitle":"申请人电话","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "projectDate","sTitle":"申请时间","bSortable":true,"bVisible":true,"fnRender":function(data){
	                	return new Date(data.aData.projectDate).format("yyyy-MM-dd hh:mm:ss");
	                }}
	            ];
	var btns=[];
	
	table=$("#taskTable").dataTable({
	"bProcessing": false,
    "bServerSide":true,
    "sPaginationType": "full_numbers",
    "sAjaxSource":springUrl+"/supplier/task_page",
    "sServerMethod": "POST",
    "bAutoWidth": false,
    "bStateSave": false,
    "sScrollY":"100%",
    "bScrollCollapse": true,
    "bPaginate":true,
    "oLanguage": {
        "sLengthMenu": "每页显示 _MENU_ 条记录",
        "sZeroRecords": "抱歉， 没有找到",
        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
        "sInfoEmpty": "没有数据",
        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
        "oPaginate": {
            "sFirst": "首页",
            "sPrevious": "前页",
            "sNext": "后页",
            "sLast": "尾页"}
    },
    "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bJQueryUI": true,
    "bFilter":false,
	"fnServerData":function ( sSource, aoData, fnCallback ) {
			var id=$("#id").val();
			var projectName = $("#projectName").val();
			var projectNum = $("#projectNum").val();
			var taskStatus = $("#taskStatus").val();
			aoData.push( { "name": "id", "value":id },
						 { "name": "projectName", "value":projectName },
						 { "name": "projectNum", "value":projectNum },
						 { "name": "statusCode", "value":taskStatus });
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","数据字典数据查询失败");
                        }
		    });
		},
	"fnDrawCallback": function( oSettings ){
		var that = this;
		this.$('td:first-child').each(function(i){
			that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
	    });          
	},
	"aaSorting":[],//默认排序
	"aoColumns":columns,
	"oTableTools":{
	    "aButtons":btns
	}


	});
	
	//查询
	$('#requirement_search').click(function() {
		if(validate($("#taskForm"))){
			table.fnDraw(); 
		} 
	});

});
	
	function getView(id){
		loadUrl(springUrl+"/supplier/task_view?id="+id);
	}


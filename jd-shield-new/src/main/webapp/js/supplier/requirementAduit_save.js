$(function() {

	$("#requirementAduit_add_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
		}
		addRequirementAduitRecord();
	});
	
	$("#requirementReject_btn").click(function() {
			var requirementId=$("#requirementId").val();
			requirementReject(requirementId);
	});
	
	
	$("#requirement_update_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			updateRequirementRecord();
		}
	});

	
	$("#outputtime").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
});


function addRequirementAduitRecord(e) {
	var urlpara = "/supplier/requirementAduit_save";
	//把表单的数据进行序列化 
	var params = $("#insertRequirementAduitForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/requirementAduit_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}


function updateRequirementRecord(e) {
	var urlpara = "/supplier/requirement_update";
	//把表单的数据进行序列化 
	var params = $("#updateRequirementForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/requirement_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

function requirement_back_btn(){
	loadUrl("/supplier/requirement_index");
}


function requirementReject(requirementId){
	    var buttons = [
	        {
	            "label": "取消",
	            "class": "btn-success",
	            "callback": function () {
	            }
	        },
	        {
	            "label": "确认",
	            "class": "btn-success",
	            "callback": function () {
	                if(!validate($("#addPaneRequirementReject"))){
	                    return false;
	                }else{
	                	var requirementId=$("#requirementId").val();
	                	saveRequirementReject(requirementId);
	                }
	                return false;
	            }
	        }
	    ];
	    Dialog.openRemote('requirementReject_savePage','需求驳回意见', springUrl+'/supplier/requirementReject_savePage?requirementId='+requirementId, 600, 450, buttons);
}




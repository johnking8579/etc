$(function() {

	$("#bidNotice_add_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			if(validateDate()){
				addBidNoticeRecord1();
			}else{
				alert("报名截止时间应早于招标时间");
			}
		}
	});
	
	$("#bidNotice_update_btn").click(function() {
		var validateEl = $('#addPane');
		if (validate(validateEl)) {
			if(validateDate()){
				updateBidNoticeRecord();
			}else{
				alert("报名截止时间应早于招标时间");
			}
		}
	});

	

	$("#biddate,#publishdate,#enddate").datetimepicker({
        format: 'yyyy-mm-dd',
		minView : 'month',
		language:'zh-CN',
		autoclose : true,
		todayHighlight : true
    });
	
	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	
	
});

function validateDate(){
		var biddate=$("#biddate").val();
		var enddate=$("#enddate").val();
	
	    var d1 = new Date(biddate);   
	    var d2 = new Date(enddate);   
	    
	    if(Date.parse(d1) - Date.parse(d2)==0)   
	    {   
	    return false;
	    }   
	    if(Date.parse(d1) - Date.parse(d2)<0)   
	    {   
	    return false;
	    } 
	    if(Date.parse(d1) - Date.parse(d2)>0)   
	    {   
	    return true ;
	    }   
	
}

function addBidNoticeRecord1() {
	$("#requirementName").val($("#requirementId option:selected").text());
	var urlpara = "/supplier/bidNotice_save";
	//把表单的数据进行序列化 
	var params = $("#insertBidNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/bidNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}


function updateBidNoticeRecord() {
	$("#requirementName").val($("#requirementId option:selected").text());
	var urlpara = "/supplier/bidNotice_update";
	//把表单的数据进行序列化 
	var params = $("#updateBidNoticeForm").serialize();
	$.ajax({
		url : urlpara,
		type : "POST",
		data : params,
		dataType : "json",
		success : function(data) {
			alert("保存成功!");
			loadUrl("/supplier/bidNotice_index");
/*			$("#addPane").hide("fast");
			clearAddPane();
			table.fnDraw();
*/			//window.location=tag_ctx+"/jsp/demo/userList.jsp";
		},
		error : function(xhr, st, err) {
			alert("保存失败!");
		}
	});
}

function bidNotice_back_btn(){
	loadUrl("/supplier/bidNotice_index");
}

function showBidNoticeYear(){
	if($("#budgetSource option:selected").text()=='计划内'){
		$("#bidNoticeYearId").attr("style","display:block");
	}else{
	}
}

function copyProjectName(){
	$("#projectName").val($("#bidNoticeYearId option:selected").text());
}



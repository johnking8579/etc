var table;
var tableData;
	$(document).ready(function () {
		var columns = [
		    {"mDataProp": "id","sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},          
		    {"mDataProp": "name", "sTitle": "供应商名称", "bSortable": false,"sWidth":100},
//			{"mDataProp": "userCode", "sTitle": "账号", "bSortable": false,"sWidth":70 },
            {"mDataProp": "mobile", "sTitle": "企业电话", "bSortable": false,"sDefaultContent":"","sWidth":80 },
		    {"mDataProp": "email","sTitle": "企业邮箱", "bSortable": false,"sDefaultContent":"","sWidth":60},
		    {"mDataProp": "realName","sTitle": "推荐人", "bSortable": false,"sDefaultContent":"","sWidth":60},
            {"mDataProp": "recommendStatus", "sTitle":"状态", "bSortable": false,"sWidth":80,
            	 "mRender": function (data) {
                     if (data == 0) {
                         return '推荐中';
                     } else if(data == 1){
                         return '已通知';
                     }else if(data == 2){
                    	 return '推荐成功';
                     }
                 }},
		    {"mDataProp": "id", "sTitle": "操作", "bSortable": false,"sWidth":100,"fnRender": function (data, type, full) {
				var editHtml = '<a href="#"  title="修改" onclick="editRecommend(\'' + data.aData.id + '\')"><img src="' + springUrl + '/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;';
				var delHtml = '<a href="#"   title="删除" onclick="delRecommend(\''+ data.aData.id + '\')"><img src="' + springUrl + '/static/admin/skin/img/del.png" alt="删除"/></a>&nbsp;&nbsp;';
				var examineHtml = '<a href="#"  title="通知供应商" onclick="examineRecommend(\''+ data.aData.id + '\',\''+ data.aData.userCode + '\',\''+ data.aData.email + '\',\''+ data.aData.name + '\',\''+ data.aData.realName + '\');"><img src="' + springUrl + '/static/admin/skin/img/enjoy.png" alt="审核"/></a>&nbsp;&nbsp;';				
				if (data.aData.recommendStatus == 2) {
					return '';
				}else if(data.aData.isAdmin == 1){
					return editHtml + delHtml + examineHtml;
				}else{
					return editHtml + delHtml;
				}
	        }}
        ];      
        var tableBtns= [];      
        table = $('#hello').dataTable( {
            "bProcessing": false,//是否显示正在处理的提示 
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/pageRecommendList",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,//保存状态到cookie ******很重要 ， 当搜索的时候页面一刷新会导致搜索的消失。使用这个属性设置为true就可避免了 
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,// 是否使用分页
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
            	var name=$("input#name").val();
            	var userName=$("input#userName").val();
            	var realName=$("input#realName").val();
            	var recommendContent=$("input#recommendContent").val();
            	
				aoData.push( 
						{ "name": "name", "value":name },
						{ "name": "userName", "value":userName },
						{ "name": "realName", "value":realName },
						{ "name": "recommendContent", "value":recommendContent }
						);   
                jQuery.ajax({
                    type: "POST",
					async:true,
                    url: sSource,
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                    	tableData=resp;
                        fnCallback(resp);
                    }
                });
            },
            "fnDrawCallback": function( oSettings ){
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 0, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        });
        $('#searchBtn').click(function() {
    		if(validate($("#pageform"))){
    			table.fnDraw(); } 
    		}
    	);  
        
	});
	/**
	 * 新增内荐供应商基本信息
	 * @param data
	 */
	function addRecommend() {
		loadUrl(springUrl+"/supplier/supplier_recommend_add");
	}
	/**
	 * 修改供应商基本信息
	 * @param 供应商ID
	 */
	function editRecommend(supplierId) {
		  loadUrl(springUrl +'/supplier/supplier_recommend_update?supplierId=' + supplierId);
	}
	/**
	 * 根据推荐供应商完成度审核信息
	 * @param 供应商ID
	 */
	function examineRecommend(supplierId,userCode,email,name,realName) {
		Dialog.confirm("确认","确定通知吗?","是","否",function(result){
		if(result){	
		  $.ajax({
	            type: "POST",
	            cache: "false",
	            contentType: "application/x-www-form-urlencoded; charset=utf-8", 
	            url: springUrl +'/supplier/supplier_examineRecommend?supplierId=' +supplierId+'&email='+email+'&userCode='+userCode+'&name='+encodeURI(encodeURI($.trim(name)))+'&realName='+encodeURI(encodeURI($.trim(realName))),
	            dataType: "json",
	            success: function (data) { 
	                if (data == 1) { 
	                    alert("已通知!");
	                    loadUrl("/supplier/supplier_recommend");
	                } else {
	                	alert("通知供应商信息异常,请联系管理员!");
	                    return false;
	                }
	            }
	        });
		}
	})
}
	/**
	 * 删除内荐供应商基本信息
	 * @param 供应商ID
	 */
	function delRecommend(supplierId) {
		Dialog.confirm("确认","确定删除吗?","是","否",function(result){
		if(result){	
		 $.ajax({
	            type: "POST",
	            cache: "false",
	            url: springUrl +'/supplier/supplier_recommend_delete?supplierId=' +supplierId,
	            dataType: "json",
	            success: function (data) { 
	                if (data == 1) { 
	                    alert("删除成功");
	                    loadUrl("/supplier/supplier_recommend");
	                } else {
	                	alert("删除异常,请联系管理员!");
	                    return false;
	                }
	            }
	        });
		}
	})
}
	/**
	 * 刷新供应商信息列表
	 */
	function refreshSupplierTable() {
		table.fnDraw();
	}
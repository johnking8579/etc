/**
 * 数据字典-数据类别新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var dictTypeId = $('#id').val();
/**
 * 添加数据类别-确定按钮响应函数
 */
function saveDictType(){
	var validateEl=$('#dictTypeForm');
	if(!validate(validateEl)){
		return false;
	}
	var isRepeatDictTypeCode=$('#isRepeatDictTypeCode').html();
    if(isRepeatDictTypeCode!=''){
  	  return false;
    }
    var isRepeatDictTypeName=$('#isRepeatDictTypeName').html();
    if(isRepeatDictTypeName!=''){
  	  return false;
    }
    //判断当前当前类别CODE是否存在
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
        	 dictTypeCode:$('#dictTypeCode').val(),
             parentId:$('#parentName').val(),
             dictTypeName:$('#dictTypeName').val()
        },
        url:springUrl + "/supplier/systemUpload_isExistDictTypeCode",
        /*beforeSend:function(){//提交前校验
            $("#dictTypeName").trigger("change.validation",{submitting: true});
            $("#dictTypeCode").trigger("change.validation",{submitting: true});
            if($("#dictTypeCode").jqBootstrapValidation("hasErrors") || $("#dictTypeName").jqBootstrapValidation("hasErrors")){
                return false;
            }
        },*/
        success:function (data) {
            if(data.operator == false){
                Dialog.alert("失败",data.message);
            }else{
                //新增数据列别
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        dictTypeName:$('#dictTypeName').val(),
                        dictTypeCode:$('#dictTypeCode').val(),
                        parentId:$('#parentName').val(),
                        description:$('#description').val()
                    },
                    url: springUrl+"/supplier/systemUpload_addSave",
                    success:function (data) {
                        $("#box-right").load(springUrl+"/supplier/systemUpload_view?dictTypeCode="+$('#dictTypeCode').val());
                        if($("#parentName").val() == "48"){
                            refreshNode(null);
                        }
                        
                        else{
                            refreshNode($("#parentName").val());
                        }
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据类别失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }

                    }
                });
            }
        },
        error: function (data) {
            Dialog.alert("失败",data.responseText);
        }
    });
}

/**
 * 添加数据类别-取消按钮响应函数
 */
function cancelDictType(){
    $("#box-right").html("");
    refreshNode($("#parentName").val());
}

/**
 * 初始化操作
 */
$(function() {

	/*类别编码重复性判断*/
	$('#dictTypeCode').blur(function() {
		validateDictTypeCode();
	});
	
	$('#dictTypeCode').focus(function() {
		$("#isRepeatDictTypeCode").html("");
	});
	
	/*类别名称重复性判断*/
	$('#dictTypeName').blur(function() {
		validateDictTypeName();
	});
	$('#dictTypeName').focus(function() {
		$("#isRepeatDictTypeName").html("");
	});
	
     /*$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();*/
    //绑定保存按钮事件
    $('#addDictType').click(function() {
        saveDictType();
    });
    //绑定取消按钮事件
    $('#cancelDictType').click(function() {
        cancelDictType();
    });

    bindValidate($('#dictTypeForm'));
    
   /* $("#dictTypeName").jqBootstrapValidation();
    $("#dictTypeCode").jqBootstrapValidation();*/
});
	
function validateDictTypeCode(){
	var dictTypeCode=$("input#dictTypeCode").val();
	if(dictTypeCode!=''){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictTypeCode:dictTypeCode
	        },
	        url:springUrl + "/supplier/systemUpload_isExistDictTypeCodePage",
	        success:function (data) {
	        	if(data){
	        		var str = "此类别编码已存在!";
	        		$("#isRepeatDictTypeCode").html(str).css("color","red");
	        	}
	        }
		});
	}
}

function validateDictTypeName(){
	var dictTypeParentId=$("#parentName").val();
	var dictTypeName=$("input#dictTypeName").val();
	if(dictTypeName!=''){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				dictTypeName:dictTypeName,
				parentId:dictTypeParentId
	        },
	        url:springUrl + "/supplier/systemUpload_isExistDictTypeNamePage",
	        success:function (data) {
	        	if(data){
	        		var str = "此类别名称已存在!";
	        		$("#isRepeatDictTypeName").html(str).css("color","red");
	        	}
	        }
		});
	}
}

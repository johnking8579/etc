/**
 * 数据字典-数据项列表页面JS
 * User: 
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var table;

//初始化页面
$(function() {
	bindValidate($('#taskParaRelaAddBindingForm'));
    
	var columns =  [
	                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
					"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
					"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
						return "<input type=\"checkbox\" value=\""+data.aData.id+"\" class=\"checkbox\">";
					}},
					{ "mData": "type","sTitle":"类型","bSortable":true,"bVisible":true,"sDefaultContent":"","mRender":function(data, type, full){
	                    switch(data){
	                    case 1:
	                    	return '任务节点';
	                    case 2:
	                    	return '角色';
	                    case 3:
	                    	return '部门';
	                    }
						return '';
	                }},
	                { "mData": "code","sTitle":"编码","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                { "mData": "name","sTitle":"名称","bSortable":false,"sClass": "my_class","sDefaultContent":''},
	                { "mData": "url","sTitle":"链接","bSortable":false,"bVisible":true,"sDefaultContent":""},
	                {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
	                    return "<a href='#' title='修改'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'/ onclick='updateTaskParaRela(\""+data+"\");'></a>&nbsp;"+
	                    "<a href='#' title='删除'><img src='"+springUrl+"/static/admin/skin/img/del.png' alt='删除' onclick='deleteTaskParaRela(\""+data+"\");'/></a>";
	                }}
	            ];
	var btns=[
	          {
	              "sExtends":    "text",
	              "sButtonText": "绑定角色部门",
	              "sButtonClass": "btn btn-green mr10",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	            	  Dialog.hide();
	            	  addBinding(1);
	              }
	          }
	          ];
	
	table=$("#taskParaRelaBindingTable").dataTable({
	"bProcessing": false,
    "bServerSide":true,
    "sPaginationType": "full_numbers",
    "sAjaxSource":springUrl+"/supplier/taskPara_page",
    "sServerMethod": "POST",
    "bAutoWidth": false,
    "bStateSave": false,
    "sScrollY":"100%",
    "bScrollCollapse": true,
    "bPaginate":true,
    "oLanguage": {
        "sLengthMenu": "每页显示 _MENU_ 条记录",
        "sZeroRecords": "抱歉， 没有找到",
        "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
        "sInfoEmpty": "没有数据",
        "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
        "oPaginate": {
            "sFirst": "首页",
            "sPrevious": "前页",
            "sNext": "后页",
            "sLast": "尾页"}
    },
    "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "bJQueryUI": true,
    "bFilter":false,
	"fnServerData":function ( sSource, aoData, fnCallback ) {
			var taskId=$("#taskId").val();
			var name = $("#searchName").val();
			var typeName = $("#searchType").val();
			aoData.push( { "name": "taskId", "value":taskId },
						 { "name": "name", "value":name },
						 { "name": "typeName", "value":typeName });
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","数据字典数据查询失败");
                        }
		    });
		},
	"fnDrawCallback": function( oSettings ){
		var that = this;
	},
	"aaSorting":[],//默认排序
	"aoColumns":columns,
	"oTableTools":{
	    "aButtons":btns
	}


	});
	
	//查询
	$('#requirement_search').click(function() {
		if(validate($("#searchForm"))){
			table.fnDraw(); 
		} 
	});

});
	
	

/**
 * 新增参数
 * @param type
 */	            	  
function addBinding(type){
	Dialog.openRemote('taskParaRela_addBinding','新增参数',springUrl+"/supplier/taskParaRela_addBinding?type="+type,600,400,[
		{
		    "label": "取消",
		    "class": "btn-success",
		    "callback": function() {}
		},
		{
		    "label": "保存",
		    "class": "btn-success",
		    "callback": function() {
		    	if(!validate($('#taskParaAddForm'))){
						return false;
				}
		    	batchAdd();
		    }
		}
		]);                                                                                                               
}

/**
 * 保存参数
 * @param type
 */
function addBindingSave(type){
	
	jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/supplier/taskParaRela_addBindingSave",
        data:$('#taskParaRelaAddBindingForm').serialize(),
        success:function (data) {
            Dialog.hide();
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}


function binding(type){
	Dialog.openUrl("taskPara_binding", 
			"新增绑定", "/supplier/taskPara_addBinding?type="+type, 600, 400, 
			[
			 {
				 "label": "取消",
			     "class": "btn-success",
				 "callback": function() {}
			 },
			 {
				 "label": "保存",
				 "class": "btn-success",
			     "callback": function() {
			     if(!validate($('#taskParaAddForm'))){
							return false;
				 }
			     addBinding(type);
			     }
			 }
			 ]);
}

//function addBinding(type){
//	 var taskId = $("#taskId").val();
//	 jQuery.ajax({
//	        type:"POST",
//	        cache:false,
//	        url: springUrl+"/supplier/taskPara_saveBinding",
//	        data:$('#taskParaAddForm').serialize(),
//	        success:function (data) {
//	            Dialog.hide();
//	        },
//	        error: function (data) {
//	            Dialog.alert("失败","数据字典数据更新失败");
//	        }
//	    });
//	 
//}
/**
 * 删除单条预选信息
 */
function deleteTaskParaRela(id){
    Dialog.confirm("确认","确定删除数据字典数据项?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/supplier/taskParaRela_delete",
                data:{
                    id:id
                },
                success:function (data) {
                	if(typeof(data) != 'object')
                		data = eval("("+data+")");
                	if (data.operator == true){
                		Dialog.alert("成功！",data.message);
                		loadUrl(springUrl+"/supplier/taskParaRela_index?taskId="+$("input#taskId").val());
                	}else{
                    	Dialog.alert("失败！",data.message);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","系统异常");
                }
            });
        }
    });
}

//批量添加
function batchAdd(){
	var taskId = $("#taskId").val();
	var roleId = $("#roleId_ad").val();
	var organizationId = $("#organizationId_ad").val();
	var checkArry = $(".checkbox:checked");
	 var ids="";
	 checkArry.each(function(){
		 ids += ","+ $(this).val();
	 });
	 ids = ids.substr(1,ids.length);
    var length = ids.length;
    ids = ids.toString();
    if(length==0){
        Dialog.alert("消息提示","没有选中项","确定");
    }else{
    	Dialog.confirm("确认","确定添加绑定吗?","是","否",function(result){
    		if(result){
    				jQuery.ajax({
    					type:"POST",
    					cache:false,
    					url: springUrl+"/supplier/taskParaRela_addBindingSave",
    					data:{
    						ids:ids,
    						taskId:taskId,
    						roleId:roleId,
    						organizationId:organizationId
    					},
    					success:function (data) {
    						if(typeof(data) != 'object')
    							data = eval("("+data+")");
    						if (data.operator == true){
    							Dialog.alert("成功！",data.message);
    							loadUrl(springUrl+"/supplier/taskParaRela_bindingIndex?taskId="+$("input#taskId").val());
    						}else{
    							Dialog.alert("失败！",data.message);
    						}
    					},
    					error: function (data) {
    						Dialog.alert("失败","系统异常");
    					}
    				});
    		}
    	});
    }
}

function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}

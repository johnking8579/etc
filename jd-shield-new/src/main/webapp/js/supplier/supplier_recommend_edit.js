
function save() {
	if(validate($("#form1"))){
		edit(); } 
	}

function back() {
	loadUrl("/supplier/supplier_recommend");
}

function edit() {
	Dialog.confirm("确认","确定修改吗?","是","否",function(result){
	if(result){	
		var urlpara = "/supplier/supplier_recommend_edit";
		var params = $("#form1").serialize();
		$.ajax({
			url : urlpara,
			type : "POST",
			data : params,
			dataType : "json",
			success : function(data) {
				if(data == 1){
					bootbox.alert("修改数据成功!");
				 loadUrl("/supplier/supplier_recommend");
				}else{
					bootbox.alert("操作异常,请联系管理员!");
					return false;
				}
			}
		
		});
	 }
	})
}
var table;
var tableData;
$(function(){ 
//新的加载方式   begin//
var columns = [
                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" name='system' value=\""+data.aData.id+"\" fileName=\""+data.aData.fileName+"\" class=\"checkbox\">";
	                			}},
	            {"sTitle":"序号","sDefaultContent": "","sWidth": "50px","sDefaultContent":"","bSortable": true},
	            {"mData": "fileName", "sDefaultContent":"","sTitle": "文件名称" , "bSortable": true , "fnRender": function (obj) {
					return '&nbsp;&nbsp;<a href="#" title="打开" style="font-size:12px;" onclick="showSystem(this,\''+obj.aData.fileKey+'\')" >'+obj.aData.fileName+'</a>';		        	
		        }
	            },
	            {"mData": "createTime", "sDefaultContent":"","sTitle": "上传时间" , "bSortable": true , "fnRender": function (obj) {
					return parseDate(obj.aData.createTime);		        	
		        }
	            },
		        {"mData": "id", "sDefaultContent":"","sTitle": "操作", "bSortable": false,"fnRender": function (obj) {
		        	return '<a style="font-size:12px;" onclick="showSystem(this,\''+obj.aData.fileKey+'\')" title="查看" href="javascript:void(0)" >查看</a>'  +
		        	'&nbsp;&nbsp;<a style="font-size:12px;" title="删除" onclick="deleteSystems('+obj.aData.id+')" href="javascript:void(0)">删除</a>&nbsp;&nbsp;';
		        	
		        	
		        }
		        }
	        ];


 table = $('#tenderSystemTable').dataTable( {
	 		"sDefaultContent":"requirement",
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/supplier/tenderSystemPage",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
				var fileName=$("input#fileName").val();
				if(fileName==null) fileName="";
				fileName=fileName.replace("_","\\_");
				var dictTypeId=$("input#dictTypeId").val();
				if(dictTypeId==null) dictTypeId="";
				dictTypeId=dictTypeId.replace("_","\\_");
				
				aoData.push( 
						{ "name": "fileName", "value":fileName },
						{ "name": "dictTypeId", "value":dictTypeId }
						);  
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	tableData=resp;
		                        fnCallback(resp);
		                    }
			    });


            },
            "fnDrawCallback": function( oSettings ){
            	//alert(oSetting);
            	
            	//alert("ddd");
				/*添加回调方法*/
            	var that = this;
            	this.$('td:eq(1)').each(function(i){
            		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
                }); 
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":""
                
            }
        } );


	$('#expression_search').click(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); } 
		}
	);
	

	
	$('#fileName').keyup(function() {
		if(validate($("#supplierSearchForm"))){
			table.fnDraw(); 
		} 
	} );
		
	//批量删除验证
	$('#all_Delete').click(function() {
				var ids="";
				$('input[name=system]').each(function() {
		            	
					if ($(this).attr('checked')=="checked") {
							ids+=$(this).val()+",";
					}
					});
				if(ids.length==0){
					Dialog.alert("消息提示","没有选中项","确定");
				}else{
					deleteSystems(ids);
		                }
				return false;
	    	}
		
	);
	

	
	//getBaseControlInfo();
	bindValidate($('#addPane'));
	bindValidate($("#supplierSearchForm"));
});

function toggleChecks(obj){
    $('.checkbox').prop('checked', obj.checked);
}


function parseDate(data){
	 if(data!="")
return new Date(data).format('yyyy-MM-dd hh:mm:ss');
else return "";
}


//批量删除
function deleteSystems(ids){
	 Dialog.confirm("操作提示","确定进行删除操作?","是","否",function(result){
			if(result){
	            jQuery.ajax({
	                type:"POST",
	                cache:false,
	                url: springUrl+"/supplier/AffixFile_delete",
	                data:{
	                    "ids":ids
	                },
	                success:function (data) {
	                	Dialog.alert("操作提示",data.message,"确定");
	        			table.fnDraw();
	                },
	                error: function (data) {
	                    Dialog.alert("失败",data.message,"确定");
	                }
	            });
	        }
				

		});
}

//查看文件
function showSystem(o,fileKey){
	var fileName=$(o).parent().siblings().eq(0).find("input").attr('fileName');
	var url=springUrl+"/supplier/downFilename?filename="+fileName+"&filekey="+fileKey;
	window.location.href=url;
}




//将文件信息保存到数据库中
function insertAffixFile() {
	
	var aoData=[];
	var urlpara = "/supplier/insertAffixFile";
	var str=$("#fileNameBack").val();
	var fileType=$("#fileType").val();
		var tempStr=str.split("@@");
		aoData.push( 
				{ "name": "fileName", "value":tempStr[0] },
				{ "name": "fileKey", "value":tempStr[1] },
				{ "name": "fileType", "value":fileType },
				{ "name": "bussineType", "value":"3" }
				);  
	
	$.ajax({
		url : urlpara,
		type : "POST",
		dataType : "json",
		data : aoData,
		
		success : function(data) {
			table.fnDraw();
		},
		error : function(xhr, st, err) {
		}
	});
}





function uploadifyUpload(){
	$('#btn_upload').uploadifySettings('scriptData');
	$('#btn_upload').uploadifyUpload();
}

function showUpload(){
if($("#fileType").attr("checked")=="checked"){
	$("#uploadifyUploader").attr("style","visibility:visible;");
	$('#uploadifyStart').attr("style","display:block");
	$('#uploadifyCancel').attr("style","display:block");
}else{
	$("#uploadifyUploader").attr("style","visibility:hidden;");
	$('#uploadifyStart').attr("style","display:none");
	$('#uploadifyCancel').attr("style","display:none");
}

}



$(function (){
	var filename="";
		$("#btn_upload").uploadify({
		    'uploader'       : '/static/supplier/upload/uploadify.swf',
		    'script'         : '/servlet/fileUpload',
		    'cancelImg'      : '/static/supplier/images/cancel.png',
		    'buttonImg'      : '/static/supplier/images/btn_upload.png',
		    'queueID'        : 'fileQueue',
		    'auto'           : false,
		    'multi'          : true,
		    'fileDesc'		:'*.pdf;*.doc;*.docx',//上传文件类型说明  
		   'fileExt'		:'*.pdf;*.doc;*.docx',
		    'buttonText'     : '',
		                
		    onComplete: function (evt, queueID, fileObj, response, data) {
		    		if(response=="0")
		    			{
		    				alert("文件上传失败");
			    			return false;
		    			}else{
		    			filename=response;
					    	$("#fileNameBack").val(filename);
					    	insertAffixFile();
		    			}
			    	
			},
			onError: function(a, b, c, d) {
		         alert("文件上传失败");
				},
			onAllComplete : function (evt ,data){
					
			}
		});
		
});







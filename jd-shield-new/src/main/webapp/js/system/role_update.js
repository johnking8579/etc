$(document).ready(function(){
	var editRoleTypeValue=$("#editRoleTypeValue").val();
	$("#editRoleType").val(editRoleTypeValue);
	
	$('#editRoleCode').blur(function() {
		validateRoleCode();
	});
	
	$('#editRoleCode').focus(function() {
		$("#isRepeatRoleCode").html("");
	});
	
    bindValidate($("#editRole"));// 绑定验证
});

function validateRoleCode(){
	var roleCode=$("input#editRoleCode").val();
	var roleCodeOld=$("input#roleCodeOld").val();
	if(roleCode!='' && roleCode != roleCodeOld){
		jQuery.ajax({
			type:"POST",
			cache:false,
			dataType : 'json',
			data:{
				roleCode:roleCode
	        },
	        url:springUrl + "/system/role_checkRoleCodeUnique",
	        success:function (data) {
	        	if(data>0){
	        		var str = "此编码已存在!";
	        		$("#isRepeatRoleCode").html(str).css("color","red");
	        	}
	        }
		});
	}
}

function modify(model) {
    jQuery.ajax({
        type: "POST",
        cache: false,
        url:springUrl+"/system/role_updateSave.json",
        data: {
        	id:$('#editRoleId').val(),
            roleType:$('#editRoleType').val(),
            roleName:$('#editRoleName').val(),
            roleCode:$('#editRoleCode').val()
        },
        success: function (msg) {
        	if(msg.operator){
	            Dialog.alert("提示信息","修改成功！");
	            Dialog.hideModal(model);
	            table.fnDraw();
          }
        },
        error: function (msg) {

        }
    });
}
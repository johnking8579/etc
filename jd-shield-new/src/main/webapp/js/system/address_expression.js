var defaultSetting = {
	callback : defaultCallBackFunction
};
var orgTree;
//根节点 folders 传入参数
var zNodes = {
	    "id" : null,
	    "name" : "组织机构",
	    "isParent" : true,
	    "iconSkin" : "diy1"
	};
var setting = {
	async : {
		enable : true,
		url : springUrl + "/system/address_getTreeNodeDataJson",
		autoParam : ["id"]
	},
	check: {
		enable : false
	},
	 callback : {
        onClick : treeNodeClick
    },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine : true,
		selectedMulti : false,
		expandSpeed : "fast"
	}
};

/**
 * 初始化操作
 */
var expressionDataTable;
var dataTable;
var source;//记录是单选还是复选
$(function() {
    source =$("#source").val();
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");

    var columns;
    if(source=='radio'){
        columns =  [
            {  "mDataProp": "sid","sTitle":"","sDefaultContent":"","bSortable":false,"bVisible":true,"sWidth": "20px","fnRender" : function(obj) {
                return '<input type="radio" id="radio" name="radio" autheName="'+obj.aData.authExpressionName+'" value="'+obj.aData.id+'"/>';
            }},
            { "mDataProp": "authExpressionName","sTitle":"表达式名称","bSortable":true,"bVisible":true,"sDefaultContent":""},
            {  "mDataProp": "mode","sTitle":"表达式","bSortable":true,"bVisible":true,"fnRender":function(obj){
                return describeExpressMap(obj.aData);
            }}
        ];
    }else{
        columns =  [
            {  "mDataProp": "sid","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>","sDefaultContent":"","bSortable":false,"bVisible":true,"sWidth": "20px","fnRender" : function(obj) {
                return '<input type="checkbox" id="checkbox" name="checkbox" class="checkbox" autheName="'+obj.aData.authExpressionName+'" value="'+obj.aData.id+'"/>';
            }},
            { "mDataProp": "authExpressionName","sTitle":"表达式名称","bSortable":true,"bVisible":true,"sDefaultContent":""},
            {  "mDataProp": "mode","sTitle":"表达式","bSortable":true,"bVisible":true,"fnRender":function(obj){
                return describeExpressMap(obj.aData);
            }}
        ];
    }

	var  btns = [];
	

	expressionDataTable = $('#expressionDataTable').dataTable( {
	           
	        	"bProcessing": false,
	            "bServerSide":true,
	            "sPaginationType": "full_numbers",
	            "sAjaxSource":springUrl+"/system/authExpression_page",//address_getOrgUserDataJson",
	            "sServerMethod": "POST",
	           
	            "bStateSave": false,
	            "sScrollY":"100%",
	            "bScrollCollapse": true,
	            "bPaginate":true,
	            "bAutoWidth":true,
	            "oLanguage": {
	                "sLengthMenu": "每页显示 _MENU_ 条记录",
	                "sZeroRecords": "抱歉， 没有找到",
	                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
	                "sInfoEmpty": "没有数据",
	                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
	                "oPaginate": {
	                    "sFirst": "首页",
	                    "sPrevious": "前页",
	                    "sNext": "后页",
	                    "sLast": "尾页"}
	            },
	            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span11'i><'span11'p>>",
	            "sPaginationType": "bootstrap",
	            "bJQueryUI": true,
	            "bFilter":false,
	            "fnServerData":function ( sSource, aoData, fnCallback) {

					var mode=$("input#mode").val(); //获取查询用户姓名
					var authExpressionName = $("#authExpressionName").val();
					aoData.push( { "name": "mode", "value":mode },{ "name": "authExpressionName", "value" : authExpressionName });
					jQuery.ajax( {
	                    type: "POST",
	                    url : sSource, 
	                    dataType : "json",
	                    data : aoData, 
	                    success : function(resp) {
							//Table.currentPage=resp.current_page;
	                    	dataTable=resp;
							   fnCallback(resp);
	                    },
						error : function(data){
							
						}
					});
	            },
	            "fnDrawCallback": function( oSettings ){
					/*添加回调方法*/
	            },
	            "aaSorting":[],//默认排序
	            "aoColumns":columns,
	            "oTableTools":{
	                "aButtons":btns
//	                "sRowSelect":"multi"
	            }
	        } );

	
	document.getElementById('mainPanel').style.display = '';
	
	//搜索按钮
	$('#searchExpression').click(function() {
		expressionDataTable.fnDraw();
    });
	/*$('#authExpressionName').keyup(function() {
		expressionDataTable.fnDraw();	
	} );*/
	
	$("#tree_open_arrow").click(function(){
		if($("#tree_org").css("display") == "none"){
			$("#tree_org").css("display","");
			$("#tree_open_arrow img").attr('src','/static/admin/skin/img/tabs_rightarrow.png');
		}else{
			$("#tree_org").css("display","none");
			$("#tree_open_arrow img").attr('src','/static/admin/skin/img/tabs_leftarrow.png');
		}
	});
});	
	



function treeNodeClick(treeId, treeNode) {
	$("input#authExpressionName").val("");
	expressionDataTable.fnDraw();
}

function callBack(modal){
    var authArray = new Array();
    var authorizeType=$('input[name="subjectradio"]:checked').val();
    if(source=='radio'){
        var radio =$('input[name="radio"]:checked');
        if(radio.index()>=0){
            var id =radio.val();
            var autheName=redio.attr("autheName");
            var mode=radio.parent().next().next().html();
            authArray.push({id:id,authName:autheName,mode:mode,authorizeType:authorizeType});
        }
    }else{
        $('input[name="checkbox"]:checked').each(function(){
            var id = $(this).val();
            var autheName=$(this).attr("autheName");
            var mode=$(this).parent().next().next().html();
            authArray.push({id:id,authName:autheName,mode:mode,authorizeType:authorizeType});
        });

    }

    if(authArray.length<=0){
        Dialog.alert("提示", "请选择权限表达式数据记录!");

    }else{
        setAuthExpressionInfo(authArray);
        Dialog.hideModal(modal);
    }
}

function cancel(){
	Dialog.hide();
}
function defaultCallBackFunction(selectRecords){
	 Dialog.alert("提示", "请定义【确定】按钮的回调函数!");
	 return;
}
function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}

function getUrlParam(wlocation,name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = wlocation.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	}

//解释表达式
function describeExpressMap(expression){
	var nameVlaue;
	$.each(dataTable.names,function(i,n){
		if(n.id==expression.id){
			nameVlaue=n;
			return false;
		}
	});
	
	var str_buf="";
	str_buf+="+[部门："+(nameVlaue.organizationName==null?"不限":nameVlaue.organizationName)+"]";
	str_buf+="+[职位："+(nameVlaue.positionName==null?"不限":nameVlaue.positionName)+"]";
	if(nameVlaue.levelUpName!= null && nameVlaue.levelDownName == null){
		str_buf+="+[职级："+nameVlaue.levelUpName+"以上]";
	}else if(nameVlaue.levelUpName == null && nameVlaue.levelDownName != null){
		str_buf+="+[职级："+nameVlaue.levelDownName+"以下]";
	}else if(nameVlaue.levelUpName != null && nameVlaue.levelDownName != null){
		if(nameVlaue.levelUpName == nameVlaue.levelDownName){
			str_buf+="+[职级："+nameVlaue.levelUpName+"]";
		}else{
			str_buf+="+[职级："+nameVlaue.levelUpName+"-"+nameVlaue.levelDownName+"]";
		}
	}else{
		str_buf+="+[职级：不限]";
	}
	str_buf+="+[用户："+(nameVlaue.userName==null?"任何人":nameVlaue.userName)+"]";
	str_buf+="+[角色："+(nameVlaue.roleName==null?"不限":nameVlaue.roleName)+"]";
	str_buf+="+[用户组："+(nameVlaue.groupName==null?"不限":nameVlaue.groupName)+"]";
	str_buf=str_buf.substring(1);
	return str_buf;
}
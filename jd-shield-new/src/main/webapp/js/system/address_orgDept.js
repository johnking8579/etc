var orgTree;
//根节点 folders 传入参数
var zNodes = {
	    "id" : null,
	    "name" : "组织机构",
	    "isParent" : true,
	    "iconSkin" : "diy1"
	};
var check = {
	enable : true
};
if(!isMulti){
	check.chkStyle = "radio";
	check.radioType = "all";
}
var setting = {
	async : {
		enable : true,
		url : springUrl + "/system/address_getTreeNodeDataJson",
		autoParam : ["id"]
	},
	check: check,
	 callback : {
        //onClick : treeNodeClick
    },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine : true,
		selectedMulti : false,
		expandSpeed : "fast"
	}
};
var zTreeHeight =jQuery("#mainPanelTreeDept").parent("div").height();
jQuery("#orgTree").height(zTreeHeight - 10);
/**
 * 初始化操作
 */
$(function() {
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
	document.getElementById('mainPanel').style.display = '';
});



function callBack(modal){
    var orgArray = new Array();
    var selectRecords = orgTree.getCheckedNodes();
    var authorizeType=$('input[name="subjectradio"]:checked').val();
    if(selectRecords.length <= 0){
        Dialog.alert("提示", "请选择组织机构数据记录!");
    }else{
     if(!isMulti){
                var id =selectRecords[0].props.organization.id;
                var fullName=selectRecords[0].props.organization.organizationFullname;
                var orgName=selectRecords[0].props.organization.organizationName;
                var orgCode=selectRecords[0].props.organization.organizationCode
                orgArray.push({id:id,fullName:fullName,orgCode:orgCode,orgName:orgName,authorizeType:authorizeType});

        }else{
            for(var i=0;i<selectRecords.length;i++) {
                if(selectRecords[i].id!=null){
                    var id =selectRecords[i].props.organization.id;
                    var fullName=selectRecords[i].props.organization.organizationFullname;
                    var orgName=selectRecords[i].props.organization.organizationName;
                    var orgCode=selectRecords[i].props.organization.organizationCode
                    orgArray.push({id:id,fullName:fullName,orgCode:orgCode,orgName:orgName,authorizeType:authorizeType});
                }
            }
        }

        setOrganizationInfo(orgArray);
        Dialog.hideModal(modal);
    }
}

function cancel(){
	Dialog.hide();
}
function defaultCallBackFunction(selectRecords){
	 Dialog.alert("提示", "请定义【确定】按钮的回调函数!");
	 return;
}

﻿var table;
    $(document).ready(function () {
        var columns = [
        	{"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
          	{"mData": "operationCode", "sTitle": "操作编码", "bSortable": true },
            {"mData": "operationName", "sTitle": "操作名称" , "bSortable": true},
            {"mData": "description", "sTitle": "描述" , "bSortable": false},
            {"mData": "id", "sTitle": "操作", "bSortable": false,"mRender": function (data, type, full) {
                var render = '<a href="#" class="btn" title="编辑" onclick="editOperation(\'' + data + '\')" ><img src="' + springUrl + '/static/admin/skin/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;'
                        + '<a href="#" class="btn " title="删除" onclick="deleteOperation(\'' + data + '\')"><img src="' + springUrl + '/static/admin/skin/img/del.png" alt="删除"/></a>';
                return render;
            }}
        ];

        var tableBtns= [
            {
                "sExtends": "text",
                "sButtonText": "新增",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    var buttons = [
                        {
                            "label": "取消",
                            "class": "btn-success",
                            "callback": function (modal) {
                            }
                        },
                        {
                            "label": "保存",
                            "class": "btn-success",
                            "callback": function (modal) {
								if(!validate($("#addOperation"))){
									return false;
								}else{
									save(modal);
								}
								return false;//不让弹窗自己关闭
                            }
                        }
                    ];
                    Dialog.openRemote('operation_add','新增', springUrl+'/system/operation_add', 600, 400, buttons);
                }
            }
        ];

        table = $('#sysoperation').dataTable( {
            "bProcessing": false,
            "bServerSide":true,
            "sPaginationType": "full_numbers",
            "sAjaxSource":springUrl+"/system/operation_list",
            "sServerMethod": "POST",
            "bAutoWidth": false,
            "bStateSave": false,
//					 "sScrollX": "100%",    //开启水平排序
            "sScrollY":"100%",
            "bScrollCollapse": true,
            "bPaginate":true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前页",
                    "sNext": "后页",
                    "sLast": "尾页"}
            },
            //"sDom": "<'row-fluid'<'span6'Tl><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "bJQueryUI": true,
            "bFilter":false,
            "fnServerData":function (sSource, aoData, fnCallback) {
                var operationName = $("input#operationName").val();
                var operationCode = $("input#operationCode").val();
                aoData.push({ "name": "operationName", "value": operationName },{ "name": "operationCode", "value": operationCode });

                jQuery.ajax({
                    type: "POST",
					async:true,
                    url: sSource,
                    dataType: "json",
                    data: aoData,
                    success: function (resp) {
                        fnCallback(resp);
                    }
                });
            },
            "fnDrawCallback": function( oSettings ){
				var that = this;                        
				//if ( oSettings.bSorted || oSettings.bFiltered ){                
				this.$('td:first-child', {"filter":"applied"}).each( function (i) {                    
					that.fnUpdate( i+1, this.parentNode, 0, false, false );                
				} );            
				//}         
            },
            "aaSorting":[],//默认排序
            "aoColumns":columns,
            "oTableTools":{
                "aButtons":tableBtns
            }
        } );
//        table.rowReordering();
    });
    function editOperation(id) {
        var buttons = [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function () {
                }
            },
            {
                "label": "保存",
                "class": "btn-success",
                "callback": function (modal) {
                    if(!validate($("#updateOperation"))){
                        return false;
                    }else{
                        modify(modal);
                    }
                    return false;
                }
            }
        ];
        Dialog.openRemote('operation_update','编辑操作', springUrl+'/system/operation_update?id=' + id, 550, 400, buttons);
    }

    function deleteOperation(id) {
        Dialog.confirm("提示", "确定要删除该条记录吗?", "是", "否", function (result) {
            if (result) {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    url: springUrl+"/system/operation_delete",
                    data: {
                        id: id
                    },
                    success: function (msg) {
						table.fnDraw();
                    },
                    error: function (msg) {

                    }
                });
            }
        });
    }
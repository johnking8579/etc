/**
 * 数据字典-数据项新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 初始化操作
 */

$(function() {
    bindValidate($('#updateDataPermission'));
    var entityType = $("#entityType").val();
    $.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
            dictTypeCode: "ENTITY_TYPE"//之后改成数据字典中对应的业务对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var entity = '<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                    entity +='<option value="'+json[i].dictDataCode+'">'+json[i].dictDataName+'</option>';
                }
            }
            $("#entityType").html(entity);
            $("#entityType").val(entityType);
        }
    });
    
    $.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/dict/dictData_findDictDataList",
        data: {
        	dictTypeCode: "AUTHORIZE_SUBJECT"//之后改成数据字典中对应的可授权主体对象字典类型编码
        },
        dataType: "json",
        success: function(json){
            var ary = new Array();
            var entity = '<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                	if(json[i].dictDataCode=='Organization'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectOrgnaztion();return false;">'+json[i].dictDataName+'</option>';
                	}else if(json[i].dictDataCode=='AuthExpression'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSysExpression();return false;">'+json[i].dictDataName+'</option>';
                	}else if(json[i].dictDataCode=='Group'){
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSysWorkGroup();return false;">'+json[i].dictDataName+'</option>';
                	}else{
                		entity +='<option value="'+json[i].dictDataCode+'" onclick="selectSys'+json[i].dictDataCode+'();return false;">'+json[i].dictDataName+'</option>';
                	}
                		  
                }
            }
            $("#subjectType").html(entity);
        }
    });


    $.ajax({
        type: "POST",
        cache:"false",
        url: springUrl+"/system/dpsubject_list",
        data: {
            dataPermissionId: dataId
        },
        dataType: "json",
        success: function(json){
            tableInfo(json);
        }
    });


});



function modify(modal) {
    var subjectType = "";
    var subjectId = "";
    var entityType = $('#entityType').val();
    var entityName = $("#entityName").val();
    var permissionType = $('#permissionType').val();
    var expression = $("#expression").val();

    if(permissionType==""||expression==""){
        Dialog.alert("提示","请将必填项填写完整");
        return false;
    }

    $('#info input[type="hidden"]').each(function(){
        subjectType+=$(this).attr("valueType")+";";
        var ids = $(this).val();
        subjectId+=ids.substr(0,ids.length-1)+";";
    });

    if(subjectId!=""){
        subjectType =subjectType.substr(0,subjectType.length-1);
        subjectId=subjectId.substr(0,subjectId.length-1);
    }else{
        Dialog.alert("提示","请选择主体对象");
        return false;
    }

    jQuery.ajax({
        type: "POST",
        cache: false,
        url: springUrl+"/system/dataPermission_save",
        data: {
            id: $('#id').val(),
            entityType:entityType,
            entityName:entityName,
            subjectType:subjectType,
            subjectIds:subjectId,
            permissionType:permissionType,
            expression:expression
        },
        success: function (msg) {
            Dialog.alert("提示信息","修改成功！");
            Dialog.hideModal(modal);
            table.fnDraw();
        },
        error: function (msg) {

        }
    });
}

function selectChange(obj){
    var index = obj.selectedIndex;
    var value = obj.options[index].value;
    $("#permissionType").attr("value",value);
}



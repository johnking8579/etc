var table_userPop;
$(document).ready(function() {
var options_userPop={
		pageUrl:springUrl+"/system/expression_findUserList",
		useCheckbox:false,
		sendData:function ( sSource, aoData, fnCallback ) {
			var realName=$("input#realName").val();
			if(realName==null) realName="";
			aoData.push( { "name": "realName", "value":realName } );
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(resp) {
	                            fnCallback(resp);
	                    }
		    });
		},
		columns: [
            { "mDataProp": "userCode","sTitle":"用户编码","bSortable":true,"bVisible":true},
			{ "mDataProp": "realName","sTitle":"用户姓名","bSortable":true,"bVisible":true},
			{ "mDataProp": "organizationFullname","sTitle":"部门","bSortable":true,"bVisible":true},
            { "mDataProp": "positionName","sTitle":"职位","bSortable":true,"bVisible":true},
			{ "mDataProp": "id","sTitle":"操作","fnRender":function(obj){
					var userId = obj.aData.id;
					var realName = obj.aData.realName;
					return "<a href='#' class='btn btn-mini btn-success' onclick='setMainId(\""+userId+"\",\""+realName+"\");'>选择</a>";
			}}
        ],
		btns:[]
}
table_userPop=Table.dataTable("userList",options_userPop);
$('#search_userPop').click(function() {Table.render(table_userPop); } );
$('#realName').keyup(function() {Table.render(table_userPop); } );
});

function setMainId(obj1,obj2){	
	setUserName(obj1,obj2);
	Dialog.hide($('#realName'));
}
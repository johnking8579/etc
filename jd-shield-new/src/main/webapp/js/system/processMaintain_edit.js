/**
 * @Description: 流程实例当前审批人的修改页面JS
 * @author:yujiahe
 * 
 */
//保存修改  流程实例当前审批人的修改
function editSave(count){
    var taskId=$("#id"+count).val(); //待重新分配的任务Id
    var origAssignee=$("#candidateUsers"+count).val();//当前的任务分配用户
    var newAssignee=$("#newAssignee"+count).val();//新的任务分配用户，必须为小写
    var data={
            "taskId":taskId,
            "origAssignee":origAssignee,
            "newAssignee":newAssignee
    };
    $.ajax({
        url:springUrl+"/system/processMaintain_editSave",
        async:true,
        type:"get",
        data:data,
        dataType : 'json',
        cache:false,
        complete:function(){},
        success: function(result){
            if( result.operator == "success"){
                Dialog.alert("请求结果",result.message);
                Table.render(PMTable);
            }
        },
        error : function(data) {
            Dialog.alert("请求异常",result.message);
        }
    });
}
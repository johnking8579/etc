var defaultSetting = {
	isMulti : false,
	callback : defaultCallBackFunction
};
var orgTree;
//根节点 folders 传入参数
var zNodes = {
	    "id" : null,
	    "name" : "组织机构",
	    "isParent" : true,
	    "iconSkin" : "diy1"
	};
var setting = {
	async : {
		enable : true,
		url : baseUrl + "/system/sysAddress_getTreeNodeDataJson",
		autoParam : ["id"]
	},
	check: {
		enable : false
	},
	 callback : {
        onClick : treeNodeClick
    },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine : true,
		selectedMulti : false,
		expandSpeed : "fast"
	}
};
var zTreeHeight = getHeight();
jQuery("#orgTree").height(zTreeHeight - 80);

/**
 * 初始化操作
 */
var orgUserDataTable;
$(function() {
	//加载组织机构树
//	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
//	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
	orgUserDataTable = initOrgUserDataTableComponent();
	
	//隐藏dataTable 数据条目信息
	jQuery(".dataTables_info").parent().hide();
	
	document.getElementById('loading').style.display = 'none';
	document.getElementById('mainPanel').style.display = '';
	
	//搜索按钮
	$('#searchUser').click(function() {
        Table.render(orgUserDataTable);
    });
	$('#userName').keyup(function() {Table.render(orgUserDataTable); } );
});	
function initOrgUserDataTableComponent(){
	var options = Table.options;
	options = {
//			bPaginate : false,
//			isPaging:false,
//			scrollY:"280px",
			pageUrl : "sysAddress_getOrgUserDataJson",
			useCheckbox:true,
            defaultSort:[[0,"asc"]],
			sendData : function ( sSource, aoData, fnCallback ) {
//				var sNodes = orgTree.getSelectedNodes();
				var nodeId = "";
//				if(sNodes.length>0){
//					var node = sNodes[0];
//					nodeId = node.props.id;
//				}
				var userName=$("input#userName").val(); //获取查询用户姓名
				if(userName!=''){
					nodeId = "";
				}

				aoData.push( { "name": "organizationId", "value" : nodeId },{ "name": "userName", "value":userName },{"name":"processInstanceId" , "value":currentProcessInstanceId });
				jQuery.ajax( {
                    type: "POST",
                    url : baseUrl + "/system/sysAddress_getOrgUserDataJson", 
                    dataType : "json",
                    data : aoData, 
                    success : function(resp) {
						Table.currentPage=resp.current_page;
                        fnCallback(resp);
                    },
					error : function(data){
						
					}
				});
			},   
			columns: [
			            { "mDataProp": "userName","sTitle":"ERP","bSortable":false,"bVisible":true},
						{ "mDataProp": "realName","sTitle":"姓名","bSortable":false,"bVisible":true},
						{ "mDataProp": "organizationId","sTitle":"部门","bSortable":false,"bVisible":true}
	        ],
	        btns:[]
	};
	return Table.dataTable("orgUserDataTable",options);
}	
function treeNodeClick(treeId, treeNode) {
	$("input#userName").val("");
	Table.render(orgUserDataTable);
}
function getHeight(){
	  return jQuery(".modal-body").height();
}
function callBack(){
	var selectRecords = Table.getSelectedRows(orgUserDataTable);
	  if(selectRecords.length <= 0){
		  Dialog.alert("提示", "请选择用户数据记录!");
	  }else{
		  setSelectOrgUserData(selectRecords);
	  }
}
function cancel(){
	Dialog.hide();
}
function defaultCallBackFunction(selectRecords){
	 Dialog.alert("提示", "请定义【确定】按钮的回调函数!");
	 return;
}


function getUrlParam(wlocation,name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = wlocation.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	} 
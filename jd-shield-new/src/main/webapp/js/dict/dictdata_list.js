/**
 * 数据字典-数据项列表页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var typeId = $("#dictTypeId").val();
var table;

//初始化页面
$(function() {
	var columns =  [
	                { "mData": "id2","sTitle":"<input type='checkbox' id='selectall' onclick='toggleChecks(this);' ></input>",
	                			"mDataProp": null, "sWidth": "20px", "sDefaultContent": "<input type='checkbox' ></input>",
	                			"bSortable":false,"bVisible":true,"sClass": "my_class","fnRender":function(data){
	                				return "<input type=\"checkbox\" value=\""+data.aData.id+"\" class=\"checkbox\">";
	                			}},
	                {"sTitle":"序号","sDefaultContent": "","sWidth": "30px","bSortable": false},
	                { "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
	                { "mData": "dictDataCode","sTitle":"数据编码","bSortable":false,"bVisible":true},
	                { "mData": "dictDataName","sTitle":"数据名称","bSortable":false,"sClass": "my_class",
	                    "fnRender": function (obj)
	                    {
	                        var html ="<a href='#' onclick='detailData(\""+ obj.aData.id+"\");'>"+ obj.aData.dictDataName +"</a>";
	                        return html;
	                    }
	                },
	                //{ "mData": "parentName","sTitle":"父编号","bSortable":false,"sClass": "my_class"},
	                { "mData": "modifier","sTitle":"操作人","bSortable":false,"sDefaultContent": "","sClass": "my_class"},
	                { "mData": "modifyTime","sTitle":"最后更新" ,"sDefaultContent": "","bSortable":false,"fnRender":function(data){
	                	if(data.aData.modifyTime!=null){
	                		return new Date(data.aData.modifyTime).format("yyyy-MM-dd hh:mm:ss");
	                	}
	                }},
	                { "mData": "sortNo","sTitle":"显示顺序","bSortable":false,"bVisible":true},
	                {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
	                    return "<a href='#' title='修改' class='btn'><img src='"+springUrl+"/static/admin/skin/img/edit.png' alt='修改'/ onclick='updateDictData(\""+data+"\");'></a>&nbsp;"+
	                    "<a href='#' title='删除' class='btn'><img src='"+springUrl+"/static/admin/skin/img/del.png' alt='删除' onclick='deleteDictData(\""+data+"\");'/></a>";
	                }}
	            ];
	var btns=[
	          {
	              "sExtends":    "text",
	              "sButtonText": "新增",
	              "sButtonClass": "btn btn-success",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	                  Dialog.openRemote('dictData_add','数据项新增',springUrl+"/dict/dictData_add?dictTypeId="+typeId,600,400,[
	                      {
	                          "label": "取消",
	                          "class": "btn-success",
	                          "callback": function() {}
	                      },
	                      {
	                          "label": "保存",
	                          "class": "btn-success",
	                          "callback": function() {
	                              if(!validate($('#dictDataForm'))){
	  	     						return false;
	  	     					  }
	                              var isRepeatDictDataCode=$('#isRepeatDictDataCode').html();
	                              if(isRepeatDictDataCode!=''){
	                            	  return false;
	                              }
	                              var isRepeatDictDataName=$('#isRepeatDictDataName').html();
	                              if(isRepeatDictDataName!=''){
	                            	  return false;
	                              }
	                              addDictData();
	                          }
	                      }
	                  ]);
	              }
	          },
	          {
	              "sExtends":    "text",
	              "sButtonText": "删除",
	              "sButtonClass": "btn btn-success",
	              "sToolTip": "",
	              "fnClick": function ( nButton, oConfig, oFlash ) {
	                  batchDeleteDictData();
	              }
	          }
	          ];
    table=$("#dictData").dataTable({
    		//pageUrl: springUrl+"/dict/dictData_findDictDataPage",
    		//defaultSort:[[1,"asc"],[2,"desc"]],
    	"bProcessing": false,
        "bServerSide":true,
        "sPaginationType": "full_numbers",
        "sAjaxSource":springUrl+"/dict/dictData_findDictDataPage",
        "sServerMethod": "POST",
        "bAutoWidth": false,
        "bStateSave": false,
        "sScrollY":"100%",
        "bScrollCollapse": true,
        "bPaginate":true,
        "oLanguage": {
            "sLengthMenu": "每页显示 _MENU_ 条记录",
            "sZeroRecords": "抱歉， 没有找到",
            "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
            "sInfoEmpty": "没有数据",
            "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "前页",
                "sNext": "后页",
                "sLast": "尾页"}
        },
        "sDom": "<'row-fluid'<'span12'lT>>Rt<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bJQueryUI": true,
        "bFilter":false,
    	"fnServerData":function ( sSource, aoData, fnCallback ) {
    			var dictTypeId=$("input#dictTypeId").val();
    			var dictDataName=$("input#dictDataName").val();
    			if(dictTypeId==null) dictTypeId="";
    			if(dictDataName==null) dictDataName="";
    			aoData.push( { "name": "dictTypeId", "value":dictTypeId } ,{"name":"dictDataName","value":dictDataName} );
    			jQuery.ajax( {
    	                    type: "POST", 
    	                    url:sSource, 
    	                    dataType: "json",
    	                    data: aoData, 
    	                    success: function(json) {
    	                            fnCallback(json);
    	                    },
                            error: function (data) {
                                Dialog.alert("失败","数据字典数据查询失败");
                            }
    		    });
    		},
    "fnDrawCallback": function( oSettings ){
    	var that = this;
    	this.$('td:eq(1)').each(function(i){
    		that.fnUpdate( i+1, this.parentNode, 1, false, false ); 
        });          
    },
    "aaSorting":[],//默认排序
    "aoColumns":columns,
    "oTableTools":{
        "aButtons":btns
    }
    });
    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#searchDictData').click(function() {
    	//$("#dictDataTable").render(table);
    	table.fnDraw();
    });
    
    
  
});

function toggleChecks(obj)
{
    $('.checkbox').prop('checked', obj.checked);
}

/**
 * 数据字典-数据项修改函数
 */
function updateDictData(id){
    Dialog.openRemote('dictData_update','数据项修改',springUrl+"/dict/dictData_update?dictDataId="+id,600,400,[
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function() {}
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function() {
            	if(!validate($('#dictDataForm'))){
						return false;
					}
            	var isRepeatDictDataName=$('#isRepeatDictDataName').html();
                if(isRepeatDictDataName!=''){
              	  return false;
                }
                updateDictDataInfo();
            }
        }
    ]);
}

/**
 * 数据字典-数据项删除函数
 */
function deleteDictData(id){
    Dialog.confirm("确认","确定删除数据字典数据项?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/dict/dictData_delete",
                data:{
                    ids:id
                },
                success:function (data) {
                	if (data.operator == false)
                        Dialog.alert("失败！",data.message);
                    else{
                        Dialog.alert("成功！",data.message);
                        $("#dictDataTable").load(springUrl+"/dict/dictData_list?dictTypeId="+typeId);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","删除数据字典项失败");
                }
            });
        }
    });
}

/**
 * 数据字典-数据项查看详情函数
 */
function detailData(id){
   /* $("#box-right").load(springUrl+"/dictData/dictData_viewDictData?dictDataId="+id);*/
    Dialog.openRemote('dictData_view','数据项查看',springUrl+"/dict/dictData_view?dictDataId="+id,600,440,[
        {
            "label": "关闭",
            "class": "btn-success",
            "callback": function() {}
        }]);
}

function batchDeleteDictData()
{
	 var checkArry = $(".checkbox:checked");
	 var ids="";
	 checkArry.each(function(){
		 ids += ","+ $(this).val();
	 });
	 ids = ids.substr(1,ids.length);
     var length = ids.length;
     ids = ids.toString();
     if(length==0){
         Dialog.alert("消息提示","没有选中项","确定");
     }else{
    	 Dialog.confirm("确认","确定批量删除字典数据项?","是","否",function(result){
    	    if(result){
    	    	 jQuery.ajax({
    	             type:"POST",
    	             cache:false,
    	             url: springUrl+"/dict/dictData_delete",
    	             data:{
    	                 ids:ids,
    	                 //length:length
    	             },
    	             success:function (data) {
    	            	 if (data.operator == false)
                             Dialog.alert("失败！",data.message);
                         else{
                             Dialog.alert("成功！",data.message);
                             $("#dictDataTable").load(springUrl+"/dict/dictData_list?dictTypeId="+typeId);
                         }
    	             },
                     error: function (data) {
                         Dialog.alert("失败","数据字典数据批量删除失败");
                     }
    	         });   
    	    }
    	  });
     }
}
/**
 * 添加数据项-确定按钮响应函数
 */
function addDictData(){
	 var sortNo = $('#sortNo').val();
     var dictTypeId = $('#dictTypeId').val();
     var dictDataCode = $("#dictDataCode1").val();
     var dictDataName = $('#dictDataName1').val();
     var parentId = $('#parentName').val();
    //判断当前数据项CODE是否存在
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            dictDataCode:dictDataCode,
            dictTypeId:dictTypeId
        },
        url:springUrl + "/dict/dictData_isExistDictDataCode",
        success:function (data) {
            if(data == true){
                Dialog.alert("失败","数据编码不能重复");
                return false;
            }else{
                //新增数据项
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        sortNo:sortNo,
                        dictTypeId:dictTypeId,
                        dictDataCode:dictDataCode,
                        dictDataName:dictDataName,
                        parentId:parentId
                    },
                    url: springUrl+"/dict/dictData_addSave",
                    success:function (data) {
                        Dialog.hide();
                        $("#dictDataTable").load(springUrl+"/dict/dictData_list?dictTypeId="+$("#dictTypeId").val());
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据项失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }

                    }
                });
            }
        },
        error: function (data) {
            Dialog.alert("失败",data.responseText);
        }
    });
}

/**
 * 修改数据项-提交按钮响应函数
 */
function updateDictDataInfo(){
    var sortNo = $('#sortNo').val();
    var dictDataName = $('#dictDataName2').val();
    var parentId = $('#parentName').val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/dict/dictData_updateSave",
        data:{
            id:$('#id').val(),
            sortNo:sortNo,
            dictDataName:dictDataName,
            parentId:parentId
        },
        success:function (data) {
            Dialog.hide();
            $("#dictDataTable").load(springUrl+"/dict/dictData_list?dictTypeId="+ $("#dictTypeId").val());
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}
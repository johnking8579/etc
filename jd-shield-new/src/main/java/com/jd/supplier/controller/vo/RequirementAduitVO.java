package com.jd.supplier.controller.vo;

/**
 * @author duandongdong
 * @date 2014年4月29日
 * @desc 需求审核vo，用于自动封装表单数据
 */
public class RequirementAduitVO {

    private Long requirementId;

    private String examineStatus;

    private String mark;

    private String status;

    private String id;

    private String creator;

    private String createTime;

    private String modifier;

    private String modifyTime;

    private String searchProjectName;

    private String searchProjectNum;

    private String outputtime;

    private String configtime;

    private String configmeetingtime;

    private String projecttime;

    private String fileName1;

    private String fileName2;
    

    public String getFileName1() {
        return fileName1;
    }

    public void setFileName1(String fileName1) {
        this.fileName1 = fileName1;
    }

    public String getFileName2() {
        return fileName2;
    }

    public void setFileName2(String fileName2) {
        this.fileName2 = fileName2;
    }

    public String getOutputtime() {
	return outputtime;
    }

    public void setOutputtime(String outputtime) {
	this.outputtime = outputtime;
    }

    public String getConfigtime() {
	return configtime;
    }

    public void setConfigtime(String configtime) {
	this.configtime = configtime;
    }

    public String getConfigmeetingtime() {
	return configmeetingtime;
    }

    public void setConfigmeetingtime(String configmeetingtime) {
	this.configmeetingtime = configmeetingtime;
    }

    public String getProjecttime() {
	return projecttime;
    }

    public void setProjecttime(String projecttime) {
	this.projecttime = projecttime;
    }

    public String getSearchProjectName() {
	return searchProjectName;
    }

    public void setSearchProjectName(String searchProjectName) {
	this.searchProjectName = searchProjectName;
    }

    public String getSearchProjectNum() {
	return searchProjectNum;
    }

    public void setSearchProjectNum(String searchProjectNum) {
	this.searchProjectNum = searchProjectNum;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getCreator() {
	return creator;
    }

    public void setCreator(String creator) {
	this.creator = creator;
    }

    public String getCreateTime() {
	return createTime;
    }

    public void setCreateTime(String createTime) {
	this.createTime = createTime;
    }

    public String getModifier() {
	return modifier;
    }

    public void setModifier(String modifier) {
	this.modifier = modifier;
    }

    public String getModifyTime() {
	return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
	this.modifyTime = modifyTime;
    }

    public String getExamineStatus() {
	return examineStatus;
    }

    public void setExamineStatus(String examineStatus) {
	this.examineStatus = examineStatus == null ? null : examineStatus.trim();
    }

    public String getMark() {
	return mark;
    }

    public void setMark(String mark) {
	this.mark = mark == null ? null : mark.trim();
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }

    public Long getRequirementId() {
	return requirementId;
    }

    public void setRequirementId(Long requirementId) {
	this.requirementId = requirementId;
    }
}
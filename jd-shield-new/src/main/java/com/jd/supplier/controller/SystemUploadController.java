package com.jd.supplier.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.constant.DictConstant;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.tree.TreeNode;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictType;
import com.jd.official.modules.dict.service.DictTypeService;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.service.RequirementAffixService;

/**
 * @Description: 招标制度上传Controller
 * @author hetengfei
 * @date 
 * @version V1.0
 */

@Controller
@RequestMapping(value="/supplier")
public class SystemUploadController {
    private static final Logger logger = Logger.getLogger(SystemUploadController.class);

    @Autowired
    private DictTypeService dictTypeService;
    
    @Autowired
    private RequirementAffixService requirementAffixService;
    
    @Autowired
    
    private JssService jssService;

    /**
     * 字典类别树异步加载
     * @param systemUpload 文档分类类别
     * @throws Exception
     */
    @RequestMapping(value = "/systemUpload_load", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<TreeNode> sysResourceLoad(Long id) {
    	try{
    		if(id==0)
    			id=null;
    		List<DictType> dictTypeList =  dictTypeService.findByParentIdForSystem(id);
    	    List<TreeNode> listData= this.getTreeNodes(dictTypeList);
    	    return listData;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("字典类别树异步加载失败",e);
        }
    }

    /**
     * 字典类别树数据封装
     * @param dictTypes
     * @return
     */
    private List<TreeNode> getTreeNodes(List<DictType> dictTypes) {
        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
        for (DictType dictType : dictTypes) {
            TreeNode treeNode = new TreeNode();
            treeNode.setIsParent(true);
            treeNode.setId(String.valueOf(dictType.getId()));
            treeNode.setpId(String.valueOf(dictType.getParentId()));
            treeNode.setName(dictType.getDictTypeName());
            treeNode.setIconSkin("systemupload");
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("id", dictType.getId());
            props.put("parentId", dictType.getParentId());
            treeNode.setProps(props);
            treeNodes.add(treeNode);
        }
        return treeNodes;
    }


    /**
     * @Description: 进入招标制度后台管理页面
     * @return
     * @throws Exception String
     * @author hetengfei
     * @date 2013-8-27下午03:29:58
     * @version V1.0
     */
    @RequestMapping(value = "/systemUpload_index", method = RequestMethod.GET)
    public ModelAndView list()
            throws Exception {
        ModelAndView mav = new ModelAndView("supplier/systemUpload_index");
        return mav;
    }
    
    /**
     * @Description: 进入员工平台招标制度页面
     * @return
     * @throws Exception String
     * @author hetengfei
     * @date 2013-8-27下午03:29:58
     * @version V1.0
     */
    @RequestMapping(value = "/tenderSystem_index", method = RequestMethod.GET)
    public ModelAndView userIndex()
            throws Exception {
        ModelAndView mav = new ModelAndView("supplier/userSystem_index");
        return mav;
    }
    

    /**
     * @Description: 进入新增数据字典类别页面
     * @param id 数据类别id
     * @return String
     * @author hetengfei
     * @date 2013-8-27下午03:49:05
     * @version V1.0
     */
    @RequestMapping(value = "/systemUpload_add",method=RequestMethod.GET)
    public ModelAndView addDictType(Long id) {
        ModelAndView mav = new ModelAndView("supplier/systemType_add");
        if(id==0L)
        	id=48L;
        DictType dictType = dictTypeService.get(id);
        if(null == dictType) {
            mav.addObject("root", "48");
        }
        mav.addObject("dictType", dictType);
        return mav;
    }
    /**
     *
     * @Description: 保存新增文档分类类别
     * @param dictType
     * @return String
     * @author hetengfei
     * @date 2013-8-27下午03:57:03 
     * @version V1.0
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="DictType")
    @RequestMapping(value="/systemUpload_addSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveAddDictType(DictType dictType,HttpServletRequest request){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
        	//获取操作人姓名
//            String userName = ComUtils.getLoginName();
            dictType.setStatus(DictConstant.STATUS_DISABLE);
            Long primaryKey = dictTypeService.insert(dictType);
            if(primaryKey>0){
    	 		map.put("operator", true);
    	        map.put("message", "添加成功");
    	 	}else{
    	 		map.put("operator", false);
    	        map.put("message", "添加失败");
    	 	}
            request.setAttribute("entityId", primaryKey);
            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存新增数据字典类别失败",e);
        }
    }
    /**
     * @Description: 进入到修改页面的公共方法
     * @param dicttypeId
     * @return ModelAndView
     * @author hetengfei
     * @date 2013-8-27下午04:46:16
     * @version V1.0
     */
    @RequestMapping(value = "/systemUpload_update",method=RequestMethod.GET)
    public ModelAndView updateDictType(Long dictTypeId) {
        DictType dictType = dictTypeService.get(dictTypeId);
        ModelAndView mav = new ModelAndView("supplier/systemUpload_update");
        mav.addObject("dictType",dictType);
        return mav;
    }
    /**
     * @Description: 进入到查看文档类别页面
     * @param dictTypeInfo
     * @return ModelAndView
     * @author hetengfei
     * @date 2013-8-27下午04:46:16
     * @version V1.0
     */
    @RequestMapping(value = "/systemUpload_view",method=RequestMethod.GET)
    public ModelAndView viewDictType(DictType dictTypeInfo) {
        //根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
        List<DictType> dictTypes = dictTypeService.find(dictTypeInfo);
        ModelAndView mav = new ModelAndView("supplier/tenderSystem_index");
        mav.addObject("dictType",dictTypes.size()>0?dictTypes.get(0):null);
        return mav;
    }
    
    /**
     * @Description: 员工进入到查看文档类别页面
     * @param dictTypeInfo
     * @return ModelAndView
     * @author hetengfei
     * @date 2013-8-27下午04:46:16
     * @version V1.0
     */
    @RequestMapping(value = "/userSystemUpload_view",method=RequestMethod.GET)
    public ModelAndView viewUserSystem(DictType dictTypeInfo) {
        //根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
        List<DictType> dictTypes = dictTypeService.find(dictTypeInfo);
        ModelAndView mav = new ModelAndView("supplier/userTenderSystem_index");
        mav.addObject("dictType",dictTypes.size()>0?dictTypes.get(0):null);
        return mav;
    }
    
    /**
     * @Description: 保存修改数据字典类别
     * @param dictType
     * @return String
     * @author hetengfei
     * @date 2013-8-27下午05:01:21
     * @version V1.0
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="DictType")
    @RequestMapping(value="/systemUpload_updateSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveEditDictType(DictType dictType){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
        	//获取操作人姓名
            String userName = ComUtils.getLoginName();
            dictType.setModifier(userName);
            dictType.setModifyTime(new Date());
            int i = dictTypeService.update(dictType);
            if(i>0){
            	map.put("operator", true);
     	        map.put("message", "修改成功");
            }else{
            	map.put("operator", false);
     	        map.put("message", "修改失败");
            }
            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
        	throw new BusinessException("保存修改数据字典类别失败",e);
        }
    }
    /**
     * @Description: 删除文档类别
     * @param dicttypeId
     * @return String
     * @author hetengfei
     * @date 2013-8-27下午06:20:48
     * @version V1.0
     */
    @RecordLog(operationType=OperationTypeValue.delete, entityName="DictType")
    @RequestMapping(value="/systemUpload_delete" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String deleteDictType(DictType entity){
        Map<String,Object> map = new HashMap<String,Object>();
        boolean flag= false;
        flag = dictTypeService.isOwnSubTypeOrData(entity.getId());
		if(flag){
			map.put("operator", false);
            map.put("message", "存在子类别或该类别下存在数据,删除失败");
		}
		int result = dictTypeService.delete(entity.getId());
		if(result>0){
            map.put("operator", true);
            map.put("message", "删除成功");
        }else{
            map.put("operator", false);
            map.put("message", "删除失败");
        }		
        return JSONObject.fromObject(map).toString();
    }
    /**
      * @Description: 根据类别编码查看类别是否存在
      * @param dictTypeCode
      * @return String
      * @author hetengfei
      * @date 2013-9-2上午10:09:58 
      * @version V1.0
     */
    @RequestMapping(value="/systemUpload_isExistDictTypeCode",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String isExistDictTypeCode(DictType dictType){
    	Map<String,Object> map = dictTypeService.isExistDictTypeCodeOrName(dictType);
    	return JSONObject.fromObject(map).toString();
//        boolean flag = dictTypeService.isExistDictTypeCode(dictTypeCode);
//        return flag;
    }
    
    @RequestMapping(value="/systemUpload_isExistDictTypeCodePage",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean isExistDictTypeCodePage(DictType dictType){
    	boolean flag = dictTypeService.isExistDictTypeCode(dictType);
    	return flag;
    }
    
    @RequestMapping(value="/systemUpload_isExistDictTypeNamePage",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean isExistDictTypeNamePage(DictType dictType){
    	boolean flag = dictTypeService.isExistDictTypeName(dictType);
    	return flag;
    }
    
    /**
	 * @Description:招标制度列表分页查询
	 * @author hetengfei
	 */
	@RequestMapping(value = "/tenderSystemPage",method = RequestMethod.POST)
	@ResponseBody
	public Object userRolePage(@RequestParam String fileName,@RequestParam Long dictTypeId,HttpServletRequest request) {
		// 创建pageWrapper
		PageWrapper<RequirementAffix> pageWrapper = new PageWrapper<RequirementAffix>(request);
		List<Long> typeList = findAllSonByPanrentId(dictTypeId);
		// 添加搜索条件
		pageWrapper.addSearch("bussineType", "3");
		if(typeList.size()>0){
			pageWrapper.addSearch("typeList", typeList);
		}
		if (null != fileName && !fileName.equals("")) {
			pageWrapper.addSearch("fileName", fileName);
		}
		// 后台取值
		requirementAffixService.find(pageWrapper.getPageBean(), "findByMap",
				pageWrapper.getConditionsMap());
		// 返回到页面的数据
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}
	
	/**
	 * 将上传文件信息保存到数据库
	 * @param requirementAffix
	 */
    @RequestMapping(value = "/insertAffixFile",method = RequestMethod.POST)
    @ResponseBody
    public  void saveSystem(@RequestParam String fileName,@RequestParam String fileKey,@RequestParam String fileType,HttpServletRequest request) throws Exception {
    	
    	RequirementAffix requirementAffix=new RequirementAffix();
    	requirementAffix.setFileName(fileName);
    	requirementAffix.setFileKey(fileKey);
    	requirementAffix.setFileType(fileType);
    	requirementAffix.setBussineType("3");
    	requirementAffixService.insert(requirementAffix);
    }
    
    
    /**
	 * 批量删除
	 * @param requirementAffix
	 */
    @RequestMapping(value = "/AffixFile_delete",method = RequestMethod.POST)
    @ResponseBody
    public Object rejectAll(@RequestParam( required = false) String ids,HttpServletRequest request) throws Exception {
    
	    Map<String, Object> resultMap = new HashMap<String, Object>();
	    requirementAffixService.deleteFromIds(ids);
	    String[] idStrings=ids.split(",");
		List<String> idsList=Arrays.asList(idStrings);
		try{
			for(String id:idsList){
				RequirementAffix requirementAffix=requirementAffixService.get(Long.valueOf(id));
				jssService.deleteObject("jd.sheild.doc", requirementAffix.getFileKey());
			}
		}catch(Exception e){
			logger.error(e);
			resultMap.put("message", "删除失败");
		}
		
		resultMap.put("message", "已删除！");
		return resultMap;
    }
	
	/**
	 * 根据父ID查找所有的子id及孙子id
	 * @param parentId 父ID
	 * @return 子类id
	 */
	
	
	public List<Long> findAllSonByPanrentId(Long typeID){
		List<Long> l=new ArrayList<Long>();
		addId(typeID,l);
		return l;
	}
	
	/*
	 * 递归遍历制度列表id，加入List中
	 */
	public void addId(Long typeID,List<Long> l){
		
		l.add(typeID);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("typeID",typeID);
		List<DictType> list=dictTypeService.findByParentId(typeID);
		
		if(list != null && list.size()>0){
			for(DictType dictType:list){
				addId(dictType.getId(),l);
			}
		}
	}

    
}

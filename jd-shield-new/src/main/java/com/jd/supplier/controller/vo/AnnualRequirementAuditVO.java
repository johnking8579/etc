package com.jd.supplier.controller.vo;

import java.util.Date;

import com.jd.official.core.model.IdModel;

public class AnnualRequirementAuditVO  extends IdModel  {

    private String tJdsupplierRequirementYearId;

    private String approvalStatus;
    
    private String approvalReply;

    private String tenderType;

    private String isBudget;

    private String isTenderCenter;

    private String tenderStartDate;

    private String tenderLevel;
    private String isTenderMethod;

    private String isPlan;

    private String mark;


    private String status;


    public String gettJdsupplierRequirementYearId() {
        return tJdsupplierRequirementYearId;
    }

    public void settJdsupplierRequirementYearId(String tJdsupplierRequirementYearId) {
        this.tJdsupplierRequirementYearId = tJdsupplierRequirementYearId;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus == null ? null : approvalStatus.trim();
    }
    
    public String getApprovalReply() {
		return approvalReply;
	}

	public void setApprovalReply(String approvalReply) {
		this.approvalReply = approvalReply;
	}

	public String getTenderType() {
        return tenderType;
    }

    public void setTenderType(String tenderType) {
        this.tenderType = tenderType == null ? null : tenderType.trim();
    }

    public String getIsBudget() {
        return isBudget;
    }

    public void setIsBudget(String isBudget) {
        this.isBudget = isBudget == null ? null : isBudget.trim();
    }

    public String getIsTenderCenter() {
        return isTenderCenter;
    }

    public void setIsTenderCenter(String isTenderCenter) {
        this.isTenderCenter = isTenderCenter == null ? null : isTenderCenter.trim();
    }

    public String getTenderStartDate() {
        return tenderStartDate;
    }

    public void setTenderStartDate(String tenderStartDate) {
        this.tenderStartDate = tenderStartDate;
    }

    public String getTenderLevel() {
        return tenderLevel;
    }

    public void setTenderLevel(String tenderLevel) {
        this.tenderLevel = tenderLevel == null ? null : tenderLevel.trim();
    }

    public String getIsTenderMethod() {
        return isTenderMethod;
    }

    public void setIsTenderMethod(String isTenderMethod) {
        this.isTenderMethod = isTenderMethod == null ? null : isTenderMethod.trim();
    }

    public String getIsPlan() {
        return isPlan;
    }

    public void setIsPlan(String isPlan) {
        this.isPlan = isPlan == null ? null : isPlan.trim();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}
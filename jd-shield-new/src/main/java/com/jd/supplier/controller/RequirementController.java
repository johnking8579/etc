package com.jd.supplier.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.DateUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.LevelService;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserAuthorizationService;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.controller.vo.RequirementVO;
import com.jd.supplier.model.AnnualRequirement;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.RequirementAduit;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.service.AnnualRequirementService;
import com.jd.supplier.service.RequirementAduitService;
import com.jd.supplier.service.RequirementAffixService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.util.SupplierConstant;

@Controller
@RequestMapping("/supplier")
public class RequirementController {
    private static final Logger logger = Logger.getLogger(RequirementController.class);

    @Autowired
    private UserAuthorizationService userAuthorizationService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PositionService positionService;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LevelService levelService;

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private RequirementAffixService requirementAffixService;

    @Autowired
    private AnnualRequirementService annualRequirementService;

    @Autowired
    private RequirementAduitService requirementAduitService;

    @RequestMapping(value = "/requirement_index")
    public ModelAndView requirement_expression(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirement_index");
	return mav;
    }

    /**
     * @return
     * @desc 生成编号
     */
    private String generateProjectNum() {
	String projectNum;
	String requirementIdStr;
	String prefix = "";
	int length = 0;
	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int requirementId = requirementService.getMaxRequirementID();
	if (requirementId == 0) {
	    requirementIdStr = "000001";
	} else {
	    requirementIdStr = String.valueOf((requirementId + 1));
	    if (StringUtils.isNotBlank(requirementIdStr)) {
	    	length = 6 - requirementIdStr.length();
	    	StringBuilder sb = new StringBuilder();
	    	for (int i = 0; i < length; i++) {
	    		sb.append("0");
	    	}
	    	prefix = sb.toString();
	    }
	    requirementIdStr = prefix + requirementIdStr;
	}

	projectNum = String.valueOf(year) + String.valueOf(ComUtils.getCurrentLoginUser().getOrganizationId())
		+ requirementIdStr;
	return projectNum;
    }

    @RequestMapping(value = "/requirement_save",method = RequestMethod.POST)
    @ResponseBody
    public Object save(@ModelAttribute RequirementVO requirementvo) throws Exception {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	Requirement requirement = new Requirement();
	setRequirementAttr(requirementvo, requirement);
	requirementService.insert(requirementvo, requirement);
	resultMap.put("message", "保存成功！");
	return resultMap;
    }

    /**
     * @param requirementvo
     * @param requirement
     * @desc 接受参数存入requirement中
     */
    private void setRequirementAttr(RequirementVO requirementvo, Requirement requirement) {
	requirement.setAddress(requirementvo.getAddress());
	User user = ComUtils.getCurrentLoginUser();
	user = userService.get(user.getId());
	if (requirementvo.getRequirementYearId() != null && !requirementvo.getRequirementYearId().equals("")) {
	    requirement.setRequirementYearId(Long.valueOf(requirementvo.getRequirementYearId()));
	}
	requirement.setApplicant(requirementvo.getApplicant());
	requirement.setApplicantDep(requirementvo.getApplicantDep());
	requirement.setApplicantTel(requirementvo.getApplicantTel());
	requirement.setApplicantDepId(String.valueOf(user.getOrganization().getId()));
	requirement.setAptitude(requirementvo.getAptitude());
	requirement.setBudgetAmount(Long.valueOf(requirementvo.getBudgetAmount()));
	requirement.setBudgetSource(requirementvo.getBudgetSource());
	requirement.setCooperationDetail(requirementvo.getCooperationDetail());
	requirement.setCreateTime(new Date());
	requirement.setCreator(requirementvo.getCreator());
	requirement.setDeliveryDate(DateUtils.convertFormatDateString_yyyy_MM_dd(requirementvo.getDeliveryDate()));
	requirement.setDemandStatus(requirementvo.getDemandStatus());
	requirement.setDepSuggestion(requirementvo.getDepSuggestion());
	requirement.setExamineStatus(requirementvo.getExamineStatus());
	if (requirementvo.getId() != null && !requirementvo.getId().equals("")) {
	    requirement.setId(Long.valueOf(requirementvo.getId()));
	} else {
	    requirement.setId(null);
	}
	requirement.setIsYear(requirementvo.getIsYear());
	requirement.setMark(requirementvo.getMark());
	requirement.setModifier(requirementvo.getModifier());
	requirement.setModifyTime(new Date());
	requirement.setOutsideReason(requirementvo.getOutsideReason());
	requirement.setPayMethod(requirementvo.getPayMethod());
	requirement.setProjectDate(requirementvo.getProjectDate());
	requirement.setProjectName(requirementvo.getProjectName());
	requirement.setProjectNum(requirementvo.getProjectNum());
	requirement.setRecommendLevel(requirementvo.getRecommendLevel());
	requirement.setSampleTest(requirementvo.getSampleTest());
	requirement.setSceneTest(requirementvo.getSceneTest());
	requirement.setServiceRequire(requirementvo.getServiceRequire());
	requirement.setStandard(requirementvo.getStandard());
	requirement.setSupplementInfo(requirementvo.getSupplementInfo());
	requirement.setTechnical(requirementvo.getTechnical());
	requirement.setStatus(SupplierConstant.AVAILABLESTATUS);
	requirement.setReqStatus(SupplierConstant.REQCOMMIT);
    }

    // 删除对应id的需求
    @RequestMapping(value = "/requirement_delete",method = RequestMethod.POST)
    @ResponseBody
    public String expression_delete(@RequestParam(value = "locale", required = false) Locale locale,
	    @RequestParam(required = false) String expressionIds) {
	// bussinessRequirementService.deleteBusinessIdKeyAndExpressionId(businessId,
	// expressionId);
	// List<String> ids = JsonUtils.fromJsonByGoogle(expressionIds,new
	// TypeToken<List<String>>(){});
	String[] ids = expressionIds.split(",");
	List<Requirement> undeleted = requirementService.deleteOnlyUnused("deleteByPrimaryKey", ids);

	return JsonUtils.toJsonByGoogle(undeleted);
    }

    @RequestMapping(value = "/requirement_page",method = RequestMethod.POST)
    @ResponseBody
    public Object requirement_expression_page(HttpServletRequest request, @ModelAttribute RequirementVO requirementvo) {
	// 创建pageWrapper
	PageWrapper<Requirement> pageWrapper = new PageWrapper<Requirement>(request);

	// 添加搜索条件
	pageWrapper.addSearch("status", SupplierConstant.AVAILABLESTATUS);
	if (requirementvo.getSearchProjectName() != null && !("").equals(requirementvo.getSearchProjectName()))
	    pageWrapper.addSearch("searchProjectName", requirementvo.getSearchProjectName());
	if (requirementvo.getSearchProjectNum() != null && !("").equals(requirementvo.getSearchProjectNum()))
	    pageWrapper.addSearch("searchProjectNum", requirementvo.getSearchProjectNum());
	// 后台取值
	requirementService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());

	// 返回到页面的额外数据
	pageWrapper.addResult("returnKey", "returnValue");
	// 返回到页面的数据
	// List<Requirement> names =
	// userAuthorizationService.getRequirementNameListByIds(requirements);
	List<Requirement> names = new ArrayList<Requirement>();
	pageWrapper.addResult("names", names);

	String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());

	logger.debug(json);
	return json;
    }

    @RequestMapping(value = "/requirement_editpage")
    @ResponseBody
    public ModelAndView requirement_edit(@RequestParam("id") String id, Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirement_edit");
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist = dictDataService.findDictDataList("recommendLevel");
	Requirement requirement = (Requirement) requirementService.get(Long.valueOf(id));
	// 取附件
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("bussineType", SupplierConstant.BUSSINETYPE_REQCOMMIT);
	map.put("bussineId", requirement.getId());
	map.put("status", SupplierConstant.AVAILABLESTATUS);
	List<RequirementAffix> requirementAffixList = requirementAffixService.find("findByMap", map);
	// 年度计划(不用加状态为1)
	Map<String, Object> annualRequirementMap = new HashMap<String, Object>();
	annualRequirementMap.put("status", 3);
	annualRequirementMap.put("creator", ComUtils.getLoginName());
	List<AnnualRequirement> annualRequirementList = annualRequirementService
		.find("findByMap", annualRequirementMap);
	model.addAttribute("requirement", requirement);
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist);
	model.addAttribute("requirementAffixList", requirementAffixList);
	model.addAttribute("annualRequirementList", annualRequirementList);
	logger.debug("requirement===" + requirement);

	return mav;// + userList.size();
    }

    @RequestMapping(value = "/queryAffixList")
    @ResponseBody
    public List<RequirementAffix> queryAffixList(@RequestParam("requirementId") String requirementId,
	    @RequestParam("fileType") String fileType) throws Exception {
	// 取附件
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("bussineType", SupplierConstant.BUSSINETYPE_REQCOMMIT);
	map.put("bussineId", requirementId);
	map.put("status", SupplierConstant.AVAILABLESTATUS);
	map.put("fileType", fileType);
	List<RequirementAffix> requirementAffixList = requirementAffixService.find("findByMap", map);
	return requirementAffixList;
    }

    @RequestMapping(value = "/requirement_update",method = RequestMethod.POST)
    @ResponseBody
    public Object update(@ModelAttribute RequirementVO requirementvo) throws Exception {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	Requirement requirement = new Requirement();
	setRequirementAttr(requirementvo, requirement);
	requirementService.update(requirementvo, requirement);
	resultMap.put("message", "保存成功！");
	return resultMap;
    }

    @RequestMapping(value = "/requirement_savepage")
    public ModelAndView requirement_savepage(Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/requirement_save");
	String projectNum = "";
	projectNum = generateProjectNum();
	User user = ComUtils.getCurrentLoginUser();
	user = userService.get(user.getId());
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
	// 封装数据字段数据
	List<DictData> isYearDatalist = dictDataService.findDictDataList("isYear");
	List<DictData> budgetSourceDatalist = dictDataService.findDictDataList("budgetSource");
	List<DictData> sampleTestDatalist = dictDataService.findDictDataList("sampleTest");
	List<DictData> sceneTestDatalist = dictDataService.findDictDataList("sceneTest");
	List<DictData> recommendLevelDatalist = dictDataService.findDictDataList("recommendLevel");
	// 年度计划(不用加状态为1)
	Map<String, Object> annualRequirementMap = new HashMap<String, Object>();
	annualRequirementMap.put("status", 3);
	annualRequirementMap.put("creator", ComUtils.getLoginName());
	List<AnnualRequirement> annualRequirementList = annualRequirementService
		.find("findByMap", annualRequirementMap);
	model.addAttribute("projectNum", projectNum);
	model.addAttribute("applicantDep", user.getOrganization().getOrganizationName());
	model.addAttribute("applicant", user.getUserName());
	model.addAttribute("projectDate", df.format(new Date()));
	model.addAttribute("isYearDatalist", isYearDatalist);
	model.addAttribute("budgetSourceDatalist", budgetSourceDatalist);
	model.addAttribute("sampleTestDatalist", sampleTestDatalist);
	model.addAttribute("sceneTestDatalist", sceneTestDatalist);
	model.addAttribute("recommendLevelDatalist", recommendLevelDatalist);
	model.addAttribute("annualRequirementList", annualRequirementList);
	return mav;// + userList.size();
    }

    /**
     * author duandongdong date 2014年5月13日 desc 查看需求时间流程
     */
    @RequestMapping(value = "/requirement_process_view")
    public ModelAndView viewRequirementStatus(@RequestParam("id") String id, Model model) {
	ModelAndView modelAndView = new ModelAndView("supplier/requirement_process_view");
	String reqProcessStr = "";
	String reqProcessImg = "";
	// 需求提交状态和时间
	Requirement requirement = (Requirement) requirementService.get(Long.valueOf(id));
	Map<String, Object> requirementAduitMap = new HashMap<String, Object>();
	requirementAduitMap.put("status", SupplierConstant.AVAILABLESTATUS);
	requirementAduitMap.put("requirementId", id);
	requirementAduitMap.put("examineStatus", SupplierConstant.REQCONFIG);
	List<RequirementAduit> requirementAduitList = requirementAduitService.find("findByMap", requirementAduitMap);
	if (requirement != null) {
	    reqProcessImg += "<image src='/static/supplier/images/status_green.png'/>";
	    reqProcessStr += SupplierConstant.REQCOMMIT_CN + DateUtils.dateFormat(requirement.getCreateTime())
		    + "&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	if (requirementAduitList != null && requirementAduitList.size() > 0) {
	    RequirementAduit requirementAduit = requirementAduitList.get(0);
	    reqProcessImg = "";
	    reqProcessImg += "<image src='/static/supplier/images/status_green.png'/>&nbsp;&nbsp;";
	    reqProcessImg += "<image src='/static/supplier/images/status_green.png'/>&nbsp;&nbsp;";
	    reqProcessImg += "<image src='/static/supplier/images/status_gray.png'/>&nbsp;&nbsp;";
	    reqProcessImg += "<image src='/static/supplier/images/status_gray.png'/>&nbsp;&nbsp;";
	    reqProcessStr += SupplierConstant.REQCONFIG_CN + DateUtils.dateFormat(requirementAduit.getCreateTime())
		    + "&nbsp;&nbsp;&nbsp;&nbsp;";
	    reqProcessStr += SupplierConstant.REQPROJECT_CN + DateUtils.dateFormat(requirementAduit.getProjecttime())
		    + "&nbsp;&nbsp;&nbsp;&nbsp;";
	    reqProcessStr += SupplierConstant.OUTPUTTIME_CN + DateUtils.dateFormat(requirementAduit.getOutputtime())
		    + "&nbsp;&nbsp;&nbsp;&nbsp;";
	}

	// 加载进程图片
	model.addAttribute("reqProcessImg", reqProcessImg);
	model.addAttribute("reqProcessStr", reqProcessStr);
	return modelAndView;
    }

}

package com.jd.supplier.controller.vo;

import java.io.Serializable;


public class AnnualRequirementVO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6604757080661991319L;
	
	public static String ENTITY_NAME = "年度招标需求";

	private String reqNumber;
	
    private String id;

    private String reqUserid;

    private String reqUsername;

    private String firstDepId;
    
    private String firstDepName;

    private String demandDepId;
    
    private String demandDepName;
    
    private String projectManagerId;

    private String projectManager;

    private String tenderName;

    private String tenderDetailName;

    private String originalContractDate;

    private String tenderFinishDate;

    private String tenderBudget;

    private String tenderStatus;

    private String mark;
    
    private String status;
    
    private String creator;

    private String createTime;

    private String modifier;

    private String modifyTime;
    
    


    public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReqNumber() {
		return reqNumber;
	}

	public void setReqNumber(String reqNumber) {
		this.reqNumber = reqNumber;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReqUserid() {
        return reqUserid;
    }

    public void setReqUserid(String reqUserid) {
        this.reqUserid = reqUserid == null ? null : reqUserid.trim();
    }

    public String getReqUsername() {
        return reqUsername;
    }

    public void setReqUsername(String reqUsername) {
        this.reqUsername = reqUsername == null ? null : reqUsername.trim();
    }


    public String getFirstDepId() {
		return firstDepId;
	}

	public void setFirstDepId(String firstDepId) {
		this.firstDepId = firstDepId;
	}

	public String getFirstDepName() {
		return firstDepName;
	}

	public void setFirstDepName(String firstDepName) {
		this.firstDepName = firstDepName;
	}

	public String getDemandDepId() {
		return demandDepId;
	}

	public void setDemandDepId(String demandDepId) {
		this.demandDepId = demandDepId;
	}

	public String getDemandDepName() {
		return demandDepName;
	}

	public void setDemandDepName(String demandDepName) {
		this.demandDepName = demandDepName;
	}

	public String getProjectManagerId() {
		return projectManagerId;
	}

	public void setProjectManagerId(String projectManagerId) {
		this.projectManagerId = projectManagerId;
	}

	public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager == null ? null : projectManager.trim();
    }

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName == null ? null : tenderName.trim();
    }

    public String getTenderDetailName() {
        return tenderDetailName;
    }

    public void setTenderDetailName(String tenderDetailName) {
        this.tenderDetailName = tenderDetailName == null ? null : tenderDetailName.trim();
    }

    public String getOriginalContractDate() {
        return originalContractDate;
    }

    public void setOriginalContractDate(String originalContractDate) {
        this.originalContractDate = originalContractDate;
    }

    public String getTenderFinishDate() {
        return tenderFinishDate;
    }

    public void setTenderFinishDate(String tenderFinishDate) {
        this.tenderFinishDate = tenderFinishDate;
    }

    public String getTenderBudget() {
        return tenderBudget;
    }

    public void setTenderBudget(String tenderBudget) {
        this.tenderBudget = tenderBudget;
    }

    public String getTenderStatus() {
        return tenderStatus;
    }

    public void setTenderStatus(String tenderStatus) {
        this.tenderStatus = tenderStatus == null ? null : tenderStatus.trim();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
    }

}
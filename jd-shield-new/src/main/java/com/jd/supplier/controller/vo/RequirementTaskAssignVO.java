package com.jd.supplier.controller.vo;


/**
 * @author duandongdong
 * @date 2014年4月29日
 * @desc 需求任务分配vo，用于自动封装表单数据
 */
public class RequirementTaskAssignVO {
    
    private String id;

    private String taskId;

    private String roleid;

    private String creator;

    private String createTime;

    private String modifier;

    private String modifyTime;

    private String status;

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public String getTaskId() {
	return taskId;
    }

    public void setTaskId(String taskId) {
	this.taskId = taskId;
    }

    public String getRoleid() {
	return roleid;
    }

    public void setRoleid(String roleid) {
	this.roleid = roleid;
    }

    public String getCreateTime() {
	return createTime;
    }

    public void setCreateTime(String createTime) {
	this.createTime = createTime;
    }

    public String getModifyTime() {
	return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
	this.modifyTime = modifyTime;
    }

    public String getCreator() {
	return creator;
    }

    public void setCreator(String creator) {
	this.creator = creator == null ? null : creator.trim();
    }

    public String getModifier() {
	return modifier;
    }

    public void setModifier(String modifier) {
	this.modifier = modifier == null ? null : modifier.trim();
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }
}
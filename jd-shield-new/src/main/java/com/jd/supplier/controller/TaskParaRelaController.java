/**
 * 
 */
package com.jd.supplier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.common.util.Bean2Utils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.supplier.enm.TaskParaEnum;
import com.jd.supplier.model.TaskPara;
import com.jd.supplier.model.TaskParaRela;
import com.jd.supplier.service.TaskParaRelaService;
import com.jd.supplier.service.TaskParaService;

/**
 * 参数关系
 * @author wangchanghui
 *
 */
@Controller
@RequestMapping("/supplier")
public class TaskParaRelaController {
	
	private static Logger logger = Logger.getLogger(PreSelectionController.class);
	
	@Autowired
	private TaskParaService taskParaService;
	
	@Autowired
	private TaskParaRelaService taskParaRelaService;
	
	/**
	 * 主页
	 * @param taskId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/taskParaRela_index", method = RequestMethod.GET)
	public String index(Long taskId,Model model){
		model.addAttribute("taskId", taskId);
		return "supplier/taskParaRela_index";
	}
	
	/**
	 * 分页
	 * @param request
	 * @param taskParaRela
	 * @return
	 */
	@RequestMapping(value = "/taskParaRela_page",method = RequestMethod.POST)
    @ResponseBody
    public Object page(HttpServletRequest request, TaskParaRela taskParaRela) {

    	if(taskParaRela.getTaskId() == null || taskParaRela.getTaskId()==0 ){
    		return null;
    	}
        //创建pageWrapper
        PageWrapper<TaskParaRela> pageWrapper = new PageWrapper<TaskParaRela>(request);
        //添加搜索条件
        pageWrapper.addSearch("taskId", taskParaRela.getTaskId());
        Map<String,Object> map = null;
        try {
			map = Bean2Utils.objToHash(taskParaRela);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

        //后台取值
        taskParaRelaService.find(pageWrapper.getPageBean(), "findByMap", map);//pageWrapper.getConditionsMap()
        pageWrapper.addResult("returnKey","returnValue");
        //返回到页面的数据
        Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(pageWrapper.getResult());
        logger.debug("result:" + json); 
        return json;

    }
	
	/**
	 * 关系绑定页
	 * @param taskParaRela
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/taskParaRela_bindingIndex",method=RequestMethod.GET)
	public String bindingIndex(TaskParaRela taskParaRela,Model model){
//		Map<String,Object> map = new HashMap<String,Object> ();
//		map.put("type", TaskParaEnum.TASK.getType());
//		List<TaskPara> taskNodeList = taskParaService.findParaList(map);
//		model.addAttribute("taskNodeList", taskNodeList);
//		model.addAttribute("taskParaEnum", TaskParaEnum.values());
		model.addAttribute("taskId", taskParaRela.getTaskId());
		return "supplier/taskParaRela_bindingIndex";
	}
	
	/**
	 * 添加绑定页
	 * @param taskParaRela
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/taskParaRela_addBinding",method=RequestMethod.GET)
	public String addBinding(TaskParaRela taskParaRela,Model model){
		Map<String,Object> map = new HashMap<String,Object> ();
		map.put("type", TaskParaEnum.ROLE.getType());
		List<TaskPara> roleList = taskParaService.findParaList(map);
		map.put("type", TaskParaEnum.ORGANIZATION.getType());
		List<TaskPara> organizationList = taskParaService.findParaList(map);
		model.addAttribute("roleList", roleList);
		model.addAttribute("organizationList", organizationList);
		model.addAttribute("taskParaEnum", TaskParaEnum.values());
		model.addAttribute("TaskParaRela", taskParaRela);
		return "supplier/taskParaRela_addBinding";
	}
	
	/**
	 * 添加绑定保存
	 * @param taskParaRela
	 * @param ids
	 * @return
	 */
	@RequestMapping(value="/taskParaRela_addBindingSave",method=RequestMethod.POST)
	@ResponseBody
	public Object addSave(TaskParaRela taskParaRela,String ids){
//		taskParaRelaService.insert(taskParaRela);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("operator", taskParaRelaService.addBatch(taskParaRela,ids));
		map.put("message", "初始化成功");
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(map);
	}
	
}

package com.jd.supplier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.common.constants.CharacterConstant;
import com.jd.common.constants.DictDataConstant;
import com.jd.common.constants.SupplierConstant;
import com.jd.common.enm.DeleteEnum;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.common.util.Bean2Utils;
import com.jd.common.util.StringHandler;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.supplier.controller.vo.SpecialSupplierVO;
import com.jd.supplier.enm.IsOrNotEnum;
import com.jd.supplier.enm.SupplierSelectionEnum;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.service.ISupplierService;
import com.jd.supplier.service.TaskSuppSelectionService;

@Controller
@RequestMapping("/supplier")
public class PreSelectionController {
	
	private static Logger logger = Logger.getLogger(PreSelectionController.class);

	@Autowired
	private TaskSuppSelectionService taskSuppSelectionService;
	
	@Autowired
	private DictDataService dictDataService;
	
	@Autowired
	private ISupplierService supplierService;
	
	@Autowired
	private JssService jssService;
	
	@Value("${jss.temp.directory}")
	private String tempDir;
	
	@Value("${jss.bucket}")
	private String bucketName;
	
	/**
	 * 预选首页
	 * @param model
	 * @param taskId
	 * @param nodeId
	 * @return
	 */

    @RequestMapping(value = "/preSelection_index", method = RequestMethod.GET)
    public String index(Model model,Long taskId,Long nodeId) {
    	//暂无数据 所以传入一个固定值
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/preSelection_index";
    }
    
    /**
     * 预选完成页
     * @param model
     * @param taskId
     * @param nodeId
     * @return
     */
    @RequestMapping(value = "/preSelection_index_cp", method = RequestMethod.GET)
    public String index_cp(Model model,Long taskId,Long nodeId) {
    	//暂无数据 所以传入一个固定值
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/preSelection_index_cp";
    }

    /**
     * 预选分页
     * @param request
     * @param taskSuppSelection
     * @return
     */
    @RequestMapping(value = "/preSelection_page",method = RequestMethod.POST)
    @ResponseBody
    public Object page(HttpServletRequest request, TaskSuppSelection taskSuppSelection) {

    	if(taskSuppSelection.getTaskId() == null || taskSuppSelection.getTaskId()==0 ){
    		return null;
    	}
        //创建pageWrapper
        PageWrapper<TaskSuppSelection> pageWrapper = new PageWrapper<TaskSuppSelection>(request);
        //添加搜索条件
//        pageWrapper.addSearch("taskId", taskSuppSelection.getTaskId());
        taskSuppSelection.setEnterpriseName(StringHandler.handleName(taskSuppSelection.getEnterpriseName()));
        taskSuppSelection.setContent(StringHandler.handleName(taskSuppSelection.getContent()));
        taskSuppSelection.setFinalFlag(SupplierSelectionEnum.PREVIOUS.getType());
        taskSuppSelection.setStatus(DeleteEnum.NO.getStatus());
        taskSuppSelection.setEnterpriseStatus(DeleteEnum.NO.getStatus());
        Map<String,Object> map = null;
        try {
			map = Bean2Utils.objToHash(taskSuppSelection);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

        //后台取值
        taskSuppSelectionService.find(pageWrapper.getPageBean(), "findByMap", map);//pageWrapper.getConditionsMap()
        List<TaskSuppSelection> list = pageWrapper.getPageBean().getResult();
        //为特殊供应商做处理
        for(int i=0;i<list.size();i++){
        	if(list.get(i).getSupplierId()==null){
        		String enterpriseName = list.get(i).getRemarks();
        		if(StringUtils.isNotBlank(enterpriseName)){
        			String arry[] = enterpriseName.split(CharacterConstant.DELIMITER4VIEW);
        			list.get(i).setEnterpriseName(new StringBuffer(arry[0].trim()).append(SupplierConstant.SUPPLIER_SPECIAL).toString());
        			if(arry.length>0)
        				list.get(i).setRemarks(arry[1].trim());
        		}
        	}
        }
        pageWrapper.addResult("returnKey","returnValue");
        //返回到页面的数据
        Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(pageWrapper.getResult());
        logger.debug("result:" + json); 
        return json;

    }
    
    /**
     * 预选-企业列表分页
     * @param request
     * @param taskSuppSelection
     * @return
     */
    @RequestMapping(value = "/preSelection_page4Enterprise",method = RequestMethod.POST)
    @ResponseBody
    public Object page4Enterprise(HttpServletRequest request, TaskSuppSelection taskSuppSelection) {

    	if(taskSuppSelection.getTaskId() == null || taskSuppSelection.getTaskId()==0 ){
    		return null;
    	}
        //创建pageWrapper
        PageWrapper<TaskSuppSelection> pageWrapper = new PageWrapper<TaskSuppSelection>(request);
        taskSuppSelection.setEnterpriseName(StringHandler.handleName(taskSuppSelection.getEnterpriseName()));
        taskSuppSelection.setContent(StringHandler.handleName(taskSuppSelection.getContent()));
        taskSuppSelection.setStatus(DeleteEnum.NO.getStatus());
        taskSuppSelection.setEnterpriseStatus(DeleteEnum.NO.getStatus());
        //添加搜索条件
        pageWrapper.addSearch("taskId", taskSuppSelection.getTaskId());
        Map<String,Object> map = null;
        try {
			map = Bean2Utils.objToHash(taskSuppSelection);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

        //后台取值
        taskSuppSelectionService.find(pageWrapper.getPageBean(), "findEnterpriseByMap", map);//pageWrapper.getConditionsMap()
        pageWrapper.addResult("returnKey","returnValue");
        //返回到页面的数据
        Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(pageWrapper.getResult());
        logger.debug("result:" + json); 
        return json;

    }
    

    /**
     * 预选添加界面
     * @param taskId
     * @param nodeId
     * @param model
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value = "/preSelection_add", method = RequestMethod.GET)
    public String add(Long taskId,Long nodeId,Model model) {
    	List<DictData> areaList = dictDataService.findDictDataList(DictDataConstant.Supplier.AREA),
    			currencyList = dictDataService.findDictDataList(DictDataConstant.Supplier.CURRENCY),
    			enterpiseTypeList = dictDataService.findDictDataList(DictDataConstant.Supplier.ENTERPRISE),
    					qualificationList = dictDataService.findDictDataList(DictDataConstant.Supplier.QUALIFICATION);
    	model.addAttribute("areaList",areaList);
    	model.addAttribute("currencyList",currencyList);
    	model.addAttribute("enterpiseTypeList",enterpiseTypeList);
    	model.addAttribute("qualificationList",qualificationList);
    	model.addAttribute("taskId", taskId);
    	model.addAttribute("nodeId", nodeId);
        return "supplier/preSelection_add";
    }

    /**
     * 预选修改界面
     * @param id
     * @param model
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="TaskSuppSelection")
    @RequestMapping(value = "/preSelection_update", method = RequestMethod.GET)
    public String showSupplier(Long id, Model model) {
    	TaskSuppSelection taskSuppSelection = taskSuppSelectionService.get(id);
        model.addAttribute("taskSuppSelection", taskSuppSelection);
        model.addAttribute("qualifiedEnum",IsOrNotEnum.values());
        model.addAttribute("willingEnum",IsOrNotEnum.values());
        return "supplier/preSelection_update";
    }

    /**
     * 预选新增保存
     * @param taskSuppSelection
     * @param supplierIds
     * @param request
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value = "/preSelection_addSave",method = RequestMethod.POST)
    @ResponseBody
    public String addSave(TaskSuppSelection taskSuppSelection,String supplierIds,HttpServletRequest request) {
    	if(taskSuppSelection != null){
    		//添加预选新增保存
    		taskSuppSelectionService.savePreSelection(taskSuppSelection, supplierIds);
    		Map<String, Object> map = new HashMap<String, Object>();
    		map.put("operator", true);
    		map.put("message", "添加成功");
    		Gson gson = new GsonBuilder().serializeNulls().create();
    		return gson.toJson(map);
    		
    	}
    	return null;
    }
    

    /**
     * 预选修改保存
     * @param taskSuppSelection
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.update, entityName="TaskSuppSelection")
    @RequestMapping(value = "/preSelection_updateSave", method = RequestMethod.POST)
    @ResponseBody
    public String updateSave(TaskSuppSelection taskSuppSelection) {
        taskSuppSelection.setStatus(DeleteEnum.NO.getStatus());
        //修改预选信息
        taskSuppSelectionService.updatePreSelection(taskSuppSelection);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("operator", true);
        map.put("message", "添加成功");
        return new Gson().toJson(map);
    }

    /**
     * 预选删除
     * @param taskSuppSelection
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.delete, entityName="TaskSuppSelection")
    @RequestMapping(value = "/preSelection_delete",method = RequestMethod.POST)
    @ResponseBody
    public String deleteTaskSuppSelection(TaskSuppSelection taskSuppSelection) {
        taskSuppSelection.setStatus(DeleteEnum.YES.getStatus());//逻辑删除
        taskSuppSelectionService.update(taskSuppSelection);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("operator", true);
        map.put("message", "删除成功");
        return new Gson().toJson(map);//性能优于JSONObject.fromObject(map).toString();
    }
    


    /**
     * 预选信息详细
     * @param id
     * @param model
     * @return
     */

    @RequestMapping(value = "/preSelection_view", method = RequestMethod.GET)
    public String taskSuppSelectionView(Long id, Model model) {
        TaskSuppSelection taskSuppSelection = taskSuppSelectionService.get(id);
        model.addAttribute("taskSuppSelection", taskSuppSelection);
        return "supplier/preSelection_view";
    }
    
    /**
     * 新增特殊供应商保存
     * @param specialSupplierVO
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value="/preSelection_addSaveSpecial",method = RequestMethod.POST)
    @ResponseBody
    public String addSaveSpecial(SpecialSupplierVO specialSupplierVO){
//    	supplierService.saveSpecialSupplier(specialSupplierVO);
    	taskSuppSelectionService.saveSpecialSupplier(specialSupplierVO);;
    	 Map<String, Object> map = new HashMap<String, Object>();
         map.put("operator", true);
         map.put("message", "添加成功");
         return new Gson().toJson(map);
    }
    
    /**
     * 新增特殊供应商界面
     * @return
     */
    @RequestMapping(value="/preSelection_addSpecial",method = RequestMethod.GET)
    public String addSpecial(){
    	return "supplier/preSelection_addSpecial";
    }
    
    /**
     * 下载文件
     * @param id
     * @param fileType
     * @param request
     * @param response
     * @return
     */
    @RecordLog(operationType=OperationTypeValue.add, entityName="TaskSuppSelection")
    @RequestMapping(value="/selection_download",method = RequestMethod.GET)
    public String downloadFile(Long id,String fileType,HttpServletRequest request,HttpServletResponse response){
//    	Map<String, Object> map = new HashMap<String, Object>();
//    	if(taskSuppSelectionService.downloadFile(id, fileType)){
//    		map.put("operator", true);
//    		map.put("tempDir", tempDir);
//    	}
//    	return new Gson().toJson(map);
    	taskSuppSelectionService.downloadFile(id, fileType,response);
    	return null;
    }
    
    /**
     * 检查预选完成情况
     * @param taskId
     * @return
     */
    @RequestMapping(value="/preSelection_checkFinish",method = RequestMethod.POST)
    @ResponseBody
    public String checkFinish(Long taskId){
    	Integer count = taskSuppSelectionService.findSelectionCount(taskId, SupplierSelectionEnum.PREVIOUS.getType());
    	 Map<String, Object> map = new HashMap<String, Object>();
         map.put("count", count);
         return new Gson().toJson(map);
    }
    
    /**
     * 预选删除文件
     * @param key
     * @return
     */
    @RequestMapping(value="/preSelection_delFile",method = RequestMethod.POST)
    @ResponseBody
    public String delFile(String key){
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{
    		map.put("result", 1);
    		//调用jss服务删除文件
    		jssService.deleteObject(bucketName, key);
    	}catch(Exception e){
    		map.put("result", 0);
    	}
        return new Gson().toJson(map);
    }
}

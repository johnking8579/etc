package com.jd.supplier.controller;


import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.DateUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.controller.vo.AnnualRequirementAuditVO;
import com.jd.supplier.controller.vo.AnnualRequirementVO;
import com.jd.supplier.model.AnnualRequirement;
import com.jd.supplier.model.AnnualRequirementAudit;
import com.jd.supplier.model.SupplierDto;
import com.jd.supplier.service.AnnualRequirementAuditService;
import com.jd.supplier.service.AnnualRequirementService;

@Controller
@RequestMapping(value = "/supplier")
public class AnnualRequirementController {
	
	private static final Logger logger = Logger.getLogger(AnnualRequirementController.class);
    
	@Autowired
	private	AnnualRequirementService annualRequirementService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AnnualRequirementAuditService annualRequirementAuditService;
	
	@Autowired
	private DictDataService dictDataService;
	
	private final static String      XLS    = "xls";
	private final static String      XLSX   = "xlsx";
	
	
   
    //打开年度招标计划提交页面
    @RequestMapping(value = "/annualRequirement_index")
    public ModelAndView requirement_expression(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response,Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/annualRequirement_index");
	List<DictData> statusSearchDataList =dictDataService.findDictDataList("statusSearch");
	model.addAttribute("statusSearchDataList", statusSearchDataList);
	return mav;
    }
    //打开年度招标计划审核页面
    @RequestMapping(value = "/annualReqAudit_index")
    public ModelAndView openAudit(@RequestParam(value = "locale", required = false) Locale locale,
	    HttpServletRequest request, HttpServletResponse response,Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/annualReqAudit_index");
	List<DictData> statusSearchDataList =dictDataService.findDictDataList("statusSearch");
	model.addAttribute("statusSearchDataList", statusSearchDataList);
	
	return mav;
    }
    //查询记录
    @RequestMapping(value = "/annualRequirementPage",method = RequestMethod.POST)
	@ResponseBody
	public Object annualRequirement(HttpServletRequest request,@ModelAttribute AnnualRequirementVO annualRequirementVO) {
		// 创建pageWrapper
    	String userName=ComUtils.getLoginName();
		PageWrapper<AnnualRequirement> pageWrapper = new PageWrapper<AnnualRequirement>(request);
		pageWrapper.addSearch("creator", userName);
		if(annualRequirementVO.getReqNumber() != null && !("").equals(annualRequirementVO.getReqNumber())){
			pageWrapper.addSearch("reqNumber", annualRequirementVO.getReqNumber());
		}
		
		if(annualRequirementVO.getTenderName() != null && !("").equals(annualRequirementVO.getTenderName())){
			pageWrapper.addSearch("tenderName", annualRequirementVO.getTenderName());
		}
		
		if(annualRequirementVO.getStatus() != null && !("").equals(annualRequirementVO.getStatus())){
			pageWrapper.addSearch("status", annualRequirementVO.getStatus());
		}
		
		annualRequirementService.find(pageWrapper.getPageBean(), "findByMap",pageWrapper.getConditionsMap());
		pageWrapper.addResult("returnKey", "returnValue");
		//返回到页面的数据
		
		List<AnnualRequirement> names = new ArrayList<AnnualRequirement>();
		pageWrapper.addResult("names", names);
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}  
    
    //查询记录
    @RequestMapping(value = "/annualReqAuditPage",method = RequestMethod.POST)
	@ResponseBody
	public Object audit(HttpServletRequest request,@ModelAttribute AnnualRequirementVO annualRequirementVO) {
		// 创建pageWrapper
		PageWrapper<AnnualRequirement> pageWrapper = new PageWrapper<AnnualRequirement>(request);
		if(annualRequirementVO.getReqNumber() != null && !("").equals(annualRequirementVO.getReqNumber())){
			pageWrapper.addSearch("reqNumber", annualRequirementVO.getReqNumber());
		}
		
		if(annualRequirementVO.getTenderName() != null && !("").equals(annualRequirementVO.getTenderName())){
			pageWrapper.addSearch("tenderName", annualRequirementVO.getTenderName());
		}
		
		if(annualRequirementVO.getStatus() != null && !("").equals(annualRequirementVO.getStatus())){
			pageWrapper.addSearch("status", annualRequirementVO.getStatus());
		}
		
		
		annualRequirementService.find(pageWrapper.getPageBean(), "findByMap",pageWrapper.getConditionsMap());
		pageWrapper.addResult("returnKey", "returnValue");
		//返回到页面的数据
		
		List<AnnualRequirement> names = new ArrayList<AnnualRequirement>();
		pageWrapper.addResult("names", names);
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}  
    
    
  //进入新增页面
    @RequestMapping(value = "/annualRequirement_savepage")
    public ModelAndView annualRequirement_savepage( Model model) throws Exception {
	ModelAndView mav = new ModelAndView("supplier/annualRequirement_save");
//	String reqNumber = "";
	
	User user = ComUtils.getCurrentLoginUser();
	user = userService.get(user.getId());
	String[] fullDeptStrs=user.getOrganization().getOrganizationFullname().split("-");
	String[] fullDeptCodeStrs=user.getOrganization().getOrganizationFullPath().split("/");
	String firstDepName=fullDeptStrs[2];
	String demandDepName=fullDeptStrs[3];
	String firstDepId=fullDeptCodeStrs[4];
	String demandDepId=fullDeptCodeStrs[5];
	
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
	model.addAttribute("firstDepName", firstDepName);
	model.addAttribute("demandDepName", demandDepName);
	model.addAttribute("firstDepId", firstDepId);
	model.addAttribute("demandDepId", demandDepId);
	model.addAttribute("reqUserid", user.getUserName());
	model.addAttribute("reqUsername", user.getRealName());
	model.addAttribute("projectDate", df.format(new Date()));
	return mav;// + userList.size();
    }
    
    //进入编辑页面
    @RequestMapping(value = "/annualRequirement_edit")
    public String reqGetById(@RequestParam("id") String id,Model model) {
        AnnualRequirement annualRequirement = annualRequirementService.get(Long.valueOf(id));
        model.addAttribute("annualRequirement", annualRequirement);
        return "supplier/annualRequirement_edit";
    }
    
    @RequestMapping(value = "/getAnnualRequirement",method = RequestMethod.POST)
    @ResponseBody
    public AnnualRequirement getAnnualRequirement(@RequestParam("id") String id,Model model) {
        AnnualRequirement annualRequirement = annualRequirementService.get(Long.valueOf(id));
        //model.addAttribute("annualRequirement", annualRequirement);
        return annualRequirement;
    }
    
    
    //新增和更新一条记录
    @RecordLog(operationType=OperationTypeValue.add, entityName="AnnualRequirement")
    @RequestMapping(value = "/annualRequirement_save",method = RequestMethod.POST)
    @ResponseBody
    public  Object savenew(AnnualRequirementVO annualRequirementVO,@RequestParam("status") String status, HttpServletRequest request) throws Exception {
    	
	Map<String, Object> resultMap = new HashMap<String, Object>();
	AnnualRequirement annualRequirement = new AnnualRequirement();
	AnnualRequirementAudit annualRequirementAudit =new AnnualRequirementAudit();
	setRequirementAttr(annualRequirementVO,annualRequirement);
	
	if(annualRequirement.getId()!=null){
		annualRequirementService.update(annualRequirement);
		if(status.equals("0")){
			resultMap.put("message", "保存成功！");
		} 
		if(status.equals("1")){
			annualRequirementAudit.setApprovalStatus(status);
			annualRequirementAudit.settJdsupplierRequirementYearId(annualRequirement.getReqNumber());
			annualRequirementAuditService.update("updateToCancel", annualRequirementAudit.gettJdsupplierRequirementYearId());
			annualRequirementAuditService.insert(annualRequirementAudit);
			resultMap.put("message", "提交成功！");
		}
	}else {
		
		String reqNumber = "";
		reqNumber = generateReqNumber();
		annualRequirement.setReqNumber(Long.valueOf(reqNumber));
		annualRequirementAudit.settJdsupplierRequirementYearId(annualRequirement.getReqNumber());
		annualRequirementAudit.setApprovalStatus(status);
		annualRequirementService.insert(annualRequirement);
		annualRequirementAuditService.insert(annualRequirementAudit);
		if(status.equals("0")){
			resultMap.put("message", "保存成功！");
		} 
		if (status.equals("1")) {
			resultMap.put("message", "提交成功！");
		}
	}
	
	return resultMap;
    }
    
  //取消一条记录
    @RecordLog(operationType=OperationTypeValue.delete, entityName="AnnualRequirement")
    @RequestMapping(value = "/annualRequirement_delete",method = RequestMethod.POST)
    @ResponseBody
    public Object deleteOne(AnnualRequirement annualRequirement, String id,HttpServletRequest request) throws Exception {
    	
	Map<String, Object> resultMap = new HashMap<String, Object>();
	annualRequirementService.delete("delete", id);
	resultMap.put("message", "已取消！");
	return resultMap;
    }
    
    
    
    /**
 	 * 进入批量审核页面
 	 * 
 	 * @param ids
 	 * @return Map
 	 */
 	@RequestMapping(value = "/allAudit_Page")
 	public String allAuditPage(@RequestParam( required = false) String reqNumber,Model model) {
        List<DictData> tenderTypeDatalist = dictDataService.findDictDataList("tenderType");
     	List<DictData> tenderLevelDatalist = dictDataService.findDictDataList("tenderLevel");
     	List<DictData> isTenderMethodDatalist = dictDataService.findDictDataList("isTenderMethod");
     	List<DictData> isPlanDatalist = dictDataService.findDictDataList("isPlan");
     	model.addAttribute("tenderTypeDatalist", tenderTypeDatalist);
     	model.addAttribute("tenderLevelDatalist", tenderLevelDatalist);
     	model.addAttribute("isTenderMethodDatalist", isTenderMethodDatalist);
     	model.addAttribute("isPlanDatalist", isPlanDatalist);
 		model.addAttribute("reqNumber", reqNumber);
 		return "supplier/annualRequirement_AllAudit";
 	}
 	
 	 /**
 	 * 进行批量审核操作
 	 * 
 	 * @param ids
 	 * @return Map
 	 */
    @RequestMapping(value = "/annualReqAudit_AllAudit",method = RequestMethod.POST)
    @ResponseBody
    public Object auditAll(@RequestParam( required = false) String reqNumber,AnnualRequirementAuditVO annualRequirementAuditVO) throws Exception {
    
    Map<String, Object> resultMap = new HashMap<String, Object>();
    AnnualRequirementAudit annualRequirementAudit =new AnnualRequirementAudit();
    setAnnualReqAudit(annualRequirementAuditVO,annualRequirementAudit);
    annualRequirementAuditService.updateAuditAllReqs(reqNumber, annualRequirementAudit);
	resultMap.put("message", "已审核！");
	return resultMap;
    }
    
 	
    //批量驳回
    @RecordLog(operationType=OperationTypeValue.update, entityName="AnnualRequirement")
    @RequestMapping(value = "/annualReqAudit_reject",method = RequestMethod.POST)
    @ResponseBody
    public Object rejectAll(@RequestParam( required = false) String ids,@RequestParam( required = false) String approvalReply,HttpServletRequest request) throws Exception {
    
    Map<String, Object> resultMap = new HashMap<String, Object>();
	annualRequirementAuditService.updateRejectAllReqs(ids, approvalReply);
	resultMap.put("message", "已驳回！");
	return resultMap;
    }
    
    //进入审批页面
    @RequestMapping(value = "/annualRequirement_audit")
    public String auditGetById(@RequestParam("id") String id,Model model) {
        AnnualRequirement annualRequirement = annualRequirementService.get(Long.valueOf(id));
        List<DictData> tenderTypeDatalist = dictDataService.findDictDataList("tenderType");
    	List<DictData> tenderLevelDatalist = dictDataService.findDictDataList("tenderLevel");
    	List<DictData> isTenderMethodDatalist = dictDataService.findDictDataList("isTenderMethod");
    	List<DictData> isPlanDatalist = dictDataService.findDictDataList("isPlan");
    	model.addAttribute("tenderTypeDatalist", tenderTypeDatalist);
    	model.addAttribute("tenderLevelDatalist", tenderLevelDatalist);
    	model.addAttribute("isTenderMethodDatalist", isTenderMethodDatalist);
    	model.addAttribute("isPlanDatalist", isPlanDatalist);
    	model.addAttribute("annualRequirement", annualRequirement);
        return "supplier/annualRequirement_audit";
    }
    
  //查看审批结果
    @RequestMapping(value = "/annualReqAudit_detail")
    public String auditDetail(@RequestParam("id") String id,@RequestParam("reback") String reback,Model model) {
        List<DictData> tenderTypeDatalist = dictDataService.findDictDataList("tenderType");
    	List<DictData> tenderLevelDatalist = dictDataService.findDictDataList("tenderLevel");
    	List<DictData> isTenderMethodDatalist = dictDataService.findDictDataList("isTenderMethod");
    	List<DictData> isPlanDatalist = dictDataService.findDictDataList("isPlan");
    	AnnualRequirement annualRequirement = annualRequirementService.get(Long.valueOf(id));
        model.addAttribute("tenderTypeDatalist", tenderTypeDatalist);
    	model.addAttribute("tenderLevelDatalist", tenderLevelDatalist);
    	model.addAttribute("isTenderMethodDatalist", isTenderMethodDatalist);
    	model.addAttribute("isPlanDatalist", isPlanDatalist);
    	model.addAttribute("annualRequirement", annualRequirement);
    	model.addAttribute("reback", reback);
        return "supplier/annualReqAuditDetal";
    }
    
    //新增和更新一条记录
    @RecordLog(operationType=OperationTypeValue.update, entityName="AnnualRequirementAudit")
    @RequestMapping(value = "/annualReqAudit_save",method = RequestMethod.POST)
    @ResponseBody
    public Object audit(AnnualRequirementAuditVO annualRequirementAuditVO,HttpServletRequest request) throws Exception {
    	
	Map<String, Object> resultMap = new HashMap<String, Object>();
	annualRequirementAuditVO.settJdsupplierRequirementYearId(request.getParameter("reqNumber"));
	AnnualRequirementAudit annualRequirementAudit=new AnnualRequirementAudit();
	setAnnualReqAudit(annualRequirementAuditVO,annualRequirementAudit);
	annualRequirementAuditService.update("updateByPrimaryKeySelective",annualRequirementAudit);
	
	resultMap.put("message", "审核完成！");
	return resultMap;
    }
    
    
    /**
	 * 跳转到导入年度招标需求
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/annualReq_import")
	public String importSupplier(Model view) {
		return "supplier/annualReq_import";
	}
	
	
	/**
	 * 导入年度招标计划
	 * 
	 * @param request
	 * @param response
	 * @param view
	 * @return 
	 * @return
	 * @throws IOException 
	 */
	@RecordLog(operationType=OperationTypeValue.add, entityName="AnnualRequirement")
	@RequestMapping(value = "/importAnnualReqList",method = RequestMethod.POST)
	@ResponseBody
	public  Object upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request,Model view) throws IOException {
		String result = null;
		Map<String,String> map = new HashMap<String,String>();
		String fileName = null;
		
		 InputStream is = file.getInputStream();	  
         if (is.available() / (1024 * 1024) > 2) {
        	 result="上传文件不能大于2M！";
        	 	map.put("result", result);
				return result ;
			}
		try {
			// 文件名
			fileName = file.getOriginalFilename().replace(".xlsx", "")
					+ Math.random() + ".xlsx";
			result=common(file,fileName);
			map.put("result", result);
		} catch (Exception e) {
			result = "导入失败！";
			map.put("result", result);
			logger.error("导入失败！", e);
		}
		String json=JsonUtils.toJsonByGoogle(map);
		return json;

	}
	
    
    
    //年度申请对象转换
    private void setRequirementAttr(AnnualRequirementVO annualRequirementVO, AnnualRequirement annualRequirement) {
    	if(annualRequirementVO.getId() != null && !"".equals(annualRequirementVO.getId())){
    		annualRequirement.setId(Long.valueOf(annualRequirementVO.getId()));
    	}
    	if(annualRequirementVO.getReqNumber() != null && !"".equals(annualRequirementVO.getReqNumber()))
    	annualRequirement.setReqNumber(Long.valueOf(annualRequirementVO.getReqNumber()));
    	if(annualRequirementVO.getReqUserid() != null && !"".equals(annualRequirementVO.getReqUserid()))
    	annualRequirement.setReqUserid(annualRequirementVO.getReqUserid());
    	if(annualRequirementVO.getReqUsername() != null && !"".equals(annualRequirementVO.getReqUsername()))
    	annualRequirement.setReqUsername(annualRequirementVO.getReqUsername());
    	if(annualRequirementVO.getDemandDepId() != null && !"".equals(annualRequirementVO.getDemandDepId()))
    	annualRequirement.setDemandDepId(annualRequirementVO.getDemandDepId());
    	if(annualRequirementVO.getDemandDepName() != null && !"".equals(annualRequirementVO.getDemandDepName()))
        	annualRequirement.setDemandDepName(annualRequirementVO.getDemandDepName());
    	if(annualRequirementVO.getFirstDepId() != null && !"".equals(annualRequirementVO.getFirstDepId()))
    	annualRequirement.setFirstDepId(annualRequirementVO.getFirstDepId());
    	if(annualRequirementVO.getFirstDepName() != null && !"".equals(annualRequirementVO.getFirstDepName()))
        	annualRequirement.setFirstDepName(annualRequirementVO.getFirstDepName());
    	if(annualRequirementVO.getMark() != null && !"".equals(annualRequirementVO.getMark()))
    	annualRequirement.setMark(annualRequirementVO.getMark());
    	if(annualRequirementVO.getProjectManager() != null && !"".equals(annualRequirementVO.getProjectManager()))
    	annualRequirement.setProjectManager(annualRequirementVO.getProjectManager());
    	if(annualRequirementVO.getProjectManagerId() != null && !"".equals(annualRequirementVO.getProjectManagerId()))
        	annualRequirement.setProjectManagerId(annualRequirementVO.getProjectManagerId());
    	if(annualRequirementVO.getTenderBudget() != null && !"".equals(annualRequirementVO.getTenderBudget()))
    	annualRequirement.setTenderBudget(Integer.valueOf(annualRequirementVO.getTenderBudget()));
    	if(annualRequirementVO.getTenderDetailName() != null && !"".equals(annualRequirementVO.getTenderDetailName()))
    	annualRequirement.setTenderDetailName(annualRequirementVO.getTenderDetailName());
    	if(annualRequirementVO.getTenderName() != null && !"".equals(annualRequirementVO.getTenderName()))
    	annualRequirement.setTenderName(annualRequirementVO.getTenderName());
    	if(annualRequirementVO.getTenderStatus() != null && !"".equals(annualRequirementVO.getTenderStatus()))
    	annualRequirement.setTenderStatus(annualRequirementVO.getTenderStatus());
    	if(annualRequirementVO.getOriginalContractDate() != null && !"".equals(annualRequirementVO.getOriginalContractDate()))
    	annualRequirement.setOriginalContractDate(DateUtils.convertFormatDateString_yyyy_MM_dd(annualRequirementVO.getOriginalContractDate()));
    	if(annualRequirementVO.getTenderFinishDate() != null && !"".equals(annualRequirementVO.getTenderFinishDate()))
    	annualRequirement.setTenderFinishDate(DateUtils.convertFormatDateString_yyyy_MM_dd(annualRequirementVO.getTenderFinishDate()));
        }
    
    //审核对象转换
    private void setAnnualReqAudit(AnnualRequirementAuditVO annualRequirementAuditVO,AnnualRequirementAudit annualRequirementAudit){
    	if(annualRequirementAuditVO.getId() !=null){
    		annualRequirementAudit.setId(annualRequirementAuditVO.getId());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.gettJdsupplierRequirementYearId())){
    		annualRequirementAudit.settJdsupplierRequirementYearId(Long.valueOf(annualRequirementAuditVO.gettJdsupplierRequirementYearId()));
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getApprovalReply())){
    		annualRequirementAudit.setApprovalReply(annualRequirementAuditVO.getApprovalReply());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getApprovalStatus())){
    		annualRequirementAudit.setApprovalStatus(annualRequirementAuditVO.getApprovalStatus());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getIsBudget())){
    		annualRequirementAudit.setIsBudget(annualRequirementAuditVO.getIsBudget());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getIsPlan())){
    		annualRequirementAudit.setIsPlan(annualRequirementAuditVO.getIsPlan());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getIsTenderCenter())){
    		annualRequirementAudit.setIsTenderCenter(annualRequirementAuditVO.getIsTenderCenter());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getIsTenderMethod())){
    		annualRequirementAudit.setIsTenderMethod(annualRequirementAuditVO.getIsTenderMethod());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getMark())){
    		annualRequirementAudit.setMark(annualRequirementAuditVO.getMark());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getTenderLevel())){
    		annualRequirementAudit.setTenderLevel(annualRequirementAuditVO.getTenderLevel());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getTenderType())){
    		annualRequirementAudit.setTenderType(annualRequirementAuditVO.getTenderType());
    	}
    	if(StringUtils.isNotBlank(annualRequirementAuditVO.getTenderStartDate())){
    		annualRequirementAudit.setTenderStartDate(DateUtils.convertFormatDateString_yyyy_MM_dd(annualRequirementAuditVO.getTenderStartDate()));
    	}
    	
    }
    
    
    
    public String common(MultipartFile upload, String uploadFileName)throws Exception {
    	String result="";
        Workbook workbook = null;
        String extensionName = FilenameUtils.getExtension(uploadFileName);
        InputStream is = null;
        // 创建一个列表，用于保存EXCEL工作表的数据
        AnnualRequirement annualRequirement = new AnnualRequirement();
        AnnualRequirementAudit annualRequirementAudit = new AnnualRequirementAudit();
        try {
        	is = upload.getInputStream();
            if (XLS.equalsIgnoreCase(extensionName)) {
                workbook = new HSSFWorkbook(is);
            } else{// if (XLSX.equalsIgnoreCase(extensionName)) 
                workbook = new XSSFWorkbook(is);
            }
            NumberFormat numFt = new DecimalFormat("#0");
            // 解析公式结果
            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            Sheet sheet = workbook.getSheetAt(0);
            
            // 获取第一行对象
            Row row = sheet.getRow(0);
            // 获取excel中总行数
            int rows = sheet.getLastRowNum() - sheet.getFirstRowNum();
            Cell cell;
            // 以行进行循环
            for (int i = 1; i <= rows; i++) {
            	
            	try {
            		
            		row = sheet.getRow(i);
                    // 以列进行循环，并且列的总数以第一行列数为准
                    // 通过迭代器遍历获得行数据后遍历行数据获得单元格数据，并保存在列表sheetData中
                    int j=0;
                    String reqId=evaluator.evaluate((Cell) row.getCell(j)).getStringValue();
                    User reqUser=userService.get(userService.getByUserName(reqId).getId());
                	String reqUserName = reqUser.getRealName();
                	String[] reqFullDeptStrs=reqUser.getOrganization().getOrganizationFullname().split("-");
                	String[] reqFullDeptCodeStrs=reqUser.getOrganization().getOrganizationFullPath().split("/");
                	String reqFirstDepName=reqFullDeptStrs[2];
                	String reqDemandDepName=reqFullDeptStrs[3];
                	String reqFirstDepId=reqFullDeptCodeStrs[4];
                	String reqDemandDepId=reqFullDeptCodeStrs[5];

                	String pmId=evaluator.evaluate((Cell) row.getCell(++j)).getStringValue();
                	
                	User pmUser =userService.get(userService.getByUserName(pmId).getId());
                	String pmName=pmUser.getRealName();
                	
                    annualRequirement.setReqUsername(reqUserName);
                    annualRequirement.setReqUserid(reqId);
                    annualRequirement.setFirstDepId(reqFirstDepId);
                    annualRequirement.setFirstDepName(reqFirstDepName);
                    annualRequirement.setDemandDepId(reqDemandDepId);
                    annualRequirement.setDemandDepName(reqDemandDepName);
                    annualRequirement.setProjectManager(pmName);
                    annualRequirement.setProjectManagerId(pmId);
                    annualRequirement.setTenderName((evaluator.evaluate((Cell) row.getCell(++j)).getStringValue()));
                    annualRequirement.setTenderDetailName(evaluator.evaluate((Cell) row.getCell(++j)).getStringValue());
                    if(row.getCell(++j).getDateCellValue()!=null)
                    {
                    	annualRequirement.setOriginalContractDate((Date) row.getCell(j).getDateCellValue());
                    }
                    annualRequirement.setTenderFinishDate((Date) row.getCell(++j).getDateCellValue());
                    annualRequirement.setTenderBudget(Integer.valueOf(numFt.format(evaluator.evaluate((Cell) row.getCell(++j)).getNumberValue())));
                    annualRequirement.setMark(evaluator.evaluate((Cell) row.getCell(++j)).getStringValue());
                    String reqNumber=generateReqNumber();
                    annualRequirement.setReqNumber(Long.valueOf(reqNumber));
                    annualRequirementAudit.settJdsupplierRequirementYearId(Long.valueOf(reqNumber));
                    annualRequirementAudit.setApprovalStatus("1");
                    annualRequirementService.insert(annualRequirement);
                    annualRequirementAuditService.insert(annualRequirementAudit);
					
				} catch (Exception e) {
					int l=i+1;
            		result+="、"+l;
            		e.printStackTrace();
				}
                
            }
        } catch (Exception e) {
        	logger.error("导入失败！", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                workbook = null;
                annualRequirement = null;
            }
        }
        if("".equals(result))
        result="导入成功！";
        else {
        	result=result.replaceFirst("、", "");
        	result="第"+result+"行导入失败，请检查后重新导入未导入的行！";
        }
        return result;
    }
 
 
 //获取新增reqNumber
    public synchronized String  generateReqNumber() {
    	
    	String prefix = "";
    	String reqNumber;
    	String requirementIdStr;
    	int length = 0;
    	Calendar cal = Calendar.getInstance();
    	int year = cal.get(Calendar.YEAR);
    	int requirementId = (int)annualRequirementService.findCount("findByMapCount", null);
    	if (requirementId == 0) {
    	    requirementIdStr = "000001";
    	} else {
    		requirementId = annualRequirementService.getMaxRequirementID()+1;
    	    requirementIdStr = String.valueOf(requirementId);
    	    if (StringUtils.isNotBlank(requirementIdStr)) {
    	    	StringBuilder sb = new StringBuilder();
    	    	length = 6 - requirementIdStr.length();
    	    	for (int i = 0; i < length; i++) {
    	    		sb.append("0");
    	    	}
    	    	prefix = sb.toString();
    	    }
    	    requirementIdStr = prefix + requirementIdStr;
    	}

    	reqNumber = String.valueOf(year) + String.valueOf(ComUtils.getCurrentLoginUser().getOrganizationId())
    		+ requirementIdStr;
    	return reqNumber;
        }


    
}
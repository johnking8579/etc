package com.jd.supplier.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.jd.common.constants.SupplierConstant;
import com.jd.common.util.ReadExcel;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.controller.base.BaseController;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.SupplierDto;
import com.jd.supplier.service.ISupplierService;

/**
 * 供应商资源库后台
 * 
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see
 * @author Wang YunLong
 * @version 1.0
 * @date 2014年4月22日
 */
@Controller
@RequestMapping(value = "/supplier")
public class SupplierController extends BaseController{
	private static final Logger log = Logger
			.getLogger(SupplierController.class);

	@Autowired
	private ISupplierService supplierService;
	@Autowired
	private DictDataService dictDataService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	/**
	 * 跳转查询供应商信息页面，默认不查询数据
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_index")
	public String querysrSupplierList(Model view) {
		try {
			// 币种类型
			view.addAttribute("integrityList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_DATA_INTEGRITY));
			// 供应商覆盖范围
			view.addAttribute("currencyList", getDictDataListByDictCode(SupplierConstant.CURRENCY));
			// 资料完整度
			view.addAttribute("coverageList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_COVERAGE));
			// 企业级别
			view.addAttribute("levelList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE_LEVEL));
		} catch (Exception e) {
			log.error("跳转查询供应商信息页面异常...", e);
		}
		return "supplier/supplier_index";
	}

	/**
	 * 分页查询供应商信息
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "unused" })
	@RequestMapping(value = "/pageSearchSupplierList", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object pageSeachSupplierList(HttpServletRequest request) {
		PageWrapper<Supplier> pageWrapper = new PageWrapper<Supplier>(request);
		try {
			String name = request.getParameter("name");
			if (StringUtils.isNotEmpty(name)) {
				pageWrapper.addSearch("name", name);
			}
			String data = request.getParameter("dataIntegrity");
			if (StringUtils.isNotEmpty(data)) {
				pageWrapper.addSearch("dataIntegrity", data);
			}
			String supplierRange = request.getParameter("supplierRange");
			if (StringUtils.isNotEmpty(supplierRange)) {
				pageWrapper.addSearch("supplierRange", supplierRange);
			}
			String supplierStatus = request.getParameter("supplierStatus");
			if (StringUtils.isNotEmpty(supplierStatus)) {
				pageWrapper.addSearch("supplierStatus", supplierStatus);
			}
			String encurrency = request.getParameter("encurrency");
			if (StringUtils.isNotEmpty(encurrency)) {
				pageWrapper.addSearch("encurrency", encurrency);
			}
			String funds = request.getParameter("funds");
			if (StringUtils.isNotEmpty(funds)) {
				pageWrapper.addSearch("funds", Integer.parseInt(funds));
			}
			String ficurrency = request.getParameter("ficurrency");
			if (StringUtils.isNotEmpty(ficurrency)) {
				pageWrapper.addSearch("ficurrency", ficurrency);
			}
			String salesCount = request.getParameter("salesCount");
			if (StringUtils.isNotEmpty(salesCount)) {
				pageWrapper.addSearch("salesCount",
						Integer.parseInt(salesCount));
			}
			String isCooperation = request.getParameter("isCooperation");
			if (StringUtils.isNotEmpty(isCooperation)) {
				pageWrapper.addSearch("isCooperation", isCooperation);
			}
			String content = request.getParameter("content");
			if (StringUtils.isNotEmpty(content)) {
				pageWrapper.addSearch("content", content);
			}
			String level = request.getParameter("level");
			if (StringUtils.isNotEmpty(level)) {
				pageWrapper.addSearch("level", level);
			}
			supplierService.find(pageWrapper.getPageBean(), "findByPage",

			pageWrapper.getConditionsMap());

		} catch (Exception e) {
			log.error("分页查询供应商信息异常...", e);
		}
		String json;
		json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		return json;
	}

	/**
	 * 跳转新增供应商信息页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_add")
	public String supplierAdd(Model view) {
		try {
			// 供应商类型
			view.addAttribute("supplierTypeList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_TYPE));
			// 供应商覆盖范围
			view.addAttribute("coverageList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_COVERAGE));
			// 币种类型
			view.addAttribute("currencyList", getDictDataListByDictCode(SupplierConstant.CURRENCY));
			// 供应商组织
			view.addAttribute("orgList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_ORG));
			// 企业性质
			view.addAttribute("enterpriseList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE));
		} catch (Exception e) {
			log.error("跳转新增供应商信息页面异常...", e);
		}
		return "supplier/supplier_add";
	}

	/**
	 * 跳转修改供应商信息页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_update")
	public String supplierUpdate(HttpServletRequest request, Model view) {
		try {
			Long id = Long.parseLong(request.getParameter("supplierId"));
			Supplier supplier = supplierService.get(id);
			change(supplier);		
			// 供应商类型
			view.addAttribute("supplierTypeList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_TYPE));
			// 供应商覆盖范围
			view.addAttribute("coverageList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_COVERAGE));
			// 币种类型
			view.addAttribute("currencyList", getDictDataListByDictCode(SupplierConstant.CURRENCY));
			// 供应商组织
			view.addAttribute("orgList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_ORG));
			// 企业性质
			view.addAttribute("enterpriseList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE));
			view.addAttribute("supplier", supplier);
		} catch (Exception e) {
			log.error("跳转到修改供应商信息页面异常...", e);
		}
		return "supplier/supplier_edit";
	}

	/**
	 * 跳转查看详细供应商信息页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_detail")
	public String supplierDetail(HttpServletRequest request, Model view) {
		try {
			Long id = Long.parseLong(request.getParameter("supplierId"));
			Supplier supplier = supplierService.get(id);
			change(supplier);
			List<String> rangeList = getRangeList(supplier);// 供应商覆盖范围
			List<String> auList = getAuList(supplier);// 认证状况
			
			view.addAttribute("auList", auList);
			// 默认申报纳税类型
			view.addAttribute("taxList", getDictDataListByDictCode(SupplierConstant.TAX_TYPE));
			// 认证状况
			view.addAttribute("authenticList", getDictDataListByDictCode(SupplierConstant.AUTHENTIC_TYPE));
			view.addAttribute("rangeList", rangeList);

			// 资料完整度
			view.addAttribute("integrityList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_DATA_INTEGRITY));
			// 供应商类型
			view.addAttribute("supplierTypeList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_TYPE));
			// 供应商覆盖范围
			view.addAttribute("coverageList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_COVERAGE));
			// 币种类型
			view.addAttribute("currencyList", getDictDataListByDictCode(SupplierConstant.CURRENCY));
			// 供应商组织
			view.addAttribute("orgList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_ORG));
			// 企业性质
			view.addAttribute("enterpriseList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE));
			// 企业级别
			view.addAttribute("levelList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE_LEVEL));

			view.addAttribute("supplier", supplier);
		} catch (Exception e) {
			log.error("跳转到修改供应商信息页面异常...", e);
		}
		return "supplier/supplier_detail";
	}

	/**
	 * 新增供应商
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_save")
	@ResponseBody
	public String saveSupplier(Supplier supplier, Model view) {
		try {
			int count = 0;
			Supplier s = new Supplier();
			s.setName(supplier.getEnterprise().getName());
			List<Supplier> supCount = supplierService.find(s);
			if (supCount.size() > 0) {
				return "0";
			} else {
				count = supplierService.insertData(supplier);
				if (count == 1) {
					// 添加操作日志
				}
				return "1";
			}
		} catch (Exception e) {
			log.error("新增供应商信息异常........", e);
		}
		return null;
	}

	/**
	 * 修改供应商
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_edit")
	@ResponseBody
	public String editSupplier(Supplier supplier, Model view) {
		try {
			int count = supplierService.updateData(supplier);
			if (count == 1) {
				// 添加操作日志
			}
			return "1";
		} catch (Exception e) {
			log.error("新增供应商信息异常........", e);
			return null;
		}
	}

	/**
	 * 刪除供应商的基本信息
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/supplier_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String deleteSupplier(HttpServletRequest request, Model view) {
		try {
			int i = delSupplier(request.getParameter("supplierId"));
			if (i > 0) {
				return "1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}

		return null;
	}

	/**
	 * 跳转到审核供应商页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_examine")
	public String supplierExamine(HttpServletRequest request, Model view) {
		try {
			Long id = Long.parseLong(request.getParameter("supplierId"));
			Supplier supplier = supplierService.get(id);
			change(supplier);
			if (supplier != null && supplier.getRecommended() != null) {
				User user = userService.get(supplier.getRecommended());
				if (user != null) {
					supplier.setRealName(user.getRealName());
				}
			}
			List<String> rangeList = getRangeList(supplier);// 供应商覆盖范围
			List<String> auList = getAuList(supplier);// 认证状况
			
			view.addAttribute("auList", auList);
			// 默认申报纳税类型
			view.addAttribute("taxList", getDictDataListByDictCode(SupplierConstant.TAX_TYPE));
			// 认证状况
			view.addAttribute("authenticList", getDictDataListByDictCode(SupplierConstant.AUTHENTIC_TYPE));
			view.addAttribute("rangeList", rangeList);

			// 资料完整度
			view.addAttribute("integrityList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_DATA_INTEGRITY));
			// 供应商归属区
			view.addAttribute("ownerList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_OWNER));
			// 供应商所属区
			view.addAttribute("homeList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_HOME));
			// 供应商类型
			view.addAttribute("supplierTypeList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_TYPE));
			// 供应商覆盖范围
			view.addAttribute("coverageList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_COVERAGE));
			// 币种类型
			view.addAttribute("currencyList", getDictDataListByDictCode(SupplierConstant.CURRENCY));
			// 供应商组织
			view.addAttribute("orgList", getDictDataListByDictCode(SupplierConstant.SUPPLIER_ORG));
			// 企业性质
			view.addAttribute("enterpriseList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE));
			// 企业级别
			view.addAttribute("levelList", getDictDataListByDictCode(SupplierConstant.ENTERPRISE_LEVEL));

			view.addAttribute("supplier", supplier);
		} catch (Exception e) {
			log.error("跳转到审核供应商信息页面异常...", e);
		}
		return "supplier/supplier_examine";
	}

	/**
	 * 审核记录操作
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_saveExamine")
	@ResponseBody
	public String saveExamine(Supplier supplier, Model view) {
		try {
			int i = supplierService.updateStatus(supplier);
			if (i > 0) {
				return "1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
		return null;
	}

	/**
	 * 审核记录操作
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_rejectExamine")
	@ResponseBody
	public String rejectExamine(Supplier supplier, Model view) {
		try {
			int i = supplierService.updateReject(supplier);
			if (i > 0) {
				return "1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
		return null;
	}

	/**
	 * 跳转到导入供应商页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_import")
	public String importSupplier(Model view) {
		return "supplier/supplier_import";
	}

	/**
	 * 导入供应商
	 * 
	 * @param request
	 * @param response
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/importSupplierList")
	@ResponseBody
	public String upload(@RequestParam(value = "file") MultipartFile file,
			HttpServletRequest request, Model view) {
		List<SupplierDto> supplierDtoList = new ArrayList<SupplierDto>();
		List<String> resultString = new ArrayList<String>();
		String result = null;
		// String fileName = null;
		String fileNametemp = null;
		InputStream is = null;
		String json = "";
		try {
			// // 文件名
			// fileName = file.getOriginalFilename().replace(".csv", "")
			// + Math.random() + ".csv";
			fileNametemp = file.getOriginalFilename();
			// 字节流
			is = file.getInputStream();
			if (is.available() / (1024 * 1024) > 2) {
				return "上传文件不能大于2M";
			}
			ReadExcel readExcel = new ReadExcel();
			result = readExcel.importExcel(is, fileNametemp, supplierDtoList,
					resultString, dictDataService);

			if (resultString.size() == 0) {// 判断是否有错误信息
				supplierService.insertImportSupplier(supplierDtoList,
						resultString, fileNametemp, result);
				if (resultString.size() > 0) {
					json = JsonUtils.toJsonByGoogle(resultString);
					return json;

				}
				return JsonUtils.toJsonByGoogle(result);
			} else {
				json = JsonUtils.toJsonByGoogle(resultString);
				return json;
			}

		} catch (Exception e) {
			try {
				if (is != null)
					is.close();
			} catch (IOException ex) {
				log.error("导入文件失败", ex);
			}
			json = JsonUtils.toJsonByGoogle(resultString);
			return json;
		}

	}

	/**
	 * 跳转内荐供应商页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend")
	public String recommendIndex(Model view) {
		return "supplier/supplier_recommend_index";
	}

	/**
	 * 分页查询供应商信息
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/pageRecommendList", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object pageRecommendList(HttpServletRequest request) {
		PageWrapper<Supplier> pageWrapper = new PageWrapper<Supplier>(request);
		try {
			boolean flag = false;
			User user = ComUtils.getCurrentLoginUser();
			List<Role> roleList = roleService.findByUserId(user.getId());
			for (Role role : roleList) {
				if ("Administrator".equals(role.getRoleCode())) {
					flag = true;
					break;
				}
			}
			String name = request.getParameter("name");
			if (StringUtils.isNotEmpty(name)) {
				pageWrapper.addSearch("name", name);
			}
			String userName = request.getParameter("userName");
			if (StringUtils.isNotEmpty(userName)) {
				pageWrapper.addSearch("userName", userName);
			}
			String realName = request.getParameter("realName");
			if (StringUtils.isNotEmpty(realName)) {
				pageWrapper.addSearch("realName", realName);
			}
			String recommendContent = request.getParameter("recommendContent");
			if (StringUtils.isNotEmpty(recommendContent)) {
				pageWrapper.addSearch("recommendContent", recommendContent);
			}
			if (!flag) {
				pageWrapper.addSearch("recommended", user.getId());
			}
			supplierService.find(pageWrapper.getPageBean(), "findByMap",
					pageWrapper.getConditionsMap());
			List<Supplier> list = (List<Supplier>) pageWrapper.getResult().get(
					"aaData");
			for (Supplier s : list) {
				if (flag) {
					s.setIsAdmin("1");
				}
				s.setRealName(user.getRealName());
			}

		} catch (Exception e) {
			log.error("分页查询供应商内荐信息异常...", e);
		}
		String json;
		json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		return json;
	}

	/**
	 * 跳转新增供应商内荐信息页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend_add")
	public String recommendAdd(Model view) {
		return "supplier/supplier_recommend_add";
	}

	/**
	 * 新增供应商内荐
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend_save")
	@ResponseBody
	public String saveRecommend(Supplier supplier, Model view) {
		User user = ComUtils.getCurrentLoginUser();
		try {
			long count = 0;
			supplier.setName(supplier.getName());
			Supplier supCount = (Supplier) supplierService.get("getName",
					supplier);
			if (supCount != null) {
				return "0";
			} else {
				supplier.setRecommended(user.getId());
				count = supplierService.insertRecommend(supplier);
				if (count > 1) {
					// 添加操作日志
				}
				return "1";
			}
		} catch (Exception e) {
			log.error("新增供应商内荐异常........", e);
		}
		return null;
	}

	/**
	 * 跳转内荐供应商页面
	 * 
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend_update")
	public String recommendUpdate(HttpServletRequest request, Model view) {
		Long id = Long.parseLong(request.getParameter("supplierId"));
		Supplier supplier = supplierService.get(id);
		view.addAttribute("supplier", supplier);
		return "supplier/supplier_recommend_edit";
	}

	/**
	 * 修改内荐供应商
	 * 
	 * @param supplier
	 * @param view
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend_edit")
	@ResponseBody
	public String editRecommend(Supplier supplier, Model view) {
		try {
			int count = supplierService.update(supplier);
			if (count == 1) {
				// 添加操作日志
			}
			return "1";
		} catch (Exception e) {
			log.error("修改内荐供应商信息异常........", e);
			return null;
		}
	}

	/**
	 * 刪除内荐供应商的基本信息
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/supplier_recommend_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String deleteRecommend(HttpServletRequest request, Model view) {
		try {
			int i = delSupplier(request.getParameter("supplierId"));
			if (i > 0) {
				return "1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}

		return null;
	}

	/**
	 * 通知内荐供应商的基本信息
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/supplier_examineRecommend", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String examineRecommend(HttpServletRequest request, Model view) {
		try {
			Long supplierId = Long
					.parseLong(request.getParameter("supplierId"));
			String email = request.getParameter("email");
			String userCode = request.getParameter("userCode");
			String name = URLDecoder.decode(request.getParameter("name"),
					"UTF-8");
			String realName = URLDecoder.decode(
					request.getParameter("realName"), "UTF-8");
			Supplier supplier = new Supplier();
			supplier.setUserCode(userCode);
			supplier.setId(supplierId);
			supplier.setEmail(email);
			supplier.setName(name);
			supplier.setRealName(realName);
			int i = supplierService.updateNotice(supplier);
			if (i == 2) {
				return "1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}

		return null;
	}
	
	/**
	 * 获取供应商覆盖范围集合
	 * @param supplier
	 * @return
	 */
	private List<String> getRangeList(Supplier supplier){
		if(supplier != null){
			List<String> rangeList = null;
			if (supplier.getEnterprise() != null) {
				if (StringUtils.isNotBlank(supplier.getEnterprise().getSupplierRange())) {
					rangeList = new ArrayList<String>();
					String[] str = supplier.getEnterprise()
							.getSupplierRange().split(",");
					for (int i = 0; i < str.length; i++) {
						rangeList.add(str[i]);
					}
				}
			}
			return rangeList;
		}
		return null;
	}
	
	/**
	 * 获取认证状况集合
	 * @param supplier
	 * @return
	 */
	private List<String> getAuList(Supplier supplier){
		if(supplier != null){
			List<String> auList = null;
			if (supplier.getQuality() != null) {
				if (StringUtils.isNotBlank(supplier.getQuality().getAuthentication())) {
					auList = new ArrayList<String>();
					String[] st = supplier.getQuality().getAuthentication()
							.split(",");
					for (int i = 0; i < st.length; i++) {
						auList.add(st[i]);
					}
				}
			}
			return auList;
		}
		return null;
	}
	
	/**
	 * 删除供应商
	 * @param supplierIdStr
	 * @return
	 */
	private int delSupplier(String supplierIdStr){
		if(StringUtils.isNotBlank(supplierIdStr)){
			Long supplierId = Long
					.parseLong(supplierIdStr);
			Supplier supplier = new Supplier();
			supplier.setId(supplierId);
			supplier.setDataStatus("0");
			return supplierService.update(supplier);
		}
		return 0;
	}
	public void change(Supplier su){
		java.text.DecimalFormat   df   =new   java.text.DecimalFormat("#.##");
		if(su!=null){
			if(su.getEnterprise().getFunds()!=null && su.getEnterprise().getFunds()!= 0){
				float temp = su.getEnterprise().getFunds();
				su.getEnterprise().setMoneyo(df.format(temp/100));
			}
		}
	}
}

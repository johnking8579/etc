package com.jd.supplier.web.vo;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.supplier.model.Extraction;

public class ExtractionVO {
	
	public ExtractionVO(){
    	GregorianCalendar gc=new GregorianCalendar();
    	evalStartTime = gc.getTime();
    	evalEndTime = gc.getTime();
    	createTime = gc.getTime();
    }
	
	private Long id;
	private String projectName, reqExpert;
	private Date evalStartTime, evalEndTime;
	private String creator;
	private Date createTime;
	
	private String extractResult;	//抽取结果
	private int additionCount;		//需要补录的人数
	
	public ExtractionVO(Extraction ext, String extractResult, int additionCount)	{
		GregorianCalendar gc=new GregorianCalendar();
    	evalStartTime = gc.getTime();
    	evalEndTime = gc.getTime();
    	createTime = gc.getTime();
		this.id = ext.getId();
		this.projectName = ext.getReq().getProjectName();
		this.reqExpert = ext.getReqExpert();
		this.evalStartTime = ext.getEvalStartTime();
		this.evalEndTime = ext.getEvalEndTime();
		this.creator = ext.getCreator();
		this.createTime = ext.getCreateTime();
		this.extractResult = extractResult;
		this.additionCount = additionCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Date getEvalStartTime() {
		return (Date)evalStartTime.clone();
	}
	
	public String getReqExpert() {
		return reqExpert;
	}

	public void setReqExpert(String reqExpert) {
		this.reqExpert = reqExpert;
	}

	public void setEvalStartTime(Date evalStartTime) {
		if(evalStartTime != null)
			this.evalStartTime = new Date(evalStartTime.getTime());
		else
			this.evalStartTime = null;
	}

	public Date getEvalEndTime() {
		return (Date)evalEndTime.clone();
	}

	public void setEvalEndTime(Date evalEndTime) {
		if(evalEndTime != null)
			this.evalEndTime = new Date(evalEndTime.getTime());
		else
			this.evalEndTime = null;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return (Date)createTime.clone();
	}

	public void setCreateTime(Date createTime) {
		if(createTime != null)
			this.createTime = new Date(createTime.getTime());
		else
			this.createTime = null;
	}

	public String getExtractResult() {
		return extractResult;
	}

	public void setExtractResult(String extractResult) {
		this.extractResult = extractResult;
	}

	public int getAdditionCount() {
		return additionCount;
	}

	public void setAdditionCount(int additionCount) {
		this.additionCount = additionCount;
	}
}

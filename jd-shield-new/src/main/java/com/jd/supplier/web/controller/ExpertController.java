package com.jd.supplier.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.dict.service.DictDataService;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.UserService;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertTag;
import com.jd.supplier.service.ExpertService;

/**
 * 专家库相关页面
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年4月22日
 */
@Controller
@RequestMapping(value = "/expert")
public class ExpertController {
	
	private static Logger log = Logger.getLogger(ExpertController.class);
	public static final String SESSION_ID = "expertSessionId";
	
	@Resource
	private DictDataService dictDataService; 
	@Resource
	private ExpertService expertService;
	@Resource
	private UserService userService;
	@Resource
	private ShardedXCommands redisCommands;
	
	private Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * 专家首页
	 * @return
	 */
	@RequestMapping(value="index")
	public ModelAndView index()  {
		List<DictData> 
			tags = expertService.findAllAvailableTag(),
			groups = dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别),
			regions = dictDataService.findDictDataList(Expert.DICT_TYPE_所属区域);
		
		return new ModelAndView("supplier/expert_index")
			.addObject("regions", regions)
			.addObject("groups", groups)
			.addObject("tags", new Gson().toJson(tags));
	}
	
	/**
	 * 人工补录评标专家页面
	 * @return
	 */
	@RequestMapping(value="addition")
	public ModelAndView addition(long extractionId)  {
		List<DictData> 
			tags = expertService.findAllAvailableTag(),
			groups = dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别),
			regions = dictDataService.findDictDataList(Expert.DICT_TYPE_所属区域);
		
		return new ModelAndView("supplier/expert_addition")
			.addObject("regions", regions)
			.addObject("groups", groups)
			.addObject("tags", new Gson().toJson(tags))
			.addObject("extractionId", extractionId);
	}
	
	/**
	 * 搜索专家
	 * @return
	 */
	@RequestMapping(value="search")
	@ResponseBody
	public String search(HttpServletRequest req)  {
		PageWrapper<Expert> wrapper = new PageWrapper<Expert>(req);
		
		Page<Expert> pager = expertService.find(this.toParamMap(req), wrapper.getsStart(), wrapper.getsLength());
		return this.toDataTablesJson(pager, wrapper.getsEcho()).toString();
	}
	
	/**
	 * 专家录入或修改页面
	 * @return
	 */
	@RequestMapping(value="input")
	public ModelAndView input(HttpServletResponse resp)  {
		List<DictData> 
			tags = expertService.findAllAvailableTag(),
			groups = dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别),
			regions = dictDataService.findDictDataList(Expert.DICT_TYPE_所属区域);
		
		String token = setFormTokenToRedis(setSessionIdToCookie(resp));
		
		return new ModelAndView("supplier/expert_input")
			.addObject("regions", regions)
			.addObject("groups", groups)
			.addObject("tags", new Gson().toJson(tags))
			.addObject("token", token);
	}
	
	/**
	 * 新增存入数据库
	 * @param expert
	 * @param tags
	 * @return
	 */
	@RequestMapping(value="doInsert", method=RequestMethod.POST)
	@ResponseBody
	public String doInsert(Expert expert, 
							String tags, String token,
							@CookieValue(SESSION_ID)String sessionId)	{
		int err;
		try	{
			if(isExpertExists(expert.getUser().getId(), expert.getGroup().getId()))	{
				err = 1;
			} else	{
				if(isTokenValid(token, sessionId)){
					expert.setEntryMode(Expert.ENTRY_MODE_手动录入);
					expert.setCreator(ComUtils.getLoginName());
					expert.setCreateTime(new Date());
					expertService.saveExpertAndTags(expert, parseFromParam(tags), new int[0]);
					err = 0;
				} else	{
					err = 2;
				}
			}
		} catch(Exception e)	{
			err = 500;
			log.error(e.getMessage(), e);
		}
		return "{\"err\":" + err + "}";
	}
	
	/**
	 * 该专家是否存在
	 * @param userId 用户ID
	 * @param groupId 专家组别ID
	 * @return
	 */
	private boolean isExpertExists(long userId, long groupId)	{
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("userId", userId);
		condition.put("group", groupId);
		List<Expert> exists = expertService.find(condition, 0, 10000).getResult() ;
		for(Expert e : exists)	{
			if(e.getGroup().getId().longValue() == groupId)
				return true;
		}
		return false;
	}
	
	/**
	 * 编辑页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value="edit")
	public ModelAndView edit(long id, HttpServletResponse resp)  {
		Expert expert = expertService.findById(id);
		List<ExpertTag> assTags = expertService.findExpertTag(id);
		List<DictData> 
			tags = expertService.findAllAvailableTag(),
			groups =  dictDataService.findDictDataList(Expert.DICT_TYPE_专家组别),
			regions = dictDataService.findDictDataList(Expert.DICT_TYPE_所属区域);
		
		return new ModelAndView("supplier/expert_edit")
			.addObject("regions", regions)
			.addObject("groups", groups)
			.addObject("tags", new Gson().toJson(tags))
			.addObject("assTags", new Gson().toJson(assTags))
			.addObject("expert", expert);
	}
	
	/**
	 * 编辑后存入数据库
	 * @param expert
	 * @param tags
	 * @param delTagId
	 * @return
	 */
	@RequestMapping(value="doEdit", method=RequestMethod.POST)
	@ResponseBody
	public String doEdit(Expert expert, String tags, String delTagId)	{
		int err = 0;
		try	{
			Expert exist = expertService.findById(expert.getId());
			exist.setGroup(expert.getGroup());
			exist.setLeader(expert.getLeader());
			exist.setModifier(ComUtils.getLoginName());
			exist.setModifyTime(new Date());
			exist.setRegion(expert.getRegion());
			exist.setStatus(expert.getStatus());
			
			int[] ints = new int[0];
			if(!"".equals(delTagId) && delTagId != null)	{
				ints = Util.toIntArray(delTagId.split(","));
			}
			
			expertService.saveExpertAndTags(exist, this.parseFromParam(tags), ints);
		} catch(Exception e)	{
			err = 1;
			log.error(e.getMessage(), e);
		} 
		return "{\"err\":" + err + "}";
	}
	
	private String setSessionIdToCookie(HttpServletResponse resp)	{
		String sessionId = UUID.randomUUID().toString();
		Cookie c = new Cookie(SESSION_ID, sessionId);
		resp.addCookie(c);
		return sessionId;
	}
	
	private String setFormTokenToRedis(String sessionId)	{
		String token = UUID.randomUUID().toString();
		redisCommands.set(sessionId + "_formToken", token);
		return token;
	}
	
	private synchronized boolean isTokenValid(String inputToken, String sessionId)	{
		String redisKey = sessionId + "_formToken";
		String existToken = redisCommands.get(redisKey);
		redisCommands.del(redisKey);
		return existToken != null && existToken.equals(inputToken); 
	}
	
	/**
	 * 从tags请求参数中拆分出expertTag对象。
	 * @param requestParam 《tags=1,1,2,3|1,4,,|,3,4,,》.每4位的格式为[expertTagId,tag1Id,tag2Id,tag3Id]
	 * @return
	 */
	private List<ExpertTag> parseFromParam(String requestParam)	{
		List<ExpertTag> list = new ArrayList<ExpertTag>(); 
		if(!"".equals(requestParam) && requestParam != null)	{
			String[] arr = requestParam.split("[|]");
			for(String s : arr)	{
				String[] ss = s.split(",", -1);
				ExpertTag et = new ExpertTag();
				
				if(!"".equals(ss[0]))	{
					et.setId(Integer.parseInt(ss[0]));
				}
				
				if(!"".equals(ss[1]))	{
					DictData d = new DictData();
					d.setId(Long.parseLong(ss[1]));
					et.setTag1(d);
				}
				if(!"".equals(ss[2]))	{
					DictData d = new DictData();
					d.setId(Long.parseLong(ss[2]));
					et.setTag2(d);
				}
				if(!"".equals(ss[3]))	{
					DictData d = new DictData();
					d.setId(Long.parseLong(ss[3]));
					et.setTag3(d);
				}
				list.add(et);
			}
		}
		return list;
	}
	
	private <T> JsonObject toDataTablesJson(Page<T> pager, String sEcho)	{
		JsonObject json = new JsonObject();
		json.addProperty("sEcho", sEcho);
		json.addProperty("iTotalRecords", pager.getTotalCount());
		json.addProperty("iTotalDisplayRecords", pager.getTotalCount());
		json.add("aaData", gson.toJsonTree(pager.getResult()));
		return json;
	}
	
	private Map<String,Object> toParamMap(HttpServletRequest req)	{
		Map<String, String[]> map = req.getParameterMap();
		Map<String, Object> result = new HashMap<String, Object>();
		for(Entry<String, String[]> e : map.entrySet())	{
			result.put(e.getKey(), e.getValue()[0]);
		}
		return result;
	}
	
	
	/**
	 * 专家详情页面
	 */
	@RequestMapping(value="info")
	public ModelAndView info(long id)  {
		return new ModelAndView("supplier/expert_info")
		.addObject("expert", expertService.findById(id))
		.addObject("expertTags", expertService.findExpertTag(id));
	}
	
	/**
	 * 改变专家状态
	 * @return
	 */
	@RequestMapping(value="changeStat")
	@ResponseBody
	public String changeStat(long id)  {
		int err = 0;
		String desc = null;
		
		try {
			Expert e = expertService.findById(id);
			int i;
			if(e.getStatus() == 0)	{
				i = 1;
			} else if(e.getStatus() == 1)	{
				i = 0;
			} else	{
				throw new BusinessException(e.getStatus() + "");
			}
			e.setStatus(i);
			expertService.save(e);
			err = i;
		} catch (Exception e) {
			err = 500;
			desc = e.getMessage();
		}
		
		JsonObject json = new JsonObject();
		json.addProperty("err", err);
		json.addProperty("desc", desc);
		return json.toString();
	}
	
    /**
     * 根据realName搜索
     * @param query 真实姓名
     * @return {id:xx,name:xxx} json格式
     */
    @RequestMapping(value="/userSuggestion", produces="text/json")
	@ResponseBody
	public String userSuggestion(String query)	{
		List<User> list = userService.findLikeRealName(query);
		JsonArray arr = new JsonArray();
		for(User u : list)	{
			JsonObject json = new JsonObject();
			json.addProperty("id", u.getId());
			
			String org = "无部门";
			if(u.getOrganization() != null)	{
				org = u.getOrganization().getOrganizationName();
			}
			json.addProperty("name", u.getRealName() + " | " + org);
			arr.add(json);
		}
		return arr.toString();
	}
	
	/**
	 * REST方式的搜索.
	 * @param userId
	 * @return json
	 */
	@RequestMapping(value="/searchApi")
	@ResponseBody
	public String searchApi(Long userId, String realName)	{
		Map<String,Object> condition = new HashMap<String, Object>();
		condition.put("userId", userId);
		condition.put("realName", realName);
		List<Expert> list = expertService.find(condition, 0, 10000).getResult() ;
		return gson.toJson(list);
	}
	
	
	
	/**
	 * 从excel导入. uploadify在firefox/chrome下无法携带COOKIE,所以拦截器要排除
	 * @return 反馈用excel的名称
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@RequestMapping(value="fromExcel")
	@ResponseBody
	public String fromExcel(MultipartFile file) {
		int err;
		String desc = null;
		try {
			desc = expertService.insertFromExcel(file.getInputStream(), file.getOriginalFilename());
			err = 0;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			err = 1;
			desc = e.getMessage();
		}
		
		JsonObject j = new JsonObject();
		j.addProperty("err", err);
		j.addProperty("desc", desc);
		return j.toString();
	}
	
	/**
	 * 下载反馈用的EXCEL文件
	 * @param key redis的key
	 * @param resp
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="downExcel",produces="application/octet-stream")
	@ResponseBody
	public byte[] downExcel(String key, HttpServletResponse resp) throws Exception {
		byte[] bytes = redisCommands.get(RedisCacheSerializer.serialize(key));
		if(bytes != null)	{
			resp.addHeader("Content-Disposition", "attachment;filename=" + key);
		} else	{
			resp.setStatus(404);
		}
		return bytes;
	}
}

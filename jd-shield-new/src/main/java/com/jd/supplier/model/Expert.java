package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.system.model.User;

/**
 * 评标专家
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年4月23日
 */
public class Expert extends IdModel {
	
	public static final String 
		DICT_TYPE_一级分类标签 	= "expertTag1",		//专家一级标签
		DICT_TYPE_二级分类标签 	= "expertTag2",		//专家二级标签
		DICT_TYPE_三级分类标签 	= "expertTag3",		//专家三级标签
		DICT_TYPE_专家组别  	= "expertGroup",	//专家组别
		DICT_TYPE_所属区域		= "area";			//专家所属区域
	
	public static final String
		ENTRY_MODE_文件导入 	= "import",
		ENTRY_MODE_手动录入 	= "manual";
		
	
	private User user;
	private String leader;
	private int status;			//0正常, 1冻结
	private DictData group;
	private DictData region;
	private String entryMode;	//IMPORT'文件导入, 'MANU'手动录入
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getLeader() {
		return leader;
	}
	public void setLeader(String leader) {
		this.leader = leader;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public DictData getGroup() {
		return group;
	}
	public void setGroup(DictData group) {
		this.group = group;
	}
	public DictData getRegion() {
		return region;
	}
	public void setRegion(DictData region) {
		this.region = region;
	}
	public String getEntryMode() {
		return entryMode;
	}
	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}
}

package com.jd.supplier.model;

import java.util.ArrayList;
import java.util.List;

public class SupplierDto {
	private Supplier supplier;
	private Contacts contacts;
	private Enterprise enterprise;
	private Finance finace;
	private Quality quality;
	private Material material;
	private List<Equip> equip = new ArrayList<Equip>();
	private Other other;

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Contacts getContacts() {
		return contacts;
	}

	public void setContacts(Contacts contacts) {
		this.contacts = contacts;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public Finance getFinace() {
		return finace;
	}

	public void setFinace(Finance finace) {
		this.finace = finace;
	}

	public Quality getQuality() {
		return quality;
	}

	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public List<Equip> getEquip() {
		return equip;
	}

	public void setEquip(List<Equip> equip) {
		this.equip = equip;
	}

	public Other getOther() {
		return other;
	}

	public void setOther(Other other) {
		this.other = other;
	}

}

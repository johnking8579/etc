package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;

public class Equip extends IdModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long qualityId;

	private String name;

	private String brand;

	private Integer eqNumber;

	private String eqProduction;

	private Integer eqLimit;

	private String type;

	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQualityId() {
		return qualityId;
	}

	public void setQualityId(Long qualityId) {
		this.qualityId = qualityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand == null ? null : brand.trim();
	}

	public Integer getEqNumber() {
		return eqNumber;
	}

	public void setEqNumber(Integer eqNumber) {
		this.eqNumber = eqNumber;
	}

	public String getEqProduction() {
		return eqProduction;
	}

	public void setEqProduction(String eqProduction) {
		this.eqProduction = eqProduction == null ? null : eqProduction.trim();
	}

	public Integer getEqLimit() {
		return eqLimit;
	}

	public void setEqLimit(Integer eqLimit) {
		this.eqLimit = eqLimit;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
}
package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;

public class RequirementAduit extends IdModel {

    private static final long serialVersionUID = 1659168822681619710L;

    public RequirementAduit(){
    	GregorianCalendar gc = new GregorianCalendar();
    	outputtime = gc.getTime();
    	configtime = gc.getTime();
    	configmeetingtime = gc.getTime();
    	projecttime = gc.getTime();
    }
    
    private Long requirementId;

    private String examineStatus;

    private String mark;

    private String status;

    //中标结果时间
    private Date outputtime;

    //确认时间
    private Date configtime;

    //需求确认会时间
    private Date configmeetingtime;

    //立项会时间
    private Date projecttime;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Date getOutputtime() {
    	if(outputtime == null)
			return null;
	return (Date)outputtime.clone();
    }

    public void setOutputtime(Date outputtime) {
    	if(outputtime != null)
    		this.outputtime = new Date(outputtime.getTime());
    	else
    		this.outputtime = null;
    }

    public Date getConfigtime() {
    	if(configtime == null)
			return null;
	return (Date)configtime.clone();
    }

    public void setConfigtime(Date configtime) {
	if(configtime != null)
		this.configtime = new Date(configtime.getTime());
	else
		this.configtime = null;
    }

    public Date getConfigmeetingtime() {
    	if(configmeetingtime == null)
			return null;
	return (Date)configmeetingtime.clone();
    }

    public void setConfigmeetingtime(Date configmeetingtime) {
		if(configmeetingtime != null)
			this.configmeetingtime = new Date(configmeetingtime.getTime());
		else
			this.configmeetingtime = null;
    }

    public Date getProjecttime() {
    	if(projecttime == null)
			return null;
	return (Date)projecttime.clone();
    }

    public void setProjecttime(Date projecttime) {
    	if(projecttime != null)
			this.projecttime = new Date(projecttime.getTime());
		else
			this.projecttime = null;
    }

    public Long getRequirementId() {
	return requirementId;
    }

    public void setRequirementId(Long requirementId) {
	this.requirementId = requirementId;
    }

    public String getExamineStatus() {
	return examineStatus;
    }

    public void setExamineStatus(String examineStatus) {
	this.examineStatus = examineStatus == null ? null : examineStatus.trim();
    }

    public String getMark() {
	return mark;
    }

    public void setMark(String mark) {
	this.mark = mark == null ? null : mark.trim();
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status == null ? null : status.trim();
    }
}
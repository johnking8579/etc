package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;
/**
 * 年的需求计划
 * @author hetengfei
 *
 */
public class AnnualRequirement extends IdModel {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1749767311239713018L;
	public AnnualRequirement(){
		GregorianCalendar gc=new GregorianCalendar();
		originalContractDate = gc.getTime();
		tenderFinishDate = gc.getTime();
	}
	
	public static String ENTITY_NAME = "年度招标需求";

	
	
	private Long reqNumber;
	
    private String reqUserid;

    private String reqUsername;
    
    private String firstDepId;
    
    private String firstDepName;

    private String demandDepId;
    
    private String demandDepName;
    
    private String projectManagerId;

    private String projectManager;

    private String tenderName;

    private String tenderDetailName;

    private Date originalContractDate;

    private Date tenderFinishDate;
    
	private Integer tenderBudget;

    private String tenderStatus;

    
	private String mark;
    
    private String status;
    private AnnualRequirementAudit annualRequirementAudit;
    public Long getReqNumber() {
		return reqNumber;
	}

	public void setReqNumber(Long reqNumber) {
		this.reqNumber = reqNumber;
	}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReqUserid() {
        return reqUserid;
    }

    public void setReqUserid(String reqUserid) {
        this.reqUserid = reqUserid == null ? null : reqUserid.trim();
    }

    public String getReqUsername() {
        return reqUsername;
    }

    public void setReqUsername(String reqUsername) {
        this.reqUsername = reqUsername == null ? null : reqUsername.trim();
    }



	public String getFirstDepId() {
		return firstDepId;
	}

	public void setFirstDepId(String firstDepId) {
		this.firstDepId = firstDepId;
	}

	public String getDemandDepId() {
		return demandDepId;
	}

	public void setDemandDepId(String demandDepId) {
		this.demandDepId = demandDepId;
	}

	public String getFirstDepName() {
		return firstDepName;
	}

	public void setFirstDepName(String firstDepName) {
		this.firstDepName = firstDepName;
	}


	public String getDemandDepName() {
		return demandDepName;
	}

	public void setDemandDepName(String demandDepName) {
		this.demandDepName = demandDepName;
	}

	public String getProjectManagerId() {
		return projectManagerId;
	}

	public void setProjectManagerId(String projectManagerId) {
		this.projectManagerId = projectManagerId;
	}

	public String getProjectManager() {
        return projectManager;
    }

    public void setProjectManager(String projectManager) {
        this.projectManager = projectManager == null ? null : projectManager.trim();
    }

    public String getTenderName() {
        return tenderName;
    }

    public void setTenderName(String tenderName) {
        this.tenderName = tenderName == null ? null : tenderName.trim();
    }

    public String getTenderDetailName() {
        return tenderDetailName;
    }

    public void setTenderDetailName(String tenderDetailName) {
        this.tenderDetailName = tenderDetailName == null ? null : tenderDetailName.trim();
    }

    public Date getOriginalContractDate() {
    	if(originalContractDate == null)
    		return null;
        return (Date)originalContractDate.clone();
    }

    public void setOriginalContractDate(Date originalContractDate) {
    	if(originalContractDate != null)
			this.originalContractDate = new Date(originalContractDate.getTime());
		else
			this.originalContractDate = null;
    }

    public Date getTenderFinishDate() {
    	if(tenderFinishDate == null)
    		return null;
        return (Date)tenderFinishDate.clone();
    }

    public void setTenderFinishDate(Date tenderFinishDate) {
    	if(tenderFinishDate != null)
			this.tenderFinishDate = new Date(tenderFinishDate.getTime());
		else
			this.tenderFinishDate = null;
    }

    public Integer getTenderBudget() {
        return tenderBudget;
    }

    public void setTenderBudget(Integer tenderBudget) {
        this.tenderBudget = tenderBudget;
    }


    public String getTenderStatus() {
        return tenderStatus;
    }

    public void setTenderStatus(String tenderStatus) {
        this.tenderStatus = tenderStatus == null ? null : tenderStatus.trim();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark == null ? null : mark.trim();
        
        
    }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public AnnualRequirementAudit getAnnualRequirementAudit() {
		return annualRequirementAudit;
	}

	public void setAnnualRequirementAudit(
			AnnualRequirementAudit annualRequirementAudit) {
		this.annualRequirementAudit = annualRequirementAudit;
	}

	
}
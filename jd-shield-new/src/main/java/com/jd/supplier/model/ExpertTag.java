package com.jd.supplier.model;

import com.jd.official.modules.dict.model.DictData;

public class ExpertTag {

	private Integer id;
	private int expertId;
	private DictData tag1, tag2, tag3;
	
	public ExpertTag() {
	}
	
	public ExpertTag(int expertId, DictData tag1, DictData tag2, DictData tag3) {
		this.expertId = expertId;
		this.tag1 = tag1;
		this.tag2 = tag2;
		this.tag3 = tag3;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + expertId;
		result = prime * result + ((tag1 == null) ? 0 : tag1.hashCode());
		result = prime * result + ((tag2 == null) ? 0 : tag2.hashCode());
		result = prime * result + ((tag3 == null) ? 0 : tag3.hashCode());
		return result;
	}

	/**
	 * 当tag1Id,tag2Id,tag3Id,expertId都相等时,则认为对象重复
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpertTag other = (ExpertTag) obj;
		if (expertId != other.expertId)
			return false;
		if (tag1 == null) {
			if (other.tag1 != null)
				return false;
		} else if (tag1.getId().longValue() != other.tag1.getId().longValue())
			return false;
		if (tag2 == null) {
			if (other.tag2 != null)
				return false;
		} else if (tag2.getId().longValue() != other.tag2.getId().longValue())
			return false;
		if (tag3 == null) {
			if (other.tag3 != null)
				return false;
		} else if (tag3.getId().longValue() != other.tag3.getId().longValue())
			return false;
		return true;
	}

	public DictData getTag1() {
		return tag1;
	}

	public void setTag1(DictData tag1) {
		this.tag1 = tag1;
	}

	public DictData getTag2() {
		return tag2;
	}

	public void setTag2(DictData tag2) {
		this.tag2 = tag2;
	}

	public DictData getTag3() {
		return tag3;
	}

	public void setTag3(DictData tag3) {
		this.tag3 = tag3;
	}

	public int getExpertId() {
		return expertId;
	}

	public void setExpertId(int expertId) {
		this.expertId = expertId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}

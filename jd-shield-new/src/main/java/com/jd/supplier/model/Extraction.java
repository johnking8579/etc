package com.jd.supplier.model;

import java.util.Date;
import java.util.GregorianCalendar;

import com.jd.official.core.model.IdModel;
import com.jd.official.modules.dict.model.DictData;

public class Extraction extends IdModel	{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1048114167084377447L;
	public static final String DICT_TYPE_预算范围 = "budgetScope";
	
	public Extraction(){
		GregorianCalendar gc=new GregorianCalendar();
    	evalStartTime = gc.getTime();
    	evalEndTime = gc.getTime();
	}
	
	private Requirement req;
	private DictData tag1, tag2, tag3, budgetScope, region;
	private Date evalStartTime, evalEndTime;
	private String reqExpert;
	private String avoidExpert, avoidReason;
	public Requirement getReq() {
		return req;
	}
	public void setReq(Requirement req) {
		this.req = req;
	}
	public DictData getTag1() {
		return tag1;
	}
	public void setTag1(DictData tag1) {
		this.tag1 = tag1;
	}
	public DictData getTag2() {
		return tag2;
	}
	public void setTag2(DictData tag2) {
		this.tag2 = tag2;
	}
	public DictData getTag3() {
		return tag3;
	}
	public void setTag3(DictData tag3) {
		this.tag3 = tag3;
	}
	public Date getEvalStartTime() {
		if(evalStartTime==null)
    		return null;
		return (Date)evalStartTime.clone();
	}
	public void setEvalStartTime(Date evalStartTime) {
		if(evalStartTime != null)
			this.evalStartTime = new Date(evalStartTime.getTime());
		else
			this.evalStartTime = null;
	}
	public Date getEvalEndTime() {
		if(evalEndTime == null)
			return null;
		return (Date)evalEndTime.clone();
	}
	public void setEvalEndTime(Date evalEndTime) {
		if(evalEndTime != null)
			this.evalEndTime = new Date(evalEndTime.getTime());
		else
			this.evalEndTime = null;
	}
	public String getReqExpert() {
		return reqExpert;
	}
	public void setReqExpert(String reqExpert) {
		this.reqExpert = reqExpert;
	}
	public String getAvoidExpert() {
		return avoidExpert;
	}
	public void setAvoidExpert(String avoidExpert) {
		this.avoidExpert = avoidExpert;
	}
	public String getAvoidReason() {
		return avoidReason;
	}
	public void setAvoidReason(String avoidReason) {
		this.avoidReason = avoidReason;
	}
	public DictData getBudgetScope() {
		return budgetScope;
	}
	public void setBudgetScope(DictData budgetScope) {
		this.budgetScope = budgetScope;
	}
	public DictData getRegion() {
		return region;
	}
	public void setRegion(DictData region) {
		this.region = region;
	}
}

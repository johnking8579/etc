package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;
/**
 * 任务关系
 * @author wangchanghui
 *
 */
public class TaskParaRela extends IdModel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1977338346718868439L;

	/**
	 * 任务id
	 */
	private Long taskId;

	/**
	 * 角色id
	 */
    private Integer roleId;

    /**
     * 机构id
     */
    private Integer organizationId;

    /**
     * 任务节点id
     */
    private Integer taskNodeId;
    
    /**
     * paracode
     */
    private String code;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 1:roleId 2:organizationId 3:taskNodeId
     */
    private Integer type;

    /**
     * 任务url
     */
    private String url;
    
    /**
     * 节点类型
     */
    private Integer nodeType;
    
    /**
     * 父id
     */
    private Long parentId;
    
    /**
     * 模板类型
     */
    private Integer templateType;


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Integer getTaskNodeId() {
        return taskNodeId;
    }

    public void setTaskNodeId(Integer taskNodeId) {
        this.taskNodeId = taskNodeId;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getTemplateType() {
		return templateType;
	}

	public void setTemplateType(Integer templateType) {
		this.templateType = templateType;
	}
}
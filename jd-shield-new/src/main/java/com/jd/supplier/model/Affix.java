package com.jd.supplier.model;

import com.jd.official.core.model.IdModel;
/**
 * 文件管理
 * @author duandongdong
 *
 */
public class Affix extends IdModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 供应商id
	 */
	private Long supplierId;

	/**
	 * 文件名
	 */
	private String fileName;

	/**
	 * 文件url
	 */
	private String fileUrl;
	
	/**
	 * 文件类型
	 */
	private String fileType;

	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName == null ? null : fileName.trim();
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl == null ? null : fileUrl.trim();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
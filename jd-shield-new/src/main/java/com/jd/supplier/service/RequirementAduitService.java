package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementAduitVO;
import com.jd.supplier.model.RequirementAduit;

public interface RequirementAduitService extends BaseService<RequirementAduit, Long> {

    List<RequirementAduit> deleteOnlyUnused(String ql, Object... values);

    List<RequirementAduit> findRequirementAduitListByIds(final String... values);

    List<RequirementAduit> findRequirementAduitListByIds(final RequirementAduit... values);

    List<RequirementAduit> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId);

    RequirementAduit findRequirementAduitByReqId(Long reqId);

    Integer getMaxRequirementAduitID();

    void insert(RequirementAduitVO requirementAduitvo, RequirementAduit requirementAduit, String flag) throws Exception;
    
    void insertReqAduitMetting(RequirementAduitVO requirementAduitvo, FileAffixVO fileAffixvo) throws Exception;
}

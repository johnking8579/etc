/**
 * 
 */
package com.jd.supplier.service;

import java.util.List;
import java.util.Map;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.TaskPara;

/**
 * 任务参数接口
 * @author wangchanghui
 *
 */
public interface TaskParaService extends BaseService<TaskPara, Long> {

	/**
	 * 查询参数集合
	 * @param map
	 * @return
	 */
	List<TaskPara> findParaList(Map<String,Object> map);
	
	/**
	 * 查询需要绑定的集合
	 * @param map
	 * @return
	 */
	List<TaskPara> findToBeBindedList(Map<String,Object> map);
}

package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementVO;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.RequirementAffix;

public interface RequirementAffixService extends BaseService<RequirementAffix, Long> {

    List<RequirementAffix> findRequirementAffixListByIds(final String... values);

    List<RequirementAffix> findRequirementAffixListByIds(final RequirementAffix... values);

    List<RequirementAffix> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId);
    
    List<RequirementAffix> findExpressionByParams(String businessType, String businessId,String fileType);

    Integer getMaxRequirementAffixID();

    List<RequirementAffix> findRequirementAffixListByReqId(Long reqId);
    
    public void deleteFromIds(String ids);

    public void insert(FileAffixVO fileAffixVO, long bussineId, String bussineType);

    // public Long update(RequirementVO requirementvo,long requirementId);

}

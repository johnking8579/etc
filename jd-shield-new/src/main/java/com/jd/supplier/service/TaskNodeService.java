package com.jd.supplier.service;

import java.util.List;
import java.util.Map;

import com.jd.official.core.service.BaseService;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.model.TaskNode;
import com.jd.supplier.model.TaskNodeAssignment;
/**
 * 任务节点服务接口
 * @author wangchanghui
 *
 */
public interface TaskNodeService extends BaseService<TaskNode, Long> {

	/**
	 * 查找节点集合
	 * @param map
	 * @return
	 */
	List<TaskNode> findNodeList(Map<String,Object> map);
	
	/**
	 * 添加初始节点
	 * @param taskId
	 * @return
	 */
	Boolean addInitNode(Long taskId);
	
	/**
	 * 查询初始节点
	 * @param nodeId
	 * @param nodeStatus
	 * @return
	 */
	TaskNode findInitNode(Long nodeId,Integer nodeStatus);
	
	/**
	 * 通过角色code查找任务分配用户集合
	 * @param roleCode
	 * @return
	 */
	List<User> findTaskAssignUsers(String roleCode);
	
	/**
	 * 新增保存任务指派
	 * @param taskNodeAssignment
	 * @param nodeName
	 * @return
	 */
	Boolean addSaveAssignment(TaskNodeAssignment taskNodeAssignment,String nodeName);
	
	/**
	 * 检查节点状态
	 * @param nodeId
	 * @param roleCode
	 * @param nodeStatus
	 * @return
	 */
	Integer checkNodeStatus(Long nodeId,String roleCode,Integer nodeStatus);
	
	/**
	 * 更新节点状态
	 * @param taskNode
	 * @return
	 */
	Boolean updateNodeStatus(TaskNode taskNode);
	
	/**
	 * 更新任务认领
	 * @param taskNode
	 * @return
	 */
	Boolean updateTaskClaim(TaskNode taskNode);
	
	/**
	 * 检查是否存在认领
	 * @param nodeId
	 * @param userId
	 * @return
	 */
	Boolean ifExistsClaim(Long nodeId,Long userId);
	
	/**
	 * 通过父节点查找对象
	 * @param id
	 * @return
	 */
	TaskNode getByParentId(Long id);
	
	/**
	 * 更新任务驳回
	 * @param taskNode
	 * @return
	 */
	Boolean updateTaskReject(TaskNode taskNode);
	
	/**
	 * 更新驳回状态
	 * @param taskNode
	 * @return
	 */
	Boolean updateRejectNodeStatus(TaskNode taskNode);
	
	/**
	 * 新增入围完成
	 * @param nodeId
	 * @return
	 */
	Boolean addFinalSelectionComplete(Long nodeId);
	
	/**
	 * 更新入围驳回
	 * @param taskNode
	 * @return
	 */
	Boolean updateFinalSelectionReject(TaskNode taskNode);
	
	/**
	 * 更新认领驳回
	 * @param taskNode
	 * @return
	 */
	Boolean updateClaimReject(TaskNode taskNode);
	
	/**
	 * 添加任务指派
	 * @param taskNodeAssignment
	 * @return
	 */
	Boolean addAssignment(TaskNodeAssignment taskNodeAssignment);
	
	/**
	 * 添加入围确认
	 * @param id
	 * @return
	 */
	Boolean addFinalSelectionConfirmCompleted(Long id);
}

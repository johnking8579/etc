package com.jd.supplier.service;

/**
 * 专家数量不足
 * 
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年5月3日
 */
public class NotEnoughExpertException extends Exception	{

	public NotEnoughExpertException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotEnoughExpertException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughExpertException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughExpertException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}

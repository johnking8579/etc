package com.jd.supplier.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.jd.common.constants.CharacterConstant;
import com.jd.common.enm.DeleteEnum;
import com.jd.official.core.jss.service.JssService;
import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.controller.vo.SpecialSupplierVO;
import com.jd.supplier.dao.RequirementAffixDao;
import com.jd.supplier.dao.TaskDao;
import com.jd.supplier.dao.TaskSuppSelectionDao;
import com.jd.supplier.enm.SupplierSelectionEnum;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
import com.jd.supplier.service.TaskSuppSelectionService;

/**
 * 预选入围服务接口实现
 * @author wangchanghui
 *
 */
@Service("taskSuppSelectionService")
public class TaskSuppSelectionServiceImpl extends BaseServiceImpl<TaskSuppSelection, Long> implements
		TaskSuppSelectionService {
	
	private static Logger logger = Logger.getLogger(TaskSuppSelectionServiceImpl.class);
	
	@Autowired
	private TaskSuppSelectionDao taskSuppSelectionDao;
	
	@Autowired
	private RequirementAffixDao requirementAffixDao;
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private JssService jssService;
	
	@Value("${jss.bucket}")
	private String bucketName;
	
	@Value("${jss.temp.directory}")
	private String tempDir;

	/**
	 * 返回true可以删除
	 */
	@Override
	public synchronized boolean checkSuppSectionCanDelete(Map<String, Object> map) {
		map.put("status",DeleteEnum.YES);
		return taskSuppSelectionDao.findTaskSuppSelectionSize(map)>0;
	}

	/**
	 * 根据需求id查找预选集合
	 */
	@Override
	public List<Supplier> findTaskSuppSelectionListByReqId(Long requirementId){
	    return taskSuppSelectionDao.findTaskSuppSelectionListByReqId(requirementId);
	}


	public TaskSuppSelectionDao getDao() {
		return taskSuppSelectionDao;
	}

	/**
	 * 保存特殊供应商
	 */
	@Override
	public void saveSpecialSupplier(SpecialSupplierVO specialSupplierVO) {
		String delimiter = "|";
		String name = specialSupplierVO.getName();
		String contactor = specialSupplierVO.getContactor();
		String mobile = specialSupplierVO.getMobile();
		String remarks = specialSupplierVO.getRemarks();
		StringBuffer sb = new StringBuffer().append(name).append(CharacterConstant.DELIMITER).append(contactor).append(delimiter)
				.append(mobile).append(delimiter).append(remarks);
		TaskSuppSelection taskSuppSelection = new TaskSuppSelection();
		taskSuppSelection.setTaskId(specialSupplierVO.getTaskId());
		taskSuppSelection.setRemarks(sb.toString());
		taskSuppSelection.setFinalFlag(SupplierSelectionEnum.PREVIOUS.getType());
		getDao().insert(taskSuppSelection);
	}

	/**
	 * 更新预选或入围状态
	 */
	@Override
	public void updatePreSelection(TaskSuppSelection taskSuppSelection) {
		if(taskSuppSelection == null)
			return;
		//更新调查报告
		if(StringUtils.isNotBlank(taskSuppSelection.getInvestigationReportId())
				&& StringUtils.isNotBlank(taskSuppSelection.getInvestigationReportName())){
			addAffix(taskSuppSelection.getId(),taskSuppSelection.getInvestigationReportName(),
					taskSuppSelection.getInvestigationReportId(),"1");
		}
		//更新样本测试
		if(StringUtils.isNotBlank(taskSuppSelection.getSampleTestReportName())
				&& StringUtils.isNotBlank(taskSuppSelection.getSampleTestReportId())){
			addAffix(taskSuppSelection.getId(),taskSuppSelection.getSampleTestReportName(),
					taskSuppSelection.getSampleTestReportId(),"2");
		}
		
		getDao().update(taskSuppSelection);
	}
	
	/**
	 * 添加文件信息
	 * @param bussineId
	 * @param fileName
	 * @param fileKey
	 * @param type
	 */
	private void addAffix(Long bussineId,String fileName,String fileKey,String type){
		RequirementAffix affix = new RequirementAffix();
		affix.setBussineId(bussineId);
		affix.setFileType(type);
		affix.setBussineType("4");
		affix =	requirementAffixDao.findByPara(affix);
		if(affix == null){
			insertAffix(bussineId, fileName,fileKey,type);
		}else{
			updateAffix(affix.getId(),fileName,fileKey);
		}
	}
	
	/**
	 * 插入文件存放表
	 * @param BussieId
	 * @param fileName
	 * @param fileKey
	 * @param fileType
	 */
	private void insertAffix(Long BussieId,String fileName,String fileKey,String fileType){
		RequirementAffix affix = new RequirementAffix();
		affix.setBussineId(BussieId);
		affix.setBussineType("4");
		affix.setFileName(fileName);
		affix.setFileType(fileType);
		affix.setFileKey(fileKey);
		requirementAffixDao.insert(affix);
	}
	
	/**
	 * 更新文件表
	 * @param id
	 * @param fileName
	 * @param fileKey
	 */
	private void updateAffix(Long id,String fileName,String fileKey){
		RequirementAffix rAffix = new RequirementAffix();
		rAffix.setId(id);
		rAffix.setFileName(fileName);
		rAffix.setFileKey(fileKey);
		requirementAffixDao.update(rAffix);
	}


	public static HttpHeaders httpHeaderExcelFileAttachment(final String fileName,
	        final int fileSize) {
	    String encodedFileName = fileName.replace('"', ' ').replace(' ', '_');

	    HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.setContentType(MediaType.parseMediaType("APPLICATION/OCTET-STREAM"));
	    responseHeaders.setContentLength(fileSize);
	    responseHeaders.set("Content-Disposition", "attachment");
	    responseHeaders.add("Content-Disposition", "filename=\"" + encodedFileName + '\"');
	    return responseHeaders;
	}

	/**
	 * 下载文件
	 */
	public void downloadFile(Long id, String fileType,HttpServletResponse response) {
		RequirementAffix affix = new RequirementAffix();
		affix.setBussineId(id);
		affix.setFileType(fileType);
		affix.setBussineType("4");
		affix =	requirementAffixDao.findByPara(affix);
//		if(affix == null)
//			return null;
		String key = affix.getFileKey();
		String fileName = affix.getFileName();
//		long fileLength = file.length();
		if(fileName.indexOf(".xls") != -1)
			response.setContentType("application/msexcel");
		else
			response.setContentType("APPLICATION/OCTET-STREAM");
//		response.setContentType("UTF-8");
        try {
			response.setHeader("Content-disposition", "attachment; filename="
			        + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
//        response.setHeader("Content-Length", String.valueOf(fileLength));
        InputStream inputStream = jssService.downloadFile(bucketName, key);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        BufferedOutputStream bufferedOutputStream = null;
		try {
			bufferedOutputStream = new BufferedOutputStream(response.getOutputStream());
			byte[] buff = new byte[2048];
	        int bytesRead;
	        while (-1 != (bytesRead = bufferedInputStream.read(buff, 0, buff.length))) {
	        	bufferedOutputStream.write(buff, 0, bytesRead);
	        }
	        response.setStatus(response.SC_OK);
	        response.flushBuffer();
		} catch (FileNotFoundException e) {
			logger.error("", e);
		}catch(IOException e){
			logger.error("", e);
		}finally{
			try {
				if(bufferedOutputStream != null)
					bufferedOutputStream.close();
				if(bufferedInputStream != null)
					bufferedInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        
	}

	/**
	 * 保存预选或入围信息
	 * @param taskSuppSelection
	 * @param supplierIds
	 * @param finalStatus
	 */
	public void saveSelection(TaskSuppSelection taskSuppSelection,
			String supplierIds,Integer finalStatus) {
		if(StringUtils.isNotBlank(supplierIds)){
			String ids[] = supplierIds.trim().split(",");
			for(int i=0;i<ids.length;i++){
				TaskSuppSelection t = new TaskSuppSelection();
				BeanUtils.copyProperties(taskSuppSelection, t);
//				t.setSupplierId(Long.valueOf(ids[i]));
				saveSelection(t,finalStatus,Long.valueOf(ids[i]));
			}
		}else{
			saveSelection(taskSuppSelection,finalStatus,taskSuppSelection.getSupplierId());
		}
	}
	
	/**
	 * 保存预选或入围信息
	 * @param taskSuppSelection
	 * @param supplierIds
	 * @param finalStatus
	 */
	private void saveSelection(TaskSuppSelection t,Integer finalStatus,Long id){
//		t.setStatus(DeleteEnum.NO.getStatus());//1状态为可用
		t.setFinalFlag(finalStatus);
		if(finalStatus == SupplierSelectionEnum.PREVIOUS.getType()){
			Long requirementId = taskDao.get(t.getTaskId()).getRequirementId();
			t.setRequirementId(requirementId);
			t.setSupplierId(id);
			taskSuppSelectionDao.insert(t);
		}else{
			if(t.getId()==null)
				t.setId(id);
			taskSuppSelectionDao.update(t);
		}
	}

	/**
	 * 保存预选
	 */
	@Override
	public Boolean savePreSelection(TaskSuppSelection taskSuppSelection,
			String supplierIds) {
		saveSelection(taskSuppSelection, supplierIds,SupplierSelectionEnum.PREVIOUS.getType());
		return true;
	}

	/**
	 * 保存入围
	 */
	@Override
	public Boolean saveFinalSelection(TaskSuppSelection taskSuppSelection,
			String supplierIds) {
		saveSelection(taskSuppSelection, supplierIds,SupplierSelectionEnum.FINAL.getType());
		return true;
	}

	/**
	 * 查询预选或入围数量
	 */
	@Override
	public Integer findSelectionCount(Long taskId, Integer finalFlag) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("taskId", taskId);
		map.put("finalFlag", finalFlag);
		return taskSuppSelectionDao.findSelectionCount(map);
	}
}

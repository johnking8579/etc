/**
 * 
 */
package com.jd.supplier.service;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.model.TaskParaRela;

/**
 * 任务参数关系服务接口
 * @author wangchanghui
 *
 */
public interface TaskParaRelaService extends BaseService<TaskParaRela, Long> {
	/**
	 * 批量添加
	 * @param taskParaRela
	 * @param ids
	 * @return
	 */
	Boolean addBatch(TaskParaRela taskParaRela,String ids);
}

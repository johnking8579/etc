package com.jd.supplier.service;

import java.io.IOException;
import java.util.List;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertExtraction;
import com.jd.supplier.model.Extraction;

public interface ExtractionService {

	/**
	 * 根据抽取条件进行专家抽取
	 * @param extraction
	 * @return
	 */
	List<Expert> insertExtract(Extraction extraction) ;

	void insertAddition(List<Expert> experts, long extractionId, String extractMode);
	
	Page<Extraction> find(String prjName, String creator, String startTime,
			String endTime, int offset, int pageSize);

	byte[] toExcel(List<Extraction> list) throws IOException;

	Extraction findById(long id);
	
	/**
	 * 计算抽取总人数,回复人数,需要补录人数等等数据
	 * @param extractionId
	 * @return new int[]{预设抽取总人数, 已抽取总人数, 需人工补录数, 未回复人数, 已确认参加人数, 已拒绝人数}
	 */
	int[] calculateExpertCount(long extractionId);
	
	List<ExpertExtraction> findExpertExtraction(long extractionId);
	
	ExpertExtraction findExpertExtraction(long reqId, long expertId);

	Extraction findByReqId(long reqId);

	void saveExpertExtraction(ExpertExtraction ee);

	boolean hasExtracted(long extractionId, long expertId);



}

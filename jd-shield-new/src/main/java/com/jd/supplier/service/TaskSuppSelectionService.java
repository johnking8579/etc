package com.jd.supplier.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.controller.vo.SpecialSupplierVO;
import com.jd.supplier.model.Supplier;
import com.jd.supplier.model.TaskSuppSelection;
/**
 * 入围预选服务接口
 * @author wangchanghui
 *
 */
public interface TaskSuppSelectionService extends BaseService<TaskSuppSelection, Long> {
	
	/**
	 * 查找入围信息
	 * @param taskId
	 * @return
	 */
	List<Supplier> findTaskSuppSelectionListByReqId(Long taskId);
	
	/**
	 * 检查是否可以删除
	 * @param map
	 * @return
	 */
	boolean checkSuppSectionCanDelete(Map<String,Object> map);
	
	/**
	 * 保存特殊供应商
	 * @param specialSupplierVO
	 */
	void saveSpecialSupplier(SpecialSupplierVO specialSupplierVO);
	
	/**
	 * 更新预选信息
	 * @param taskSuppSelection
	 */
	void updatePreSelection(TaskSuppSelection taskSuppSelection);
	
	/**
	 * 删除文件
	 * @param id
	 * @param fileType
	 * @param response
	 */
	void downloadFile(Long id,String fileType,HttpServletResponse response);
	
	/**
	 * 保存预选
	 * @param taskSuppSelection
	 * @param supplierIds
	 * @return
	 */
	Boolean savePreSelection(TaskSuppSelection taskSuppSelection,String supplierIds);
	
	/**
	 * 保存入围信息
	 * @param taskSuppSelection
	 * @param supplierIds
	 * @return
	 */
	Boolean saveFinalSelection(TaskSuppSelection taskSuppSelection,String supplierIds);
	
	/**
	 * 根据任务id查找预选、入围的数量
	 * @param taskId
	 * @param finalFlag
	 * @return
	 */
	Integer findSelectionCount(Long taskId,Integer finalFlag);
}

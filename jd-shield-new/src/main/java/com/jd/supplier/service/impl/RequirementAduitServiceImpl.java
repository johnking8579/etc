package com.jd.supplier.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.util.DateParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.official.core.utils.DateUtils;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementAduitVO;
import com.jd.supplier.dao.RequirementAduitDao;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.dao.RequirementTaskDao;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.RequirementAduit;
import com.jd.supplier.model.RequirementTask;
import com.jd.supplier.service.RequirementAduitService;
import com.jd.supplier.service.RequirementAffixService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.service.TaskNodeService;
import com.jd.supplier.util.SupplierConstant;

@Service("requirementAduitService")
@Transactional
public class RequirementAduitServiceImpl extends BaseServiceImpl<RequirementAduit, Long> implements
	RequirementAduitService {
    @Autowired
    private RequirementAduitDao requirementAduitDao;

    @Autowired
    private RequirementDao requirementDao;

    @Autowired
    private RequirementTaskDao requirementTaskDao;

    @Autowired
    private RequirementService requirementService;
    
    @Autowired
    private TaskNodeService taskNodeService;
    
    @Autowired
    private RequirementAffixService requirementAffixService;

    public RequirementAduitDao getDao() {
	return requirementAduitDao;
    }
    
    @Override
    public void insertReqAduitMetting(RequirementAduitVO requirementAduitvo, FileAffixVO fileAffixvo) throws Exception{
	requirementAffixService.insert(fileAffixvo, requirementAduitvo.getRequirementId(),
		SupplierConstant.BUSSINETYPE_REQADUITMETTING);
	Requirement requirement = requirementService.get(requirementAduitvo.getRequirementId());
	requirement.setReqStatus(SupplierConstant.REQCONFIGMETTING);
	requirementService.update(requirement);
    }
    

    /**
     * author duandongdong
     * date 2014年5月9日
     * desc: flag 确认需求或者驳回，驳回不插入任务单；确认和驳回修改不同的需求状态
     */
    @Override
    public void insert(RequirementAduitVO requirementAduitvo, RequirementAduit requirementAduit, String flag)
	    throws Exception {
	    //删除原有数据
	    Map<String ,Object> reqMap=new HashMap<String, Object>();
	    reqMap.put("requirementId", requirementAduitvo.getRequirementId());
	    List<RequirementAduit> requirementAduitList=requirementAduitDao.find("findByMap",reqMap);
	    
	    if(requirementAduitList!=null){
		for(int i=0;i<requirementAduitList.size();i++){
		    RequirementAduit requirementAduitTmp=(RequirementAduit)requirementAduitList.get(i);
		    requirementAduitDao.delete(requirementAduitTmp.getId());
		}
	    }
	    setRequirementAduitAttr(requirementAduitvo, requirementAduit, flag);
	    requirementAduitDao.insert(requirementAduit);
	    
	if (flag.equals(SupplierConstant.REQCONFIG)) {
	    Requirement requirement = requirementService.get(requirementAduitvo.getRequirementId());
	    requirement.setReqStatus(SupplierConstant.REQCONFIG);
	    requirementDao.update(requirement);
	    RequirementTask requirementTask = new RequirementTask();
	    setReqTaskAttr(requirementTask, requirement);
	    Long taskId = requirementTaskDao.insert(requirementTask);
	    taskNodeService.addInitNode(taskId);
	}else if(flag.equals(SupplierConstant.REQREFUSE)){
	    Requirement requirement = requirementService.get(requirementAduitvo.getRequirementId());
	    requirement.setReqStatus(SupplierConstant.REQREFUSE);
	    requirementDao.update(requirement);
	}
    }

    private void setRequirementAduitAttr(RequirementAduitVO requirementAduitvo, RequirementAduit requirementAduit,
	    String flag) throws Exception {
	requirementAduit.setExamineStatus(requirementAduitvo.getExamineStatus());
	/*requirementAduit.setCreateTime(new Date());
	requirementAduit.setCreator(requirementAduitvo.getCreator());*/
	if (requirementAduitvo.getId() != null && !requirementAduitvo.getId().equals("")) {
	    requirementAduit.setId(Long.valueOf(requirementAduitvo.getId()));
	} else {
	    requirementAduit.setId(null);
	}
	requirementAduit.setRequirementId(requirementAduitvo.getRequirementId());
	requirementAduit.setMark(requirementAduitvo.getMark());
	/*requirementAduit.setModifier(requirementAduitvo.getModifier());
	requirementAduit.setModifyTime(new Date());
	requirementAduit.setStatus(SupplierConstant.AVAILABLESTATUS);*/
	if(flag.equals(SupplierConstant.REQCONFIG)){
	    requirementAduit.setExamineStatus(SupplierConstant.REQCONFIG);
	    setAduitTime(requirementAduitvo, requirementAduit);
	}else{
	    requirementAduit.setExamineStatus(SupplierConstant.REQREFUSE);
	}
    }

    private void setAduitTime(RequirementAduitVO requirementAduitvo, RequirementAduit requirementAduit)
	    throws DateParseException {
	requirementAduit.setConfigtime(new Date());
	requirementAduit
		.setOutputtime(DateUtils.convertFormatDateString_yyyy_MM_dd(requirementAduitvo.getOutputtime()));
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.add(Calendar.DAY_OF_YEAR, 3);
	Date configMeetIngTime = calendar.getTime();
	requirementAduit.setConfigmeetingtime(configMeetIngTime);
	Calendar calendar1 = Calendar.getInstance();
	calendar1.setTime(new Date());
	calendar1.add(Calendar.DAY_OF_YEAR, -15);
	Date projecttime = calendar1.getTime();
	requirementAduit.setProjecttime(projecttime);
    }

    private RequirementTask setReqTaskAttr(RequirementTask requirementTask, Requirement requirement) {
	/*User user = ComUtils.getCurrentLoginUser();
	requirementTask.setCreateTime(new Date());
	requirementTask.setCreator(user.getUserCode());*/
	requirementTask.setId(null);
	requirementTask.setRequirementId(requirement.getId());
	requirementTask.setRemarks("");
	/*requirementTask.setModifier(user.getUserName());
	requirementTask.setModifyTime(new Date());*/
	requirementTask.setTaskStatus(SupplierConstant.TASKASSIGN);
	/*requirementTask.setStatus(SupplierConstant.AVAILABLESTATUS);*/
	return requirementTask;
    }

    @Override
    public RequirementAduit findRequirementAduitByReqId(Long reqId) {
	RequirementAduit requirementAduit = (RequirementAduit) requirementAduitDao.get("findRequirementAduitByReqId", reqId);
	return requirementAduit;
    }

    @Override
    public List<RequirementAduit> deleteOnlyUnused(String ql, Object... values) {
	List<RequirementAduit> listUnDelete = new ArrayList<RequirementAduit>();
	if (values != null) {
	    for (int i = 0; i < values.length; i++) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("expressionid", values[i]);

		List<RequirementAduit> list = requirementAduitDao.find("findByExpressionId", map);
		if (list != null && list.size() > 0) {
			listUnDelete.add((RequirementAduit) get(values[i].toString()));
		}else{
		    delete(ql, new Object[] { values[i] });
		}

	    }
	}
	return listUnDelete;
    }

    @Override
    public Integer getMaxRequirementAduitID() {
	return (Integer) requirementAduitDao.get("getMaxRequirementAduitID", null);
    }

    @Override
    public List<RequirementAduit> findRequirementAduitListByIds(String... values) {
	List<RequirementAduit> list = new ArrayList<RequirementAduit>();
	for (int i = 0; i < values.length; i++) {
	    RequirementAduit authExpression = (RequirementAduit) get(Long.valueOf(values[i]));
	    if (authExpression != null)
		list.add(authExpression);
	}
	return list;

    }

    @Override
    public List<RequirementAduit> findRequirementAduitListByIds(RequirementAduit... values) {
	List<RequirementAduit> list = new ArrayList<RequirementAduit>();
	for (int i = 0; i < values.length; i++)
	    list.add((RequirementAduit) get(values[i].getId()));
	return list;
    }

    @Override
    public List<RequirementAduit> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId) {
	Map<String, String> map = new HashMap<String, String>();
	map.put("businessType", businessType);
	map.put("businessId", businessId);
	List<RequirementAduit> list = requirementAduitDao.find("findExpressionByBusinessIdAndBusinessType", map);
	return list;
    }

}

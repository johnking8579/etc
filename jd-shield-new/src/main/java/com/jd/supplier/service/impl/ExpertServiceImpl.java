package com.jd.supplier.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.common.util.Util;
import com.jd.official.core.dao.Page;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.modules.dict.dao.DictDataDao;
import com.jd.official.modules.dict.model.DictData;
import com.jd.official.modules.system.dao.UserDao;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.dao.ExpertDao;
import com.jd.supplier.dao.ExpertTagDao;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.ExpertTag;
import com.jd.supplier.service.ExpertService;

@Component("expertService")
public class ExpertServiceImpl implements ExpertService{
	static Logger log = Logger.getLogger(ExpertServiceImpl.class);
	
	@Resource
	private ExpertDao expertDao;
	@Resource
	private DictDataDao dictDataDao;
	@Resource
	private ExpertTagDao expertTagDao;
	@Resource
	private UserDao userDao;
	@Resource
	private ShardedXCommands redisCommands;
	
	@Override
	public Page<Expert> findAll(int offset, int pageSize) {
		return expertDao.findAll(offset, pageSize);
	}

	@Override
	public List<DictData> findAllAvailableTag() {
		List<DictData> tags = new ArrayList<DictData>();
		List<DictData> tag1s = dictDataDao.findDictDataList(Expert.DICT_TYPE_一级分类标签);
		tags.addAll(tag1s);
		for(DictData tag1 : tag1s)	{
			List<DictData> tag2s = dictDataDao.findDictDataList(Expert.DICT_TYPE_二级分类标签, tag1.getId());
			tags.addAll(tag2s);
			for(DictData tag2 : tag2s)	{
				List<DictData> tag3s = dictDataDao.findDictDataList(Expert.DICT_TYPE_三级分类标签, tag2.getId());
				tags.addAll(tag3s);
			}
		}
		return tags;
	}

	@Override
	public List<Expert> findLikeRealName(String name) {
		return expertDao.findLikeRealName(name);
	}

	@Override
	public Page<Expert> find(Map<String, Object> condition, int offset,
			int pageSize) {
		return expertDao.find(condition, offset, pageSize);
	}

	@Override
	public void save(Expert expert) {
		expertDao.save(expert);
	}

	@Override
	public Expert findById(long id) {
		return expertDao.findById(id);
	}
	
	@Override
	public List<Expert> findByIds(long[] ids) {
		return expertDao.findByIds(ids);
	}

	@Override
	public void saveExpertAndTags(Expert expert, List<ExpertTag> tags, int[] delTagId) {
		Long id = expertDao.save(expert);
		Set<ExpertTag> set = new LinkedHashSet<ExpertTag>(tags);//过滤重复值 
		for(ExpertTag t : set)	{
			t.setExpertId(id.intValue());
			expertTagDao.save(t);
		}
		for(int i : delTagId)	{
			expertTagDao.delete(i);
		}
	}

	@Override
	public List<ExpertTag> findExpertTag(long expertId) {
		return expertTagDao.findByExpertId(expertId);
	}
	
	@Override
	public final String insertFromExcel(InputStream is, String fileName) throws IOException	{
		Workbook read, write;
		FileOutputStream fos = null;
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		try	{
			if(fileName.endsWith(".xlsx"))	{
				read = new XSSFWorkbook(is);
				read.write(buf);
				write = new XSSFWorkbook(new ByteArrayInputStream(buf.toByteArray()));	//复制workbook,做为反馈用
			} else if(fileName.endsWith(".xls")){
				read = new HSSFWorkbook(is);
				read.write(buf);
				write = new HSSFWorkbook(new ByteArrayInputStream(buf.toByteArray()));	//复制workbook,做为反馈用
			} else	{
				throw new BusinessException("不支持的EXCEL文件:" + fileName); 
			}
			
			this.doOperate(read, write);
			
			String key = "feedback_" + UUID.randomUUID().toString() + ".xlsx";
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			write.write(baos);
			this.putToRedis(key, baos.toByteArray(), 3600); 	//1小时后过期
			return key;
		} finally	{
			Util.closeStream(is, fos);
		}
	}
	
	private void doOperate(Workbook wbRead, Workbook wbWrite) {
		Sheet read = wbRead.getSheetAt(0), write = wbWrite.getSheetAt(0);
		if(write == null)	throw new RuntimeException();
		wbWrite.setActiveSheet(0);
		
		List<DictData> groups = dictDataDao.findDictDataList(Expert.DICT_TYPE_专家组别),
						regions = dictDataDao.findDictDataList(Expert.DICT_TYPE_所属区域);
		
		CellStyle success = wbWrite.createCellStyle();
		Font f1 = wbWrite.createFont();
		f1.setColor(HSSFColor.BLUE.index);
		success.setFont(f1);
		
		CellStyle fail = wbWrite.createCellStyle();
		Font f2 = wbWrite.createFont();
		f2.setColor(HSSFColor.RED.index);
		fail.setFont(f2);

		int i = 1;	//起始行
		while (true) {
			CellStyle style = null;
			String msg;
			Row row = read.getRow(i);
			try	{
				if (row == null)	{
					write.createRow(i).createCell(0).setCellValue("读取结束");
					break;	//遇到空白行, 则读取完成
				}
				
				String userName = row.getCell(1).getStringCellValue();
				User user = userDao.getByUserName(userName);
				Expert expert = expertDao.findByUserName(userName);
				
				if(expert != null)	{		//修改专家资料
					expert.setLeader(row.getCell(0).getStringCellValue());
					expert.setModifier(ComUtils.getLoginNamePin());
					expert.setModifyTime(new Date());
					
					DictData region = chooseRegionFromXls(row.getCell(2).getStringCellValue(), regions);
					expert.setRegion(region);
					DictData grp = this.chooseGroupFromXls(row.getCell(3).getStringCellValue(), groups);
					expert.setGroup(grp);
					Integer status = chooseStatusFromXls(row.getCell(4).getStringCellValue());
					expert.setStatus(status);
					
					expertDao.save(expert);
					msg = "修改成功";
					style = success;
						
				} else if(user != null)	{	//新增专家
					expert = new Expert();
					expert.setCreateTime(new Date());
					expert.setCreator(ComUtils.getLoginNamePin());
					expert.setEntryMode(Expert.ENTRY_MODE_文件导入);
					expert.setLeader(row.getCell(0).getStringCellValue());
					expert.setUser(user);
					
					DictData region = chooseRegionFromXls(row.getCell(2).getStringCellValue(), regions);
					expert.setRegion(region);
					DictData grp = this.chooseGroupFromXls(row.getCell(3).getStringCellValue(), groups);
					expert.setGroup(grp);
					Integer status = chooseStatusFromXls(row.getCell(4).getStringCellValue());
					expert.setStatus(status);
					
					expertDao.save(expert);
					msg = "新增成功";
					style = success;
					
				} else	{	//员工不存在
					msg = "数据库中没有找到该员工:" + userName;
					style = fail;
				}
				
				Cell c = write.getRow(i).createCell(row.getLastCellNum());	//最后一列这行处理结果
				c.setCellStyle(style);
				c.setCellValue(msg);
				
			} catch(Exception e)	{
				e.printStackTrace();
				if(write.getRow(i) != null){
					Cell c = write.getRow(i).createCell(row.getLastCellNum());	//最后一列写入失败信息
					if(c != null){
						c.setCellStyle(fail);
						c.setCellValue(e.getMessage());	
					}
				}
			}
			i++;
		}
	}
	
	private DictData chooseRegionFromXls(String region, List<DictData> regions)	{
		if(region == null)	
			throw new IllegalArgumentException("专家区域不能为空");
		for(DictData d : regions)	{
			if(d.getDictDataName().trim().equals(region.trim()))	{
				return d;
			}
		}
		
		List<String> available = new ArrayList<String>();
		for(DictData d : regions)	{
			available.add(d.getDictDataName());
		}
		
		throw new IllegalArgumentException(String.format("不识别的所属区域:%s. 可用区域名称:%s", region, Util.join(available, "|")));
	}
	
	private DictData chooseGroupFromXls(String groupName, List<DictData> groups)	{
		if(groupName == null)	
			throw new IllegalArgumentException("专家组别不能为空");
		for(DictData d : groups)	{
			if(d.getDictDataName().trim().equals(groupName.trim()))	{
				return d;
			}
		}
		
		List<String> available = new ArrayList<String>();
		for(DictData d : groups)	{
			available.add(d.getDictDataName());
		}
		
		throw new IllegalArgumentException(String.format("不识别的专家组别名称:%s. 可用组别名称:%s", groupName, Util.join(available, "|")));
	}
	
	private Integer chooseStatusFromXls(String status)	{
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("解冻", 0);
		map.put("冻结", 1);
		
		for(Entry<String,Integer> s : map.entrySet())	{
			if(s.getKey().equals(status)){
				return s.getValue();
			}
		}
		throw new IllegalArgumentException(String.format("不识别的状态:%s. 可用状态名称:%s", status, Util.join(new ArrayList<String>(map.keySet()), "|")));
	}
	
	private void putToRedis(String key, byte[] bytes, int expireSec) throws IOException {
        redisCommands.setex(RedisCacheSerializer.serialize(key), expireSec, bytes);
    }
	
}

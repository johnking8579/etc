package com.jd.supplier.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Advertising;

public interface AdvertisingService {

	Page<Advertising> findLikeAdvertiser(Map<String, Object> condition, 
			String orderBy, String order, int offset, int pageSize);

	void save(Advertising adv);

	Advertising findById(long id);

	void delete(long id);

	Map<String, String> findPositionValue();

	String[] uploadToCloud(MultipartFile file) throws IOException;
	
	void deleteFromCloud(String key);

	Page<Advertising> findByPosition(String position, int offset, int pageSize);

	/**
	 * 保存顺序
	 * @param sorted 按顺序排列好的advertising.id
	 */
	void saveOrder(List<long[]> sorted);
}

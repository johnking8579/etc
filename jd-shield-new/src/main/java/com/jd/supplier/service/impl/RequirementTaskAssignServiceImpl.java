package com.jd.supplier.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.dao.RequirementTaskAssignDao;
import com.jd.supplier.model.RequirementTaskAssign;
import com.jd.supplier.service.RequirementTaskAssignService;

@Service("requirementTaskAssignService")
@Transactional
public class RequirementTaskAssignServiceImpl extends BaseServiceImpl<RequirementTaskAssign, Long> implements RequirementTaskAssignService {
    @Autowired
    private RequirementTaskAssignDao requirementTaskAssignDao;

    public RequirementTaskAssignDao getDao() {
	return requirementTaskAssignDao;
    }

    @Override
    public List<RequirementTaskAssign> deleteOnlyUnused(String ql, Object... values) {
	List<RequirementTaskAssign> listUnDelete = new ArrayList<RequirementTaskAssign>();
	if (values != null) {
	    for (int i = 0; i < values.length; i++) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("expressionid", values[i]);

		List<RequirementTaskAssign> list = requirementTaskAssignDao.find("findByExpressionId", map);
		if (list != null && list.size() > 0) {
		    listUnDelete.add((RequirementTaskAssign) get(values[i].toString()));
		} else{
			delete(ql, new Object[] { values[i] });
		}

	    }
	}
	return listUnDelete;
    }

    @Override
    public Integer getMaxRequirementTaskAssignID() {
	return (Integer) requirementTaskAssignDao.get("getMaxRequirementTaskAssignID", null);
    }

    @Override
    public List<RequirementTaskAssign> findRequirementTaskAssignListByIds(String... values) {
	List<RequirementTaskAssign> list = new ArrayList<RequirementTaskAssign>();
	for (int i = 0; i < values.length; i++) {
	    RequirementTaskAssign authExpression = (RequirementTaskAssign) get(Long.valueOf(values[i]));
	    if (authExpression != null)
		list.add(authExpression);
	}
	return list;

    }

    @Override
    public List<RequirementTaskAssign> findRequirementTaskAssignListByIds(RequirementTaskAssign... values) {
	List<RequirementTaskAssign> list = new ArrayList<RequirementTaskAssign>();
	for (int i = 0; i < values.length; i++)
	    list.add((RequirementTaskAssign) get(values[i].getId()));
	return list;
    }

    @Override
    public List<RequirementTaskAssign> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId) {
	Map<String, String> map = new HashMap<String, String>();
	map.put("businessType", businessType);
	map.put("businessId", businessId);
	List<RequirementTaskAssign> list = requirementTaskAssignDao.find("findExpressionByBusinessIdAndBusinessType", map);

	return list;
    }

}

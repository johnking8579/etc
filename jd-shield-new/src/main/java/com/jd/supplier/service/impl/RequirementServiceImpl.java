package com.jd.supplier.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.official.core.service.BaseServiceImpl;
import com.jd.supplier.controller.vo.FileAffixVO;
import com.jd.supplier.controller.vo.RequirementVO;
import com.jd.supplier.dao.RequirementDao;
import com.jd.supplier.model.Requirement;
import com.jd.supplier.model.RequirementAffix;
import com.jd.supplier.service.RequirementAffixService;
import com.jd.supplier.service.RequirementService;
import com.jd.supplier.util.SupplierConstant;

@Service("requirementService")
@Transactional
public class RequirementServiceImpl extends BaseServiceImpl<Requirement, Long> implements RequirementService {
    @Autowired
    private RequirementDao requirementDao;
    
    @Autowired
    private RequirementAffixService requirementAffixService;

    public RequirementDao getDao() {
	return requirementDao;
    }
    
     @Override
     public Long insert(RequirementVO requirementvo,Requirement requirement) {
	long requirementId= requirementDao.insert(requirement);
	FileAffixVO fileAffixVO = setFileAffixAttr(requirementvo);
	requirementAffixService.insert(fileAffixVO, requirementId,SupplierConstant.BUSSINETYPE_REQCOMMIT);
	return requirementId;
    }

    private FileAffixVO setFileAffixAttr(RequirementVO requirementvo) {
	FileAffixVO fileAffixVO=new FileAffixVO();
	fileAffixVO.setFileName1(requirementvo.getFileName1());
	fileAffixVO.setFileName2(requirementvo.getFileName2());
	fileAffixVO.setFileName3(requirementvo.getFileName3());
	fileAffixVO.setFileName4(requirementvo.getFileName4());
	fileAffixVO.setFileName5(requirementvo.getFileName5());
	fileAffixVO.setFileType(requirementvo.getFileType());
	return fileAffixVO;
    }
     
     @Override
     public Long update(RequirementVO requirementvo,Requirement requirement) {
	 long requirementId=requirementDao.update(requirement);
	 FileAffixVO fileAffixVO = setFileAffixAttr(requirementvo);
	 requirementAffixService.insert(fileAffixVO, Long.valueOf(requirementvo.getId()),SupplierConstant.BUSSINETYPE_REQCOMMIT);
	 return requirementId;
    }
     
    

    @Override
    public List<Requirement> deleteOnlyUnused(String ql, Object... values) {
	List<Requirement> listUnDelete = new ArrayList<Requirement>();
	if (values != null) {
	    for (int i = 0; i < values.length; i++) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("expressionid", values[i]);

		List<Requirement> list = requirementDao.find("findByExpressionId", map);
		if (list != null && list.size() > 0) {
			listUnDelete.add((Requirement) get(values[i].toString()));
		}else{
		    delete(ql, new Object[] { values[i] });
		}

	    }
	}
	return listUnDelete;
    }

    @Override
    public Integer getMaxRequirementID() {
	return (Integer) requirementDao.get("getMaxRequirementID", null);
    }

    @Override
    public List<Requirement> findRequirementListByIds(String... values) {
	List<Requirement> list = new ArrayList<Requirement>();
	for (int i = 0; i < values.length; i++) {
	    Requirement authExpression = (Requirement) get(Long.valueOf(values[i]));
	    if (authExpression != null)
		list.add(authExpression);
	}
	return list;

    }

    @Override
    public List<Requirement> findRequirementListByIds(Requirement... values) {
	List<Requirement> list = new ArrayList<Requirement>();
	for (int i = 0; i < values.length; i++)
	    list.add((Requirement) get(values[i].getId()));
	return list;
    }

    @Override
    public List<Requirement> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId) {
	Map<String, String> map = new HashMap<String, String>();
	map.put("businessType", businessType);
	map.put("businessId", businessId);
	List<Requirement> list = requirementDao.find("findExpressionByBusinessIdAndBusinessType", map);

	return list;
    }

}

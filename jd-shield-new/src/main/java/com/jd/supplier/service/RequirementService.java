package com.jd.supplier.service;

import java.util.List;

import com.jd.official.core.service.BaseService;
import com.jd.supplier.controller.vo.RequirementVO;
import com.jd.supplier.model.Requirement;

public interface RequirementService extends BaseService<Requirement, Long> {
    List<Requirement> deleteOnlyUnused(String ql, Object... values);

    List<Requirement> findRequirementListByIds(final String... values);

    List<Requirement> findRequirementListByIds(final Requirement... values);

    List<Requirement> findExpressionByBusinessIdAndBusinessType(String businessType, String businessId);

    Integer getMaxRequirementID();
    
    public Long insert(RequirementVO requirementvo,Requirement entity) ;
    
    public Long update(RequirementVO requirementvo,Requirement entity) ;
}

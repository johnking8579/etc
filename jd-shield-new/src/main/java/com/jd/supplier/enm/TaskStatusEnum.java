package com.jd.supplier.enm;
/**
 * 任务状态枚举
 * @author wangchanghui
 *
 */
public enum TaskStatusEnum {
	TASK_ASSIGNMENT(1,"任务指派","task_assignment"),
	TASK_CLAIM(2,"任务认领","task_claim"),
	REQUIREMENT_CONFIRM(3,"需求确认会","task_confirm"),
	PRE_SELECTION(4,"预选","task_preSelection"),
	FINAL_SELECTION(5,"入围","task_finalSelection"),
	FINAL_SELECTION_CONFIRM(6,"入围确认","task_finalSelection_confirm"),
	FINAL_SELECTION_CONFIRM_CP(13,"入围确认","task_finalSelection_confirm_cp");
	
	/**
	 * 类型
	 */
	private Integer type;
	
	/**
	 * 代码
	 */
	private String code;
	
	/**
	 * 名称
	 */
	private String name;
	
	TaskStatusEnum(Integer type,String name,String code){
		this.type = type;
		this.code = code;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

/**
 * 
 */
package com.jd.supplier.enm;

/**
 * 任务参数枚举
 * @author wangchanghui
 *
 */
public enum TaskParaEnum {
	TASK(1,"任务节点"),
	
	ROLE(2,"角色"),
	
	ORGANIZATION(3,"部门");
	
	/**
	 * 类型
	 */
	private Integer type;
	
	
	/**
	 * 名称
	 */
	private String name;
	

	TaskParaEnum(Integer type,String name){
		this.type = type;
		this.name = name;
	}
	public Integer getType() {
		return type;
	}


	public String getName() {
		return name;
	}


}

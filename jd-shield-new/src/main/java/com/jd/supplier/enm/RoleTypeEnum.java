package com.jd.supplier.enm;

/**
 * 角色类型枚举
 * @author wangchanghui
 *
 */
public enum RoleTypeEnum {
	ASSIGNMENT_ROLE("supplier_assignment_role","供应商管理部任务指派角色"),
	CLAIM_ROLE("supplier_claim_role","供应商管理部任务认领角色"),
	FINAL_SELECTON_CONFIRM("supplier_finalSelection_role","供应商管理部入围确认角色"),
	ASSIGNMENT_Q_ROLE("qualified_assignment_role","质量管理部任务指派角色"),
	CLAIM_Q_ROLE("qualified_claim_role","质量管理部任务认领角色"),
	ASSIGNMENT_B_ROLE("bidding_claim_role","招标部任务指派角色"),
	CLAIM_B_ROLE("bidding_claim_role","招标部任务认领角色");
	
	/**
	 * 代码
	 */
	private String code;
	
	/**
	 * 名称
	 */
	private String name;
	
	RoleTypeEnum(String code,String name){
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

/**
 * 
 */
package com.jd.supplier.enm;

/**
 * 操作枚举
 * @author wangchanghui
 *
 */
public enum OprationEnum {
	ADD(1,"添加"),
	
	DELETE(2,"删除"),
	
	UPDATE(3,"修改"),
	
	CUSTOM(4,"");
	
	/**
	 * 类型
	 */
	private final Integer type;
	
	/**
	 * 名称
	 */
	private String name;
	

	OprationEnum(Integer type,String name){
		this.type = type;
		this.name = name;
	}


	public Integer getType() {
		return type;
	}


	public String getName() {
		return name;
	}

}

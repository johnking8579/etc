package com.jd.supplier.enm;
/**
 * 任务分配状态枚举类
 * @author wangchanghui
 *
 */
public enum AssignmentStatusEnum {
	ASSIGNED(1,"已分配"),UNASSIGNED(0,"未分配"),CLAIMED(2,"已认领");
	
	/**
	 * 类型
	 */
	private final Integer type;
	
	/**
	 * 名称
	 */
	private String name;
	
	AssignmentStatusEnum(Integer type,String name){
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}


	public String getName() {
		return name;
	}

}

package com.jd.supplier.enm;
/**
 * 是或否枚举类
 * @author wangchanghui
 *
 */
public enum IsOrNotEnum {
	YES(1,"是"),
	No(0,"否");
	
	/**
	 * 类型
	 */
	private final Integer type;
	
	/**
	 * 名称
	 */
	private final String name;
	
	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	IsOrNotEnum(Integer type,String name){
		this.type = type;
		this.name = name;
	}
	
}

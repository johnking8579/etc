package com.jd.supplier.enm;
/**
 * 供应商意愿枚举
 * @author wangchanghui
 *
 */
public enum WillingEnum {
	TRUE(1,"愿意"),FALSE(0,"不愿意");
	
	/**
	 * 类型
	 */
	private final int type;
	
	/**
	 * 名称
	 */
	private String name;
	
	WillingEnum(int type,String name){
		this.type = type;
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}

}

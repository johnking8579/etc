/**
 * 
 */
package com.jd.supplier.enm;

/**
 * 节点状态枚举类
 * @author wangchanghui
 *
 */
public enum NodeStatusEnum {
	COMPLETED(2,"已完成","completed"),RUNNING(1,"执行中","running"),INITIAL(0,"未执行","initial");
	
	/**
	 * 类型
	 */
	private final Integer type;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 代码
	 */
	private String code;
	
	NodeStatusEnum(Integer type,String name,String code){
		this.type = type;
		this.name = name;
		this.code = code;
	}

	public Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	
}

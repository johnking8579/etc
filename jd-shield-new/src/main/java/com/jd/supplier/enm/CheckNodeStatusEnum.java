package com.jd.supplier.enm;
/**
 * 检查节点状态枚举类
 * @author wangchanghui
 *
 */
public enum CheckNodeStatusEnum {
	NO_PRIVILEGE(1,"没有权限"),
	NO_ACCESS(2,"前置节点未完成"),
	COMPLETED(3,"已经完成"),
	PRE_SELE_COMPLETED(4,"预选已经完成"),
	FINAL_SELE_COMPLETED(5,"入围已经完成"),
	SUCCESS(9,"成功");
	/**
	 * 类型
	 */
	private Integer type;
	
	/**
	 * 
	 * 名称
	 */
	private String name;
	
	CheckNodeStatusEnum(Integer type,String name){
		this.type = type;
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

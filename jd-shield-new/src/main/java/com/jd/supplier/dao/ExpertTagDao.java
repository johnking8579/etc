package com.jd.supplier.dao;

import java.util.List;

import com.jd.supplier.model.ExpertTag;

public interface ExpertTagDao {

	List<ExpertTag> findByExpertId(long expertId);

	/**
	 * 插入或修改
	 * @param t
	 * @return expertTag的主键
	 */
	Integer save(ExpertTag t);
	
//	ExpertTag findUnique(long expertId, Long tag1Id, Long tag2Id, Long tag3Id);

	void delete(int id);
	
}

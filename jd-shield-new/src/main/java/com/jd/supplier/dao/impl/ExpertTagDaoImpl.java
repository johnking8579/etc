package com.jd.supplier.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.ExpertTagDao;
import com.jd.supplier.model.ExpertTag;

@Component("expertTagDao")
public class ExpertTagDaoImpl extends MyBatisDaoImpl implements ExpertTagDao	{

	private static final String NAMESPACE = ExpertTag.class.getName();
	@Override
	public List<ExpertTag> findByExpertId(long expertId) {
		return getSqlSession().selectList(NAMESPACE + ".byExpertId", expertId);
	}
	
	@Override
	public Integer save(ExpertTag t) {
		if(t.getId() == null)	{
			getSqlSession().insert(NAMESPACE + ".insert", t);
		} else {
			getSqlSession().update(NAMESPACE + ".update", t);
		}
		return t.getId();
	}

	@Override
	public void delete(int id) {
		getSqlSession().delete(NAMESPACE + ".delete", id);
	}

}

package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskDao;
import com.jd.supplier.model.Task;

/**
 * 任务单数据层接口
 * @author wangchanghui
 *
 */
@Repository("taskDao")
public class TaskDaoImpl extends MyBatisDaoImpl<Task, Long> implements TaskDao {

}

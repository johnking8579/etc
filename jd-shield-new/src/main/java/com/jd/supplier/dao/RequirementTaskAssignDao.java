package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.RequirementTaskAssign;

public interface RequirementTaskAssignDao extends BaseDao<RequirementTaskAssign, Long> {

}

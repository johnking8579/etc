package com.jd.supplier.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.ExpertExtractionDao;
import com.jd.supplier.model.ExpertExtraction;

@Component("expertExtractionDao")
public class ExpertExtractionDaoImpl extends MyBatisDaoImpl<ExpertExtraction, Integer> 
				implements ExpertExtractionDao{

	private static final String NAMESPACE = ExpertExtraction.class.getName();
	
	@Override
	public List<ExpertExtraction> findByExtrId(long extractionId) {
		return getSqlSession().selectList(NAMESPACE + ".findByExtrId", extractionId);
	}
	
	@Override
	public List<ExpertExtraction> find(long extractionId, long expertId) {
		Map map = new HashMap();
		map.put("extractionId", extractionId);
		map.put("expertId", expertId);
		return getSqlSession().selectList(NAMESPACE + ".find", map);
	}

	@Override
	public void save(ExpertExtraction ee) {
		if(ee.getId() == null)
			getSqlSession().insert(NAMESPACE + ".insert", ee);
		else
			getSqlSession().update(NAMESPACE + ".update", ee);
	}

	@Override
	public ExpertExtraction findById(int id) {
		return getSqlSession().selectOne(NAMESPACE + ".findById", id);
	}

	@Override
	public ExpertExtraction findUnique(long reqId, long expertId) {
		Map map = new HashMap();
		map.put("reqId", reqId);
		map.put("expertId", expertId);
		return getSqlSession().selectOne(NAMESPACE + ".findUnique", map);
	}

}

package com.jd.supplier.dao;

import java.util.List;

import com.jd.supplier.model.ExpertExtraction;

public interface ExpertExtractionDao {

	List<ExpertExtraction> findByExtrId(long extractionId);

	void save(ExpertExtraction ee);

	ExpertExtraction findById(int id);

	ExpertExtraction findUnique(long reqId, long expertId);

	List<ExpertExtraction> find(long extractionId, long expertId);

}

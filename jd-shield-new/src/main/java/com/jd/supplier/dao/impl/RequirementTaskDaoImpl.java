package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.RequirementTaskDao;
import com.jd.supplier.model.RequirementTask;

@Component("requirementTaskDao")
public class RequirementTaskDaoImpl extends MyBatisDaoImpl<RequirementTask, Long> implements RequirementTaskDao{

}

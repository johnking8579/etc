package com.jd.supplier.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.official.core.dao.Page;
import com.jd.official.core.dao.sqladpter.SqlAdapter;
import com.jd.supplier.dao.AnnualRequirementAuditDao;
import com.jd.supplier.model.AnnualRequirementAudit;

@Component("annualRequirementAuditDao")
public class AnnualRequirementAuditDaoImpl extends MyBatisDaoImpl<AnnualRequirementAudit, Long>
		implements AnnualRequirementAuditDao {

}

package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Other;

public interface IOtherDao extends BaseDao<Other, Long>{

}

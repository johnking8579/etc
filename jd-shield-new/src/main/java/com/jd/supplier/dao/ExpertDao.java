package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Expert;
import com.jd.supplier.model.Extraction;

public interface ExpertDao {

	public Page<Expert> findAll(int offset, int pageSize);

	public List<Expert> findLikeRealName(String name);

	public Page<Expert> find(Map<String, Object> condition, int offset, int pageSize);

	public Long save(Expert expert);

	public Expert findById(long id);
	
	public List<Expert> findByIds(long[] ids);

	public Page<Expert> extract(Extraction extraction, String groupId, List<Long> excludedUserIds,
			int offset, int pageSize);

	public Expert findByUserName(String userName);

	
}

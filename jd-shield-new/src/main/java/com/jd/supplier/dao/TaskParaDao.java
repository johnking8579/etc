package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.TaskPara;
/**
 * 任务参数数据层接口
 * @author wangchanghui
 *
 */
public interface TaskParaDao extends BaseDao<TaskPara,Long>{
	/**
	 * 查找参数集合
	 * @param map
	 * @return
	 */
	List<TaskPara> findParaList(Map<String,Object> map);
	
	/**
	 * 查找需要绑定的集合
	 * @param map
	 * @return
	 */
	List<TaskPara> findToBeBindedList(Map<String,Object> map);
}
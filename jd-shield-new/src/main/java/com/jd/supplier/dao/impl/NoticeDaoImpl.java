package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.NoticeDao;
import com.jd.supplier.model.Notice;

@Component("noticeDao")
public class NoticeDaoImpl extends MyBatisDaoImpl<Notice, Long> implements NoticeDao{

	@Override
	public Notice getByNodeId(Long nodeId) {
		return this.getSqlSession().selectOne("com.jd.supplier.model.Notice.getByNodeId", nodeId);
	}

}

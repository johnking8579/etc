package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.TaskNode;

/**
 * 任务节点数据层接口
 * @author wangchanghui
 *
 */
public interface TaskNodeDao extends BaseDao<TaskNode,Long>{
	/**
	 * 查找节点集合
	 * @param map
	 * @return
	 */
	List<TaskNode> findNodeList(Map<String, Object> map);
	
	/**
	 * 查找初始节点集合
	 * @param map
	 * @return
	 */
	TaskNode fintInitNode(Map<String,Object> map);
	
	/**
	 * 查找未完成父节点数量
	 * @param map
	 * @return
	 */
	Integer findUncompletedParentNodeCount(Map<String,Object> map);
	
	/**
	 * 检查是否是父节点
	 * @param map
	 * @return
	 */
	Integer checkIsParent(Map<String,Object> map);
	
	/**
	 * 通过父节点查询主键
	 * @param map
	 * @return
	 */
	Long getPkByParentId(Map<String,Object> map);
	
	/**
	 * 通过父节点查找对象
	 * @param id
	 * @return
	 */
	TaskNode getByParentId(Long id);
	
	/**
	 * 通过主键查找父节点
	 * @param id
	 * @return
	 */
	TaskNode getParent(Long id);
	
	/**
	 * 通过机构号查找节点集合
	 * @param map
	 * @return
	 */
	List<TaskNode> findNodeListByOrgCode(Map<String,Object> map);
	
	/**
	 * 查找id集合
	 * @param map
	 * @return
	 */
	List<Long> findIdList(Map<String,Object> map);
	
	/**
	 * 通过节点状态查找节点数量
	 * @param map
	 * @return
	 */
	Integer findNodeCountByStatus(Map<String,Object> map);
	
	/**
	 * 通过任务节点查找节点数量
	 * @param taskId
	 * @return
	 */
	Integer findNodeCountByTaskId(Long taskId);
}
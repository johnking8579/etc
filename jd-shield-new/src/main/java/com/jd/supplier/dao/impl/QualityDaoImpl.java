package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.IQualityDao;
import com.jd.supplier.model.Quality;
@Component("qualityDao")
public class QualityDaoImpl extends MyBatisDaoImpl<Quality, Long> implements IQualityDao{

}

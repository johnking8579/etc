package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.Notice;

public interface NoticeDao extends BaseDao<Notice, Long> {

	Notice getByNodeId(Long nodeId);
}

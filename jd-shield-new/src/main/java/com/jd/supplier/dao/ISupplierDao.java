package com.jd.supplier.dao;

import com.jd.official.core.dao.BaseDao;
import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Supplier;

public interface ISupplierDao extends BaseDao<Supplier, Long>{
}
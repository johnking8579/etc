package com.jd.supplier.dao;

import com.jd.official.core.dao.Page;
import com.jd.supplier.model.Extraction;

public interface ExtractionDao {

	Page<Extraction> find(String projectName, String creator, String startTime,
			String endTime, int offset, int pageSize);

	Extraction findById(long id);

	void save(Extraction extraction);

	Extraction findByReqId(long reqId);

}

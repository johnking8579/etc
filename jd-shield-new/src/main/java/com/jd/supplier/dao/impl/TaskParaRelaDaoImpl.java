package com.jd.supplier.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.TaskParaRelaDao;
import com.jd.supplier.model.TaskParaRela;
/**
 * 任务参数关系数据层实现
 * @author wangchanghui
 *
 */
@Repository("taskParaRelaDao")
public class TaskParaRelaDaoImpl extends MyBatisDaoImpl<TaskParaRela, Long>
		implements TaskParaRelaDao {


	/**
	 * 查询参数关系集合
	 */
	@Override
	public List<TaskParaRela> findList(Map<String, Object> map) {
		return this.getSqlSession().selectList("com.jd.supplier.model.TaskParaRela.findByMap", map);
	}


}

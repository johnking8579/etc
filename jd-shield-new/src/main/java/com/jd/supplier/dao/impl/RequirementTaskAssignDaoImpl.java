package com.jd.supplier.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.official.core.dao.MyBatisDaoImpl;
import com.jd.supplier.dao.RequirementTaskAssignDao;
import com.jd.supplier.model.RequirementTaskAssign;

@Component("requirementTaskAssignDao")
public class RequirementTaskAssignDaoImpl extends MyBatisDaoImpl<RequirementTaskAssign, Long> implements RequirementTaskAssignDao{

}

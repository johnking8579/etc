package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.supplier.model.TaskParaRela;
/**
 * 任务参数关系数据层接口
 * @author wangchanghui
 *
 */
public interface TaskParaRelaDao extends BaseDao<TaskParaRela, Long>{
	List<TaskParaRela> findList(Map<String,Object> map);
}
package com.jd.supplier.dao;

import java.util.List;
import java.util.Map;

import com.jd.official.core.dao.BaseDao;
import com.jd.official.modules.system.model.User;
import com.jd.supplier.model.TaskNodeAssignment;

/**
 * 任务指派数据层接口
 * @author wangchanghui
 *
 */
public interface TaskNodeAssignmentDao extends BaseDao<TaskNodeAssignment, Long>{
	
	/**
	 * 查找任务指派用户集合
	 * @param map
	 * @return
	 */
	List<User> findTaskAssignUsers(Map<String,Object> map);
	
	/**
	 * 获取认领数量
	 * @param map
	 * @return
	 */
	Integer getClaimCount(Map<String,Object> map);
	
	/**
	 * 获取分配数量
	 * @param map
	 * @return
	 */
	Integer getAssignedCount(Map<String,Object> map);
	
	/**
	 * 根据节点id获取指派对象
	 * @param nodeId
	 * @return
	 */
	TaskNodeAssignment getAssignmentByNodeId(Long nodeId);
}
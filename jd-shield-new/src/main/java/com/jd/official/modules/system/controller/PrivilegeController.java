/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import redis.clients.jedis.Jedis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.cachecloud.driver.jedis.JedisOperation;
import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.dao.Page;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.tree.TreeNode;
import com.jd.official.modules.system.model.Operation;
import com.jd.official.modules.system.model.Privilege;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.service.OperationService;
import com.jd.official.modules.system.service.PrivilegeService;
import com.jd.official.modules.system.service.ResourceService;
import com.jd.official.modules.system.service.UserService;

/**
 * 资源功能 controller 该类提供资源功能的查询,添加,修改,删除操作
 * 
 * @author liub
 * 
 */
@Controller
@RequestMapping(value = "/system")
public class PrivilegeController {
	
	private static Logger log = Logger.getLogger(PrivilegeController.class);

	@Autowired
	private PrivilegeService privilegeService;
	@Autowired
	private ResourceService resourceService;
	@Autowired
	private OperationService operationService;
	private static List<Operation> operations = null;
	
	@Autowired
    private UserService userService;
	 
	@Autowired
	private ShardedXCommands redisClient;

	/**
	 * 功能权限
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/privilege_index", method = RequestMethod.GET)
	public ModelAndView list() throws Exception {
		ModelAndView mav = new ModelAndView("system/privilege_index");
		
		List<Resource> container = new ArrayList<Resource>();
		container = this.recurisiveFetchChildren(container, null);
		
		List<TreeNode> nodes = new ArrayList<TreeNode>();
		TreeNode root = new TreeNode();
		root.setId(null);
		root.setName("京东-办公自动化");
		root.setIsParent(Boolean.TRUE);
		root.setIconClose("../static/zTree/css/zTreeStyle/img/diy/1_close.png");
		root.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/1_open.png");
		nodes.add(root);
		
		for(Resource r : container)	{
			nodes.add(this.toTreeNode(r));
		}

		mav.addObject("treeNode", new Gson().toJson(nodes));
		return mav;
	}
	
	/**
	 * 递归生成resource树
	 * @param container
	 * @param pid
	 * @return
	 */
	private List<Resource> recurisiveFetchChildren(List<Resource> container, Long pid)	{
		List<Resource> list =  resourceService.findByParentId(pid);
		if(!list.isEmpty())	{
			for(Resource rsc : list)	{
				container.add(rsc);
				recurisiveFetchChildren(container, rsc.getId());
			}
		}
		return container;
	}
	
	/**
	 * resource转换成treeNode
	 * @param r
	 * @return
	 */
	private TreeNode toTreeNode(Resource r) {
		TreeNode node = new TreeNode();
		node.setId(r.getId() + "");
		if(null == r.getParentId()){
			node.setpId(null);
		}else{
			node.setpId(r.getParentId() + "");
		}
		node.setName(r.getResourceName());
		node.setIsParent(r.getChildrenNum() == 0 ? false : true);
		node.setIconClose("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
		node.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
		
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("id", r.getId());
		props.put("operations", operations);
		props.put("url", r.getResourceUrl());
		props.put("code", r.getResourceCode());
		node.setProps(props);
		
		return node;
	}

	/**
	 * 保存操作
	 * 
	 * @param
	 * @return
	 * @throws IOException 
	 * @throws JDOAException
	 */
	@RecordLog(operationType=OperationTypeValue.add_update, entityName="Privilege")
	@RequestMapping(value = "/privilege_save", method = RequestMethod.POST)
	@ResponseBody
	private String save(Privilege entity, HttpServletRequest request) throws BusinessException, IOException {
		Long id = 0l;
		String operId = request.getParameter("operationId");
		if(operId != null)	{
			entity.setOperation(operationService.get(Long.parseLong(operId)));
		}
		
		if (entity.getId() != null) {
			privilegeService.update(entity);
		} else {
			id = privilegeService.insert(entity);
			request.setAttribute("entityId", id);
		}
		//redis删除缓存
		removeCache("*");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("operator", true);
		map.put("message", "保存成功");
		return JSONObject.fromObject(map).toString();
	}
	
	
	private synchronized void removeCache(final String key){
		final Set<byte[]> bytesKey = new HashSet<byte[]>();
		//不删除数据字典缓存
		final String[] safeKeys = {"JD-OFFICIAL/DictData/findDictDataListByDictCode",
				"JD-OFFICIAL/DictData/findDictDataListByPid"};
		redisClient.execute(new JedisOperation<Void>() {
            @Override
            public Void call(Jedis jedis) throws Throwable {
                bytesKey.addAll(jedis.keys(key.getBytes("utf-8")));
                return null;
            }
        });
        for (byte[] s : bytesKey) {
        	try {
        		String allKeys = (String) RedisCacheSerializer.deSerialize(s);
        		if(allKeys.indexOf(safeKeys[0]) == -1
        				&&
        		   allKeys.indexOf(safeKeys[1]) == -1)
        			redisClient.del(s);
        	} catch (IOException e) {
        		e.printStackTrace();
        	} catch (ClassNotFoundException e) {
        		e.printStackTrace();
        	}
        }
        
	}

	/**
	 * 根据id 获取　Privilege　对象.
	 * 
	 * @param id
	 * @return
	 * @throws JDOAException
	 */
	@RequestMapping(value = "/privilege_edit", method = RequestMethod.GET)
	private ModelAndView get(Long id, String resourceCode, String resourceName)
			throws BusinessException {
		ModelAndView mav = new ModelAndView("system/privilege_edit");
		Privilege privilege = privilegeService.get(id);
		if (privilege != null) {
			Operation operation = operationService.get(privilege.getOperation().getId());
			if (operation != null) {
				mav.addObject("selectOperation",
						getOperationSelectTagHtml(operation.getId()));
				mav.addObject("operationCode", operation.getOperationCode());
				mav.addObject("privilegeCode", privilege.getPrivilegeCode());
				mav.addObject("privilegeName", privilege.getPrivilegeName());
				mav.addObject("url", privilege.getUrl() == null ? "" : privilege.getUrl());
				mav.addObject("id", id);
				mav.addObject("operationId", privilege.getOperation().getId());
				mav.addObject("resourceId", privilege.getResourceId());
			}
		}
		mav.addObject("resourceCode", resourceCode);
		mav.addObject("resourceName", resourceName);
		return mav;
	}

	@RecordLog(operationType=OperationTypeValue.delete, entityName="Privilege")
	@RequestMapping(value = "/privilege_remove", method = RequestMethod.POST)
	@ResponseBody
	private String remove(Privilege entity) throws BusinessException, IOException {
		Privilege privilege = privilegeService.get(entity.getId());
		if (privilege != null) {
			privilegeService.delete(privilege, true);
		}
		//redis删除缓存
		removeCache("*");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("operator", true);
		map.put("message", "保存成功");
		return JSONObject.fromObject(map).toString();
	}

	/**
	 * 打开 添加operation界面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/privilege_add", method = RequestMethod.GET)
	public ModelAndView addOperation(String resourceId, String resourceCode,
			String resourceName) throws Exception {
		ModelAndView mav = new ModelAndView("system/privilege_add");
		mav.addObject("selectOperation", this.getOperationSelectTagHtml());
		mav.addObject("resourceId", resourceId);
		mav.addObject("resourceCode", resourceCode);
		mav.addObject("resourceName", resourceName);
		mav.addObject("url", "");

		return mav;
	}

	/**
	 * 获取针对资源ID的功能有权限数据json
	 * 
	 * @param request
	 * @param resourceId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getOperationDataJson")
	@ResponseBody
	public String findPriv(String rscId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("resourceId", rscId);
		
		Page<Privilege> page = new Page<Privilege>();
		page.setPageNo(1);
		page.setPageSize(Integer.MAX_VALUE);
		
		page = privilegeService.find(page, "findByRscidAndOperid", map);
		
		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonElement je = new JsonParser().parse(gson.toJson(page.getResult()));
		JsonObject json = new JsonObject();
		json.add("aaData", je);
		return json.toString();
	}
	
	
	private String getOperationSelectTagHtml() {
		return getOperationSelectTagHtml(0L);
	}

	/**
	 * 获取 选择select html标签片段
	 * 
	 * @return
	 */
	private String getOperationSelectTagHtml(Long selectedValue) {
		synchronized(Object.class){
			if (operations == null) {
				operations = operationService.find(new Operation());
			}
		}
		StringBuffer html = new StringBuffer();
		if (operations != null && operations.size() > 0) {
			html.append("<select id=\"operationName\" class=\"\" onchange=\"selectChange(this)\">\n");
			html.append("<option value=\"\">").append("请选择")
					.append("</option>\n");
			for (int i = 0; i < operations.size(); i++) {
				Operation operation = operations.get(i);
				if (operation != null) {
					if (selectedValue.equals(operation.getId())) {
						html.append("<option selected operationCode=\"")
								.append(operation.getOperationCode())
								.append("\" value=\"")
								.append(operation.getId()).append("\">")
								.append(operation.getOperationName())
								.append("</option>\n");
					} else {
						html.append("<option operationCode=\"")
								.append(operation.getOperationCode())
								.append("\" value=\"")
								.append(operation.getId()).append("\">")
								.append(operation.getOperationName())
								.append("</option>\n");
					}
				}
			}
			html.append("</select>\n");
		}
		return html.toString();
	}
	
}

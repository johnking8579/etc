/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Position;
import com.jd.official.modules.system.service.PositionService;
import com.jd.official.modules.system.service.UserAuthorizationService;

/**
 * @author qiuyang
 *
 */
@Controller
@RequestMapping(value = "/system")
public class PositionController {
	private static final Logger logger = Logger
			.getLogger(PositionController.class);

	@Autowired
	private PositionService positionService;
	
	@Autowired
	private UserAuthorizationService userAuthorizationService ;
	
	@RequestMapping(value = "/position_index", method = RequestMethod.GET)
	public String index() throws Exception {
	     return "system/position_index";
	}
	
	@RequestMapping(value = "/position_list", method = RequestMethod.GET)
	public ModelAndView dictDataList()
            throws Exception {
        ModelAndView mav = new ModelAndView("system/position_list");
        return mav;
    }
	
	@RequestMapping(value = "/findPositionPage")
    @ResponseBody
	public Object findPositionPage(HttpServletRequest request,@RequestParam String  orgid,@RequestParam String positionName) throws Exception{
		//创建pageWrapper
		PageWrapper<Position> pageWrapper=new PageWrapper<Position>(request);
		if(StringUtils.isNotEmpty(orgid) && StringUtils.isEmpty(positionName)){
			List<Long> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(orgid);
			pageWrapper.addSearch("allOrgIds",allOrgIds);
		}
		if(StringUtils.isNotEmpty(positionName)){
			pageWrapper.addSearch("positionName",positionName);
		}
		
		positionService.find(pageWrapper.getPageBean(),"findByPage",pageWrapper.getConditionsMap());
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	    logger.debug("result:"+json);
		return json;
	}
	
	@RequestMapping(value = "/position_view",method=RequestMethod.GET)
	public String viewPosition(Long positionId, Model model) {
		Position position = positionService.get(positionId);
		model.addAttribute("position", position);
    	return "system/position_view" ;
	}
}

package com.jd.official.modules.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.common.springmvc.interceptor.DotnetTicketContextInterceptor;
import com.jd.common.springmvc.interceptor.LoginContextInterceptor;
import com.jd.common.web.cookie.CookieUtils;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.shiro.ShiroUser;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.modules.system.model.Organization;
import com.jd.official.modules.system.model.Privilege;
import com.jd.official.modules.system.model.Resource;
import com.jd.official.modules.system.model.Role;
import com.jd.official.modules.system.model.RolePrivilege;
import com.jd.official.modules.system.model.User;
import com.jd.official.modules.system.service.OrganizationService;
import com.jd.official.modules.system.service.PrivilegeService;
import com.jd.official.modules.system.service.ResourceService;
import com.jd.official.modules.system.service.RolePrivilegeService;
import com.jd.official.modules.system.service.RoleService;
import com.jd.official.modules.system.service.UserRoleService;
import com.jd.official.modules.system.service.UserService;

/**
 * 
 * @author: zhouhq
 */
@Controller
@RequestMapping("/")
public class LoginController {
	private static final Logger logger = Logger
			.getLogger(LoginController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RolePrivilegeService rolePrivilegeService;

	@Autowired
	private PrivilegeService privilegeService;

	@Autowired
	private ResourceService resourceService;
	
	@Autowired
	private DotnetTicketContextInterceptor dotnetTicketContextInterceptor;
	@Autowired
	private LoginContextInterceptor loginContextInterceptor;
	
	@Autowired
    private ShardedXCommands redisClient;

	private static List<RolePrivilege> rolePrivileges = null;
	private static List<Privilege> privileges = null;

	public LoginController() {

	}

	/* @RequestMapping(value = "/", method = RequestMethod.GET) */
	public String index(
			@RequestParam(value = "locale", required = false) Locale locale,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return "login";
	}

	@RequestMapping(value = "/login")
	public RedirectView login(String userName, String password,
			String rememberMe) {
		try {
			if (userName != null && password != null) {
				UsernamePasswordToken token = new UsernamePasswordToken(
						userName, password);
				if (rememberMe != null && rememberMe.equals("1"))
					token.setRememberMe(true);
				SecurityUtils.getSubject().login(token);

				User user = userService
						.getByUserName(((ShiroUser) SecurityUtils.getSubject()
								.getPrincipal()).getLoginName());

//				Subject currentUser = SecurityUtils.getSubject();

				// 把用户编码放到缓存
				if (user.getId() != null) {
					/*currentUser.getSession().setAttribute("SESSION_USERCODE",
							user.getOrganizationName());*/
				}

				/*
				 * Cookie cookie = new
				 * Cookie("USERID",CryptoUtils.desEncryptToBase64
				 * (entity.getUsername()+"&"+new Date().toString(),KEY));
				 * ServletActionContext.getResponse().setHeader("P3P",
				 * "CP=CAO PSA OUR");
				 * ServletActionContext.getResponse().addCookie(cookie);
				 */
				if (rolePrivileges == null) {
					rolePrivileges = rolePrivilegeService.find(null);
				}
				if (privileges == null) {
					privileges = privilegeService.find(null);
				}
				return new RedirectView("index");
			}
		} catch (UnknownAccountException ex) {
			throw new BusinessException("账号不存在。", ex);
		} catch (IncorrectCredentialsException ex) {
			// password provided did not match password found in database
			// for the username provided
			throw new BusinessException("密码不正确。", ex);
			// ServletActionContext.getRequest().setAttribute("error","2");
		} catch (Exception ex) {
			throw new BusinessException("用户名密码不正确，请重新输入。", ex);
		}
		return null;
	}
	
	@RequestMapping(value = "/")
	public RedirectView index()  throws IOException {
		redisClient.del(RedisCacheSerializer.serialize("JD-OFFICIAL/AuthorizedResources/"+userService.getUserIdByUserName(ComUtils.getLoginNamePin())));
		redisClient.del(RedisCacheSerializer.serialize("JD-OFFICIAL/AuthorizedPrivilegeUrls/"+userService.getUserIdByUserName(ComUtils.getLoginNamePin())));
		redisClient.del(RedisCacheSerializer.serialize("JD-OFFICIAL/AuthorizedPrivilegeCodes/"+userService.getUserIdByUserName(ComUtils.getLoginNamePin())));
		
		RedirectView rv = new RedirectView("index");;
		User user = (User) userService.getByUserName(ComUtils
				.getLoginNamePin());
		if(user != null){
			boolean isAdmin = true;
//			List<Role> roles = roleService.findByUserId(user.getId());
//			for(Role role : roles){
//				if(role.getRoleName().contains("管理员")){
//					isAdmin = true;
//					break;
//				}
//			}
			
			if(isAdmin){
				rv = new RedirectView("/admin");
			}
			else{
				rv = new RedirectView("/app");
			}
		}
		return rv;
	}

	@RequestMapping(value = "/admin")
	public ModelAndView admin_index()  throws IOException {
		ModelAndView mav = new ModelAndView("index");;
		User user = (User) userService.getByUserName(ComUtils
				.getLoginNamePin());
		if(user != null){
			Organization organization = organizationService.get(user
					.getOrganizationId());
			mav.addObject("displayName", user.getRealName());
			if(organization!=null)
				mav.addObject("organizationName",
						organization.getOrganizationFullname());
		}
		return mav;
	}
	
	@RequestMapping(value = "/app")
	public ModelAndView app_index()  throws IOException {
		ModelAndView mav = new ModelAndView("app/app_index");;
		User user = (User) userService.getByUserName(ComUtils
				.getLoginNamePin());
		if(user != null){
			Organization organization = organizationService.get(user
					.getOrganizationId());
			mav.addObject("displayName", user.getRealName());
			if(organization!=null)
				mav.addObject("organizationName",
						organization.getOrganizationFullname());
		}
		return mav;
	}

	@RequestMapping(value = "/header")
	public ModelAndView header() {
		
		ModelAndView mav = new ModelAndView("layout/header");
		User user = (User) userService.getByUserName(ComUtils
				.getLoginNamePin());
		if(user != null){
			Organization organization = organizationService.get(user
					.getOrganizationId());
			mav.addObject("displayName", ComUtils.getLoginName());
			if(organization!=null)
				mav.addObject("organizationName",
						organization.getOrganizationFullname());
		}
		return mav;
	}

	@RequestMapping(value = "/left")
	public ModelAndView left(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("layout/left");
		List<Resource> resourcesTree = resourceService.findAuthorizedResources(userService.getUserIdByUserName(ComUtils
				.getLoginNamePin()));
		String html = getMenuHtml(resourcesTree);
		mav.addObject("menu", html);
		return mav;
	}
	
	@RequestMapping(value = "/main")
	public ModelAndView main() {
		ModelAndView mav = new ModelAndView("main");
		return mav;
	}
	
	@RequestMapping(value = "/logout")
	public RedirectView logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		/**
		* clear Cookie
		*/
		CookieUtils cookieUtils = ((DotnetTicketContextInterceptor)dotnetTicketContextInterceptor).getCookieUtils();
		cookieUtils.deleteCookie(response, ((LoginContextInterceptor)loginContextInterceptor).getLoginCookieKey()); //清除登录cookie
		cookieUtils.deleteCookie(response, ((DotnetTicketContextInterceptor)dotnetTicketContextInterceptor).getDotnetAuthCookieName()); //清楚dotnet的cookie
		
		RedirectView rv = new RedirectView();
		rv.setUrl("/");
		return rv;
	}

    /**
     * 拼凑menu html片段
     *修改menu的展示方式
     * @param resources
     * @return
     */
    private String getMenuHtml(List<Resource> resources){
        StringBuffer html = new StringBuffer();
        int num =0;
        for (int i = 0; i < resources.size(); i++) {
            Resource systemResource = (Resource) resources.toArray()[i];
          //  String URL = systemResource.getResourceUrl();
            num =i;
            if (systemResource != null) {
                if (systemResource.getParent() == null) {
                    html.append("<a href=\"").append("#menu"+num)
                            .append("\"  class=\"nav-header menu-first collapsed\" data-toggle=\"collapse\">");
                    html.append("<i class=\"icon-book icon-large\"></i> ")
                            .append(systemResource.getResourceName())
                            .append("</a>");
                    if (systemResource.getChildrenNum() > 0) {

                        if (((Resource) resources.toArray()[i + 1])
                                .getParent() != null) {
                            List<Resource> childResourceList = new ArrayList<Resource>();
                            for (int j = ++i; j < resources.size(); j++, i++) {
                                if (((Resource) resources.toArray()[j])
                                        .getParent() != null)
                                    childResourceList
                                            .add((Resource) resources
                                                    .toArray()[j]);
                                else {
                                    i--;
                                    break;
                                }
                            }
                            if (childResourceList != null
                                    && childResourceList.size() > 0) {
                                html.append(" <ul style=\"height: 0;\" id=\"").append("menu"+num)
                                        .append("\" class=\"nav nav-list menu-second collapse\">");
                                for (int k = 0; k < childResourceList.size(); k++) {
                                    Resource childResource = childResourceList
                                            .get(k);
                                    if (childResource != null) {
                                        html.append("<li><a href='#' onclick=\"")
												.append("loadUrl('/"+childResource.getResourceUrl()+"')")
												.append("\"")
                                                .append("\"><i class='icon-pencil'></i>")
                                                .append(childResource
                                                        .getResourceName())
                                                .append("</a></li>\n");
                                    }
                                }
                                html.append("</ul>\n");
                            }
                        }
                    }
                }
            }
        }
//        html.append(getJavaScript());
        return html.toString();
    }

	/**
	 * 拼凑menu html片段
	 * 
	 * @param resources
	 * @return
	 */
	private String getMenuHtml1(List<Resource> resources) {
		StringBuffer html = new StringBuffer();
		for (int i = 0; i < resources.size(); i++) {
			Resource systemResource = (Resource) resources.toArray()[i];
			if (systemResource != null) {
				String URL = systemResource.getResourceUrl();
				if (systemResource.getParent() == null) {
					html.append("<div class='lev1'>");
					if (systemResource.getChildrenNum() > 0) {
						html.append("<a class='tit'><i class='ico-network'></i><span>")
								.append(systemResource.getResourceName())
								.append("</span></a>\n");
						if (((Resource) resources.toArray()[i + 1])
								.getParent() != null) {
							List<Resource> childResourceList = new ArrayList<Resource>();
							for (int j = ++i; j < resources.size(); j++, i++) {
								if (((Resource) resources.toArray()[j])
										.getParent() != null)
									childResourceList
											.add((Resource) resources
													.toArray()[j]);
								else {
									i--;
									break;
								}
							}
							if (childResourceList != null
									&& childResourceList.size() > 0) {
								html.append("<div class='lev2'>");
								for (int k = 0; k < childResourceList.size(); k++) {
									Resource childResource = childResourceList
											.get(k);
									if (childResource != null) {
										html.append("<a href=\"")
												.append("/")
												.append(childResource
														.getResourceUrl())
												.append("\" target=\"mainFrame\"><i class='ico-network'></i><span>")
												.append(childResource
														.getResourceName())
												.append("</span></a>\n");
									}
								}
								html.append("</div>\n");
							}
						}
					} else
						html.append("<a class='tit' href=\"").append(URL)
								.append("\" target=\"mainFrame\"><span>")
								.append(systemResource.getResourceName())
								.append("</span></a>\n");
				}
			}
			html.append("</div>\n");
		}
		html.append(getJavaScript());
		return html.toString();
	}

	/**
	 * 控制首面的遮罩
	 * 
	 * @return
	 */
	private String getJavaScript() {
		StringBuffer script = new StringBuffer();
		script.append("<script type=\"text/javascript\">\n");
		script.append("document.getElementById('loading').style.display = 'none';\n");
		script.append("document.getElementById('mainPanel').style.display = '';\n");
		script.append("</script>");
		return script.toString();
	}
}

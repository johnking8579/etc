package com.jd.official.modules.system.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.official.core.redis.redis.RedisCacheSerializer;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.SpringContextUtils;
/**
 * 管理控制 Controller
 * @author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class RedisController {
	private static final Logger logger = Logger.getLogger(RedisController.class);
	
	@RequestMapping(value = "/redis", method = RequestMethod.GET)
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView();
    	mv.setViewName("system/redis");
		return mv;
	}
	/**
	 * redis 管理
	 * @param type
	 * @param value
	 */
	@RequestMapping(value = "/redis/get", method = RequestMethod.POST)
	@ResponseBody
	public String consoleRedisGet(String key){
		ShardedXCommands redisCommands = SpringContextUtils.getBean("redisClient");
		byte[] buf;
	    Object returnValue = null;
	    try {
	      buf = redisCommands.get(RedisCacheSerializer.serialize(key));//根据key获取redis缓存值
	      if (buf == null) {
	    	  return "没有查找到key:[" + key + "]";
	      }
	      returnValue = RedisCacheSerializer.deSerialize(buf);
	   } catch (Exception e) {
		   return "调用缓存服务器get操作出错";
	   }
	    return JsonUtils.toJsonByGoogle(returnValue);
	}
	/**
	 * redis 管理
	 * @param type
	 * @param value
	 */
	@RequestMapping(value = "/redis/delete", method = RequestMethod.POST)
	@ResponseBody
	public String consoleRedisDelete(String key){
		ShardedXCommands redisCommands = SpringContextUtils.getBean("redisClient");
		byte[] buf;
	    Object returnValue = null;
	    try {
	      redisCommands.del(RedisCacheSerializer.serialize(key));//根据key获取redis缓存值
	   } catch (Exception e) {
		   return "调用缓存服务器get操作出错";
	   }
	   try {
		   buf = redisCommands.get(RedisCacheSerializer.serialize(key));//根据key获取redis缓存值
		   if (buf == null) {
			   return "指定KEY[" + key + "]删除成功";
		   } else {
			   return "指定KEY[" + key + "]删除失败";
		   }
		} catch (Exception e) {
			   logger.error("调用缓存服务器get操作出错:", e);
		 }
	   return "";
	}
}

/**
 * 
 */
package com.jd.official.modules.system.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.springmvc.interceptor.RecordLog;
import com.jd.common.springmvc.interceptor.RecordLog.OperationTypeValue;
import com.jd.official.core.exception.BusinessException;
import com.jd.official.core.utils.ComUtils;
import com.jd.official.core.utils.JsonUtils;
import com.jd.official.core.utils.PageWrapper;
import com.jd.official.modules.system.model.Operation;
import com.jd.official.modules.system.service.OperationService;

/**
 *@Description: 系统功能 Controller
 *@author liub
 *
 */

@Controller
@RequestMapping(value="/system")
public class OperationController {
	
	 private static final Logger logger = Logger.getLogger(OperationController.class);
	
	 @Autowired
	 private OperationService operationService;
	 
	 @RequestMapping(value = "/operation_index", method = RequestMethod.GET)
	 public String index() throws Exception {
	     return "system/operation_index";
	 }
	 @RequestMapping(value = "/operation_list")
	 @ResponseBody
	 public Object list(HttpServletRequest request,@RequestParam String  operationName,@RequestParam String  operationCode) throws Exception {
		 PageWrapper<Operation> pageWrapper = new PageWrapper<Operation>(request);
         if (null != operationName && !operationName.equals("")) {
             pageWrapper.addSearch("operationName", operationName);
         }
         if (null != operationCode && !operationCode.equals("")) {
             pageWrapper.addSearch("operationCode", operationCode);
         }
		 operationService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);
	     return json;
	 }
	 /**
	  * add view
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/operation_add", method = RequestMethod.GET)
	 public String list() throws Exception {
	     return "system/operation_add";
	 }

    /**
     *
     * @param entity
     * @return
     */
	 @RecordLog(operationType=OperationTypeValue.add_update, entityName="Operation")
	 @RequestMapping(value="/operation_save" , method = RequestMethod.POST)
	 @ResponseBody
	 public String save(Operation entity,HttpServletRequest request){
		Long id = 0l;
	    Map<String,Object> map = new HashMap<String,Object>();
	 	if(entity.getId() != null){
	 		Operation operation = operationService.get(entity.getId());
	 		try {
				ComUtils.copyProperties(entity, operation);
			} catch (Exception e) {
				throw new BusinessException("对象模板复制失败：" + e.getMessage(),e);
			}
	 		operationService.update(operation);
	 	} else {
	 		id = operationService.insert(entity);
	 		request.setAttribute("entityId", id);
	 	}
	 	map.put("operator", true);
	 	map.put("message", "添加成功");
	 	return JSONObject.fromObject(map).toString();
	 }
	 /**
	  * delete 
	  * @param id entity id
	  * @return
	  */
	 @RecordLog(operationType=OperationTypeValue.delete, entityName="Operation")
	 @RequestMapping(value="/operation_delete" , method = RequestMethod.POST)
	 @ResponseBody
	 public String delete(Operation entity){
		 Map<String,Object> map = new HashMap<String,Object>();
		 Operation operation = operationService.get(entity.getId());
		 if(operation != null){
			 operationService.delete(operation,true);
		 }
		 map.put("operator", true);
		 map.put("message", "添加成功");
		 return JSONObject.fromObject(map).toString();
	 }
	 
	 /**
	  * edit 
	  * @param id entity id
	  * @return
	  */
	 @RequestMapping(value="/operation_update" , method = RequestMethod.GET)
	 public ModelAndView get(Operation entity){
		 ModelAndView mv = new ModelAndView();
		 Operation operation = operationService.get(entity.getId());
		 mv.setViewName("system/operation_edit");
		 mv.addObject("operation", operation);
		 return mv;
	 }
}

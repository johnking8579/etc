package com.jd.common.constants;
/**
 * 邮箱常量类
 * @author wangchanghui
 *
 */
public class EmailConstant {
	/**
	 * 任务单
	 * @author wangchanghui
	 *
	 */
	public static class Task{
		public static final String SIGN_NAME = "全国招标中心";
		
		public static final String CHECK = "请登录系统查看。";
		
		public static final String AUTO_SEND = "[此邮件由系统自动发出，请勿直接回复]";
		
		public static final String HELLO = "，您好。";
		
		public static final String ASSIGNMENT = "招标中心有新的任务提派给您，";
		
		public static final String REJECT = "驳回，";
		
		public static final String REJECT_ASSIGNMENT = "您在招标中心分配的任务已经被";
	}
}

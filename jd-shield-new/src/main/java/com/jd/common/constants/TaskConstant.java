package com.jd.common.constants;
/**
 * 任务单常量类
 * @author wangchanghui
 *
 */
public class TaskConstant {
	
	/**
	 * 符号
	 * @author wangchanghui
	 *
	 */
	public static class Symbol{
		
		public static final String WRAP = "\r\n";
		
		public static final String COLON = ":";
		
		public static final String COMMA = ",";
	}
	
	/**
	 * 名词
	 * @author wangchanghui
	 *
	 */
	public static class Noun{
		public static final String TASK = "任务";
		
		public static final String PROJECT = "项目";
		
		public static final String STATUS = "状态";
		
		public static final String REASON = "原因";
	}
	
	/**
	 * 任务初始化
	 * @author wangchanghui
	 *
	 */
	public static class TaskInit{
		
		public static final int TEMPLATE_DEFAULT = 1; 
	}
	
	/**
	 * 消息
	 * @author wangchanghui
	 *
	 */
	public static class Tips{
		
		public static final String REJECT = "驳回";
		
		public static final String NOTICE = "通知";
		
		public static final String ASSIGN = "指派";
		
		public static final String WAIT4CLAIM = "等待接收";
		
		
		/**
		 * 错误信息
		 * @author wangchanghui
		 *
		 */
		public static class Error{
			public static final String PERMISSION = "权限不足！";
			
		}
		/**
		 * 成功信息
		 * @author wangchanghui
		 *
		 */
		public static class Success{
		}
	}
	/**
	 * 参数码
	 * @author wangchanghui
	 *
	 */
	public static class ParaCode{
		
		/**
		 * 机构
		 * @author wangchanghui
		 *
		 */
		public static class Org{
			
			/**
			 * 供应商管理部
			 */
			public static final String SUPPLIER_CODE = "00008410";
			
			/**
			 * 质量管理部
			 */
			public static final String QUALITY_CODE = "00008413";
			
			/**
			 * 招标部
			 */
			public static final String BIDDING_CODE = "00008411,00008412";
		}
		
	}
}

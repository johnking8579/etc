package com.jd.common.constants;

/**
 * 字典常量类
 * @author wangchanghui
 *
 */
public class DictDataConstant {
	/**
	 * 供应商
	 * @author wangchanghui
	 *
	 */
	public static class Supplier{
		/**
		 * 供应商范围code
		 */
		public static final String AREA = "supplier_coverage";
		
		/**
		 * 企业code
		 */
		public static final String ENTERPRISE = "enterprise";
		
		/**
		 * 货币种类code
		 */
		public static final String CURRENCY = "currency";
		
		/**
		 * 质量认证code
		 */
		public static final String QUALIFICATION = "qualification";
	}
	
	/**
	 * 任务单
	 * @author wangchanghui
	 *
	 */
	public static class Task{
		/**
		 * 任务状态code
		 */
		public static final String TASK_STATUS = "task_status";
	}
}

package com.jd.common.util;

import org.apache.commons.lang3.StringUtils;

public class StringHandler {
	
	/**
	 * 处理前台查询条件中的手动输入条件
	 * @param name
	 * @return
	 */
	public static String handleName(String name){
		if(StringUtils.isNotBlank(name)){
    		if( name.contains("%")){
    			name=name.replace("%","\\%");
    		}
    		if( name.contains("_")){
    			name=name.replace("_","\\_");
    		}
    	}
    	return name;
	}
}

package com.jd.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author Wang YunLong
 * @Description: 验证字符串正则表达式
 */

public class MatchesString {

	/**
	 * 数字
	 * @param var
	 * @return
	 */
	public static boolean matchesMAth(String var){
		
		return var.matches("^[0-9]*$");
	}
	
	/**
	 * 数字，小数点
	 * @param var
	 * @return
	 */
	public static boolean matchesMAthAndP(String var){
		
		return var.matches("^\\d+\\.{0,1}\\d*$");
	}
	 

	/**
	 * 手机
	 * @param var
	 * @return
	 */
	 public static boolean matchesPhone(String var){
		 
		return var.matches("^((13[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$");
	}
	 
	 /**
	  *邮箱
	  * @param var
	  * @return
	 */
	 public static boolean matchesEMAL(String var){
			 
		return var.matches("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
	}
	 /**
	  * 字符串转换date
	  * @param dateStr
	  * @param formatStr
	  * @return
	  */
	 public static Date stringToDate(String dateStr){
		 java.sql.Date sdt=java.sql.Date.valueOf(dateStr);
//			DateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//			Date date=null;
//			try {
//				date = (Date) sdf.parse(dateStr);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
			return sdt;
		}
}
package com.i8app.ezinstall.common.app.assembly;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.AppService;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.assembly.process.OsProcessor;
import com.i8app.ezinstall.common.app.dao.AssemblyLogDao;
import com.i8app.ezinstall.common.app.domain.App;
import com.i8app.ezinstall.common.app.domain.AssemblyLog;
import com.i8app.ezinstall.common.app.query.CpQuery;
import com.i8app.ezinstall.common.app.query.CpQueryFactory;
import com.i8app.ezinstall.common.app.robot.PkgnameController;

/**
 * APP汇集功能的主类
 * 
 * @author JingYing 2013-5-3
 */
@Component
public class AppAssemblierImpl implements Constants, AppAssemblier	{
	private static Logger log = Logger.getLogger(AppAssemblierImpl.class);
	public static final String FILESERVER_DIR = ClientParamHolder.getFileServerDir();
	@Resource
	private PkgnameController pkgnameController;
	@Resource
	private AssemblyLogDao assemblyDao;
	@Resource
	private AppService appService;
	@Resource 
	private CpQueryFactory cpQueryFactory;
	@Resource 
	private AppAdapteeFactory appAdapteeFactory;
	@Resource(name="androidProcessorImpl")
	private OsProcessor androidProcessorImpl;
	@Resource(name="iOSProcessorImpl")
	private OsProcessor iOSProcessorImpl;
	@Resource(name="otherOsProcessorImpl")
	private OsProcessor otherOsProcessorImpl;
	
	/**
	 * 使用synchronized, 防止外界并发调用时, 生成重复app记录
	 */
	@Override
	public synchronized Map<String, Integer> assembly(String cpId, List<String> originalPackIds)	{
		AppAdaptee adaptee = appAdapteeFactory.factory(cpId);
		List list = adaptee.findByOriginalPackIds(originalPackIds);
		List<AssemblyReport> reports = this.importSpecifiedOriginalApp(adaptee, list);
		log.info("已汇集指定的app");
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(AssemblyReport a : reports)	{
			if(a.getAssemblyStatus() != ERRCODE_NORMAL)	{
				map.put(a.getOriginalPackId(), a.getAssemblyStatus());
			}
		}
		return map;
	}
	
	
	@Override
	public void startAssembly() {
		long stime = System.currentTimeMillis();
		log.error(new Date().toString() + " : 开始进行数据汇集...");
		try	{
			log.info("开始更新app的冗余字段");
			appService.updateRedundantField(); //首先处理冗余字段,防止解析CP资源时间过长,导致APP冗余字段长时间无法更新.
			log.info("冗余字段更新完成");
			for(Entry<String,String> e : ClientParamHolder.getCpMapToAssembly().entrySet())	{	//按照cpList的顺序导入
				AppAdaptee adaptee = appAdapteeFactory.factory(e.getKey());
				log.info("开始解析CP商资源:" + adaptee + "....");
				importToApp(adaptee);
				log.info("CP商资源解析完成:" + adaptee);
			}
			log.info("开始修复ICON和pic");
			fixMissingIconAndPic();
			log.info("ICON和pic修复完成");
			appService.updateRedundantField(); //再次更新app表中的冗余字段,防止遗漏
			log.info(String.format("完成APP汇集.用时%.1f分",(System.currentTimeMillis() - stime)/1000/60.0));
		} catch(Exception e)	{
			log.fatal("出现错误");
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 导入app表和app_install_pack表.
	 * 见visio流程图
	 * @param <T>
	 * @param cp
	 */
	public <T> void importToApp(AppAdaptee<T> adaptee) {
		adaptee.setRscTypeMap(null);	//每一次导入时, 重置adaptee的rsctypemap为空, 以便根据rsctypeMapper的内容变化来更新数据
		int pageSize = 100, 
			count = adaptee.findValidApp(0, 1).getTotalCount();
			pageSize = pageSize < count ? pageSize : count;
		
		for(int offset=0; offset < count; offset += pageSize)	{
			Pager<T> pager = adaptee.findValidApp(offset, pageSize);
			this.importSpecifiedOriginalApp(adaptee, pager.getList());
		}
	}
	
	/**
	 * 导入某cp商软件下, 指定的app
	 * @param <T>
	 * @param adaptee
	 * @param originalAppList
	 */
	private <T> List<AssemblyReport> importSpecifiedOriginalApp(AppAdaptee<T> adaptee, List<T> originalAppList)	{
		List<AssemblyReport> reports = new ArrayList<AssemblyReport>();
		for(T t : originalAppList)	{
			AppMeta meta = adaptee.toAppMeta(t);
			AssemblyReport rpt = chooseOsProcess(meta.getOs()).process(meta, adaptee);
			reports.add(rpt);
			if(rpt.getAssemblyStatus() != 0)	{
				AssemblyLog log = new AssemblyLog();
				log.setCpId(rpt.getCpId());
				log.setOperTime(new Date());
				log.setOriginalPackId(rpt.getOriginalPackId());
				log.setOs(rpt.getOs());
				log.setStatus(rpt.getAssemblyStatus());
				assemblyDao.insert(log);
			}
		}
		appService.updateRedundantField();	//每导入一批数据后, 更新冗余字段.
		return reports;
	}
	
	private OsProcessor chooseOsProcess(String os)	{
		if(ANDROID.equals(os))	{
			return androidProcessorImpl;
		} else if(IOS.equals(os) || IOSX.equals(os))	{
			return iOSProcessorImpl;
		} else	{
			return otherOsProcessorImpl;
		}
	}
	
	
	/**
	 * 每次导入后,处理无ICON的app. 无icon的原因:未提供icon或下载失败
	 * 没有icon的app不再使用安装包内的icon, 而是直接使用cp商提供的icon
	 */
	@Override
	public void fixMissingIconAndPic()	{
		List<App> list = appService.findMissingIcon();
		for(App a : list)	{
			CpQuery packUrlQuery = cpQueryFactory.factory(a.getCpId());
			String url = packUrlQuery.findIconUrl(a.getOriginalAppId());
			if(url != null && !"".equals(url))	{
				try {
					log.debug("准备下载图片:" + url);
					String iconPath = appService.saveToIconDir(new URL(url).openStream(), ".jpg");
					a.setIcon(iconPath);
					appService.persist(a);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		List<App> list2 = appService.findMissingPic();	//如果没有pic1, 则重新下载所有pic
		for(App a : list2)	{
			CpQuery packUrlQuery = cpQueryFactory.factory(a.getCpId());
			List<String> urls = packUrlQuery.findScreenshots(a.getOriginalAppId());
			int index = 1;
			for(String url : urls)	{
				if(url != null && !"".equals(url))	{
					try {
						log.debug("准备下载图片:" + url);
						String halfPath = appService.saveToPicDir(new URL(url).openStream(), ".jpg");	//统一jpg
						a.getClass().getMethod("setPic" + index, String.class).invoke(a, halfPath);
						index ++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			appService.persist(a);
		}
		
	}
		

	/**
	 * 改变app的cpid,originalAppId字段, 并改变app相关的属性.
	 */
	@Override
	public void switchSource(String appId, String cpId, String originalAppId) {
		App app = appService.findById(appId);
		if(app == null)
			throw new IllegalArgumentException("不存在的appid:" + appId);

		AppAdaptee adaptee = appAdapteeFactory.factory(cpId);
		List list = adaptee.findByOriginalPackIds(Arrays.asList(originalAppId));
		if(list.isEmpty())
			throw new IllegalArgumentException("没找到对应的数据. cpid=" + cpId + ",originalAppId=" + originalAppId);
		
		List<File> oldPics = new ArrayList<File>();
		if(app.getIcon() != null)	{
			oldPics.add(new File(FILESERVER_DIR + app.getIcon()));
		}
		if(app.getPic1() != null)	{
			oldPics.add(new File(FILESERVER_DIR + app.getPic1()));
		}
		if(app.getPic2() != null)	{
			oldPics.add(new File(FILESERVER_DIR + app.getPic2()));
		}
		if(app.getPic3() != null)	{
			oldPics.add(new File(FILESERVER_DIR + app.getPic3()));
		}
		if(app.getPic4() != null)	{
			oldPics.add(new File(FILESERVER_DIR + app.getPic4()));
		}
		
		
		AppMeta meta = adaptee.toAppMeta(list.get(0));
		app.setCpId(meta.getCpId());
		app.setDeveloper(meta.getDeveloper());
		app.setInfo(meta.getInfo());
		app.setName(meta.getOriginalName());	//暂时使用原始名称,不使用安装包内的名称,避免重复下载
		app.setOriginalAppId(meta.getOriginalAppId());
		app.setOriginalTypeId(meta.getOriginalTypeId());
		app.setPinyin(Util.getPinyinInitial(app.getName()));
		app.setRscTypeId(meta.getRscTypeId());
		app.setUpdateTime(new Date());
		
		//改icon
		try {
			app.setIcon(appService.saveToIconDir(meta.getIconUri().toURL().openStream(), ".jpg"));
		} catch (Exception e) {
			e.printStackTrace();
			app.setIcon(null);	//更新图片失败后,要把图片设为null
		}
		
		//改截图
		int index = 1;
		for(URI uri : meta.getScreenshotsUri())	{
			try {
				String halfPath = appService.saveToPicDir(uri.toURL().openStream(), ".jpg");	//统一jpg
				app.getClass().getMethod("setPic" + index, String.class).invoke(app, halfPath);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					//把没下载下来的图片设为null
					app.getClass().getMethod("setPic" + index, String.class).invoke(app, new Object[]{null});
				} catch (Exception e1) {
					e1.printStackTrace();
				}	
			}
			index ++;
		}
		appService.persist(app);
		
		//删除原有图片
		for(File f : oldPics)	{
			if(f.isFile() && f.exists())	{
				f.delete();
			}
		}
	}
	
	/**
	 * 使用线程同步, 避免并发造成数据库负担
	 */
	@Override
	public synchronized void updatePackStatus() {
		//TODO 是否要取消
		throw new UnsupportedOperationException();
//		logger.info("开始updatePackStatus");
//		for(Entry<String,String> e : ClientParamHolder.getCpMapToAssembly().entrySet())	{	//按照cpList的顺序导入
//			for(AppAdaptee adaptee : appAdapteeList)	{
//				if(adaptee.getCpId().equals(e.getKey()))	{
//					adaptee.updatePackStatus();
//				}
//			}
//		}
//		logger.info("updatePackStatus完成");
	}


	@Override
	public boolean receivePkgname(String cpId, String os, String pkgname) {
		return pkgnameController.receive(cpId, os, pkgname);
	}
}

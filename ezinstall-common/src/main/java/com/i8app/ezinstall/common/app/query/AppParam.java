package com.i8app.ezinstall.common.app.query;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.i8app.ezinstall.common.app.Constants;

/**
 * 查询APP时, 使用的查询条件组合. 
 * @author JingYing 2013-5-20
 */
public class AppParam implements Constants  {
	
	/**
	 * 软件类别id
	 */
	public String rscTypeId;
	
	/**
	 * 软件名称关键字
	 */
	public String name;
	
	/**
	 * 软件名称的拼音首字母. 
	 */
	public String pinyin;
	
	/**
	 * cp商的id集合.
	 */
	public List<String> cpIds;
	
	/**
	 * cp商软件的更新时间, 查询该时间点以后更新的软件
	 */
	public Date cpUpdateTime;

	/**
	 * 排序条件. 取值见Constants.OrderBy
	 */
	public OrderBy orderBy;
	
	public AppParam() {	}


	/**
	 * @param rscTypeId 资源类型
	 * @param name 软件名称关键字
	 * @param pinyin 软件名称的拼音首字母
	 * @param cpIds cp商的id
	 * @param cpUpdateTime cp商的更新时间
	 * @param orderBy 排序字段(降序), 取值见Constants.OrderBy.
	 */
	public AppParam(String rscTypeId, String name, String pinyin, 
			List<String> cpIds, Date cpUpdateTime, OrderBy orderBy) {
		this.rscTypeId = rscTypeId;
		this.name = name;
		this.pinyin = pinyin;
		this.cpIds = cpIds;
		this.cpUpdateTime = cpUpdateTime;
		this.orderBy = orderBy;
	}

	/**
	 * 转换为ibatis使用的参数
	 * @return
	 */
	public Map<String, Object> toParamMap()	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("cpIds", cpIds);
		map.put("rscTypeId", rscTypeId);
		map.put("name", name);
		map.put("pinyin", pinyin);
		map.put("orderBy", orderBy);
		map.put("cpUpdateTime", cpUpdateTime);
		return map;
	}
	
	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

	public OrderBy getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(OrderBy orderBy) {
		this.orderBy = orderBy;
	}

	public List<String> getCpIds() {
		return cpIds;
	}

	public void setCpIds(List<String> cpIds) {
		this.cpIds = cpIds;
	}

	public Date getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(Date cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}
}

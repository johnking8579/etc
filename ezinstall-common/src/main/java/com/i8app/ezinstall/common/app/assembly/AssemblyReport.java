package com.i8app.ezinstall.common.app.assembly;

public class AssemblyReport {
	private String cpId, originalPackId, os;
	private int assemblyStatus;
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	public String getOriginalPackId() {
		return originalPackId;
	}
	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public int getAssemblyStatus() {
		return assemblyStatus;
	}
	public void setAssemblyStatus(int assemblyStatus) {
		this.assemblyStatus = assemblyStatus;
	}
}

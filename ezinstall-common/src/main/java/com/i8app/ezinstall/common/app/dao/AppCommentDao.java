package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.AppComment;

@Component
public class AppCommentDao extends BaseDao	{
	
	public Pager<AppComment> findByApp(String appId, int offset, int pageSize)	{
		Map map = new HashMap();
		map.put("appId", appId);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		int count = (Integer)sqlMapClientTemplate.queryForObject("AppComment.byApp-count", appId);
		List<AppComment> list = sqlMapClientTemplate.queryForList("AppComment.byApp", map);
		return new Pager<AppComment>(list, count);
	}

	public List findOldApp() {
		return sqlMapClientTemplate.queryForList("AppComment.findOldApp");
	}

	public void updateAppId(Object id, String uuid) {
		Map map = new HashMap(2);
		map.put("id", id);
		map.put("uuid", uuid);
		sqlMapClientTemplate.update("AppComment.updateAppId", map);
	}

}

package com.i8app.ezinstall.common.app.robot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.HttpClientUtil;
import com.i8app.ezinstall.common.app.AbstractI8appAcquireOnUpdate;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.I8appAcquire;
import com.i8app.ezinstall.common.app.I8appService;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppAdapteeFactory;
import com.i8app.ezinstall.common.app.assembly.AppAssemblierImpl;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.dao.I8appDao;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.domain.I8app;
import com.i8app.ezinstall.common.app.query.CpQuery;
import com.i8app.ezinstall.common.app.query.CpQueryFactory;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;
import com.i8app.ezinstall.common.wsclient.WdjConnection;
import com.i8app.ezinstall.common.wsclient.WdjJsonParser;
import com.i8app.ezinstall.packreader.PackReadException;

/**
 * i8app爬虫, 用于更新android包 
 * 
 * @author JingYing 2013-11-15
 */
@Component
public class I8appApkRobot implements Constants{
	
	static Logger logger = Logger.getLogger(I8appApkRobot.class);
	
	static String 
		FILESERVER_DIR = ClientParamHolder.getFileServerDir(),
		FILESERVER_URL = ClientParamHolder.getFileServerUrl();
	
	@Resource
	private I8appDao i8appDao;
	@Resource
	private I8appService i8appService;
	@Resource
	private AppInstallPackDao appInstallPackDao;
	@Resource
	private CpQueryFactory cpQueryFactory;
	@Resource
	private AppAssemblierImpl appAssemblier;
	@Resource
	private AppAdapteeFactory appAdapteeFactory;
	@Resource
	private WdjService wdjService;
	

	/**
	 * 从installPack库中更新
	 */
	public void startFromInstallPack()	{
		logger.info("正在添加i8app...");
		insertFromInstallPack();
		logger.info("正在更新i8app...");
		updateFromInstallPack();
		logger.info("将i8app导入中心库...");
		AppAdaptee i8appAdapteeImpl = appAdapteeFactory.factory(CP_I8APP);
		appAssemblier.importToApp(i8appAdapteeImpl);	//调用appAssemblier的汇集程序,将新增加软件入库.
		logger.info("i8appRobot执行完毕");
	}
	
	/**
	 * 从pack库中补充资源至i8app库
	 */
	public void insertFromInstallPack()	{
		List<AppDTO> list = i8appDao.findMissingFromPack();
		logger.info("需要补充" + list.size() + "个i8app...");
		for(final AppDTO app : list)	{
			logger.debug("正在处理:" + app.getName() + "-" + app.getPackCpId() + "-" + app.getOriginalPackId());
			CpQuery query = cpQueryFactory.factory(app.getPackCpId());
			String url;
			try {
				url = query.findPackUrl(app.getOriginalPackId(), app.getPackOs());
				File tmp = HttpClientUtil.downloadToTemp(url, ".apk", 10*60);
				i8appService.insertApk(tmp, new I8appAcquire() {
					
					@Override
					public String getRscTypeId() {
						return app.getRscTypeId();
					}
					
					@Override
					public List<InputStream> getPics() {
						List<InputStream> list = new ArrayList<InputStream>();
						try {
							if(app.getPic1() != null)
								list.add(new FileInputStream(FILESERVER_DIR + app.getPic1()));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						try {
							if(app.getPic2() != null)
								list.add(new FileInputStream(FILESERVER_DIR + app.getPic2()));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						try {
							if(app.getPic3() != null)
							list.add(new FileInputStream(FILESERVER_DIR + app.getPic3()));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						try {
							if(app.getPic4() != null)
								list.add(new FileInputStream(FILESERVER_DIR + app.getPic4()));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						return list;
					}
					
					@Override
					public InputStream getBanner() {
						if(app.getBanner() != null)
							try {
								return new FileInputStream(FILESERVER_DIR + app.getBanner());
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
							
						return null;
					}
					
					@Override
					public String getOs() {
						return app.getPackOs();
					}
					
					@Override
					public int getIsAutoUpdate() {
						return 1;
					}
					
					@Override
					public String getInfo() {
						return app.getInfo();
					}
					
					@Override
					public String getDeveloper() {
						return app.getDeveloper();
					}
					
					@Override
					public String getChangeLog() {
						return null;	//无值
					}
					
				});
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				continue;
			}
		}
		
	}
	
	/**
	 * 从appInstallPack库中检索, 更新I8APP
	 */
	public void updateFromInstallPack()	{
		List<Map<String, Object>>  list = i8appDao.findOverdueFromPack();
		logger.info("需要更新" + list.size() + "个i8app记录...");
		for(Map<String, Object> map : list)	{
			final AppInstallPack pack = appInstallPackDao.findById((String)map.get("uuid"));
			CpQuery query = cpQueryFactory.factory(pack.getCpId());
			String url;
			try {
				url = query.findPackUrl(pack.getOriginalPackId(), pack.getOs());
				logger.debug("准备下载:" + (String)map.get("name") + "-" + (String)map.get("uuid"));
				File tmp = HttpClientUtil.downloadToTemp(url, ".apk", 10 * 60);
				i8appService.updateApk(tmp, (Integer)map.get("id"), new AbstractI8appAcquireOnUpdate() {
					
					@Override
					public String getInfo() {
						return pack.getApp().getInfo();
					}
					
					@Override
					public String getDeveloper() {
						return pack.getApp().getDeveloper();
					}
					
					@Override
					public String getChangeLog() {
						return null;	//资源中心库中没有changeLog
					}
					
					@Override
					public InputStream getBanner() {
						try {
							if(pack.getApp().getBanner() != null)
								return new FileInputStream(FILESERVER_DIR + pack.getApp().getBanner());
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						return null;	
					}
				});
				
			} catch(Exception e)	{
				logger.error(e.getMessage() + ":" + (String)map.get("uuid"), e);
			}
		}
	}
	
	
	/**
	 * 从豌豆荚更新, 并导入中心库
	 */
	public void updateFromWdj()	{
		logger.info("查找需要更新的数据");
		List<I8app> list = i8appDao.findNeedUpdateFromWandoujia();
		logger.info("准备从豌豆荚更新, 待检测数据量:" + list.size());
		for(I8app i8app : list)	{
			wdjService.updateFromWdj(i8app);
		}
		
		logger.info("将i8app导入中心库...");
		AppAdaptee i8appAdapteeImpl = appAdapteeFactory.factory(CP_I8APP);
		appAssemblier.importToApp(i8appAdapteeImpl);	//调用appAssemblier的汇集程序,将新增加软件入库.
		logger.info("i8appRobot执行完毕");
	}
	
}

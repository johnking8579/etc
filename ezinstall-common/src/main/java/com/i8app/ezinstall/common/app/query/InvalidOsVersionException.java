package com.i8app.ezinstall.common.app.query;

/**
 * 无效的操作系统版本号
 * 
 * @author JingYing 2013-5-17
 */
public class InvalidOsVersionException extends Exception {

	public InvalidOsVersionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidOsVersionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}

package com.i8app.ezinstall.common.app.domain;

import java.util.Date;

public class I8app {
	
	private Integer id;
	private String name, developer, info, infoHtml, icon, pic1, pic2, pic3,
			pic4, minOsVer, pkgName, version, ipaItemId, memo, itunesLatestVer, 
			appleId, language, packUrl, changeLog, banner;
	private Long minOsVerValue;
	private String os;
	private String rscTypeId;
	private int isDel, fileSize, isAutoUpdate;
	private Integer versionCode;
	private Date addTime, updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfoHtml() {
		return infoHtml;
	}

	public void setInfoHtml(String infoHtml) {
		this.infoHtml = infoHtml;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}

	public String getPic4() {
		return pic4;
	}

	public void setPic4(String pic4) {
		this.pic4 = pic4;
	}

	public String getMinOsVer() {
		return minOsVer;
	}

	public void setMinOsVer(String minOsVer) {
		this.minOsVer = minOsVer;
	}

	public String getPkgName() {
		return pkgName;
	}

	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getMinOsVerValue() {
		return minOsVerValue;
	}

	public void setMinOsVerValue(Long minOsVerValue) {
		this.minOsVerValue = minOsVerValue;
	}
	
	public int getIsDel() {
		return isDel;
	}

	public void setIsDel(int isDel) {
		this.isDel = isDel;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public Integer getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIpaItemId() {
		return ipaItemId;
	}

	public void setIpaItemId(String ipaItemId) {
		this.ipaItemId = ipaItemId;
	}

	public String getItunesLatestVer() {
		return itunesLatestVer;
	}

	public void setItunesLatestVer(String itunesLatestVer) {
		this.itunesLatestVer = itunesLatestVer;
	}

	public String getAppleId() {
		return appleId;
	}

	public void setAppleId(String appleId) {
		this.appleId = appleId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPackUrl() {
		return packUrl;
	}

	public void setPackUrl(String packUrl) {
		this.packUrl = packUrl;
	}

	public int getIsAutoUpdate() {
		return isAutoUpdate;
	}

	public void setIsAutoUpdate(int isAutoUpdate) {
		this.isAutoUpdate = isAutoUpdate;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}
}

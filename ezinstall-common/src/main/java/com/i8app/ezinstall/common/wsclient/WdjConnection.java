package com.i8app.ezinstall.common.wsclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import com.i8app.ezinstall.common.Util;

/**
 * 使用httpclient连接池连接豌豆荚, 并缓存一个集合, 用来缓存无法找到的包名, 降低访问次数.
 * 
 * @author JingYing 2014-1-23
 */
public class WdjConnection {
	
	public static final String URL = "http://apps.wandoujia.com/api/v1/apps/";

	private static PoolingHttpClientConnectionManager defaultMng 
		= new PoolingHttpClientConnectionManager();
	private static CloseableHttpClient defaultHttpClient 
		= HttpClients.custom().setConnectionManager(defaultMng).build(); 
	
	private static Set<String> NOTFOUND_PKGNAME = new HashSet<String>();
	
	/**
	 * 访问豌豆荚
	 * @param pkgName
	 * @return json
	 * @throws IOException. 包名没查到时, 状态码为404
	 */
	public String connect(String pkgName) throws IOException	{
		if(NOTFOUND_PKGNAME.contains(pkgName))	{
			throw new IOException("包名无法在豌豆荚检测到:" + pkgName);
		}
		
		final HttpGet get = new HttpGet(URL + pkgName);
		get.setConfig(RequestConfig.custom()
	    		.setConnectionRequestTimeout(30000) //从连接池等待连接的时间
	    		.setConnectTimeout(10000)	//连接超时
	    		.setSocketTimeout(30000)	//每次socket通信超时时间	
	    		.build());
		
		CloseableHttpResponse resp = null;
		try {
			resp = defaultHttpClient.execute(get);
			int status = resp.getStatusLine().getStatusCode();
			if (status >= 200 && status < 300) {
				return EntityUtils.toString(resp.getEntity());
			} else	{
				NOTFOUND_PKGNAME.add(pkgName);
				throw new IOException(URL + pkgName + ", 返回码:" + status);
			}
		} finally	{
			Util.closeStream(resp);
		}
	}
	
	
	/**
	 * 访问豌豆荚
	 * @param pkgName
	 * @return json
	 * @throws IOException. 包名没查到时,报fileNotFoundException
	 * @deprecated urlconnection不稳定
	 */
	public String connect2(String pkgName) throws IOException	{
		HttpURLConnection conn = (HttpURLConnection)new URL(URL + pkgName).openConnection();
		conn.setConnectTimeout(15000);
		conn.setReadTimeout(15000);
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
		StringBuilder sb = new StringBuilder();
		String s;
		while((s = br.readLine()) != null)
			sb.append(s);
		return sb.toString();
	}
	
	/**
	 * 返回示例:
	 * {
  "ad": false,
  "alias": "支付宝",
  "apks": [
    {
      "adsType": "NONE",
      "beta": false,
      "brief": null,
      "bytes": 22122826,
      "compatible": 1,
      "creation": 1386293769000,
      "dangerousPermissions": [
        "发送短信",
        "监听新安装应用",
        "访问联系人数据",
        "读取短信或彩信"
      ],
      "developerId": null,
      "downloadUrl": {
        "aggrWeight": null,
        "market": "官方",
        "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
        "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826&pos=t/detail"
      },
      "downloadUrls": [
        {
          "aggrWeight": null,
          "market": "官方",
          "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
          "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826&pos=t/detail"
        }
      ],
      "downloadUrlsOrigin": [
        {
          "aggrWeight": null,
          "market": "官方",
          "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
          "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826"
        }
      ],
      "featuresInfo": 12949390466,
      "id": 7519865,
      "incompatibleDetail": null,
      "itemStatus": "SHOW",
      "langType": null,
      "language": [
        "繁体中文",
        "英文",
        "简体中文"
      ],
      "maxSdkVersion": 0,
      "md5": "bad1911bad6b2578910aae92a5092b52",
      "minSdkVersion": 8,
      "packageName": "com.eg.android.AlipayGphone",
      "paidType": "NONE",
      "permissionLevel": "TRUSTED",
      "permissionStatement": "1,支付宝钱包已经跟很多应用打通和开展支付合作，可以实现支付宝ID快捷登录合作应用，方便用户登录和完成支付，因此需要监听新安装应用权限； <br /><br />2，支付宝有手机号转账功能，需调用通讯录； <br /><br />3，读取短信或彩信用于移动快捷支付安全校验。",
      "permissions": [
        "查看 Wi-Fi 状态",
        "显示系统级警报",
        "拍照",
        "发送短信",
        "防止手机休眠",
        "控制振动器",
        "录音",
        "读取基于网络的粗略位置",
        "查看网络状态",
        "读取精准的 GPS 位置",
        "访问联系人数据",
        "修改全局系统设置",
        "发现已知帐户",
        "读取手机状态和身份",
        "更改 Wi-Fi 状态",
        "装载和卸载文件系统",
        "更改您的音频设置",
        "监听新安装应用",
        "更改网络连接性",
        "检索当前运行的应用程序",
        "网络通信",
        "读取短信或彩信",
        "更改用户界面设置"
      ],
      "resolution": [
        "ldpi",
        "mdpi",
        "hdpi",
        "xhdpi"
      ],
      "securityDetail": [
        {
          "failedInfo": null,
          "provider": "360",
          "status": "UNKNOWN"
        },
        {
          "failedInfo": null,
          "provider": "tencent",
          "status": "SAFE"
        },
        {
          "failedInfo": null,
          "provider": "lbe",
          "status": "SAFE"
        }
      ],
      "securityStatus": "SAFE",
      "signature": "55f2d10f4275f46a3c0ad229cdb71ac7",
      "size": "21.1M",
      "superior": 1,
      "targetSdkVersion": 17,
      "verified": 3,
      "versionCode": 46,
      "versionName": "7.7.7.1206"
    }
  ],
  "award": {
    "authorDesc": null,
    "authorLink": null,
    "authorName": null,
    "banner": null,
    "blogImage": null,
    "blogLink": null,
    "blogTitle": null,
    "comment": null,
    "creation": null,
    "issue": null,
    "markdownComment": null,
    "modification": null
  },
  "banner": "http://img.wdjimg.com/mms/banner/6/64/7da7f8d02449676eea6d0c728be72646_720_1230.jpeg",
  "categories": [
    {
      "alias": "finance",
      "name": "金融理财"
    },
    {
      "alias": null,
      "name": "支付"
    }
  ],
  "changelog": "【更新信息】 <br /><br />1、更多受欢迎的精品公众服务入驻 <br /><br />2、优化转账，给小伙伴转账更便捷 <br /><br />3、余额宝增加转入、转出时间提示 <br /><br />4、优化钱包操作界面，高端又大气",
  "commentsCount": 2748,
  "cooperator": false,
  "defaultVersionCode": 46,
  "description": "支付宝钱包原手机支付宝是支付宝官方推出的集手机支付和生活应用为一体的手机软件.伴随7亿用户的期待,在手机上,支付宝和钱包融为一体了.收款付款转账缴费充值电子券它们随时随身,你自由自在.主要功能1.卡券管理一堆的电子券,现在都可以把它们收到这里,尊享与优惠再也不落下2.手机转账免手续费,只要有对方手机号即可转账到其支付宝账户或银行卡3.手机充值折扣优惠,为自己为他人随时充话费,让手机永不停机4.信用卡还款免手续费,支持主流的30余家银行的信用卡还款,还有还款日提醒5.水电燃缴费免手续费,随时随地轻松完成水电燃气缴费,免去排队之苦.",
  "detailParam": "pos=t/detail",
  "developer": {
    "email": "chengzhu.zhangcz@alipay.com",
    "id": 4228264,
    "intro": null,
    "name": "支付宝（中国）网络技术有限公司",
    "urls": null,
    "verified": null,
    "website": "http://mobile.alipay.com",
    "weibo": null
  },
  "dislikesCount": 1150,
  "downloadCount": 11169630,
  "downloadCountStr": "1117万",
  "editorComment": "国内领先的移动支付平台，不仅有理财神器余额宝，还可以还信用卡、转账、付款、收款、缴费。",
  "exclusiveBadge": 0,
  "extensionPacks": [
    
  ],
  "icons": {
    "px24": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_24_24.png",
    "px36": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_36_36.png",
    "px256": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_256_256.png",
    "px100": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_100_100.png",
    "px78": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_78_78.png",
    "px68": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_68_68.png",
    "px48": "http://img.wdjimg.com/mms/icon/v1/5/9f/d056243f3440b848c2c2fd508f2d99f5_48_48.png"
  },
  "iconsStr": "d056243f3440b848c2c2fd508f2d99f5#512#512#png",
  "id": 2509,
  "imprUrl": null,
  "installedCount": 40657097,
  "installedCountStr": "4066万",
  "itemStatus": 1,
  "latestApk": {
    "adsType": "NONE",
    "beta": false,
    "brief": null,
    "bytes": 22122826,
    "compatible": null,
    "creation": 1386293769000,
    "dangerousPermissions": [
      "发送短信",
      "监听新安装应用",
      "访问联系人数据",
      "读取短信或彩信"
    ],
    "developerId": null,
    "downloadUrl": {
      "aggrWeight": 300,
      "market": "官方",
      "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
      "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826"
    },
    "downloadUrls": [
      {
        "aggrWeight": 300,
        "market": "官方",
        "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
        "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826"
      }
    ],
    "downloadUrlsOrigin": [
      {
        "aggrWeight": null,
        "market": "官方",
        "referUrl": "http://apps.wandoujia.com/apps/com.eg.android.AlipayGphone?uid=4228264",
        "url": "http://apps.wandoujia.com/redirect?signature=3655ef1&url=http%3A%2F%2Fapk.wdjcdn.com%2F2%2Fb5%2Fbad1911bad6b2578910aae92a5092b52.apk&pn=com.eg.android.AlipayGphone&md5=bad1911bad6b2578910aae92a5092b52&apkid=7519865&vc=46&size=22122826"
      }
    ],
    "featuresInfo": 12949390466,
    "id": 7519865,
    "incompatibleDetail": null,
    "itemStatus": "SHOW",
    "langType": null,
    "language": [
      "繁体中文",
      "英文",
      "简体中文"
    ],
    "maxSdkVersion": 0,
    "md5": "bad1911bad6b2578910aae92a5092b52",
    "minSdkVersion": 8,
    "packageName": "com.eg.android.AlipayGphone",
    "paidType": "NONE",
    "permissionLevel": "TRUSTED",
    "permissionStatement": "1,支付宝钱包已经跟很多应用打通和开展支付合作，可以实现支付宝ID快捷登录合作应用，方便用户登录和完成支付，因此需要监听新安装应用权限； <br /><br />2，支付宝有手机号转账功能，需调用通讯录； <br /><br />3，读取短信或彩信用于移动快捷支付安全校验。",
    "permissions": [
      "查看 Wi-Fi 状态",
      "显示系统级警报",
      "拍照",
      "发送短信",
      "防止手机休眠",
      "控制振动器",
      "录音",
      "读取基于网络的粗略位置",
      "查看网络状态",
      "读取精准的 GPS 位置",
      "访问联系人数据",
      "修改全局系统设置",
      "发现已知帐户",
      "读取手机状态和身份",
      "更改 Wi-Fi 状态",
      "装载和卸载文件系统",
      "更改您的音频设置",
      "监听新安装应用",
      "更改网络连接性",
      "检索当前运行的应用程序",
      "网络通信",
      "读取短信或彩信",
      "更改用户界面设置"
    ],
    "resolution": [
      "ldpi",
      "mdpi",
      "hdpi",
      "xhdpi"
    ],
    "securityDetail": [
      {
        "failedInfo": null,
        "provider": "360",
        "status": "UNKNOWN"
      },
      {
        "failedInfo": null,
        "provider": "tencent",
        "status": "SAFE"
      },
      {
        "failedInfo": null,
        "provider": "lbe",
        "status": "SAFE"
      }
    ],
    "securityStatus": "SAFE",
    "signature": "55f2d10f4275f46a3c0ad229cdb71ac7",
    "size": "21.1M",
    "superior": 1,
    "targetSdkVersion": 17,
    "verified": 3,
    "versionCode": 46,
    "versionName": "7.7.7.1206"
  },
  "likesCount": 1637,
  "likesRate": 59,
  "negComments": [
    {
      "authorName": "Joe",
      "comment": "真垃圾,越更新越垃圾,官方更新到最新8月的版本后给手机充值居然只有1.10.20.30这种充值后要多收了手续费的面额唯独没有能打折的50.100.等大面额,擦想钱想疯了吧,擦以后去财付通冲去了,什么垃圾玩意。",
      "id": 198317,
      "uid": null
    }
  ],
  "packageName": "com.eg.android.AlipayGphone",
  "posComments": [
    {
      "authorName": "hidecloud",
      "comment": "在手机上快捷支付很方便，配合网票网这些客户端，比在电脑上消费方便多了。",
      "id": 1155434,
      "uid": 1340698
    }
  ],
  "publishDate": 1344604646000,
  "publishNoticeBean": null,
  "reputationScore": 61,
  "screenshots": {
    "normal": [
      "http://img.wdjimg.com/mms/screenshot/a/32/9fb30838cafc7831f0af2175e41da32a_320_533.jpeg",
      "http://img.wdjimg.com/mms/screenshot/a/ce/88fc80c929b2501db2af65c02d62bcea_320_533.jpeg",
      "http://img.wdjimg.com/mms/screenshot/d/f5/6f4dd4f215d3945c3cc61c537e326f5d_320_533.jpeg",
      "http://img.wdjimg.com/mms/screenshot/0/fb/e499113b36e7b487b3fb70d6d738efb0_320_533.jpeg",
      "http://img.wdjimg.com/mms/screenshot/5/de/0ae220ab531ce788bd343d0c644aade5_320_533.jpeg",
      "http://img.wdjimg.com/mms/screenshot/5/4a/f04c716b02c886d39ee741ed7a57d4a5_320_533.jpeg"
    ],
    "small": [
      "http://img.wdjimg.com/mms/screenshot/a/32/9fb30838cafc7831f0af2175e41da32a_200_333.jpeg",
      "http://img.wdjimg.com/mms/screenshot/a/ce/88fc80c929b2501db2af65c02d62bcea_200_333.jpeg",
      "http://img.wdjimg.com/mms/screenshot/d/f5/6f4dd4f215d3945c3cc61c537e326f5d_200_333.jpeg",
      "http://img.wdjimg.com/mms/screenshot/0/fb/e499113b36e7b487b3fb70d6d738efb0_200_333.jpeg",
      "http://img.wdjimg.com/mms/screenshot/5/de/0ae220ab531ce788bd343d0c644aade5_200_333.jpeg",
      "http://img.wdjimg.com/mms/screenshot/5/4a/f04c716b02c886d39ee741ed7a57d4a5_200_333.jpeg"
    ]
  },
  "screenshotsStr": "9fb30838cafc7831f0af2175e41da32a#480#800#png,88fc80c929b2501db2af65c02d62bcea#480#800#png,6f4dd4f215d3945c3cc61c537e326f5d#480#800#png,e499113b36e7b487b3fb70d6d738efb0#480#800#png,0ae220ab531ce788bd343d0c644aade5#480#800#png,f04c716b02c886d39ee741ed7a57d4a5#480#800#png",
  "selectedExtensionPack": 0,
  "snippet": "支付宝钱包原手机支付宝是支付宝官方推出的集手机支付和生活应用为一体的手机软件.伴随7亿用户的期待,在手机上,支付宝和钱包融为一体了.收款付款转账缴费充值电子券它们随时随身,你自由自在.主要功能1.卡券管理一堆的电子券,现在都可以把它们收到这里,尊享与优惠再也不落下2.手机转账免手续费,只要有对方手机号即可转账到其支付宝账户或银行卡3.手机充值折扣优惠,为自己为他人随时充话费,让手机永不停机4.信用卡还款免手续费,支持主流的30余家银行的信用卡还款,还有还款日提醒5.水电燃缴费免手续费,随时随地轻松完成水电燃气缴费,免去排队之苦.",
  "stat": {
    "daily": [
      {
        "date": 1387296000000,
        "downloadCount": 32527,
        "downloadCountStr": "3.3万"
      },
      {
        "date": 1387382400000,
        "downloadCount": 32402,
        "downloadCountStr": "3.2万"
      },
      {
        "date": 1387468800000,
        "downloadCount": 34768,
        "downloadCountStr": "3.5万"
      },
      {
        "date": 1387555200000,
        "downloadCount": 35985,
        "downloadCountStr": "3.6万"
      },
      {
        "date": 1387641600000,
        "downloadCount": 34544,
        "downloadCountStr": "3.5万"
      },
      {
        "date": 1387728000000,
        "downloadCount": 33021,
        "downloadCountStr": "3.3万"
      },
      {
        "date": 1387814400000,
        "downloadCount": 27478,
        "downloadCountStr": "2.7万"
      }
    ],
    "total": 11169630,
    "totalStr": "1117万",
    "weekly": 230725,
    "weeklyCommentsCount": 102,
    "weeklyCommentsCountStr": "102",
    "weeklyStr": "23万"
  },
  "tagline": "支付宝官方客户端",
  "tags": [
    {
      "tag": "金融理财"
    },
    {
      "tag": "支付"
    },
    {
      "tag": "支付宝"
    },
    {
      "tag": "钱包"
    },
    {
      "tag": "转账"
    },
    {
      "tag": "基金"
    },
    {
      "tag": "阿里巴巴"
    }
  ],
  "title": "支付宝钱包",
  "updatedDate": 1386302780000
}
	 */

}

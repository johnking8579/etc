package com.i8app.ezinstall.common.app.assembly.cp;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.HttpClientUtil;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.dao.AxonDao;
import com.i8app.ezinstall.common.app.dao.RscTypeMapperDao;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.domain.Axon;
import com.i8app.ezinstall.common.wsclient.AxonApi;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkReader2;

@Component
public class AxonAdapteeImpl implements AppAdaptee<Axon>	{
	private static Logger logger = Logger.getLogger(AxonAdapteeImpl.class);
	public static Map<String, String> typeMap;

	@Resource 
	private AxonDao axonDao;
	@Resource 
	private RscTypeMapperDao rscTypeMapperDao;
	
	private static AxonApi axonApi = ClientParamHolder.getAxonApi();
	private static int remoteType = ClientParamHolder.getRemoteType();
	private AxonConnection axonConnection = new AxonConnection();
	private ApkReader2 apkReader2 = new ApkReader2();
	private String packAccessMode;
	
	/**
	 * 查询安讯的资源. 要求:android,未下架 ,有效链接,优先度>0,按优先度降序排列,按更新时间降序排
	 */
	@Override
	public Pager<Axon> findValidApp(int offset, int pageSize) {
		String[] os = new String[]{"android"};	//只取android资源, 不取all系统, 因为all不确定
		return axonDao.findNeededOsApp(os, offset, pageSize);
	}

	@Override
	public AppMeta toAppMeta(Axon axon) {
		AppMeta meta = new AppMeta();
		meta.setBannerUri(null);
		meta.setChangeLog(null);
		meta.setCpId(CP_AXON);
		try {
			//安讯格式值为2012-09-12T12:52:24
			Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(axon.getUpdateTime());	//一定要将安讯日期格式处理后再入往回
			meta.setCpUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
		} catch (ParseException e) {
			meta.setCpUpdateTime(axon.getUpdateTime());
		}
		meta.setDeveloper(axon.getDevelopers());
		
		String iconUrl = axon.getLogosUrl();
		if(iconUrl != null && Util.containChinese(iconUrl))	{	
			iconUrl = Util.encodeChUrl(iconUrl);
		}
		try {
			meta.setIconUri(new URI(iconUrl));
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		meta.setInfo(Util.unicodeToUtf8(axon.getDescription()));
		meta.setOriginalAppId(axon.getContentId());
		meta.setOriginalName(axon.getContentName());
		meta.setOriginalPackId(axon.getContentId());
		meta.setOriginalTypeId(axon.getTypeId());
		meta.setOriginalVersion(axon.getVersion());
		meta.setOs(toOs(axon.getPlatform()));
		meta.setOsMinVer(null);
		meta.setRscTypeId(toRscTypeId(axon.getTypeId()));
		
		List<URI> pics = new ArrayList<URI>();
		if(axon.getScreenshots1() != null)
			try {
				pics.add(new URI(axon.getScreenshots1()));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		if(axon.getScreenshots2() != null)
			try {
				pics.add(new URI(axon.getScreenshots2()));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		if(axon.getScreenshots3() != null)
			try {
				pics.add(new URI(axon.getScreenshots3()));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		if(axon.getScreenshots4() != null)
			try {
				pics.add(new URI(axon.getScreenshots4()));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		meta.setScreenshotsUri(pics);
		meta.setStatus(axon.getIsDel());
		return meta;
	}


	private String toRscTypeId(String typeId) {
		if(typeMap == null)	{
			typeMap = rscTypeMapperDao.listToMap(rscTypeMapperDao.findByCp(getCpId()));
		}
		return typeMap.get(typeId);
	}
	
	/**
	 * os转换
	 * @param os
	 * @return
	 */
	private String toOs(String os) {
		if("android".equals(os))	{
			return ANDROID;
		} else if("ios".equals(os))	{
			return IOS;
		} else if("symbian".equals(os))	{
			return SYMBIAN;
		} else if("wm".equals(os))	{
			return WM;
		} else if("all".equals(os))	{	//把all转换为android
			return ANDROID;
		} else	{
			return null;
		}
	}
	
	@Override
	public String findPackUrl(String originalPackId, String os) throws CpServerAccessException  {
		try {
			return linkAxon(originalPackId);
		} catch (Exception e) {
			throw new CpServerAccessException(e);
		}
	}
	
	@Override
	public boolean isPackUpdated(AppMeta meta, AppInstallPack existPack) {
		if(meta.getCpUpdateTime().equals(existPack.getCpUpdateTime()))	{
			return false;
		} else	{
			return true;
		}
	}

	@Override
	public void setRscTypeMap(Map<String, String> rscTypeMap) {
		typeMap = rscTypeMap;
	}

	@Override
	public List<String> findScreenshots(String originalAppId) {
		Axon a = axonDao.findById(originalAppId);
		if(a == null){
			logger.error("missing axon id:" + originalAppId);
		}	//throw new RuntimeException("missing axon id : " + originalAppId);
		List<String> list = new ArrayList<String>();
		if(a != null)	{	//当axon表不存在相关数据时,不要向外抛空指针
			String s1 = a.getScreenshots1();
			if(s1 != null && s1.length() > 0)
				list.add(s1);
			String s2 = a.getScreenshots2();
			if(s2 != null && s2.length() > 0)
				list.add(s2);
			String s3 = a.getScreenshots3();
			if(s3 != null && s3.length() > 0)
				list.add(s3);
			String s4 = a.getScreenshots4();
			if(s4 != null && s4.length() > 0)
				list.add(s4);
		}
		return list;
	}

	@Override
	public String getCpId() {
		return CP_AXON;
	}

	public String getPackAccessMode() {
		return packAccessMode;
	}
	
	public void setPackAccessMode(String packAccessMode) {
		this.packAccessMode = packAccessMode;
	}
	
	@Override
	public String findIconUrl(String originalAppId) {
		Axon axon = axonDao.findById(originalAppId);
		if(axon == null)	return null;
		String iconUrl = axon.getLogosUrl();
		if(Util.containChinese(iconUrl))	
			iconUrl = Util.encodeChUrl(iconUrl);
		return iconUrl;
	}
	
	@Override
	public List findByOriginalPackIds(List<String> originalPackIds) {
		return axonDao.findByIds(originalPackIds);
	}

	private String linkAxon(String originalPackId) throws Exception	{
		String phoneNo = "13" + Util.randStr(9);		//使用随机手机号下载安讯资源
		switch(remoteType)	{
		case 1:
			return axonConnection.getDownloadUrl(originalPackId, phoneNo);
		case 2:
			return axonApi.getDownloadUrl(originalPackId);
		default:
			throw new IllegalArgumentException("不支持的remoteType:" + remoteType);
		}
	}

	@Override
	public Object readPack(String originalPackId, String os)
			throws CpServerAccessException, PackReadException, FileNotFoundException {
		if(MODE_REMOTE.equals(packAccessMode))	{
			File tmp = null;
			if(ANDROID.equalsIgnoreCase(os))	{
				try {
					try {
						String url = this.linkAxon(originalPackId);
						logger.info(String.format("正在下载安讯软件...原始id:%s, 地址:%s", originalPackId, url));
						tmp = HttpClientUtil.downloadToTemp(url, ".apk", 60 * 10);	//10分钟超时
					} catch (Exception e) {
						logger.error("获取下载地址/下载时失败...原始id:" + originalPackId);
						throw new CpServerAccessException(e.getMessage(), e);	//未确认的异常定义为CpServerAccessException
					}
					return apkReader2.read(tmp, true);
				} finally	{	//最后要删除临时文件
					if(tmp != null)	{
						tmp.delete();
					}
				}
			} else	{
				throw new UnsupportedOperationException("不支持的操作系统:" + os);
			}
		} else	{
			throw new UnsupportedOperationException("不支持的访问模式:" + packAccessMode);
		}
	}

}


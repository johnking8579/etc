package com.i8app.ezinstall.common.app.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.Axon;
import com.ibatis.sqlmap.client.SqlMapExecutor;

@Component
public class AxonDao extends BaseDao	{
	
	public Axon findById(String contentId) {
		return (Axon)sqlMapClientTemplate.queryForObject("Axon.byId", contentId);
	}

	/**
	 * 查询android, all, symbian, wm的数据
	 * 不查询ios
	 * @param param 
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<Axon> findNeededOsApp(String[] osArray, int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		map.put("osArray", osArray);
		List<Axon> list = sqlMapClientTemplate.queryForList("Axon.neededOsApp", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("Axon.neededOsApp-count", map);
		return new Pager<Axon>(list, count);
	}

	public List<Axon> findDelApp() {
		return sqlMapClientTemplate.queryForList("Axon.findDelApp");
	}
	
	public List<Axon> findAll()	{
		return sqlMapClientTemplate.queryForList("Axon.findAll");
	}
	
	public void update(Axon axon)	{
		sqlMapClientTemplate.update("Axon.update", axon);
	}
	
	public void bulkUpdate(final List<Axon> list)	{
		sqlMapClientTemplate.execute(new SqlMapClientCallback<Axon>() {

			@Override
			public Axon doInSqlMapClient(SqlMapExecutor executor)
					throws SQLException {
				executor.startBatch();
				for(Axon a : list)	{
					executor.update("Axon.update", a);
				}
				executor.executeBatch();
				return null;
			}
		});
	}

	public List<Axon> findByIds(List<String> originalPackIds) {
		Map map = new HashMap();
		map.put("ids", originalPackIds);
		return sqlMapClientTemplate.queryForList("Axon.byIds", map);
	}

	public void updatePackStatus() {
		sqlMapClientTemplate.update("Axon.updatePackStatus");
	}
	
	

}

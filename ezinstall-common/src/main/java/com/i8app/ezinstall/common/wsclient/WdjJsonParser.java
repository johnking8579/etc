package com.i8app.ezinstall.common.wsclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * 豌豆荚的json解析, 用来获取常用属性
 * 由于返回的json太复杂, 不直接转化成java对象, 只取常用属性
 * 
 * @author JingYing 2013-12-26
 */
public class WdjJsonParser {
	
	private JsonObject json;
	
	public WdjJsonParser(String jsonStr)	{
		this.json = new JsonParser().parse(jsonStr).getAsJsonObject();
	}
	
	public String getTitle()	{
		return json.get("title").getAsString();
	}

	public String getPackageName()	{
		return json.get("packageName").getAsString();
	}
	
	private JsonObject getLatestApk()	{
		return json.get("latestApk").getAsJsonObject();
	}
	public int getVersionCode()	{
		return getLatestApk().get("versionCode").getAsInt();
	}
	
	public String getVersionName()	{
		return getLatestApk().get("versionName").getAsString();
	}
	
	public String getDownloadUrl()	{
		return getLatestApk().get("downloadUrl").getAsJsonObject().get("url").getAsString();
	}
	
	public List<String> getLanguage()	{
		JsonArray ja = getLatestApk().get("language").getAsJsonArray();
		List<String> list = new ArrayList<String>();
		for(JsonElement je : ja)	{
			list.add(je.getAsString());
		}
		return list;
	}
	
	public String getBanner()	{
		JsonElement je = json.get("banner");
		return je instanceof JsonNull ? null : je.getAsString();
	}
	
	public String getChangelog()	{
		return json.get("changelog").getAsString().replaceAll("(<br />)|(</p>)|(<p>)", "");
	}
	
	/**
	 * 获取第一个类别ID
	 * @return
	 */
	public String getCategory0()	{
		return json.get("categories").getAsJsonArray()
				.get(0).getAsJsonObject()
				.get("name").getAsString();
	}
	
	/**
	 * 过滤html标签
	 * @return
	 */
	public String getDescription()	{
		return json.get("description").getAsString().replaceAll("(<br />)|(</p>)|(<p>)", "");
	}
	
	public String getDeveloper()	{
		return json.get("developer").getAsJsonObject().get("name").getAsString();
	}
	
	public String getLargestIcon()	{
		return json.get("icons").getAsJsonObject().get("px256").getAsString();
	}
	
	public List<String> getScreenshots()	{
		JsonArray ja = json.get("screenshots").getAsJsonObject().get("normal").getAsJsonArray();
		List<String> list = new ArrayList<String>();
		for(JsonElement je : ja)	{
			list.add(je.getAsString());
		}
		return list;
	}
	
	public static void main(String[] args) throws IOException {
		WdjJsonParser jp = new WdjJsonParser(new WdjConnection().connect("com.lbstory.sip"));
		System.out.println(jp.getScreenshots());
		System.out.println(jp.getLanguage());
		System.out.println(jp.getBanner());
	}

}

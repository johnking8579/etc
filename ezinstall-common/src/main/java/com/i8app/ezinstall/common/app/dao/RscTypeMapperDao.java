package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.domain.RscTypeMapper;

@Component
public class RscTypeMapperDao extends BaseDao	{
	
	public List<RscTypeMapper> findByCp(String cpId)	{
		return sqlMapClientTemplate.queryForList("RscTypeMapper.byCp", cpId);
	}
	
	public RscTypeMapper findUnique(String cpId, String cpAppTypeId)	{
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("cpAppTypeId", cpAppTypeId);
		return (RscTypeMapper)sqlMapClientTemplate.queryForObject("RscTypeMapper.findUnique", map);
	}
	
	public Map<String, String> listToMap(List<RscTypeMapper> list)	{
		Map<String, String> map = new HashMap<String, String>();
		for(RscTypeMapper r : list)	{
			map.put(r.getCpAppTypeId(), r.getRscTypeId());
		}
		return map;
	}

}

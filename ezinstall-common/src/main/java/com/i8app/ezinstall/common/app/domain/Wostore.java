package com.i8app.ezinstall.common.app.domain;
public class Wostore {
	private String id, name, typeId, typeName, developer, price, info,
			pic1, pic2, pic3, pic4, icon;
	private String pic1Path, pic2Path, pic3Path, pic4Path, iconPath;
	private int isDel, priority;
	
	public String getPic1Path() {
		return pic1Path;
	}

	public void setPic1Path(String pic1Path) {
		this.pic1Path = pic1Path;
	}

	public String getPic2Path() {
		return pic2Path;
	}

	public void setPic2Path(String pic2Path) {
		this.pic2Path = pic2Path;
	}

	public String getPic3Path() {
		return pic3Path;
	}

	public void setPic3Path(String pic3Path) {
		this.pic3Path = pic3Path;
	}

	public String getPic4Path() {
		return pic4Path;
	}

	public void setPic4Path(String pic4Path) {
		this.pic4Path = pic4Path;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}

	public String getPic4() {
		return pic4;
	}

	public void setPic4(String pic4) {
		this.pic4 = pic4;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getIsDel() {
		return isDel;
	}

	public void setIsDel(int isDel) {
		this.isDel = isDel;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}

package com.i8app.ezinstall.common.app;

/**
 * 常量接口
 * 
 * @author JingYing 2013-5-9
 */
public interface Constants {
	
	/**
	 * 排序字段. cpUpdateTime
	 */
	public enum OrderBy	{
		cpUpdateTime,downCount
	}
	
	/**
	 * CP商列表
	 */
	public static final String 
			CP_I8SOFT  	 = "cp01",
			CP_I8GAME  	 = "cp02", 
			CP_WOSTORE 	 = "cp03",
			CP_AXON    	 = "cp04",
			CP_TALKWEB	 = "cp05",
			CP_MM 		 = "cp06",
			CP_ITUNES	 = "cp07",
			CP_I8APP     = "cp09",
			CP_WANDOUJIA = "cp10";
	
			
	/**
	 * 操作系统列表
	 */
	public static final String 
			ANDROID = "os1",
			IOS     = "os2",
			IOSX    = "os3",
			SYMBIAN = "os4",
			JAVA    = "os5",
			WM      = "os6";
	
	/**
	 * APP汇集的错误码
	 */
	public static final int
			ERRCODE_NORMAL 		  			= 0,
			ERRCODE_DOWNLOAD_FAIL 			= 1,	//下载出错
			ERRCODE_PACKREAD_FAIL  			= 2,	//解析包失败
			ERRCODE_ONLY_IOS_FROM_APPSTORE 	= 3, 	//废弃
			ERRCODE_NO_PACK_ON_DISK			= 4,
			ERRCODE_INVALID_INSTALL_PACK    = 5;	//最终下载地址不是安装包
	
	/**
	 * 业务类型
	 */
	public static final int
			BIZ_ESSENTIAL 		= 1,
			BIZ_RANK 			= 2,
			BIZ_FORCEINSTALL 	= 3,
			BIZ_ZONE 			= 4,
			BIZ_ACTIVITY 		= 5,
			BIZ_AUTO_OPEN 		= 6,
			BIZ_HIDDEN_INSTALL 	= 7,
			BIZ_CITY_PKG 		= 8,	
			BIZ_WOSTORE_NATION_PKG = 9,
			BIZ_HOME_RECOMMEND_PKG = 10,
			BIZ_CLASSIFY_PKG = 11,
			BIZ_CUSTOM_PKG = 12,
			BIZ_BOUTIQUE_RECOMMEND_PKG = 13,
			BIZ_ADVERTISE_PKG = 14;
	
	/**
	 * 部门ID
	 */
	public static final String
			DEPTID_LIANTONG 	= "01",	//联通
			DEPTID_YIDONG 		= "02",	//移动
			DEPTID_JIETONGDA 	= "04";	//捷通达
	
	
	/**
	 * APP,应用,游戏的resource_type值
	 */
	public static final String
			RSCTYPE_APP 	= "1",		//应用与游戏的父级:app
			RSCTYPE_SOFT 	= "101",	//应用
			RSCTYPE_GAME 	= "102";	//游戏
	
	/**
	 * 获取安装包的方式:local(从本地磁盘分析安装包), remote(远程下载安装包,分析后删除)
	 */
	public static final String
			MODE_LOCAL = "local",
			MODE_REMOTE = "remote";
}

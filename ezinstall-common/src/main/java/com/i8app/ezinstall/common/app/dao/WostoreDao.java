package com.i8app.ezinstall.common.app.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.Wostore;

@Component
public class WostoreDao extends BaseDao	{
	
	public Wostore findById(String id) {
		return (Wostore)sqlMapClientTemplate.queryForObject("Wostore.byId", id);
	}

	public Pager<Wostore> findAll(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Wostore> list = sqlMapClientTemplate.queryForList("Wostore.findAll", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("Wostore.findAll-count");
		return new Pager<Wostore>(list, count);
	}
	
	public Pager<Wostore> findValidApp(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Wostore> list = sqlMapClientTemplate.queryForList("Wostore.findValidApp", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("Wostore.findValidApp-count");
		return new Pager<Wostore>(list, count);
	}

	/**
	 * 使用%like%模糊匹配wostore的操作系统. 数据库中os的格式为android,iphone这样的数据
	 * @param string
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<Wostore> findByOs(String os, int offset, int pageSize) {
		Map map = new HashMap();
		map.put("os", os);
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<Wostore> list = sqlMapClientTemplate.queryForList("Wostore.byOs", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("Wostore.byOs-count", map);
		return new Pager<Wostore>(list, count);
	}
	
	public List<String> findScreenshots(String appId)	{
		Map map = sqlMapClientTemplate.queryForMap("Wostore.findPics", appId, "id");
		Map<String, String> objMap = (Map<String, String>)map.get(appId);
		List<String> list = new ArrayList<String>();
		list.add(objMap.get("pic1"));
		list.add(objMap.get("pic2"));
		list.add(objMap.get("pic3"));
		list.add(objMap.get("pic4"));
		return list;
	}

	public List findByIds(List<String> ids) {
		Map map = new HashMap();
		map.put("ids", ids);
		return sqlMapClientTemplate.queryForList("Wostore.byIds", map);
	}

	public void updatePackStatus() {
		sqlMapClientTemplate.update("Wostore.updatePackStatus");
	}
}

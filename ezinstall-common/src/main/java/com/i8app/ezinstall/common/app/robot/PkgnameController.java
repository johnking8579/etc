package com.i8app.ezinstall.common.app.robot;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.Constants;

/**
 * 使用队列接收安装包包名, 把包名转发到业务模块进行更新或新增
 * 
 * @author JingYing 2014-1-6
 */
@Component
public class PkgnameController {
	
	private static BlockingQueue<PkgnameMsg> queue = new LinkedBlockingQueue<PkgnameMsg>(200);	//限定个数,否则常连接豌豆荚时被禁 
	private static Thread forwardThread; 
	
	@Resource
	private WdjService wdjService;
	
	static	{
		new Timer(true).scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				queue.clear();
			}
		}, 0, 48 * 3600 * 1000);	//间隔一段时间清空pkgnameQueue.
	}
	
	
	public PkgnameController()	{
		startForwardThread();
	}
	
	/**
	 * 向队列放包名
	 * @param cpId
	 * @param os
	 * @param pkgname
	 * @return
	 */
	public boolean receive(String cpId, String os, String pkgname)	{
		PkgnameMsg msg = new PkgnameMsg(cpId, os, pkgname);
		if(!queue.contains(msg))	{
			return queue.offer(msg);	//队列中有该对象时不再向里放, 超过限定容量时不再向里放
		} else{
			return false;
		}
	}
	
	/**
	 * 启动转发线程, 为了防止重启多个线程, 使用forwardThread变量判断
	 */
	private synchronized void startForwardThread()	{
		if(forwardThread == null)	{
			forwardThread = new Thread(new Runnable(){
				@Override
				public void run() {
					while(true)	{
						try {
							PkgnameMsg msg = queue.take();	//使用阻塞模式
							if(Constants.CP_WANDOUJIA.equalsIgnoreCase(msg.getCpId()) 
									&& Constants.ANDROID.equalsIgnoreCase(msg.getOs()))	{
								wdjService.checkFromWdj(msg.getPkgname());
							}
							Thread.sleep(1000 * 60 * 12); //多休眠会
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				
			});
			forwardThread.start();
		}
	}
	
}

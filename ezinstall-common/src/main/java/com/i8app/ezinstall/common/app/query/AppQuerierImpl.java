package com.i8app.ezinstall.common.app.query;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.dao.AppDao;
import com.i8app.ezinstall.common.app.dao.AppInstallPackDao;
import com.i8app.ezinstall.common.app.dao.PkgDao;
import com.i8app.ezinstall.common.app.dao.PkgGroupDao;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;

@Component
public class AppQuerierImpl implements AppQuerier	{
	private static final Logger logger = Logger.getLogger(AppQuerierImpl.class);
	
	@Resource private AppDao appDao;
	@Resource private CpQueryFactory cpQueryFactory;
	@Resource private AppInstallPackDao appInstallPackDao;
	
	@Override
	public Pager<AppDTO> findInstallPackByPkg(int pkgId, Os os, int offset, int pageSize) {
		return appDao.findInstallPackByPkg(null, pkgId, os, null, offset, pageSize);
	}

	@Override
	public Pager<AppDTO> findInstallPackByPkg(AreaParam areaParam, int pkgId, Os os, int offset, int pageSize) {
		return appDao.findInstallPackByPkg(areaParam, pkgId, os, null, offset, pageSize);
	}
	
	@Override
	public Pager<AppDTO> query(AreaParam areaParam, Os os, AppParam appParam,
			int offset, int pageSize) {
		return appDao.query(areaParam, os, appParam, offset, pageSize);
	}
	
	@Override
	public Pager<AppDTO> search(AreaParam areaParam, Os os, AppParam appParam,
			int offset, int pageSize) {
		return appDao.search(areaParam, os, appParam, offset, pageSize);
	}

	@Override
	public Pager<AppDTO> queryPack(AreaParam areaParam, Os os, AppParam appParam,
			int offset, int pageSize) {
		return appDao.queryPack(areaParam, os, appParam, offset, pageSize);
	}

	@Override
	public AppDTO findSingle(String appId, Os os) {
		return appDao.findSingle(appId, os);
	}
	
	@Override
	public AppDTO findSinglePack(String packUuid) {
		return appDao.findSinglePack(packUuid);
	}
	
	/**
	 * 根据appId查询对应的ipaItemId.
	 * 先查询正版包的itemid, 如果没有记录, 则取越狱包的itemid(可能有些越狱包信息不准).
	 *  
	 * 在appInstallpack中查询appId=? and os = 'os2'的值.
	 * 如果有值, 则返回ipaitemid.
	 * 如果无值, 则在在appInstallpack中查询appId=? and os = 'os3'的值.
	 * 如果结果为null,则说明库中只有一个越狱包,且没有itemid,无法匹配.
	 * @param appId
	 * @return
	 */
	public String findItunesId1(String appId)	{
		List<AppInstallPack> iosList = appInstallPackDao.findByAppIdAndOs(appId, IOS);
		if(iosList.size() > 0)	{
			return iosList.get(0).getIpaItemId();
		} else	{
			List<AppInstallPack> iosxList = appInstallPackDao.findByAppIdAndOs(appId, IOSX);
			if(iosxList.size() > 0)	{
				return iosxList.get(0).getIpaItemId();
			}
		}
		return null;
	}
	
	/**
	 * 根据安装包id查询对应的itemid
	 * @param packUuid
	 * @return
	 */
	public String findItunesId2(String packUuid)	{
		AppInstallPack pack = appInstallPackDao.findById(packUuid);
		return this.findItunesId1(pack.getAppId());
	}

	@Override
	public String getPackUrl(String packUuid) throws CpServerAccessException {
		AppInstallPack pack = appInstallPackDao.findById(packUuid);
		if(pack == null)	throw new IllegalArgumentException("未找到安装包:" + packUuid);
		CpQuery cpQuery = cpQueryFactory.factory(pack.getCpId());
		return cpQuery.findPackUrl(pack.getOriginalPackId(), pack.getOs());
	}
	

	/**
	 * 查出所有符合条件的安装包, 对比版本号后, 返回最新版本
	 */
	@Override
	public String confirmPack(String appId, Os os, String cpId) {
		List<AppInstallPack> list = appInstallPackDao.findByAppId(appId, os, cpId);
		if(list.isEmpty())	{	
			return null;
		} else if(list.size() == 1)	{	//只有一个结果时,不需要对比
			return list.get(0).getUuid();
		} else if(os.getOsStr().equals(Constants.ANDROID))	{	//安卓的, 按versionCode对比,值越大版本越新
			return Collections.max(list, new Comparator<AppInstallPack>() {
				@Override
				public int compare(AppInstallPack o1, AppInstallPack o2) {
					if(o1.getApkVersionCode() < o2.getApkVersionCode())	{
						return -1;
					} else if(o1.getApkVersionCode() > o2.getApkVersionCode())	{
						return 1;
					} else {
						return 0;
					}
				}
			}).getUuid();
		} else {	//非安卓系统中, 按appVer分拆对比
			return Collections.max(list, new Comparator<AppInstallPack>() {
				@Override
				public int compare(AppInstallPack o1, AppInstallPack o2) {
					try {
						return Util.compareVersion(o1.getAppVer(), o2.getAppVer());
					} catch (Exception e) {
						logger.error(String.format("版本比对出现错误:[%s][%s]", o1.getAppVer(), o2.getAppVer()), e);
						return 0;
					}
				}
			}).getUuid();
		}
	}
}

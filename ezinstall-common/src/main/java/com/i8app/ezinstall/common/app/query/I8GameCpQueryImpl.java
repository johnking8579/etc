package com.i8app.ezinstall.common.app.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.dao.AssemblyLogDao;
import com.i8app.ezinstall.common.app.dao.I8GameDao;
import com.i8app.ezinstall.common.app.dao.RscTypeMapperDao;
import com.i8app.ezinstall.common.app.domain.I8Game;

@Component
public class I8GameCpQueryImpl implements Constants, CpQuery	{
	private static final Logger logger = Logger.getLogger(I8GameCpQueryImpl.class);
	
	public static final String 
	FILESERVER_URL 			= ClientParamHolder.getFileServerUrl(),
	FILESERVER_DIR 			= ClientParamHolder.getFileServerDir(); 
	
	public static Map<String, String> typeMap;
	
	@Resource
	private I8GameDao i8GameDao;

	@Override
	public String findPackUrl(String originalPackId, String os) throws CpServerAccessException {
		I8Game s = i8GameDao.findById(Integer.parseInt(originalPackId));
		if(s == null)	throw new RuntimeException("missing game id:" + originalPackId);
		String halfPath = IOS.equals(os) ? s.getLegalFileName() : s.getFile();
		if(halfPath != null && !"".equals(halfPath))	{
			return FILESERVER_URL + halfPath;
		} else	{
			return null;
		}
	}

	@Override
	public List<String> findScreenshots(String orginalAppId ) {
		I8Game s = i8GameDao.findById(Integer.parseInt(orginalAppId));
		if(s == null){
			logger.error("missing game id:" + orginalAppId);
		}//	throw new RuntimeException("missing game id:" + orginalAppId);
		List<String> list = new ArrayList<String>();
		if(s != null)	{
			String s1 = s.getPic1();
			if(s1 != null && s1.length()>0)
				list.add(FILESERVER_URL + s1);
	
			String s2 = s.getPic2();
			if(s2 != null && s2.length()>0)
				list.add(FILESERVER_URL + s2);
			
			String s3 = s.getPic3();
			if(s3 != null && s3.length()>0)
				list.add(FILESERVER_URL + s3);
		}
		return list;
	}

	@Override
	public String findIconUrl(String originalAppId) {
		I8Game s = i8GameDao.findById(Integer.parseInt(originalAppId));
		if(s != null && s.getIcon() != null && s.getIcon().length() > 0)	{
			return FILESERVER_URL + s.getIcon(); 
		} 
		return null;
	}

	@Override
	public List findByOriginalPackIds(List<String> originalPackIds) {
		return i8GameDao.findByIds(originalPackIds);
	}

}

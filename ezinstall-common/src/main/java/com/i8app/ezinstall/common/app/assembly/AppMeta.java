package com.i8app.ezinstall.common.app.assembly;

import java.net.URI;
import java.util.Date;
import java.util.List;

public class AppMeta {
	private String developer, info, changeLog, rscTypeId;
	private URI iconUri, bannerUri;	//可接收file或url
	private List<URI> screenshotsUri;	//可接收file或url
	private String cpId, originalPackId, originalAppId, originalTypeId, os;
	private String originalName, originalVersion;	//cp提供的软件名和cp商提供的版本号, 不一定准确
	private String cpUpdateTime;
	private int status;	//安装包状态
	private String osMinVer;
	
	public String getOsMinVer() {
		return osMinVer;
	}

	public void setOsMinVer(String osMinVer) {
		this.osMinVer = osMinVer;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getChangeLog() {
		return changeLog;
	}

	public void setChangeLog(String changeLog) {
		this.changeLog = changeLog;
	}
	
	public URI getIconUri() {
		return iconUri;
	}

	public void setIconUri(URI iconUri) {
		this.iconUri = iconUri;
	}

	public URI getBannerUri() {
		return bannerUri;
	}

	public void setBannerUri(URI bannerUri) {
		this.bannerUri = bannerUri;
	}

	public List<URI> getScreenshotsUri() {
		return screenshotsUri;
	}

	public void setScreenshotsUri(List<URI> screenshotsUri) {
		this.screenshotsUri = screenshotsUri;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}

	public String getOriginalAppId() {
		return originalAppId;
	}

	public void setOriginalAppId(String originalAppId) {
		this.originalAppId = originalAppId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(String cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public String getOriginalVersion() {
		return originalVersion;
	}

	public void setOriginalVersion(String originalVersion) {
		this.originalVersion = originalVersion;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getOriginalTypeId() {
		return originalTypeId;
	}

	public void setOriginalTypeId(String originalTypeId) {
		this.originalTypeId = originalTypeId;
	}
}

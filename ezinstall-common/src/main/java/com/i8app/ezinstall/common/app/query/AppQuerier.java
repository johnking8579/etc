package com.i8app.ezinstall.common.app.query;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;

/**
 * <pre>
 * app查询器,包含了app的所有通用查询
 * 调用方式:spring配置文件中
 * {@code
 * <bean id="appQuerier" class="com.i8app.ezinstall.common.app.query.AppQuerierFactory">
 *	<property name="dataSource" ref="dataSource"/>
 *	<!-- 1本机直连第三方服务器, 2通过接口服务器连接第三方服务器 -->
 *	<property name="remoteType" value="1"/>
 *	<property name="connectorUrl" value="localhost:8080/connector"/>
 *	<property name="fileServerUrl" value="http://192.168.1.127:7080/up/"/>
 * </bean>
 * }
 * </pre>
 * @author JingYing 2013-5-16
 */
public interface AppQuerier extends Constants {
	
	/**
	 * 分页查询包下的安装包列表,不对app去重. 不显示软件标签
	 * 查询结果中,安装包的属性(os,cpid,filesize等)都准确.且discriminator值 =2
	 * @param areaParam 区域信息, 用于查找该区域对应的软件标签
	 * @param pkgId 软件包的id
	 * @param os 为null时查所有操作系统的软件
	 * @param offset 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则offset为X
	 * @param pageSize 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则pageSize为Y
	 * @return  
	 */
	Pager<AppDTO> findInstallPackByPkg(int pkgId, Os os, int offset, int pageSize);

	/**
	 * 分页查询包下的安装包列表,不对app去重. 显示区域定制的标签,需要传areaParam信息.
	 * 查询结果中,安装包的属性(os,cpid,filesize等)都准确.且discriminator值 =2
	 * @param areaParam 区域信息, 用于查找该区域对应的软件标签
	 * @param pkgId 软件包的id
	 * @param os 为null时查所有操作系统的软件
	 * @param offset 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则offset为X
	 * @param pageSize 分页参数, 例"SELECT * FROM TABLE LIMIT X, Y", 则pageSize为Y
	 * @return  
	 */
	Pager<AppDTO> findInstallPackByPkg(AreaParam areaParam, int pkgId, Os os, int offset, int pageSize);
	
	/**
	 * 全量查询, 包括以下条件,(版本适配, 资源类型,软件名关键字,拼音首字母, 过滤区域定制, 取特定CP商)<br>
	 * 该函数查询的主表为appInstallPack, 不滤重APP, 而且所有appInstallPack相关的字段均完整有效<br>
	 * 
	 * 根据返回的packUuid, 再调用getPackUrl获取下载地址<br><br>
	 * 
	 * 注:本方法开启了查询缓存<br><br>
	 * @param areaParam	必须带有区域信息, 防止出现其它区域专属软件
	 * @param os 为null时查所有操作系统的软件
	 * @param appParam app的查询参数
	 * @param offset 分页参数, 例( LIMIT X, Y), 则offset为X
	 * @param pageSize 分页参数, 例( LIMIT X, Y), 则pageSize为Y
	 * @return
	 */
	Pager<AppDTO> queryPack(AreaParam areaParam, Os os, AppParam appParam,
			int offset, int pageSize);

	
	
	/**
	 * 全量查询,并滤重APP. 包括以下条件,(版本适配, 资源类型,软件名关键字,拼音首字母, 过滤区域定制, 取特定CP商)<br>
	 * 根据返回的appDTO.uuid, 调用confirmPack获取安装包ID, 再调用getPackUrl获取下载地址<br><br>
	 * 该函数查询速度慢, 推荐使用search(AreaParam areaParam, Os os, AppParam appParam,int offset, int pageSize)
	 * 
	 * 注:本方法开启了查询缓存<br><br>
	 * 
	 * @param areaParam 必须带有区域信息, 防止出现其它区域专属软件
	 * @param os 为null时查所有操作系统的软件
	 * @param appParam app的查询参数
	 * @param offset 分页参数, 例( LIMIT X, Y), 则offset为X
	 * @param pageSize 分页参数, 例( LIMIT X, Y), 则pageSize为Y
	 * @return
	 */
	Pager<AppDTO> query(AreaParam areaParam, Os os, AppParam appParam, 
			int offset, int pageSize);
	
	/**
	 * 全量查询,并滤重APP. 包括以下条件,(版本适配, 资源类型,软件名关键字,拼音首字母, 过滤区域定制, 取特定CP商)<br>
	 * 根据返回的appDTO.uuid, 调用confirmPack获取安装包ID, 再调用getPackUrl获取下载地址<br><br>
	 * 注:本方法开启了查询缓存<br><br>
	 * 
	 * 该方法的查询速度比query快, 但结果字段有限制, 只能查到app的字段, 查不到pack的字段.
	 * 当用户根据名称搜索软件时, 推荐使用本方法.
	 * 
	 * @param areaParam 必须带有区域信息, 防止出现其它区域专属软件
	 * @param os 为null时查所有操作系统的软件
	 * @param appParam app的查询参数
	 * @param offset 分页参数, 例( LIMIT X, Y), 则offset为X
	 * @param pageSize 分页参数, 例( LIMIT X, Y), 则pageSize为Y
	 * @return
	 */
	Pager<AppDTO> search(AreaParam areaParam, Os os, AppParam appParam, 
			int offset, int pageSize);

	/**
	 * 查看单个软件的详细信息.包括截图.  以app信息为主.
	 * @param appId
	 * @param Os 不能为空, 防止出现不适配的软件包的信息(例如包大小,系统,更新日期,cp商)
	 * @return appDTO中, discriminator为1
	 */
	AppDTO findSingle(String appId, Os os);
	
	/**
	 * 根据安装包ID, 查询单个软件的详细信息.包括截图 
	 * @param packUuid
	 * @return appDTO中, discriminator为2
	 */
	AppDTO findSinglePack(String packUuid);
	
	/**
	 * 根据APPID, OS, CPID, 确定唯一安装包
	 * @param appId APP的ID
	 * @param os 操作系统, 不能为NULL
	 * @param cpId CP商ID, 可以为NULL
	 * @return 唯一安装包的ID, 为null时代表未找到符合条件的安装包
	 */
	String confirmPack(String appId, Os os, String cpId);

	/**
	 * 查询安装包的下载地址
	 * @param packUuid 安装包ID
	 * @return 该安装包的完整下载地址
	 * @throws CpServerAccessException 访问CP商服务器时异常
	 * @throws IllegalArgumentException 未找到相关安装包
	 */
	String getPackUrl(String packUuid) throws CpServerAccessException;
	
	 /** 根据appId查询该ios软件对应的itunes上的itemid.
	 * @param appId
	 * @return 可能为null,表示无法获取该软件的itemid
	 */
	String findItunesId1(String appId);
	
	/**
	 * 根据安装包id, 查询该ios软件对应的itunes上的itemid.
	 * @param packUuid
	 * @return 可能为null,表示无法获取该软件的itemid
	 */
	String findItunesId2(String packUuid);
	
}

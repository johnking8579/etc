package com.i8app.ezinstall.common.app.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.domain.Pkg;

@Component
public class PkgDao extends BaseDao{

	public List<Pkg> findByGroup(int pkgGroupId) {
		return sqlMapClientTemplate.queryForList("Pkg.byGroupId", pkgGroupId);
	}

}

package com.i8app.ezinstall.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 根据spring文件生成IOC容器
 * @author JingYing 2013-5-25
 *
 */
public class SpringIOC {
	
	private static ApplicationContext context = null;
	
	public synchronized static ApplicationContext getIOC()	{
		if(context == null)	{
			context = new ClassPathXmlApplicationContext(
			"classpath:com/i8app/ezinstall/common/applicationContext.xml");
		}
		return context;
	}

}

package com.i8app.ezinstall.common.app.assembly;

import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.SpringIOC;
import com.i8app.ezinstall.common.app.Constants;

/**
 * appAssemblier的工厂类
 * 
 * @author JingYing 2013-5-9
 */
public class AppAssemblierFactory implements FactoryBean<AppAssemblier>	{

	@Override
	public AppAssemblier getObject() throws Exception {
		if(ClientParamHolder.getFileServerDir() == null 
			|| ClientParamHolder.getFileServerUrl() == null
			|| ClientParamHolder.getDataSource() == null
			|| ClientParamHolder.getCpMapToAssembly() == null)	{
			throw new IllegalArgumentException("需要在appAssemblierFactory中设置fileServerDir, " +
					"fileServerUrl, wostoreFileServerDir, dataSource, cpMapToAssembly");
		}
		
		return SpringIOC.getIOC().getBean(AppAssemblier.class);
	}

	@Override
	public Class<?> getObjectType() {
		return AppAssemblier.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
	
	/**
	 * softinfo和gameinfo的软件包和图片的下载路径头地址
	 * @param fileServerUrl
	 */
	public void setFileServerUrl(String fileServerUrl)	{
		ClientParamHolder.setFileServerUrl(fileServerUrl);
	}
	
	/**
	 * 从CP商APP表中下载的图片存在哪个fileServer中
	 * @param fileServerDir
	 */
	public void setFileServerDir(String fileServerDir)	{
		ClientParamHolder.setFileServerDir(fileServerDir);
	}
	
	/**
	 * APP汇集程序操作的数据库
	 * @param dataSource
	 */
	public void setDataSource(DataSource dataSource) {
		ClientParamHolder.setDataSource(dataSource);
	}
	
	public void setCpMapToAssembly(Map<String, String> map)	{
		for(Entry<String, String> e : map.entrySet())	{
			if(!Constants.MODE_LOCAL.equals(e.getValue()) && !Constants.MODE_REMOTE.equals(e.getValue()))	{
				throw new IllegalArgumentException("不识别的模式:" + e.getValue());
			}
		}
		ClientParamHolder.setCpMapToAssembly(map);
	}
	
	public void setRemoteType(int remoteType)	{
		ClientParamHolder.setRemoteType(remoteType);
	}
}

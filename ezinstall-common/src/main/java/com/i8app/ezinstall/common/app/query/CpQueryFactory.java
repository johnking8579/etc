package com.i8app.ezinstall.common.app.query;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.assembly.cp.AxonAdapteeImpl;
import com.i8app.ezinstall.common.app.assembly.cp.I8appAdapteeImpl;
import com.i8app.ezinstall.common.app.assembly.cp.WostoreAdapteeImpl;

/**
 * cpQuery的工厂类.
 * 
 * @author JingYing 2013-5-16
 */
@Component
public class CpQueryFactory implements Constants{
	
	@Resource
	private AxonAdapteeImpl axonAdapteeImpl;
	@Resource
	private WostoreAdapteeImpl wostoreAdapteeImpl;
	@Resource
	private I8appAdapteeImpl i8appAdapteeImpl;
	@Resource
	private I8SoftCpQueryImpl i8SoftCpQueryImpl; 
	@Resource
	private I8GameCpQueryImpl i8GameCpQueryImpl; 
	
	public CpQuery factory(String cpId)	{
		if(CP_AXON.equalsIgnoreCase(cpId))	{
			return axonAdapteeImpl;
		} else if(CP_WOSTORE.equalsIgnoreCase(cpId))	{
			return wostoreAdapteeImpl;
		} else if(CP_I8APP.equalsIgnoreCase(cpId))	{
			return i8appAdapteeImpl;
		} else if(CP_I8SOFT.equalsIgnoreCase(cpId))	{
			return i8SoftCpQueryImpl;
		} else if(CP_I8GAME.equalsIgnoreCase(cpId))	{
			return i8GameCpQueryImpl;
		}
		throw new IllegalArgumentException("不支持的cpId:" + cpId);
	}

}

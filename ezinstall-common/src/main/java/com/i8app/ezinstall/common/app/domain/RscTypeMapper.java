package com.i8app.ezinstall.common.app.domain;

public class RscTypeMapper {
	private Integer id;
	private String cpId, cpAppTypeId, rscTypeId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getCpAppTypeId() {
		return cpAppTypeId;
	}

	public void setCpAppTypeId(String cpAppTypeId) {
		this.cpAppTypeId = cpAppTypeId;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

}

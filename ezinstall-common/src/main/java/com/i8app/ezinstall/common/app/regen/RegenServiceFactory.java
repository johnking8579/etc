package com.i8app.ezinstall.common.app.regen;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.SpringIOC;

public class RegenServiceFactory implements FactoryBean<RegenService> {

	@Override
	public RegenService getObject() throws Exception {
		return SpringIOC.getIOC().getBean(RegenService.class);
	}

	@Override
	public Class<?> getObjectType() {
		return RegenService.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
	
	public void setDataSource(DataSource dataSource) {
		ClientParamHolder.setDataSource(dataSource);
	}

}

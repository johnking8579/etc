package com.i8app.ezinstall.common.app.assembly.process;

import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.assembly.AssemblyReport;

public interface OsProcessor{
	
	/**
	 * 
	 * @param appMeta
	 * @param adaptee
	 * @return 汇集报告
	 */
	AssemblyReport process(AppMeta appMeta, AppAdaptee adaptee);

}

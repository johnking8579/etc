package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.query.Os;

@Component
public class AppInstallPackDao extends BaseDao 	{
	
	public AppInstallPack findById(String uuid) {
		return (AppInstallPack)sqlMapClientTemplate.queryForObject("AppInstallPack.byId", uuid);
	}
	
	public String insert(AppInstallPack pack) {
		sqlMapClientTemplate.insert("AppInstallPack.insert", pack);
		return pack.getUuid();
	}
	
	public void updateStatus(String uuid, int status) {
		Map map = new HashMap();
		map.put("uuid", uuid);
		map.put("status", status);
		sqlMapClientTemplate.update("AppInstallPack.updateStatus", map);
	}

	public void update(AppInstallPack appInstallPack) {
		sqlMapClientTemplate.update("AppInstallPack.update", appInstallPack);
	}
	
	/**
	 * 查出所有, 不过滤status=0的数据
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	public Pager<AppInstallPack> findAll(int offset, int pageSize)	{
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<AppInstallPack> list = sqlMapClientTemplate.queryForList("AppInstallPack.findAll", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("AppInstallPack.findAll-count", map);
		return new Pager<AppInstallPack>(list, count);
	}
	
	/**
	 * 根据appid+os查询所有记录.
	 * @param appId
	 * @param os
	 * @return
	 */
	public List<AppInstallPack> findByAppIdAndOs(String appId, String os) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("os", os);
		map.put("appId", appId);
		return sqlMapClientTemplate.queryForList("AppInstallPack.byAppIdAndOs", map);
	}
	
	public List<AppInstallPack> findByAppId(String appId, Os os, String cpId)	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("os", os);
		map.put("cpId", cpId);
		map.put("appId", appId);
		return sqlMapClientTemplate.queryForList("AppInstallPack.byAppId", map);
	}
	
	/**
	 * 根据cpId, originalPackId, os确定一条唯一记录
	 * @param cpId
	 * @param originalPackId
	 * @param os
	 * @return
	 */
	public AppInstallPack find(String cpId, String originalPackId, String os) {
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("originalPackId", originalPackId);
		map.put("os", os);
		return (AppInstallPack)sqlMapClientTemplate.queryForObject("AppInstallPack.byCpIdAndOriginalPackIdAndOs", map);
	}
	
	/**
	 * 根据cpId, originalPackId, os确定一条唯一记录
	 * @param cpId
	 * @param originalPackId
	 * @param os
	 * @return
	 */
	public AppInstallPack findInNewTable(String cpId, String originalPackId, String os) {
		Map map = new HashMap();
		map.put("cpId", cpId);
		map.put("originalPackId", originalPackId);
		map.put("os", os);
		return (AppInstallPack)sqlMapClientTemplate.queryForObject("AppInstallPack.confirmInNewTable", map);
	}

	public List<AppInstallPack> findByApkPkgName(String apkPkgName) {
		return sqlMapClientTemplate.queryForList("AppInstallPack.byApkPkgName", apkPkgName);
	}

	public List<AppInstallPack> findByIpaBundleId(String ipaBundleId) {
		return sqlMapClientTemplate.queryForList("AppInstallPack.byIpaBundleId", ipaBundleId);
	}

}

package com.i8app.ezinstall.common.app.query.dto;

import java.util.ArrayList;
import java.util.List;

import com.i8app.ezinstall.common.ClientParamHolder;

/**
 * 以app信息为主,包含部分appInstallPack信息.
 * 
 * @author JingYing 2013-6-21
 */
public class AppDTO {

	private String uuid, name, pinyin, developer, info, infoHtml, markerType, markerTypeName,
					originalAppId, cpId;
	private int downCount, installCount;
	private String cpUpdateTime, rscTypeId, rscTypeName, rscTypePid;
	private String packUuid, packCpId, packAppVer, packOsMinVer,
			packCpUpdateTime, originalPackId, cpName;	//cpName指appInstallPack对应的cp, 不是指app对应的cp
	
	/**
	 * discriminator: 为1或null时,表示不指定下载appInstallPack, 为2时表示指定下载packUuid对应的appInstallPack
	 */
	private Integer discriminator;
	private Float point;
	private String icon, pic1, pic2, pic3, pic4, banner;
	
	/**
	 * app表中加入os,fileSize字段后, 为保持兼容性,仍保留packOs, packFileSize. 前者取app.os, 前者取app.fileSize
	 */
	private String packOs, packFileSize, bundleId, versionCode;
	
	
	/* =======================================================================================  */


	public String getIcon()	{
		return icon;
	}
	
	/**
	 * 拼接fileServer.url拼接至icon半截路径前
	 * @return
	 */
	public String getIconUrl()	{
		if(icon == null)	return null;
		String s = ClientParamHolder.getFileServerUrl() + icon;
		s = s.replaceAll("^http://", "").replaceAll("//", "/");
		return "http://" + s;
	}
	
	public String getBanner() {
		return banner;
	}

	/**
	 * 拼接fileserver.url至banner,得到完整url
	 * @return
	 */
	public String getBannerUrl() {
		return banner == null ? null : ClientParamHolder.getFileServerUrl() + banner;
	}
	
	
	/**
	 * 获取pic1完整路径, 也可以自己把fileServer的路径和getPic1()拼接起来获得完整地址
	 * @return
	 */
	public String getPic1Url()	{
		return pic1 == null ? null : ClientParamHolder.getFileServerUrl() + pic1;
	}
	
	/**
	 * 获取pic2完整路径, 也可以自己把fileServer的路径和getPic2()拼接起来获得完整地址
	 * @return
	 */
	public String getPic2Url()	{
		return pic2 == null ? null : ClientParamHolder.getFileServerUrl() + pic2;
	}
	
	/**
	 * 获取pic3完整路径, 也可以自己把fileServer的路径和getPic3()拼接起来获得完整地址
	 * @return
	 */
	public String getPic3Url()	{
		return pic3 == null ? null : ClientParamHolder.getFileServerUrl() + pic3;
	}
	
	/**
	 * 获取pic4完整路径, 也可以自己把fileServer的路径和getPic4()拼接起来获得完整地址
	 * @return
	 */
	public String getPic4Url()	{
		return pic4 == null ? null : ClientParamHolder.getFileServerUrl() + pic4;
	}
	
	/**
	 * 获取完整截图路径的便捷方法. 
	 * 把pic1,pic2,pic3,pic4放至pics集合中, 并拼接fileServer地址至pic路径前
	 * @return
	 */
	public List<String> getPicsUrl() {
		List<String> list = new ArrayList<String>();
		if(pic1 != null)	list.add(ClientParamHolder.getFileServerUrl() + pic1);
		if(pic2 != null)	list.add(ClientParamHolder.getFileServerUrl() + pic2);
		if(pic3 != null)	list.add(ClientParamHolder.getFileServerUrl() + pic3);
		if(pic4 != null)	list.add(ClientParamHolder.getFileServerUrl() + pic4);
		return list;
	}
	
	public List<String> getPics()	{
		List<String> list = new ArrayList<String>();
		if(pic1 != null)	list.add(pic1);
		if(pic2 != null)	list.add(pic2);
		if(pic3 != null)	list.add(pic3);
		if(pic4 != null)	list.add(pic4);
		return list;
	}
	
	/* =======================================================================================  */

	/**
	 * app的uuid
	 * @return
	 */
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * app名称
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * app的拼音首字母
	 * @return
	 */
	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	/**
	 * app的开发商
	 * @return
	 */
	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	/**
	 * app介绍
	 * @return
	 */
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfoHtml() {
		return infoHtml;
	}

	public void setInfoHtml(String infoHtml) {
		this.infoHtml = infoHtml;
	}

	public String getMarkerType() {
		return markerType;
	}

	public void setMarkerType(String markerType) {
		this.markerType = markerType;
	}

	public int getDownCount() {
		return downCount;
	}

	public void setDownCount(int downCount) {
		this.downCount = downCount;
	}

	public int getInstallCount() {
		return installCount;
	}

	public void setInstallCount(int installCount) {
		this.installCount = installCount;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCpUpdateTime() {
		return cpUpdateTime;
	}

	public void setCpUpdateTime(String cpUpdateTime) {
		this.cpUpdateTime = cpUpdateTime;
	}

	public String getRscTypeId() {
		return rscTypeId;
	}

	public void setRscTypeId(String rscTypeId) {
		this.rscTypeId = rscTypeId;
	}

	public String getRscTypeName() {
		return rscTypeName;
	}

	public void setRscTypeName(String rscTypeName) {
		this.rscTypeName = rscTypeName;
	}

	public String getRscTypePid() {
		return rscTypePid;
	}

	public void setRscTypePid(String rscTypePid) {
		this.rscTypePid = rscTypePid;
	}

	public String getPackCpId() {
		return packCpId;
	}

	public void setPackCpId(String packCpId) {
		this.packCpId = packCpId;
	}

	public String getPackAppVer() {
		return packAppVer;
	}

	public void setPackAppVer(String packAppVer) {
		this.packAppVer = packAppVer;
	}

	public String getPackOs() {
		return packOs;
	}

	public void setPackOs(String packOs) {
		this.packOs = packOs;
	}

	public String getPackOsMinVer() {
		return packOsMinVer;
	}

	public void setPackOsMinVer(String packOsMinVer) {
		this.packOsMinVer = packOsMinVer;
	}

	public String getPackFileSize() {
		return packFileSize;
	}

	public void setPackFileSize(String packFileSize) {
		this.packFileSize = packFileSize;
	}

	public String getPackCpUpdateTime() {
		return packCpUpdateTime;
	}

	public void setPackCpUpdateTime(String packCpUpdateTime) {
		this.packCpUpdateTime = packCpUpdateTime;
	}

//	public void setPics(List<String> pics) {
//		this.pics = pics;
//	}

	public String getOriginalAppId() {
		return originalAppId;
	}

	public void setOriginalAppId(String originalAppId) {
		this.originalAppId = originalAppId;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	
	public String getMarkerTypeName() {
		return markerTypeName;
	}

	public void setMarkerTypeName(String markerTypeName) {
		this.markerTypeName = markerTypeName;
	}
	
	public String getCpName() {
		return cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}
	
	public String getPackUuid() {
		return packUuid;
	}

	public void setPackUuid(String packUuid) {
		this.packUuid = packUuid;
	}

	public Integer getDiscriminator() {
		return discriminator;
	}

	public void setDiscriminator(Integer discriminator) {
		this.discriminator = discriminator;
	}

	public Float getPoint() {
		return point;
	}

	public void setPoint(Float point) {
		this.point = point;
	}
	
	public String getOriginalPackId() {
		return originalPackId;
	}

	public void setOriginalPackId(String originalPackId) {
		this.originalPackId = originalPackId;
	}
	
	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}

	public String getPic4() {
		return pic4;
	}

	public void setPic4(String pic4) {
		this.pic4 = pic4;
	}
	
	public String getBundleId() {
		return bundleId;
	}

	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}
	
	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	@Override
	public String toString() {
		return "AppDTO [uuid=" + uuid + ", name=" + name + ", pinyin=" + pinyin
				+ ", developer=" + developer + ", markerType=" + markerType + ", markerTypeName="
				+ markerTypeName + ", originalAppId=" + originalAppId
				+ ", cpId=" + cpId + ", downCount=" + downCount
				+ ", installCount=" + installCount + ", cpUpdateTime="
				+ cpUpdateTime + ", rscTypeId=" + rscTypeId + ", rscTypeName="
				+ rscTypeName + ", rscTypePid=" + rscTypePid + ", packUuid="
				+ packUuid + ", packCpId=" + packCpId + ", packAppVer="
				+ packAppVer + ", packOsMinVer=" + packOsMinVer
				+ ", packCpUpdateTime=" + packCpUpdateTime
				+ ", originalPackId=" + originalPackId + ", cpName=" + cpName
				+ ", discriminator=" + discriminator + ", point=" + point
				+ ", icon=" + icon + ", pic1=" + pic1
				+ ", pic2=" + pic2 + ", pic3=" + pic3 + ", pic4=" + pic4
				+ ", packOs=" + packOs + ", packFileSize=" + packFileSize
				+ ", bundleId=" + bundleId + "]";
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}

}

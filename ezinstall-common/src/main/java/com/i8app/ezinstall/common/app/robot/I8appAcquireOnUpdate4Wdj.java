package com.i8app.ezinstall.common.app.robot;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.i8app.ezinstall.common.app.AbstractI8appAcquireOnUpdate;
import com.i8app.ezinstall.common.wsclient.WdjJsonParser;

public class I8appAcquireOnUpdate4Wdj extends AbstractI8appAcquireOnUpdate{
	
	private WdjJsonParser wdj;
	public I8appAcquireOnUpdate4Wdj(WdjJsonParser wdj)	{
		this.wdj = wdj;
	}

	@Override
	public InputStream getBanner() {
		if(wdj.getBanner() != null)	{
			try {
				return new URL(wdj.getBanner()).openStream();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getInfo() {
		return wdj.getDescription();
	}

	@Override
	public String getDeveloper() {
		return wdj.getDeveloper();
	}

	@Override
	public String getChangeLog() {
		return wdj.getChangelog();
	}

}

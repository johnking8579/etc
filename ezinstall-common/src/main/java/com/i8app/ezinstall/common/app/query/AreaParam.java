package com.i8app.ezinstall.common.app.query;

import java.util.HashMap;
import java.util.Map;

import com.i8app.ezinstall.common.app.Constants;

/**
 * 用户所在的区域信息. 包括以下参数:运营商id,省id,市id
 * 
 * @author JingYing 2013-5-20
 */
public class AreaParam implements Constants	{
	
	private String deptId;
	private int provId, cityId;

	public AreaParam() {	}

	/**
	 * 
	 * @param deptId 运营商id 取值AreaParam.DEPTID_***
	 * @param provId 省id
	 * @param cityId 市id
	 */
	public AreaParam(String deptId, int provId, int cityId) {
		this.deptId = deptId;
		this.provId = provId;
		this.cityId = cityId;
	}
	
	public Map<String, Object> toParamMap()	{
		checkArg();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("dept1", "0");
		map.put("prov1", 1000);	//改成1000
		map.put("city1", 0);
		map.put("dept2", deptId);
		map.put("prov2", 1000);	//改成1000
		map.put("city2", 0);
		map.put("dept3", deptId);
		map.put("prov3", provId);
		map.put("city3", 0);
		map.put("dept4", deptId);
		map.put("prov4", provId);
		map.put("city4", cityId);
		return map;
	}
	
	private void checkArg()	{
		
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public int getProvId() {
		return provId;
	}

	public void setProvId(int provId) {
		this.provId = provId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

}

package com.i8app.ezinstall.common.app.assembly.cp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.HttpClientUtil;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.assembly.AppAdaptee;
import com.i8app.ezinstall.common.app.assembly.AppMeta;
import com.i8app.ezinstall.common.app.dao.RscTypeMapperDao;
import com.i8app.ezinstall.common.app.dao.WostoreDao;
import com.i8app.ezinstall.common.app.dao.WostorePackDao;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.domain.Wostore;
import com.i8app.ezinstall.common.app.domain.WostorePack;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkReader2;

@Component
public class WostoreAdapteeImpl implements AppAdaptee<WostorePack> {
	private static final Logger logger = Logger.getLogger(WostoreAdapteeImpl.class);
	public static Map<String, String> typeMap;

	@Resource
	private WostoreDao wostoreDao;
	@Resource
	private WostorePackDao wostorePackDao;
	@Resource
	private RscTypeMapperDao rscTypeMapperDao;
	private String packAccessMode;
	private ApkReader2 apkReader2 = new ApkReader2();
	
	/**
	 * 查找需要导入的软件. 优先级>0,且未被删除
	 */
	@Override
	public Pager<WostorePack> findValidApp(int offset, int pageSize) {
		return wostorePackDao.findValidApp(offset, pageSize);//只导android/android,iphone, 不导入symbian
	}
	
	@Override
	public AppMeta toAppMeta(WostorePack pack) {
		Wostore w = pack.getWostore();
		AppMeta meta = new AppMeta();
		
		if(MODE_LOCAL.equals(packAccessMode))	{
			if(w.getIconPath() != null)	{
				meta.setIconUri(new File(FILESERVER_DIR + w.getIconPath()).toURI());
			}
			
			List<URI> ssUris = new ArrayList<URI>();
			if(w.getPic1Path() != null && w.getPic1Path().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + w.getPic1Path()).toURI());
			if(w.getPic2Path() != null && w.getPic2Path().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + w.getPic2Path()).toURI());
			if(w.getPic3Path() != null && w.getPic3Path().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + w.getPic3Path()).toURI());
			if(w.getPic4Path() != null && w.getPic4Path().length() > 0)		
				ssUris.add(new File(FILESERVER_DIR + w.getPic4Path()).toURI());
			meta.setScreenshotsUri(ssUris);
		} else if(MODE_REMOTE.equals(packAccessMode))	{	//远程下载模式下,直接取icon,pic1,pic2...
			if(w.getIcon() != null)	{
				meta.setIconUri(new File(w.getIcon()).toURI());
			}
			
			List<URI> ssUris = new ArrayList<URI>();
			if(w.getPic1() != null && w.getPic1().length() > 0)
				try {
					ssUris.add(new URI(w.getPic1()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(w.getPic2() != null && w.getPic2().length() > 0)
				try {
					ssUris.add(new URI(w.getPic2()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(w.getPic3() != null && w.getPic3().length() > 0)
				try {
					ssUris.add(new URI(w.getPic3()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			if(w.getPic4() != null && w.getPic4().length() > 0)
				try {
					ssUris.add(new URI(w.getPic4()));
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			meta.setScreenshotsUri(ssUris);
		} else 	{
			throw new UnsupportedOperationException("不支持的访问模式:" + packAccessMode);
		}
		
		meta.setBannerUri(null);
		meta.setChangeLog(null);
		meta.setCpId(CP_WOSTORE);
		try {
			Date date = new SimpleDateFormat("yyyyMMddHHmmss").parse(pack.getUpdateTime());
			meta.setCpUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
		} catch (ParseException e) {
			meta.setCpUpdateTime(pack.getUpdateTime());
		}
		meta.setDeveloper(w.getDeveloper());
		meta.setInfo(w.getInfo());
		meta.setOriginalAppId(w.getId());
		meta.setOriginalName(w.getName());
		meta.setOriginalPackId(pack.getId());
		meta.setOriginalTypeId(w.getTypeId());
		meta.setOriginalVersion(pack.getVersion());
		meta.setOs(toOs(pack.getOs()));
		meta.setOsMinVer(null);
		meta.setRscTypeId(toRscTypeId(w.getTypeId()));
		meta.setStatus(w.getIsDel());
		return meta;
	}

	private String toOs(String os) {
		if("android,iphone".equals(os))	{	//wostore中有很多os为android,iphone的数据
			return ANDROID;
		} else if("android".equals(os))	{
			return ANDROID;
		} else if("iphone".equals(os))	{
			return IOS;
		} else if("symbian".equals(os))	{
			return SYMBIAN;
		} else if("symbianv3".equals(os))	{
			return SYMBIAN;
		} else if("wm".equals(os))	{
			return WM;
		} else	{
			return null;
		}
	}
	

	@Override
	public String findPackUrl(String originalPackId, String os) throws CpServerAccessException {
		WostorePack wp = wostorePackDao.findById(originalPackId);
		if(wp == null)	throw new RuntimeException("missing wostore id: " + originalPackId);
		return wp.getUrl();
	}

	/**
	 * 转换应用类型ID
	 * @param typeid
	 * @return
	 */
	private String toRscTypeId(String typeid)	{
		if(typeMap == null)	{
			typeMap = rscTypeMapperDao.listToMap(rscTypeMapperDao.findByCp(getCpId()));
		}
		
		return typeMap.get(typeid) == null ? "101" : typeMap.get(typeid);	//无法匹配时, 默认101
	}

	@Override
	public boolean isPackUpdated(AppMeta meta, AppInstallPack existPack) {
		//沃商店的更新时间转换成yyyy-MM-dd HH:mm:ss的格式后, 再和数据库中的时间做比较
		if(meta.getCpUpdateTime().equals(existPack.getCpUpdateTime()))	{
			return false;
		} else	{
			return true;
		}
	}
	
	@Override
	public void setRscTypeMap(Map<String, String> rscTypeMap) {
		typeMap = rscTypeMap;
	}

	@Override
	public List<String> findScreenshots(String originalAppId ) {
		Wostore w = wostoreDao.findById(originalAppId);
		List<String> list = new ArrayList<String>();
		if(w == null){
			logger.error("missing wostore id: " + originalAppId);
//			throw new RuntimeException("missing wostore id: " + originalAppId);
		}
		if(w != null)	{
			String s1 = w.getPic1();
			if(s1!=null && s1.length()>0)
				list.add(s1);
			String s2 = w.getPic2();
			if(s2!=null && s2.length()>0)
				list.add(s2);
			String s3 = w.getPic3();
			if(s3!=null && s3.length()>0)
				list.add(s3);
			String s4 = w.getPic4();
			if(s4!=null && s4.length()>0)
				list.add(s4);
		}
		return list;
	}

	@Override
	public String getCpId() {
		return CP_WOSTORE;
	}
	
	public String getPackAccessMode() {
		return packAccessMode;
	}
	
	public void setPackAccessMode(String packAccessMode) {
		this.packAccessMode = packAccessMode;
	}
	
	@Override
	public Object readPack(String originalPackId, String os)
			throws CpServerAccessException, PackReadException, FileNotFoundException {
		if(MODE_LOCAL.equals(getPackAccessMode()))	{			//本地读取沃商店安装包
			if(ANDROID.equalsIgnoreCase(os)){
				WostorePack wp = wostorePackDao.findById(originalPackId);
				File f = new File(FILESERVER_DIR + wp.getFilePath());
				if(f.isFile() && f.exists())	{
					return apkReader2.read(f, true);
				} else	{
					throw new FileNotFoundException("未找到相关磁盘文件:" + originalPackId + ":" + f.getAbsolutePath());
				}
			} else	{
				throw new UnsupportedOperationException("CP沃商店不支持该操作系统:" + os);
			}
			
		} else if(MODE_REMOTE.equals(getPackAccessMode()))	{			//远程下载沃商店安装包
			if(ANDROID.equalsIgnoreCase(os)){
				String url = wostorePackDao.findById(originalPackId).getUrl();	//使用用wostore_pack的原始url,不使用filepath
				File tmp = null;
				try {
					logger.info("正在远程下载沃商店资源,id=" + originalPackId + ", url=" + url);
					tmp = HttpClientUtil.downloadToTemp(url, ".apk", 60 * 15);
					return apkReader2.read(tmp, true);
				} catch (IOException e) {
					throw new CpServerAccessException(e.getMessage(), e);
				} finally {
					if(tmp != null)	{
						tmp.delete();
					}
				}
			} else	{
				throw new UnsupportedOperationException("CP沃商店不支持该操作系统:" + os);
			}
			
		} else	{
			throw new RuntimeException("不识别的mode:" + packAccessMode);
		}
	}

	@Override
	public String findIconUrl(String originalAppId) {
		Wostore w = wostoreDao.findById(originalAppId);
		return w == null ? null : w.getIcon();
	}
	
	@Override
	public List findByOriginalPackIds(List<String> originalPackIds) {
		return wostorePackDao.findByIds(originalPackIds);
	}

}

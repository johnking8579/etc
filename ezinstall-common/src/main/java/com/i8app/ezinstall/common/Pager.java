package com.i8app.ezinstall.common;

import java.util.List;


/**
 * 简单分页模型
 * @author JingYing 2013-5-25
 *
 * @param <T>
 */
public class Pager <T> {
	
	private int totalCount;
	
	private List<T> list;
	
	public Pager() {}
	
	public Pager(List<T> list, int totalCount)	{
		this.list = list;
		this.totalCount = totalCount;
	}
	
	/****************************************************
	 * 以下是访问器
	 ************************************************/
	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
}

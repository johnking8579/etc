package com.i8app.ezinstall.common.app.assembly.cp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.i8app.ezinstall.common.Util;

/**
 * 使用httpclient, 防止并发数过高导致too many open files.
 * 
 * @author JingYing 2014-1-7
 */
public class AxonConnection {

	private static Logger log = Logger.getLogger(AxonConnection.class);
	
	private static final String PKEY = "39",
		BASE_URL = "http://open.17wo.cn/OpenApi/distribute",
		//BASE_URL = 	"http://112.96.28.198/OpenApi",
		SECRET_KEY = "510b77de4e7acb201812afc0";
	
	private static CloseableHttpClient defaultHttpClient ;
	private static PoolingHttpClientConnectionManager defaultMng; 
	
	static	{
		defaultMng = new PoolingHttpClientConnectionManager();//http连接池,默认向每个主机头只并发两个端口
		defaultMng.setDefaultMaxPerRoute(1000);	//控制并发数
	    defaultHttpClient = HttpClients.custom().setConnectionManager(defaultMng).build();
	}
	
	/**
	 * 
	 * 查询下载地址. 该接口要求访问迅速, 所以要使用独立的连接池设置
	 * 
	 * @param phoneNum
	 * @return
	 * @throws AxonException
	 */
	public String getDownloadUrl(String contentId, String phoneNum) throws AxonException {
		String tm = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String code = Util.toMD5(tm + PKEY + phoneNum + contentId + SECRET_KEY);
		String url = String.format("%s/wo/content/getdownloadurl/%s?pKey=%s&tm=%s&code=%s&phoneNum=%s",
						BASE_URL, contentId, PKEY, tm, code, phoneNum);

		HttpGet get = new HttpGet(url);
		get.setConfig(RequestConfig.custom()
	    		.setConnectionRequestTimeout(30000)//从连接池等待连接的时间
	    		.setConnectTimeout(10000)
	    		.setSocketTimeout(1000 * 10)	//超时时间不要太长
	    		.build());
		long start = System.currentTimeMillis();
		CloseableHttpResponse resp = null;
		try {
			resp = defaultHttpClient.execute(get);
			InputStream is = resp.getEntity().getContent();
			JsonObject json = new JsonParser().parse(new InputStreamReader(is, "utf-8")).getAsJsonObject();
			int err = json.get("errcode").getAsInt();
			if (err == 0) {
				return json.get("resultData").getAsJsonObject().get("url").getAsString();
			} else {
				throw new AxonException("地址:" + url + "   安讯错误码:" + err + "    信息:" + json.get("msg").getAsString());
			}
		} catch (IOException e) {
			long seconds = (System.currentTimeMillis() - start) / 1000;
			throw new AxonException(url + "  用时(秒):" + seconds, e);
		} finally	{
			try {
				resp.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 获取下载地址url
	 * 
	 * @param contentID
	 * @param phoneNum
	 * @return
	 * @throws AxonException 
	 * @deprecated 容易导致too many open files
	 */
	public String getDownloadUrl2(String contentID, String phoneNum) throws AxonException {
		String tm = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String code = Util.toMD5(tm + PKEY + phoneNum + contentID + SECRET_KEY);
		String urlt = String.format("%s/wo/content/getdownloadurl/%s?pKey=%s&tm=%s&code=%s&phoneNum=%s", 
					BASE_URL, contentID, PKEY, tm, code, phoneNum);

		InputStream is = null;
		try {
			HttpURLConnection conn = (HttpURLConnection)new URL(urlt).openConnection();
			conn.setConnectTimeout(1000 * 15);		
			conn.setReadTimeout(1000 * 15);	//请求比较频繁, 超时要短		
			is = conn.getInputStream();
			JsonObject json = new JsonParser().parse(
					new InputStreamReader(is, "utf-8")).getAsJsonObject();
			if (json.get("resultData") != null) {
				if (json.get("resultData").getAsJsonObject().get("url") != null) {
					return json.get("resultData").getAsJsonObject().get("url").getAsString();
				}
			}
			throw new AxonException("解析json数据失败:" + json.toString());
		} catch (IOException e) {
			throw new AxonException(e);
		} finally	{
			Util.closeStream(is);
		}
	}
	
}

package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class AppAreaDao extends BaseDao	{

	public List findOldApp() {
		return (List)sqlMapClientTemplate.queryForList("AppArea.findOldApp");
	}

	public void updateAppId(Object id, String uuid) {
		Map map = new HashMap(2);
		map.put("id", id);
		map.put("uuid", uuid);
		sqlMapClientTemplate.update("AppArea.updateAppId", map);
	}

}

package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.I8Game;

@Component
public class I8GameDao extends BaseDao	{

	public Pager<I8Game> find(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<I8Game> list = sqlMapClientTemplate.queryForList("I8Game.findAll", map);
		int total = (Integer)sqlMapClientTemplate.queryForObject("I8Game.findAll-count");
		return new Pager<I8Game>(list, total);
	}

	public I8Game findById(int id) {
		return (I8Game)sqlMapClientTemplate.queryForObject("I8Game.byId", id);
	}

	public List findByIds(List<String> originalPackIds) {
		Map map = new HashMap();
		map.put("ids", originalPackIds);
		return sqlMapClientTemplate.queryForList("I8Game.byIds", map);
	}

	public List<I8Game> findByOs(int osType) {
		return sqlMapClientTemplate.queryForList("I8Game.byOs", osType);
	}

	
}

package com.i8app.ezinstall.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.ConnectionClosedException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

/**
 * 使用httpclient连接池下载
 * 
 * @author JingYing 2014-1-16
 */
public class HttpClientUtil {
	
	private static PoolingHttpClientConnectionManager defaultMng 
						= new PoolingHttpClientConnectionManager();
	private static CloseableHttpClient defaultHttpClient 
						= HttpClients.custom().setConnectionManager(defaultMng).build(); 
	
	/**
	 * url中下载至临时文件夹, 加入超时设置, 防止无限读取
	 * @param url
	 * @param suffix 后缀名, ".apk", ".ipa", ...
	 * @param readTimeoutSec 读超时时间,单位秒
	 * @return
	 * @throws IOException
	 */
	public static File downloadToTemp(String url, String suffix, Integer readTimeoutSec) throws IOException	{
		final HttpGet get = new HttpGet(url);
		get.setConfig(RequestConfig.custom()
	    		.setConnectionRequestTimeout(30000) //从连接池等待连接的时间
	    		.setConnectTimeout(10000)	//连接超时
	    		.setSocketTimeout(30000)	//每次socket通信超时时间	
	    		.build());
		
		if(readTimeoutSec != null)	{
			new Timer(true).schedule(new TimerTask() {
				@Override
				public void run() {
					if(get != null)
						get.abort();
				}
			}, readTimeoutSec * 1000);
		}
		CloseableHttpResponse resp = null;
		FileOutputStream fos = null;
		File tmp = null;
		try {
			resp = defaultHttpClient.execute(get);
			int status = resp.getStatusLine().getStatusCode();
			if (status >= 200 && status < 300) {
				tmp = File.createTempFile("tmpDl", suffix);
				fos = new FileOutputStream(tmp);
				try {
					resp.getEntity().writeTo(fos);	//httpclient有时候在这里报异常
				} catch (ConnectionClosedException e) {
					//e.printStackTrace();
				} 
				return tmp;
			} else	{
				throw new IOException(url + ", 返回码:" + status);
			}
		} catch(IOException e)	{
			if(tmp != null && tmp.isFile() && tmp.exists())	{
				tmp.delete();
			}
			throw e;
		} finally	{
			Util.closeStream(resp, fos);
		}
	}
	
	/**
	 * 下载至byte数组, 适合小文件
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static byte[] downloadToBytes(String url) throws IOException	{
		final HttpGet get = new HttpGet(url);
		get.setConfig(RequestConfig.custom()
	    		.setConnectionRequestTimeout(30000) //从连接池等待连接的时间
	    		.setConnectTimeout(10000)	//连接超时
	    		.setSocketTimeout(30000)	//每次socket通信超时时间	
	    		.build());
		
		CloseableHttpResponse resp = null;
		try {
			resp = defaultHttpClient.execute(get);
			int status = resp.getStatusLine().getStatusCode();
			if (status >= 200 && status < 300) {
				return EntityUtils.toByteArray(resp.getEntity());
			} else	{
				throw new IOException(url + ", 返回码:" + status);
			}
		} finally	{
			Util.closeStream(resp);
		}
	}
	
	
	
	public static void main(String[] args) {
//		String url = "http://3gdl2.wostore.cn/fileServer/woresource/resfile/qrcode/filepath/90002734/cnt/114297/1186358/1385364768627.apk";//200
//		String url = "http://sx.source.i8app.com/fileServer/soft/files/20130821/20130821092619498.apk";//404
//		String url = "http://wap.17wo.cn/downloadhb.jsp?cpd=3300001001201301290339112057";
		String url = "http://www.shaanig.com/";
		try {
			File tmp = HttpClientUtil.downloadToTemp(url, ".apk", 500);
			System.out.println(tmp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

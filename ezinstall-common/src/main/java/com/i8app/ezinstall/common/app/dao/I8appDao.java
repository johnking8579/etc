package com.i8app.ezinstall.common.app.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.domain.I8app;
import com.i8app.ezinstall.common.app.query.dto.AppDTO;

@Component
public class I8appDao extends BaseDao {

	public void insert(I8app i8app) {
		sqlMapClientTemplate.insert("I8app.insert", i8app);
	}
	
	public I8app findById(int id) {
		return (I8app)sqlMapClientTemplate.queryForObject("I8app.byId", id);
	}

	public List findByIds(List<String> ids) {
		Map map = new HashMap();
		map.put("ids", ids);
		return sqlMapClientTemplate.queryForList("I8app.byIds", map);
	}

	public Pager<I8app> find(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<I8app> list = sqlMapClientTemplate.queryForList("I8app.findAll", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("I8app.findAll-count");
		return new Pager<I8app>(list, count);
	}

	public Pager<I8app> findToImport(int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<I8app> list = sqlMapClientTemplate.queryForList("I8app.findToImport", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("I8app.findToImport-count");
		return new Pager<I8app>(list, count);
	}

	public List<I8app> findIsDel() {
		return sqlMapClientTemplate.queryForList("I8app.findIsDel");
	}
	
	public Pager<I8app> findByOs(String os, int offset, int pageSize) {
		Map map = new HashMap();
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		List<I8app> list = sqlMapClientTemplate.queryForList("I8app.findByOs", map);
		int count = (Integer)sqlMapClientTemplate.queryForObject("I8app.findByOs-count");
		return new Pager<I8app>(list, count);
	}
	
	public List<I8app> findByPkgname(String os, String pkgName, int isDel)	{
		Map map = new HashMap();
		map.put("os", os);
		map.put("pkgName", pkgName);
		map.put("isDel", isDel);
		return (List<I8app>)sqlMapClientTemplate.queryForObject("I8app.findByPkgname", map);
	}

	public I8app findByOsMemoRscType(String os, String memo, String rscTypeId) {
		Map map = new HashMap();
		map.put("os", os);
		map.put("memo", memo);
		map.put("rscTypeId", rscTypeId);
		return (I8app)sqlMapClientTemplate.queryForObject("I8app.findByOsMemoRscType", map);
	}

	public List<AppDTO> findMissingFromPack() {
		return sqlMapClientTemplate.queryForList("I8app.findMissingFromPack");
	}

	public List<Map<String, Object>> findOverdueFromPack() {
		return sqlMapClientTemplate.queryForList("I8app.findOverdueFromPack");
	}

	public void update(I8app i) {
		sqlMapClientTemplate.update("I8app.update", i);
	}

	public void updatePackStatusByOs123() {
		sqlMapClientTemplate.update("I8app.updatePackStatusByOs123");
	}
	public void updatePackStatusByOs456() {
		sqlMapClientTemplate.update("I8app.updatePackStatusByOs456");
	}

	/**
	 * 查找需要从豌豆荚更新的i8app资源
	 * @return
	 */
	public List<I8app> findNeedUpdateFromWandoujia() {
		return sqlMapClientTemplate.queryForList("I8app.findNeedUpdateFromWandoujia");
	}

}

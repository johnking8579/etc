package com.i8app.ezinstall.common.app.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.SqlMapClientTemplate;

import com.ibatis.sqlmap.client.SqlMapClient;

public class BaseDao	{
	
	protected SqlMapClientTemplate sqlMapClientTemplate;
	protected JdbcTemplate jdbcTemplate;
	
	@Resource
	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		sqlMapClientTemplate = new SqlMapClientTemplate(sqlMapClient);
	}
	
	@Resource
	public void setDataSource(DataSource dataSource)	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
}

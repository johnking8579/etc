package com.i8app.ezinstall.common.app.assembly;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.i8app.ezinstall.common.ClientParamHolder;
import com.i8app.ezinstall.common.Pager;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.common.app.query.CpQuery;
import com.i8app.ezinstall.packreader.PackReadException;
import com.i8app.ezinstall.packreader.apk2.ApkInfo;
import com.i8app.ezinstall.packreader.ipa.ItunesMetadata;

/**
 * CP商的APP适配器接口, AppAssemblier调用此接口
 * 
 * @author JingYing 2013-5-9
 * @param <T>
 */
public interface AppAdaptee<T> extends Constants, CpQuery	{
	
	public static final String 
		FILESERVER_URL 			= ClientParamHolder.getFileServerUrl(),
		FILESERVER_DIR 			= ClientParamHolder.getFileServerDir(); 
	
	/**
	 * 遍历cp商需要被导入的资源
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	Pager<T> findValidApp(int offset, int pageSize);
	
	/**
	 * 将CP商的单条记录转化成appMeta
	 * @param t
	 * @return
	 */
	AppMeta toAppMeta(T t);
	
	/**
	 * 读取安装包或从数据中获取安装包元信息
	 * @param originalPackId
	 * @param os
	 * @return 为android时,应当返回ApkInfo, 为ios时, 应当返回ItunesMetadata, 为其它操作系统时,应当返回Long(代表安装包大小)
	 * @throws CpServerAccessException
	 * @throws PackReadException
	 * @throws FileNotFoundException
	 */
	Object readPack(String originalPackId, String os)
		throws CpServerAccessException, PackReadException, FileNotFoundException;
	
	/**
	 * 该软件安装包是否更新了?
	 * 有的CP商的软件版本会更新, 通过版本号来判断是否更新.
	 * 但有的CP商的软件无版本号, 只能通过更新日期来判断
	 * @param existPack 数据库已有的appInstallPack记录
	 * @return 
	 */
	boolean isPackUpdated(AppMeta meta, AppInstallPack existPack);

	/**
	 * 设置rsctype的映射map. 
	 * 主要用于每次定时任务开始前, 重置rsctypemap, 可以更新rsctypeid
	 * @param rscTypeMap
	 */
	void setRscTypeMap(Map<String, String> rscTypeMap);
	
	/**
	 * cp商的ID
	 * @return
	 */
	String getCpId();

}

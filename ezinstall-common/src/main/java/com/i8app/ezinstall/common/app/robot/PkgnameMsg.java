package com.i8app.ezinstall.common.app.robot;

/**
 * 安装包包名消息
 * 
 * @author JingYing 2014-1-6
 */
public class PkgnameMsg {
	
	private String cpId, os, pkgname;
	
	public PkgnameMsg(String cpId, String os, String pkgname) {
		this.cpId = cpId;
		this.os = os;
		this.pkgname = pkgname;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getPkgname() {
		return pkgname;
	}

	public void setPkgname(String pkgname) {
		this.pkgname = pkgname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpId == null) ? 0 : cpId.hashCode());
		result = prime * result + ((os == null) ? 0 : os.hashCode());
		result = prime * result + ((pkgname == null) ? 0 : pkgname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PkgnameMsg other = (PkgnameMsg) obj;
		if (cpId == null) {
			if (other.cpId != null)
				return false;
		} else if (!cpId.equals(other.cpId))
			return false;
		if (os == null) {
			if (other.os != null)
				return false;
		} else if (!os.equals(other.os))
			return false;
		if (pkgname == null) {
			if (other.pkgname != null)
				return false;
		} else if (!pkgname.equals(other.pkgname))
			return false;
		return true;
	}
}

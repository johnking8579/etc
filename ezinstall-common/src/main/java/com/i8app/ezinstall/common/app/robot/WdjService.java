package com.i8app.ezinstall.common.app.robot;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.i8app.ezinstall.common.HttpClientUtil;
import com.i8app.ezinstall.common.app.Constants;
import com.i8app.ezinstall.common.app.I8appService;
import com.i8app.ezinstall.common.app.dao.I8appDao;
import com.i8app.ezinstall.common.app.domain.I8app;
import com.i8app.ezinstall.common.wsclient.WdjConnection;
import com.i8app.ezinstall.common.wsclient.WdjJsonParser;
import com.i8app.ezinstall.packreader.PackReadException;

/**
 * 从豌豆荚更新或导入
 * 
 * @author JingYing 2014-1-26
 */
@Component
public class WdjService {
	
	private static Logger logger = Logger.getLogger(WdjService.class);
	
	private WdjConnection wdjConn = new WdjConnection();
	
	@Resource
	private I8appService i8appService;
	@Resource
	private I8appDao i8appDao;
	
	@Resource
	private I8appAcquireOnInsert4Wdj i8appAcquireOnInsert4Wdj;
	
	
	/**
	 * 根据包名从豌豆荚检测
	 * @param apkPkgName
	 */
	public void checkFromWdj(String apkPkgName) {
		List<I8app> i8List = i8appDao.findByPkgname(Constants.ANDROID, apkPkgName, 0);
		if(i8List.isEmpty())	{
			insertFromWdj(apkPkgName);
		} else	{
			for(I8app i : i8List)	{
				updateFromWdj(i);
			}
		}
	}
	
	/**
	 * 从豌豆荚新增
	 * @param pkgName
	 */
	public void insertFromWdj(String pkgName)	{
		try {
			WdjJsonParser wdj = new WdjJsonParser(wdjConn.connect(pkgName));
			logger.info("正在从豌豆荚下载:" + wdj.getDownloadUrl());
			File tmp = HttpClientUtil.downloadToTemp(wdj.getDownloadUrl(), ".apk", 10 * 60);			//设置10分钟超时
			i8appAcquireOnInsert4Wdj.setWdj(wdj);
			i8appService.insertApk(tmp, i8appAcquireOnInsert4Wdj);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PackReadException e) {
			e.printStackTrace();
		}	
		
		try {
			Thread.sleep(1000 * 60 * 3);	//休眠,防止被禁
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 从豌豆荚更新
	 * @param i8app
	 */
	public void updateFromWdj(I8app i8app)	{
		if(!Constants.ANDROID.equals(i8app.getOs()))	{
			throw new UnsupportedOperationException("只能从豌豆荚更新android数据,不支持的os:" + i8app.getOs());
		}
		try {
			WdjJsonParser wdj = new WdjJsonParser(wdjConn.connect(i8app.getPkgName()));
			if(wdj.getVersionCode() > i8app.getVersionCode())	{	//更新
				logger.info("正在从豌豆荚下载:" + wdj.getDownloadUrl());
				File tmp = HttpClientUtil.downloadToTemp(wdj.getDownloadUrl(), ".apk", 10 * 60);	//设置10分钟超时
				i8appService.updateApk(tmp, i8app.getId(), new I8appAcquireOnUpdate4Wdj(wdj));
			} else	{
				logger.info(String.format("豌豆荚上没有更新,包名:%s,豌豆荚版本:%s,i8app版本:%s", 
						i8app.getPkgName(), wdj.getVersionName(), i8app.getVersion()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Thread.sleep(1000 * 60 * 3);	//休眠,防止被禁
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

package com.i8app.ezinstall.common.app.domain;

public class OsInfo {
	private Integer osId;
	private String osVersion;

	public Integer getOsId() {
		return osId;
	}

	public void setOsId(Integer osId) {
		this.osId = osId;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

}

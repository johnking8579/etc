package com.i8app.ezinstall.common.app.dao;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.Util;
import com.i8app.ezinstall.common.app.domain.Axon;

public class AxonDaoTest extends BaseTest	{

	@Resource AxonDao dao;
	
	@Test
	@Rollback(false)
	public void executeReplaceDesc()	{
		List<Axon> list = dao.findAll();
		for(Axon a : list)	{
			System.out.println(a.getContentId());
			a.setDescription(Util.unicodeToUtf8(a.getDescription()));
			dao.update(a);
		}
	}
	
	@Test
	@Rollback(false)
	public void test()	{
		Axon a = dao.findById("4326581");
		System.out.println(a.getDescription());
		String s = Util.unicodeToUtf8(a.getDescription());
		a.setDescription(s);
		dao.update(a);
		
	}
}

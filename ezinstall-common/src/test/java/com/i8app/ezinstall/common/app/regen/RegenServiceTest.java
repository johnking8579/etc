package com.i8app.ezinstall.common.app.regen;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.ClientParamHolder;

public class RegenServiceTest extends BaseTest	{

	@Resource RegenService service;
	
	@Before
	public void setUp()	{
		ClientParamHolder.setFileServerDir("c:/users/jing/desktop");
	}
	
	@Test
	public void test1()	{
		service.afterRegen();
	}
	
	@Test
	public void testAddPic()	{
		service.fixPicAfterAddColumn();
	}
	
	
}

package com.i8app.ezinstall.common.app.assembly.cp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.annotation.Resource;

import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.app.CpServerAccessException;
import com.i8app.ezinstall.common.app.domain.AppInstallPack;
import com.i8app.ezinstall.packreader.PackReadException;

public class AxonAdapteeTest extends BaseTest {
	
	@Resource
	AxonAdapteeImpl adaptee;
	
	@Test
	public void testGetPackUrl() throws PackReadException, IOException, CpServerAccessException	{
		AppInstallPack pack = new AppInstallPack();
		pack.setOriginalPackId("1139618");
		
		InputStream is = new URL(adaptee.findPackUrl(pack.getOriginalPackId(), pack.getOs())).openStream();
		
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		File file = null;
		try	{
			file = File.createTempFile("axon", ".zip");
			bis = new BufferedInputStream(is);
			bos = new BufferedOutputStream(new FileOutputStream(file));
			int i;
			while((i=bis.read()) != -1){
				bos.write(i);
			}
			bos.flush();
		} finally	{
			try {
				bis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				bos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

}

package com.i8app.ezinstall.common;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations={"classpath:applicationContext-test.xml"})
public class BaseTest extends AbstractTransactionalJUnit4SpringContextTests{

	/**
	 * 空方法, 避免MAVEN测试时找不到TEST实例而报错
	 */
	@Test
	public void test()	{
		
	}
	
}


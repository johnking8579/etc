package com.i8app.ezinstall.common;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.i8app.ezinstall.common.app.assembly.AppAssemblier;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.regen.RegenService;

/**
 * 测试
 * 外部系统调用本JAR包. 
 * 
 * @author JingYing 2013-5-9
 */
public class ClientInvokeTest 	{
	
	private ApplicationContext ac;
	
	@Before
	public void setUp()	{
		ac = new ClassPathXmlApplicationContext("classpath:applicationContext-invokerTest.xml");
	}
	
	@Test
	public void test()	{
		AppQuerier aq = ac.getBean(AppQuerier.class);
		System.out.println(aq);
		AppAssemblier aa = ac.getBean(AppAssemblier.class);
		System.out.println(aa);
		System.out.println(ac.getBean(RegenService.class));
		try {
			System.out.println(ClientParamHolder.getAxonApi().getDownloadUrl("7445766"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}

package com.i8app.ezinstall.common.app.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.i8app.ezinstall.common.BaseTest;
import com.i8app.ezinstall.common.app.domain.PkgGroup;
import com.i8app.ezinstall.common.app.query.AppQuerier;
import com.i8app.ezinstall.common.app.query.AreaParam;

public class PkgGroupDaoTest extends BaseTest {

	@Resource
	PkgGroupDao dao;
	
	@Before
	public void setUp()	{
		deleteFromTables("pkg_group", "biz", "biz_area");
	}

	@Test
	public void test1() throws SQLException {
		AreaParam a = new AreaParam("01", 1, 2);
		List<PkgGroup> list = dao.find(a, AppQuerier.BIZ_ESSENTIAL);
		for (PkgGroup p : list) {
			System.out.println(p.getName());
		}
	}

	/**
	 * 检测区域内的活动列表, 重点测试是否处于deptid,provid,cityid范围内
	 */
	@Test
	public void testfindAboutActivity_AREA() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cur = Calendar.getInstance(), start = Calendar.getInstance(), end = Calendar
				.getInstance();
		start.add(Calendar.DAY_OF_YEAR, -5); // 开始日设为当前日-5
		end.add(Calendar.DAY_OF_YEAR, 5); // 结束日设为当前日+5

		simpleJdbcTemplate.update("INSERT INTO PKG_GROUP(ID,NAME,RELATEID,ISDEL) VALUES" +
					"(1,'活动1的pg',null,0),(2,'活动2的pg',null,0),(3,'活动3的pg',null,0),(4,'活动4的pg',null,0)");

		simpleJdbcTemplate.update("INSERT INTO BIZ(ID,NAME,BIZTYPE,STARTTIME,ENDTIME,PKGGROUPID) "
					+ "VALUES(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?),(?,?,?,?,?,?)",
					5, "活动1", "biz005", sdf.format(start.getTime()),sdf.format(end.getTime()), 1, 
					6, "活动2", "biz005",sdf.format(start.getTime()), sdf.format(end.getTime()), 2,
					7, "活动3", "biz005", sdf.format(start.getTime()),sdf.format(end.getTime()), 3, 
					8, "活动4", "biz005",sdf.format(start.getTime()), sdf.format(end.getTime()), 4);

		simpleJdbcTemplate.update("INSERT INTO BIZ_AREA(BIZID,DEPTID,PROVID,CITYID,ISINHERITPKGGROUP,ISINHERITBIZTIME) "
						+ "VALUES(5,'01',0,0,1,1),(6,'02',0,0,1,1),(7,'01',1,0,1,1),(8,'01',1,3,1,1)");

		List<PkgGroup> list = dao.find(new AreaParam("01", 1, 3), 5);
		assertEquals(3, list.size());
		assertTrue(!list.get(0).getId().equals(list.get(1).getId()));	//所有pkgGroup的不相同
		assertTrue(!list.get(1).getId().equals(list.get(2).getId()));
	}

	@Test
	public void testFindActivity_time() throws ParseException	{
		executeSqlScript("com/i8app/ezinstall/common/app/dao/findActivity_time.sql", true);
		Date cur = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2013-05-15 10:00:00");
		List<PkgGroup> list = dao.find(new AreaParam("01", 1, 3), 5);
		assertEquals(2, list.size());
	}

}

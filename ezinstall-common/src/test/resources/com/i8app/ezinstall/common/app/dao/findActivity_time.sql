insert  into `biz`(`id`,`name`,`bizType`,`startTime`,`endTime`,`pkgGroupId`,`info`,`infoUrl`,`picId`) values 
(8,'act1','biz005','2013-05-12 16:04:50','2013-05-18 16:05:05',1,NULL,NULL,NULL),
(9,'act2','biz005','2013-05-19 16:05:10','2013-05-30 16:05:24',2,NULL,NULL,NULL),
(10,'act3','biz005','2013-05-05 16:05:29','2013-05-07 16:05:33',3,NULL,NULL,NULL),
(11,'act4','biz005','2013-05-12 16:05:38','2013-05-24 16:05:43',4,NULL,NULL,NULL);

insert  into `biz_area`(`id`,`bizId`,`deptId`,`provId`,`cityId`,`isInheritPkgGroup`,`customPkgGroupId`,`isInheritBizTime`,`customStartTime`,`customEndTime`,`creator`,`insertTime`,`mender`,`updateTime`) values 
 (39,8,'01',0,0,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),
(40,9,'01',0,0,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),
(41,10,'01',0,0,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),
(42,11,'01',0,0,1,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL);

insert  into `pkg_group`(`id`,`name`,`info`,`relateId`,`isDel`) values 
(1,'act1pg',NULL,NULL,0),
(2,'act2pg',NULL,NULL,0),
(3,'act3pg',NULL,NULL,0),
(4,'act4pg',NULL,NULL,0);
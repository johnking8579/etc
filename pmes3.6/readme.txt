1.0
	* 继承PMES模板
	* Extjs3.0 + Struts2.1.8 + Spring2.5.6 + Hibernate3.3 + Annotation + MySQL5.0
	
1.1
	* 建立User和Dept实体，使用JPA注解映射
	
1.2
	* 建立UserDao、UserService
	
1.3
	* 建立UserAction
	* 完成用户查询
	
1.4
	* 添加用户
	* 焦点定位到第一个输入域
	* 默认选中性别中的GG
	* 下拉列表中的数据，从数据库中取得
	* 引入JSON_LIB
		* json-lib-2.1.jar
		* ezmorph-1.0.3.jar
		* commons-lang-2.3.jar
		* commons-collections-3.2.jar
		* commons-beanutils-1.7.0.jar
		* commons-logging-1.0.4.jar	
		
1.5
	* 将JSON字符串的打印封装到BaseAction中
	* 完成用户添加
	
1.6
	* 修改用户
		
1.7
	* 删除用户
	
1.8
	* 登录

1.9
	* grid中文排序问题
	// 重载 Ext.data.Store.prototype.applySort 函数以修复 DataStore 对汉字排序异常的问题   
	// var _applySort = Ext.data.Store.prototype.applySort;           
	//如有需要，保存原 applySort 函数的引用   
	Ext.data.Store.prototype.applySort = function(){          
	         //重载 applySort   
	        if(this.sortInfo && !this.remoteSort){   
	                var s = this.sortInfo, f = s.field;   
	                var st = this.fields.get(f).sortType;   
	                var fn = function(r1, r2){   
	                        var v1 = st(r1.data[f]), v2 = st(r2.data[f]);   
	                        // 添加:修复汉字排序异常的Bug   
	                        if(typeof(v1) == "string"){ //若为字符串，   
	                                return v1.localeCompare(v2);   
	                                //则用 localeCompare 比较汉字字符串, Firefox 与 IE 均支持   
	                        }   
	                        // 添加结束   
	                        return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);   
	                };   
	                this.data.sort(s.direction, fn);   
	                if(this.snapshot && this.snapshot != this.data){   
	                        this.snapshot.sort(s.direction, fn);   
	                }   
	        }   
	};   
	
	建立gridSort.js,使用到排序的grid引用
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/gridSort.js"></script>
	
2.0
	* 将JBPM4集成到SSH架构中
		* 引入JBPM4核心依赖包JBPM_HOME/jbpm.jar
        * 引入JBPM4其他依赖包JBPM_HOME/*.jar(必须删除自带的MySQL依赖包mysql-connector-java.jar)
        * 引入MySQL JDBC驱动	
        * 修改jdbc.properties文件，MySQL的方言修改为：org.hibernate.dialect.MySQLInnoDBDialect
        * 将JBPM4中的hibernate映射文件全部交给Spring管理
	 		<property name="mappingResources">
				<list>
					<value>jbpm.repository.hbm.xml</value>
					<value>jbpm.execution.hbm.xml</value>
					<value>jbpm.history.hbm.xml</value>
					<value>jbpm.task.hbm.xml</value>
					<value>jbpm.identity.hbm.xml</value>
				</list>
			</property>
		* 将ProcessEngine交给Spring管理，这样JBPM4中使用的Hibernate Session就是Spring提供的
		* 将JPBM_HOME/install/src/cfg/spring/applicationContext.xml文件中的内容
		   <bean id="springHelper" class="org.jbpm.pvm.internal.processengine.SpringHelper" />
		   <bean id="processEngine" factory-bean="springHelper" factory-method="createProcessEngine" />
		      拷贝到PMES项目的Spring配置文件中
		* 将JPBM_HOME/install/src/cfg/jbpm/spring.jbpm.cfg.xml拷贝到PMES的src目录下
		  --将spring.jbpm.cfg.xml修改jbpm.cfg.xml  
		* 启动Tomcat，是否可以创建出JBPM4相关的表，如果可以集成成功
		* 去除JBPM4定时发送JOB，修改jbpm.cfg.xml,将JOB注释
		  <!-- 
		  <import resource="jbpm.jobexecutor.cfg.xml" />
		   -->
		* 为了更便面的使用JBPM3，  使用processEngine分别创建出Service，以后注入Service		   
			<bean id="repositoryService" factory-bean="processEngine" factory-method="getRepositoryService"/>
			<bean id="executionService" factory-bean="processEngine" factory-method="getExecutionService"/>
			<bean id="historyService" factory-bean="processEngine" factory-method="getHistoryService"/>
			<bean id="taskService" factory-bean="processEngine" factory-method="getTaskService"/>
			<bean id="identityService" factory-bean="processEngine" factory-method="getIdentityService"/>
			<bean id="managementService" factory-bean="processEngine" factory-method="getManagementService"/>
		   
2.1
	* 建立ProcessDefinitionDao和ProcessDefinitionService
	* 配置到IoC容器中
				       	
2.2
	* 建立ProcessDefinitionAction，配置到IoC容器中，配置到Struts2中
	* 提供查询页面，完成流程定义查询
	*  解决Tomcat6下面包冲突
		--删除TOMCAT_HOME/lib/el-api.jar文件
		--将PMES项目中WEB-INF/lib/juel-api.jar、juel-engine.jar、juel-impl.jar
		  剪切到TOMCAT_HOME/lib下
		  
2.3
	* 添加流程（部署流程）
	
2.4
	* 删除流程				
		
2.5
	* 查看流程定义图片
					
2.6
	* 查看流程定义JPDL
	
2.7
	* 建立TroubleTicket实体和ApproveInfo实体，采用JPA映射，导出表
		
2.8
	* 查询我的故障单
	
2.9
	* 添加故障单
		
3.0
	* 派障
		
3.1
	* 取得待审故障单列表	

3.2
	* 显示添加审批意见界面（extjs部分）
		
3.3
	* 建立ApproveDao、ApproveDaoService、ApproveDaoAction
	* 配置到IoC容器
	* 完成审批意见的添加
	* 让流程继续向下走
	
3.4
	* 查看审批历史
		
3.5
	* 对故障单JSON字符串拼装，进行封装
	* 建立AbtractTroubleTicketAction
	
3.6
	* 已审故障单		
			
		
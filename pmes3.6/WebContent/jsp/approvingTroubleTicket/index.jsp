<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/extjs/resources/css/ext-all.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/extjs/ext-all.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/extjs/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/extjs/ux/ProgressBarPager.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/gridSort.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/approvingTroubleTicket.js"></script>
</head>
<body>
	<div id="approvingTroubleTicket_div"></div>
</body>	
</html>
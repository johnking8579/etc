package com.bjpowernode.pmes.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bjpowernode.pmes.dao.DeptDao;
import com.bjpowernode.pmes.domain.Dept;

@Repository
public class DeptDaoImpl extends BaseDao implements DeptDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Dept> findDeptList() {
		return getHibernateTemplate().find("from Dept d order by d.id");
	}
	
}

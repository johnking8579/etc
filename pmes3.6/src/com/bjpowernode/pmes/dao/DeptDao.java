package com.bjpowernode.pmes.dao;

import java.util.List;

import com.bjpowernode.pmes.domain.Dept;

public interface DeptDao {

	/**
	 * 取得所有的部门
	 * @return
	 */
	public List<Dept> findDeptList();
}

package com.bjpowernode.pmes.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="t_approve_info")
public class ApproveInfo {

	private Integer id;
	
	private String desc;
	
	private String approveTime;
	
	private User approver;
	
	private TroubleTicket troubleTicket;

	@Id @GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="desc_")
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getApproveTime() {
		return approveTime;
	}

	public void setApproveTime(String approveTime) {
		this.approveTime = approveTime;
	}
	
	@ManyToOne
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}
	
	@ManyToOne
	public TroubleTicket getTroubleTicket() {
		return troubleTicket;
	}

	public void setTroubleTicket(TroubleTicket troubleTicket) {
		this.troubleTicket = troubleTicket;
	}
	
}

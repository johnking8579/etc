package com.bjpowernode.pmes.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_dept")
public class Dept {

	private Integer id;
	
	private String name;
	
	//@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	//@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Id @GeneratedValue
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

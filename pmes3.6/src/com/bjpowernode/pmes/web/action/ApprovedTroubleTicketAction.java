package com.bjpowernode.pmes.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.TroubleTicket;
import com.bjpowernode.pmes.service.TroubleTicketService;

@Controller
@Scope("prototype")
public class ApprovedTroubleTicketAction extends AbtractTroubleTicketAction {

	private TroubleTicketService troubleTicketService;
	
	@Autowired
	public void setTroubleTicketService(TroubleTicketService troubleTicketService) {
		this.troubleTicketService = troubleTicketService;
	}
	
	public String index() throws Exception {
		return SUCCESS;
	}
	
	public String list() throws Exception {
		Pager<TroubleTicket> pager = troubleTicketService.findApprovedTroubleTicketList(currentUser().getId());
		return troubleTicket2JsonString(pager);
	}
}

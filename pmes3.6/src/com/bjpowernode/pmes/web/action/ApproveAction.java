package com.bjpowernode.pmes.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.domain.ApproveInfo;
import com.bjpowernode.pmes.service.ApproveService;

@Controller
@Scope("prototype")
public class ApproveAction extends BaseAction {

	private ApproveService approveService;
	
	private ApproveInfo approveInfo;
	
	@Autowired
	public void setApproveService(ApproveService approveService) {
		this.approveService = approveService;
	}

	public ApproveInfo getApproveInfo() {
		return approveInfo;
	}

	public void setApproveInfo(ApproveInfo approveInfo) {
		this.approveInfo = approveInfo;
	}

	public String add() throws Exception {
		approveInfo.setApprover(currentUser());
		approveService.add(approveInfo);
		return ajaxOut("{success: true}");
	}
	
	public String history()  throws Exception {
		List<ApproveInfo> approveInfoList = approveService.findHistoryByTroubleTicketId(approveInfo.getTroubleTicket().getId());
		StringBuilder sbJson = new StringBuilder();
		sbJson.append("[");
		for (int i=0; i<approveInfoList.size(); i++) {
			ApproveInfo approveInfo = approveInfoList.get(i);
			sbJson.append("{");
			sbJson.append("desc:'");
			sbJson.append(approveInfo.getDesc());
			sbJson.append("',approveTime:'");
			sbJson.append(approveInfo.getApproveTime());
			sbJson.append("',approver:'");
			sbJson.append(approveInfo.getApprover().getName());
			sbJson.append("'}");
			if (i <(approveInfoList.size() - 1)) {
				sbJson.append(",");
			}
		}
		sbJson.append("]");
		return ajaxOut(sbJson.toString());
	}
}

package com.bjpowernode.pmes.web.action;

import java.util.List;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bjpowernode.pmes.domain.Dept;
import com.bjpowernode.pmes.service.DeptService;

@Controller
@Scope("prototype")
public class DeptAction extends BaseAction {

	private DeptService deptService;
	
	@Autowired
	public void setDeptService(DeptService deptService) {
		this.deptService = deptService;
	}

	public String list() throws Exception {
		List<Dept> deptList = deptService.findDeptList();
//		String jsonString = JSONArray.fromObject(deptList).toString();
//		getResponse().setContentType("text/html;charset=utf-8");
//		getResponse().getWriter().print(jsonString);		
		return ajaxOut(deptList);
	}
}

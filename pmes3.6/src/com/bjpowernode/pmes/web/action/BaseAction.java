package com.bjpowernode.pmes.web.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.bjpowernode.pmes.Globals;
import com.bjpowernode.pmes.domain.User;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class BaseAction extends ActionSupport {

	public static final String AJAX = "ajax"; 
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	

	protected HttpServletRequest getRequest() {
		return ServletActionContext.getRequest();
	}
	
	protected HttpSession getSession() {
		return getRequest().getSession();
	}
	
	protected HttpServletResponse getResponse() {
		return ServletActionContext.getResponse();
	}
	
	protected String getClientIp() {
		return getRequest().getRemoteAddr();
	}
	
	protected String getContextPath() {
		return getRequest().getContextPath();
	}
	
	protected String ajaxOut(List list) throws Exception {
		String jsonString = JSONArray.fromObject(list).toString();
//		getResponse().setContentType("text/html;charset=utf-8");
//		getResponse().getWriter().print(jsonString);
		return ajaxOut(jsonString);
	}
	
	protected String ajaxOut(String jsonString) throws Exception {
		getResponse().setContentType("text/html;charset=utf-8");
		getResponse().getWriter().print(jsonString);
		return null;
	}
	
	protected String ajaxOut() throws Exception {
		return null;
	}	
	
	protected User currentUser() {
		return (User)getSession().getAttribute(Globals.CURRENT_USER);
	}
}

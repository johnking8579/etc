package com.bjpowernode.pmes.service;

import java.util.List;

import com.bjpowernode.pmes.domain.ApproveInfo;

public interface ApproveService {
	
	/**
	 * 添加审批意见
	 * @param approveInfo
	 */
	public void add(ApproveInfo approveInfo);
	
	/**
	 * 根据故障单单号查询审批历史信息
	 * @param troubleTicketId
	 * @return
	 */
	public List<ApproveInfo> findHistoryByTroubleTicketId(Integer troubleTicketId);
	
}

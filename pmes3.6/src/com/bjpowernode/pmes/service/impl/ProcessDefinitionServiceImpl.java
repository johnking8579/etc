package com.bjpowernode.pmes.service.impl;

import java.io.File;
import java.io.InputStream;

import org.jbpm.api.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.dao.ProcessDefinitionDao;
import com.bjpowernode.pmes.service.ProcessDefinitionService;

@Service
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {

	private ProcessDefinitionDao processDefinitionDao;
	
	@Autowired
	public void setProcessDefinitionDao(ProcessDefinitionDao processDefinitionDao) {
		this.processDefinitionDao = processDefinitionDao;
	}

	@Override
	public Pager<ProcessDefinition> findProcessDefinitionList() {
		return processDefinitionDao.findProcessDefinitionList();
	}

	@Override
	public void add(File processDefinitionFile) {
		processDefinitionDao.add(processDefinitionFile);
	}

	@Override
	public void del(String[] deploymentIds) {
		for (String deploymentId : deploymentIds) {
			processDefinitionDao.del(deploymentId);
		}
	}

	@Override
	public InputStream viewImage(String deploymentId, String imageResourceName) {
		return processDefinitionDao.viewImage(deploymentId, imageResourceName);
	}

	@Override
	public InputStream viewConent(String deploymentId) {
		return processDefinitionDao.viewConent(deploymentId);
	}
}

package com.bjpowernode.pmes.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.dao.UserDao;
import com.bjpowernode.pmes.domain.User;
import com.bjpowernode.pmes.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserDao userDao;
	
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void addOrUpdate(User user) {
		userDao.addOrUpdate(user);
	}

	public void del(Integer userId) {
		userDao.del(userId);
	}

	public User findUserById(Integer userId) {
		return userDao.findUserById(userId);
	}

	public Pager<User> findUserList() {
		return userDao.findUserList();
	}

	@Override
	public void del(Integer[] userIds) {
		for (Integer userId : userIds) {
			userDao.del(userId);
		}
	}

	@Override
	public User login(String userCode, String password) {
		return userDao.login(userCode, password);
	}

}

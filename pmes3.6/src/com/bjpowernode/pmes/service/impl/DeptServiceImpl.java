package com.bjpowernode.pmes.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjpowernode.pmes.dao.DeptDao;
import com.bjpowernode.pmes.domain.Dept;
import com.bjpowernode.pmes.service.DeptService;

@Service
public class DeptServiceImpl implements DeptService {

	private DeptDao deptDao;
	
	@Autowired
	public void setDeptDao(DeptDao deptDao) {
		this.deptDao = deptDao;
	}

	@Override
	public List<Dept> findDeptList() {
		return deptDao.findDeptList();
	}

}

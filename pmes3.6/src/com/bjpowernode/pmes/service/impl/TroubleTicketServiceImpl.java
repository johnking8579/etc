package com.bjpowernode.pmes.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.dao.TroubleTicketDao;
import com.bjpowernode.pmes.domain.TroubleTicket;
import com.bjpowernode.pmes.service.TroubleTicketService;

@Service
public class TroubleTicketServiceImpl implements TroubleTicketService {

	private TroubleTicketDao troubleTicketDao;
	
	@Autowired
	public void setTroubleTicketDao(TroubleTicketDao troubleTicketDao) {
		this.troubleTicketDao = troubleTicketDao;
	}

	@Override
	public void addOrUpdate(TroubleTicket troubleTicket) {
		if (troubleTicket.getId() == null) {
			troubleTicket.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
			troubleTicket.setStatus(TroubleTicket.NEW);
			troubleTicketDao.addOrUpdate(troubleTicket);
		}
	}

	@Override
	public void del(Integer[] troubleTicketId) {
		for (Integer id : troubleTicketId) {
			troubleTicketDao.del(id);
		}
	}

	@Override
	public Pager<TroubleTicket> findMyTroubleTicketList(Integer creatorId) {
		return troubleTicketDao.findMyTroubleTicketList(creatorId);
	}

	@Override
	public TroubleTicket findTroubleTicketById(Integer troubleTicketId) {
		return troubleTicketDao.findTroubleTicketById(troubleTicketId);
	}

	@Override
	public void dispatch(Integer[] troubleTicketIds) {
		for (Integer troublelTicketId : troubleTicketIds) {
			troubleTicketDao.dispatch(troublelTicketId);	
		}
	}

	@Override
	public Pager<TroubleTicket> findApprovingTroubleTicketList(
			Integer approverId) {
		return troubleTicketDao.findApprovingTroubleTicketList(approverId);
	}

	@Override
	public Pager<TroubleTicket> findApprovedTroubleTicketList(Integer approverId) {
		return troubleTicketDao.findApprovedTroubleTicketList(approverId);
	}

}

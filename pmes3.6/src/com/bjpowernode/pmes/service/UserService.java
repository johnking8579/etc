package com.bjpowernode.pmes.service;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.User;

public interface UserService {
	
	/**
	 * 添加或修改
	 * @param user
	 */
	public void addOrUpdate(User user);

	/**
	 * 根据id删除用户
	 * @param userId
	 */
	public void del(Integer userId);
	
	/**
	 * 根据id删除用户
	 * @param userId
	 */
	public void del(Integer[] userIds);
	
	/**
	 * 根据id查询用户
	 * @param userId
	 * @return
	 */
	public User findUserById(Integer userId);
	
	/**
	 * 分页查询用户
	 * @return
	 */
	public Pager<User> findUserList();
	
	/**
	 * 登录
	 * @param userCode
	 * @param password
	 * @return
	 */
	public User login(String userCode, String password);	
}

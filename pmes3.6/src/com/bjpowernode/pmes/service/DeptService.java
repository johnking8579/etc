package com.bjpowernode.pmes.service;

import java.util.List;

import com.bjpowernode.pmes.domain.Dept;

public interface DeptService {

	/**
	 * 取得所有的部门
	 * @return
	 */
	public List<Dept> findDeptList();	
}

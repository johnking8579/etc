package com.bjpowernode.pmes.service;

import com.bjpowernode.pmes.Pager;
import com.bjpowernode.pmes.domain.TroubleTicket;

public interface TroubleTicketService {

	/**
	 * 添加或修改故障单
	 * @param troubleTicket
	 */
	public void addOrUpdate(TroubleTicket troubleTicket);
	
	/**
	 * 根据id批量删除故障单
	 * 
	 * @param troubleTicketId
	 */
	public void del(Integer[] troubleTicketId);
	
	/**
	 * 根据故障单id查询
	 * @param troubleTicketId
	 * @return
	 */
	public TroubleTicket findTroubleTicketById(Integer troubleTicketId);
	
	/**
	 * 查询我的故障单
	 * @param creatorId
	 * @return
	 */
	public Pager<TroubleTicket> findMyTroubleTicketList(Integer creatorId);
	
	/**
	 * 派障
	 * @param troubleTicketIds
	 */
	public void dispatch(Integer[] troubleTicketIds);
	
	/**
	 * 待审故障单
	 * @param creatorId
	 * @return
	 */
	public Pager<TroubleTicket> findApprovingTroubleTicketList(Integer approverId);

	
	/**
	 * 已审故障单
	 * @param creatorId
	 * @return
	 */
	public Pager<TroubleTicket> findApprovedTroubleTicketList(Integer approverId);	
}

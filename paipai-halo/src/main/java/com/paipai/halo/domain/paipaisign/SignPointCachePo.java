package com.paipai.halo.domain.paipaisign;

import java.io.Serializable;

/**
 * 连续登陆天数和当前橡果数
 * @author liubenlong3
 *
 */
public class SignPointCachePo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int signDays;
	private long currentPoint;
	
	public int getSignDays() {
		return signDays;
	}
	public SignPointCachePo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SignPointCachePo(int signDays, long currentPoint) {
		super();
		this.signDays = signDays;
		this.currentPoint = currentPoint;
	}
	public void setSignDays(int signDays) {
		this.signDays = signDays;
	}
	public long getCurrentPoint() {
		return currentPoint;
	}
	public void setCurrentPoint(int currentPoint) {
		this.currentPoint = currentPoint;
	}
	
	
	
}

package com.paipai.halo.domain;

import java.util.ArrayList;
import java.util.List;

public class OutModelInfo<T> {
	
	public T datainfo = null;
	public List<Files> files = new ArrayList<Files>();
	public List<Files> getFiles() {
		return files;
	}
	public void setFiles(List<Files> files) {
		this.files = files;
	}
	public T getDatainfo() {
		return datainfo;
	}
	public void setDatainfo(T datainfo) {
		this.datainfo = datainfo;
	}
	

}

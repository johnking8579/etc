package com.paipai.halo.domain;

import java.io.Serializable;

public class ItemLikes implements Serializable{
	private Long id;
	private String itemid;
	private int itemtype;
	private int likesnum;
	private int commentnum;
	private Long ownerid; 
	
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getItemtype() {
		return itemtype;
	}
	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}
	public int getLikesnum() {
		return likesnum;
	}
	public void setLikesnum(int likesnum) {
		this.likesnum = likesnum;
	}
	public int getCommentnum() {
		return commentnum;
	}
	public void setCommentnum(int commentnum) {
		this.commentnum = commentnum;
	}
	public Long getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(Long ownerid) {
		this.ownerid = ownerid;
	}
	
	
}

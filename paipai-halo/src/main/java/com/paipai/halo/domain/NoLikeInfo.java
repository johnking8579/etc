package com.paipai.halo.domain;

import java.io.Serializable;

/**
 * 
 * @ClassName: LikeInfo 
 * @Description:个人点踩记录
 * @author lhn
 * @date 2015-3-4 下午4:03:37 
 *
 */
public class NoLikeInfo implements Serializable{
	  private Long id;
	  private int itemtype;
	  private Long noliketime;
	  private Long wid;
	  private String itemid;
	  private int nolikenum;
	  private int status;
	  private String nickname;
	  private String head;
	  
	  
	  
	  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getItemtype() {
		return itemtype;
	}
	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public Long getNoliketime() {
		return noliketime;
	}
	public void setNoliketime(Long noliketime) {
		this.noliketime = noliketime;
	}
	public int getNolikenum() {
		return nolikenum;
	}
	public void setNolikenum(int nolikenum) {
		this.nolikenum = nolikenum;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	  
	  
	  
}

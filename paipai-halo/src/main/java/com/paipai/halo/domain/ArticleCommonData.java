package com.paipai.halo.domain;

import java.io.Serializable;


public class ArticleCommonData implements Serializable{
		private String itemid;
		private CommentData commentDatas = new CommentData();
		public String getItemid() {
			return itemid;
		}
		public void setItemid(String itemid) {
			this.itemid = itemid;
		}
		public CommentData getCommentDatas() {
			return commentDatas;
		}
		public void setCommentDatas(CommentData commentDatas) {
			this.commentDatas = commentDatas;
		}
		
}

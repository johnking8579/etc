package com.paipai.halo.domain.paipaisign;


public class AddressPo {

	/**
	 * 收货地址ID
	 *
	 * 版本 >= 0
	 */
	 private long addrId;

	/**
	 * QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 地址ID
	 *
	 * 版本 >= 0
	 */
	 private long regionId;	

	/**
	 * 接收者的姓名
	 *
	 * 版本 >= 0
	 */
	 private String recvName = new String();

	/**
	 * 邮编
	 *
	 * 版本 >= 0
	 */
	 private String postCode = new String();

	/**
	 * 详细收货地址
	 *
	 * 版本 >= 0
	 */
	 private String recvAddress = new String();

	/**
	 * 电话
	 *
	 * 版本 >= 0
	 */
	 private String recvPhone = new String();

	/**
	 * 手机
	 *
	 * 版本 >= 0
	 */
	 private String recvMobile = new String();


	/**
	 * 省
	 *
	 * 版本 >= 2
	 */
	 private String recvProvince = new String();

	/**
	 * 市
	 *
	 * 版本 >= 2
	 */
	 private String recvCity = new String();

	/**
	 * 区
	 *
	 * 版本 >= 2
	 */
	 private String recvDistric = new String();

	public long getAddrId() {
		return addrId;
	}

	public void setAddrId(long addrId) {
		this.addrId = addrId;
	}

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public String getRecvName() {
		return recvName;
	}

	public void setRecvName(String recvName) {
		this.recvName = recvName;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getRecvAddress() {
		return recvAddress;
	}

	public void setRecvAddress(String recvAddress) {
		this.recvAddress = recvAddress;
	}

	public String getRecvPhone() {
		return recvPhone;
	}

	public void setRecvPhone(String recvPhone) {
		this.recvPhone = recvPhone;
	}

	public String getRecvMobile() {
		return recvMobile;
	}

	public void setRecvMobile(String recvMobile) {
		this.recvMobile = recvMobile;
	}

	public String getRecvProvince() {
		return recvProvince;
	}

	public void setRecvProvince(String recvProvince) {
		this.recvProvince = recvProvince;
	}

	public String getRecvCity() {
		return recvCity;
	}

	public void setRecvCity(String recvCity) {
		this.recvCity = recvCity;
	}

	public String getRecvDistric() {
		return recvDistric;
	}

	public void setRecvDistric(String recvDistric) {
		this.recvDistric = recvDistric;
	}
}

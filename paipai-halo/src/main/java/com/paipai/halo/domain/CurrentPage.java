package com.paipai.halo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @ClassName: CurrentPage 
 * @Description: 统用的 页面类
 * @author lhn
 * @date 2015-3-10 上午10:49:02 
 * 

 * @param <E>
 */
public class CurrentPage<E> implements Serializable{
    private int pagenum;//当前页数
    private int pagesize;//一页多少条
    private int totalnum;//总页数
    private int totalcount;//总的个数
    private int likesnum;//点赞数
    private int commentnum;//评论数
    private int islike;//是否点赞
    private long curtime;//当前时间
    private List<E> pageItems = new ArrayList<E>();//数据
	public int getPagenum() {
		return pagenum;
	}
	public void setPagenum(int pagenum) {
		this.pagenum = pagenum;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	public int getTotalnum() {
		return totalnum;
	}
	public void setTotalnum(int totalnum) {
		this.totalnum = totalnum;
	}
	public List<E> getPageItems() {
		return pageItems;
	}
	public void setPageItems(List<E> pageItems) {
		this.pageItems = pageItems;
	}
	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	public int getLikesnum() {
		return likesnum;
	}
	public void setLikesnum(int likesnum) {
		this.likesnum = likesnum;
	}
	public int getCommentnum() {
		return commentnum;
	}
	public void setCommentnum(int commentnum) {
		this.commentnum = commentnum;
	}
	public int getIslike() {
		return islike;
	}
	public void setIslike(int islike) {
		this.islike = islike;
	}
	public long getCurtime() {
		return curtime;
	}
	public void setCurtime(long curtime) {
		this.curtime = curtime;
	}
    
}

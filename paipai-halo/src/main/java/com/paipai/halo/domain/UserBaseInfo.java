package com.paipai.halo.domain;

import java.io.Serializable;

/**
 * 
 * @ClassName: UserBaseInfo 
 * @Description:用户基本信息表 
 * @author lhn
 * @date 2015-3-12 下午3:38:44 
 *
 */
public class UserBaseInfo implements Serializable{
	private Long wid;
	private String nickname;
	private String head;//头像
	private int usertype;
	private String headhash;
	/**
	 * 0:普通用户1：黑名单2：白名单
	 */
	private int status;
	
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	public int getUsertype() {
		return usertype;
	}
	public void setUsertype(int usertype) {
		this.usertype = usertype;
	}
	public String getHeadhash() {
		return headhash;
	}
	public void setHeadhash(String headhash) {
		this.headhash = headhash;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}

package com.paipai.halo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CommentData implements Serializable{
	private List<Comment> comment = new ArrayList<Comment>();
	private int likenum;
	private int commnetnum;
	private Long ownerid;
	private String owner_nickname;
	private String owner_head;
	private List<UserBaseInfo> likeUsers = new ArrayList<UserBaseInfo>();
	
	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	public int getLikenum() {
		return likenum;
	}
	public void setLikenum(int likenum) {
		this.likenum = likenum;
	}
	public int getCommnetnum() {
		return commnetnum;
	}
	public void setCommnetnum(int commnetnum) {
		this.commnetnum = commnetnum;
	}
	public Long getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(Long ownerid) {
		this.ownerid = ownerid;
	}
	public String getOwner_nickname() {
		return owner_nickname;
	}
	public void setOwner_nickname(String owner_nickname) {
		this.owner_nickname = owner_nickname;
	}
	public String getOwner_head() {
		return owner_head;
	}
	public void setOwner_head(String owner_head) {
		this.owner_head = owner_head;
	}
	public List<UserBaseInfo> getLikeUsers() {
		return likeUsers;
	}
	public void setLikeUsers(List<UserBaseInfo> likeUsers) {
		this.likeUsers = likeUsers;
	}
	
	
}

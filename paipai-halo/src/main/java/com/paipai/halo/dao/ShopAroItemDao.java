package com.paipai.halo.dao;

import org.springframework.stereotype.Component;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.domain.ItemLikes;
/**
  * @ClassName: ShopAroServiceImpl
  * @Description:业务逻辑实现类
  * @author Comsys-lianghaining1
  * @date 2015年3月17日 下午5:20:43
  *
 */
@Component
public class ShopAroItemDao extends BaseDaoImpl<ItemLikes, Long>  {
	private String namespace = "ItemLikes";
	 
	public ShopAroItemDao() {
	 super.setNamespace(this.namespace);
	 }
	/**
	 * 
	  * 根据itemid以及itemtype查总的点赞以及
	  * @Title: getItemLikes
	  * @Description: TODO
	  * @param @param itemLikes
	  * @param @return    设定文件
	  * @return ItemLikes    返回类型
	  * @throws
	 */
	public ItemLikes getItemLikes(ItemLikes itemLikes) {
		return getSqlSession().selectOne(namespace+".selectByitemid", itemLikes);
	}
	/**
	 * @return 
	 * 
	  * @Title: 更新点点赞数
	  * @Description: TODO
	  * @param @param itemLikes    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public int updateItemLikes(ItemLikes itemLikes) {
		return getSqlSession().update(namespace+".updatelikesnum", itemLikes);
	}
	/**
	 * @return 
	 * 
	  * @Title: 更新评论数
	  * @Description: TODO
	  * @param @param itemLikes    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public int updateItemcommnum(ItemLikes itemLikes) {
		return getSqlSession().update(namespace+".updatecommentnum", itemLikes);
	}
	public int updateminuscomm(ItemLikes itemLikes) {
		return getSqlSession().update(namespace+".updateminus", itemLikes);
	}
	/**
	 * 
	  * @Description: TODO
	  * @param     设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void updatenum() {
		getSqlSession().update(namespace+".updatenum");
	}
	public void updatenolikesnum(ItemLikes it) {
		getSqlSession().update(namespace+".updatenolikesnum", it);
	}
	

}

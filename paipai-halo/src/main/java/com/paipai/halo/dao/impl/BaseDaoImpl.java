package com.paipai.halo.dao.impl;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import com.paipai.halo.common.PageEntity;
import com.paipai.halo.common.PagingResult;

/**
 * baseDAO的实现基类
 * 
 * @author lhn
 * 
 * @param <T>
 * @param <PK>
 */
public class BaseDaoImpl<T, PK extends Serializable> extends
		SqlSessionDaoSupport implements BaseDao<T, PK> {
	public String namespace ;
	
	public String setNames ="setNames";

	// .xml定义文件中对应的sqlid
	public static final String SQLID_INSERT = "insert";
	public static final String SQLID_INSERT_BATCH = "insertBatch";
	public static final String SQLID_UPDATE = "update";
	public static final String SQLID_UPDATE_PARAM = "updateParam";
	public static final String SQLID_UPDATE_BATCH = "updateBatch";
	public static final String SQLID_DELETE = "delete";
	public static final String SQLID_DELETE_PARAM = "deleteParam";
	public static final String SQLID_DELETE_BATCH = "deleteBatch";
	public static final String SQLID_TRUNCATE = "truncate";
	public static final String SQLID_SELECT = "select";
	public static final String SQLID_SELECT_PK = "selectPk";
	public static final String SQLID_SELECT_PARAM = "selectParam";
	public static final String SQLID_SELECT_FK = "selectFk";
	public static final String SQLID_COUNT = "count";
	public static final String SQLID_COUNT_PARAM = "countParam";


	@Resource(name = "sqlSession")
	public void setSuperSqlSessionTemplate(SqlSessionTemplate sqlSession) {
		super.setSqlSessionTemplate(sqlSession);
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	@Override
	public Integer insert(T entity) throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().insert(namespace + "." + SQLID_INSERT,
					entity);
		} catch (Exception e) {
			rows =110;
			throw e;
			
		}
		return rows;
	}

	@Override
	public Integer update(T entity) throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().update(namespace + "." + SQLID_UPDATE,
					entity);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;
	}

	@Override
	public Integer updateParam(Map param) throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().update(namespace + "." + SQLID_UPDATE_PARAM,
					param);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;
	}

	@Override
	public Integer delete(PK primaryKey) throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().delete(namespace + "." + SQLID_DELETE,
					primaryKey);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;
	}

	@Override
	public Integer deleteParam(Map param) throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().delete(namespace + "." + SQLID_DELETE_PARAM,
					param);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;
	}

	@Override
	public Integer truncate() throws Exception {
		int rows = 0;
		try {
			rows = getSqlSession().delete(namespace + "." + SQLID_TRUNCATE);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;
	}

	@Override
	public Integer count() throws Exception {
		Integer result = 0;
		try {
			result = getSqlSession().selectOne(namespace + "." + SQLID_COUNT);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	@Override
	public Integer count(Object param) throws Exception {
		Integer result = 0;
		try {
			result = getSqlSession().selectOne(namespace + "." + SQLID_COUNT_PARAM,param);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	@Override
	public T get(PK primaryKey) {
		try {
			return getSqlSession().selectOne(namespace + "." + SQLID_SELECT_PK,primaryKey);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<T> select() {
		try {
			return getSqlSession().selectList(namespace + "." + SQLID_SELECT);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public List<T> selectParam(Map param) {
		try {
			return getSqlSession().selectList(namespace + "." + SQLID_SELECT_PARAM,param);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public PagingResult<T> selectPagination(PageEntity pageEntity) {
		try {
			int page = pageEntity.getPage() == null || "".equals(pageEntity.getPage()) ? 1 : pageEntity.getPage(); //默认为第一页
			int size = pageEntity.getSize() == null || "".equals(pageEntity.getSize()) ? 15 : pageEntity.getSize();; //默认每页15个
			
			RowBounds rowBounds = new RowBounds((page-1)*size, size);
			
			Map param = pageEntity.getParams();
			if (param != null) {
				param.put("orderColumn", pageEntity.getOrderColumn());
				param.put("orderTurn", pageEntity.getOrderTurn());
			}else {
				param = new HashMap();
				param.put("orderColumn", pageEntity.getOrderColumn());
				param.put("orderTurn", pageEntity.getOrderTurn());
			}
			
			List<T> resultList = getSqlSession().selectList(namespace + "." + SQLID_SELECT_PARAM,param,rowBounds);
			int totalSize = count(pageEntity.getParams());
			
			PagingResult<T> pagingResult = new PagingResult<T>();
			pagingResult.setCurrentPage(page);
			pagingResult.setTotalSize(totalSize);
			pagingResult.setResultList(resultList);
			return pagingResult;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	@Override
	public Integer insertBatch(List<T> list) throws Exception {
		try {
			return getSqlSession().insert(namespace + "." + SQLID_INSERT_BATCH,list);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
 
	@Override
	public Integer updateBatch(List<T> list) throws Exception {
		int rows = 0;
		try {
			for (T t : list) {
				rows = rows + getSqlSession().update(namespace + "." + SQLID_UPDATE, t);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return rows;

	}

	@Override
	public Integer deleteBatch(List<PK> list) throws Exception {
		try {
			return getSqlSession().delete(namespace + "." + SQLID_DELETE_BATCH,list);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
	public   List<Object> showvariables(){
		return getSqlSession().selectList(setNames+".showvariables");
	}
	/**
	 *设置字符集
	  * @Title: setNames
	  * @Description: TODO
	  * @param     设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void setNames(){
		getSqlSession().update(setNames+".setnames");
	}
	/**
	 * 日志打印
	 * @param sqlId
	 * @param param
	 */
	public void printLog(String sqlId,Object param){
		Configuration configuration = getSqlSession().getConfiguration();
		//sqlId为配置文件中的sqlid
		MappedStatement mappedStatement = configuration.getMappedStatement(sqlId);
		//param为传入到sql语句中的参数
		BoundSql boundSql = mappedStatement.getBoundSql(param);
		//得到sql语句
		String sql = boundSql.getSql().trim();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("info-sql: "+sdf.format(new Date())+"  "+sql);
	}
}

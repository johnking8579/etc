package com.paipai.halo.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.domain.UserBaseInfo;
/**
  * @ClassName: ShopAroServiceImpl
  * @Description:业务逻辑实现类
  * @author Comsys-lianghaining1
  * @date 2015年3月17日 下午5:20:43
  *
 */
@Component
public class ShopAroUserDao extends BaseDaoImpl<UserBaseInfo, Long>  {
	private String namespace = "UserBaseInfo";
	 
	public ShopAroUserDao() {
	 super.setNamespace(this.namespace);
	 }
	
	public void insertc(UserBaseInfo e)	{
		getSqlSession().insert(namespace + ".insert", e);
	}

	public List<UserBaseInfo> findUsrByMap(Long[] wid) {
		return getSqlSession().selectList(namespace+".selectEntryArray", wid);
	}

	public void updatestatusParam(Map<String, Object> map) {
	   getSqlSession().update(namespace + ".updateByParm" ,
			   map);
	}

	public List<UserBaseInfo> getBlUsers(int status) {
		return getSqlSession().selectList(namespace+".selectByst",status);
	}
	/**
	  * @Title: inOrupUser
	  * @Description: 更新or插入用户信息：如果没有则插入如果有则更新
	  * @param @param userBaseInfo
	  * @return void    返回类型
	  * @throws
	 */
	public int inOrupUser(UserBaseInfo userBaseInfo) {
		return getSqlSession().update(namespace + ".inOrupUser" ,userBaseInfo);
	}


}

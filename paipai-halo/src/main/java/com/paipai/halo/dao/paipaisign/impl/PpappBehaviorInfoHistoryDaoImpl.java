/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign.impl;

import org.springframework.stereotype.Repository;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoHistoryDao;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfoHistory;

/**
 * PpappBehaviorInfoHistoryDao 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Repository("ppappBehaviorInfoHistoryDao")
public class PpappBehaviorInfoHistoryDaoImpl extends BaseDaoImpl<PpappBehaviorInfoHistory,String> implements PpappBehaviorInfoHistoryDao {
	private final static String NAMESPACE = "com.paipai.halo.dao.paipaisign.PpappBehaviorInfoHistoryDao";
	
	public PpappBehaviorInfoHistoryDaoImpl() {
		 super.setNamespace(NAMESPACE);
	}
	
	
	//返回本DAO命名空间,并添加statement
	public String getNameSpace(String statement) {
		return NAMESPACE + statement;
	}
		
}
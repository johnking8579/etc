package com.paipai.halo.dao;

import org.springframework.stereotype.Component;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.domain.LikeLogInfo;
/**
  * @ClassName: ShopAroServiceImpl
  * @Description:业务逻辑实现类
  * @author Comsys-lianghaining1
  * @date 2015年3月17日 下午5:20:43
  *
 */
@Component
public class ShopAroLikeLogDao extends BaseDaoImpl<LikeLogInfo, Long>  {
	private String namespace = "LikeLogInfo";
	 
	public ShopAroLikeLogDao() {
	 super.setNamespace(this.namespace);
	 }
	
	/**
	 * 
	  * 根据wid itemid itemtype查出个人记录
	  * @Title: getLikeLogInfo
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @return    设定文件
	  * @return LikeLogInfo    返回类型
	  * @throws
	 */
	public LikeLogInfo getLikeLogInfo(LikeLogInfo lk) {
		return 	getSqlSession().selectOne(namespace+".selectByitemeid", lk);
	}
	/**
	 * 
	  * @Title: 更新或者插入一条个人点赞记录
	  * @Description: 1
	  * @param @param likes
	  * @param @return    设定文件
	  * @return int    返回类型
	  * @throws
	 */
	public int saveOrupLike(LikeLogInfo likes) {
		return getSqlSession().update(namespace+".inOrupLike", likes);
	}
	/**
	 * 
	  * @Title: 取消点赞
	  * @Description: 取消点赞
	  * @param
	  * @return返回影响行数
	  * @throws
	 */
	public int upCancelLike(LikeLogInfo likeinf) {
		return getSqlSession().update(namespace+".upCancelLike", likeinf);
	}

}

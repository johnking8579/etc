package com.paipai.halo.dao.paipaisign.impl;


/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */



import org.springframework.stereotype.Repository;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.dao.paipaisign.PpappSignLogDao;
import com.paipai.halo.domain.paipaisign.PpappSignLog;

/**
 * PpappSignLogDao 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Repository("ppappSignLogDao")
public class PpappSignLogDaoImpl extends BaseDaoImpl<PpappSignLog,String> implements PpappSignLogDao {
	private final static String NAMESPACE = "com.paipai.halo.dao.paipaisign.PpappSignLogDao";
	
	public PpappSignLogDaoImpl() {
		 super.setNamespace(NAMESPACE);
	}
	
	//返回本DAO命名空间,并添加statement
	public String getNameSpace(String statement) {
		return NAMESPACE + statement;
	}
		
}
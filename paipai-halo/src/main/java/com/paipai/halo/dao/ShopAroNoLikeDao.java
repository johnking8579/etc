package com.paipai.halo.dao;

import org.springframework.stereotype.Component;

import com.paipai.halo.dao.impl.BaseDaoImpl;
import com.paipai.halo.domain.NoLikeInfo;
/**
 * 
  * @ClassName: ShopAroNoLikeDao
  * @Description: 踩
  * @author Comsys-lianghaining1
  * @date 2015年4月23日 上午10:54:05
  *
 */
@Component
public class ShopAroNoLikeDao extends BaseDaoImpl<NoLikeInfo, Long>{
	private String namespace = "NoLikeInfo";
	 
	public ShopAroNoLikeDao() {
	 super.setNamespace(this.namespace);
	 }

	/**
	 * 
	  * 根据wid itemid itemtype查出个人踩记录
	  * @Title: getLikeLogInfo
	  * @Description: TODO
	  * @param @param itemid
	  * @param @param itemtype
	  * @param @param wid
	  * @param @return    设定文件
	  * @return LikeLogInfo    返回类型
	  * @throws
	 */
	public NoLikeInfo getNoLikeInfo(NoLikeInfo lk) {
		return 	getSqlSession().selectOne(namespace+".selectByitemeid", lk);
	}


}

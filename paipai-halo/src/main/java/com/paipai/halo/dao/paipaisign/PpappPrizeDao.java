/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.dao.paipaisign;

import java.util.List;
import java.util.Map;

import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.domain.paipaisign.PpappPrize;

/**
 * PpappPrizeDao 接口
 * @author J-ONE
 * @since 2015-04-09
 */
public interface PpappPrizeDao extends BaseDao<PpappPrize,String>{
	//自定义扩展
	/**
	 * 根据MAP条件查找单个对象
	 * @param paramMap
	 * @param readOnly
	 * @return
	 * @throws Exception
	 */
	public PpappPrize selectEntryByMap(Map<String, Object> paramMap, boolean readOnly) throws Exception ;
	
	/**
	 * 根据MAP条件查找对象列表
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List<PpappPrize> selectListByMap(Map<String, Object> paramMap) throws Exception ;
	
	

}
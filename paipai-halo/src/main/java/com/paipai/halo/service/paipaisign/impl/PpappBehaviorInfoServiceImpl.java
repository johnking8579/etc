/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;
import com.paipai.halo.service.paipaisign.PpappBehaviorInfoService;

/**
 * PpappBehaviorInfoService 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappBehaviorInfoService")
@Transactional
public class PpappBehaviorInfoServiceImpl implements PpappBehaviorInfoService {
	
	@Resource private PpappBehaviorInfoDao ppappBehaviorInfoDao;
	
	public BaseDao<PpappBehaviorInfo,String> getDao() {
		return ppappBehaviorInfoDao;
	}

	@Override
	public List<PpappBehaviorInfo> behaviorInfo(long wid, int activeId, int type, String beginTime, String endTime) {
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("wid", wid);
		param.put("activeId", activeId);
		if(type != 0)param.put("type", type);
		param.put("beginTime", beginTime);
		param.put("endTime", endTime);

		return this.ppappBehaviorInfoDao.behaviorInfo(param);
	}
	
	
	
}
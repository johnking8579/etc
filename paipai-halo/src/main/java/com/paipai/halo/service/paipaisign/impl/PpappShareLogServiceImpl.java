/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonObject;
import com.jd.jim.cli.Cluster;
import com.paipai.halo.common.JIMConstant;
import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.dao.paipaisign.PpappActivePointDao;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao;
import com.paipai.halo.dao.paipaisign.PpappShareLogDao;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;
import com.paipai.halo.domain.paipaisign.PpappShareLog;
import com.paipai.halo.service.paipaisign.PpappActivePointService;
import com.paipai.halo.service.paipaisign.PpappShareLogService;
import com.paipai.halo.service.paipaisign.PpappSignLogService;

/**
 * PpappShareLogService 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappShareLogService")
@Transactional
public class PpappShareLogServiceImpl  implements PpappShareLogService {
	
	@Resource private PpappShareLogDao ppappShareLogDao;
	@Resource private Cluster jimClient;
	@Resource private PpappBehaviorInfoDao ppappBehaviorInfoDao;
	@Resource private PpappActivePointDao ppappActivePointDao;
	@Resource private PpappActivePointService ppappActivePointService;
	@Resource private PpappSignLogService ppappSignLogService;
	
	public BaseDao<PpappShareLog,String> getDao() {
		return ppappShareLogDao;
	}
	
	
	@Override
	public JsonObject share(long wid, int activeId, String mk, String mk2, String ip,int type) throws Exception {
		JsonObject jsonObject = new JsonObject();
		long now = new Date().getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		int signDays = this.ppappActivePointService.selectSignPointCache(wid, activeId).getSignDays();
		long currentPoint = this.ppappActivePointService.selectSignPointCache(wid, activeId).getCurrentPoint();
		
		
		//判断今天有没有分享过
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("wid", wid);
		param.put("activeId", activeId);
		param.put("insertDay", format.format(new Date()));
		List<PpappShareLog> list = this.ppappShareLogDao.selectParam(param);
		if(list.size() > 0){//今天已经分享过，则只可以分享，不送橡果
			
		}else{
			currentPoint = currentPoint + 2;
			
			//更新数据库PpappActivePoint
			param.clear();
			param.put("wid", wid);
			param.put("activeId", activeId);
			param.put("forUpdate", 1);
			
			List<PpappActivePoint> ppappActivePoints = this.ppappActivePointDao.selectParam(param);
			PpappActivePoint ppappActivePoint = ppappActivePoints.get(0);
			ppappActivePoint.setCurrentPoint(ppappActivePoint.getCurrentPoint() + 2);
			ppappActivePoint.setTotalPoint(ppappActivePoint.getTotalPoint() + 2);
			ppappActivePoint.setUpdateTime(now);
			
			this.ppappActivePointDao.update(ppappActivePoint);
		}
		
		
		
		
		//签到天数加1，找本次签到奖励的数量
		jsonObject.addProperty("num", 2);
		jsonObject.addProperty("currentPoint", currentPoint);
		jsonObject.addProperty("signDays", signDays);
		jsonObject.addProperty("activeId", activeId);
				
				
		
		
		//记录分享日志
		PpappShareLog ppappShareLog = new PpappShareLog();
		String shareLogId = UUID.randomUUID() + "_" + wid;
		ppappShareLog.setId(shareLogId);
		ppappShareLog.setWid(wid);
		ppappShareLog.setActiveId(activeId);
		ppappShareLog.setType(type);
		ppappShareLog.setInsertTime(now);
		ppappShareLog.setInsertDay(format.format(new Date()));
		ppappShareLog.setMk2(mk2);
		ppappShareLog.setMk(mk);
		ppappShareLog.setIp(ip);
		
		this.ppappShareLogDao.insert(ppappShareLog);
		
		
		//记录ppapp_behavior_info
 		PpappBehaviorInfo behaviorInfo = new PpappBehaviorInfo();
 		behaviorInfo.setId(UUID.randomUUID() + "_" + wid);
 		behaviorInfo.setWid(wid);
 		behaviorInfo.setActiveId(activeId);
 		behaviorInfo.setCurrentNum(currentPoint);
 		behaviorInfo.setNum(2L);
 		behaviorInfo.setInsertTime(now);
 		behaviorInfo.setInsertDay(format.format(new Date()));
 		behaviorInfo.setMk2(mk2);
 		behaviorInfo.setMk(mk);
 		behaviorInfo.setIp(ip);
 		behaviorInfo.setType(2);
 		JsonObject ext = new JsonObject();
 		ext.addProperty("fid", shareLogId);
 		ext.addProperty("ftype", 1);//冗余，没有实际意义
 		ext.addProperty("signDays", signDays);
 		ext.addProperty("currentPoint", currentPoint);
 		behaviorInfo.setExt(ext.toString());
 		this.ppappBehaviorInfoDao.insert(behaviorInfo);
		
		//删除缓存
		jimClient.del(JIMConstant.SIGN_POINT_CACHE + wid + "_" + activeId);
		
		return jsonObject;
	}
	
}
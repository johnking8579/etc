/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.service.paipaisign.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.jim.cli.Cluster;
import com.paipai.halo.dao.impl.BaseDao;
import com.paipai.halo.dao.paipaisign.PpappSignLogDao;
import com.paipai.halo.domain.paipaisign.PpappSignLog;
import com.paipai.halo.service.paipaisign.PpappSignLogService;

/**
 * PpappSignLogService 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappSignLogService")
@Transactional
public class PpappSignLogServiceImpl implements PpappSignLogService {
	
	@Resource private PpappSignLogDao ppappSignLogDao;
	@Resource private Cluster jimClient;
	
	
	public BaseDao<PpappSignLog,String> getDao() {
		return ppappSignLogDao;
	}

	@Override
	public List<PpappSignLog> getByParam(Map<String, Object> param) {
		return this.ppappSignLogDao.selectParam(param);
	}

	@Override
	public int getSignDays(long wid, int activeId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("wid", wid);
		param.put("activeId", activeId);
		List<PpappSignLog> list = this.ppappSignLogDao.selectParam(param);
		if(null == list || list.size() == 0) return 0;
		
		GregorianCalendar calendar = new GregorianCalendar();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		
		int SignDays = 0;
		if(list.get(0).getInsertDay().equals(format.format(new Date()))){//今天
			SignDays = 1;
		}else if(list.get(0).getInsertDay().equals(format.format(calendar.getTime()))){//昨天
			
		}else{//中间断签了
			return 0;
		}
		
		Set<String> set = new HashSet<String>();
		for(int i = SignDays ; i < list.size() ; i ++){
			set.add(list.get(i).getInsertDay());
		}
	
		return SignDays + set.size();
	}
	
}
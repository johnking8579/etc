package com.paipai.halo.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.jd.jim.cli.Cluster;
import com.jd.jmq.client.producer.MessageProducer;
import com.jd.jmq.common.exception.JMQException;
import com.jd.jmq.common.message.Message;
import com.paipai.halo.common.PageEntity;
import com.paipai.halo.common.Pager;
import com.paipai.halo.common.PagingResult;
import com.paipai.halo.common.SerializeUtil;
import com.paipai.halo.common.jqgrid.ColumnMapper;
import com.paipai.halo.common.jqgrid.Rule;
import com.paipai.halo.dao.ShopAroCommentDao;
import com.paipai.halo.dao.ShopAroItemDao;
import com.paipai.halo.domain.Comment;
import com.paipai.halo.domain.ItemLikes;
import com.paipai.halo.domain.JmqMsgInfo;
import com.paipai.halo.service.impl.ShopAroServiceImpl;
import com.paipai.halo.web.JsonOutput;

@Component
public class CommentService implements ColumnMapper<Map>{
	
	@Resource
	private Cluster jimClient;
	@Resource
	private ShopAroCommentDao commentDao;
	@Resource
	private ShopAroItemDao itemDao;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Resource(name = "producer")
    private MessageProducer producer;
    @Value("#{configProperties['jmq.producer.topic.c']}")
    private String topic = "halo_comment";
    
    
	public Pager<Map> find(String orderField, String order, int offset, int pageSize) {
		return commentDao.find(orderField, order, offset, pageSize);
	}

	@Override
	public List<String> mapColumn(Map m) {
		List<String> l = new ArrayList<String>();
		l.add(String.valueOf(m.get("itemid")));
		l.add(String.valueOf(m.get("itemtype")).equals("1")?"App文章":"非App文章");
		l.add(String.valueOf(m.get("title")));
		l.add(String.valueOf(m.get("context")));
		l.add(String.valueOf(m.get("NICKNAME")));
		l.add(String.valueOf(m.get("wid")));
		l.add(String.valueOf(m.get("isshow")).equals("1")?"通过":(String.valueOf(m.get("isshow")).equals("0")?"未通过":"删除"));
		Long comTime = (Long)m.get("commenttime");
		if(comTime != null)
			l.add(sdf.format(new Date(comTime*1000)));
		else
			l.add("");
		return l;
	}

	@Override
	public String getRowId(Map m) {
		return String.valueOf(m.get("id"));
	}

	public Pager<Map> filterSearch(List<Rule> rules, String orderField, String order, int offset, int pageSize) {
		return commentDao.filterSearchBeginWith(rules, orderField, order, offset, pageSize);
	}
	
	public void updateIsshow(List<Long> ids, int isshow) {
		commentDao.updateIsshow(ids, isshow);
		//更新通过时同时发送MQ
		if(isshow==1){
			setCache(ids);
		}else {
			removeCache(ids);
		}
		//发送mq
		try {
		sendCommentsMq(ids,String.valueOf(isshow==1?1:0));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void setCache(List<Long> commentIds) {

		for(long l : commentIds)	{
			Comment c = commentDao.get(l);
			if(c != null)	{
				 ItemLikes itemLikes = new ItemLikes();
				 itemLikes.setItemid(c.getItemid());
				 itemLikes.setItemtype(c.getItemtype());
				 setCommentListCach(itemLikes);
				 try {
					 ItemLikes it = itemDao.getItemLikes(itemLikes);
				     it.setCommentnum(it.getCommentnum()+1);
					 this.itemDao.updateItemcommnum(it);
					 setItemLike(it,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		
		}
	
	}

	private void removeCache(List<Long> commentIds)	{
		for(long l : commentIds)	{
			Comment c = commentDao.get(l);
			if(c != null)	{
				//删除缓存
				 ItemLikes itemLikes = new ItemLikes();
				 itemLikes.setItemid(c.getItemid());
				 itemLikes.setItemtype(c.getItemtype());
				 setCommentListCach(itemLikes);
				 try {
					 ItemLikes it = itemDao.getItemLikes(itemLikes);
				     it.setCommentnum((it.getCommentnum()-1)<0?0:(it.getCommentnum()-1));
					 this.itemDao.updateItemcommnum(it);
					 setItemLike(it,false);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		
		}
	}
	/**
	 * 
	  * @Title:将文章的前20条放入缓存
	  * @Description: TODO
	  * @param @param itemLikes    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	private void setCommentListCach(ItemLikes itemLikes) {
		PageEntity pe = new PageEntity();
		pe.setPage(1);
		pe.setSize(20);
		pe.setOrderColumn("commenttime");
		pe.setOrderTurn("desc");
		Map params = new HashMap<String, String>();
		params.put("itemid", itemLikes.getItemid());
		params.put("id", 0);
		params.put("itemtype", itemLikes.getItemtype());
		pe.setParams(params);
		PagingResult<Comment> pageRanges  = null;
		pageRanges = this.commentDao.selectPagination(pe);
		if(jimClient.exists((ShopAroServiceImpl.JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes())){
			jimClient.del((ShopAroServiceImpl.JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes());
			jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}else {
			jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_COMMLIST_BEGIN+itemLikes.getItemid()+itemLikes.getItemtype()).getBytes(), SerializeUtil.serialize(pageRanges), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}
		
	}
	/**
	 * 
	  * @Title: 该该用 于向缓存中set最新的评论点赞信息
	  * @Description: TODO
	  * @param     设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void setItemLike(ItemLikes itemLikes,boolean isupdate) {
		//查出最新的点赞以及评论个数。放入缓存
		ItemLikes itl = null;
		if(isupdate){
			itl = this.itemDao.getItemLikes(itemLikes);
		}else {
			itl = itemLikes;
		}
		if(itl!=null){
				if(jimClient.exists((ShopAroServiceImpl.JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes())){
					jimClient.del((ShopAroServiceImpl.JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes());
					jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes(), SerializeUtil.serialize(itl), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
				}else {
					jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_BEGIN+itl.getItemid()+itl.getItemtype()).getBytes(), SerializeUtil.serialize(itl), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
				}
		}
	}
	/**
	 * 
	  * @Title: 发送MQ信息
	  * @Description: 发送文章的评论以及点赞数
	  * @param @param itemid 文章ID
	  * @param @param itemtype    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void sendCommentsMq(List<Long> ids,String islike){
		//查出点赞的信息发一个Mq type:1:占先2：评论
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("ids", ids);
		List<Comment> comments = this.commentDao.selectByidParam(map) ;
		for(Comment c:comments){
			JsonOutput out = new JsonOutput();
			ItemLikes i = getLikeNum(c.getItemid(), c.getItemtype());
			if(i!=null){
				JmqMsgInfo jmqMsgInfo = new JmqMsgInfo();
				jmqMsgInfo.setBizData(i);
				jmqMsgInfo.setBizType("2");
				jmqMsgInfo.setContext(c.getContext()==null?"":c.getContext());
				jmqMsgInfo.setIslike(islike);
				jmqMsgInfo.setWid(c.getWid());
				jmqMsgInfo.setTime(c.getCommenttime());
				out.setData(new Gson().toJsonTree(jmqMsgInfo));
				Message message = new Message(topic,new Gson().toJson(out), topic+c.getItemid());
		        try {
					producer.send(message);
				} catch (JMQException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public ItemLikes getLikeNum(String itemid, Integer itemtype) {
		//查看缓存中是否有该内容数据
		ItemLikes commonLikes = null;
		if(jimClient.exists((ShopAroServiceImpl.JIM_ITEM_BEGIN+itemid+itemtype).getBytes())){
			 commonLikes = (ItemLikes) SerializeUtil.unserialize(jimClient.get((ShopAroServiceImpl.JIM_ITEM_BEGIN+itemid+itemtype).getBytes()));
			 if(commonLikes==null){
				//如果没有。查数据库并写入缓存 
					ItemLikes itemLikes = new ItemLikes();
					itemLikes.setItemid(itemid);
					itemLikes.setItemtype(itemtype);
					// 取消点赞前先查一下是否已经有数据。理论上肯定有
					commonLikes = this.itemDao.getItemLikes(itemLikes);
					jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_BEGIN+itemid+itemtype).getBytes(), SerializeUtil.serialize(commonLikes), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
			 }
		}else {
			//如果没有。查数据库并写入缓存 
			ItemLikes itemLikes = new ItemLikes();
			itemLikes.setItemid(itemid);
			itemLikes.setItemtype(itemtype);
			// 取消点赞前先查一下是否已经有数据。理论上肯定有
			commonLikes = this.itemDao.getItemLikes(itemLikes);
			jimClient.setEx((ShopAroServiceImpl.JIM_ITEM_BEGIN+itemid+itemtype).getBytes(), SerializeUtil.serialize(commonLikes), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		}
		
		return commonLikes;
	}
}

package com.paipai.halo.service.paipaisign.impl;


/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonObject;
import com.jd.jim.cli.Cluster;
import com.paipai.halo.common.JIMConstant;
import com.paipai.halo.common.SerializeUtil;
import com.paipai.halo.dao.paipaisign.PpappActivePointDao;
import com.paipai.halo.dao.paipaisign.PpappBehaviorInfoDao;
import com.paipai.halo.dao.paipaisign.PpappSignLogDao;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;
import com.paipai.halo.domain.paipaisign.PpappSignLog;
import com.paipai.halo.domain.paipaisign.SignPointCachePo;
import com.paipai.halo.service.impl.ShopAroServiceImpl;
import com.paipai.halo.service.paipaisign.PpappActivePointService;
import com.paipai.halo.service.paipaisign.PpappSignLogService;

/**
 * PpappActivePointService 实现类
 * @author J-ONE
 * @since 2015-04-09
 */
@Service("ppappActivePointService")
@Transactional
public class PpappActivePointServiceImpl implements PpappActivePointService {
	
	@Resource private PpappActivePointDao ppappActivePointDao;
	@Resource private PpappBehaviorInfoDao ppappBehaviorInfoDao;
	@Resource private PpappSignLogDao ppappSignLogDao;
	@Resource private Cluster jimClient;
	@Resource private PpappSignLogService ppappSignLogService;
	
	@Override
	public List<PpappActivePoint> selectAll(){
		return ppappActivePointDao.select();
	}

	@Override
	public int add(PpappActivePoint activePoint) throws Exception {
		return ppappActivePointDao.insert(activePoint);
	}
	
	
	@Override
	public List<PpappActivePoint> selectParam(Map<String, Object> param){
		return ppappActivePointDao.selectParam(param);
	}

	@Override
	public JsonObject sign(long wid, int activeId, String mk, String mk2, String ip) throws Exception {
		System.out.println("mk2: " + mk2);
		
		JsonObject jsonObject = new JsonObject();
		long now = new Date().getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		//读取缓存--连续签到的天数
//		String dayStr = jimClient.get("sign_signDayStatistics_" + wid + "_" + activeId);
		int day = selectSignPointCache(wid, activeId).getSignDays() + 1;
		System.out.println("签到后："+day);
		
		//签到天数加1，找本次签到奖励的数量
		long num = 1;
		if(day > 1){
			num = (day - 1) * 2;
		}
		long currentPoint = selectSignPointCache(wid, activeId).getCurrentPoint() + num;
		
		jsonObject.addProperty("num", num);
		jsonObject.addProperty("currentPoint", currentPoint);
		jsonObject.addProperty("signDays", day);
		jsonObject.addProperty("activeId", activeId);
				
				
		//更新数据库ActivePoints
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("wid", wid);
		param.put("activeId", activeId);
		param.put("forUpdate", 1);
		List<PpappActivePoint> ppappActivePoints = this.ppappActivePointDao.selectParam(param);
		
		JsonObject ext = new JsonObject();
 		
 		ext.addProperty("ftype", 1);//冗余，没有实际意义
 		ext.addProperty("signDays", day);
 		ext.addProperty("currentPoint", currentPoint);
 		if(ppappActivePoints.size() == 0){
 			ext.addProperty("totalPoint", num);
 		}else{
 			System.out.println(ppappActivePoints.get(0).getTotalPoint() + "--------" + ppappActivePoints.get(0).getCurrentPoint());
 			ext.addProperty("totalPoint", ppappActivePoints.get(0).getTotalPoint() + num);
 		}
 		
		if(ppappActivePoints.size() == 0){
			PpappActivePoint activePoint = new PpappActivePoint();
			activePoint.setId(activeId + "_" + wid);
			activePoint.setWid(wid);
			activePoint.setActiveId(activeId);
			activePoint.setCurrentPoint(num);
			activePoint.setTotalPoint(num);
			activePoint.setUsedPoint(num);
			activePoint.setMk2(mk2);
			activePoint.setMk(mk);
			activePoint.setIp(ip);
			activePoint.setInsertTime(now);
			activePoint.setUpdateTime(now);
			
			this.ppappActivePointDao.insert(activePoint);
		}else{
			PpappActivePoint ppappActivePoint = ppappActivePoints.get(0);
			ppappActivePoint.setCurrentPoint(ppappActivePoint.getCurrentPoint() + num);
			ppappActivePoint.setTotalPoint(ppappActivePoint.getTotalPoint() + num);
			ppappActivePoint.setUpdateTime(now);
			
			this.ppappActivePointDao.update(ppappActivePoint);
		}
		
		//记录签到日志
		PpappSignLog ppappSignLog = new PpappSignLog();
		String signLogId = UUID.randomUUID() + "_" + wid;
		ppappSignLog.setId(signLogId);
		ppappSignLog.setWid(wid);
		ppappSignLog.setActiveId(activeId);
		ppappSignLog.setInsertTime(now);
		ppappSignLog.setInsertDay(format.format(new Date()));
		ppappSignLog.setMk2(mk2);
		ppappSignLog.setMk(mk);
		ppappSignLog.setIp(ip);
		
		this.ppappSignLogDao.insert(ppappSignLog);
		
		
		//记录ppapp_behavior_info
 		PpappBehaviorInfo behaviorInfo = new PpappBehaviorInfo();
 		behaviorInfo.setId(UUID.randomUUID() + "_" + wid);
 		behaviorInfo.setWid(wid);
 		behaviorInfo.setActiveId(activeId);
 		behaviorInfo.setCurrentNum(currentPoint);
 		behaviorInfo.setNum(num);
 		behaviorInfo.setInsertTime(now);
 		behaviorInfo.setInsertDay(format.format(new Date()));
 		behaviorInfo.setMk2(mk2);
 		behaviorInfo.setMk(mk);
 		behaviorInfo.setIp(ip);
 		behaviorInfo.setType(1);
 		
 		ext.addProperty("fid", signLogId);
 		behaviorInfo.setExt(ext.toString());
 		this.ppappBehaviorInfoDao.insert(behaviorInfo);
		
		//删除缓存
//		jimClient.del(JIMConstant.SIGN_POINTSTATISTICE + wid + "_" + activeId);
//		jimClient.del(JIMConstant.SIGN_SIGNDAYSTATISTICE + wid + "_" + activeId);
		
 		jimClient.del(JIMConstant.SIGN_POINT_CACHE + wid + "_" + activeId);
 		
		return jsonObject;
	}

	@Override
	public long selectCurrentPoint(long wid, int activeId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("wid", wid);
		param.put("activeId", activeId);
		List<PpappActivePoint> list = this.selectParam(param);
		
		if(null == list || list.size() == 0){
			return 0;
		}else{
			return list.get(0).getCurrentPoint();
		}
	}
	
	@Override
	public SignPointCachePo selectSignPointCache(long wid, int activeId) {
		
		String key = JIMConstant.SIGN_POINT_CACHE + wid + "_" + activeId;
		SignPointCachePo signPointCachePo = null;
		
		if(jimClient.exists(key.getBytes())){
			signPointCachePo = (SignPointCachePo) SerializeUtil.unserialize(jimClient.get(key.getBytes()));
		}else{
			long currentPoint = selectCurrentPoint(wid, activeId);
			int signDays = this.ppappSignLogService.getSignDays(wid, activeId);
			System.out.println("数据库中查询signDays：" + signDays + "; currentPoint : " + currentPoint);
			signPointCachePo = new SignPointCachePo(signDays, currentPoint);
		}

		jimClient.setEx(key.getBytes(), SerializeUtil.serialize(signPointCachePo), ShopAroServiceImpl.JIM_EFFECTIVE_TIME, TimeUnit.DAYS);
		
		return signPointCachePo;
	}
	
}
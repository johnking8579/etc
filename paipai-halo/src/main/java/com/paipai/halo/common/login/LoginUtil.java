package com.paipai.halo.common.login;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.halo.client.ao.login.LoginClient;
import com.paipai.halo.client.ao.login.protocal.CheckLoginByWgUidResp;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.StringUtil;
import com.paipai.halo.domain.login.AppToken;

public class LoginUtil {
	 /**
     * 运行日志
     */
    public static Logger run = LogManager.getLogger("run");
    /**
	 * 
	 * @param wid
	 *            通过COOKIE获取的WID
	 * @param sk
	 *            通过COOKIE获取的SK
	 * @return true:已登录 false：未登录
	 * @throws Exception
	 */
	public static boolean checkLoginForPc(long wid, String sk, String isIdc) throws Exception {
		run.info("wid= " + wid + ",sk = " + sk ) ;
		if (Config.isDevEnv()) {
			return true ;
		}
		long longinlevel = LoginClient.getUserSimpleProfileinf(
				String.valueOf(System.currentTimeMillis()), wid, sk);		
		if (longinlevel == 0) {
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @param wid
	 *            通过COOKIE获取的WID
	 * @param sk
	 *            通过COOKIE获取的SK
	 * @return true:已登录 false：未登录
	 * @throws Exception
	 */
	public static boolean checkLoginForH5(long wid, String sk) throws Exception {
		run.info("wid= " + wid + ",sk = " + sk ) ;		
		if (Config.isDevEnv()) {
			return true ;
		}
		CheckLoginByWgUidResp resp = LoginClient.checkLoginByWgUid(
				String.valueOf(System.currentTimeMillis()), wid, sk);
		if (resp != null && resp.getResult() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param appTokenStr token串
	 * @param mk 设备号
	 * @return
	 * @throws Exception
	 */
	public static boolean checkLoginForApp(String appTokenStr, String mk)
			throws Exception {
		if (checkLoginForToken(appTokenStr, mk) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 获取token对象
	 * @param appTokenStr token串
	 * @param mk 设备号
	 * @return 登录返回apptoken对象，未登录返回null
	 * @throws Exception
	 */
	public static AppToken checkLoginForToken(String appTokenStr, String mk)
			throws Exception {
		run.info("appToken= " + appTokenStr + ",mk = " + mk ) ;
		AppToken appToken = null;
		if (StringUtil.isEmpty(appTokenStr) || appTokenStr.length() < 10
				|| StringUtils.isEmpty(mk)) {
			return null;
		}
		appToken = TokenUtil.getAppToken(appTokenStr);
		if (null == appToken) {
			return null;
		}
		if (-1 != appToken.getDeadline()
				&& appToken.getDeadline() < System.currentTimeMillis()) {
			return null;
		}	
		if (Config.isDevEnv()) {
			return appToken ;
		}
		CheckLoginByWgUidResp resp = LoginClient.checkLoginByWgUid(mk,
				appToken.getWid(), appToken.getLsk());
		if (resp == null || resp.getResult() != 0) {
			return null;
		}
		return appToken;
	}

}

//package com.paipai.halo.common;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.UnsupportedEncodingException;
//import java.net.URI;
//import java.util.HashMap;
//import java.util.List;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import com.jcloud.jss.Credential;
//import com.jcloud.jss.JingdongStorageService;
//import com.jcloud.jss.client.ClientConfig;
//import com.jcloud.jss.domain.Bucket;
//import com.jcloud.jss.domain.ObjectListing;
//import com.jcloud.jss.service.ObjectService;
//
///**
// * Jss云存储服务类
// * User: zhaoming
// * Date: 13-8-28
// * Time: 下午4:49
// * To change this template use File | Settings | File Templates.
// */
//public class JssOper{
//
//    private static final Logger logger = LogManager.getLogger();
//
//    private String endpoint;
//    
//    private String accessKey;
//
//    private String secretKey;
//
//    private JingdongStorageService jdStorageService;
//
//    private HashMap<String, String> exceptionHm;
//
//    /**
//     * 初始化身份验证获得京东云存储服务
//     */
//    public void init(){
//        Credential credential = new Credential(accessKey, secretKey);
//        ClientConfig config = new ClientConfig();
//        config.setEndpoint(endpoint);
//        jdStorageService = new JingdongStorageService(credential,config);
//
//        exceptionHm = new HashMap<String, String>();
//        exceptionHm.put("AccessDenied","访问被拒绝");
//        exceptionHm.put("AccountProblem","帐号被冻结");
//        exceptionHm.put("BadDigest","提供的 Content-MD5 值与服务端计算的不匹配");
//        exceptionHm.put("BucketAccessDenied","没有权限访问目标Bucket");
//        exceptionHm.put("BucketAlreadyExists","要创建的 Bucket 已存在");
//        exceptionHm.put("BucketNotEmpty","要删除的 Bucket 不为空");
//        exceptionHm.put("EntityTooLarge","上传的数据大小超过了限制");
//        exceptionHm.put("IncompleteBody","上传的数据大小小于 HTTP Header 中的 Content-Length");
//        exceptionHm.put("InvalidBucketName","要创建的 Bucket 名称不合法");
//        exceptionHm.put("KeyTooLong","Object Key 长度过长");
//        exceptionHm.put("MissingContentLength","缺少 HTTP Header Content-Length");
//        exceptionHm.put("NoSuchBucket","请求的 Bucket 不存在");
//        exceptionHm.put("NoSuchKey","请求的 Object 不存在");
//        exceptionHm.put("TooManyBuckets","要创建的 Bucket 数目超过了极限");
//        exceptionHm.put("RequestTimeTooSkewed","请求的时间戳与服务器本地时间差距过大");
//        exceptionHm.put("InvalidPart","Complete Multipart Upload 时，HTTP请求中的Part在云存储中找不到");
//        exceptionHm.put("InvalidEntity","HTTP请求的Entity无效");
//        exceptionHm.put("InvalidPartNumber","Complete Multipart Upload 时，HTTP请求中的Partnumber在规定范围外");
//        exceptionHm.put("KeyAlreadyExists","Object Key 已存在");
//    }
//
//    /**
//     * 获取Jss云存储服务类
//     * @return
//     */
//    public JingdongStorageService getJssService() {
//        if(jdStorageService == null){
//            this.init();
//        }
//        return jdStorageService;
//    }
//
//    /**
//     * 创建桶
//     * @param bucketName 桶名
//     */
//    public void createBucket(String bucketName){
//        jdStorageService.bucket(bucketName).create();
//    }
//
//    /**
//     * 获取全部桶
//     * @return 桶列表
//     */
//    public List<Bucket> getBuckets(){
//        return jdStorageService.listBucket();
//    }
//
//    /**
//     * 删除桶
//     * @param bucketName 桶名
//     */
//    public void deleteBucket(String bucketName) {
//        jdStorageService.bucket(bucketName).delete();
//    }
//
//    /**
//     * 以文本形式上传到云端
//     * @param bucketName 桶名
//     * @param objKey 对象的KEY
//     * @param content 文本内容
//     */
//    public void uploadFile(String bucketName, String objKey, String content){
//    	InputStream inputStringStream = null;
//        try{
//            inputStringStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
//            jdStorageService.bucket(bucketName).object(objKey).entity(content.getBytes("UTF-8").length, inputStringStream).put();
//        } catch (UnsupportedEncodingException e) {
//            logger.error(e);
//            throw new RuntimeException();
//        } finally {
//        	try{
//        		if(inputStringStream!=null){
//        			inputStringStream.close();
//        		}
//        		
//        	} catch(IOException e) {
//        	}
//        }
//    }
//
//    /**
//     * 以流形式上传到云端
//     * @param bucketName 桶名
//     * @param objKey 对象的KEY
//     * @param inputStream 文本内容
//     */
//    public void uploadFile(String bucketName, String objKey, InputStream inputStream){
//        try{
//            jdStorageService.bucket(bucketName).object(objKey).entity(inputStream.available(), inputStream).put();
//        } catch (IOException e) {
//            logger.error(e);
//            throw new RuntimeException("输入输出流异常");
//        } finally {
//        	try{
//        		inputStream.close();
//        	} catch(IOException e) {
//        	}
//        }
//    }
//
//    /**
//     * 下载文件
//     * @param bucketName 桶名
//     * @param objKey 对象的KEY
//    public InputStream downloadFile(String bucketName, String objKey){
//        File filePath = new File(tempDirectory);
//        if (!filePath.exists()) {
//            filePath.mkdir();
//        }
//        File file = new File(tempDirectory+objKey);
//        file.createNewFile();
//        jdStorageService.bucket(bucketName).object(objKey).get().toFile(file);
//        return new BufferedInputStream(new FileInputStream(file));
//    }
//     * */
//
//    /**
//     * 查找所有对象(默认返回前1000个ObjectSummary)
//     * @param bucketName 桶名
//     * @return 对象列表
//     */
//    public ObjectListing findObjects(String bucketName){
//    	return jdStorageService.bucket(bucketName).listObject();
//    }
//
//    /**
//     * 判断KEY是否存在
//     * @param bucketName 桶名
//     * @param key 对象的KEY
//     */
//    public boolean exist(String bucketName,String key){
//    	return jdStorageService.bucket(bucketName).object(key).exist();//判断key是否存
//    }
//
//    /**
//     * 删除对象
//     * @param bucketName 桶名
//     * @param key 对象的KEY
//     */
//    public void deleteObject(String bucketName,String key){
//    	ObjectService o = jdStorageService.bucket(bucketName).object(key);
//    	if (o.exist())	o.delete();        		
//    }
//
//    /**
//     * 根据异常码获取异常信息
//     * @param code 异常码
//     * @return 异常信息
//     */
////    private String getExceptionMessage(String code){
////
////        String message = "Jss服务调用失败";
////        if(code != null && !code.equals("")){
////            message = exceptionHm.get(code);
////            if(message == null || message.equals("")){
////                message = "Jss服务调用失败";
////            }
////        }
////        return message;
////    }
//
//	/**
//	 * 获取jss文件的外部链接
//	 * @param bucketName 桶名
//	 * @param key 对象的KEY
//	 * @return 对象外链URI
//	 */
//	public URI getObjectUri(String bucketName, String key) {
//		return jdStorageService.bucket(bucketName).object(key).generatePresignedUrl(3600*24*365*50);
//	}
//
//	public String getEndpoint() {
//		return endpoint;
//	}
//
//	public void setEndpoint(String endpoint) {
//		this.endpoint = endpoint;
//	}
//
//	public String getAccessKey() {
//        return accessKey;
//    }
//
//    public void setAccessKey(String accessKey) {
//        this.accessKey = accessKey;
//    }
//
//    public String getSecretKey() {
//        return secretKey;
//    }
//
//    public void setSecretKey(String secretKey) {
//        this.secretKey = secretKey;
//    }
//}

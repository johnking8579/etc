/**
 * Copyright (C) 1998-2008 TENCENT Inc.All Rights Reserved.     
 *                                                                  
 * FileName：StringUtil.java                 
 *          
 * Description：简要描述本文件的内容                                                                           
 * History：
 * 版本号    作者           日期          简要介绍相关操作
 *  1.0   wendyhu        2011-10-10           Create 
 */

package com.paipai.halo.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 字符串的处理应用类
 * 
 * @author wendyhu（最新修改者）
 * @version 1.0（新版本号）
 * @see 参考的JavaDoc
 */

public class StringUtil
{
    public static final String EMPTY = "";

    /**
     * 
     * @Title: removeBlank 
     * @Description: 删除参数里面的空格
     * @param str
     * @return  
     *        String    返回类型 
     *
     * @throws
     */
    
	private static StringUtil eInstance = new StringUtil();

	public static StringUtil getInstance() 
	{
		return eInstance;
	}
	
    public static String removeBlank(String str)
    {
        String ret = null;
        if(str != null)
        {
            ret = str.replaceAll(" ", "");
        }
        else
        {
            
        }
        return ret;
    }
    /**
     * 将字符串转换成 long 型
     * 
     * @param s
     *            字符串
     * @return 正常情况返回 long 类型数据，如果出错返回 Long.MIN_VALUE，
     */
    public static long toLong(String s)
    {
        return toLong(s,Long.MIN_VALUE);
    }

    /**
     * 将字符串转换成 long 型，如果发生转换的异常那么返回默认值
     * 
     * @param s
     *            字符串
     * @param defaultValue
     *            默认值
     * @return 如果发生转换的异常那么返回默认值
     */
    public static long toLong(String s, long defaultValue)
    {
        long ret = defaultValue;

        try
        {
            if(s!=null)
                ret = Long.valueOf(s.trim());
        } catch (Exception e)
        {
        }

        return ret;
    }
    /**
     * 将字符串转换成 int 型，如果发生转换的异常那么返回默认值
     * 
     * @param s
     *            字符串
     * @param defaultValue
     *            默认值
     * @return 如果发生转换的异常那么返回默认值
     */
    public static int toInt(String s, int defaultValue)
    {
        int ret = defaultValue;

        try
        {
            ret = Integer.valueOf(s);
        } catch (Exception e)
        {
        }

        return ret;
    }

    /**
     * 返回一个字符串在原字符串中的结束位置
     * 
     * @param 参数说明
     *            ：oriString 原字符串
     * @param 参数说明
     *            ：subString 子字符串
     * @return 返回值：注释出失败、错误、异常时的返回情况
     * @exception 异常
     *                ：注释出什么条件下会引发什么样的异常
     * @see 参考的JavaDoc
     */
    public static int getSubStringBeginPos(String oriString, String subString)
    {
        if (oriString == null || subString == null || oriString.length() == 0)
        {
            return -1;
        }

        return oriString.indexOf(subString);
    }

    /**
     * 返回一个字符串在原字符串中的结束位置
     * 
     * @param 参数说明
     *            ：oriString 原字符串
     * @param 参数说明
     *            ：subString 子字符串
     * @return 返回值：注释出失败、错误、异常时的返回情况
     * @exception 异常
     *                ：注释出什么条件下会引发什么样的异常
     * @see 参考的JavaDoc
     */
    public static int getSubStringEndPos(String oriString, String subString)
    {
        if (oriString == null || subString == null || oriString.length() == 0
                || oriString.indexOf(subString) == -1)
        {
            return -1;
        }

        return oriString.indexOf(subString) + subString.length();
    }

    /**
     * <p>
     * Checks if a String is empty ("") or null.
     * </p>
     * 
     * <pre>
     * StringUtils.isEmpty(null)      = true
     * StringUtils.isEmpty(&quot;&quot;)        = true
     * StringUtils.isEmpty(&quot; &quot;)       = false
     * StringUtils.isEmpty(&quot;bob&quot;)     = false
     * StringUtils.isEmpty(&quot;  bob  &quot;) = false
     * </pre>
     * 
     * <p>
     * NOTE: This method changed in Lang version 2.0. It no longer trims the
     * String. That functionality is available in isBlank().
     * </p>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if the String is empty or null
     */
    public static boolean isEmpty(String str)
    {
        return (str == null || str.trim().length() == 0);
    }
    
    /**
     * <p>
     * Checks if a String is not empty ("") and not null.
     * </p>
     * 
     * <pre>
     * StringUtils.isNotEmpty(null)      = false
     * StringUtils.isNotEmpty(&quot;&quot;)        = false
     * StringUtils.isNotEmpty(&quot; &quot;)       = true
     * StringUtils.isNotEmpty(&quot;bob&quot;)     = true
     * StringUtils.isNotEmpty(&quot;  bob  &quot;) = true
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if the String is not empty and not null
     */
    public static boolean isNotEmpty(String str)
    {
        return (str != null && str.length() > 0);
    }

    /**
     * <p>
     * Checks if a String is whitespace, empty ("") or null.
     * </p>
     * 
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank(&quot;&quot;)        = true
     * StringUtils.isBlank(&quot; &quot;)       = true
     * StringUtils.isBlank(&quot;bob&quot;)     = false
     * StringUtils.isBlank(&quot;  bob  &quot;) = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if the String is null, empty or whitespace
     * @since 2.0
     */
    public static boolean isBlank(String str)
    {
        int strLen;
        if (str == null || (strLen = str.length()) == 0)
        {
            return true;
        }
        for (int i = 0; i < strLen; i++)
        {
            if ((Character.isWhitespace(str.charAt(i)) == false))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Checks if a String is not empty (""), not null and not whitespace only.
     * </p>
     * 
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank(&quot;&quot;)        = false
     * StringUtils.isNotBlank(&quot; &quot;)       = false
     * StringUtils.isNotBlank(&quot;bob&quot;)     = true
     * StringUtils.isNotBlank(&quot;  bob  &quot;) = true
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if the String is not empty and not null and not
     *         whitespace
     * @since 2.0
     */
    public static boolean isNotBlank(String str)
    {
        int strLen;
        if (str == null || (strLen = str.length()) == 0)
        {
            return false;
        }
        for (int i = 0; i < strLen; i++)
        {
            if ((Character.isWhitespace(str.charAt(i)) == false))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>
     * Removes control characters (char &lt;= 32) from both ends of this String,
     * handling <code>null</code> by returning <code>null</code>.
     * </p>
     * 
     * <p>
     * The String is trimmed using {@link String#trim()}. Trim removes start and
     * end characters &lt;= 32. To strip whitespace use {@link #strip(String)}.
     * </p>
     * 
     * <p>
     * To trim your choice of characters, use the {@link #strip(String, String)}
     * methods.
     * </p>
     * 
     * <pre>
     * StringUtils.trim(null)          = null
     * StringUtils.trim(&quot;&quot;)            = &quot;&quot;
     * StringUtils.trim(&quot;     &quot;)       = &quot;&quot;
     * StringUtils.trim(&quot;abc&quot;)         = &quot;abc&quot;
     * StringUtils.trim(&quot;    abc    &quot;) = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to be trimmed, may be null
     * @return the trimmed string, <code>null</code> if null String input
     */
    public static String trim(String str)
    {
        return (str == null ? null : str.trim());
    }
    
    /**
     * <p>
     * Compares two Strings, returning <code>true</code> if they are equal.
     * </p>
     * 
     * <p>
     * <code>null</code>s are handled without exceptions. Two <code>null</code>
     * references are considered to be equal. The comparison is case sensitive.
     * </p>
     * 
     * <pre>
     * StringUtils.equals(null, null)   = true
     * StringUtils.equals(null, &quot;abc&quot;)  = false
     * StringUtils.equals(&quot;abc&quot;, null)  = false
     * StringUtils.equals(&quot;abc&quot;, &quot;abc&quot;) = true
     * StringUtils.equals(&quot;abc&quot;, &quot;ABC&quot;) = false
     * </pre>
     * 
     * @see java.lang.String#equals(Object)
     * @param str1
     *            the first String, may be null
     * @param str2
     *            the second String, may be null
     * @return <code>true</code> if the Strings are equal, case sensitive, or
     *         both <code>null</code>
     */
    public static boolean equals(String str1, String str2)
    {
        return (str1 == null ? str2 == null : str1.equals(str2));
    }

    /**
     * <p>
     * Compares two Strings, returning <code>true</code> if they are equal
     * ignoring the case.
     * </p>
     * 
     * <p>
     * <code>null</code>s are handled without exceptions. Two <code>null</code>
     * references are considered equal. Comparison is case insensitive.
     * </p>
     * 
     * <pre>
     * StringUtils.equalsIgnoreCase(null, null)   = true
     * StringUtils.equalsIgnoreCase(null, &quot;abc&quot;)  = false
     * StringUtils.equalsIgnoreCase(&quot;abc&quot;, null)  = false
     * StringUtils.equalsIgnoreCase(&quot;abc&quot;, &quot;abc&quot;) = true
     * StringUtils.equalsIgnoreCase(&quot;abc&quot;, &quot;ABC&quot;) = true
     * </pre>
     * 
     * @see java.lang.String#equalsIgnoreCase(String)
     * @param str1
     *            the first String, may be null
     * @param str2
     *            the second String, may be null
     * @return <code>true</code> if the Strings are equal, case insensitive, or
     *         both <code>null</code>
     */
    public static boolean equalsIgnoreCase(String str1, String str2)
    {
        return (str1 == null ? str2 == null : str1.equalsIgnoreCase(str2));
    }

    /**
     * <p>
     * Finds the first index within a String, handling <code>null</code>. This
     * method uses {@link String#indexOf(int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> or empty ("") String will return <code>-1</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.indexOf(null, *)         = -1
     * StringUtils.indexOf(&quot;&quot;, *)           = -1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'a') = 0
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'b') = 2
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchChar
     *            the character to find
     * @return the first index of the search character, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int indexOf(String str, char searchChar)
    {
        if (str == null || str.length() == 0)
        {
            return -1;
        }
        return str.indexOf(searchChar);
    }

    /**
     * <p>
     * Finds the first index within a String from a start position, handling
     * <code>null</code>. This method uses {@link String#indexOf(int, int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> or empty ("") String will return <code>-1</code>. A
     * negative start position is treated as zero. A start position greater than
     * the string length returns <code>-1</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.indexOf(null, *, *)          = -1
     * StringUtils.indexOf(&quot;&quot;, *, *)            = -1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'b', 0)  = 2
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'b', 3)  = 5
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'b', 9)  = -1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, 'b', -1) = 2
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchChar
     *            the character to find
     * @param startPos
     *            the start position, negative treated as zero
     * @return the first index of the search character, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int indexOf(String str, char searchChar, int startPos)
    {
        if (str == null || str.length() == 0)
        {
            return -1;
        }
        return str.indexOf(searchChar, startPos);
    }

    /**
     * <p>
     * Finds the first index within a String, handling <code>null</code>. This
     * method uses {@link String#indexOf(String)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>-1</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.indexOf(null, *)          = -1
     * StringUtils.indexOf(*, null)          = -1
     * StringUtils.indexOf(&quot;&quot;, &quot;&quot;)           = 0
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;a&quot;)  = 0
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;b&quot;)  = 2
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;ab&quot;) = 1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;&quot;)   = 0
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchStr
     *            the String to find, may be null
     * @return the first index of the search String, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int indexOf(String str, String searchStr)
    {
        if (str == null || searchStr == null)
        {
            return -1;
        }
        return str.indexOf(searchStr);
    }

    /**
     * <p>
     * Finds the first index within a String, handling <code>null</code>. This
     * method uses {@link String#indexOf(String, int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>-1</code>. A negative start
     * position is treated as zero. An empty ("") search String always matches.
     * A start position greater than the string length only matches an empty
     * search String.
     * </p>
     * 
     * <pre>
     * StringUtils.indexOf(null, *, *)          = -1
     * StringUtils.indexOf(*, null, *)          = -1
     * StringUtils.indexOf(&quot;&quot;, &quot;&quot;, 0)           = 0
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;a&quot;, 0)  = 0
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 0)  = 2
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;ab&quot;, 0) = 1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 3)  = 5
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 9)  = -1
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;b&quot;, -1) = 2
     * StringUtils.indexOf(&quot;aabaabaa&quot;, &quot;&quot;, 2)   = 2
     * StringUtils.indexOf(&quot;abc&quot;, &quot;&quot;, 9)        = 3
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchStr
     *            the String to find, may be null
     * @param startPos
     *            the start position, negative treated as zero
     * @return the first index of the search String, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int indexOf(String str, String searchStr, int startPos)
    {
        if (str == null || searchStr == null)
        {
            return -1;
        }
        // JDK1.2/JDK1.3 have a bug, when startPos > str.length for "", hence
        if (searchStr.length() == 0 && startPos >= str.length())
        {
            return str.length();
        }
        return str.indexOf(searchStr, startPos);
    }

    // LastIndexOf
    // -----------------------------------------------------------------------
    /**
     * <p>
     * Finds the last index within a String, handling <code>null</code>. This
     * method uses {@link String#lastIndexOf(int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> or empty ("") String will return <code>-1</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.lastIndexOf(null, *)         = -1
     * StringUtils.lastIndexOf(&quot;&quot;, *)           = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'a') = 7
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b') = 5
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchChar
     *            the character to find
     * @return the last index of the search character, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int lastIndexOf(String str, char searchChar)
    {
        if (str == null || str.length() == 0)
        {
            return -1;
        }
        return str.lastIndexOf(searchChar);
    }

    /**
     * <p>
     * Finds the last index within a String from a start position, handling
     * <code>null</code>. This method uses {@link String#lastIndexOf(int, int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> or empty ("") String will return <code>-1</code>. A
     * negative start position returns <code>-1</code>. A start position greater
     * than the string length searches the whole string.
     * </p>
     * 
     * <pre>
     * StringUtils.lastIndexOf(null, *, *)          = -1
     * StringUtils.lastIndexOf(&quot;&quot;, *,  *)           = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b', 8)  = 5
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b', 4)  = 2
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b', 0)  = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b', 9)  = 5
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'b', -1) = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, 'a', 0)  = 0
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchChar
     *            the character to find
     * @param startPos
     *            the start position
     * @return the last index of the search character, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int lastIndexOf(String str, char searchChar, int startPos)
    {
        if (str == null || str.length() == 0)
        {
            return -1;
        }
        return str.lastIndexOf(searchChar, startPos);
    }

    /**
     * <p>
     * Finds the last index within a String, handling <code>null</code>. This
     * method uses {@link String#lastIndexOf(String)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>-1</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.lastIndexOf(null, *)          = -1
     * StringUtils.lastIndexOf(*, null)          = -1
     * StringUtils.lastIndexOf(&quot;&quot;, &quot;&quot;)           = 0
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;a&quot;)  = 0
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;b&quot;)  = 2
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;ab&quot;) = 1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;&quot;)   = 8
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchStr
     *            the String to find, may be null
     * @return the last index of the search String, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int lastIndexOf(String str, String searchStr)
    {
        if (str == null || searchStr == null)
        {
            return -1;
        }
        return str.lastIndexOf(searchStr);
    }

    /**
     * <p>
     * Finds the first index within a String, handling <code>null</code>. This
     * method uses {@link String#lastIndexOf(String, int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>-1</code>. A negative start
     * position returns <code>-1</code>. An empty ("") search String always
     * matches unless the start position is negative. A start position greater
     * than the string length searches the whole string.
     * </p>
     * 
     * <pre>
     * StringUtils.lastIndexOf(null, *, *)          = -1
     * StringUtils.lastIndexOf(*, null, *)          = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;a&quot;, 8)  = 7
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 8)  = 5
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;ab&quot;, 8) = 4
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 9)  = 5
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;b&quot;, -1) = -1
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;a&quot;, 0)  = 0
     * StringUtils.lastIndexOf(&quot;aabaabaa&quot;, &quot;b&quot;, 0)  = -1
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchStr
     *            the String to find, may be null
     * @param startPos
     *            the start position, negative treated as zero
     * @return the first index of the search String, -1 if no match or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static int lastIndexOf(String str, String searchStr, int startPos)
    {
        if (str == null || searchStr == null)
        {
            return -1;
        }
        return str.lastIndexOf(searchStr, startPos);
    }

    // Contains
    // -----------------------------------------------------------------------
    /**
     * <p>
     * Checks if String contains a search character, handling <code>null</code>.
     * This method uses {@link String#indexOf(int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> or empty ("") String will return <code>false</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.contains(null, *)    = false
     * StringUtils.contains(&quot;&quot;, *)      = false
     * StringUtils.contains(&quot;abc&quot;, 'a') = true
     * StringUtils.contains(&quot;abc&quot;, 'z') = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchChar
     *            the character to find
     * @return true if the String contains the search character, false if not or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static boolean contains(String str, char searchChar)
    {
        if (str == null || str.length() == 0)
        {
            return false;
        }
        return (str.indexOf(searchChar) >= 0);
    }

    /**
     * <p>
     * Find the first index within a String, handling <code>null</code>. This
     * method uses {@link String#indexOf(int)}.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>false</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.contains(null, *)     = false
     * StringUtils.contains(*, null)     = false
     * StringUtils.contains(&quot;&quot;, &quot;&quot;)      = true
     * StringUtils.contains(&quot;abc&quot;, &quot;&quot;)   = true
     * StringUtils.contains(&quot;abc&quot;, &quot;a&quot;)  = true
     * StringUtils.contains(&quot;abc&quot;, &quot;z&quot;)  = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @param searchStr
     *            the String to find, may be null
     * @return true if the String contains the search character, false if not or
     *         <code>null</code> string input
     * @since 2.0
     */
    public static boolean contains(String str, String searchStr)
    {
        if (str == null || searchStr == null)
        {
            return false;
        }
        return (str.indexOf(searchStr) >= 0);
    }

    /**
     * <p>
     * Gets a substring from the specified String avoiding exceptions.
     * </p>
     * 
     * <p>
     * A negative start position can be used to start <code>n</code> characters
     * from the end of the String.
     * </p>
     * 
     * <p>
     * A <code>null</code> String will return <code>null</code>. An empty ("")
     * String will return "".
     * </p>
     * 
     * <pre>
     * StringUtils.substring(null, *)   = null
     * StringUtils.substring(&quot;&quot;, *)     = &quot;&quot;
     * StringUtils.substring(&quot;abc&quot;, 0)  = &quot;abc&quot;
     * StringUtils.substring(&quot;abc&quot;, 2)  = &quot;c&quot;
     * StringUtils.substring(&quot;abc&quot;, 4)  = &quot;&quot;
     * StringUtils.substring(&quot;abc&quot;, -2) = &quot;bc&quot;
     * StringUtils.substring(&quot;abc&quot;, -4) = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to get the substring from, may be null
     * @param start
     *            the position to start from, negative means count back from the
     *            end of the String by this many characters
     * @return substring from start position, <code>null</code> if null String
     *         input
     */
    public static String substring(String str, int start)
    {
        if (str == null)
        {
            return null;
        }

        // handle negatives, which means last n characters
        if (start < 0)
        {
            start = str.length() + start; // remember start is negative
        }

        if (start < 0)
        {
            start = 0;
        }
        if (start > str.length())
        {
            return EMPTY;
        }

        return str.substring(start);
    }

    /**
     * <p>
     * Gets a substring from the specified String avoiding exceptions.
     * </p>
     * 
     * <p>
     * A negative start position can be used to start/end <code>n</code>
     * characters from the end of the String.
     * </p>
     * 
     * <p>
     * The returned substring starts with the character in the
     * <code>start</code> position and ends before the <code>end</code>
     * position. All postion counting is zero-based -- i.e., to start at the
     * beginning of the string use <code>start = 0</code>. Negative start and
     * end positions can be used to specify offsets relative to the end of the
     * String.
     * </p>
     * 
     * <p>
     * If <code>start</code> is not strictly to the left of <code>end</code>, ""
     * is returned.
     * </p>
     * 
     * <pre>
     * StringUtils.substring(null, *, *)    = null
     * StringUtils.substring(&quot;&quot;, * ,  *)    = &quot;&quot;;
     * StringUtils.substring(&quot;abc&quot;, 0, 2)   = &quot;ab&quot;
     * StringUtils.substring(&quot;abc&quot;, 2, 0)   = &quot;&quot;
     * StringUtils.substring(&quot;abc&quot;, 2, 4)   = &quot;c&quot;
     * StringUtils.substring(&quot;abc&quot;, 4, 6)   = &quot;&quot;
     * StringUtils.substring(&quot;abc&quot;, 2, 2)   = &quot;&quot;
     * StringUtils.substring(&quot;abc&quot;, -2, -1) = &quot;b&quot;
     * StringUtils.substring(&quot;abc&quot;, -4, 2)  = &quot;ab&quot;
     * </pre>
     * 
     * @param str
     *            the String to get the substring from, may be null
     * @param start
     *            the position to start from, negative means count back from the
     *            end of the String by this many characters
     * @param end
     *            the position to end at (exclusive), negative means count back
     *            from the end of the String by this many characters
     * @return substring from start position to end positon, <code>null</code>
     *         if null String input
     */
    public static String substring(String str, int start, int end)
    {
        if (str == null)
        {
            return null;
        }

        // handle negatives
        if (end < 0)
        {
            end = str.length() + end; // remember end is negative
        }
        if (start < 0)
        {
            start = str.length() + start; // remember start is negative
        }

        // check length next
        if (end > str.length())
        {
            end = str.length();
        }

        // if start is greater than end, return ""
        if (start > end)
        {
            return EMPTY;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (end < 0)
        {
            end = 0;
        }

        return str.substring(start, end);
    }

    // Left/Right/Mid
    // -----------------------------------------------------------------------
    /**
     * <p>
     * Gets the leftmost <code>len</code> characters of a String.
     * </p>
     * 
     * <p>
     * If <code>len</code> characters are not available, or the String is
     * <code>null</code>, the String will be returned without an exception. An
     * exception is thrown if len is negative.
     * </p>
     * 
     * <pre>
     * StringUtils.left(null, *)    = null
     * StringUtils.left(*, -ve)     = &quot;&quot;
     * StringUtils.left(&quot;&quot;, *)      = &quot;&quot;
     * StringUtils.left(&quot;abc&quot;, 0)   = &quot;&quot;
     * StringUtils.left(&quot;abc&quot;, 2)   = &quot;ab&quot;
     * StringUtils.left(&quot;abc&quot;, 4)   = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to get the leftmost characters from, may be null
     * @param len
     *            the length of the required String, must be zero or positive
     * @return the leftmost characters, <code>null</code> if null String input
     */
    public static String left(String str, int len)
    {
        if (str == null)
        {
            return null;
        }
        if (len < 0)
        {
            return EMPTY;
        }
        if (str.length() <= len)
        {
            return str;
        }

        return str.substring(0, len);

    }

    /**
     * <p>
     * Gets the rightmost <code>len</code> characters of a String.
     * </p>
     * 
     * <p>
     * If <code>len</code> characters are not available, or the String is
     * <code>null</code>, the String will be returned without an an exception.
     * An exception is thrown if len is negative.
     * </p>
     * 
     * <pre>
     * StringUtils.right(null, *)    = null
     * StringUtils.right(*, -ve)     = &quot;&quot;
     * StringUtils.right(&quot;&quot;, *)      = &quot;&quot;
     * StringUtils.right(&quot;abc&quot;, 0)   = &quot;&quot;
     * StringUtils.right(&quot;abc&quot;, 2)   = &quot;bc&quot;
     * StringUtils.right(&quot;abc&quot;, 4)   = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to get the rightmost characters from, may be null
     * @param len
     *            the length of the required String, must be zero or positive
     * @return the rightmost characters, <code>null</code> if null String input
     */
    public static String right(String str, int len)
    {
        if (str == null)
        {
            return null;
        }
        if (len < 0)
        {
            return EMPTY;
        }
        if (str.length() <= len)
        {
            return str;
        }
        return str.substring(str.length() - len);

    }

    /**
     * <p>
     * Gets <code>len</code> characters from the middle of a String.
     * </p>
     * 
     * <p>
     * If <code>len</code> characters are not available, the remainder of the
     * String will be returned without an exception. If the String is
     * <code>null</code>, <code>null</code> will be returned. An exception is
     * thrown if len is negative.
     * </p>
     * 
     * <pre>
     * StringUtils.mid(null, *, *)    = null
     * StringUtils.mid(*, *, -ve)     = &quot;&quot;
     * StringUtils.mid(&quot;&quot;, 0, *)      = &quot;&quot;
     * StringUtils.mid(&quot;abc&quot;, 0, 2)   = &quot;ab&quot;
     * StringUtils.mid(&quot;abc&quot;, 0, 4)   = &quot;abc&quot;
     * StringUtils.mid(&quot;abc&quot;, 2, 4)   = &quot;c&quot;
     * StringUtils.mid(&quot;abc&quot;, 4, 2)   = &quot;&quot;
     * StringUtils.mid(&quot;abc&quot;, -2, 2)  = &quot;ab&quot;
     * </pre>
     * 
     * @param str
     *            the String to get the characters from, may be null
     * @param pos
     *            the position to start from, negative treated as zero
     * @param len
     *            the length of the required String, must be zero or positive
     * @return the middle characters, <code>null</code> if null String input
     */
    public static String mid(String str, int pos, int len)
    {
        if (str == null)
        {
            return null;
        }
        if (len < 0 || pos > str.length())
        {
            return EMPTY;
        }
        if (pos < 0)
        {
            pos = 0;
        }
        if (str.length() <= (pos + len))
        {
            return str.substring(pos);
        }
        return str.substring(pos, pos + len);

    }

    /**
     * <p>
     * Deletes all whitespaces from a String as defined by
     * {@link Character#isWhitespace(char)}.
     * </p>
     * 
     * <pre>
     * StringUtils.deleteWhitespace(null)         = null
     * StringUtils.deleteWhitespace(&quot;&quot;)           = &quot;&quot;
     * StringUtils.deleteWhitespace(&quot;abc&quot;)        = &quot;abc&quot;
     * StringUtils.deleteWhitespace(&quot;   ab  c  &quot;) = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to delete whitespace from, may be null
     * @return the String without whitespaces, <code>null</code> if null String
     *         input
     */
    public static String deleteWhitespace(String str)
    {
        if (str == null)
        {
            return null;
        }
        int sz = str.length();
        StringBuffer buffer = new StringBuffer(sz);
        for (int i = 0; i < sz; i++)
        {
            if (!Character.isWhitespace(str.charAt(i)))
            {
                buffer.append(str.charAt(i));
            }
        }
        return buffer.toString();
    }

    /**
     * <p>
     * Converts a String to upper case as per {@link String#toUpperCase()}.
     * </p>
     * 
     * <p>
     * A <code>null</code> input String returns <code>null</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.upperCase(null)  = null
     * StringUtils.upperCase(&quot;&quot;)    = &quot;&quot;
     * StringUtils.upperCase(&quot;aBc&quot;) = &quot;ABC&quot;
     * </pre>
     * 
     * @param str
     *            the String to upper case, may be null
     * @return the upper cased String, <code>null</code> if null String input
     */
    public static String upperCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        return str.toUpperCase();
    }

    /**
     * <p>
     * Converts a String to lower case as per {@link String#toLowerCase()}.
     * </p>
     * 
     * <p>
     * A <code>null</code> input String returns <code>null</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.lowerCase(null)  = null
     * StringUtils.lowerCase(&quot;&quot;)    = &quot;&quot;
     * StringUtils.lowerCase(&quot;aBc&quot;) = &quot;abc&quot;
     * </pre>
     * 
     * @param str
     *            the String to lower case, may be null
     * @return the lower cased String, <code>null</code> if null String input
     */
    public static String lowerCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        return str.toLowerCase();
    }

    /**
     * <p>
     * Checks if the String contains only unicode letters.
     * </p>
     * 
     * <p>
     * <code>null</code> will return <code>false</code>. An empty String ("")
     * will return <code>true</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.isAlpha(null)   = false
     * StringUtils.isAlpha(&quot;&quot;)     = true
     * StringUtils.isAlpha(&quot;  &quot;)   = false
     * StringUtils.isAlpha(&quot;abc&quot;)  = true
     * StringUtils.isAlpha(&quot;ab2c&quot;) = false
     * StringUtils.isAlpha(&quot;ab-c&quot;) = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if only contains letters, and is non-null
     */
    public static boolean isAlpha(String str)
    {
        if (str == null)
        {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++)
        {
            if (Character.isLetter(str.charAt(i)) == false)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断一个字符串是否是英文字母不包含中文
     * 
     * @param 参数说明
     *            ：每个参数一行，注明其取值范围等
     * @return 返回值：注释出失败、错误、异常时的返回情况
     * @exception 异常
     *                ：注释出什么条件下会引发什么样的异常
     * @see 参考的JavaDoc
     */
    public static boolean isAlphaNotCHN(String str)
    {
        if (str == null)
        {
            return false;
        }
        int sz = str.length();
        char ch;
        for (int i = 0; i < sz; i++)
        {
            ch = str.charAt(i);

            if (!isAlphaChar(ch))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断一个字符串是否是英文数字字母不包含中文
     * 
     * @param 参数说明
     *            ：每个参数一行，注明其取值范围等
     * @return 返回值：注释出失败、错误、异常时的返回情况
     * @exception 异常
     *                ：注释出什么条件下会引发什么样的异常
     * @see 参考的JavaDoc
     */
    public static boolean isAlphaNumericNotCHN(String str)
    {
        if (str == null)
        {
            return false;
        }
        int sz = str.length();
        char ch;
        for (int i = 0; i < sz; i++)
        {
            ch = str.charAt(i);

            if (!isAlphaNumericChar(ch))
            {
                return false;
            }
        }
        return true;
    }

    public static boolean isAlphaChar(char ch)
    {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    public static boolean isAlphaNumericChar(char ch)
    {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
                || (ch >= '0' && ch <= '9');
    }

    /**
     * <p>
     * Checks if the String contains only unicode letters or digits.
     * </p>
     * 
     * <p>
     * <code>null</code> will return <code>false</code>. An empty String ("")
     * will return <code>true</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.isAlphanumeric(null)   = false
     * StringUtils.isAlphanumeric(&quot;&quot;)     = true
     * StringUtils.isAlphanumeric(&quot;  &quot;)   = false
     * StringUtils.isAlphanumeric(&quot;abc&quot;)  = true
     * StringUtils.isAlphanumeric(&quot;ab c&quot;) = false
     * StringUtils.isAlphanumeric(&quot;ab2c&quot;) = true
     * StringUtils.isAlphanumeric(&quot;ab-c&quot;) = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if only contains letters or digits, and is
     *         non-null
     */
    public static boolean isAlphanumeric(String str)
    {
        if (str == null)
        {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++)
        {
            if (Character.isLetterOrDigit(str.charAt(i)) == false)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Checks if the String contains only unicode digits. A decimal point is not
     * a unicode digit and returns false.
     * </p>
     * 
     * <p>
     * <code>null</code> will return <code>false</code>. An empty String ("")
     * will return <code>true</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.isNumeric(null)   = false
     * StringUtils.isNumeric(&quot;&quot;)     = true
     * StringUtils.isNumeric(&quot;  &quot;)   = false
     * StringUtils.isNumeric(&quot;123&quot;)  = true
     * StringUtils.isNumeric(&quot;12 3&quot;) = false
     * StringUtils.isNumeric(&quot;ab2c&quot;) = false
     * StringUtils.isNumeric(&quot;12-3&quot;) = false
     * StringUtils.isNumeric(&quot;12.3&quot;) = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if only contains digits, and is non-null
     */
    public static boolean isNumeric(String str)
    {
        if ((str == null) || (str.length() == 0))
        {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++)
        {
            if (Character.isDigit(str.charAt(i)) == false)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Checks if the String contains only whitespace.
     * </p>
     * 
     * <p>
     * <code>null</code> will return <code>false</code>. An empty String ("")
     * will return <code>true</code>.
     * </p>
     * 
     * <pre>
     * StringUtils.isWhitespace(null)   = false
     * StringUtils.isWhitespace(&quot;&quot;)     = true
     * StringUtils.isWhitespace(&quot;  &quot;)   = true
     * StringUtils.isWhitespace(&quot;abc&quot;)  = false
     * StringUtils.isWhitespace(&quot;ab2c&quot;) = false
     * StringUtils.isWhitespace(&quot;ab-c&quot;) = false
     * </pre>
     * 
     * @param str
     *            the String to check, may be null
     * @return <code>true</code> if only contains whitespace, and is non-null
     * @since 2.0
     */
    public static boolean isWhitespace(String str)
    {
        if (str == null)
        {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++)
        {
            if ((Character.isWhitespace(str.charAt(i)) == false))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Capitalizes a String changing the first letter to title case as per
     * {@link Character#toTitleCase(char)}. No other letters are changed.
     * </p>
     * 
     * A <code>null</code> input String returns <code>null</code>.</p>
     * 
     * <pre>
     * StringUtils.capitalize(null)  = null
     * StringUtils.capitalize("")    = ""
     * StringUtils.capitalize("cat") = "Cat"
     * StringUtils.capitalize("cAt") = "CAt"
     * </pre>
     * 
     * @param str
     *            the String to capitalize, may be null
     * @return the capitalized String, <code>null</code> if null String input
     * @see WordUtils#capitalize(String)
     * @see #uncapitalize(String)
     * @since 2.0
     */
    public static String capitalize(String str)
    {
        int strLen;
        if (str == null || (strLen = str.length()) == 0)
        {
            return str;
        }
        return new StringBuffer(strLen).append(
                Character.toTitleCase(str.charAt(0))).append(str.substring(1))
                .toString();
    }

    /**
     * <p>
     * Uncapitalizes a String changing the first letter to title case as per
     * {@link Character#toLowerCase(char)}. No other letters are changed.
     * </p>
     * 
     * A <code>null</code> input String returns <code>null</code>.</p>
     * 
     * <pre>
     * StringUtils.uncapitalize(null)  = null
     * StringUtils.uncapitalize("")    = ""
     * StringUtils.uncapitalize("Cat") = "cat"
     * StringUtils.uncapitalize("CAT") = "cAT"
     * </pre>
     * 
     * @param str
     *            the String to uncapitalize, may be null
     * @return the uncapitalized String, <code>null</code> if null String input
     * @see #capitalize(String)
     * @since 2.0
     */
    public static String uncapitalize(String str)
    {
        int strLen;
        if (str == null || (strLen = str.length()) == 0)
        {
            return str;
        }
        return new StringBuffer(strLen).append(
                Character.toLowerCase(str.charAt(0))).append(str.substring(1))
                .toString();
    }

    public static String[] toStringArray(Collection<String> collection)
    {
        if (collection == null)
        {
            return null;
        }
        return (String[]) collection.toArray(new String[collection.size()]);
    }

    public static String[] tokenizeToStringArray(String str, String delimiters,
            boolean trimTokens, boolean ignoreEmptyTokens)
    {

        if (str == null)
        {
            return null;
        }
        StringTokenizer st = new StringTokenizer(str, delimiters);
        List<String> tokens = new ArrayList<String>();
        while (st.hasMoreTokens())
        {
            String token = st.nextToken();
            if (trimTokens)
            {
                token = token.trim();
            }
            if (!ignoreEmptyTokens || token.length() > 0)
            {
                tokens.add(token);
            }
        }
        return toStringArray(tokens);
    }

    /**
     * 分割字符串
     * @param line			原始字符串
     * @param seperator		分隔符
     * @return				分割结果
     */
    public static String[] split(String line, String seperator)
	{
		if(line == null || seperator == null || seperator.length() == 0)
			return null;
		ArrayList<String> list = new ArrayList<String>();
		int pos1 = 0;
		int pos2;
		for(; ; )
		{
			pos2 = line.indexOf(seperator, pos1);
			if(pos2 < 0)
			{
				list.add(line.substring(pos1));
				break;
			}
			list.add(line.substring(pos1, pos2));
			pos1 = pos2 + seperator.length();
		}
		// 去掉末尾的空串，和String.split行为保持一致
		for(int i = list.size() - 1; i >= 0 && list.get(i).length() == 0; --i)
		{
			list.remove(i);
		}
		return list.toArray(new String[0]);
	}

    public static String Longs2String(long[] longs)
    {
        StringBuffer sbf = new StringBuffer();
        if (longs != null && longs.length > 0)
        {
            sbf.append("[");
            for (int i = 0; i < longs.length; i++)
            {
                sbf.append(longs[i]);
                if (i < longs.length - 1)
                {
                    sbf.append(", ");
                }
            }
            sbf.append("]");
        }
        return sbf.toString();
    }
    
    public static String genParameterStr(Object... objs)
    {
        StringBuffer sbf = new StringBuffer();
        sbf.append("(");
        for (int i = 0; i < objs.length; i += 2)
        {
            sbf.append(objs[i].toString());
            sbf.append("=");
            if (i + 1 < objs.length)
            {
                if (objs[i + 1] == null)
                {
                    sbf.append("null");
                } else
                {
                    sbf.append(objs[i + 1].toString());
                }
            }
            if (i + 2 < objs.length)
            {
                sbf.append(",");
            }
        }
        sbf.append(")");
        return sbf.toString();
    }
    /**
     * 
     * @author:jiaqiangxu
     * @date:2012-3-8
     * @desc:按指定字节长度截取字符串，如果结果字符串最后一个字符是中文且不够，则略去此字符
     * @param:s 需要截取的字符串  ;length 截取的字节长度，(中文和全角字符占2字节)
     * @return:截取后悔的字符串
     */
    public static String bSubstring(String s, int length) throws Exception{
    	if(s==null || s.length() == 0){
    		return "";
    	}
    	byte[] bytes = s.getBytes("Unicode");
    	int n = 0; // 表示当前的字节数
    	int i = 2; // 要截取的字节数，从第3个字节开始
    	for (; i < bytes.length && n < length; i++)
    	{
    	// 奇数位置，如3、5、7等，为UCS2编码中两个字节的第二个字节
    		if (i % 2 == 1)
    		{   			
    			n++; // 在UCS2第二个字节时n加1

    		}
    		else
    		{
    	// 当UCS2编码的第一个字节不等于0时，该UCS2字符为汉字，一个汉字算两个字节
    			if (bytes[i] != 0)
    			{
    				n++;
    			}
    	   }
    	}

    	// 如果i为奇数时，处理成偶数
    	if (i % 2 == 1)
    	{
    	// 该UCS2字符是汉字时，去掉这个截一半的汉字
    		if (bytes[i - 1] != 0)
    			i = i - 1;
    	// 该UCS2字符是字母或数字，则保留该字符
    		else
    			i = i + 1;
    	}
    	return new String(bytes, 0, i, "Unicode");
   }
    
 // Joining
    // -----------------------------------------------------------------------

    /**
     * <p>
     * 连接数组对象的元素为字符串
     * </p>
     * 
     * <pre>
     * StringUtils.join(null)            = null
     * StringUtils.join([])              = ""
     * StringUtils.join([null])          = ""
     * StringUtils.join(["a", "b", "c"]) = "abc"
     * StringUtils.join([null, "", "a"]) = "a"
     * </pre>
     * 
     * @param array
     *            数组对象
     * @return 串接后的字符串，如果数组对象为 NULL 则返回 NULL
     */
    public static String join(Object[] array)
    {
        return join(array, null);
    }

    /**
     * <p>
     * 以指定的分隔符串接数组对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * <pre>
     * StringUtils.join(null, *)               = null
     * StringUtils.join([], *)                 = ""
     * StringUtils.join([null], *)             = ""
     * StringUtils.join(["a", "b", "c"], ';')  = "a;b;c"
     * StringUtils.join(["a", "b", "c"], null) = "abc"
     * StringUtils.join([null, "", "a"], ';')  = ";;a"
     * </pre>
     * 
     * @param array
     *            数组对象
     * @param separator
     *            分隔符
     * @return 串接后的字符串，如果数组对象为 NULL 则返回 NULL
     */
    public static String join(Object[] array, char separator)
    {
        if (array == null)
        {
            return null;
        }

        return join(array, separator, 0, array.length);
    }

    /**
     * <p>
     * 以指定的分隔符串接数组对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * <pre>
     * StringUtils.join(null, *)               = null
     * StringUtils.join([], *)                 = ""
     * StringUtils.join([null], *)             = ""
     * StringUtils.join(["a", "b", "c"], ';')  = "a;b;c"
     * StringUtils.join(["a", "b", "c"], null) = "abc"
     * StringUtils.join([null, "", "a"], ';')  = ";;a"
     * </pre>
     * 
     * @param array
     *            数组对象
     * @param separator
     *            分隔符
     * @param startIndex
     *            数组开始下标
     * @param endIndex
     *            数组结束下标
     * @return 串接后的字符串，如果数组对象为 NULL 则返回 NULL
     */
    public static String join(Object[] array, char separator, int startIndex,
            int endIndex)
    {
        if (array == null)
        {
            return null;
        }
        int bufSize = (endIndex - startIndex);
        if (bufSize <= 0)
        {
            return EMPTY;
        }

        bufSize *= ((array[startIndex] == null ? 16 : array[startIndex]
                .toString().length()) + 1);
        StringBuilder buf = new StringBuilder(bufSize);

        for (int i = startIndex; i < endIndex; i++)
        {
            if (i > startIndex)
            {
                buf.append(separator);
            }
            if (array[i] != null)
            {
                buf.append(array[i]);
            }
        }
        return buf.toString();
    }

    /**
     * <p>
     * 以指定的分隔符串接数组对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * <pre>
     * StringUtils.join(null, *)                = null
     * StringUtils.join([], *)                  = ""
     * StringUtils.join([null], *)              = ""
     * StringUtils.join(["a", "b", "c"], "--")  = "a--b--c"
     * StringUtils.join(["a", "b", "c"], null)  = "abc"
     * StringUtils.join(["a", "b", "c"], "")    = "abc"
     * StringUtils.join([null, "", "a"], ',')   = ",,a"
     * </pre>
     * 
     * @param array
     *            数组对象
     * @param separator
     *            分隔符
     * @return 串接后的字符串，如果数组对象为 NULL 则返回 NULL
     */
    public static String join(Object[] array, String separator)
    {
        if (array == null)
        {
            return null;
        }
        return join(array, separator, 0, array.length);
    }

    /**
     * <p>
     * 以指定的分隔符串接数组对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * <pre>
     * StringUtils.join(null, *)                = null
     * StringUtils.join([], *)                  = ""
     * StringUtils.join([null], *)              = ""
     * StringUtils.join(["a", "b", "c"], "--")  = "a--b--c"
     * StringUtils.join(["a", "b", "c"], null)  = "abc"
     * StringUtils.join(["a", "b", "c"], "")    = "abc"
     * StringUtils.join([null, "", "a"], ',')   = ",,a"
     * </pre>
     * 
     * @param array
     *            数组对象
     * @param separator
     *            分隔符
     * @param startIndex
     *            数组开始下标
     * @param endIndex
     *            数组结束下标
     * @return 串接后的字符串，如果数组对象为 NULL 则返回 NULL
     */
    public static String join(Object[] array, String separator, int startIndex,
            int endIndex)
    {
        if (array == null)
        {
            return null;
        }
        if (separator == null)
        {
            separator = EMPTY;
        }

        int bufSize = (endIndex - startIndex);
        if (bufSize <= 0)
        {
            return EMPTY;
        }

        bufSize *= ((array[startIndex] == null ? 16 : array[startIndex]
                .toString().length()) + separator.length());

        StringBuilder buf = new StringBuilder(bufSize);

        for (int i = startIndex; i < endIndex; i++)
        {
            if (i > startIndex)
            {
                buf.append(separator);
            }
            if (array[i] != null)
            {
                buf.append(array[i]);
            }
        }
        return buf.toString();
    }

    /**
     * <p>
     * 以指定的分隔符串接迭代对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * @param iterator
     *            迭代器
     * @param separator
     *            分隔符
     * @return 串接后的字符串
     */
    @SuppressWarnings("unchecked")
	public static String join(Iterator iterator, char separator)
    {

        // handle null, zero and one elements before building a buffer
        if (iterator == null)
        {
            return null;
        }
        if (!iterator.hasNext())
        {
            return EMPTY;
        }
        Object first = iterator.next();
        if (!iterator.hasNext())
        {
            return objToString(first);
        }

        // two or more elements
        StringBuilder buf = new StringBuilder(256); // Java default is 16,
                                                    // probably too small
        if (first != null)
        {
            buf.append(first);
        }

        while (iterator.hasNext())
        {
            buf.append(separator);
            Object obj = iterator.next();
            if (obj != null)
            {
                buf.append(obj);
            }
        }

        return buf.toString();
    }

    /**
     * <p>
     * 以指定的分隔符串接迭代对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * @param iterator
     *            迭代器
     * @param separator
     *            分隔符
     * @return 串接后的字符串
     */
    @SuppressWarnings("unchecked")
	public static String join(Iterator iterator, String separator)
    {

        // handle null, zero and one elements before building a buffer
        if (iterator == null)
        {
            return null;
        }
        if (!iterator.hasNext())
        {
            return EMPTY;
        }
        Object first = iterator.next();
        if (!iterator.hasNext())
        {
            return objToString(first);
        }

        // two or more elements
        StringBuilder buf = new StringBuilder(256); // Java default is 16,
                                                    // probably too small
        if (first != null)
        {
            buf.append(first);
        }

        while (iterator.hasNext())
        {
            if (separator != null)
            {
                buf.append(separator);
            }
            Object obj = iterator.next();
            if (obj != null)
            {
                buf.append(obj);
            }
        }
        return buf.toString();
    }

    /**
     * <p>
     * 以指定的分隔符串接集合对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * @param collection
     *            集合
     * @param separator
     *            分隔符
     * @return 串接后的字符串
     */
    @SuppressWarnings("unchecked")
	public static String join(Collection collection, char separator)
    {
        if (collection == null)
        {
            return null;
        }
        return join(collection.iterator(), separator);
    }

    /**
     * <p>
     * 以指定的分隔符串接集合对象
     * </p>
     * 
     * <p>
     * 生成的字符串头尾不会添加分隔符
     * </p>
     * 
     * @param collection
     *            集合
     * @param separator
     *            分隔符
     * @return 串接后的字符串
     */
    @SuppressWarnings("unchecked")
	public static String join(Collection collection, String separator)
    {
        if (collection == null)
        {
            return null;
        }
        return join(collection.iterator(), separator);
    } 
    
    /**
     * 对象转化成字符串，处理 null 对象
     * 
     * @param o
     *            对象
     * @return 字符串
     */
    private static String objToString(Object o)
    {
        return null == o ? "" : o.toString();
    }
    
    static Set<String> specialWords = new HashSet<String>() {
		private static final long serialVersionUID = -5015742853025220605L;
		{
            add("\\?");
            add("\\*");
            add("\\.");
            add("\\+");
            add("\\)");
            add("\\(");
            add("\\$");
            add("\\{");
            add("\\}");
            add("\\|");
            add("\\^");
            add("\\\\");
            add("@");
            add("&");
            add("&amp;");
            add("#");
            add("!");
            add("（");
            add("）");
            add("｛");
            add("｝");
            add("＋");
            add("．");
            add("＊");
            add("？");
            add("＄");
            add("｜");
            add("＆");
            add("＃");
            add("！");
            add("'");
            add("\"");
        }
    };

    /**
     * 过滤搜索相关的敏感字符
     * @param keyword 关键词
     * @return 过滤后的字符串
     */
    public static String escapeForSearch(String keyword) {
        if(isBlank(keyword)) {
            return keyword;
        }
        for(String specialWord : specialWords) {
            keyword = keyword.replaceAll(specialWord, "");
        }
        return keyword.trim();
    }
    
    public static long[] toLongArray(Set<Long> ls) {
    	if (ls != null && ls.size() > 0) {
    		long[] ret = new long[ls.size()];
    		Long[] longArray = ls.toArray(new Long[] {});
    		for (int i=0; i<longArray.length; i++) {
    			ret[i] = longArray[i];
    		}
    		return ret;
    	}
    	return new long[] {};
    }
    public static String getRandomFromList(List<String> ls) {
    	if (ls != null && ls.size() > 0 && ls.size() <= 7) {
    		int  index =  (int) (Math.random() * (ls.size()));
    		return ls.get(index);
    	}
    	return "";
    }
    
 // Defaults
    // -----------------------------------------------------------------------

    /**
     * <p>
     * 判断字符串如果为空，则返回默认的字符串，否则返回原字符串
     * </p>
     * 
     * <pre>
     * StringUtils.defaultIfEmpty(null, "NULL")  = "NULL"
     * StringUtils.defaultIfEmpty("", "NULL")    = "NULL"
     * StringUtils.defaultIfEmpty("bat", "NULL") = "bat"
     * </pre>
     * 
     * @param str
     *            待校验字符串
     * @param defaultStr
     *            默认字符串
     * @return 原字符串或者默认字符串
     */
    public static String defaultIfEmpty(String str, String defaultStr)
    {
        return isEmpty(str) ? defaultStr : str;
    }
    
    public static String genParameterStringNoEmpty(Object... objs) {
    	StringBuffer sbf = new StringBuffer();
    	for (int i=0; i<objs.length; i+=2) {
    		if (i+1 < objs.length) {
    			if (objs[i+1] == null) {
    				continue;
    			}
    			if ((objs[i+1] instanceof String) && StringUtil.isEmpty((String)objs[i+1])) {
    				continue;
    			}
    			sbf.append(objs[i].toString());
    			sbf.append("=");
    			sbf.append(objs[i+1].toString());
    			if (i+3 < objs.length) {
    				sbf.append(",");
    			}
    		}
    	}
    	return sbf.toString();
    }
  
  
  public static String getEncode(String key){
		try {
			return URLEncoder.encode(key,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			return key;
		}
	}
  
  public static String getTraceInfo(){            
      StackTraceElement[] stacks = new Throwable().getStackTrace();  
      return stacks[2].getMethodName();            
  }  
  public static String getTraceInfo2(){            
      return getTraceInfo();            
  }  
  
  
  	//删除 ￥199.00中的".00" 变成￥199
	public static String removePriceZZ(String str) {
		if (!StringUtil.isEmpty(str)) {
			String _doc = str.replaceAll("\\.00", "");
			return _doc;
		}
		return str;
	}
	
	public static String priceDispFormater(long score) {
		 String price = String.format("%.2f", score / 100.0);
		 return removePriceZZ(price);
	}
	/**
	 * 转换为十六进制
	 * @param num
	 * @return
	 * @date:2013-6-13
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static String convertToHex(int num) {		 
		 return Integer.toHexString(num);
	}
	
	public static String maskStr(String srcStr, int  outLen)
	{
        String destStr = srcStr;
        try
        {
        	destStr = StringUtil.bSubstring(destStr, outLen);// 截取8个汉字长度
            if (StringUtil.isNotEmpty(destStr)
                    && !destStr.equals(srcStr))
            {
            	destStr = destStr + "...";
            }
        }
        catch (Exception e)
        {
            	//do nothing
        }	
        
        return destStr;
	}
	
	public static String encodeSidStr(String sid) {
		if (sid !=null && sid.trim().length() > 0 && sid.contains("=")) {
			try {
				return URLEncoder.encode(sid,"utf-8");
			} catch (Exception e) {
				return sid;
			}
		}
		return sid;
	}
	
	/**
	 * 将字符串转为WML编码,用于wml页面显示
	 * 根据unicode编码规则Blocks.txt：E000..F8FF; Private Use Area
	 * @param str 
	 * @return String
	 */
	public static String encodeWML(String str)
	{
		if(str == null)
		{
			return "";
		}
		// 不用正则表达式替换，直接通过循环，节省cpu时间
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < str.length(); ++i)
		{
			char c = str.charAt(i);
			switch(c)
			{
				case '\u00FF':
				case '\u0024':
					break;
				case '&':
					sb.append("&amp;");
					break;
				case '\t':
					sb.append("  ");
					break;
				case '<':
					sb.append("&lt;");
					break;
				case '>':
					sb.append("&gt;");
					break;
				case '\"':
					sb.append("&quot;");
					break;
				case '\n':
					sb.append("<br/>");
					break;
				default:
					if(c >= '\u0000' && c <= '\u001F')
						break;
					if(c >= '\uE000' && c <= '\uF8FF')
						break;
					if(c >= '\uFFF0' && c <= '\uFFFF')
						break;
					sb.append(c);
					break;
			}
		}
		return sb.toString();
	}
	
	/**
	 * 转换&#123;这种编码为正常字符<br/>
	 * 有些手机会将中文转换成&#123;这种编码,这个函数主要用来转换成正常字符.
	 * @param str
	 * @return String
	 */
	public static String decodeNetUnicode(String str)
	{
		if(str == null)
			return null;
        String pStr = "&#(\\d+);";
        Pattern p = Pattern.compile(pStr);
        Matcher m = p.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (m.find())
        {
            String mcStr = m.group(1);
            int charValue = StringUtil.convertInt(mcStr, -1);
            String s = charValue > 0 ? (char) charValue + "" : "";
            m.appendReplacement(sb, Matcher.quoteReplacement(s));
        }
        m.appendTail(sb);
        return sb.toString();		
	}
	
	/**
	 * 过滤SQL字符串,防止SQL inject
	 * @param sql
	 * @return String
	 */
	public static String encodeSQL(String sql)
	{
		if (sql == null)
		{
			return "";
		}
		// 不用正则表达式替换，直接通过循环，节省cpu时间
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < sql.length(); ++i)
		{
			char c = sql.charAt(i);
			switch(c)
			{
			case '\\':
				sb.append("\\\\");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\'':
				sb.append("\'\'");
				break;
			case '\"':
				sb.append("\\\"");
				break;
			default:
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	/**
	 * 删除在wml下不能正确处理的字符
	 * 根据unicode编码规则Blocks.txt：E000..F8FF; Private Use Area
	 * @param str	要处理的字符串
	 * @return		结果
	 */
	public static String removeInvalidWML(String str){
    	if(str == null)
    		return null;
    	//* 不用正则表达式替换，直接通过循环，节省cpu时间
    	StringBuilder sb = new StringBuilder();
		for(int i = 0; i < str.length(); ++i)
		{
			char c = str.charAt(i);
			if(c >= '\u0000' && c <= '\u001F')
				continue;
			if(c >= '\uE000' && c <= '\uF8FF')
				continue;
			if(c >= '\uFFF0' && c <= '\uFFFF')
				continue;
			switch(c)
			{
			case '\u00FF':
			case '\u0024':
				break;
			case '&':
				sb.append("&amp;");
				break;
			case '\t':
				sb.append("  ");
				break;
			case '<':
				sb.append("&lt;");
				break;
			case '>':
				sb.append("&gt;");
				break;
			case '\"':
				sb.append("&quot;");
				break;
			case '^':
			case '`':
				break;
			default:
				sb.append(c);
				break;
			}
		}
		return sb.toString();
	}
	
	/**
	 * 格式化日期
	 * 
	 * @param dateStr
	 *            输入的日期字符串
	 * @param inputFormat
	 *            输入日期格式
	 * @param format
	 *            输出日期格式
	 * @return String 格式化后的日期,如果不能格式化则输出原日期字符串
	 */
	public static String formatDate(String dateStr, String inputFormat, String format)
	{
		String resultStr = dateStr;
		try 
		{
			Date date = new SimpleDateFormat(inputFormat).parse(dateStr);
			resultStr = formatDate(date,format);
		}
		catch (ParseException e) 
		{
		}	
		return resultStr;		
	}
	/**
	 * 格式化日期
	 * 输入日期格式只支持yyyy-MM-dd HH:mm:ss 或 yyyy/MM/dd HH:mm:ss
	 * @param dateStr 输入的日期字符串
	 * @param format 输出日期格式
	 * @return String 格式化后的日期,如果不能格式化则输出原日期字符串
	 */	
	public static String formatDate(String dateStr, String format)
	{
		String resultStr = dateStr;
		String inputFormat = "yyyy-MM-dd HH:mm:ss";
		if(dateStr == null)
		{
			return "";
		}
		if(dateStr.matches("\\d{1,4}\\-\\d{1,2}\\-\\d{1,2}\\s+\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}"))
		{
			inputFormat = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		else if(dateStr.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2} +\\d{1,2}:\\d{1,2}"))
		{
			inputFormat = "yyyy-MM-dd HH:mm:ss";
		}
		else if(dateStr.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2} +\\d{1,2}:\\d{1,2}"))
		{
			inputFormat = "yyyy-MM-dd HH:mm";
		}
		else if(dateStr.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2} +\\d{1,2}"))
		{
			inputFormat = "yyyy-MM-dd HH";
		}
		else if(dateStr.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2} +\\d{1,2}"))
		{
			inputFormat = "yyyy-MM-dd";
		}
		else if(dateStr.matches("\\d{1,4}/\\d{1,2}/\\d{1,2}\\s+\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}"))
		{
			inputFormat = "yyyy/MM/dd HH:mm:ss.SSS";
		}
		else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} +\\d{1,2}:\\d{1,2}"))
		{
			inputFormat = "yyyy/MM/dd HH:mm:ss";
		}
		else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} +\\d{1,2}:\\d{1,2}"))
		{
			inputFormat = "yyyy/MM/dd HH:mm";
		}
		else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} +\\d{1,2}"))
		{
			inputFormat = "yyyy/MM/dd HH";
		}
		else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} +\\d{1,2}"))
		{
			inputFormat = "yyyy/MM/dd";
		}
		resultStr = formatDate(dateStr,inputFormat,format);
		return resultStr;
	}
	
	/**
	 * 格式化日期
	 * @param date 输入日期
	 * @param format 输出日期格式
	 * @return String
	 */
	public static String formatDate(Date date, String format)
	{
    	SimpleDateFormat sdf = new SimpleDateFormat(format);
    	return sdf.format(date);		
	}
	
    /**
     * 获取字符型参数，若输入字符串为null，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值 
     * @return 字符串参数
     */
    public static String convertString(String str, String defaults)
    {
        if(str == null)
        {
            return defaults;
        }
        else
        {
            return str;
        }
    }
        
    /**
     * 获取int参数，若输入字符串为null或不能转为int，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return int参数
     */
    public static int convertInt(String str, int defaults)
    {
        if(str == null)
        {
            return defaults;
        }        
        try
        {
            return Integer.parseInt(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
    /**
     * 获取long型参数，若输入字符串为null或不能转为long，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return long参数
     */
    public static long convertLong(String str, long defaults)
    {
        if(str == null)
        {
            return defaults;
        }        
        try
        {
            return Long.parseLong(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
    /**
     * 获取double型参数，若输入字符串为null或不能转为double，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return double型参数
     */
    public static double convertDouble(String str, double defaults)
    {
        if(str == null)
        {
            return defaults;
        }
        try
        {
            return Double.parseDouble(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
    /**
     * 获取short型参数，若输入字符串为null或不能转为short，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return short型参数
     */
    public static short convertShort(String str, short defaults)
    {
        if(str == null)
        {
            return defaults;
        }
        try
        {
            return Short.parseShort(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
    /**
     * 获取float型参数，若输入字符串为null或不能转为float，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return float型参数
     */
    public static float convertFloat(String str, float defaults)
    {
        if(str == null)
        {
            return defaults;
        }
        try
        {
            return Float.parseFloat(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
    /**
     * 获取boolean型参数，若输入字符串为null或不能转为boolean，则返回设定的默认值
     * @param str 输入字符串
     * @param defaults 默认值
     * @return boolean型参数
     */
    public static boolean convertBoolean(String str, boolean defaults)
    {
        if(str == null)
        {
            return defaults;
        }
        try
        {
            return Boolean.parseBoolean(str);
        }
        catch(Exception e)
        {
            return defaults;
        }
    }
    
   
    
    /**
     * 分割字符串，并转换为int
     * @param line		原始字符串
     * @param seperator	分隔符
     * @param def		默认值
     * @return			分割结果
     */
    public static int[] splitInt(String line, String seperator, int def)
    {
    	String[] ss = split(line, seperator);
    	int[] r = new int[ss.length];
    	for(int i = 0; i < r.length; ++i)
    	{
    		r[i] = convertInt(ss[i], def);
    	}
    	return r;
    }
    
    @SuppressWarnings("unchecked")
	public static String join(String separator, Collection c)
    {
    	if(c.isEmpty())
    		return "";
    	StringBuilder sb = new StringBuilder();
    	Iterator i = c.iterator();
    	sb.append(i.next());
    	while(i.hasNext()){
    		sb.append(separator);
    		sb.append(i.next());
    	}
    	return sb.toString();
    }
    
    public static String join(String separator, String[] s)
    {
    	return joinArray(separator,s);
    }

    public static String joinArray(String separator, Object[] s)
    {
    	if(s == null || s.length == 0)
    		return "";
    	StringBuilder sb = new StringBuilder();
    	sb.append(s[0]);
    	for(int i = 1; i < s.length; ++i){
    		if(s[i] != null)
    		{
	    		sb.append(separator);
	    		sb.append(s[i].toString());
    		}
    	}
    	return sb.toString();
    }

    public static String joinArray(String separator, int[] s)
    {
    	if(s == null || s.length == 0)
    		return "";
    	StringBuilder sb = new StringBuilder();
    	sb.append(s[0]);
    	for(int i = 1; i < s.length; ++i){
    		sb.append(separator);
    		sb.append(s[i]);
    	}
    	return sb.toString();
    }
    
    public static String join(String separator, Object...c)
    {
    	return joinArray(separator, c);
    }
    
    /**
     * 字符串全量替换
     * @param s			原始字符串
     * @param src		要替换的字符串
     * @param dest		替换目标
     * @return			结果
     */
    public static String replaceAll(String s, String src, String dest)
    {
    	if(s == null || src == null || dest == null || src.length() == 0)
    		return s;
    	int pos = s.indexOf(src);			// 查找第一个替换的位置
    	if(pos < 0)
    		return s;
    	int capacity = dest.length() > src.length() ? s.length() * 2: s.length();
    	StringBuilder sb = new StringBuilder(capacity);
    	int writen = 0;
    	for(; pos >= 0; )
    	{
    		sb.append(s, writen, pos);		// append 原字符串不需替换部分
    		sb.append(dest);				// append 新字符串
    		writen = pos + src.length();	// 忽略原字符串需要替换部分
    		pos = s.indexOf(src, writen);	// 查找下一个替换位置
    	}
    	sb.append(s, writen, s.length());	// 替换剩下的原字符串
    	return sb.toString();
    }

    /**
     * 只替换第一个
     * @param s
     * @param src
     * @param dest
     * @return
     */
	public static String replaceFirst(String s, String src, String dest)
	{
		if(s == null || src == null || dest == null || src.length() == 0)
			return s;
		int pos = s.indexOf(src);
		if(pos < 0)
		{
			return s;
		}
		StringBuilder sb = new StringBuilder(s.length() - src.length() + dest.length());
		
		sb.append(s, 0, pos);
		sb.append(dest);
		sb.append(s, pos + src.length(), s.length());
		return sb.toString();
	}

	/**
	 *@see java.lang.String#trim()
	 */

    
    public static String removeAll(String s, String src)
    {
    	return replaceAll(s, src, "");
    }
   

    /**
     * 以某一长度缩写字符串（1个中文或全角字符算2个长度单位，英文或半角算一个长度单位）.
     * 如果要显示n个汉字的长度，则maxlen= 2* n
     * @param src utf-8字符串
     * @param maxlen 缩写后字符串的最长长度（1个中文或全角字符算2个单位长度）
     * @param replacement 替换的字符串，该串长度会计算到maxlen中
     * @return String
     */
    public static String abbreviate(String src, int maxlen, String replacement){
        
        if(src==null)  return "";
        
        if ( replacement == null ) {
            replacement = "";
        }
        
        StringBuffer dest = new StringBuffer();                         //初始值设定为源串
        
        try{
            maxlen = maxlen - computeDisplayLen(replacement);

            if ( maxlen < 0) {
                return src;
            }

            int i = 0;
            for(; i < src.length() && maxlen > 0; ++i)
            {
                char c = src.charAt(i);
                if(c >= '\u0000' && c <= '\u00FF') {
                    maxlen = maxlen - 1;
                } else {
                    maxlen = maxlen - 2;
                }
                if ( maxlen >= 0 ) {
                    dest.append(c);
                }
            }
            
            //没有取完 src 所有字符时，才需要加后缀 ...
            if(i<src.length()-1){
            	dest.append( replacement );
            }
            return dest.toString();
        }catch(Throwable e){
        	e.printStackTrace();
        }
        return src;
    }
    
    /**
     * @see  abbreviate(String src, int maxlen, String replacement)
     * @param src
     * @param maxlen
     * @return
     */
    public static String abbreviate(String src, int maxlen) 
    {
        return abbreviate(src, maxlen, "");
    }
    
	/**
	 * 将字符串截短,功能与abbreviate()类似
	 * 全角字符算一个字,半角字符算半个字,这样做的目的是为了显示的时候排版整齐,因为全角字占的位置要比半角字小
	 * @param str
	 * @param maxLen
	 * @return String
	 */
	public static String toShort(String str, int maxLen, String replacement)
	{
		if(str == null)
		{
			return "";
		}
		if(str.length() <= maxLen)
		{
			return str;
		}
		StringBuilder dest = new StringBuilder();
		double len = 0;
		for (int i = 0; i < str.length(); i++)
		{
			char c = str.charAt(i);
			if(c >= '\u0000' && c <= '\u00FF')
			{//半角字算半个字
				len += 0.5;
			}
			else
			{
				len += 1;
			}
			if(len > maxLen)
				return dest.toString() + replacement;
			else
				dest.append(c);
		}
		return dest.toString();
	}
	
	public static String toShort(String str, int maxLen)
	{
		return toShort(str, maxLen, "...");
	}
    
    /**
     * 计算字符串的显示长度，半角算１个长度，全角算两个长度
     * @param s
     * @return
     */
    public static int computeDisplayLen( String s ) {
        int len = 0;
        if ( s == null ) {
            return len;
        }
       
        for(int i = 0; i < s.length(); ++i)
        {
            char c = s.charAt(i);
            if(c >= '\u0000' && c <= '\u00FF') {
                len = len + 1;
            } else {
                len = len + 2;
            }
        }
        return len;
    }
	
	
    
}

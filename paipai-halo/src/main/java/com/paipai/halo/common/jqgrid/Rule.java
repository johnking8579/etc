package com.paipai.halo.common.jqgrid;

/**
 * jqgrid搜索过滤器相关模型, 表示在哪个字段上搜索什么值.
 * 
 */
public class Rule {
	private String field;	//字段名
	private String op;		//>, <, >=, <=等等, 见Const.SEARCH_OPER_*
	private String data;	//输入的搜索值
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}

package com.paipai.halo.common.exception;

public class CmsException extends RuntimeException{

	public CmsException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CmsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CmsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CmsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}

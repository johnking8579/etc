package com.paipai.halo.common.ump;

import org.aspectj.lang.ProceedingJoinPoint;

import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;


/**
 * 上报至UMP统一监控平台.为避免到处在代码中引用UMP,降低与UMP耦合,做下简单封装
 */
public class Monitor {
	
	private static final String SYSTEM = "paipai-halo";

	/**
	 * 注册JVM和系统心跳监控, 在spring中初始化一次即可
	 */
	public void initHeartbeatAndJvm()	{
		Profiler.InitHeartBeats(SYSTEM + ".heartbeat");
		Profiler.registerJVMInfo(SYSTEM + ".jvm");
	}
	
	
	/**
	 * 开启监控method. 为避免到处在代码中引用UMP, 做下简单封装
	 * <pre>使用方法:
	 * public void methodA()	{
	 * 	Profiler.registerInfo(key, enableHeartbeat, enableTP);
	 * 	.....do sth
	 * 	Profiler.registerInfoEnd((CallerInfo)callerInfo);
	 * }</pre>
	 * @param key
	 * @return
	 */
	public static Object registerInfo(String key)	{
		try {
			return Profiler.registerInfo(key, SYSTEM, false, true); //不监控方法心跳, 只监控方法执行时间
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 此方法调用则记录本次方法执行失败，在一定时间粒度内该方法的调用失败次数除以调用总次数即得到该方法执行成功率。注意：该方法需保证在Profiler.registerInfoEnd(info)之前被调用
	 * @param callerInfo
	 */
	public static void functionError(Object callerInfo)	{
		if(callerInfo != null)
			Profiler.functionError((CallerInfo)callerInfo);
	}
	
	/**
	 * 结束监控method. 为避免到处在代码中引用UMP, 做下简单封装
	 * @param callerInfo Profiler.registerInfo的返回值
	 */
	public static void registerInfoEnd(Object callerInfo)	{
		try {
			if(callerInfo != null)	{
				Profiler.registerInfoEnd((CallerInfo)callerInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 立即报警. 自动生成key,key=调用栈中的上一级调用者的包名+方法名
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(String detail)	{
		StackTraceElement ele = Thread.currentThread().getStackTrace()[2];
		String key = ele.getClassName() + "." + ele.getMethodName() + "()";
		Profiler.businessAlarm(key, System.currentTimeMillis(), detail);
	}

	/**
	 * 立即报警. 自动生成key,key=调用栈中的上一级调用者的包名+方法名
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(int callerIndex, String detail)	{
		StackTraceElement ele = Thread.currentThread().getStackTrace()[callerIndex];
		String key = ele.getClassName() + "." + ele.getMethodName() + "()";
		Profiler.businessAlarm(key, System.currentTimeMillis(), detail);
	}
	
	/**
	 * 立即报警
	 * @param key 在http://ump.360buy.com上注册的自定义监控点的key,为空或仅含有空格则不会报警. 一般是包名+方法名
	 * @param time 该条信息产生的时间(ms)
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(String key, long time, String detail)	{
		Profiler.businessAlarm(key, time, detail);
	}
	
	
	/**
	 * 上报dao层运行时长. 在spring配置文件里配置.
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	public Object reportDao(ProceedingJoinPoint pjp) throws Throwable	{
		int hash = pjp.getSignature().toLongString().hashCode();
		String daoId = String.format("%s.%s(%s)", pjp.getTarget().getClass().getSimpleName(), 
													pjp.getSignature().getName(), 
													Math.abs(hash - hash/100*100)); 
		CallerInfo caller = Profiler.registerInfo(UmpKeyGen.gen1stDao(daoId), SYSTEM, false, true);
		try {
			return pjp.proceed();
		} catch (Exception e) {
			Profiler.functionError(caller);
			throw e;
		} finally	{
			Profiler.registerInfoEnd(caller);
		}
	}
	
}

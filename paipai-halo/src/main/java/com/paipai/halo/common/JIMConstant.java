package com.paipai.halo.common;

/**
 * JIM使用的公共参数
 * @author zhaohuayu
 *
 */
public class JIMConstant {
	/*************************************** 签到相关前缀 ***********************************************/
	public static final String SIGN_PRIZELIST = "sign_prizeList_" ;
	public static final String SIGN_PRIZEINFO = "sign_prizeInfo_" ;
	public static final String SIGN_POINT_CACHE = "sign_signPointCachePo_" ;
	public static final String KEY_SEPARATOR = "_" ;
	public static final Long JIM_EFFECTIVE_TIME = 1L;
	
	//评论最后一条缓存前缀
	public static final String JIM_LASTCOMM_FIRST = "user_lastcomment_" ;
	public static final Long   JIM_LASTCOMM_PL_FT = 30L; //30分钟
	
	//将某人的点赞记录放入到缓存中。只放4小时
	public static final String JIM_USERLIKEINFO_FIRST = "user_likeinfolog_" ;
	public static final Long   JIM_USERLIKEINFO_FT = 4L; //4小时
	/**
	 * 获得奖品详情JIM KEY
	 * @param prizeId 奖品ID
	 * @param activeDay 查询天
	 * @param type 0：今天   1：非今天
	 * @return
	 */
	public static String getPrizeInfoKey(String prizeId, String activeDay, int type) {
		StringBuffer sb = new StringBuffer() ;
		return sb.append(SIGN_PRIZEINFO).append(prizeId)
				.append(KEY_SEPARATOR).append(activeDay)
				.append(KEY_SEPARATOR).append(type).toString() ;
	}
	/**
	 * 获得奖品列表JIM key
	 * @param prizeLevel
	 * @param activeId
	 * @return
	 */
	public static String getPrizeListKey(int prizeLevel, int activeId) {
		StringBuffer sb = new StringBuffer() ;
		return sb.append(SIGN_PRIZELIST).append(prizeLevel)
				.append(KEY_SEPARATOR).append(activeId).toString();
	}

	/**
	 * 获得用户点券信息
	 * @param wid
	 * @param activeId
	 * @return
	 */
	public static String getSignPointCachePoKey(long wid, int activeId) {
		StringBuffer sb = new StringBuffer() ;
		return sb.append(SIGN_POINT_CACHE).append(wid)
				.append(KEY_SEPARATOR).append(activeId).toString();
	}
	
}

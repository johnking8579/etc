package com.paipai.halo.common.jqgrid;

import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;

/**
 * url参数:search=true&nd=1423105348779&rows=20&page=1&sidx=ID&sord=desc&searchField=name&searchString=222222&searchOper=cn&filters=
 * 其中filters={"groupOp":"AND","rules":[{"field":"id","op":"bw","data":"1"},
 * {"field":"name","op":"bw","data":"quick"},
 * {"field":"osName","op":"bw","data":"android"} ]}
 * 审核时isshow
 */
public class JqgridReq {
	private boolean search;
	private int rows, page;
	private Filters filters;
	private String oper, sidx, sord, searchField, searchString, searchOper,isshow;
	
	public static JqgridReq parse(Map<String, String[]> requestParam)	{
		JqgridReq req = new JqgridReq();
		for(Entry<String,String[]> e : requestParam.entrySet())	{
			if("_search".equals(e.getKey()))	{		//jqGrid默认传值为_search
				req.setSearch(Boolean.parseBoolean(e.getValue()[0]));
			} else if("rows".equals(e.getKey()))	{
				req.setRows(Integer.parseInt(e.getValue()[0]));
			} else if("page".equals(e.getKey()))	{
				req.setPage(Integer.parseInt(e.getValue()[0]));
			} else if("oper".equals(e.getKey()))	{
				req.setOper(e.getValue()[0]);
			} else if("sidx".equals(e.getKey()))	{
				req.setSidx(filterChar(e.getValue()[0]));
			} else if("sord".equals(e.getKey()))	{
				req.setSord(filterChar(e.getValue()[0]));
			} else if("searchField".equals(e.getKey()))	{
				req.setSearchField(filterChar(e.getValue()[0]));
			} else if("searchString".equals(e.getKey()))	{
				req.setSearchString(e.getValue()[0]);
			} else if("searchOper".equals(e.getKey()))	{
				req.setSearchOper(e.getValue()[0]);
			} else if("filters".equals(e.getKey()))	{
				req.setFilters(new Gson().fromJson(e.getValue()[0], Filters.class));
			} else if ("isshow".equals(e.getKey())) {
				req.setIsshow(e.getValue()[0]);
			}
		}
		return req;
	}
	
	private static String filterChar(String s)	{
		if(s == null)
			return null;
		else
			return s.replace(";", "");
	}
	
	/**
	 *  将从1开始的pageNo转成offset
	 * @return SELECT * FROM T LIMIT ? , 10 , offset即start
	 */
	public int calcOffset()	{
		return (page - 1) * rows;
	}
	

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Filters getFilters() {
		return filters;
	}

	public void setFilters(Filters filters) {
		this.filters = filters;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getSearchOper() {
		return searchOper;
	}

	public void setSearchOper(String searchOper) {
		this.searchOper = searchOper;
	}

	public String getIsshow() {
		return isshow;
	}

	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}

}

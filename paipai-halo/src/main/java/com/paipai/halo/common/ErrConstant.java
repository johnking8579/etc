package com.paipai.halo.common;

/**
 * 此处定义的是webapp_qqbuy对外和对内的错误码，其他系统调用产生的错误码请放在ErrThirdConstant
 * 
 * @author winsonwu
 * 
 */
public interface ErrConstant {
	/*************************************** 操作成功 ***********************************************/
	public static final int SUCCESS = 0;

	/*************************************** 公共错误编码 ********************************************/
	public static final int ERRCODE_INVALID_PARAMETER = 0x0100; // 参数校验不合法 =256
	public static final int ERRCODE_CHECKLOGIN_FAIL = 0x0102; // 登录态校验失败
	public static final int ERRCODE_ACTION_UNKNOW_EXP = 0x0103; // 在Action处捕获到未知的Exception =259
	public static final int ERRCODE_COMMON_CFG_NOT_FOUND = 0x0104; // 配置中心的配置项没配置
	public static final int ERRCODE_COMMON_NOTSUPPORT_WXPAY = 0x0105; //不支持微信支付类型

	/*************************************** DAO调用过程相关异常编码 **********************************/
	public static final int ERRCODE_DAO_NULLSESSION = 0x0200; // 无法取得Sql Session
	public static final int ERRCODE_DAO_SQL_EXECUTION_FAIL = 0x0201; // SQL执行抛出异常
	public static final int ERRCODE_DAO_MULTI_NOT_COMPLETE = 0x0202; // 进行多表操作时候，没有全部成功
	public static final int ERRCODE_DAO_QUERY_DB_FAIL = 0x0203; // 查询DB出错，因为现在查询部分异常无法对外暴露，故统一使用此处ErrorCode

	/*************************************** 接口调用失败 **********************************/
	public static final int HTTP_FAILURE = 0x0300; 
	public static final int IDL_FAILURE = 0x0301; //=769

	/*************************************** 内部ErrCode ********************************************/
	// OpenAPI相关
	public static int INERRCODE_OPENAPI_XML_IS_NULL = -1;
	public static int INERRCODE_OPENAPI_PARSE_XML_FAILED = -2;
	public static int INERRCODE_OPENAPI_XML_DOM_FAILED = -3;
	public static int INERRCODE_OPENAPI_HTTP_IO_FAILED = -4;
	public static int INERRCODE_OPENAPI_BIZ_FAILED = -5; // TODO winson

	/*************************************** 商品 ********************************************/
	public static final int ERRCODE_ITEMWAP2_NOT_FOUND = 0x1001; // 根据itemCode获取商品信息失败
	public static final int ERRCODE_ITEMWAP2_EXTINFO_FAIL = 0x1002; // 根据itemCode查找商品扩展信息时报
	public static final int ERRCODE_ITEM_EVAL_FAIL = 0x1003; // 根据itemCode查找商品评论信息时报
	public static final int ERRCODE_ITEM_DETAIL_FAIL = 0x1004; // 根据itemCode查找商品详情报错
	public static final int ERRCODE_ITEMWAP2_ITEM_ID_ILLEGAL = 0x1005; // 商品id不合法
	public static final int ERRCODE_FAV_ITEM = 0x1006; // 不能收藏自己的商品

	/*************************************** 订单 ********************************************/
	public static final int INVALID_ITEMINFO = 0x2001; // 商品信息不全
	public static final int INVALID_ITEMCOUNT = 0x2002; // 商品数量无效
	public static final int INVALID_ADDRESS = 0x2003; // 收货地址无效
	public static final int INVALID_PP_DEAL = 0x2004; // 拍拍订单生成失败
	public static final int INVALID_PAY_INFO = 0x2005; // 支付中心调用失败
	public static final int COD_DEAL_FAILURE = 0x2006; // 货到付款订单生成失败
	public static final int INVALID_PAY_TYPE = 0x2007; // 支付类型错误
	public static final int INVALID_STOCKATTR = 0x2008; // 商品属性无效
	public static final int DEAL_NOT_EXIST = 0x2009; // 商品属性无效
	public static final int DEAL_TOO_MANY = 0x2010; // 支付订单太多
	public static final int PC_COD_DEAL_FAILURE = 0x2011; // PC货到付款订单生成失败
	public static final int EVAL_PARAM_ERROR = 0x2012; // 评价参数错误
	public static final int ALL_EVAL_DONE = 0x2013; // 没有可评价的订单
	public static final int EVAL_CHECK_LOGIN_FAIL = 0x2014; // 订单评价鉴权失败
	public static final int EVAL_IDL_ERROR = 0x2015; // 调用订单评价IDL失败
	public static final int CFMRECV_PARAM_ERROR = 0x2016; // 确认收货参数错误
	public static final int COD_MSG_FAILURE = 0x2017; // 确认订单失败
	public static final int CFMRECV_IDL_ERROR = 0x2018; // 确认收货
	public static final int INVALID_PAY_LSKEY = 0x2019; // 确认收货获取跳转财付通页面URL，lskey为空
	public static final int CFMRECV_IDL_PARAM_ERROR = 0x2020; // 确认收货调用idl参数错误
	public static final int INVALID_PAYTYPE_WXPAY = 0X2021; //非法订单，不支持微信支付类型
	public static final int INVALID_PAYTYPE_TENPAY = 0X2022; //非法订单，不支持财富通支付类型
	public static final int INVALID_PAYTYPE_JDPAY = 0X2023; //非法订单，不支持京东收银台支付类型
	public static final int INVALID_PAYTYPE_OFF_LINE = 0X2024; //非法订单，不支持货到付款
	/*************************************** 搜索 ********************************************/
	public static final int ERRCODE_SEARCH_PARSE_RESULT_FAILED = 0x3001; // 解析搜索返回内容失败
	public static final int ERRCODE_SEARCH_NO_RESULT = 0x3002; // 搜索cgi没有返回内容
	public static final int ERRCODE_SEARCH_WRONG_RESULT = 0x3003; // 搜索cgi返回错误码
	/*************************************** 站内信 ********************************************/
	public static final int ERRCODE_MSG_LIST_FAIL = 0x4001; // 查询列表失败
	public static final int ERRCODE_MSG_DETAIL_FAIL = 0x4002; // 查看详情失败
	public static final int ERRCODE_MSG_DELETE_FAIL = 0x4003; // 删除失败
	public static final int ERRCODE_MSG_MARKREAD_FAIL = 0x4004; // 标记读失败
	public static final int ERRCODE_MSG_COUNT_FAIL = 0x4005; // 取数量失败

	/*************************************** 用户 ********************************************/
	public static final int ERRCODE_SUPERQQ_FAIL = 0x4001; // 查超Q失败
	public static final int ERRCODE_COLORDIEMOND_FAIL = 0x4002; // 查彩钻失败
	public static final int ERRCODE_ADD_FEEDBACK_FAIL = 0x4003;// 反馈失败
	public static final int ERRCODE_QUERY_USER_FAIL = 0x4004;// 查询用户信息失败

	/*************************************** 支付 ********************************************/
	public static final int INVALID_TOKEN = 0x5001; // 商品属性无效

	/*************************************** 活动 ********************************************/
	public static final int INVALID_ACTIVITY_LIST = 0x6001; // 活动列表请求失败
	public static final int INVALID_ACTIVITY_DETAIL = 0x6002; // 活动列表请求失败

	/*************************************** 物流信息 ********************************************/
	public static final int ERRCODE_MSG_GETWULIU_FAIL = 0x7001; // 物流商异常
	public static final int ERRCODE_MSG_DEAL_NOT_HAS_WULIUINFO = 0x7002; // 订单不包括物流信息
	public static final int ERRCODE_MSG_COMPANY_NAME_IS_NULL = 0x7003; // 物流公司名称为空
	public static final int ERRCODE_MSG_CANNOT_FIND_COMPANYID = 0x7004; // 查找不到物流公司名称
	public static final int ERRCODE_DEAL_CALC_SHIPPING_FAIL = 0x7005; // 根据运费模版计算运费失败
	public static final int ERRCODE_SHIP_CODTMPID_NO_SUPPORT = 0x7006; // 运费模版类型不支持
	public static final int ERRCODE_SHIP_CALC_CODCOUNT_FAIL = 0x7007; // 计算货到付款运费出错

	/*************************************** 虚拟充值 **********************************************/
	public static final int ERRCODE_BIZ_VB2C_MOBILE_SHORTAGE = 0x1400; // 话费充值特定面额缺货
	public static final int ERRCODE_BIZ_VB2C_GET_TENPAY_TOKEN_FAIL = 0x1401; // 虚拟充值下单去财付通处获取tokenId失败
	public static final int ERRCODE_BIZ_VB2C_GEN_TENPAY_WAPURL_FAIL = 0x1402; // 虚拟充值生成wap的下单财付通页面访问url失败
	public static final int ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL = 0X1403; // 获取虚拟充值下单详情失败
	public static final int ERRCODE_BIZ_VB2C_MOBILE_ORDER_DEAL_FAIL = 0x1404; // 虚拟话费充值下单失败
	public static final int ERRCODE_BIZ_VB2C_GAME_ORDER_DEAL_FAIL = 0x1405; // 虚拟网游充值下单失败
	public static final int ERRCODE_HTTP_VB2C_TENPAY_NOTIFY_FAIL = 0x1406; // 通知虚拟充值中心财付通支付情况
	public static final int ERRCODE_BIZ_VB2C_GAME_DETAIL_FAIL = 0x1407; // 虚拟充值网游信息获取失败情况
	public static final int ERRCODE_BIZ_VB2C_QQSERVICE_ORDER_DEAL_FAIL = 0x1408; // 虚拟QQ服务充值下单失败
	public static final int ERRCODE_VB2C_CALL_IDL_FAIL = 0x1409; // 调用虚拟IDL接口失败

	/*************************************** 购物车操作 **********************************************/
	public static final int ERRCODE_CMDY_ADD_FAIL = 0x1500; // 购物车新增商品失败
	public static final int ERRCODE_CMDY_LIST_FAIL = 0x1501; // 查询购物车列表失败
	public static final int ERRCODE_CMDY_MODIFYNUM_FAIL = 0x1502; // 修改购物车中商品数量失败
	public static final int ERRCODE_CMDY_REMOVE_FAIL = 0x1503; // 删除购物车中商品失败
	public static final int ERRCODE_DEAL_ADDRLIST_EMPTY = 0x1504; // 收货地址列表为空
	public static final int ERRCODE_DEAL_FINDADDR_FAIL = 0x1505; // 查找不到收货地址
	public static final int ERRCODE_DEAL_FINDITEM_FAIL = 0x1506; // 商品详情查询失败
	public static final int ERRCODE_CMDY_CONFIRMORDER_FAIL = 0x1507; // 购物车分单失败
	public static final int ERRCODE_CMDY_MAKEORDER_FAIL = 0x1508; // 购物车下单失败
	public static final int ERRCODE_CMDY_MAKEORDER_FAIL_SAME = 0x1509;// 买卖家相同
	public static final int ERRCODE_CMDY_MAKEORDER_FAIL_PARAM = 0x1510;// 参数错误
	public static final int ERRCODE_CMDY_ADD_FAIL_LIMIT = 0x1511;// 您购买的商品数量已经超过了卖家设定的最大限额。如果您想购买更多商品，请与卖家联系。
	public static final int ERRCODE_CMDY_ADD_FAIL_FRE = 0x1512;// 操作频繁
	public static final int ERRCODE_CMDY_ADD_FAIL_IP = 0x1513;// 操作频繁
	public static final int ERRCODE_CMDY_MAKEORDER_FAIL_OVER_LIMIT=0x1514;//一口价商品购买超过卖家对单个买家的最大限制
	public static final int ERRCODE_CMDY_ADD_FAIL_DW = 0x1515;// 商品已下回，不能购买
	public static final int ERRCODE_CMDY_ADD_FAIL_LESS = 0x1516;// 商品库存不足
	/*************************************** 收货地址操作 **********************************************/
	public static final int ERRCODE_QUERY_ADDR_FAIL = 0x1600; // 找不到收货地址
	public static final int ERRCODE_ADD_ADDR_LIMIT = 0x1601; // 找不到收货地址
	
	/****************************************批价******************************************************/
	public static final int ERRCODE_PROMOTE_PARAM_ERROR = 0x1701; 			// 批价请求参数错误
	public static final int ERRCODE_PROMOTE_COMBO_IDL_FAIL = 0x1702; 		// 商品套餐接口异常
	public static final int ERRCODE_PROMOTE_ITEM_PRICE_IDL_FAIL = 0x1703; 	// 商品批价接口异常
	public static final int ERRCODE_PROMOTE_SHOP_ACTIVE_IDL_FAIL = 0x1704; 	// 店铺促销接口异常
	public static final int ERRCODE_PROMOTE_RED_PKG_IDL_FAIL = 0x1705; 		// 商品红包接口异常
	public static final int ERRCODE_PROMOTE_IDL_TIME_OUT = 0x1706; 			// 超时
	public static final int ERRCODE_PROMOTE_ITEM_FAVOR_ERROR = 0x1707; 		// 商品请求优惠不存在或异常
	public static final int ERRCODE_PROMOTE_DEAL_FAVOR_ERROR = 0x1708; 		// 订单请求优惠不存在或异常
	public static final int ERRCODE_PROMOTE_ACTIVE_FAVOR_ERROR = 0x1709; 	// 店铺促销请求优惠不存在或异常
	public static final int ERRCODE_PROMOTE_MAX_DEAL_FEE_ERROR = 0x1710; 	// 批价请求参数错误
	public static final int ERRCODE_PROMOTE_OTHER_ERROR = 0x1711; 			// 其他错误

	/*************************************** 在线支付 0x1200 *******************************************/
	public static final int ERRCODE_TENPAY_HTTP_POST_FAIL = 0x1200; // 财付通http协议出现异常
	public static final int ERRCODE_TENPAY_BUILD_INIT_PARAMS_FAIL = 0x1201; // 拼接财付通init参数失败
	public static final int ERRCODE_TENPAY_BUILD_GATE_PARAMS_FAIL = 0x1202; // 拼接财付通gate参数失败
	public static final int ERRCODE_TENPAY_GETTOKEN_FAIL = 0x1203; // 解析财付通回包获取不到tokenId
	public static final int ERRCODE_TENPAY_SIGN_VERIFY_FAIL = 0x1204; // 财付通参数的MD5校验失败
	public static final int ERRCODE_TENPAY_NOTFINDSIGNKEY = 0x1205; // 没有找到MD5签名的Key
	public static final int ERRCODE_TENPAY_PAIPAIDEAL_INVALID = 0x1206; // 拍拍订单数据不符合要求
	public static final int ERRCODE_TENPAY_CALLBACK_MD5_FAIL = 0x1207; // callback回调参数MD5校验sign失败
	public static final int ERRCODE_TENPAY_NOTIFYPARAM_TRANS_FAIL = 0x1208; // notify回调参数转调PC同步状态参数失败
	public static final int ERRCODE_TENPAY_NOTIFY5TIMES_FAIL = 0x1209; // notify回调PC侧5次还是失败
	public static final int ERRCODE_TENPAY_SELLER_NOCONFIG_SPID = 0x1210; // 卖家没有配置相应的spid
	public static final int ERRCODE_TENPAY_NOTIFY_ATTACH_INVALID = 0x1211; // notify返回的Attach字段有问题
	public static final int ERRCODE_DEAL_AFTER_ORDERDEAL_FAIL = 0x1106; // 下单后业务逻辑错误
	/*************************************** Json接口对外的错误码 ************************************/
	public static final int ERRCODE_JSON_CHECKLOGIN_FAIL = 0x0102;
	public static final int ERRCODE_FIND_THROWABLE_EXP = 0x0103;

	/**************************************** PASSBOOK ******************************************/
	public static final int ERRCODE_PASSBOOK_ERROR = 0x2300; // 获取passbook失败

	/**************************************** 搜索热词 ******************************************/
	public static final int ERRCODE_SEARCHRC_ERROR = 0x2400; // 搜索热词PPMS上配置数据无效
	/**************************************** 搜索热词 ******************************************/
	public static final int ERRCODE_SEARCHSHOP_ERROR = 0x2500; // 调用搜索店铺接口失败
	
	public static final int ERRCODE_POINT_LACK = 0x3001; //用户点券不足
	public static final int ERRCODE_PRIZE_USED_UP = 0x3002; //奖品已领完
	public static final int ERRCODE_SIGNED = 0x3003; //已经签到过
	public static final int ERRCODE_NOTBEGIN = 0x3004; //签到活动还未开始
	public static final int ERRCODE_HASEND = 0x3005; //签到活动已结束
	
	public static final int ERRCODE_SAVECOMM_FAIL_WID = 0x1512;// 操作频繁
}

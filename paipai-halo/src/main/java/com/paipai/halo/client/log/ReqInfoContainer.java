package com.paipai.halo.client.log;


public class ReqInfoContainer {
	
	private static ThreadLocal<ReqInfo> t = new ThreadLocal<ReqInfo>();
	
	public static void set(Object umpCaller, long startTime, long wid, String requestId)	{
		ReqInfo r = new ReqInfo(umpCaller, startTime, wid, requestId);
		t.set(r);
	}
	
	public static Object getUmpCaller()	{
		ReqInfo r = t.get();
		return r == null ? null : r.getUmpCaller(); 
	}
	
	public static long getStartTime()	{
		ReqInfo r = t.get();
		return r == null ? 0L : r.getStartTime();
	}
	
	public static long getWid()	{
		ReqInfo r = t.get();
		return r == null ? 0L : r.getWid();
	}
	
	public static String getRequestId()	{
		ReqInfo r = t.get();
		return r == null ? null : r.getRequestId();
	}
}

package com.paipai.halo.client.log;

public class ReqInfo {

	private Object umpCaller;
	private long startTime, wid;
	private String requestId;
	
	public ReqInfo() {	}
	
	public ReqInfo(Object umpCaller, long startTime, long wid, String requestId) {
		this.umpCaller = umpCaller;
		this.startTime = startTime;
		this.wid = wid;
		this.requestId = requestId;
	}

	public Object getUmpCaller() {
		return umpCaller;
	}

	public void setUmpCaller(Object umpCaller) {
		this.umpCaller = umpCaller;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public long getWid() {
		return wid;
	}

	public void setWid(long wid) {
		this.wid = wid;
	}
}

package com.paipai.halo.client.ao.login;

import java.util.BitSet;
import java.util.Map;
import java.util.Vector;
import java.util.Set;

import com.paipai.halo.client.ao.login.protocal.BuyerInfoPo;
import com.paipai.halo.client.ao.login.protocal.LoginInfoPo;
import com.paipai.lang.uint8_t;
import com.paipai.lang.uint16_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;


/**
 * ao_user_wanggou_bj
 * 
 * @author yujianming
 * 
 * 接入帮助文档请见：http://tc-svn.tencent.com/paipai/paipai_b2b2c_rep/document_proj/trunk/设计文档/用户账户/帮助文档/电商统一用户接入帮助文档.doc
 * 
 */
@HeadApiProtocol(cPlusNamespace = "b2b2c::user::ao", needInit = true, needReset = true)
public class UserWangGouBjAo {

    @Member(desc = "绑定帐号po", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = false, isNeedTempVersion=true)
    public class BindInfoPo {
        @Field(desc = "版本号", defaultValue = "0")
            uint32_t version;

        @Field(desc = "绑定帐号类型，必需，0-无效值 1-email 2-手机号，3-易迅微信号，4-网购微信公众号(填4时，appid和openid必填).目前只支持4")
            uint8_t bindInfoType;

        @Field(desc = "微信appid,bindInfoType为4时必填")
            String appid;

        @Field(desc = "网购微信openid,bindInfoType为4时必填")
            String openid;

        @Field(desc = "预留")
            String reserve;	

        uint8_t version_u;
        uint8_t bindInfoType_u;
        uint8_t appid_u;
        uint8_t openid_u;
        uint8_t reserve_u;
    }	


    @Member(desc = "微信公众号登陆下默认登陆类型do", cPlusNamespace = "b2b2c::user::ddo", isNeedUFlag = false, isNeedTempVersion=true)
    public class WechatDfLoginTypeDo {
        @Field(desc = "版本号", defaultValue = "0")
            uint32_t version;

        @Field(desc = "微信openid")
            String openid;

        @Field(desc="默认登陆类型")
            uint32_t defaultLoginType;

        @Field(desc = "添加时间")
            uint32_t addTime;

        @Field(desc = "预留")
            String Reserve;	
    }

    /**
     * QQ号登录：
     * loginInfoPo.SetAccountType(1); // 1:代表QQ号类型
     * loginInfoPo.SetLoginFrom(3);   // 3:代表拍拍 2:代表网购
     * loginInfoPo.SetQQNumber(qq);   // 登录QQ号
     * loginInfoPo.SetQQSkey(p_skey); // 这里填p_skey
     * 
     * CCGI_STUB_CNTL->setDomainId(domainId); // DomainID必填, 具体填即通分配的domainid, 拍拍的是126,网购的是154
     * 
     * QQ号弱登陆
     * loginInfoPo.SetAccountType(1); //1代表QQ类型
     * loginInfoPo.SetLoginFrom(10);  //网购弱登陆
     * loginInfoPo.SetQQNumber(qq);   //登陆qq号
     * loginInfoPo.SetQQSkey(lskey);  //填lskey
     * loginInfoPo.SetLoginIntensityType(2);  //弱登陆
     * 
     * 无线sid登陆
     * loginInfoPo.SetAccountType(1); //1代表QQ类型
     * loginInfoPo.SetLoginFrom(11); //sid登录
     * loginInfoPo.SetQQSkey(sid); //填sid
     * 
     * 微购直登 openid+ appid + token/ticket登录:
     * loginInfoPo.SetAccountType(3); // 3:代表使用OPENID登录
     * loginInfoPo.SetLoginFrom(7);   // 7:代表登录源为微信
     * loginInfoPo.SetOpenidFrom(4);  // 4:代表来自微购
     * loginInfoPo.SetOpenid(openid);
     * loginInfoPo.SetAppid(appid);   // 填微信appid
     * loginInfoPo.SetPasswd(token);  // 填微信token或者ticket
     * loginInfoPo.SetAuthType(1或者2); //填1表示用ticket鉴权，填2表示用token鉴权
     * 
     * 网购公众号 直登 openid+ appid + token
     * loginInfoPo.SetAccountType(3); // 3:代表使用OPENID登录
     * loginInfoPo.SetLoginFrom(7);   // 7:代表登录源为微信
     * loginInfoPo.SetOpenidFrom(8);  // 8:代表来自网购公众号
     * loginInfoPo.SetOpenid(openid);
     * loginInfoPo.SetAppid(appid);   // 填微信appid
     * loginInfoPo.SetPasswd(token);  // 填微信token
     * 
     * 微信绑定qq号登录（不带qq号登陆）　openid + appid +　token/ticket登录:
     * loginInfoPo.SetAccountType(3); // 3:代表使用OPENID登录
     * loginInfoPo.SetLoginFrom(9);   // 9:代表登录源为微信绑定qq号
     * loginInfoPo.SetOpenid(openid);
     * loginInfoPo.SetAppid(appid);   // 填微信appid
     * loginInfoPo.SetPasswd(token);  // 填微信token或者ticket
     * loginInfoPo.SetAuthType(1或者2);   //填1表示用ticket鉴权，填2表示用token鉴权
     *
     * 微信绑登 openid + appid + token
     * loginInfoPo.SetAccountType(3); // 3:代表使用OPENID登录
     * loginInfoPo.SetLoginFrom(8);   // 8:代表绑登
     * loginInfoPo.SetOpenid(openid); // 填微信openid
     * loginInfoPo.SetAppid(appid);   // 填微信appid
     * loginInfoPo.SetPasswd(token);  // 填微信token
     * 
     * 微信绑定qq号登陆（带qq号登陆，无需用openid换取qq号） qq + QQSkey(约定的鉴权码)
	 * loginInfoPo.SetAccountType(1); //1代表QQ类型
     * loginInfoPo.SetLoginFrom(9);   // 9:代表登录源为微信绑定qq号
	 * loginInfoPo.SetQQNumber(qq); //微信绑定的qq号
	 * loginInfoPo.SetQQSkey(authCode);//约定的鉴权码，具体联系wisdomlin获取
     *
     * @author moovyyang
     *
     */
    @ApiProtocol(cmdid = "0x30081801L", desc = "网购拍拍用户统一登录")
    class UniformLogin {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;	

            @Field(desc = "选项掩码，0--默认值，4--GBK编码(默认UTF8编码)，具体参见user_comm_define.h中的E_GETUSER_OPTION")
                uint32_t option;

            @Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc = "登录信息po, 【注】当前只支持QQ号、微信openid登录")
                LoginInfoPo loginInfoPo;														

            @Field(desc = "输入保留字")
                String inReserve;
        }
        class Resp {

            @Field(desc = "统一后台用户id，目前仅支持32位")
                uint64_t wgUid;

            @Field(desc = "sessionKey，当accountType填1时输出为qqskey")
                String skey;

            @Field(desc = "用户昵称，当accountType填1时取的是oidb昵称")
                String nickname;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081802L", desc = "根据QQ号获取网购用户id, 若统一后台不存在该QQ号账户，则注册一个新用户.需带登录态.")
    class GetWgUidByQQ {   
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;	

            @Field(desc = "用户QQ号, 目前仅支持32位")
                uint64_t qQNumber;	

            @Field(desc = "输入保留字")
                String inReserve;
        }

        class Resp {
            @Field(desc = "统一后台用户id，目前仅支持32位")
                uint64_t wgUid;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081803L", desc = "用户验登录，请务必在rcntlinfo中设置wgUid和skey")
    class CheckLoginByWgUid {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;											

            @Field(desc = "输入保留字")
                String inReserve;
        }

        class Resp {

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081804L", desc = "用户验登录，请务必在rcntlinfo中设置QQ号和QQ skey, 若已登录, 则返回统一后台用户id")
    class CheckLoginByQQ {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;												

            @Field(desc = "输入保留字")
                String inReserve;
        }

        class Resp {
            @Field(desc = "统一后台用户id，目前仅支持32位")
                uint64_t wgUid;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081805L", desc = "临时接口：支持微客服将已存在的commid和wguid导入到统一用户后台")
    class ImportWeigouUser {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "微信commid, 必填")
                String wechatCommid;

            @Field(desc = "统一后台用户id")
                uint64_t wgUid;

            @Field(desc = "输入保留字")
                String inReserve;
        }

        class Resp {
            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081806L", desc = "根据统一用户id获取用户信息")
    class GetUserInfoByWgUid {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "选项掩码，默认填0即可")
                uint32_t option;

            @Field(desc = "统一用户id")
                uint64_t wgUid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }	

    @ApiProtocol(cmdid = "0x30081807L", desc = "根据QQ号获取用户信息.")
    class GetUserInfoByQQ {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;			

            @Field(desc = "用户QQ号，必填")
                uint64_t qqNumber;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081808L", desc = "根据微信openid获取用户信息.")
    class GetUserInfoByOpenid {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc="openid来源,必填")
                uint32_t openidFrom;

            @Field(desc = "openid值，必填")
                String openid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081809L", desc = "新增微信openid和统一用户id的绑定关系，即绑定辅助帐号")
    class AddOpenidWithWgUid {
        class Req {

            @Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli获取")
                uint32_t sceneId;	

            @Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc = "统一用户id，目前仅支持32位")
                uint64_t wgUid;												

            @Field(desc="微信appid，必填")
                String appid;

            @Field(desc="openid来源，必填")
                uint32_t openidFrom;

            @Field(desc = "openid，必填")
                String openid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }	

    @ApiProtocol(cmdid = "0x30081810L", desc = "移除微信openid和统一用户id的绑定关系，即解绑")
    class RemoveOpenidWithWgUid {
        class Req {

            @Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;	

            @Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc = "统一用户id，目前仅支持32位")
                uint64_t wgUid;	

            @Field(desc="openid来源，必填")
                uint32_t openidFrom;

            @Field(desc="微信appid，必填")
                String appid;

            @Field(desc = "openid值，必填")
                String openid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081811L", desc = "查询微信默认登陆方式.目前仅支持两种方式：直登和绑登.")
    class GetWechatDefaultLoginType {
        class Req {

            @Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc="微信oepnid，必填")
                String openid;

            @Field(desc="微信appid，必填")
                String appid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc="默认的登陆方式.返回值为1表示直登,2表示绑登")
                uint32_t defaultLoginType;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081812L", desc = "设置微信的默认登陆方式.目前仅支持两种方式：直登和绑登.")
    class SetWechatDefaultLoginType {
        class Req {

            @Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;	

            @Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc="微信openid，必填")
                String openid;

            @Field(desc="微信appid，必填")
                String appid;

            @Field(desc="默认的登陆方式.1表示直登,2表示绑登，必填")
                uint32_t defaultLoginType;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081813L", desc = "根据QQ号获取用户信息，如果用户不存在则自动导入")
    class GetUserInfoOrRegisterIfNotExistByQQ {
        class Req {
            @Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "接口调用授权码，必需，请联系joelwzli/silenchen获取")
                String authCode;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;	

            @Field(desc = "选项掩码，保留，默认填0即可")
                uint32_t option;

            @Field(desc = "用户QQ号，必需")
                uint64_t QQNumber;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081814L", desc = "根据绑定帐号获取用户信息.")
    class GetUserInfoByBindInfo {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "选项掩码，保留，默认填0即可")
                uint32_t option;

            @Field(desc = "绑定信息po")
                BindInfoPo bindInfo;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081815L", desc = "根据微信openid获取用户信息，没有会自动产生.")
    class GetUserInfoOrRegisterIfNotExistByOpenid {
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "选项掩码，默认填0即可")
                uint32_t option;

            @Field(desc="openid来源,必填")
                uint32_t openidFrom;

            @Field(desc = "openid值，必填")
                String openid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po")
                BuyerInfoPo buyerInfoPo;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }

    @ApiProtocol(cmdid = "0x30081816L", desc = "通过统一用户id批量查询用户信息")
    class BatchGetUserInfoByWgUid{
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "选项掩码，默认填0即可")
                uint32_t option;

            @Field(desc = "统一用户id集合，必填")
                Set<uint64_t> wgUidSet;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po集合")
                Map<uint64_t, BuyerInfoPo> buyerInfoPoMap;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }
    
    @ApiProtocol(cmdid = "0x30081817L", desc = "通过统一用户id查询用户QQ号。"
    		+ "如果WID对应的用户是非QQ用户，会返回3550错误。如果WID不存在，会返回3508错误")
    class GetUserQQNumberByWgUid{
        class Req {

            @Field(desc = "机器码，必需")
                String machineKey;

            @Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
                String source;

            @Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
                uint32_t sceneId;

            @Field(desc = "选项掩码，默认填0即可")
                uint32_t option;

            @Field(desc = "统一用户id集合，必填")
                uint64_t wgUid;

            @Field(desc = "输入保留字，无需设置，传空字符串即可")
                String inReserve;
        }

        class Resp {
            @Field(desc = "买家信息po集合")
                uint64_t buyerQQNumber;

            @Field(desc = "错误提示信息")
                String errmsg;

            @Field(desc = "输出保留字")
                String outReserve;
        }
    }
 
	@ApiProtocol(cmdid = "0x30081818L", desc = "用户登出，必须设置rcntlinfo中的OperatorUin（统一用户uid）和skey.注：只注销电商自生成私有skey,qqskey注销不使用该接口")
	class WanggouUserLogout {
		class Req {
			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli/wisdomlin获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/wisdomlin获取")
			String authCode;														
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0x30081819L", desc = "通过统一用户id更新用户信息")
	class UpdateUserInfoByWgUid {
		class Req {
			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，默认填0即可")
            uint32_t option;

            @Field(desc = "统一用户id，必填")
            uint64_t wgUid;

			@Field(desc = "用户信息po")
            BuyerInfoPo buyerInfoPo;	
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
}

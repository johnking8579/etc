 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserIcsonAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *
 *
 *@date 2015-03-27 09:28:06
 *
 *@since version:1
*/
public class  BindIcsonLoginAccountWithAlipayOpenidReq extends NetMessage
{
	/**
	 * 机器码，必需。
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源，必需, 请填调用方自己的源文件名称
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 *
	 * 版本 >= 0
	 */
	 private String authCode = new String();

	/**
	 * 支付宝用户openid，必填
	 *
	 * 版本 >= 0
	 */
	 private String AlipayOpenid = new String();

	/**
	 * 易迅个性化帐号，必填
	 *
	 * 版本 >= 0
	 */
	 private String IcsonLoginAccount = new String();

	/**
	 * 易迅个性化帐号登陆密码，必填,需做一次md5加密
	 *
	 * 版本 >= 0
	 */
	 private String IcsonLoginPassword = new String();

	/**
	 * 输入保留字，无需设置，传空字符串即可
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(authCode);
		bs.pushString(AlipayOpenid);
		bs.pushString(IcsonLoginAccount);
		bs.pushString(IcsonLoginPassword);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		authCode = bs.popString();
		AlipayOpenid = bs.popString();
		IcsonLoginAccount = bs.popString();
		IcsonLoginPassword = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xA0A91852L;
	}


	/**
	 * 获取机器码，必需。
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，必需。
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @return authCode value 类型为:String
	 * 
	 */
	public String getAuthCode()
	{
		return authCode;
	}


	/**
	 * 设置接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAuthCode(String value)
	{
		this.authCode = value;
	}


	/**
	 * 获取支付宝用户openid，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return AlipayOpenid value 类型为:String
	 * 
	 */
	public String getAlipayOpenid()
	{
		return AlipayOpenid;
	}


	/**
	 * 设置支付宝用户openid，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAlipayOpenid(String value)
	{
		this.AlipayOpenid = value;
	}


	/**
	 * 获取易迅个性化帐号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return IcsonLoginAccount value 类型为:String
	 * 
	 */
	public String getIcsonLoginAccount()
	{
		return IcsonLoginAccount;
	}


	/**
	 * 设置易迅个性化帐号，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIcsonLoginAccount(String value)
	{
		this.IcsonLoginAccount = value;
	}


	/**
	 * 获取易迅个性化帐号登陆密码，必填,需做一次md5加密
	 * 
	 * 此字段的版本 >= 0
	 * @return IcsonLoginPassword value 类型为:String
	 * 
	 */
	public String getIcsonLoginPassword()
	{
		return IcsonLoginPassword;
	}


	/**
	 * 设置易迅个性化帐号登陆密码，必填,需做一次md5加密
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIcsonLoginPassword(String value)
	{
		this.IcsonLoginPassword = value;
	}


	/**
	 * 获取输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(BindIcsonLoginAccountWithAlipayOpenidReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(authCode, null);  //计算字段authCode的长度 size_of(String)
				length += ByteStream.getObjectSize(AlipayOpenid, null);  //计算字段AlipayOpenid的长度 size_of(String)
				length += ByteStream.getObjectSize(IcsonLoginAccount, null);  //计算字段IcsonLoginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(IcsonLoginPassword, null);  //计算字段IcsonLoginPassword的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(BindIcsonLoginAccountWithAlipayOpenidReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(authCode, encoding);  //计算字段authCode的长度 size_of(String)
				length += ByteStream.getObjectSize(AlipayOpenid, encoding);  //计算字段AlipayOpenid的长度 size_of(String)
				length += ByteStream.getObjectSize(IcsonLoginAccount, encoding);  //计算字段IcsonLoginAccount的长度 size_of(String)
				length += ByteStream.getObjectSize(IcsonLoginPassword, encoding);  //计算字段IcsonLoginPassword的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

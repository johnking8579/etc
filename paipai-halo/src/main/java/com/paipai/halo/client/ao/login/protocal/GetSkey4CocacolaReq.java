 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.UserIcsonAo.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *
 *
 *@date 2015-03-27 09:28:06
 *
 *@since version:1
*/
public class  GetSkey4CocacolaReq extends NetMessage
{
	/**
	 * 机器码，必需。
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源，必需, 请填调用方自己的源文件名称
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 *
	 * 版本 >= 0
	 */
	 private String authCode = new String();

	/**
	 * 易迅用户id，目前仅支持32位,必填
	 *
	 * 版本 >= 0
	 */
	 private long icsonUid;

	/**
	 * Openid值,必填
	 *
	 * 版本 >= 0
	 */
	 private String openid = new String();

	/**
	 * appid,必填
	 *
	 * 版本 >= 0
	 */
	 private String appid = new String();

	/**
	 * accessToken,必填
	 *
	 * 版本 >= 0
	 */
	 private String accessToken = new String();

	/**
	 * 输入保留字，无需设置，传空字符串即可
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(authCode);
		bs.pushLong(icsonUid);
		bs.pushString(openid);
		bs.pushString(appid);
		bs.pushString(accessToken);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		authCode = bs.popString();
		icsonUid = bs.popLong();
		openid = bs.popString();
		appid = bs.popString();
		accessToken = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xA0A91851L;
	}


	/**
	 * 获取机器码，必需。
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，必需。
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @return authCode value 类型为:String
	 * 
	 */
	public String getAuthCode()
	{
		return authCode;
	}


	/**
	 * 设置接口调用授权码，必需，请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAuthCode(String value)
	{
		this.authCode = value;
	}


	/**
	 * 获取易迅用户id，目前仅支持32位,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid value 类型为:long
	 * 
	 */
	public long getIcsonUid()
	{
		return icsonUid;
	}


	/**
	 * 设置易迅用户id，目前仅支持32位,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIcsonUid(long value)
	{
		this.icsonUid = value;
	}


	/**
	 * 获取Openid值,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return openid;
	}


	/**
	 * 设置Openid值,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.openid = value;
	}


	/**
	 * 获取appid,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return appid value 类型为:String
	 * 
	 */
	public String getAppid()
	{
		return appid;
	}


	/**
	 * 设置appid,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAppid(String value)
	{
		this.appid = value;
	}


	/**
	 * 获取accessToken,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return accessToken value 类型为:String
	 * 
	 */
	public String getAccessToken()
	{
		return accessToken;
	}


	/**
	 * 设置accessToken,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAccessToken(String value)
	{
		this.accessToken = value;
	}


	/**
	 * 获取输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetSkey4CocacolaReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(authCode, null);  //计算字段authCode的长度 size_of(String)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(openid, null);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(appid, null);  //计算字段appid的长度 size_of(String)
				length += ByteStream.getObjectSize(accessToken, null);  //计算字段accessToken的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetSkey4CocacolaReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(authCode, encoding);  //计算字段authCode的长度 size_of(String)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(openid, encoding);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(appid, encoding);  //计算字段appid的长度 size_of(String)
				length += ByteStream.getObjectSize(accessToken, encoding);  //计算字段accessToken的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

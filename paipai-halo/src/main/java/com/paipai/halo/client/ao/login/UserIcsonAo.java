package com.paipai.halo.client.ao.login;

import java.util.BitSet;
import java.util.Map;
import java.util.Vector;
import java.util.Set;

import com.paipai.lang.uint8_t;
import com.paipai.lang.uint16_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

/**
 * ao_user_icson
 * 
 * @author stonenie,joelwzli
 * 
 * 接入帮助文档请见：http://tc-svn.tencent.com/paipai/paipai_b2b2c_rep/document_proj/trunk/设计文档/用户账户/帮助文档/电商统一用户接入帮助文档.doc
 * 
 */
@HeadApiProtocol(cPlusNamespace = "b2b2c::user::ao", needInit = true, needReset = true)
public class UserIcsonAo {
	@Member(desc = "BuyerLoginInfoPo", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = true, isNeedTempVersion=true)
	public class BuyerLoginInfoPo {
		@Field(desc = "版本号", defaultValue = "20131030")
		uint32_t version;
		
		@Field(desc = "易讯uid")
		uint64_t icsonUid;
		
		@Field(desc = "网购uid")
		uint64_t wgUid;
		
		@Field(desc = "QQ号码")
		uint64_t QQNumber;
		
		@Field(desc = "登陆时间")
		uint64_t loginTime;
		
		@Field(desc = "个性化账号")
		String loginAccount;
		
		@Field(desc = "加密串")
		String cryptogram;
		
		@Field(desc = "visitkey")
		String visitkey;
		
		@Field(desc = "登陆IP")
		String loginIp;
		
		@Field(desc = "reserve string")
		String reserveStr;
		
		uint8_t version_u;
		uint8_t icsonUid_u;
		uint8_t wgUid_u;
		uint8_t QQNumber_u;
		uint8_t loginTime_u;
		uint8_t loginAccount_u;
		uint8_t cryptogram_u;
		uint8_t visitkey_u;
		uint8_t loginIp_u;
		uint8_t reserveStr_u;
		
		@Field(desc="登录类型，0-login登录，1-logout注销,默认是0.",defaultValue = "0", version=20131030)
		uint8_t logType;
		
		@Field(version=20131030)
		uint8_t logType_u;
		
	}
	@Member(desc = "买家用户信息Po", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = true, isNeedTempVersion=true)
	public class BuyerInfoPo {
		@Field(desc = "版本号", defaultValue = "20141229")
		uint32_t version;

		@Field(desc = "网购用户内部id，非QQ号，目前仅支持32位")
		uint64_t wgUid;

		@Field(desc = "易迅用户内部id，目前仅支持32位")
		uint64_t icsonUid;

		@Field(desc = "绑定的用户QQ号，目前仅支持32位")
		uint64_t qQNumber;

		@Field(desc = "用户帐号名，包括易迅注册帐号、非qq帐号的第三方联合登录，如LoginAlipay+openid等字符串帐户名")
		String loginAccount;
				
		@Field(desc = "用户帐号类型，1-QQ号 2-个性化帐号，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE")
		uint8_t accountType;									

		@Field(desc = "真实姓名")
		String truename;
		
		@Field(desc = "昵称")
		String nickname;

		@Field(desc = "用户属性位BitSet，属性位代表的意义请参见b2b2c_define.h中的USER_PROPERTY", bitset=128)
		BitSet bitProperty;				

		@Field(desc = "用户属性级别，如彩钻级别，Map中第一个uint32_t表示用户属性位值，第二个uint32_t表示级别值")
		Map<uint32_t,uint32_t> userFlagLevel;	
				
		@Field(desc = "性别, 0-不明 1-男 2-女 参见user_comm_define.h中的E_USER_SEX")
		uint8_t sex;

		@Field(desc = "年龄")
		uint8_t age;

		@Field(desc = "手机号，如绑定手机的用户属性位为1，则表示绑定的手机号")
		String mobile;
		
		@Field(desc = "电子邮箱，如绑定邮箱的用户属性位为1，则表示绑定的邮箱")
		String email;		

		@Field(desc = "固定电话")
		String phone;

		@Field(desc = "传真")
		String fax;
		
		@Field(desc = "用户所在地区id")
		uint32_t region;
				
		@Field(desc = "邮政编码")
		String postcode;
		
		@Field(desc = "用户详细住址")
		String address;

		@Field(desc = "身份证件类型，1-身份证 参见user_comm_define.h中E_USER_IDTYPE")
		uint8_t identityType;
		
		@Field(desc = "身份证件号码")
		String identityNum;
		
		@Field(desc = "用户信用值")
		int buyerCredit;						

		@Field(desc = "用户经验值")
		uint32_t experience ;

		@Field(desc = "财付通账号")
		String cftAccount;		

		@Field(desc = "登录级别，预留")
		uint8_t loginLevel;

		@Field(desc = "用户类型，从易迅导入，参见user_comm_define.h中E_USER_TYPE")
		uint8_t userType;

		@Field(desc = "经销商等级，从易迅导入")
		uint8_t retailerLevel;

		@Field(desc = "易迅会员等级，从易迅导入")
		uint8_t icsonMemberLevel;

		@Field(desc = "最后修改时间")
		uint32_t lastUpdateTime;
																				
		@Field(desc = "用户注册时间")
		uint32_t regTime;												

		@Field(desc = "reserve string")
		String reserveStr;

		@Field(desc = "reserve int")
		uint32_t reserveInt;
												
        uint8_t version_u;
        uint8_t wgUid_u;
        uint8_t icsonUid_u;
        uint8_t qQNumber_u;
        uint8_t loginAccount_u;        
        uint8_t accountType_u;
        uint8_t truename_u;        		
        uint8_t nickname_u;
        uint8_t bitProperty_u;				
        uint8_t userFlagLevel_u;	
        uint8_t sex_u;
        uint8_t age_u;
        uint8_t mobile_u;
        uint8_t email_u;		
        uint8_t phone_u;
        uint8_t fax_u;
        uint8_t region_u;
        uint8_t postcode_u;
        uint8_t address_u;
        uint8_t identityType_u;
        uint8_t identityNum_u;
        uint8_t buyerCredit_u;		
        uint8_t experience_u;
        uint8_t cftAccount_u;		
        uint8_t loginLevel_u;
        uint8_t userType_u;
        uint8_t retailerLevel_u;
        uint8_t icsonMemberLevel_u;
        uint8_t lastUpdateTime_u;        
        uint8_t regTime_u;				
        uint8_t reserveStr_u;
        uint8_t reserveInt_u;		
        
		@Field(desc = "易迅openid", version=20130314)
		String weChatId;
		@Field(version=20130314)
		uint8_t weChatId_u;
		@Field(desc = "虚拟经验值", version=20130429)
		uint32_t virtualExpPoints;
		@Field(version=20130429)
		uint8_t virtualExpPoints_u;
		
		@Field(desc= "登录密码安全级别", version=20130529)
		uint8_t passwordSecureLevel;
		@Field(version=20130529)
		uint8_t passwordSecureLevel_u;
        
        		
		@Field(desc= "不同来源用户注册时间(微购等), key为来源，value为注册时间。易迅用户注册时间请参考另一个字段regTime", version=20130918)
		Map<uint32_t, uint32_t> mapDiffSrcRegTime;
		@Field(version=20130918)
		uint8_t mapDiffSrcRegTime_u;
		
		@Field(desc="网购openid",version=20131220)
		String wanggouOpenid;
		@Field(version=20131220)
		uint8_t wanggouOpenid_u;

		//add by p_jdningli@20141229 拍拍用户和统一用户接口打通需求
		@Field(desc="卖家总信用",version=20141229)
		int  sellerCredit;
		@Field(version=20141229)
		uint8_t  sellerCredit_u;

		@Field(desc="认证信息",version=20141229)
		uint8_t  authMask;
		@Field(version=20141229)
		uint8_t  authMask_u;

		@Field(desc="用户属性",version=20141229)
		Map<uint16_t,uint8_t>  mapProp;
		@Field(version=20141229)
		uint8_t  mapProp_u;

		@Field(desc="留言标志位",version=20141229)
		uint16_t  recvMsgMask;
		@Field(version=20141229)
		uint8_t  recvMsgMask_u;
		
		@Field(desc="用户信息标志位",version=20141229)
		uint32_t  userInfoMask;
		@Field(version=20141229)
		uint8_t  userInfoMask_u;

		@Field(desc="省份",version=20141229)
		uint8_t  province;
		@Field(version=20141229)
		uint8_t  province_u;

		@Field(desc="城市",version=20141229)
		uint16_t  city;
		@Field(version=20141229)
		uint8_t  city_u;

		@Field(desc="城市",version=20141229)
		uint32_t  country;
		@Field(version=20141229)
		uint8_t  country_u;

		@Field(desc="肖像图片",version=20141229)
		String  mainLogoPos;
		@Field(version=20141229)
		uint8_t  mainLogoPos_u;

		@Field(desc="联系方式",version=20141229)
		uint8_t  commContactType;
		@Field(version=20141229)
		uint8_t  commContactType_u;

		@Field(desc="最后登录IP",version=20141229)
		uint32_t  lastLoginIp;
		@Field(version=20141229)
		uint8_t  lastLoginIp_u;

		@Field(desc="最后登录时间",version=20141229)
		uint32_t  lastLoginTime;
		@Field(version=20141229)
		uint8_t  lastLoginTime_u;

		@Field(desc="登录类型",version=20141229)
		uint8_t  lastLoginType;
		@Field(version=20141229)
		uint8_t  lastLoginType_u;

		@Field(desc="参考信用",version=20141229)
		String  referCredit;
		@Field(version=20141229)
		uint8_t  referCredit_u;

		@Field(desc="卖家虚拟信用",version=20141229)
		int  virtualCredit;
		@Field(version=20141229)
		uint8_t  virtualCredit_u;

		@Field(desc="卖家实物信用",version=20141229)
		int  objectCredit;
		@Field(version=20141229)
		uint8_t  objectCredit_u;

		@Field(desc="卖家联系方式",version=20141229)
		String  contacts;
		@Field(version=20141229)
		uint8_t  contacts_u;

		@Field(desc="多工号权限系统员工所属的卖家号",version=20141229)
		uint32_t sellerUin;
		@Field(version=20141229)
		uint8_t  sellerUin_u;

		@Field(desc="店铺类目id[用作店铺类型]",version=20141229)
		uint32_t shopClassId;
		@Field(version=20141229)
		uint8_t shopClassId_u;
	}

	@Member(desc = "登录信息Po", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = true)
	public class LoginInfoPo {
		@Field(desc = "版本号", defaultValue = "20131115")
		uint32_t version;

		@Field(desc = "登录来源，1-易迅 2-网购 3-拍拍 4-易迅无线 5-易迅无线sid 6-易迅个性化注册 7-微信登录 参见user_comm_define.h中的E_LOGIN_FROM_TYPE")
		uint32_t loginFrom;

		@Field(desc = "登录强度类型，0--默认，普通登录（skey有效期50分钟） 1--网站弱登录（skey有效期10天），2--无线弱登录（skey有效期30天），参见user_comm_define.h中的E_LOGIN_INTENSITY_TYPE")
		uint32_t loginIntensityType;
		
		@Field(desc = "用户登录帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber和QQSkey必填) 2-个性化帐号(填2则loginAccount和passwd必填) 3-openid(填3则openid和openidFrom必填) 参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE")
		uint32_t accountType;
					
		@Field(desc = "用户QQ号，accountType填1时必填，目前仅支持32位")
		uint64_t QQNumber;									

		@Field(desc = "QQ号用户的skey，accountType填1时必填；loginIntensityType填2时，填lskey；loginFrom填5时表示无线sid登录，填sid")
		String QQSkey;	
		
		@Field(desc = "个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填")
		String loginAccount;
																
		@Field(desc = "个性化帐号登录密码，accountType填2时必填， accountType类型为微信openid时，填微信token")
		String passwd;	
		
		@Field(desc = "Openid值， accountType填3时必填")
		String openid;
		
		@Field(desc = "openid来源，1-支付宝openid  2-返利openid 4-微信openid 参见user_comm_define.h中的E_LOGIN_OPENID_FROM")
		uint32_t openidFrom;
						
		@Field(desc = "reserve int")
		uint32_t reserveInt;
		
		@Field(desc = "reserve string")
		String reserveStr;
								
        uint8_t version_u;					
        uint8_t loginFrom_u;
        uint8_t loginIntensityType_u;
        uint8_t accountType_u;        
        uint8_t QQNumber_u;	
        uint8_t QQSkey_u;
        uint8_t loginAccount_u;					
        uint8_t passwd_u;
        uint8_t openid_u;        
        uint8_t openidFrom_u;        
        uint8_t reserveInt_u;
        uint8_t reserveStr_u;	
        
        @Field(desc = "appid, 微信openid或微信tiket登录, 必填", version=20130814)
        String appid;
        
        @Field(version=20130814)
        uint8_t appid_u;
        
        @Field(desc = "鉴权方式.1---微信ticket鉴权 2--- 微信accessToken鉴权", version=20131115)
        uint32_t AuthType;
        
        @Field(version=20131115)
        uint8_t AuthType_u;
	}

	@Member(desc = "PointsAndLevelPo", cPlusNamespace = "b2b2c::user::po", isNeedUFlag = true,isNeedTempVersion=true)
	public class PointsAndLevelPo {
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		@Field(desc = "易讯uid,必填")
		uint64_t  icsonUid;
		@Field(desc = "用户经验值")
		uint32_t experience;
		@Field(desc = "易迅会员等级")
		uint32_t icsonMemberLevel;	
		@Field(desc = "易讯虚拟经验值")
		uint32_t virtualExp;
		@Field(desc = "reserve string")
		String reserveStr;
        uint8_t version_u;					
        uint8_t icsonUid_u;
        uint8_t experience_u;
        uint8_t icsonMemberLevel_u;        
        uint8_t virtualExp_u;	
        uint8_t reserveStr_u;		
	}
		
	@ApiProtocol(cmdid = "0xA0A91801L", desc = "根据易迅用户id获取用户信息")
	class GetUserInfoByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91801L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，0-取oidb属性失败也返回0；1-取oidb属性失败返回错误码，参见user_comm_define.h中的E_GET_USERINFO_SCENE")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，0--默认值，不取用户oidb属性位，1--额外取用户oidb属性位(仅针对QQ用户)，具体参见user_comm_define.h中的E_GETUSER_OPTION")
			uint32_t option;
			
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;									

			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98801L", desc = "resp")
		class Resp {
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0A91802L", desc = "根据QQ号或个性化帐号获取用户信息，个性化帐号指易迅注册帐号、非qq帐号的第三方联合登录等字符串帐号（但不包括绑定邮箱、绑定手机号、LoginQQ+openid等）")
	class GetUserInfoByQQorAccount {
		@ApiProtocol(cmdid = "0xA0A91802L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，保留，默认填0即可")
			uint32_t option;

			@Field(desc = "用户帐号类型，必需，0-无效值  1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)")
			uint8_t accountType;
						
			@Field(desc = "用户QQ号，accountType填1时必填，目前仅支持32位")
			uint64_t qQNumber;									

			@Field(desc = "个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填")
			String loginAccount;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98802L", desc = "resp")
		class Resp {
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0A91803L", desc = "根据绑定帐号名（目前支持email或手机号）获取用户信息")
	class GetUserInfoByBindInfo {
		@ApiProtocol(cmdid = "0xA0A91803L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，保留，默认填0即可")
			uint32_t option;

			@Field(desc = "绑定帐号类型，必需，0-无效值 1-email(填1则BindInfo填email地址) 2-手机号(填2则BindInfo填手机号)，3-微信号(填3则BindInfo填微信号)")
			uint8_t BindInfoType;
						
			@Field(desc = "绑定帐号名，目前支持email,手机号,微信号")
			String BindInfo;									

			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98803L", desc = "resp")
		class Resp {
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0A91804L", desc = "查询绑定帐号名（目前支持email或手机号）是否已被绑定")
	class IsBindInfoBinded {
		@ApiProtocol(cmdid = "0xA0A91804L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	
			
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "绑定帐号类型，必需，0-无效值 1-email(填1则BindInfo填email地址) 2-手机号(填2则BindInfo填手机号) 3-微信号(填3则BindInfo填微信号)")
			uint8_t BindInfoType;
						
			@Field(desc = "绑定帐号名（目前支持email，手机号,微信号）")
			String BindInfo;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98804L", desc = "resp")
		class Resp {

			@Field(desc = "结果值，0-未绑定 1-已绑定")
			uint8_t retValue;
									
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91805L", desc = "查询QQ号或个性帐号名是否已被注册")
	class IsQQorAccountRegistered {
		@ApiProtocol(cmdid = "0xA0A91805L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;					
											
			@Field(desc = "用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)")
			uint8_t accountType;
						
			@Field(desc = "用户QQ号，accountType填1时必填，目前仅支持32位")
			uint64_t qQNumber;									

			@Field(desc = "个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填")
			String loginAccount;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98805L", desc = "resp")
		class Resp {

			@Field(desc = "结果值，0-未注册 1-已注册")
			uint8_t retValue;
									
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}		
	
	@ApiProtocol(cmdid = "0xA0A91806L", desc = "根据易迅用户id获取网购用户id")
	class GetWgUidByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91806L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	
			
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;									

			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98806L", desc = "resp")
		class Resp {
			@Field(desc = "网购用户id，目前仅支持32位")
			uint64_t wgUid;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
				
	@ApiProtocol(cmdid = "0xA0A91821L", desc = "修改用户基本信息（不能修改登录密码、经验值、易迅会员等级等）")
	class ModifyBasicUserInfoByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91821L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;

			@Field(desc = "鉴权码，必需，具体请联系stonenie/silenchen获取")
			String authCode;				
			
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98821L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xA0A91822L", desc = "修改用户经验值")
	class ModifyExperienceByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91822L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "用户经验值")
			uint32_t experience;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98822L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91823L", desc = "修改用户的易迅会员等级")
	class ModifyIcsonMemberLevelByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91823L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	
			
			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "易迅会员等级")
			uint32_t icsonMemberLevel;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98823L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}		
	
	@ApiProtocol(cmdid = "0xA0A91824L", desc = "修改个性化帐号的登录密码")
	class ModifyAccountPasswdByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91824L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;
				
			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "个性化帐号的老登录密码")
			String oldPasswd;

			@Field(desc = "个性化帐号的新登录密码")
			String newPasswd;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98824L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91825L", desc = "重置个性化帐号的登录密码")
	class ResetAccountPasswdByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91825L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;												

			@Field(desc = "重置后的登录密码")
			String initPasswd;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98825L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91826L", desc = "新增邮箱/手机号/微信号和易迅用户id的绑定关系，即绑定辅助帐号")
	class AddBindInfoWithIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91826L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
											
			@Field(desc = "绑定帐号类型，必需，0-无效值 1-email(填1则BindInfo填email地址) 2-手机号(填2则BindInfo填手机号)，3-微信号(填3则BindInfo填微信号)")
			uint8_t BindInfoType;
						
			@Field(desc = "绑定帐号名（目前支持email，手机号,微信号）")
			String BindInfo;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98826L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}	
	
	@ApiProtocol(cmdid = "0xA0A91827L", desc = "移除绑定帐号名（目前支持email或手机号）和易迅用户id的绑定关系，即解绑")
	class RemoveBindInfoWithIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91827L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
						
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;	
														
			@Field(desc = "绑定帐号类型，必需，0-无效值 1-email(填1则BindInfo填email地址) 2-手机号(填2则BindInfo填手机号)，3-微信号(填3则BindInfo填微信号)")
			uint8_t BindInfoType;
						
			@Field(desc = "绑定帐号名（目前支持email或手机号,微信号）")
			String BindInfo;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98827L", desc = "resp")
		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
		
	@ApiProtocol(cmdid = "0xA0A91828L", desc = "QQ用户或个性化帐号（字符串帐号）用户注册，不支持LoginQQ+openid的注册。已废弃，请使用统一登录接口替代")
	class UserRegister {
		@ApiProtocol(cmdid = "0xA0A91828L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
			
			@Field(desc = "用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)，参见user_comm_define.h中的E_ICSON_USER_ACCOUNT_TYPE")
			uint8_t accountType;
						
			@Field(desc = "用户QQ号，accountType填1时必填，目前仅支持32位")
			uint64_t qQNumber;									

			@Field(desc = "个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填")
			String loginAccount;
														
			@Field(desc = "个性化帐号登录密码")
			String passwd;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98828L", desc = "resp")
		class Resp {

			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;
												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	
	@ApiProtocol(cmdid = "0xA0A91841L", desc = "QQ用户或个性化帐号用户登录，对QQ用户，必须设置rcntlinfo中的skey；对个性化帐号用户，必须设置Passwd。不支持LoginQQ+openid帐号，" +
			"待废弃，新的登录接入已经不再推荐使用该接口，请使用IcsonUniformLogin接口替代")
	class IcsonUserLogin {
		@ApiProtocol(cmdid = "0xA0A91841L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;
														
			@Field(desc = "用户帐号类型，必需，0-无效值 1-QQ号(填1则QQNumber必填) 2-个性化帐号(填2则loginAccount必填)")
			uint8_t accountType;
						
			@Field(desc = "用户QQ号，accountType填1时必填，目前仅支持32位")
			uint64_t qQNumber;									

			@Field(desc = "个性登录帐号（如易迅注册帐号、LoginXXX+Openid等），accountType填2时必填")
			String loginAccount;
														
			@Field(desc = "个性化帐号登录密码，accountType填2时必填")
			String passwd;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98841L", desc = "resp")
		class Resp {

			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;
												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91842L", desc = "用户验登录，请务必在rcntlinfo中设置易迅用户ID和skey， skey请取cookie里面的wg_skey")
	class CheckLoginByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91842L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;												
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98842L", desc = "resp")
		class Resp {
												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91843L", desc = "获取用户skey，对QQ用户请将skey设置在rcntlinfo中的OperatorKey；对个性化帐号用户则请设置为空")
	class GetSkey {
		@ApiProtocol(cmdid = "0xA0A91843L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	
											
			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;

			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98843L", desc = "resp")
		class Resp {

			@Field(desc = "用户skey")
			String skey;
												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91844L", desc = "用户登出，必须设置rcntlinfo中的OperatorUin（易迅用户uid）和skey")
	class IcsonUserLogout {
		@ApiProtocol(cmdid = "0xA0A91844L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;														
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98844L", desc = "resp")
		class Resp {
												
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}	

	@ApiProtocol(cmdid = "0xA0A91845L", desc = "易迅用户统一登录")
	class IcsonUniformLogin {
		@ApiProtocol(cmdid = "0xA0A91845L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;

			@Field(desc = "登录信息po, 详细请看Po类型定义")
			LoginInfoPo loginInfoPo;																
						
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98845L", desc = "resp")
		class Resp {

			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;

			@Field(desc = "sessionKey，当accountType填1时输出为空")
			String skey;

			@Field(desc = "用户昵称，当accountType填1时取的是oidb昵称")
			String nickname;
																		
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	@ApiProtocol(cmdid = "0xA0A91846L", desc = "更新用户的经验值,易讯等级和虚拟经验值（会同步信息给易迅ERP）")
	class ModifyExpLevVirExpByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91846L", desc = "req")
		class Req {
			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;
			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	
			@Field(desc = "鉴权码，必需，具体请联系joelwzli/silenchen获取")
			String authCode;	
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;
			@Field(desc = "更新po")
			PointsAndLevelPo pointsAndLevelPo;
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}
		@ApiProtocol(cmdid = "0xA0A98846L", desc = "resp")
		class Resp {
			@Field(desc = "错误提示信息")
			String errmsg;
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}	
	
	@ApiProtocol(cmdid = "0xA0A91847L", desc = "获取易迅用户上次登录时间")
	class GetLastLoginTimeByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91847L", desc = "req")
		class Req {
			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;
			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;
			@Field(desc = "易迅用户id，目前仅支持32位")
			uint64_t icsonUid;
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}
		@ApiProtocol(cmdid = "0xA0A98847L", desc = "resp")
		class Resp {
			@Field(desc="上次登录时间")
			uint32_t lastLoginTime;
			@Field(desc = "错误提示信息")
			String errmsg;
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}	
	
	@ApiProtocol(cmdid = "0xA0A91848L", desc = "批量根据易迅用户id获取用户信息")
	class BatchGetUserInfoByIcsonUid {
		@ApiProtocol(cmdid = "0xA0A91848L", desc = "req")
		class Req {

			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;

			@Field(desc = "场景id，默认填0, 请联系joelwzli获取")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，默认填0")
			uint32_t option;
			
			@Field(desc = "要查询的易迅用户id集合，最多不超过10个元素")
			Set<uint64_t> icsonUid;

			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xA0A98848L", desc = "resp")
		class Resp {
			@Field(desc = "易迅用户信息po映射表，key为易迅用户ID，value为对应的易迅用户信息")
			Map<uint64_t, BuyerInfoPo> buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91849L", desc = "根据QQ号获取用户信息，如果用户不存在则自动导入")
	class GetUserInfoByQQ {
		class Req {
			@Field(desc = "机器码，必需，请取cookie里面的visitkey，无法获得visitkey的可以填随机字符串")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			
			@Field(desc = "接口调用授权码，必需，请联系joelwzli/silenchen获取")
			String authCode;

			@Field(desc = "场景id，保留，默认填0即可")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，保留，默认填0即可")
			uint32_t option;
						
			@Field(desc = "用户QQ号，必需")
			uint64_t QQNumber;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91850L", desc = "根据微信openid获取用户信息。如果不存在，则注册一个新用户。")
	class GetOrRegisterUserInfoByWechatOpenid {
		class Req {
			@Field(desc = "机器码，必需。")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			
			@Field(desc = "接口调用授权码，必需，请联系joelwzli/silenchen获取")
			String authCode;

			@Field(desc = "场景id，保留，默认填0即可")
			uint32_t sceneId;	

			@Field(desc = "选项掩码，保留，默认填0即可")
			uint32_t option;
						
			@Field(desc = "微信openid")
			String openid;
			
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {
			@Field(desc = "买家信息po")
			BuyerInfoPo buyerInfoPo;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
	@ApiProtocol(cmdid = "0xA0A91851L", desc = "获取电商私有skey.用于可口可乐微信号项目.临时接口，仅提供3个月的使用时间.")
	class GetSkey4Cocacola {
		class Req {
			@Field(desc = "机器码，必需。")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			
			@Field(desc = "接口调用授权码，必需，请联系joelwzli/wisdomlin获取")
			String authCode;

			@Field(desc = "易迅用户id，目前仅支持32位,必填")
			uint64_t icsonUid;	
			
			@Field(desc = "Openid值,必填")
			String openid;
			
		    @Field(desc = "appid,必填")
		    String appid;
		       
		    @Field(desc = "accessToken,必填")
		    String accessToken;
		       
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {
			@Field(desc = "sessionKey")
			String skey;
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}	
	
	@ApiProtocol(cmdid = "0xA0A91852L", desc = "将支付宝帐号和一个易迅个性化帐号绑定。绑定成功，返回该帐号的用户信息。")
	class BindIcsonLoginAccountWithAlipayOpenid {
		class Req {
			@Field(desc = "机器码，必需。")
			String machineKey;

			@Field(desc = "调用来源，必需, 请填调用方自己的源文件名称")
			String source;
			
			@Field(desc = "接口调用授权码，必需，请联系joelwzli/wisdomlin获取")
			String authCode;

			@Field(desc = "支付宝用户openid，必填")
			String AlipayOpenid;
			
			@Field(desc = "易迅个性化帐号，必填")
			String IcsonLoginAccount;
		       
			@Field(desc = "易迅个性化帐号登陆密码，必填,需做一次md5加密")
			String IcsonLoginPassword;
					       
			@Field(desc = "输入保留字，无需设置，传空字符串即可")
			String inReserve;
		}

		class Resp {
						
			@Field(desc = "错误提示信息")
			String errmsg;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
	
}

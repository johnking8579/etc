package com.paipai.halo.client.log;

import java.util.List;

/**
 * 日志分析用, 用来说明日志项应当记录哪些内容
 * @author JingYing 2014-10-13
 *
 */
public interface LogItem {
	public static final String 
		PROTOCOL_AO服务 		= 	"ao",
		PROTOCOL_BOSS服务 	= 	"boss",
		PROTOCOL_UDP 		= 	"udp",
		PROTOCOL_HTTP 		= 	"http";
	
	/**
	 * 服务的通讯协议名,取值见PROTOCOL_*
	 */
	String getProtocol();
	
	/**
	 * 服务的IP地址或URL
	 * @return
	 */
	String getServiceUrl();
	
	/**
	 * 调用接口时是否出现异常
	 * @return
	 */
	boolean hasError();
	
	/**
	 * 接口响应时长, 毫秒
	 * @return
	 */
	long getCostMillis();
	
	/**
	 * 方法栈轨迹
	 * @return
	 */
	StackTraceElement[] getInvokeStack();
	
	/**
	 * 日志条目的文本展现形式,用于人工查看
	 * @return
	 */
	String toText();
	
	/**
	 * 子类特有的字段列表,注意顺序,将来要写到分析日志中,顺序不能随意变
	 * @return
	 */
	List<String> getParticularProp();
	
	/**
	 * 获得请求ID
	 * @return
	 */
	String getRequestId();
	
	/**
	 * 用户的账号
	 * @return
	 */
	long getWid();
	
}

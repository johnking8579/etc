package com.paipai.halo.client.http;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * HTTP响应头
 * @author JingYing
 * @date 2015年4月14日
 */
public class RespHead {
	private int contentLength = -1;
	private String contentType;
	private String contentEncoding;
	private long expiration;
	private long date;
	private long lastModified;
	
	public static RespHead parse(Map<String, List<String>> header)	{
		Map<String, List<String>> change = new HashMap<String, List<String>>();
		for(Entry<String, List<String>> e : header.entrySet())	{
			String key = e.getKey()==null ? null : e.getKey().toLowerCase(); 
			change.put(key, e.getValue());
		}
		
		RespHead r = new RespHead();
		String contentLength = getHeaderField(change, "content-length".toLowerCase());
		try {
			r.contentLength = Integer.parseInt(contentLength);
		} catch (NumberFormatException e) {
		}
		
		r.contentType = getHeaderField(change, "content-type");
		r.contentEncoding = getHeaderField(change, "content-encoding".toLowerCase());
		
		String expires  = getHeaderField(change, "expires".toLowerCase());
		try {
			r.expiration = Date.parse(expires);
		} catch (Exception e) {
		}
		
		String date = getHeaderField(change, "date".toLowerCase());
		try {
			r.date = Date.parse(date);
		} catch (Exception e) {
		}
		
		String lastModified = getHeaderField(change, "last-modified".toLowerCase());
		try {
			r.lastModified = Date.parse(lastModified);
		} catch (Exception e) {
		}
		
		return r;
	}
	
	private static String getHeaderField(Map<String, List<String>> header, String key)	{
		List<String> val = header.get(key);
		if(val == null || val.isEmpty())
			return null;
		else
			return val.get(0);
	}

	public int getContentLength() {
		return contentLength;
	}

	public String getContentType() {
		return contentType;
	}

	public String getContentEncoding() {
		return contentEncoding;
	}

	public long getExpiration() {
		return expiration;
	}

	public long getDate() {
		return date;
	}

	public long getLastModified() {
		return lastModified;
	}
}

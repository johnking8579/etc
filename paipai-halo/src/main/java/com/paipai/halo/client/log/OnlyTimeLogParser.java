package com.paipai.halo.client.log;

/**
 * 默认的空实现,
 * 一般情况下, invoke-outline记录url/请求耗时, invoke-detail记录请求url/耗时/请求体/响应体
 * 使用这个默认实现时, invoke-outline和invoke-detail都只有url/请求耗时.
 * @author JingYing
 * @date 2015年4月15日
 */
public class OnlyTimeLogParser implements PacketLogParser {

	@Override
	public String getServiceName() {
		return null;
	}

	@Override
	public String parseRequest(byte[] request) throws Exception {
		return null;
	}

	@Override
	public String parseResponse(byte[] response) throws Exception {
		return null;
	}

}

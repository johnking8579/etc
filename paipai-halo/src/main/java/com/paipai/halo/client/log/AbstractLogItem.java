package com.paipai.halo.client.log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.halo.common.Util;

/**
 * LogItem的抽象实现类, 给LogItem子类加入记录的功能
 * @author JingYing 2014-10-13
 *
 */
public abstract class AbstractLogItem implements LogItem {

	public static final Logger 
			log = LogManager.getLogger(),
			log2 = LogManager.getLogger(AbstractLogItem.class.getName()+".invoke-outline");
	
	public static final String SPLITTER = "\t";
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	
	/**
	 * 哪些字符开头的方法需要打印出来
	 */
	public static final String[] WHITELIST = new String[]{
		"com.paipai.", "com.jd."
	};
	

	/**
	 * 记录日志到本地磁盘或其它地方. 注意性能问题
	 */
	public void log() {
		log2.debug(toAnalysisStr());
		if (hasError()) {
			log.error(toText());
		} else {
			log.debug(toText());
		}
	}
	
	//日期 协议 服务地址 requestId wid 花费时间 是否出错 其它
	private String toAnalysisStr()	{
		List<String> l = new ArrayList<String>();
		l.add(sdf.format(new Date()));
		l.add(getProtocol());
		l.add(getServiceUrl());
		l.add(getRequestId());
		l.add(getWid()+"");
		l.add(getCostMillis()+"");
		l.add(hasError() ? "1" : "0");
		if(!getParticularProp().isEmpty())	{
			l.addAll(getParticularProp());
		}
		return Util.join(l, SPLITTER, "$$");
	}
	
	protected List<String> filterInvokeStack()	{
		List<String> list = new ArrayList<String>();
		if(getInvokeStack() != null)	{
			for(StackTraceElement ele : getInvokeStack())	{
				for(String s : WHITELIST)	{
					if(ele.toString().startsWith(s))	{
						list.add(ele.toString());
					}
				}
			}
		}
		if(list.size() >= 2)	{	
			list.remove(0);		//移除最前面2个,和动态代理相关的栈
			list.remove(0);
		}
		return list;
	}
	
	@Override
	public String getRequestId()	{
		return ReqInfoContainer.getRequestId();
	}
	
	@Override
	public long getWid()	{
		return ReqInfoContainer.getWid();
	}
}

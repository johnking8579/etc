package com.paipai.halo.client.ao.login;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.component.configagent.Configs;
import com.paipai.halo.client.ao.WebStubFactory;
import com.paipai.halo.client.ao.login.protocal.ApiGetUserLoginLevelReq;
import com.paipai.halo.client.ao.login.protocal.ApiGetUserLoginLevelResp;
import com.paipai.halo.client.ao.login.protocal.CheckLoginByWgUidReq;
import com.paipai.halo.client.ao.login.protocal.CheckLoginByWgUidResp;
import com.paipai.halo.common.Util;
import com.paipai.util.net.NetUtils;

public class LoginClient {
	/**
	 * 运行日志
	 */
	public static Logger run = LogManager.getLogger("run");

	public static long getUserSimpleProfileinf(String mk, long userid,
			String sKey) throws AsynWebStubException {
		run.info("userid=" + userid + ",sKey=" + sKey + ",mk =" + mk);
		AsynWebStub webStub = WebStubFactory.getWebStub4PaiPai();
		webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);

		webStub.setUin(userid);
		webStub.setOperator(userid);
		webStub.setSkeyToExtHead(sKey);
		webStub.setRouteKey(userid);
		webStub.setDomainId(126);

		long longinlevel = 0;
		ApiGetUserLoginLevelReq req = new ApiGetUserLoginLevelReq();
		req.setUin(userid);
		req.setMachineKey("fispweb");
		req.setSource("fispweb");
		req.setScene(0);

		ApiGetUserLoginLevelResp resp = new ApiGetUserLoginLevelResp();
		try {
			webStub.invoke(req, resp);
			longinlevel = resp.getLoginLevel();
			run.info("getUserSimpleProfileinf:ip=" + webStub.getIp() + ",port="
					+ webStub.getPort());
			run.info(new Gson().toJson(resp));
		} catch (AsynWebStubException e) {
			e.printStackTrace();
			return 0 ;
		}
		return longinlevel;
	}

	/**
	 * 
	 * @param mk
	 *            机器码，必需
	 * @param authCode
	 *            鉴权码，必需，具体请联系joelwzli/silenchen获取
	 * @param loginInfoPo
	 *            登录信息po, 【注】当前只支持QQ号、微信openid登录
	 * @return
	 */
	public static CheckLoginByWgUidResp checkLoginByWgUid(String mk, Long uin,
			String sk) {
		run.info("uin=" + uin + ",sk=" + sk + ",mk =" + mk);
		CheckLoginByWgUidReq req = new CheckLoginByWgUidReq();
		CheckLoginByWgUidResp resp = new CheckLoginByWgUidResp();
		req.setMachineKey(mk);
		req.setSource("o2o");
		req.setSceneId(0l);
		int ret = 0;
		AsynWebStub webStub = WebStubFactory.getWebStub4PaiPaiGBK();
		webStub.setUin(uin);
		webStub.setOperator(uin);
		webStub.setSkeyToExtHead(sk);
		webStub.setRouteKey(uin);
		try {
			ret = webStub.invoke(req, resp);
			run.info("checkLoginByWgUid:ip=" + webStub.getIp() + ",port="
					+ webStub.getPort());
			run.info(new Gson().toJson(resp));
		} catch (Exception e) {
			run.warn("e", e);
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}

}

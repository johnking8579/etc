//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.halo.client.login.ModifyExpLevVirExpByIcsonUidReq.java

package com.paipai.halo.client.ao.login.protocal;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *PointsAndLevelPo
 *
 *@date 2015-03-27 09:28:05
 *
 *@since version:0
*/
public class PointsAndLevelPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 易讯uid,必填
	 *
	 * 版本 >= 0
	 */
	 private long icsonUid;

	/**
	 * 用户经验值
	 *
	 * 版本 >= 0
	 */
	 private long experience;

	/**
	 * 易迅会员等级
	 *
	 * 版本 >= 0
	 */
	 private long icsonMemberLevel;

	/**
	 * 易讯虚拟经验值
	 *
	 * 版本 >= 0
	 */
	 private long virtualExp;

	/**
	 * reserve string
	 *
	 * 版本 >= 0
	 */
	 private String reserveStr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short icsonUid_u;

	/**
	 * 版本 >= 0
	 */
	 private short experience_u;

	/**
	 * 版本 >= 0
	 */
	 private short icsonMemberLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short virtualExp_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserveStr_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(icsonUid);
		bs.pushUInt(experience);
		bs.pushUInt(icsonMemberLevel);
		bs.pushUInt(virtualExp);
		bs.pushString(reserveStr);
		bs.pushUByte(version_u);
		bs.pushUByte(icsonUid_u);
		bs.pushUByte(experience_u);
		bs.pushUByte(icsonMemberLevel_u);
		bs.pushUByte(virtualExp_u);
		bs.pushUByte(reserveStr_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		icsonUid = bs.popLong();
		experience = bs.popUInt();
		icsonMemberLevel = bs.popUInt();
		virtualExp = bs.popUInt();
		reserveStr = bs.popString();
		version_u = bs.popUByte();
		icsonUid_u = bs.popUByte();
		experience_u = bs.popUByte();
		icsonMemberLevel_u = bs.popUByte();
		virtualExp_u = bs.popUByte();
		reserveStr_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取易讯uid,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid value 类型为:long
	 * 
	 */
	public long getIcsonUid()
	{
		return icsonUid;
	}


	/**
	 * 设置易讯uid,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIcsonUid(long value)
	{
		this.icsonUid = value;
		this.icsonUid_u = 1;
	}

	public boolean issetIcsonUid()
	{
		return this.icsonUid_u != 0;
	}
	/**
	 * 获取用户经验值
	 * 
	 * 此字段的版本 >= 0
	 * @return experience value 类型为:long
	 * 
	 */
	public long getExperience()
	{
		return experience;
	}


	/**
	 * 设置用户经验值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExperience(long value)
	{
		this.experience = value;
		this.experience_u = 1;
	}

	public boolean issetExperience()
	{
		return this.experience_u != 0;
	}
	/**
	 * 获取易迅会员等级
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonMemberLevel value 类型为:long
	 * 
	 */
	public long getIcsonMemberLevel()
	{
		return icsonMemberLevel;
	}


	/**
	 * 设置易迅会员等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIcsonMemberLevel(long value)
	{
		this.icsonMemberLevel = value;
		this.icsonMemberLevel_u = 1;
	}

	public boolean issetIcsonMemberLevel()
	{
		return this.icsonMemberLevel_u != 0;
	}
	/**
	 * 获取易讯虚拟经验值
	 * 
	 * 此字段的版本 >= 0
	 * @return virtualExp value 类型为:long
	 * 
	 */
	public long getVirtualExp()
	{
		return virtualExp;
	}


	/**
	 * 设置易讯虚拟经验值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVirtualExp(long value)
	{
		this.virtualExp = value;
		this.virtualExp_u = 1;
	}

	public boolean issetVirtualExp()
	{
		return this.virtualExp_u != 0;
	}
	/**
	 * 获取reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr value 类型为:String
	 * 
	 */
	public String getReserveStr()
	{
		return reserveStr;
	}


	/**
	 * 设置reserve string
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveStr(String value)
	{
		this.reserveStr = value;
		this.reserveStr_u = 1;
	}

	public boolean issetReserveStr()
	{
		return this.reserveStr_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonUid_u value 类型为:short
	 * 
	 */
	public short getIcsonUid_u()
	{
		return icsonUid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonUid_u(short value)
	{
		this.icsonUid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return experience_u value 类型为:short
	 * 
	 */
	public short getExperience_u()
	{
		return experience_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExperience_u(short value)
	{
		this.experience_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return icsonMemberLevel_u value 类型为:short
	 * 
	 */
	public short getIcsonMemberLevel_u()
	{
		return icsonMemberLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIcsonMemberLevel_u(short value)
	{
		this.icsonMemberLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return virtualExp_u value 类型为:short
	 * 
	 */
	public short getVirtualExp_u()
	{
		return virtualExp_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVirtualExp_u(short value)
	{
		this.virtualExp_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserveStr_u value 类型为:short
	 * 
	 */
	public short getReserveStr_u()
	{
		return reserveStr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserveStr_u(short value)
	{
		this.reserveStr_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PointsAndLevelPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 4;  //计算字段experience的长度 size_of(uint32_t)
				length += 4;  //计算字段icsonMemberLevel的长度 size_of(uint32_t)
				length += 4;  //计算字段virtualExp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, null);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段experience_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段virtualExp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PointsAndLevelPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段icsonUid的长度 size_of(uint64_t)
				length += 4;  //计算字段experience的长度 size_of(uint32_t)
				length += 4;  //计算字段icsonMemberLevel的长度 size_of(uint32_t)
				length += 4;  //计算字段virtualExp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserveStr, encoding);  //计算字段reserveStr的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonUid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段experience_u的长度 size_of(uint8_t)
				length += 1;  //计算字段icsonMemberLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段virtualExp_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserveStr_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

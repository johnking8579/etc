package com.paipai.halo.client.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.paipai.halo.client.ao.WebStubErr;


/**
 * 记录AO服务
 * @author JingYing 2014-10-20
 *
 */
public class AoLogItem extends AbstractLogItem{
	
	private long costMillis, cmdId;	//调用花费时长
	private Map<String, String> 
			request = new LinkedHashMap<String, String>(),		//key=request的短类名, value=request的字符串形式
			response = new LinkedHashMap<String, String>(),		//key=response的短类名, value=response的字符串形式
			stubParam = new LinkedHashMap<String, String>();	//AsynWebStub中的参数,相当于通用参数,不可直接序列化成gson,会循环序列化导致内存泄露
	private Throwable exception;	//调用函数时抛出的异常
	private StackTraceElement[] invokeStack;	//调用栈轨迹
	private String 
				invokeResult = "void",	//调用函数后返回值
				reqIpport,		//服务端IPPORT 
				encoding;		//编码
	private boolean isTimeout;	//接口是否超时
	
	@Override
	public String getProtocol() {
		return PROTOCOL_AO服务;
	}

	@Override
	public String getServiceUrl() {
		return reqIpport;
	}

	@Override
	public boolean hasError() {
		return exception != null;
	}

	@Override
	public String toText() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(">> requestId(%s)  wid(%s)\n", getRequestId(), getWid()));
		sb.append(String.format(">> 请求IP端口(%s),命令字(0x%s),编码(%s)\n>> AsyncWebstub部分参数:%s\n", 
									reqIpport, Long.toHexString(cmdId), encoding, new Gson().toJson(stubParam)));
		for(Entry<String,String> e : request.entrySet())	{
			sb.append(String.format(">> request参数[%s]:%s\n", e.getKey(), e.getValue()));
		}
		if(exception == null)	{
			String invokeDesc = null;
			try {
				invokeDesc = WebStubErr.map.get(Integer.parseInt(invokeResult));
			} catch (NumberFormatException e1) {			}
			sb.append(String.format("<< invoke函数返回值:%s(%s), 花费时间(%d)毫秒, 是否超时(%s)\n", 
									invokeResult, invokeDesc, costMillis, isTimeout?"Y":"N"));
			if(String.valueOf(WebStubErr.SUCCESS).equals(invokeResult))	{
				for(Entry<String,String> e : response.entrySet())	{
					sb.append(String.format("<< response参数[%s]:%s\n", e.getKey(), e.getValue()));
				}
			}
			sb.append(">> 调用栈轨迹(只跟踪开发代码):\n");
			if(invokeStack != null)	{
				for(String s : filterInvokeStack())	{
					sb.append("\t").append(s).append("\n");
				}
			} else	{
				sb.append("null\n");
			}
		} else	{
			StringWriter sw = new StringWriter(1000);
			exception.printStackTrace(new PrintWriter(sw));
			sb.append("<< AO服务抛出异常:").append(sw.toString()).append("\n");
		}
		sb.append("============================================================");	//60
		return sb.toString();
	}
	

	/**
	 * 专用字段:
	 * 命令字 | req类名 | 函数返回值 | 是否超时 | 编码  
	 */
	@Override
	public List<String> getParticularProp() {
		List<String> list = new ArrayList<String>();
		list.add("0x" + Long.toHexString(cmdId));
		for(Entry<String,String> e : request.entrySet())	{
			list.add(e.getKey());
			break;	//只记录第一个
		}
		if(exception == null)	{
			list.add(invokeResult);
			list.add(isTimeout ? "1" : "0");
		} else	{
			list.add("");
			list.add("");
		}
		list.add(encoding);
		return list;
	}

	@Override
	public StackTraceElement[] getInvokeStack() {
		return invokeStack;
	}
	
	public boolean hasException()	{
		return exception != null;
	}

	@Override
	public long getCostMillis() {
		return costMillis;
	}

	public void setCostMillis(long costMillis) {
		this.costMillis = costMillis;
	}

	public Map<String, String> getRequest() {
		return request;
	}

	public void setRequest(Map<String, String> request) {
		this.request = request;
	}

	public Map<String, String> getResponse() {
		return response;
	}

	public void setResponse(Map<String, String> response) {
		this.response = response;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public void setInvokeStack(StackTraceElement[] invokeStack) {
		this.invokeStack = invokeStack;
	}
	
	public String getInvokeResult() {
		return invokeResult;
	}

	public void setInvokeResult(String invokeResult) {
		this.invokeResult = invokeResult;
	}
	
	public long getCmdId() {
		return cmdId;
	}

	public void setCmdId(long cmdId) {
		this.cmdId = cmdId;
	}
	
	public String getReqIpport() {
		return reqIpport;
	}

	public void setReqIpport(String reqIpport) {
		this.reqIpport = reqIpport;
	}
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
	public Map<String, String> getStubParam() {
		return stubParam;
	}

	public void setStubParam(Map<String, String> stubParam) {
		this.stubParam = stubParam;
	}

	public boolean getIsTimeout() {
		return isTimeout;
	}

	public void setIsTimeout(boolean isTimeout) {
		this.isTimeout = isTimeout;
	}
	
	

}
/**
 * 
 */
package com.paipai.halo.client.ao.addr;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.halo.client.ao.WebStubFactory;
import com.paipai.halo.client.ao.addr.protocol.ApiGetRecvaddrReq;
import com.paipai.halo.client.ao.addr.protocol.ApiGetRecvaddrResp;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.util.net.NetUtils;


/**
 * @author oriondeng
 * 
 */
public class RecvAddrClient {
	
	/**
	 * 运行日志
	 */
	public static Logger run = LogManager.getLogger("run");

	
	/**
	 * 根据id查地址
	 */
	public static ApiGetRecvaddrResp getRecvaddr(long addId ,long uin,String sk, String mk)  throws BusinessException{
		run.info("addId=" + addId + ",uin=" + uin + ",sk=" + sk + ",mk =" + mk);
		ApiGetRecvaddrReq req = new ApiGetRecvaddrReq() ;
		ApiGetRecvaddrResp resp = new ApiGetRecvaddrResp() ;
		req.setAddrId(addId) ;
		req.setMachineKey(mk);
		req.setSource("o2o");
		int ret = 0;
		AsynWebStub webStub = WebStubFactory.getWebStub4PaiPaiGBK();
		webStub.setUin(uin);
		webStub.setOperator(uin);
		webStub.setSkeyToExtHead(sk);
		webStub.setRouteKey(uin);
		try {
			ret = webStub.invoke(req, resp);
			run.info("getRecvaddr:ip=" + webStub.getIp() + ",port="
					+ webStub.getPort());
			run.info(new Gson().toJson(resp));
		} catch (Exception e) {
			run.warn("e", e);
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}
	
	
}

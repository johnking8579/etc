package com.paipai.halo.client.http;

import java.util.HashMap;
import java.util.Map;

/**
 * http请求头
 */
public class ReqHead {
	
	private String host, contentType, referer, userAgent, cookie;
	
	/**
	 * 转换成http协议可识别的请求头
	 * @return
	 */
	public Map<String, String> toMap()	{
		Map<String,String> map = new HashMap<String,String>();
		if(contentType != null)		map.put("Content-Type", contentType);
		if(cookie != null)			map.put("Cookie", cookie);
		if(host != null)			map.put("Host", host);
		if(referer != null)			map.put("Referer", referer);
		if(userAgent != null)		map.put("User-Agent", userAgent);
		return map;
	}

	public ReqHead setHost(String host) {
		this.host = host;
		return this;
	}

	public ReqHead setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public ReqHead setReferer(String referer) {
		this.referer = referer;
		return this;
	}

	public ReqHead setUserAgent(String userAgent) {
		this.userAgent = userAgent;
		return this;
	}

	public ReqHead setCookie(String cookie) {
		this.cookie = cookie;
		return this;
	}

	public String getHost() {
		return host;
	}

	public String getContentType() {
		return contentType;
	}

	public String getReferer() {
		return referer;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getCookie() {
		return cookie;
	}

}

package com.paipai.halo.client.log;

import java.io.UnsupportedEncodingException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import com.paipai.halo.common.ump.Monitor;
import com.paipai.halo.common.ump.UmpKeyGen;

public class CacheMonitor implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		if(invocation.getMethod().getName().startsWith("get"))	{
			return onGet(invocation);
		} else if(invocation.getMethod().getName().startsWith("set"))	{
			return onSet(invocation);
		} else	{
			return invocation.proceed();
		}
	}
	
	private Object onGet(MethodInvocation invocation) throws Throwable 	{
		String umpKey = this.toUmpKey(invocation.getArguments()[0]);
		if(umpKey != null)	{
			Object caller = Monitor.registerInfo(umpKey);
			Object ret = invocation.proceed();
			if(ret == null)	{
				//读miss++, 读总数++
			} else	{
				//读命中++, 读总数++
			}
			Monitor.registerInfoEnd(caller);
			return ret;
		} else	{
			return invocation.proceed();
		}
	}
	
	private Object onSet(MethodInvocation invocation) throws Throwable 	{
		String umpKey = this.toUmpKey(invocation.getArguments()[0]);
		if(umpKey != null)	{
			Object caller = Monitor.registerInfo(umpKey);
			Object ret = invocation.proceed();
			//写次数++
			Monitor.registerInfoEnd(caller);
			return ret;
		} else	{
			return invocation.proceed();
		}
	}
	
	
	private String toUmpKey(Object key)	{
		String cacheKey = null;
		if(key instanceof String)	{
			cacheKey = (String)key;
		} else if(key instanceof byte[])	{
			try {
				cacheKey = new String((byte[])key, "utf-8");
			} catch (UnsupportedEncodingException e) {
			}
		}
		return UmpKeyGen.gen1stCache(cacheKey.split("//")[0]);
	}

}

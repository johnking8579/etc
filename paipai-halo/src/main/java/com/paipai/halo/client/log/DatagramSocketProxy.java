package com.paipai.halo.client.log;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import com.paipai.halo.common.ump.Monitor;
import com.paipai.halo.common.ump.UmpKeyGen;

public class DatagramSocketProxy implements MethodInterceptor{
	
	private PacketLogParser packetLogParser;
	
	/**
	 * 生成DatagramSocket代理类
	 * @param logParser 记录req/resp字符串, 如果不需要代理类记录这些参数, 传null
	 * @return
	 */
	public static DatagramSocket newProxy(PacketLogParser logParser)	{
		Enhancer e = new Enhancer();
		e.setSuperclass(DatagramSocket.class);
		DatagramSocketProxy proxy = new DatagramSocketProxy();
		proxy.packetLogParser = logParser;
		e.setCallback(proxy);
		return (DatagramSocket)e.create();
	}
	
	/**
	 * 监控DatagramSocket.receive(DatagramPacket p)方法
	 */
	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		if("receive".equals(method.getName()))	{
			UdpLogItem item = new UdpLogItem();
			item.setInvokeStack(Thread.currentThread().getStackTrace());
			long start = System.currentTimeMillis();
			Object caller = null;
			try {
				try {
					DatagramSocket d = (DatagramSocket)obj;
					String serviceName = packetLogParser==null ? "null" : packetLogParser.getServiceName();
					String key = String.valueOf(d.getRemoteSocketAddress()) + "(" + serviceName + ")";
					caller = Monitor.registerInfo(UmpKeyGen.gen3rdUdp(key));
				} catch (Exception e) {
					e.printStackTrace();
				}
				Object result = proxy.invokeSuper(obj, args);
				return result;
			} catch(IOException e)	{
				item.setException(e);
				Monitor.functionError(caller);
				throw e;
			} finally	{
				Monitor.registerInfoEnd(caller);
				item.setCostMillis(System.currentTimeMillis() - start);
				if(packetLogParser != null && item.getException() == null)	{
					try {
						item.setServiceName(packetLogParser.getServiceName());
						item.setReq(packetLogParser.parseRequest(new byte[0]));
						
						DatagramPacket recvPacket = (DatagramPacket)args[0];
						byte[] validResp;
						if(recvPacket.getLength() < recvPacket.getData().length){
							validResp = new byte[recvPacket.getLength()];
							System.arraycopy(recvPacket.getData(), 0, validResp, 0, recvPacket.getLength());	//移除响应报文字节流中无用的空数组
						} else	{
							validResp = recvPacket.getData();
						}
						item.setResp(packetLogParser.parseResponse(validResp));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				try {
					//首选记录datagramsocket.connect(host,ip)的host&ip，如果未指定，再记录响应报文中的host&ip
					DatagramSocket d = (DatagramSocket)obj;
					if(d.getRemoteSocketAddress() != null)	{	
						item.setRemoteSocketAddress(d.getRemoteSocketAddress().toString());
					} else 	{
						DatagramPacket recvPacket = (DatagramPacket)args[0];
						if(recvPacket.getAddress() != null){
							item.setRemoteSocketAddress(recvPacket.getAddress().getHostAddress() + ":" + recvPacket.getPort());
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				item.log();
			}
		} else	{
			return proxy.invokeSuper(obj, args);
		}
	}
	
}

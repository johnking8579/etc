package com.paipai.halo.client.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.util.Random;

import com.paipai.halo.client.log.PacketLogParser;
import com.paipai.halo.common.ump.Monitor;
import com.paipai.halo.common.ump.UmpKeyGen;

/**
 * 1.加入京东ump监控
 * 2.根据部署环境自动判定连接策略, 见createStrategy()
 * 3.记录日志
 * 
 * 调用示例见main()
 */
public class HttpClient {
	private int connectTimeout = 5000, 			//默认连接超时时间, 毫秒为单位
				readTimeout = 5000;				//默认读取超时时间, 毫秒为单位
	private HttpConn impl;
	
	/**
	 * @param url
	 * @param head 请求头 没有特殊请求头时传null
	 * @param logParser 如何记录到invoke-detail和invoke-outline日志中, 不记录的话传null
	 * @return
	 * @throws IOException
	 */
	public HttpResp get(String url, ReqHead head, PacketLogParser logParser) throws IOException	{
		return connect("GET", url, head, null, logParser);
	}
	
	/**
	 * post提交
	 * @param url
	 * @param head 请求头 没有特殊请求头时传null
	 * @param body 请求体,使用后自动关闭流
	 * 		如果使用multipart/form-data上传文件, 参照HttpUtil.uploadFile(), 或用apache-httpclient.jar拼接请求体, 例如:
		<pre>
		String boundary = HttpClient.generateBoundary();
		ReqHead head = new ReqHead().setContentType("multipart/form-data;boundary=" + boundary);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		MultipartEntityBuilder.create()
			.setBoundary(boundary)
			.addPart("name1", new StringBody("value1", ContentType.TEXT_PLAIN))
			.addPart("name2", new FileBody(new File("c:/aaa.txt"),ContentType.MULTIPART_FORM_DATA, "aaa.txt"))
			.addPart("name3", new ByteArrayBody("1111111111".getBytes(), ContentType.APPLICATION_OCTET_STREAM, "value3"))
			.build().writeTo(bos);
		ByteArrayInputStream body = new ByteArrayInputStream(bos.toByteArray());
		new HttpClient().post("url", head, body, null);
		</pre>
	 * @param logParser 如何记录到invoke-detail和invoke-outline日志中, 不记录的话传null.	
	 * 			如果只记录时间,不记录请求体响应体,传new OnlyTimeLogParser()
	 * @return
	 * @throws IOException
	 */
	public HttpResp post(String url, ReqHead head, InputStream body, PacketLogParser logParser) throws IOException	{
		return connect("POST", url, head, body, logParser);
	}
	
	
	private final static char[] MULTIPART_CHARS = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
	/**
	 * 自动生成Content-Type=multipart/form-data;boundary=${boundary}的分隔线
	 * @return
	 */
	public static String generateBoundary() {
        StringBuilder buffer = new StringBuilder();
        Random rand = new Random();
        int count = rand.nextInt(11) + 30; // a random size from 30 to 40
        for (int i = 0; i < count; i++) {
            buffer.append(MULTIPART_CHARS[rand.nextInt(MULTIPART_CHARS.length)]);
        }
        return buffer.toString();
    }
	
	private HttpResp connect(String method, String url, ReqHead head, InputStream body, 
							PacketLogParser logParser) throws IOException	{
		prepareImpl(Proxy.NO_PROXY);
		String key = UmpKeyGen.gen3rdHttp(truncateParam(url));
		Object caller = Monitor.registerInfo(key);
		try	{
			return impl.connect(method, url, head, body, logParser);
		} catch(IOException e)	{
			Monitor.functionError(caller);
			throw e;
		} finally	{
			Monitor.registerInfoEnd(caller);
		}
	}
	
	
	private void prepareImpl(Proxy proxy)	{
		if(!(impl instanceof HttpConnJdkImpl))	{
			impl = new HttpConnJdkImpl();
		}
		HttpConnJdkImpl i1 = (HttpConnJdkImpl)impl;
		i1.setConnectTimeout(connectTimeout);
		i1.setReadTimeout(readTimeout);
		i1.setProxy(proxy);
	}
	
	private String truncateParam(String url)	{
		int i = url.indexOf("?");
		return i > 0 ? url.substring(0, i) : url;
	}
	
	public HttpClient setTimeout(int connectTimeout, int readTimeout) {
		this.connectTimeout = connectTimeout;
		this.readTimeout = readTimeout;
		return this;
	}
	
	public static void main(String[] args) throws IOException {
		//简单get
		HttpResp resp1 = new HttpClient().get("http://www.baidu.com", null, null);
		System.out.println(new String(resp1.getResponse(), "gbk"));
		
		//post
		InputStream body = new ByteArrayInputStream("bodybody".getBytes("utf-8"));
		ReqHead head = new ReqHead().setUserAgent("chrome");
		HttpResp resp2 = new HttpClient().setTimeout(3000, 3000).post("https://www.baidu.com/s?wd=111", head, body, null);
		System.out.println(new String(resp2.getResponse(), "utf-8"));
		
		//带日志记录get
		HttpResp resp3 = new HttpClient().get("http://www.baidu.com", null, new PacketLogParser() {
			
			@Override
			public String parseResponse(byte[] response) throws Exception {
				if(response != null)	return new String(response, "utf-8");
				return null;
			}
			
			@Override
			public String parseRequest(byte[] request) throws Exception {
				return "请求体请求体";	//或return null
			}
			
			@Override
			public String getServiceName() {
				return null;	//一般用于tcp/udp请求.
			}
		});
		System.out.println(new String(resp3.getResponse(), "utf-8"));
	}
}

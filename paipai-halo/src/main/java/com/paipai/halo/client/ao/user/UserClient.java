package com.paipai.halo.client.ao.user;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.halo.client.ao.WebStubFactory;
import com.paipai.halo.client.ao.user.protocol.GetUserLinkInfoReq;
import com.paipai.halo.client.ao.user.protocol.GetUserLinkInfoResp;
import com.paipai.halo.common.exception.ExternalInterfaceException;

public class UserClient {
	
	/**
	 * 给用户推荐好友
	 * @param uin
	 * @param count
	 * @return 用户UIN列表.	resp.result=1时:代表没有关系链. -1为请求失败,无细分原因. 2015-04-23
	 */
	public GetUserLinkInfoResp getUserLinkInfo(long uin, int count)	{
		GetUserLinkInfoReq req = new GetUserLinkInfoReq();
		GetUserLinkInfoResp resp = new GetUserLinkInfoResp();
		req.setUin(uin);
		req.setReqnum(count);
		
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		try {
			int ret = stub.invoke(req, resp);
			if(ret != 0 || (resp.getResult() != 0 && resp.getResult() != 1))	
				throw new ExternalInterfaceException(ret, resp.getResult());
			return resp;
		} catch (AsynWebStubException e) {
			throw new ExternalInterfaceException(e);
		}
	}
}

 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *敏感词过滤(批量)之回复
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  BatchCleanResp implements IServiceObject
{
	public long result;
	/**
	 * 过滤结果信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<DirtyRet> DirtyRetDo = new Vector<DirtyRet>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(DirtyRetDo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		DirtyRetDo = (Vector<DirtyRet>)bs.popVector(DirtyRet.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60008804L;
	}


	/**
	 * 获取过滤结果信息
	 * 
	 * 此字段的版本 >= 0
	 * @return DirtyRetDo value 类型为:Vector<DirtyRet>
	 * 
	 */
	public Vector<DirtyRet> getDirtyRetDo()
	{
		return DirtyRetDo;
	}


	/**
	 * 设置过滤结果信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DirtyRet>
	 * 
	 */
	public void setDirtyRetDo(Vector<DirtyRet> value)
	{
		if (value != null) {
				this.DirtyRetDo = value;
		}else{
				this.DirtyRetDo = new Vector<DirtyRet>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BatchCleanResp)
				length += ByteStream.getObjectSize(DirtyRetDo, null);  //计算字段DirtyRetDo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(BatchCleanResp)
				length += ByteStream.getObjectSize(DirtyRetDo, encoding);  //计算字段DirtyRetDo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *更新黑白名单信息之回复
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  UpdateListResp implements IServiceObject
{
	public long result;
	/**
	 * 输出预留项
	 *
	 * 版本 >= 0
	 */
	 private String OutReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(OutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		OutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60008803L;
	}


	/**
	 * 获取输出预留项
	 * 
	 * 此字段的版本 >= 0
	 * @return OutReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return OutReserve;
	}


	/**
	 * 设置输出预留项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.OutReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UpdateListResp)
				length += ByteStream.getObjectSize(OutReserve, null);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateListResp)
				length += ByteStream.getObjectSize(OutReserve, encoding);  //计算字段OutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

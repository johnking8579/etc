 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *敏感词过滤(批量)之请求,utf-8
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  BatchClean_UReq implements IServiceObject
{
	/**
	 * 来源信息
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 请求端IP
	 *
	 * 版本 >= 0
	 */
	 private String Ip = new String();

	/**
	 * 过滤参数
	 *
	 * 版本 >= 0
	 */
	 private Vector<DirtyDo> DirtyDo = new Vector<DirtyDo>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(Ip);
		bs.pushObject(DirtyDo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		Ip = bs.popString();
		DirtyDo = (Vector<DirtyDo>)bs.popVector(DirtyDo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60001807L;
	}


	/**
	 * 获取来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取请求端IP
	 * 
	 * 此字段的版本 >= 0
	 * @return Ip value 类型为:String
	 * 
	 */
	public String getIp()
	{
		return Ip;
	}


	/**
	 * 设置请求端IP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIp(String value)
	{
		this.Ip = value;
	}


	/**
	 * 获取过滤参数
	 * 
	 * 此字段的版本 >= 0
	 * @return DirtyDo value 类型为:Vector<DirtyDo>
	 * 
	 */
	public Vector<DirtyDo> getDirtyDo()
	{
		return DirtyDo;
	}


	/**
	 * 设置过滤参数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DirtyDo>
	 * 
	 */
	public void setDirtyDo(Vector<DirtyDo> value)
	{
		if (value != null) {
				this.DirtyDo = value;
		}else{
				this.DirtyDo = new Vector<DirtyDo>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(BatchClean_UReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Ip, null);  //计算字段Ip的长度 size_of(String)
				length += ByteStream.getObjectSize(DirtyDo, null);  //计算字段DirtyDo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(BatchClean_UReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(Ip, encoding);  //计算字段Ip的长度 size_of(String)
				length += ByteStream.getObjectSize(DirtyDo, encoding);  //计算字段DirtyDo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: c2cent.dao.dirty.DirtyDAO.java

package com.paipai.halo.dirty.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *更新黑白名单信息之请求
 *
 *@date 2015-04-09 01:46:06
 *
 *@since version:0
*/
public class  UpdateListReq implements IServiceObject
{
	/**
	 * 来源信息
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 输入预留项
	 *
	 * 版本 >= 0
	 */
	 private String InReserve = new String();

	/**
	 * 控制项
	 *
	 * 版本 >= 0
	 */
	 private long Control;

	/**
	 * 黑名单
	 *
	 * 版本 >= 0
	 */
	 private String BlackList = new String();

	/**
	 * 黑名单更新时间
	 *
	 * 版本 >= 0
	 */
	 private long BlackTimeStamp;

	/**
	 * 白名单
	 *
	 * 版本 >= 0
	 */
	 private String WhiteList = new String();

	/**
	 * 白名单更新时间
	 *
	 * 版本 >= 0
	 */
	 private long WhiteTimeStamp;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(InReserve);
		bs.pushUInt(Control);
		bs.pushString(BlackList);
		bs.pushUInt(BlackTimeStamp);
		bs.pushString(WhiteList);
		bs.pushUInt(WhiteTimeStamp);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		InReserve = bs.popString();
		Control = bs.popUInt();
		BlackList = bs.popString();
		BlackTimeStamp = bs.popUInt();
		WhiteList = bs.popString();
		WhiteTimeStamp = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x60001803L;
	}


	/**
	 * 获取来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取输入预留项
	 * 
	 * 此字段的版本 >= 0
	 * @return InReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return InReserve;
	}


	/**
	 * 设置输入预留项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.InReserve = value;
	}


	/**
	 * 获取控制项
	 * 
	 * 此字段的版本 >= 0
	 * @return Control value 类型为:long
	 * 
	 */
	public long getControl()
	{
		return Control;
	}


	/**
	 * 设置控制项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setControl(long value)
	{
		this.Control = value;
	}


	/**
	 * 获取黑名单
	 * 
	 * 此字段的版本 >= 0
	 * @return BlackList value 类型为:String
	 * 
	 */
	public String getBlackList()
	{
		return BlackList;
	}


	/**
	 * 设置黑名单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBlackList(String value)
	{
		this.BlackList = value;
	}


	/**
	 * 获取黑名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BlackTimeStamp value 类型为:long
	 * 
	 */
	public long getBlackTimeStamp()
	{
		return BlackTimeStamp;
	}


	/**
	 * 设置黑名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBlackTimeStamp(long value)
	{
		this.BlackTimeStamp = value;
	}


	/**
	 * 获取白名单
	 * 
	 * 此字段的版本 >= 0
	 * @return WhiteList value 类型为:String
	 * 
	 */
	public String getWhiteList()
	{
		return WhiteList;
	}


	/**
	 * 设置白名单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWhiteList(String value)
	{
		this.WhiteList = value;
	}


	/**
	 * 获取白名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return WhiteTimeStamp value 类型为:long
	 * 
	 */
	public long getWhiteTimeStamp()
	{
		return WhiteTimeStamp;
	}


	/**
	 * 设置白名单更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWhiteTimeStamp(long value)
	{
		this.WhiteTimeStamp = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateListReq)
				length += ByteStream.getObjectSize(Source, null);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(InReserve, null);  //计算字段InReserve的长度 size_of(String)
				length += 4;  //计算字段Control的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BlackList, null);  //计算字段BlackList的长度 size_of(String)
				length += 4;  //计算字段BlackTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(WhiteList, null);  //计算字段WhiteList的长度 size_of(String)
				length += 4;  //计算字段WhiteTimeStamp的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateListReq)
				length += ByteStream.getObjectSize(Source, encoding);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(InReserve, encoding);  //计算字段InReserve的长度 size_of(String)
				length += 4;  //计算字段Control的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BlackList, encoding);  //计算字段BlackList的长度 size_of(String)
				length += 4;  //计算字段BlackTimeStamp的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(WhiteList, encoding);  //计算字段WhiteList的长度 size_of(String)
				length += 4;  //计算字段WhiteTimeStamp的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}

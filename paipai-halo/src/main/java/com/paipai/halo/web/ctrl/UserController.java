package com.paipai.halo.web.ctrl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.jd.jim.cli.Cluster;
import com.paipai.halo.client.ao.user.UserClient;
import com.paipai.halo.client.ao.user.protocol.GetUserLinkInfoResp;
import com.paipai.halo.common.exception.NotLoginException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.common.login.TokenUtil;
import com.paipai.halo.domain.UserBaseInfo;
import com.paipai.halo.service.ShopAroService;
import com.paipai.halo.web.JsonOutput;
import com.paipai.lang.uint64_t;

@Controller
@RequestMapping("user")
@ResponseBody	//全局统一使用responseBody
public class UserController {
	
	@Resource
	ShopAroService shopAroService;
	UserClient userClient = new UserClient();
	Gson gson = new GsonBuilder().serializeNulls().create();
	@Resource
	Cluster jimClientProxy;
	
	@RequestMapping("getLink")
	public String getLink(String appToken, String mk, 
						@RequestParam(defaultValue="10") int count)	{
		assertLogin(appToken, mk);
		long uin = parseWid(appToken);
		GetUserLinkInfoResp resp = userClient.getUserLinkInfo(uin, count);
		List<UserBaseInfo> users = new ArrayList<UserBaseInfo>();
		for(uint64_t l : resp.getUserIDList())	{
			UserBaseInfo u = shopAroService.getUserInfo(l.longValue());
			if(u != null)	users.add(u);
		}
		JsonElement arr = gson.toJsonTree(users);
		return new JsonOutput().setData(arr).toJsonStr();
	}
	
	@RequestMapping("testUmp")
	public String testUmp(String method, String key, String value)	{
		if("get".equals(method))	{
			return jimClientProxy.get(key);
		} else if("set".equals(method))	{
			jimClientProxy.set(key, value);
		}
		return null;
	}
	
	/**
	 * 必须登陆
	 * @param appToken
	 * @param mk
	 */
	private void assertLogin(String appToken, String mk)	{
		boolean isLogin;
		try {
			isLogin = LoginUtil.checkLoginForApp(appToken, mk);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
		if(!isLogin)	{
			throw new NotLoginException();
		}
	}
	
	/**
	 * 解析wid
	 * @param appToken
	 * @return
	 */
	private long parseWid(String appToken)	{
		try {
			return TokenUtil.getAppToken(appToken).getWid();
		} catch (Exception e) {
			return 0;
		}
	}
	
	
}

package com.paipai.halo.web.ctrl.cms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


public abstract class CmsBaseController {
	
	private static Logger log = LogManager.getLogger();
	
	/**
	 * 不使用配置文件的exceptionHandler, 把子类的错误页面定向到cms-503.jsp
	 */
	@ExceptionHandler
	public ModelAndView handleException(Exception e)	{
		log.error(e.getMessage(), e);
	 	return new ModelAndView("cms-503");
	}

}

package com.paipai.halo.web.session;

import javax.servlet.http.HttpServletRequest;

/**
 * 原生session实现
 */
public class SessionMode implements MockSession {
	
	private HttpServletRequest req;
	
	public SessionMode(HttpServletRequest req){
		this.req = req;
	}

	@Override
	public void set(String key, Object value) {
		req.getSession().setAttribute(key, value);
	}

	@Override
	public Object get(String key) {
		return req.getSession().getAttribute(key);
	}

	@Override
	public void remove(String... key) {
		for(String s : key)	{
			req.getSession().removeAttribute(s);
		}
	}
}

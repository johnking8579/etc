package com.paipai.halo.web.session;

/**
 * cookie加密
 */
public interface CookieEncryptor {
	
	byte[] encrypt(byte[] src);
	
	byte[] decrypt(byte[] src);

}

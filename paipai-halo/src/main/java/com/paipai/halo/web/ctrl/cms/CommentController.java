package com.paipai.halo.web.ctrl.cms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.PageModel;
import com.paipai.halo.common.Pager;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.jqgrid.Const;
import com.paipai.halo.common.jqgrid.Crud;
import com.paipai.halo.common.jqgrid.CrudExecutor;
import com.paipai.halo.common.jqgrid.JqgridReq;
import com.paipai.halo.common.jqgrid.JqgridSerializer;
import com.paipai.halo.domain.Comment;
import com.paipai.halo.domain.UserBaseInfo;
import com.paipai.halo.service.CommentService;
import com.paipai.halo.service.ShopAroService;
import com.paipai.halo.web.JsonOutput;

/**
 * 评论操作
 */
@Controller
@RequestMapping("cms/comment")
public class CommentController extends CmsBaseController {
	
	@Resource
	CommentService commentService;
	
	@Resource
	private ShopAroService shopAroService;
	
	@RequestMapping("index")
	public ModelAndView index(HttpServletRequest req)	{
		return new ModelAndView("cms-comment");
	}
	
	@RequestMapping("index1")
	public ModelAndView index1()	{
		return new ModelAndView("cms-comment1");
	}
	@RequestMapping("index2")
	public ModelAndView index2()	{
		return new ModelAndView("cms-comment2");
	}
	
	@RequestMapping("index3")
	public ModelAndView index3()	{
		return new ModelAndView("cms-comment3");
	}
	@RequestMapping("batchPass")
	public String BatchPass(HttpServletResponse resp,String id){
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		//审批通过
		List<Long> ids = Util.toLongList(id.split(","));
		commentService.updateIsshow(ids, 1);
		return doPrint(resp, new Gson().toJson(out));
	}
	@RequestMapping("delall")
	public String  delall(HttpServletResponse resp,String id){
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		//审批通过
		List<Long> ids = Util.toLongList(id.split(","));
		commentService.updateIsshow(ids, 2);
		return doPrint(resp, new Gson().toJson(out));
	}
	@RequestMapping(value = "/delb")
	@ResponseBody
	public String delBl(HttpServletResponse resp,String  wid) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		if(wid!=null&&!wid.equals("")){
			String [] i = wid.split(",");
			List<Long> uids = Util.toLongList(wid.split(","));
			//批量的更新一下wid的用户缓存
			try {
				this.shopAroService.updateUserBl(uids, null,0);
				List<UserBaseInfo> ub = new ArrayList<UserBaseInfo>();
				for(Long uid :uids){
					ub.add(this.shopAroService.getUserInfo(uid));
				}
				out.setData(new Gson().toJsonTree(ub));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		
		//将传入的wid加入黑名单缓存。临时解决方式去掉
		//String blackwid = this.shopAroService.addBloack(wid);
		return doPrint(resp, new Gson().toJson(out));
	}
	@RequestMapping(value = "/addbl")
	@ResponseBody
	public String addBlack(HttpServletResponse resp,String  id) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		if(id!=null&&!id.equals("")){
			String [] i = id.split(",");
			List<Long> ids = Util.toLongList(id.split(","));
			List<Comment> item = this.shopAroService.getCommentList(ids);
			//将用户加入黑名单
			List<Long> uids = new ArrayList<Long>();
			for(Comment c:item){
				//如果里面没有则将保存
				if(Collections.frequency(uids,c.getWid())==0){
					uids.add(c.getWid());
				}
			}
			//更新黑名单 同时删除该人的所有留言并前更新评论数据 量
			try {
				this.shopAroService.updateUserBl(uids,ids,1);
				out.setErrCode(0);
				out.setMsg("success");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		
		//将传入的wid加入黑名单缓存。临时解决方式去掉
		//String blackwid = this.shopAroService.addBloack(wid);
		return doPrint(resp, new Gson().toJson(out));
	}
	@RequestMapping("jqgridReq")
	@ResponseBody
	@SuppressWarnings("rawtypes")
	public String jqgridReq(HttpServletRequest req, final String id)	{
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		return CrudExecutor.execute(req.getParameterMap(), new Crud(){

			@Override
			public String add(JqgridReq req) {
				throw new UnsupportedOperationException();
			}

			@Override
			public String edit(JqgridReq req) {
				CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
				//审批通过
				List<Long> ids = Util.toLongList(id.split(","));
				commentService.updateIsshow(ids, Integer.valueOf(req.getIsshow()));
				return null;
			}
			
			@Override
			public String del(JqgridReq req) {
				CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
				List<Long> ids = Util.toLongList(id.split(","));
				commentService.updateIsshow(ids, 0);
				return null;
			}

			@Override
			public String query(JqgridReq req) {
				CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
				Pager<Map> pager = commentService.find(req.getSidx(), req.getSord(), req.calcOffset(), req.getRows());
				return new JqgridSerializer().toMaingrid(new PageModel<Map>(pager, req.getPage(), req.getRows()), commentService);
			}

			@Override
			public String search(JqgridReq req) {
				throw new UnsupportedOperationException();
			}

			@Override
			public String filterSearch(JqgridReq req) {
				CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
				if(Const.GROUPOP_AND.equalsIgnoreCase(req.getFilters().getGroupOp()))	{
					Pager<Map> pager = commentService.filterSearch(req.getFilters().getRules(), req.getSidx(), req.getSord(), req.calcOffset(), req.getRows());
					return new JqgridSerializer().toMaingrid(new PageModel<Map>(pager, req.getPage(), req.getRows()), commentService);
				} else	{
					throw new UnsupportedOperationException(req.getFilters().getGroupOp());
				}
			}
		});
	}
	/**
	 * 使用servlet方式, 向response写入字符串
	 * 
	 * @param obj
	 * @return
	 */

	protected String doPrint(HttpServletResponse resp, String result) {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(result);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return null;
	}
}

package com.paipai.halo.web.session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;


/**
 * 利用cookie,实现session效果
 */
public class CookieMode implements MockSession{
	
	private HttpServletRequest req;
	private HttpServletResponse resp;
	private ObjectSerializer objSerializer;
	private boolean isEncrypt;
	private CookieEncryptor cookieEncryptor;
	
	public CookieMode(HttpServletRequest req, HttpServletResponse resp, ObjectSerializer objSerializer, 
						boolean isEncrypt, CookieEncryptor cookieEncryptor){
		this.req = req;
		this.resp = resp;
		this.objSerializer = objSerializer;
		this.isEncrypt = isEncrypt;
		this.cookieEncryptor = cookieEncryptor;
	}

	/**
	 * cookie的path统一设为/,时间设为最大值. 判断是否过期时,只看单点登录cookie的有效期.
	 * 
	 * string/object -> byte[](by jdk object serialize) -> byte[](by 3des encrypt) -> string(by url safe base64)
	 */
	@Override
	public void set(String key, Object value) {
		byte[] src = objSerializer.serialize(value);
		if(isEncrypt)
			src = cookieEncryptor.encrypt(src);
		Cookie c = new Cookie(key, encodeToStr(src));
		c.setPath("/");
		c.setMaxAge(Integer.MAX_VALUE);
		resp.addCookie(c);
	}
	
	/**
	 * string -> byte[](by base64 url safe decode) -> byte[] (by 3des decrypt) -> byte[] (by jdk object deserialize) -> string/object
	 */
	@Override
	public Object get(String key) {
		Cookie cookie = getCookie(key);
		if(cookie == null)	return null;
			byte[] bs = decodeFromStr(cookie.getValue());
			if(isEncrypt)
				bs = cookieEncryptor.decrypt(bs);
			return objSerializer.deserialize(bs);
	}
	
	private Cookie getCookie(String key)	{
		for(Cookie c : req.getCookies())	{
			if(c.getName().equals(key))
				return c;
		}
		return null;
	}
	
	@Override
	public void remove(String...key) {
		for(String s : key)	{
			Cookie cookie = new Cookie(s, null);
			cookie.setPath("/");
			cookie.setMaxAge(0);
			resp.addCookie(cookie);
        }
	}
	
	private String encodeToStr(byte[] value)	{
		return new String(Base64.encodeBase64URLSafe(value));
	}
	
	private byte[] decodeFromStr(String s)	{
		return Base64.decodeBase64(s);
	}

}

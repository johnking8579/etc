package com.paipai.halo.web.ctrl;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.common.login.TokenUtil;
import com.paipai.halo.domain.LikeLogInfo;
import com.paipai.halo.domain.NoLikeInfo;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.service.NoLikeService;
import com.paipai.halo.web.JsonOutput;

/**
 * 
  * @ClassName: 踩
  * @Description: TODO
  * @author Comsys-lianghaining1
  * @date 2015年4月22日 下午5:29:11
  *
 */
@Controller
@RequestMapping("/nolike")
public class NoLikeController extends BaseController {

	@Resource
	private NoLikeService nolikeService;
	
	/**
	 * 该类用于点赞或者取消点赞
	 * 
	 * @return
	 */
	@RequestMapping(value = "/setnolike")
	@ResponseBody
	public String setNoLike(HttpServletResponse resp, String itemid,
			Integer itemtype, Integer islike, Long ownerid, String nickname,
			String head, Integer usertype, Long wid, String skey,
			String appToken, String mk, String jsonp) {
		String result = "";
		JsonOutput out = new JsonOutput();
		AppToken token = null;
		try {
			token = checkegLogin(appToken, mk, String.valueOf(wid), skey);
		}  catch (Exception e1) {
			e1.printStackTrace();
		}
		if (token!=null) {
			if (wid == null || "".equals(wid)) {
					wid = token.getWid();
			}
			// 检验参数是否为空
			if (itemid == null || "".equals(itemid) || wid == null
					|| "".equals(wid) || ownerid == null || "".equals(ownerid)
					|| itemtype == null || "".equals(itemtype)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				out.setMsg("参数不合法！");
				result = new Gson().toJson(out);
			} else {
					try {
						result = this.nolikeService.setNoLike(itemid, itemtype,
								wid, ownerid);
					} catch (Exception e) {
						e.printStackTrace();
						out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
						out.setMsg("数据异常，点赞失败");
						result = new Gson().toJson(out);
					}
				
			}

		} else {

			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
			out.setMsg("请您登录");
			result = new Gson().toJson(out);
		}
	
		if (jsonp != null && !jsonp.equals("")) {
			result = "try{" + jsonp + "(" + result + ");}catch(e){}";
		}
		return doPrint(resp, result);
	}
	
}

/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.web.ctrl.paipaisign;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.domain.paipaisign.PpappBehaviorInfo;
import com.paipai.halo.service.paipaisign.PpappBehaviorInfoService;
import com.paipai.halo.web.JsonOutput;

/**
 *ppappBehaviorInfo controller层
 * @author J-ONE
 * @since 2015-04-09
 */
@Controller
@RequestMapping(value = "/ppappBehaviorInfo")
public class PpappBehaviorInfoController{
	public static Logger run = LogManager.getLogger("run");
	
	@Resource private PpappBehaviorInfoService ppappBehaviorInfoService;
	
	/**
	 * 7.	根据行为类型查询行为记录接口
	 * http://127.0.0.1:8080/paipai-halo/ppappBehaviorInfo/behaviorInfo.action?activeId=1&wid=1&type=1
	 * @param appToken
	 * @param activeId
	 * @param wid
	 * @param skey
	 * @param jsonp
	 * @param mk
	 * @param mk2
	 * @param beginTime
	 * @param endTime
	 * @param type
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/behaviorInfo", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String behaviorInfo(String appToken, int activeId, String wid, String skey, String callback, String mk, String mk2, 
			String beginTime, String endTime, int type, HttpServletRequest request){
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null == appTokenObj) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			} else {
				CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;//--------------
				List<PpappBehaviorInfo> list = this.ppappBehaviorInfoService.behaviorInfo(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid), activeId, type, beginTime, endTime);
			
				//测试：
//				List<PpappBehaviorInfo> list = this.ppappBehaviorInfoService.behaviorInfo(123456, activeId, type, "20150403", "20150413");
				
				JsonParser jsonParser = new JsonParser();
				out.setData(jsonParser.parse(new Gson().toJson(list)));
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}

		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
}
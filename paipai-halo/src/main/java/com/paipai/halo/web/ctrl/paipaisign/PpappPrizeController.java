/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.web.ctrl.paipaisign;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.TimeUtils;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.domain.paipaisign.PpappPrizePo;
import com.paipai.halo.service.paipaisign.PpappPrizeService;
import com.paipai.halo.web.JsonOutput;
import com.paipai.halo.web.ctrl.BaseController;

/**
 *ppappPrize controller层
 * @author J-ONE
 * @since 2015-04-09
 */
@Controller
@RequestMapping(value = "/ppappPrize")
public class PpappPrizeController extends BaseController{
	public static Logger run = LogManager.getLogger("run");
	@Resource private PpappPrizeService ppappPrizeService;
	
	@RequestMapping(value = "/findPrize")
	public void findPrize(String activeId, String prizeLevel, String type, String callback, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(activeId) || StringUtils.isEmpty(prizeLevel)
					|| StringUtils.isEmpty(type)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			int activeIdInt,prizeLevelInt,typeInt;
			String activeDay ;
			try {
				activeIdInt = Integer.parseInt(activeId) ;
				prizeLevelInt = Integer.parseInt(prizeLevel) ;
				typeInt = Integer.parseInt(type) ;				
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			if (0 == typeInt) {
				activeDay = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD) ;
			} else if (1 == typeInt) {
				activeDay = TimeUtils.formatDateToDayString(TimeUtils.getIncrementDate(1), TimeUtils.TIME_FMT_YYYYMMDD) ;
			} else {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			String activeStartTime = Config.get("signActive."+ activeIdInt +".startTime") ;
			String activeEndTime = Config.get("signActive."+ activeIdInt +".endTime") ;
			if (StringUtils.isEmpty(activeEndTime)||StringUtils.isEmpty(activeStartTime)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			String currentTime = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD_HHMMSS);
			if (currentTime.compareTo(activeStartTime) < 0 || currentTime.compareTo(activeEndTime) > 0) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			if (activeDay.compareTo(activeEndTime) > 0) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "今日是活动最后一天");
			}
			CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;
			List<PpappPrizePo> ppappPrizePoList = ppappPrizeService.findPrize(activeIdInt, prizeLevelInt, typeInt, activeDay) ;
			{
				JsonObject data = new JsonObject();
				data.add("prizePoList", new Gson().toJsonTree(ppappPrizePoList));
				data.addProperty("activeDay", activeDay);
				out.setData(data);
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
		}
		String result = out.toJsonStr() ;
		if (callback != null && !callback.equals("")) {
			result = "try{" + callback + "(" + result + ");}catch(e){}";
		}
		doPrint(resp, result) ;
	}
	
	@RequestMapping(value = "/receivePrize")
	public void receivePrize(String appToken, String mk, String mk2, String wid, String skey, 
			String activeId, String prizeId, String remark, 
			String addId, String callback, HttpServletRequest request, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(activeId) || StringUtils.isEmpty(prizeId)
					|| StringUtils.isEmpty(addId)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			int activeIdInt;
			long addIdL ;
			try {
				activeIdInt = Integer.parseInt(activeId) ;
				addIdL = Long.parseLong(addId) ;			
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			String activeStartTime = Config.get("signActive."+ activeIdInt +".startTime") ;
			String activeEndTime = Config.get("signActive."+ activeIdInt +".endTime") ;
			if (StringUtils.isEmpty(activeEndTime)||StringUtils.isEmpty(activeStartTime)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			String currentTime = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD_HHMMSS);
			if (currentTime.compareTo(activeStartTime) < 0 || currentTime.compareTo(activeEndTime) > 0) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			AppToken appTokenObj = checkegLogin(appToken, mk, wid, skey) ;
			if (null == appTokenObj) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CHECKLOGIN_FAIL, "请您登录");
			}
			long widL = appTokenObj.getWid() ;
			String sk = appTokenObj.getLsk() ;
			
			CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;
			Map<String,Object> resultMap = ppappPrizeService.receivePrize(activeIdInt, 
					prizeId, widL, sk, addIdL, mk, mk2, request, remark) ;
			out.setData(new Gson().toJsonTree(resultMap));
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
		}
		String result = out.toJsonStr() ;
		if (callback != null && !callback.equals("")) {
			result = "try{" + callback + "(" + result + ");}catch(e){}";
		}
		doPrint(resp, result) ;
	}
	
	@RequestMapping(value = "/receiveVouchers")
	public void receiveVouchers(String appToken, String mk, String mk2, String wid, String skey, 
			String activeId, String callback, HttpServletRequest request, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(activeId)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			int activeIdInt ;
			try {
				activeIdInt = Integer.parseInt(activeId) ;			
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			String activeStartTime = Config.get("signActive."+ activeIdInt +".startTime") ;
			String activeEndTime = Config.get("signActive."+ activeIdInt +".endTime") ;
			if (StringUtils.isEmpty(activeEndTime)||StringUtils.isEmpty(activeStartTime)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			String currentTime = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD_HHMMSS);
			if (currentTime.compareTo(activeStartTime) < 0 || currentTime.compareTo(activeEndTime) > 0) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			AppToken appTokenObj = checkegLogin(appToken, mk, wid, skey) ;
			if (null == appTokenObj) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_CHECKLOGIN_FAIL, "请您登录");
			}
			long widL = appTokenObj.getWid() ;
			String sk = appTokenObj.getLsk() ;
			
			CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;
			Map<String,Object> resultMap = ppappPrizeService.receiveVouchers(activeIdInt, widL, sk, mk, mk2, request) ;
			out.setData(new Gson().toJsonTree(resultMap));
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
		}
		String result = out.toJsonStr() ;
		if (callback != null && !callback.equals("")) {
			result = "try{" + callback + "(" + result + ");}catch(e){}";
		}
		doPrint(resp, result) ;
	}
	
	@RequestMapping(value = "/prizeInfo")
	public void prizeInfo(String activeId, String prizeIdList, String callback, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(activeId) || StringUtils.isEmpty(prizeIdList)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			int activeIdInt;
			try {
				activeIdInt = Integer.parseInt(activeId) ;
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}			
			String activeStartTime = Config.get("signActive."+ activeIdInt +".startTime") ;
			String activeEndTime = Config.get("signActive."+ activeIdInt +".endTime") ;
			if (StringUtils.isEmpty(activeEndTime)||StringUtils.isEmpty(activeStartTime)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}
			String currentTime = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD_HHMMSS);
			if (currentTime.compareTo(activeStartTime) < 0 || currentTime.compareTo(activeEndTime) > 0) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "活动不存在");
			}		
			String activeDay = TimeUtils.formatDateToDayString(TimeUtils.getCurrentDate(), TimeUtils.TIME_FMT_YYYYMMDD) ;

			CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;
			List<PpappPrizePo> ppappPrizePoList = ppappPrizeService.prizeInfo(activeIdInt, prizeIdList, 0, activeDay) ;
			{
				JsonObject data = new JsonObject();
				data.add("prizePoList", new Gson().toJsonTree(ppappPrizePoList));
				out.setData(data);
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
		}
		String result = out.toJsonStr() ;
		if (callback != null && !callback.equals("")) {
			result = "try{" + callback + "(" + result + ");}catch(e){}";
		}
		doPrint(resp, result) ;
	}
	/**
	 * 删除奖品列表缓存，活动上线，奖品确认后可以屏蔽该接口
	 */
	@RequestMapping(value = "/removePrizeListCache")
	public void removePrizeListCache(String key, HttpServletResponse resp) {
		JsonOutput out = new JsonOutput();
		try {
			if (!StringUtils.isEmpty(key) && key.equals("halo_sign_test")) {
				ppappPrizeService.removePrizeListCache() ;
				JsonObject data = new JsonObject();
				data.addProperty("removePrizeListCache", "success");
				out.setData(data);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("系统异常");
		}
		String result = out.toJsonStr() ;		
		doPrint(resp, result) ;
	}
		
	
}
package com.paipai.halo.web.session;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/*字符串 DESede(3DES) 加密  
 * ECB模式/使用PKCS7方式填充不足位,目前给的密钥是192位  
 * 3DES（即Triple DES）是DES向AES过渡的加密算法（1999年，NIST将3-DES指定为过渡的  
 * 加密标准），是DES的一个更安全的变形。它以DES为基本模块，通过组合分组方法设计出分组加  
 * 密算法，其具体实现如下：设Ek()和Dk()代表DES算法的加密和解密过程，K代表DES算法使用的  
 * 密钥，P代表明文，C代表密表，这样，  
 * 3DES加密过程为：C=Ek3(Dk2(Ek1(P)))  
 * 3DES解密过程为：P=Dk1((EK2(Dk3(C)))  
 * */
public class DesEncryptor implements CookieEncryptor{

	/**
	 * @param args在java中调用sun公司提供的3DES加密解密算法时
	 *            ，需要使 用到$JAVA_HOME/jre/lib/目录下如下的4个jar包： jce.jar
	 *            security/US_export_policy.jar security/local_policy.jar
	 *            ext/sunjce_provider.jar
	 */
	private String Algorithm = "DESede"; // 定义加密算法,可用 DES,DESede,Blowfish
	private String key = "xLyhSVcQGix62o+t2fjZyx+b";	//24位密钥

	public byte[] encrypt(byte[] key, byte[] src) throws Exception {
		SecretKey deskey = new SecretKeySpec(key, Algorithm);
		Cipher c1 = Cipher.getInstance(Algorithm);
		c1.init(Cipher.ENCRYPT_MODE, deskey);
		return c1.doFinal(src);
	}

	public byte[] decrypt(byte[] key, byte[] src) throws Exception {
		SecretKey deskey = new SecretKeySpec(key, Algorithm);
		Cipher c1 = Cipher.getInstance(Algorithm);
		c1.init(Cipher.DECRYPT_MODE, deskey);
		return c1.doFinal(src);
	}

	@Override
	public byte[] encrypt(byte[] src) {
		try {
			return encrypt(key.getBytes(), src);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte[] decrypt(byte[] src) {
		try {
			return decrypt(key.getBytes(), src);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public static void main(String[] args) throws Exception {
		// 添加新安全算法,如果用JCE就要把它添加进去
//		Security.addProvider(new com.sun.crypto.provider.SunJCE());
		DesEncryptor des = new DesEncryptor();
		
		byte[] keyBytes = "abcdefghijklmnopqrstuvwx".getBytes();//24位密钥
		byte[] encoded = des.encrypt(keyBytes, "This is a 3DES test. 测试".getBytes());
		System.out.println("加密后的字符串:" + new String(encoded));
		byte[] srcBytes = des.decrypt(keyBytes, encoded);
		System.out.println("解密后的字符串:" + (new String(srcBytes)));
	}
	
}
/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.web.ctrl.paipaisign;
import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.paipai.halo.service.paipaisign.PpappSignLogService;

/**
 *ppappSignLog controller层
 * @author J-ONE
 * @since 2015-04-09
 */
@Controller
@RequestMapping(value = "/ppappSignLog")
public class PpappSignLogController{
	public static Logger run = LogManager.getLogger("run");
	
	@Resource private PpappSignLogService ppappSignLogService;
	
//	/**
//	 * 测试查询签到天数
//	 */
//	@RequestMapping(value="/getSignDays", method = {RequestMethod.GET,RequestMethod.POST})
//	public void getSignDays(){
//		int signDays = this.ppappSignLogService.getSignDays(123456l, 1);
//		System.out.println(signDays);
//	}
}
/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.web.ctrl.paipaisign;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.service.paipaisign.PpappShareLogService;
import com.paipai.halo.web.JsonOutput;

/**
 *ppappShareLog controller层
 * @author J-ONE
 * @since 2015-04-09
 */
@Controller
@RequestMapping(value = "/ppappShareLog")
public class PpappShareLogController{
	public static Logger run = LogManager.getLogger("run");
	@Resource private PpappShareLogService ppappShareLogService;
	
	
	/**
	 * 分享送橡果
	 * http://127.0.0.1:8080/paipai-halo/ppappShareLog/share.action?activeId=1&type=1
	 * @param appToken
	 * @param activeId
	 * @param wid
	 * @param skey
	 * @param jsonp
	 * @param mk
	 * @param mk2
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/share", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String share(String appToken, int activeId, String wid, String skey, String callback, String mk, String mk2, int type, HttpServletRequest request){
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null == appTokenObj) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			} else {
				
				CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;//--------------
				JsonObject share = this.ppappShareLogService.share(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid), activeId, mk, mk2, request.getRemoteAddr(), type);
				
			    //测试：
//				JsonObject share = this.ppappShareLogService.share(123456L, activeId, mk, mk2, request.getRemoteAddr(), 1);
				
				out.setData(share);
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}

		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
	
}
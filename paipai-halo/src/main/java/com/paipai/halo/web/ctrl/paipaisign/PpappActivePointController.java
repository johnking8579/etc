/*
 * Copyright (c) 2015 www.jd.com All rights reserved.
 * 本软件源代码版权归京东成都云平台所有,未经许可不得任意复制与传播.
 */
package com.paipai.halo.web.ctrl.paipaisign;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jd.jim.cli.Cluster;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.JIMConstant;
import com.paipai.halo.common.exception.BusinessException;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.domain.UserBaseInfo;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.domain.paipaisign.PpappActivePoint;
import com.paipai.halo.domain.paipaisign.PpappSignLog;
import com.paipai.halo.service.ShopAroService;
import com.paipai.halo.service.paipaisign.PpappActivePointService;
import com.paipai.halo.service.paipaisign.PpappSignLogService;
import com.paipai.halo.web.JsonOutput;

/**
 *ppappActivePoint controller层
 * @author J-ONE
 * @since 2015-04-09
 */
@Controller
@RequestMapping(value = "/ppappActivePoint")
public class PpappActivePointController{
	
	public static Logger run = LogManager.getLogger("run");
	
	@Resource private PpappActivePointService ppappActivePointService;
	@Resource private ShopAroService shopAroService;
	@Resource private Cluster jimClient;
	@Resource private PpappSignLogService ppappSignLogService;
	
	/**
	 * 查询所有
	 * @return
	 */
	@RequestMapping(value="/selectAll", method = {RequestMethod.GET,RequestMethod.POST})
	public void selectAll(){
		List<PpappActivePoint> selectAll = this.ppappActivePointService.selectAll();
		System.out.println(new Gson().toJson(selectAll));
	}
	
	
	/**
	 * 添加
	 * @return
	 */
	@RequestMapping(value="/add", method = {RequestMethod.GET,RequestMethod.POST})
	public void add(){
		PpappActivePoint activePoint = new PpappActivePoint();
		activePoint.setId(UUID.randomUUID() + "_" + 9876543);
		activePoint.setActiveId(11);
		activePoint.setCurrentPoint(45678L);
		activePoint.setExt1("rfjkkfyvhbjks上线");
		activePoint.setUpdateTime(new Date().getTime());
		
		try {
			int i = this.ppappActivePointService.add(activePoint);
			System.out.println(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 根据条件查询
	 * @return
	 */
	@RequestMapping(value="/selectByParam", method = {RequestMethod.GET,RequestMethod.POST})
	public void selectByParam(){
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("wid", new BigInteger("9876543"));
			param.put("orderField", "activeId");
			List<PpappActivePoint> list = this.ppappActivePointService.selectParam(param);
			System.out.println(new Gson().toJson(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 判断活动是否有效及是否签过到接口
	 * 4.21修改：只要曾经签到过就不显示
	 * 192.168.146.103:8080/ ppappActivePoint/isShow.action?appToken=wtOwLs3k5d2DR5Vt9bJSPeQaA52JAn3aFMDGm00QaXrMFm4zs8%2BxKnZCAMLOWEUbKvNGmJp9eU8HhBtD3n%2BaKyr0bVlflOdhjMTBO2W6TLguGtC3D323HVtV2HuU7TiEiSxC6HJNjqpw4erqFjceHVPrAZS43uUWXiw%2Bcw4IIzBSHKVEYJWLbKvpoWW%2FBS0kD0NMISWl%2BXueMF174CKCizQRm9pCYZEV3OCgu4jIz6cKHxh5xHqCTcYKaN9pEi8TNWvQudHz21orVozZzdG4AP8HdhR1C%2FiqvKBvuny9DiyhM%3D&mk=aa&activeId=1
	 * @param appToken
	 * @param activeId
	 * @param wid
	 * @param skey
	 * @param jsonp
	 * @param mk
	 * @param mk2
	 * @return
	 */
	@RequestMapping(value="/isShow", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String isShow(String appToken, String activeId, String callback, String mk, String wid){
		JsonOutput out = new JsonOutput();
		JsonObject jsonObject = new JsonObject();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		try {
			
			//判断是否在有效期
			long now = Long.parseLong(format.format(new Date()));
			long startTimeL = Long.parseLong(Config.get("signActive." + activeId + ".startTime").substring(0, 8));
			long endTimeL = Long.parseLong(Config.get("signActive." + activeId + ".endTime").substring(0, 8));
			
			if(now < startTimeL){//活动还未开始
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_NOTBEGIN, "签到活动还未开始");
			}else if(now > endTimeL){//活动已结束
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_HASEND, "签到活动已结束");
			}else{
				
				if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
//					throw BusinessException.createInstance(
//							ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
					jsonObject.addProperty("isShow", 1);//没有appToken则表示没有登陆，一直显示
					out.setData(jsonObject);
				}else{
					AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
					
					System.out.println("appTokenObj:" + appTokenObj);
					
					if (null == appTokenObj) {//未登录一直显示
						jsonObject.addProperty("isShow", 1);
						out.setData(jsonObject);
					} else {
//						SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
						Map<String, Object> param = new HashMap<String, Object>();
						param.put("wid", null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid));
						param.put("activeId", activeId);
//						param.put("insertDay", format.format(new Date()));
						
						CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;//--------------------
						List<PpappSignLog> list = ppappSignLogService.getByParam(param);
						if(list.size() > 0){//已经领过了
							jsonObject.addProperty("isShow", 0);
						}else{
							jsonObject.addProperty("isShow", 1);
						}
						out.setData(jsonObject);
					}
				}
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}

		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
	
	/**
	 * 获取用户当前状态信息接口
	 * http://192.168.146.103:8080/ppappActivePoint/getUserState.action?activeId=1&appToken=wtDe6KxCqlhJGK80OViJiH6NOsb8BnRAG3f2kyC2ytmSSVvhV92t8%2FMnx7AVW5PpKPW0bBjuoe4WK5ehtI3%2B9QawWe8DQjEMNK9ZAHiXVuc9xMGdP99QaHxApYaZVI6%2F4ju%2FylHa%2FkjP%2F%2F1LG8lfVLCuToSBR%2B1aXJAt4fDloaafdx7fZMEBFth1%2BswVU%2FCbv5oPL09mhaNzx5LsdUs9m8kk3rwCgvrcZocTl5HIDVkmK2%2F1nThVvFRwia2UBe4najKDYe9dGdByOtcr9ze4%2Bm8qQzS1prnlTf7%2F0Iwt%2FZT0g%3D&mk=54A2D61B-4479-4EEE-BF5F-73D220A06AD5&wid=345433386
	 * @param appToken
	 * @param activeId
	 * @param wid
	 * @param skey
	 * @param jsonp
	 * @param mk
	 * @param mk2
	 * @return
	 */
	@RequestMapping(value="/getUserState", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getUserState(String appToken, int activeId, String wid, String skey, String callback, String mk, String mk2){
		
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null == appTokenObj) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			} else {
				JsonObject object = new JsonObject();
				
				CustomerContextHolder.setCustomerType(CustomerType.HALO);
				
				UserBaseInfo userInfo = shopAroService.getUserInfo(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid));
				if(null != userInfo){
					object.addProperty("userPic", userInfo.getHead());
					object.addProperty("nickName", userInfo.getNickname());
				}
					
				
				CustomerContextHolder.setCustomerType(CustomerType.SIGN);
				
				//查询当前拥有点数
				object.addProperty("pineNutNum", this.ppappActivePointService.selectSignPointCache(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid), activeId).getCurrentPoint());
				//查询连续登陆天数
				object.addProperty("signDays", this.ppappActivePointService.selectSignPointCache(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid), activeId).getSignDays());
				
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("wid", null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid));
				param.put("activeId", activeId);
				param.put("insertDay", format.format(new Date()));
				
				List<PpappSignLog> list = ppappSignLogService.getByParam(param);
				if(list.size() > 0){//已经签到过
					object.addProperty("todayIsSign", 1);
				}else{
					object.addProperty("todayIsSign", 0);
				}
				object.addProperty("activeId", activeId);
				
				out.setData(object);
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}

		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
	
	
	/**
	 * 签到送橡果
	 * http://127.0.0.1:8080/paipai-halo/ppappActivePoint/sign.action?activeId=1
	 * @param appToken
	 * @param activeId
	 * @param wid
	 * @param skey
	 * @param jsonp
	 * @param mk
	 * @param mk2
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/sign", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String sign(String appToken, int activeId, String wid, String skey, String callback, String mk, String mk2, HttpServletRequest request){
		JsonOutput out = new JsonOutput();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		try {
			
			long now = Long.parseLong(format.format(new Date()));
			long startTimeL = Long.parseLong(Config.get("signActive." + activeId + ".startTime").substring(0, 8));
			long endTimeL = Long.parseLong(Config.get("signActive." + activeId + ".endTime").substring(0, 8));
			
			System.out.println("now:"+now+";startTimeL:"+startTimeL+"；endTimeL:"+endTimeL);
			
			if(now < startTimeL){//活动结束
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_NOTBEGIN, "签到活动还未开始");
			}
			if(now > endTimeL){//活动结束
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_HASEND, "签到活动已结束");
			}
			
			if (StringUtils.isEmpty(appToken) || StringUtils.isEmpty(mk)) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验失败");
			}
			AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
			if (null == appTokenObj) {
				out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
				out.setMsg("用户未登录");
			} else {
				CustomerContextHolder.setCustomerType(CustomerType.SIGN) ;//--------------------
				
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("wid", null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid));
				param.put("activeId", activeId);
				param.put("insertDay", format.format(new Date()));
				List<PpappSignLog> list = this.ppappSignLogService.getByParam(param);
				
				if(list.size() > 0) {//今天已经签到过了
					out.setErrCode(ErrConstant.ERRCODE_SIGNED);
					out.setMsg("对不起，您今天已经签过到了！");
				}else {
					JsonObject sign = this.ppappActivePointService.sign(null != appTokenObj ? appTokenObj.getWid() : Long.parseLong(wid), activeId , mk, mk2, request.getRemoteAddr());
					out.setData(sign);
				}
			}
		} catch (BusinessException bex) {
			out.setErrCode(bex.getErrCode());
			out.setMsg(bex.getErrMsg());
		} catch (Exception ex) {
			ex.printStackTrace();
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
			out.setMsg("异常" + ex.toString());
		}

		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
	
	
	
	@RequestMapping(value="/deleteCacheTest", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String deleteCacheTest(String appToken, String mk, String callback) throws Exception{
		AppToken appTokenObj = LoginUtil.checkLoginForToken(appToken, mk);
		System.out.println("删除缓存key：" + JIMConstant.SIGN_POINT_CACHE + appTokenObj.getWid() + "_1");
		
		JsonOutput out = new JsonOutput();
		Long del = jimClient.del(JIMConstant.SIGN_POINT_CACHE + appTokenObj.getWid() + "_1");
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("result", del);
		out.setData(jsonObject);
		
		if(StringUtils.isNotBlank(callback)){
			return callback + "(" + out.toJsonStr() + ")";
		}else{
			return out.toJsonStr();
		}
	}
	
	
	/**
	 * http://192.168.146.103:8080/ppappActivePoint/getSignDaysTest.action?activeId=1&appToken=wtDe6KxCqlhJGK80OViJiH6NOsb8BnRAG3f2kyC2ytmSSVvhV92t8%2FMnx7AVW5PpKPW0bBjuoe4WK5ehtI3%2B9QawWe8DQjEMNK9ZAHiXVuc9xMGdP99QaHxApYaZVI6%2F4ju%2FylHa%2FkjP%2F%2F1LG8lfVLCuToSBR%2B1aXJAt4fDloaafdx7fZMEBFth1%2BswVU%2FCbv5oPL09mhaNzx5LsdUs9m8kk3rwCgvrcZocTl5HIDVkmK2%2F1nThVvFRwia2UBe4najKDYe9dGdByOtcr9ze4%2Bm8qQzS1prnlTf7%2F0Iwt%2FZT0g%3D&mk=54A2D61B-4479-4EEE-BF5F-73D220A06AD5&wid=345433386
	 * @param wid
	 * @param activeId
	 * @param mk
	 * @param mk2
	 * @param ip
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getSignDaysTest", produces = "text/html;charset=UTF-8")
	@ResponseBody
	public int getSignDaysTest(String appToken, long wid, int activeId, String mk, String mk2, String ip,int type) throws Exception {
		return this.ppappSignLogService.getSignDays(wid, activeId);
	}
}
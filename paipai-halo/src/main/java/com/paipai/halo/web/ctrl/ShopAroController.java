package com.paipai.halo.web.ctrl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.jmq.client.producer.MessageProducer;
import com.jd.jmq.common.exception.JMQException;
import com.jd.jmq.common.message.Message;
import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.paipai.component.configagent.Configs;
import com.paipai.halo.common.CustomerContextHolder;
import com.paipai.halo.common.CustomerType;
import com.paipai.halo.common.ErrConstant;
import com.paipai.halo.common.PageEntity;
import com.paipai.halo.common.PagingResult;
import com.paipai.halo.common.TimeUtils;
import com.paipai.halo.common.Util;
import com.paipai.halo.common.login.LoginUtil;
import com.paipai.halo.common.login.TokenUtil;
import com.paipai.halo.dirty.protocol.SenseWordCheck_UReq;
import com.paipai.halo.dirty.protocol.SenseWordCheck_UResp;
import com.paipai.halo.domain.Comment;
import com.paipai.halo.domain.CommentData;
import com.paipai.halo.domain.CurrentPage;
import com.paipai.halo.domain.Files;
import com.paipai.halo.domain.ItemLikes;
import com.paipai.halo.domain.JmqMsgInfo;
import com.paipai.halo.domain.LikeLogInfo;
import com.paipai.halo.domain.OutModelInfo;
import com.paipai.halo.domain.UserBaseInfo;
import com.paipai.halo.domain.login.AppToken;
import com.paipai.halo.service.ShopAroService;
import com.paipai.halo.web.JsonOutput;

@Controller
@RequestMapping("/shoparo")
public class ShopAroController {
	
	public static Logger run = LogManager.getLogger("run");
 
	@Resource
	private ShopAroService shopAroService;
	private static final String QQ_HEAD = "http://qlogo3.store.qq.com/qzone/";
	public static final Long QQ_LIMIT = 3900000000L;
	//是否进行严格审批
	public static final boolean IsStrict = false;
	
	////////////////以上为jim测试

    @Resource(name = "producer")
    private MessageProducer producer;
    @Value("#{configProperties['jmq.producer.topic.l']}")
    private String like_topic = "halo_like";
    @Value("#{configProperties['jmq.producer.topic.c']}")
    private String com_topic = "halo_comment";

	/**
	 * 该类用于点赞或者取消点赞
	 * 
	 * @return
	 */
	@RequestMapping(value = "/setlike")
	@ResponseBody
	public String setLike(HttpServletResponse resp, String itemid,
			Integer itemtype, Integer islike, Long ownerid, String nickname,
			String head, Integer usertype, Long wid, String skey,
			String appToken, String mk, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		String result = "";
		JsonOutput out = new JsonOutput();
		if (checkegLogin(appToken, mk, wid, skey)) {
			if (wid == null || "".equals(wid)) {
				try {
					AppToken Token = TokenUtil.getAppToken(appToken);
					wid = Token.getWid();
				} catch (Exception e) {
					e.printStackTrace();
					wid = null;
				}
			}
			// 检验参数是否为空
			if (itemid == null || "".equals(itemid) || wid == null
					|| "".equals(wid) || ownerid == null || "".equals(ownerid)
					|| itemtype == null || "".equals(itemtype)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				out.setMsg("参数不合法！");
				result = new Gson().toJson(out);
			} else {
				// 查出该人对于该文章是否点赞的状态
				LikeLogInfo l = this.shopAroService.getLikeinfo(itemid,
						itemtype, wid);
				// 判断是否是取消点赞
				if (l == null || l.getStatus() == 0) {
					run.info("用户点赞：" + wid + ",setLike--》》");
					try {
						result = this.shopAroService.setLike(itemid, itemtype,
								wid, ownerid);
					} catch (Exception e) {
						e.printStackTrace();
						out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
						out.setMsg("数据异常，点赞失败");
						result = new Gson().toJson(out);
					}
				} else if (l != null && l.getStatus() == 1) {
					run.info("用户取消点赞：" + wid + ",setLike--》》");
					// 如果是取消点赞，则减去被赞数
					try {
						result = this.shopAroService.cancelLike(itemid,
								itemtype, wid);
					} catch (Exception e) {
						e.printStackTrace();
						out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
						out.setMsg("数据异常，取消失败");
						result = new Gson().toJson(out);
					}
				}
				//发送MQ
				try {
					sendMq(itemid,itemtype,"1",out,"",((l==null||l.getStatus()==0)?"1":"0"),wid,TimeUtils.getCurrentTime());
				} catch (Exception e) {
					run.info("sendMq:error:   sendlike ");
					e.printStackTrace();
				}
				
			}

		} else {

			out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
			out.setMsg("请您登录");
			result = new Gson().toJson(out);
		}
	
		if (jsonp != null && !jsonp.equals("")) {
			result = "try{" + jsonp + "(" + result + ");}catch(e){}";
		}
		return doPrint(resp, result);
	}

	/**
	 * 
	 * @Title: getLikeNum
	 * @Description:返回点赞的个数
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getlikenum")
	@ResponseBody
	public String getLikeNum(HttpServletResponse resp, String itemid,
			Integer itemtype, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		if (itemid == null || "".equals(itemid) || itemtype == null
				|| "".equals(itemtype)) {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数不合法！");
			jsondata = new Gson().toJson(out);
		} else {
			try {
				ItemLikes itl = this.shopAroService
						.getLikeNum(itemid, itemtype);
				JsonObject jObject = new JsonObject();
				if (itl != null) {
					/*
					 * String data = "{\"likesnum\":\"" + itl.getLikesnum() +
					 * "\",\"commentnum\":\"" + itl.getCommentnum() + "\"}";
					 */
					jObject.addProperty("likesnum", itl.getLikesnum());
					jObject.addProperty("commentnum", itl.getCommentnum());
					out.setData(new Gson().toJsonTree(jObject));
					out.setErrCode(0);
					out.setMsg("success");
				} else {
					// String data =
					// "{\"likesnum\":\"0\",\"commentnum\":\"0\"}";
					jObject.addProperty("likesnum", 0);
					jObject.addProperty("commentnum", 0);
					out.setData(new Gson().toJsonTree(jObject));
					out.setErrCode(0);
					out.setMsg("未查到数据");
				}

			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询数据异常");
			}
		}
		jsondata = new Gson().toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 
	 * 获取用户头像
	 * 
	 * @Title: getUserInfo
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getUserInfo")
	@ResponseBody
	public String getUserInfo(HttpServletResponse resp, Long wid, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		Gson gn = new Gson();
		if (wid != null) {
			try {
				UserBaseInfo userBaseInfo = this.shopAroService
						.getUserInfo(wid);
				if (userBaseInfo != null) {
					// 如果是QQ
					if (userBaseInfo.getWid() < QQ_LIMIT
							&& (userBaseInfo.getHead() == null || ""
									.equals(userBaseInfo.getHead()))) {
						userBaseInfo.setHead(QQ_HEAD + userBaseInfo.getWid()
								+ "/" + userBaseInfo.getWid() + "/50");
					}
					out.setMsg("查询成功");
				} else {
					// 如果是QQ则直接设置默认头像
					if (wid < QQ_LIMIT) {
						userBaseInfo = new UserBaseInfo();
						userBaseInfo.setWid(wid);
						userBaseInfo.setUsertype(1);
						userBaseInfo.setHead(QQ_HEAD + userBaseInfo.getWid()
								+ "/" + userBaseInfo.getWid() + "/50");
						userBaseInfo.setNickname("");
					}
					out.setMsg("查询成功，但未找到该人");
				}
				out.setErrCode("0");
				out.setData(gn.toJsonTree(userBaseInfo));

			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询异常,数据连接出错");
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 更新用户头像
	 * 
	 * @Title: upUserInfo
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/upUserInfo")
	@ResponseBody
	public String upUserInfo(HttpServletResponse resp, Long wid,
			String nickname, String head, int usertype, String appToken,
			String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		Gson gn = new Gson();
		if (wid != null) {
			UserBaseInfo u = new UserBaseInfo();
			u.setWid(wid);
			/*
			 * String h = ""; try { h = URLDecoder.decode(head,"utf-8"); } catch
			 * (UnsupportedEncodingException e1) { e1.printStackTrace(); }
			 */
			
			u.setHead(head!=null?head.replaceAll("&amp;", "&"):"");
			u.setHeadhash(u.getHead().hashCode() + "");
			u.setNickname(nickname);
			u.setUsertype(usertype);
			try {
				this.shopAroService.saveorupdate(u);
				UserBaseInfo newu = this.shopAroService.getUserInfo(wid);
				out.setErrCode("0");
				out.setMsg("更新成功");
				out.setData(gn.toJsonTree(newu));
			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("您和组织断开连接了");
			}

		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 
	 * 获取列表
	 * 
	 * @Title: getCommentList
	 * @Description: TODO
	 * @param @param resp
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getCoomentList")
	@ResponseBody
	public String getCommentList(
			HttpServletResponse resp,
			String itemid,
			Integer itemtype,
			Long wid,
			@RequestParam(value = "pagenum", defaultValue = "1") int pagenum,
			@RequestParam(value = "pagesize", defaultValue = "20") int pagesize,
			String appToken, String jsonp,
			@RequestParam(value = "lastid", defaultValue = "0") int lastid) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		Gson gn = new Gson();
		String jsondata = "";
		if (pagenum > 0 && pagesize > 0 && itemid != null && !"".equals(itemid)) {
			PageEntity pe = new PageEntity();
			pe.setPage(pagenum);
			pe.setSize(pagesize);
			pe.setOrderColumn("commenttime");
			pe.setOrderTurn("desc");
			Map params = new HashMap<String, String>();
			params.put("itemid", itemid);
			params.put("itemtype", itemtype);
			params.put("id", lastid);
			pe.setParams(params);
			PagingResult<Comment> pageRanges = null;
			try {
				if (lastid == 0 && pagesize == 20) {// 如果是首页则先去缓存中去取
					pageRanges = this.shopAroService.getPageRangeByCache(
							itemid, itemtype);
					out.setMsg("缓存中获取成功");
					if (pageRanges == null) {
						pageRanges = this.shopAroService.getPgaRanges(pe);
						out.setMsg("成功");
					}
				} else {
					pageRanges = this.shopAroService.getPgaRanges(pe);
					out.setMsg("成功");
				}

				CurrentPage<Comment> commentdate = new CurrentPage<Comment>();
				if (pageRanges != null) {
					commentdate.setPageItems(pageRanges.getResultList());
					commentdate.setPagesize(pagesize);
					commentdate.setTotalcount(pageRanges.getTotalSize());
					commentdate
							.setTotalnum(pageRanges.getTotalSize() % pagesize == 0 ? (pageRanges
									.getTotalSize() / pagesize) : (pageRanges
									.getTotalSize() / pagesize + 1));
					if (commentdate.getTotalnum() < pagenum) {
						commentdate.setPagenum(commentdate.getTotalnum());
					} else {
						commentdate.setPagenum(pagenum);
					}
					// 为用户信息复制
					UserBaseInfo ub = new UserBaseInfo();
					List<Comment> comments = pageRanges.getResultList();
					if (comments != null) {
						for (int i = 0; i < comments.size(); i++) {
							ub = this.shopAroService.getUserInfo(comments
									.get(i).getWid());
							if (ub != null) {
								comments.get(i).setNickname(
										ub.getNickname() == null ? "" : ub
												.getNickname());
								// 如果是QQ则默认
								if (ub.getWid() < QQ_LIMIT
										&& (ub.getHead() == null || ""
												.equals(ub.getHead()))) {
									comments.get(i).setHead(
											QQ_HEAD + ub.getWid() + "/"
													+ ub.getWid() + "/50");
								} else {
									comments.get(i).setHead(
											ub.getHead() == null ? "" : ub
													.getHead());
								}

							} else {
								// 如果是QQ则默认
								if (comments.get(i).getWid() < QQ_LIMIT) {
									comments.get(i).setHead(
											QQ_HEAD + comments.get(i).getWid()
													+ "/"
													+ comments.get(i).getWid()
													+ "/50");
								} else {
									comments.get(i).setHead("");
								}
								comments.get(i).setNickname("");

							}
						}
					}

				}

				// 查出该文章是的点赞评论数
				ItemLikes itLikes = this.shopAroService.getLikeNum(itemid,
						itemtype);
				if (itLikes != null) {
					commentdate.setLikesnum(itLikes.getLikesnum());
					commentdate.setCommentnum(itLikes.getCommentnum());
				}
				// 查出该人是否已经点赞了
				LikeLogInfo lg = this.shopAroService.getLikeinfo(itemid,
						itemtype, wid);
				if (lg != null) {
					commentdate.setIslike(lg.getStatus());// 1:点赞了0：没有
				}
				commentdate.setCurtime(TimeUtils.getCurrentTime());
				out.setErrCode("0");
				out.setData(gn.toJsonTree(commentdate));
			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询异常");
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数有误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 
	 * 获取点赞人员的相关信息
	 * 
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getLikeList")
	@ResponseBody
	public String getLikeList(
			HttpServletResponse resp,
			String itemid,
			Integer itemtype,
			Long wid,
			@RequestParam(value = "pagenum", defaultValue = "1") int pagenum,
			@RequestParam(value = "pagesize", defaultValue = "10") int pagesize,
			String appToken, String jsonp,
			@RequestParam(value = "lastid", defaultValue = "0") int lastid) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;

		JsonOutput out = new JsonOutput();
		Gson gn = new Gson();
		String jsondata = "";
		if (itemid != null && !"".equals(itemid) && pagenum > 0 && pagesize > 0) {
			PagingResult<LikeLogInfo> pageRanges = null;
			PageEntity pe = new PageEntity();
			pe.setPage(pagenum);
			pe.setSize(pagesize);
			pe.setOrderColumn("liketime");
			pe.setOrderTurn("desc");
			Map params = new HashMap<String, String>();
			params.put("itemid", itemid);
			params.put("itemtype", itemtype);
			params.put("status", 1);
			params.put("id", lastid);
			pe.setParams(params);
			CurrentPage<LikeLogInfo> likeCurrentPage = new CurrentPage<LikeLogInfo>();
			try {
				// 如果是请求首先则先查一下缓存中是否已经有了
				if (lastid == 0 && pagesize == 10) {
					pageRanges = this.shopAroService.getUserBaseByCache(itemid,
							itemtype);
					if (pageRanges == null) {
						pageRanges = this.shopAroService.getLikePgaRanges(pe);
					}
					out.setErrCode("0");
					out.setMsg("从缓存中查出成功");
				} else {
					pageRanges = this.shopAroService.getLikePgaRanges(pe);
				}
				if (pageRanges == null) {
					out.setErrCode("10");
					out.setMsg("暂时没有人点赞");
				} else {
					List<LikeLogInfo> likeLogInfos = pageRanges.getResultList();
					likeCurrentPage.setPageItems(likeLogInfos);
					likeCurrentPage.setPagenum(pagenum);
					likeCurrentPage.setPagesize(pagesize);
					likeCurrentPage.setTotalcount(pageRanges.getTotalSize());
					likeCurrentPage
							.setTotalnum(pageRanges.getTotalSize() % pagesize == 0 ? (pageRanges
									.getTotalSize() / pagesize) : (pageRanges
									.getTotalSize() / pagesize + 1));
					UserBaseInfo ub = new UserBaseInfo();
					// 为用户信息复制
					for (int i = 0; i < likeLogInfos.size(); i++) {
						ub = this.shopAroService.getUserInfo(likeLogInfos
								.get(i).getWid());
						if (ub != null) {
							likeLogInfos.get(i).setNickname(
									ub.getNickname() == null ? "" : ub
											.getNickname());
							// 如果是QQ则默认
							if (ub.getWid() < QQ_LIMIT
									&& (ub.getHead() == null || "".equals(ub
											.getHead()))) {
								likeLogInfos.get(i).setHead(
										QQ_HEAD + ub.getWid() + "/"
												+ ub.getWid() + "/50");
							} else {
								likeLogInfos.get(i).setHead(
										ub.getHead() == null ? "" : ub
												.getHead());
							}
						} else {
							likeLogInfos.get(i).setNickname("");
							// 如果是QQ则默认
							if (likeLogInfos.get(i).getWid() < QQ_LIMIT) {
								likeLogInfos.get(i).setHead(
										QQ_HEAD + likeLogInfos.get(i).getWid()
												+ "/"
												+ likeLogInfos.get(i).getWid()
												+ "/50");
							} else {
								likeLogInfos.get(i).setHead("");
							}
						}
					}
					// 查出该文章是的点赞评论数
					ItemLikes itLikes = this.shopAroService.getLikeNum(itemid,
							itemtype);
					if (itLikes != null) {
						likeCurrentPage.setLikesnum(itLikes.getLikesnum());
						likeCurrentPage.setCommentnum(itLikes.getCommentnum());
					}
					// 查出该人是否已经点赞了
					LikeLogInfo lg = this.shopAroService.getLikeinfo(itemid,
							itemtype, wid);
					if (lg != null) {
						likeCurrentPage.setIslike(lg.getStatus());// 1:点赞了0：没有
					}
					likeCurrentPage.setCurtime(TimeUtils.getCurrentTime());
				}
				out.setErrCode("0");
				out.setMsg("成功");
				out.setData(gn.toJsonTree(likeCurrentPage));
			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询异常");
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数为误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);

	}

	/**
	 * 
	 * 发表评论
	 * 
	 * @Title: saveComment
	 * @param @param resp
	 * @param 被点赞的事物id
	 *            （评论为评论ID，商品为商品ID等） itemid
	 * @param 事物的类型
	 *            （如：1：App，2：为丁丁组调用） itemtype
	 * @param 客户请求ip
	 *            （app端可不传） ip
	 * @param 评论标题
	 *            title
	 * @param 评论内容
	 *            context
	 * @param 评论人员昵称
	 *            nickname
	 * @param 评论人员头像
	 *            head
	 * @param usertype
	 *            评论人员类型 QQ用户：1微信用户：2京东用户：3其它：0
	 * @param wid
	 * @param @param ownerid 被评论的发文所有者id
	 * @param @param skey
	 * @param @param appToken
	 * @param @param jsonp
	 * @throws
	 */
	@RequestMapping(value = "/saveComment")
	@ResponseBody
	public String saveComment(HttpServletResponse resp,
			HttpServletRequest request, String itemid, Integer itemtype,
			String ip, String title, String context, String nickname,
			String head, Integer usertype, Long wid, Long ownerid, String mk,
			String skey, String appToken, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		String result = "";
		JsonOutput out = new JsonOutput();
		if (itemid == null || itemid.equals("") || itemtype == null
				|| "".equals(itemtype) || wid == null || "".equals(wid)
				|| ownerid == null || "".equals(ownerid)) {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数有误");
			result = new Gson().toJson(out);
		} else {
			if (context != null && context.length() > 140) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
				out.setMsg("输入字段过长");
				result = new Gson().toJson(out);
			} else {
				if (ip == null || "".equals(ip)) {
					ip = getRemoteIp(request);
				}
				//1时直接放行0:时审批后放行
				int isshow = 0 ; 
				//查出评论人的相关信息
				UserBaseInfo u = this.shopAroService.getUserInfo(wid);
				//如果进行严格的审批则只有白名单可以直接放行
				if(IsStrict){
					if(u!=null&&u.getStatus()==2){
						isshow = 1;
					}else {
						isshow = 0;
					}
				}else {//如果非严格则只有黑名单需要审批
					if(u!=null&&u.getStatus()==1){
						isshow = 0;
					}else {
						isshow = 1;
					}
				}
				
			  if (checkegLogin(appToken, mk, wid, skey)) {
						// 如果wid是空了，则试着去appToken里去找一下
						if (wid == null || "".equals(wid)) {
							try {
								AppToken Token = TokenUtil
										.getAppToken(appToken);
								wid = Token.getWid();
							} catch (Exception e) {
								e.printStackTrace();
								wid = null;
							}
						}
						try {
							
								result = this.shopAroService.saveConmment(itemid,
										itemtype, wid, ip, title, context, ownerid,isshow);
						} catch (Exception e) {
							e.printStackTrace();
							out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
							out.setMsg("发表异常");
							result = new Gson().toJson(out);
						}
						//发送mq
						if(isshow==1){
							try {
								//发mq前发判断是否发送成功
								JsonParser jp = new JsonParser();
								JsonElement je = jp.parse(result);
								String errcode = je.getAsJsonObject().get("errCode").getAsString();
								//如果为频率评论则不发送
								if(!"5394".equals(errcode)){
									sendMq(itemid,itemtype,"2",out,context,"1",wid,TimeUtils.getCurrentTime());
								}
							} catch (Exception e) {
								run.info("sendMq:error:   sendcomment ");
								e.printStackTrace();
							}
							
						}
					} else {
						out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
						out.setMsg("请您登录");
						result = new Gson().toJson(out);
					}

			}

		}

		if (jsonp != null && !jsonp.equals("")) {
			result = "try{" + jsonp + "(" + result + ");}catch(e){}";
		}
		return doPrint(resp, result);
	}

	/**
	 * 获取批量用户信息
	 * 
	 * @Title: getUserList
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getUList")
	@ResponseBody
	public String getUserList(HttpServletResponse resp, String wids,
			String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		Gson gn = new Gson();
		if (wids != null) {
			try {
				String[] wid = wids.split(",");
				Long[] widl = new Long[wid.length];
				for (int i = 0; i < wid.length; i++) {
					widl[i] = (wid[i] == null ? 0L : Long.valueOf(wid[i]));
				}
				List<UserBaseInfo> userBaseInfos = this.shopAroService
						.getUserList(widl);
				out.setData(new Gson().toJsonTree(userBaseInfos));
				out.setErrCode(0);
				out.setMsg("成功");
			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询异常,数据连接出错");
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 获取批量文章的评论
	 * 
	 * @Title: getUserList
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getCommneListinfo")
	@ResponseBody
	public String getCommneListinfo(HttpServletResponse resp, String itemids,
			int itemtype, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		Gson gn = new Gson();

		PageEntity pe = new PageEntity();
		pe.setPage(1);
		pe.setSize(3);
		pe.setOrderColumn("commenttime");
		pe.setOrderTurn("desc");
		Map params = new HashMap<String, String>();
		CommentData commentData = null;
		PagingResult<Comment> pageRanges = null;
		ItemLikes itemLikes = null;
		JsonObject jsonObject = new JsonObject();
		boolean iserror = false;
		if (itemids != null) {
			try {
				String[] itemid = itemids.split(",");
				for (int i = 0; i < itemid.length; i++) {
					if(itemid[i].split("\\|").length!=2){
						iserror= true;
						break;
					}
					String newid = itemid[i].split("\\|")[0];
					String ownerid = itemid[i].split("\\|")[1];
					commentData = new CommentData();
					params = new HashMap<String, String>();
					params.put("itemid", newid);
					params.put("id", 0);
					params.put("itemtype", itemtype);
					pe.setParams(params);
					pageRanges = this.shopAroService.getPageRangeByCache(
							newid, itemtype);
					if (pageRanges == null) {
						pageRanges = this.shopAroService.getPgaRanges(pe);
						out.setMsg("成功");
					}
					if (pageRanges != null) {
						// 循环查出该评论人
						List<Comment> comments = pageRanges.getResultList();
						List<Comment> nec = new ArrayList<Comment>();
						int endsize = comments.size()>3?3:comments.size();
						// 查出用户头像来
						for (int j = 0; j < endsize; j++) {
							// 查出用户信息
							UserBaseInfo u = this.shopAroService
									.getUserInfo(comments.get(j).getWid());
							if (u != null) {
								comments.get(j).setNickname(
										u.getNickname() == null ? "" : u
												.getNickname());
								if (u.getWid() < QQ_LIMIT
										&& (u.getHead() == null || "".equals(u
												.getHead()))) {
									comments.get(j).setHead(
											QQ_HEAD + u.getWid() + "/"
													+ u.getWid() + "/50");
								} else {
									comments.get(j).setHead(
											u.getHead() == null ? "" : u
													.getHead());
								}
							} else {
								comments.get(j).setNickname("");
								if (comments.get(j).getWid() < QQ_LIMIT) {
									comments.get(j).setHead(
											QQ_HEAD + comments.get(j).getWid()
													+ "/"
													+ comments.get(j).getWid()
													+ "/50");
								} else {
									comments.get(j).setHead("");
								}
							}
							nec.add(comments.get(j));
						}
						pageRanges.setResultList(nec);
						commentData.setComment(pageRanges.getResultList());
					}
					// 查出该itemid多少点赞以及评论数
					itemLikes = new ItemLikes();
					itemLikes = this.shopAroService.getLikeNum(newid, itemtype);
					// 如果能查到则说明评论过了，则直接从发文中取出
					if (itemLikes != null) {
						commentData.setCommnetnum(itemLikes.getCommentnum());
						commentData.setLikenum(itemLikes.getLikesnum());
						UserBaseInfo u = this.shopAroService
								.getUserInfo(itemLikes.getOwnerid());
						if (u != null) {
							commentData
									.setOwner_nickname(u.getNickname() == null ? ""
											: u.getNickname());
							if (u.getWid() < QQ_LIMIT
									&& (u.getHead() == null || "".equals(u
											.getHead()))) {
								commentData.setOwner_head(QQ_HEAD + u.getWid()
										+ "/" + u.getWid() + "/50");
							} else {
								commentData
										.setOwner_head(u.getHead() == null ? ""
												: u.getHead());
							}
							commentData.setOwnerid(u.getWid());
						} else {
							if (itemLikes.getOwnerid() < QQ_LIMIT) {
								commentData.setOwner_head(QQ_HEAD
										+ itemLikes.getOwnerid() + "/"
										+ itemLikes.getOwnerid() + "/50");
							} else {
								commentData.setOwner_head("");
							}
							commentData.setOwner_nickname("");
							commentData.setOwnerid(itemLikes.getOwnerid());
						}
					} else {
						// 如果没有评论过，则直接从传过来的ownerid中查
						Long userid = Long.valueOf(ownerid == null ? "0"
								: ownerid);
						commentData.setCommnetnum(0);
						commentData.setLikenum(0);
						UserBaseInfo u = this.shopAroService
								.getUserInfo(userid);
						if (u != null) {
							commentData
									.setOwner_nickname(u.getNickname() == null ? ""
											: u.getNickname());
							if (u.getWid() < QQ_LIMIT
									&& (u.getHead() == null || "".equals(u
											.getHead()))) {
								commentData.setOwner_head(QQ_HEAD + u.getWid()
										+ "/" + u.getWid() + "/50");
							} else {
								commentData
										.setOwner_head(u.getHead() == null ? ""
												: u.getHead());
							}
							commentData.setOwnerid(u.getWid());
						} else {
							if (userid < QQ_LIMIT) {
								commentData.setOwner_head(QQ_HEAD + userid
										+ "/" + userid + "/50");
							} else {
								commentData.setOwner_head("");
							}
							commentData.setOwner_nickname("");
							commentData.setOwnerid(userid);
						}
					}
					List<UserBaseInfo> userList= new  ArrayList<UserBaseInfo>();
					//查出点赞头像
					try {
						PagingResult<LikeLogInfo> likepage = this.shopAroService.getUserBaseByCache(newid,
								itemtype);
						if(likepage!=null){
							List<LikeLogInfo> ll = likepage.getResultList();
							//查出该
							for(LikeLogInfo l:ll){
								UserBaseInfo u = this.shopAroService.getUserInfo(l.getWid());
								if(u!=null){
									userList.add(u);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					commentData.setLikeUsers(userList);
					
					jsonObject.add(newid, gn.toJsonTree(commentData));

				}
				jsonObject.addProperty("curtime", TimeUtils.getCurrentTime()
						+ "");
				
				if(iserror){
					out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
					out.setMsg("参数错误");
				}else {
					out.setErrCode("0");
					out.setMsg("查询成功");
				}
				out.setData(jsonObject);

			} catch (Exception e) {
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP);
				out.setMsg("查询异常,数据连接出错");
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * 
	 * 批量获取items的评论点赞数
	 * 
	 * @Description: TODO
	 * @param @param resp
	 * @param @param itemids
	 * @param @param jsonp
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/getItemsLikeInfo")
	@ResponseBody
	public String getItemsInfo(HttpServletResponse resp, String itemids,
			Integer itemtype,Long wid, String jsonp) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		String jsondata = "";
		Gson gn = new Gson();
		if (itemtype != null && !"".equals(itemtype) && itemids != null
				&& !"".equals(itemids)&&!"".equals(wid)) {
			String[] itemid = itemids.split(",");
			OutModelInfo<ItemLikes> outModelInfo = new OutModelInfo<ItemLikes>();
			ItemLikes itl = null;
			JsonObject json = new JsonObject();
			if (itemid != null && itemid.length > 0) {
				for (String id : itemid) {
					itl = this.shopAroService.getItemLike(id, itemtype);
					if (itl != null) {
						outModelInfo.setDatainfo(itl);
					}else {
						outModelInfo.setDatainfo(new ItemLikes());
					}
					LikeLogInfo likeLogInfo = this.shopAroService.getLikeinfo(id, itemtype, wid);
					List<Files> files = new ArrayList<Files>();
					if(likeLogInfo!=null){
						Files f = new Files();
						f.setName("islike");
						f.setValues(likeLogInfo.getStatus()+"");
						files.add(f);
					}
					outModelInfo.setFiles(files);
					json.add(id, gn.toJsonTree(outModelInfo));
				}
				
				out.setErrCode("0");
				out.setMsg("查询成功");
				out.setData(json);
			}
		} else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		jsondata = gn.toJson(out);
		if (jsonp != null && !jsonp.equals("")) {
			jsondata = "try{" + jsonp + "(" + jsondata + ");}catch(e){}";
		}
		return doPrint(resp, jsondata);
	}

	/**
	 * @throws Exception
	 *             获取批量用户信息
	 * 
	 * @Title: getUserList
	 * @Description: TODO
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	@RequestMapping(value = "/lhn")
	public void test(HttpServletResponse resp,String context) throws Exception {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		this.shopAroService.lhntest(context);
	}


	public boolean checkegLogin(String appToken, String mk, Long wid,
			String skey) {
		try {
			if (appToken != null && !"".equals(appToken) && mk != null
					&& !"".equals(mk)) {
				if (LoginUtil.checkLoginForApp(appToken, mk)) {
					return true;
				} else {
					return false;
				}
			} else {
				if (wid != null && !"".equals(wid) && skey != null
						&& !"".equals(skey)) {

					if (LoginUtil.checkLoginForH5(wid, skey)) {
						return true;
					} else {
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 使用servlet方式, 向response写入字符串
	 * 
	 * @param obj
	 * @return
	 */

	protected String doPrint(HttpServletResponse resp, String result) {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter pw = null;
		try {
			pw = resp.getWriter();
			pw.print(result);// 不要使用println,防止客户端接收到/r/n
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			pw.close();
		}
		return null;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println(URLDecoder.decode("中国人", "utf-8") + ","
				+ URLEncoder.encode("中国人", "utf-8"));
		System.out.println(Integer.MAX_VALUE);
		System.out.println(TimeUtils.getCurrentTime());
		System.out
				.println("http://q2.qlogo.cn/g?b=qq&amp;k=l6cNCibxH3LJV0ibNMicObxwA&amp;s=140&amp;t=1374117537"
						.replaceAll("&amp;", "&"));
		List<Long> uids = new ArrayList<Long>();
		System.out.println(Collections.frequency(uids,345433386L));
		
	}

	@RequestMapping(value = "/getb")
	@ResponseBody
	public List<Long> getBlackList() {
		String s = this.shopAroService.getBlackList();
		List<Long> blackwid = null;
		if (s != null) {
			String[] ss = s.split(",");
			blackwid = Util.toLongList(ss);
		}
		run.info(blackwid);
		return blackwid;
	}

	@RequestMapping(value = "/addbl")
	@ResponseBody
	public String addBlack(HttpServletResponse resp,String  id) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		if(id!=null&&!id.equals("")){
			String [] i = id.split(",");
			List<Long> ids = Util.toLongList(id.split(","));
			List<Comment> item = this.shopAroService.getCommentList(ids);
			//将用户加入黑名单
			List<Long> uids = new ArrayList<Long>();
			for(Comment c:item){
				//如果里面没有则将保存
				if(Collections.frequency(uids,c.getWid())==0){
					uids.add(c.getWid());
				}
			}
			//更新黑名单 同时删除该人的所有留言并前更新评论数据 量
			try {
				this.shopAroService.updateUserBl(uids,ids,1);
				out.setErrCode(0);
				out.setMsg("success");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		
		//将传入的wid加入黑名单缓存。临时解决方式去掉
		//String blackwid = this.shopAroService.addBloack(wid);
		return doPrint(resp, new Gson().toJson(out));
	}
	
	@RequestMapping(value = "/upUcache")
	@ResponseBody
	public String upUCache(HttpServletResponse resp,String  wid) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		if(wid!=null&&!wid.equals("")){
			String [] i = wid.split(",");
			List<Long> ids = Util.toLongList(wid.split(","));
			//批量的更新一下wid的用户缓存
			try {
				this.shopAroService.updateUserBl(ids, null,1);
				List<UserBaseInfo> ub = new ArrayList<UserBaseInfo>();
				for(Long uid :ids){
					ub.add(this.shopAroService.getUserInfo(uid));
				}
				out.setData(new Gson().toJsonTree(ub));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		
		//将传入的wid加入黑名单缓存。临时解决方式去掉
		//String blackwid = this.shopAroService.addBloack(wid);
		return doPrint(resp, new Gson().toJson(out));
	}
	@RequestMapping(value = "/delb")
	@ResponseBody
	public String delBl(HttpServletResponse resp,String  wid) {
		CustomerContextHolder.setCustomerType(CustomerType.HALO) ;
		JsonOutput out = new JsonOutput();
		if(wid!=null&&!wid.equals("")){
			String [] i = wid.split(",");
			List<Long> uids = Util.toLongList(wid.split(","));
			//批量的更新一下wid的用户缓存
			try {
				this.shopAroService.updateUserBl(uids, null,0);
				List<UserBaseInfo> ub = new ArrayList<UserBaseInfo>();
				for(Long uid :uids){
					ub.add(this.shopAroService.getUserInfo(uid));
				}
				out.setData(new Gson().toJsonTree(ub));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			out.setMsg("参数错误");
		}
		
		//将传入的wid加入黑名单缓存。临时解决方式去掉
		//String blackwid = this.shopAroService.addBloack(wid);
		return doPrint(resp, new Gson().toJson(out));
	}
	
	@RequestMapping(value = "/testdirty")
	@ResponseBody
	public String  testdirty(String context){
		AsynWebStub webStub = new AsynWebStub();
		webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
		webStub.setClientIP(11111111L);
		webStub.setStringEncodecharset("GBK");
		webStub.setStringDecodecharset("GBK");
		webStub.setMachineKey("MK".getBytes());
		webStub.setUin(345433386);
		webStub.setOperator(345433386);
		webStub.setRouteKey(345433386);
		webStub.setDomainId(126);
		SenseWordCheck_UReq req  = new SenseWordCheck_UReq();
		req.setControl(1L);
		req.setInText((context==null||"".equals(context))?"三级片黄色":context);
		req.setIp("1.1.1.1.1");
		req.setLevel(0);
		req.setSource("APP");
		req.setSenceId(1);
		SenseWordCheck_UResp resp= new SenseWordCheck_UResp();
		try {
			webStub.invoke(req, resp);
			run.info("testdirty=-------------");
			run.info(new Gson().toJson(resp));
		} catch (AsynWebStubException e) {
			e.printStackTrace();
		}finally{
			
			run.info("testdirty=-------------end");
		}
		return new Gson().toJson(resp);
	}
	protected String getRemoteIp(HttpServletRequest request) {
		if (request == null) {
			return null;
		}
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (StringUtils.isNotBlank(ip) && ip.contains(",")) {
			ip = ip.substring(0, ip.indexOf(","));
		}
		if (ip != null) {
			return ip.split(":")[0];
		}
		return null;
	}
	/**
	 * 
	  * @Title: 发送MQ信息
	  * @Description: 发送文章的评论以及点赞数
	  * @param @param itemid 文章ID
	  * @param @param itemtype    设定文件
	  * @return void    返回类型
	  * @throws
	 */
	public void sendMq(String itemid,int itemtype,String type,JsonOutput out,String context,String islike,Long wid,long time){
		//查出点赞的信息发一个Mq type:1:占先2：评论
		ItemLikes i = this.shopAroService.getItemLike(itemid, itemtype);
		if(i!=null){
				JmqMsgInfo jmqMsgInfo = new JmqMsgInfo();
				jmqMsgInfo.setBizData(i);
				jmqMsgInfo.setBizType(type);
				jmqMsgInfo.setContext(context==null?"":context);
				jmqMsgInfo.setIslike(islike);
				jmqMsgInfo.setWid(wid);
				jmqMsgInfo.setTime(time);
				out.setData(new Gson().toJsonTree(jmqMsgInfo));
				String topic = type.equals("1")?like_topic:com_topic;
				Message message = new Message(topic, new Gson().toJson(out), topic+itemid);
		        try {
					producer.send(message);
				} catch (JMQException e) {
					run.info("发送mq失败：------------------------itemid:"+itemid+",itemtype:"+itemtype);
					e.printStackTrace();
				}
		}
	}

}

package com.jd.image.common;

import java.io.File;
import java.util.List;

import com.jd.image.common.utils.HttpClinteUtils;
import com.paipai.halo.common.Config;
import com.paipai.halo.common.Util;

public class ImageUpload
{
	public static String getAucoode(){
		if(Config.isIdcEnv()){
			return "c4ccbdf1092710454808fdd01cf7612a";
		}else{
			return "d3a313cb0d444049260ab6c0a6535f3c";
		}
	}
	public static String getUrl(){
		if(Config.isIdcEnv()){
			String[] domainName = new String [] {
					"http://img10.360buyimg.com/paipai/", 
					"http://img11.360buyimg.com/paipai/", 
					"http://img12.360buyimg.com/paipai/", 
					"http://img13.360buyimg.com/paipai/", 
					"http://img14.360buyimg.com/paipai/", 
					"http://img20.360buyimg.com/paipai/", 
					"http://img30.360buyimg.com/paipai/"};
				return Util.random(domainName);
		}else{
			return "http://img30.360buyimg.com/test/";
		}
	}
	
  public static String uploadFile(List<File> paramList, String paramString)
    throws Exception
  {
    String str = HttpClinteUtils.fileUpload(paramList, paramString);
    return str;
  }

  public static String uploadFile(File paramFile, String paramString)
    throws Exception
  {
    String str = HttpClinteUtils.fileUpload(paramFile, paramString);
    return str;
  }

  public static String uploadFile(byte[] paramArrayOfByte, String paramString)
    throws Exception
  {
    String str = HttpClinteUtils.fileUpload(paramArrayOfByte, paramString);
    return str;
  }

  public static String commonUpload(List<File> paramList, String paramString)
    throws Exception
  {
    String str = HttpClinteUtils.commonUpload(paramList, paramString);
    return str;
  }
}

package com.jd.image.common.utils;

import java.util.List;

import com.google.gson.GsonBuilder;
import com.jd.image.common.Message;

public class JsonUtils {
	public static String getMsgStr(String paramString1, String paramString2) {
		Message m = new Message(paramString1, paramString2);
		return toJson(m);
	}

	public static String getMsgStr(String paramString1, String paramString2, String paramString3) {
		Message m = new Message(paramString1, paramString2, paramString3);
		return toJson(m);
	}

	public static String getMsgStr(List<Message> paramList) {
		return toJson(paramList);
	}
	
	private static String toJson(Object o)	{
		return new GsonBuilder().serializeNulls().create().toJson(o);
	}
}

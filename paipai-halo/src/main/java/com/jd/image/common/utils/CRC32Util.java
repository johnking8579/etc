/*    */ package com.jd.image.common.utils;
/*    */ 
/*    */ import java.io.BufferedInputStream;
/*    */ import java.io.InputStream;
/*    */ import java.util.zip.CRC32;
/*    */ import java.util.zip.CheckedInputStream;
/*    */ 
/*    */ public class CRC32Util
/*    */ {
/*    */   public static String getFileCRCCode(InputStream paramInputStream)
/*    */   {
/* 16 */     CRC32 localCRC32 = new CRC32();
/*    */     try {
/* 18 */       BufferedInputStream localBufferedInputStream = new BufferedInputStream(paramInputStream);
/* 19 */       CheckedInputStream localCheckedInputStream = 
/* 20 */         new CheckedInputStream(localBufferedInputStream, localCRC32);
/* 21 */       while (localCheckedInputStream.read() != -1);
/*    */     }
/*    */     catch (Exception localException) {
/* 26 */       localException.printStackTrace();
/*    */     }
/* 28 */     return Long.toHexString(localCRC32.getValue());
/*    */   }
/*    */ }

/* Location:           D:\mavenrepo\image-common\image-common\1.0.0\
 * Qualified Name:     com.jd.image.common.utils.CRC32Util
 * JD-Core Version:    0.6.2
 */
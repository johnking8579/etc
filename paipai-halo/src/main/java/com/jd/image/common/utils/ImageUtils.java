/*    */ package com.jd.image.common.utils;
/*    */ 
/*    */ import java.io.BufferedInputStream;
/*    */ import java.io.File;
/*    */ import java.io.FileInputStream;
/*    */ import java.io.FileNotFoundException;
/*    */ import java.io.IOException;
/*    */ import java.util.Iterator;
/*    */ import java.util.List;
/*    */ import org.apache.commons.io.IOUtils;
/*    */ 
/*    */ public class ImageUtils
/*    */ {
/*    */   public static byte[] fileToByte(File paramFile)
/*    */   {
/* 22 */     byte[] arrayOfByte = (byte[])null;
/*    */     try {
/* 24 */       if (paramFile.exists())
/* 25 */         arrayOfByte = IOUtils.toByteArray(new BufferedInputStream(new FileInputStream(paramFile)));
/*    */     } catch (FileNotFoundException localFileNotFoundException) {
/* 27 */       localFileNotFoundException.printStackTrace();
/*    */     } catch (IOException localIOException) {
/* 29 */       localIOException.printStackTrace();
/*    */     }
/* 31 */     return arrayOfByte;
/*    */   }
/*    */ 
/*    */   public static byte[] getImgListByte(List<File> paramList)
/*    */   {
/* 37 */     byte[] arrayOfByte1 = (byte[])null;
/*    */     try
/*    */     {
/* 40 */       int i = 0;
				for(File f : paramList)	{
/* 42 */         i += fileToByte(f).length;
/*    */       }
/*    */ 
/* 45 */       Object localObject = "#@*%!!".getBytes();
/* 46 */       arrayOfByte1 = new byte[paramList.size() * 6 + i];
/*    */ 
/* 48 */       int j = 0;
/* 49 */       for (File localFile : paramList) {
/* 50 */         byte[] arrayOfByte2 = fileToByte(localFile);
/* 51 */         int k = arrayOfByte2.length;
/*    */ 
/* 53 */         System.arraycopy(localObject, 0, arrayOfByte1, j, 6);
/* 54 */         j += 6;
/* 55 */         System.arraycopy(arrayOfByte2, 0, arrayOfByte1, j, k);
/* 56 */         j += k;
/*    */       }
/*    */     } catch (Exception localException) {
/* 59 */       localException.printStackTrace();
/*    */     }
/* 61 */     return arrayOfByte1;
/*    */   }
/*    */ 
/*    */   public static String bytesToHexString(byte[] paramArrayOfByte)
/*    */   {
/* 69 */     StringBuilder localStringBuilder = new StringBuilder();
/* 70 */     if ((paramArrayOfByte == null) || (paramArrayOfByte.length <= 0)) {
/* 71 */       return null;
/*    */     }
/* 73 */     for (int i = 0; i < paramArrayOfByte.length; i++) {
/* 74 */       int j = paramArrayOfByte[i] & 0xFF;
/* 75 */       String str = Integer.toHexString(j);
/* 76 */       if (str.length() < 2) {
/* 77 */         localStringBuilder.append(0);
/*    */       }
/* 79 */       localStringBuilder.append(str);
/*    */     }
/* 81 */     return localStringBuilder.toString();
/*    */   }
/*    */ }

/* Location:           D:\mavenrepo\image-common\image-common\1.0.0\
 * Qualified Name:     com.jd.image.common.utils.ImageUtils
 * JD-Core Version:    0.6.2
 */
package com.jd.image.common.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.FilePartSource;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jd.image.common.Message;

public class HttpClinteUtils {
	private static final int CON_TIMOUT = 20000;
	private static final int CALL_TIMOUT = 20000;
	private static final String IMAGE_POST_URL = "http://upload.erp.360buyimg.com/imageUpload.action";
	private static final String COMMON_POST_URL = "http://upload.erp.360buyimg.com/commonUpload.action";
	private static final Logger logger = LoggerFactory.getLogger(HttpClinteUtils.class);

	public static String fileUpload(File paramFile, String paramString) {
		String str = null;

		if (paramFile.exists()) {
			HttpClient localHttpClient = new HttpClient();
			PostMethod localPostMethod = new PostMethod(IMAGE_POST_URL);
			try {
				HttpConnectionManagerParams localHttpConnectionManagerParams = localHttpClient
						.getHttpConnectionManager().getParams();

				localHttpConnectionManagerParams.setConnectionTimeout(20000);
				localHttpConnectionManagerParams.setSoTimeout(20000);
				localPostMethod.addRequestHeader("aucode", paramString);
				localPostMethod.addRequestHeader("type", "0");
				localPostMethod.addRequestHeader("keycode",
						Long.toHexString(FileUtils.checksumCRC32(paramFile))
								+ paramFile.length() + "_");

				localPostMethod.setRequestEntity(new ByteArrayRequestEntity(
						ImageUtils.fileToByte(paramFile)));

				int i = localHttpClient.executeMethod(localPostMethod);
				if (i != 200) {
					logger.error("Method failed: "
							+ localPostMethod.getStatusLine());
				}

				str = localPostMethod.getResponseBodyAsString();
			} catch (Exception localException) {
				logger.error(paramString + "_" + paramFile.getName() + "_"
						+ localException.getMessage(), localException);
			} finally {
				localPostMethod.releaseConnection();
				localHttpClient.getHttpConnectionManager()
						.closeIdleConnections(0L);
			}
		} else {
			str = JsonUtils.getMsgStr("0", "5", paramFile.getAbsolutePath());
		}
		return str != null ? str : JsonUtils.getMsgStr("0", "10");
	}

	public static String fileUpload(List<File> paramList, String paramString) {
		String str1 = null;
		int i = 1;
		CopyOnWriteArrayList localCopyOnWriteArrayList = new CopyOnWriteArrayList();

		if ((paramList != null) && (paramList.size() > 0)) {
			for (File f : paramList) {
				if (!f.exists()) {
					localCopyOnWriteArrayList.add(new Message("0", "5", f
							.getAbsolutePath()));
					i = 0;
					break;
				}
			}

			if (i != 0) {
				HttpClient httpClient = new HttpClient();
				PostMethod method = new PostMethod(IMAGE_POST_URL);
				try {
					HttpConnectionManagerParams localHttpConnectionManagerParams = httpClient
							.getHttpConnectionManager().getParams();

					localHttpConnectionManagerParams
							.setConnectionTimeout(20000);

					localHttpConnectionManagerParams.setSoTimeout(20000);
					method.addRequestHeader("aucode", paramString);
					method.addRequestHeader("type", "1");
					StringBuilder localStringBuilder = new StringBuilder();
					for (File f : paramList) {
						localStringBuilder.append(Long.toHexString(FileUtils
								.checksumCRC32(f)) + f.length() + "_");
					}

					method.addRequestHeader("keycode",
							localStringBuilder.toString());

					Object localObject3 = ImageUtils.getImgListByte(paramList);
					method.setRequestEntity(new ByteArrayRequestEntity(
									(byte[]) localObject3));

					int j = ((HttpClient) httpClient)
							.executeMethod((HttpMethod) method);
					if (j != 200) {
						logger.error("Method failed: "+ method.getStatusLine());
					}

					str1 = method.getResponseBodyAsString();
					return str1 != null ? str1 : JsonUtils.getMsgStr("0", "10");
				} catch (Exception localException) {
					logger.error(
							paramString + "_" + localException.getMessage(),
							localException);
				} finally {
					method.releaseConnection();
					httpClient.getHttpConnectionManager()
							.closeIdleConnections(0L);
				}
			}
		} else {
			localCopyOnWriteArrayList.add(new Message("0", "5"));
		}
		str1 = JsonUtils.getMsgStr(localCopyOnWriteArrayList);
		return str1 != null ? str1 : JsonUtils.getMsgStr("0", "10");
	}

	public static String fileUpload(byte[] paramArrayOfByte, String paramString) {
		String str = null;
		if ((paramArrayOfByte != null) && (paramArrayOfByte.length > 0)) {
			BufferedInputStream localBufferedInputStream = new BufferedInputStream(
					new ByteArrayInputStream(paramArrayOfByte));

			HttpClient localHttpClient = new HttpClient();
			PostMethod localPostMethod = new PostMethod(IMAGE_POST_URL);
			try {
				HttpConnectionManagerParams localHttpConnectionManagerParams = localHttpClient
						.getHttpConnectionManager().getParams();

				localHttpConnectionManagerParams.setConnectionTimeout(20000);

				localHttpConnectionManagerParams.setSoTimeout(20000);

				localPostMethod.addRequestHeader("aucode", paramString);
				localPostMethod.addRequestHeader("type", "2");

				localPostMethod.addRequestHeader("keycode",
						CRC32Util.getFileCRCCode(localBufferedInputStream)
								+ paramArrayOfByte.length);

				localPostMethod.setRequestEntity(new ByteArrayRequestEntity(
						paramArrayOfByte));

				int i = localHttpClient.executeMethod(localPostMethod);
				if (i != 200) {
					logger.error("Method failed: "
							+ localPostMethod.getStatusLine());
				}

				str = localPostMethod.getResponseBodyAsString();
			} catch (Exception localException) {
				logger.error(paramString + "_" + localException.getMessage(),
						localException);
			} finally {
				localPostMethod.releaseConnection();
				localHttpClient.getHttpConnectionManager()
						.closeIdleConnections(0L);
			}
		} else {
			str = JsonUtils.getMsgStr("0", "5");
		}
		return str != null ? str : JsonUtils.getMsgStr("0", "10");
	}

	public static String commonUpload(List<File> paramList, String paramString) {
		String str = null;

		if ((paramList != null) && (paramList.size() > 0)) {
			HttpClient localHttpClient = new HttpClient();
			PostMethod localPostMethod = new PostMethod(COMMON_POST_URL);
			try {
				HttpConnectionManagerParams localHttpConnectionManagerParams = localHttpClient
						.getHttpConnectionManager().getParams();

				localHttpConnectionManagerParams.setConnectionTimeout(20000);

				localHttpConnectionManagerParams.setSoTimeout(20000);

				ArrayList localArrayList = new ArrayList();
				localArrayList.add(new StringPart("aucode", paramString));
				Object localObject1;
				for (Iterator localIterator = paramList.iterator(); localIterator
						.hasNext();) {
					localObject1 = (File) localIterator.next();
					if (((File) localObject1).exists()) {
						localArrayList.add(new FilePart("file",
								new FilePartSource(((File) localObject1)
										.getName(), (File) localObject1)));
					}
				}
				if ((localArrayList != null) && (localArrayList.size() > 0)) {
					localObject1 = new Part[localArrayList.size()];
					localArrayList.toArray((Object[]) localObject1);
					localPostMethod
							.setRequestEntity(new MultipartRequestEntity(
									(Part[]) localObject1, localPostMethod
											.getParams()));

					int i = localHttpClient.executeMethod(localPostMethod);
					if (i != 200) {
						logger.error("Method failed: "
								+ localPostMethod.getStatusLine());
					}

					str = localPostMethod.getResponseBodyAsString();
				}
			} catch (Exception localException) {
				logger.error(paramString + "_" + localException.getMessage(),
						localException);
			} finally {
				localPostMethod.releaseConnection();
				localHttpClient.getHttpConnectionManager()
						.closeIdleConnections(0L);
			}
		} else {
			str = JsonUtils.getMsgStr("0", "5");
		}
		return str != null ? str : JsonUtils.getMsgStr("0", "10");
	}

	public static void main(String[] paramArrayOfString) throws IOException {
		ArrayList localArrayList = new ArrayList();
		localArrayList.add(new File("d:\\11.bmp"));

		System.out.println(commonUpload(localArrayList,
				"d3a313cb0d444049260ab6c0a6535f3c"));
	}
}

/*
 * Location: D:\mavenrepo\image-common\image-common\1.0.0\ Qualified Name:
 * com.jd.image.common.utils.HttpClinteUtils JD-Core Version: 0.6.2
 */
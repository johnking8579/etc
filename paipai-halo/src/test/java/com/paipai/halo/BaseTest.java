package com.paipai.halo;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations={"classpath:applicationContext-common.xml",
								"classpath:applicationContext-mysql.xml"})
public class BaseTest extends AbstractTransactionalJUnit4SpringContextTests{

}

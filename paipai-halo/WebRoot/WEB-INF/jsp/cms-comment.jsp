<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<title>用户评论管理</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="static/jqueryui/smoothness/jquery-ui-1.10.4.custom.min.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="static/jqgrid/ui.jqgrid.css" />
<script src="static/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="static/jqueryui/jquery-ui-1.10.3.custom.min.js"
	type="text/javascript"></script>
<script src="static/jqgrid/i18n/grid.locale-cn.js"
	type="text/javascript"></script>
<script src="static/jqgrid/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript">
  	if("${isRedirect}" == "true"){
		window.location.href="http://member.paipai.com/cgi-bin/login_entry?PTAG=20257.1.7";
		}   
 	$(function(){ 
		var option = {
			    caption			:	'用户评论管理',
			    url				:	"${pageContext.request.contextPath}/cms/comment/jqgridReq.action",
			    editurl			:	"${pageContext.request.contextPath}/cms/comment/jqgridReq.action",
			    height			:	'50%',
			    autowidth		:	true,
			    enableAutoResize:	true,
			    datatype		:	'json',
			    mtype			: 	'POST',
			    colNames		:	['文章ID','文章类型','评论标题', '评论内容', '发表人昵称  (点击排序)','发表人ID', '评论状态','时间  (点击排序)','操作'],
			    colModel 		:	[
                  {name:'itemid', index:'itemid',sortable:false, width:30,align:'center'}, 
			      {name:'itemtype', index:'itemtype',sortable:true,stype:'select', search:true,edittype:'select',editable:true,editoptions: { value: "1:App文章;2:非App文章" }, width:70, align:'center'},
			      {name:'title', index:'title',sortable:false, width:30,align:'center'},
			      {name:'context', index:'context',sortable:false, align:'center'},
			      {name:'nickName', index:'nickName',sortable:true, search:true, align:'center',width:70},
			      {name:'wid', index:'wid',sortable:true, search:true, width:70, align:'center'},
			      {name:'isshow', index:'isshow',sortable:true,stype:'select', search:true,edittype:'select',editable:true,editoptions: { value: "1:通过;0:不通过;2:删除" }, width:70, align:'center'},
			      {name:'commentTime', index:'commentTime',sortable:true, search:false, width:70, align:'center'},
			      {name:'act',index:'act',width:50,search:false,sortable:false,editable:false},
			     
			    ],
			    pager		: 	"#pager",
			    rowNum		:	25,
			    rowList		:	[25,50,100],
			    sortname	: 	'ID',
			    sortorder	: 	'desc',
			    viewrecords	: 	true,	//在右下角显示总记录数
			    multiselect	: 	true,//复选框
			    multiselectWidth : 50,
			    rownumbers 	: 	true,
			    shrinkToFit : 	true,
			    altRows 	: 	false,
			    forceFit 	: 	true,
	            gridComplete: function(){
	                var ids = $("#list").getDataIDs();//jqGrid('getDataIDs');
	                for(var i=0;i<ids.length;i++){
	                    var cl = ids[i];
	                    be = "<input style='height:22px;width:60px;' id='passbt"+cl+"' type='button' value='审核' onclick=\"jQuery('#list').jqGrid('editGridRow','"+cl+"',{checkOnSubmit:true,checkOnUpdate:true,closeAfterEdit:true,closeOnEscape:true});\"  />";
	                   	//de = "<input style='height:22px;width:40px;' id='nopassbt"+cl+"' type='button' value='删除' onclick=\"jQuery('#list').jqGrid('delGridRow','"+cl+"',{closeOnEscape:true});\"  />";
	                    jQuery("#list").jqGrid('setRowData',ids[i],{act:be});
	                } 
	            }
			  };
			  
		 $("#list").jqGrid(option)
			  .jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true})
			  .jqGrid('navGrid',"#pager", 
					{searchtext:'查找', search:false, edit:false, add:false, addtext:'添加', edittext:'修改',deltext:'批量不通过' , refreshtext : '刷新'}, 
					{}, {},	{left:500, top:200}, 
					{sopt:['cn','bw']}
			 )
			  .navButtonAdd('#pager',{   
				   caption:"加入黑名单",    
				   buttonicon:"ui-icon-add",    
				   onClickButton: function(){    
				     	//获取选中的id
				     	var selids = $("#list").jqGrid("getGridParam", "selarrrow");
				     	if(selids==null||selids==''){
				     		alert("请至少选择一条");
				     		return;
				     	}else{
				     		$.ajax({ 
					     		url: "${pageContext.request.contextPath}/cms/comment/addbl.action", 
					     		data:{id:""+selids+""}, 
					     		dataType: "json", 
					     		success: function(data){
					     			jQuery("#list").setGridParam({url:"${pageContext.request.contextPath}/cms/comment/jqgridReq.action"}).trigger("reloadGrid");	
					          	}
					     		});
				     	}
				   },    
				   position:"first"  
				}
			  )
		  .navButtonAdd('#pager',{   
			   caption:"删除黑名单",    
			   buttonicon:"ui-icon-add",    
			   onClickButton: function(){    
			     	//获取选中的id
			     	var selids = $("#list").jqGrid("getGridParam", "selarrrow");
			     	
			     	if(selids==null||selids==''){
			     		alert("请至少选择一条");
			     		return;
			     	}else{
			     		//创建一个用户id数组
			     		 var arrayuid = new Array();
			     		for(var i = 0 ;i<selids.length;i++){
			     			var rowData = $('#list').jqGrid('getRowData',selids[i]);
			     			if(rowData!=null&&rowData.wid!=null&&rowData.wid!=''){
			     				arrayuid.push(rowData.wid);
			     			}
			     		}
			     		$.ajax({ 
				     		url: "${pageContext.request.contextPath}/cms/comment/delb.action", 
				     		data:{wid:""+arrayuid+""}, 
				     		dataType: "json", 
				     		success: function(data){
				     			jQuery("#list").setGridParam({url:"${pageContext.request.contextPath}/cms/comment/jqgridReq.action"}).trigger("reloadGrid");	
				          	}
				     		});
			     	}
			     	
			   },    
			   position:"first"  
			}
		  ).navButtonAdd('#pager',{   
			   caption:"批量通过",    
			   buttonicon:"ui-icon-add",    
			   onClickButton: function(){    
			     	//获取选中的id
			     	var selids = $("#list").jqGrid("getGridParam", "selarrrow");
			     	
			     	if(selids==null||selids==''){
			     		alert("请至少选择一条");
			     		return;
			     	}else{
			     		if(window.confirm('你确定要全部通过吗？')){
			     			//如果确认
			     			$.ajax({ 
					     		url: "${pageContext.request.contextPath}/cms/comment/batchPass.action", 
					     		data:{id:""+selids+""}, 
					     		dataType: "json", 
					     		success: function(data){
					     			jQuery("#list").setGridParam({url:"${pageContext.request.contextPath}/cms/comment/jqgridReq.action"}).trigger("reloadGrid");	
					          	}
					     		});
			              }else{
			                 return false;
			             }
			     	}
			     	
			   },    
			   position:"first"  
			}
		  ).navButtonAdd('#pager',{   
			   caption:"批量删除",    
			   buttonicon:"ui-icon-add",    
			   onClickButton: function(){    
			     	//获取选中的id
			     	var selids = $("#list").jqGrid("getGridParam", "selarrrow");
			     	
			     	if(selids==null||selids==''){
			     		alert("请至少选择一条");
			     		return;
			     	}else{
			     		if(window.confirm('你确定要删除吗？')){
			     			//如果确认
			     			$.ajax({ 
					     		url: "${pageContext.request.contextPath}/cms/comment/delall.action", 
					     		data:{id:""+selids+""}, 
					     		dataType: "json", 
					     		success: function(data){
					     			jQuery("#list").setGridParam({url:"${pageContext.request.contextPath}/cms/comment/jqgridReq.action"}).trigger("reloadGrid");	
					          	}
					     		});
			              }else{
			                 return false;
			             }
			     	}
			     	
			   },    
			   position:"first"  
			}
		  );
		});
	</script>
</head>

<body style="overflow-y: scroll">
	<table id="list"></table>
	<div id="pager"></div>
</body>
</html>

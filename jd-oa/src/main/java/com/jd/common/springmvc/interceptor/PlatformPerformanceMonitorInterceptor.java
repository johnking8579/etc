package com.jd.common.springmvc.interceptor;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.PerformanceMonitorInterceptor;

public class PlatformPerformanceMonitorInterceptor extends PerformanceMonitorInterceptor{
	private static final long serialVersionUID = 7302350984439928367L;

	/* 一个方法执行时默认允许最大执行时间 */
	private Long maxAllowedTimeMillis = 5000L;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.aop.interceptor.AbstractTraceInterceptor#invoke(org
	 * .aopalliance.intercept.MethodInvocation)
	 */
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		String name = createInvocationTraceName(invocation);
		long beginTime = System.currentTimeMillis();
		try {
			return invocation.proceed();
		} finally {
			long runTime = System.currentTimeMillis() - beginTime;
			/* 执行时间超过5s时打印信息 */
			if (runTime > maxAllowedTimeMillis) {
				defaultLogger.error("方法执行时间是 " + runTime + " ms，已超出速度允许值，请检查方法的执行效率:" + name);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.aop.interceptor.AbstractTraceInterceptor#isLogEnabled
	 * (org.apache.commons.logging.Log)
	 */
	@Override
	protected boolean isLogEnabled(Log logger) {
		return true;
	}

	/**
	 * @return the maxAllowedTimeMillis
	 */
	public Long getMaxAllowedTimeMillis() {
		return maxAllowedTimeMillis;
	}

	/**
	 * 设置一个方法执行时最大应该执行的时间（毫秒），超过这个时间将在控制台输出错误信息
	 * 
	 * @param maxAllowedTimeMillis
	 *            the maxAllowedTimeMillis to set
	 */
	public void setMaxAllowedTimeMillis(Long maxAllowedTimeMillis) {
		this.maxAllowedTimeMillis = maxAllowedTimeMillis;
	}
}

package com.jd.common.springmvc.interceptor;

import org.apache.log4j.Logger;
import org.springframework.core.NamedThreadLocal;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 监视请求方法是否超时
 */
public class WatchHandlerInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = Logger.getLogger(WatchHandlerInterceptor.class);
    private int warnSlowTime;
    private NamedThreadLocal<Long> startTimeThreadLocal =
            new NamedThreadLocal<Long>("StopWatch-StartTime");

    public int getWarnSlowTime() {
        return warnSlowTime;
    }

    public void setWarnSlowTime(int warnSlowTime) {
        this.warnSlowTime = warnSlowTime;
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        long beginTime = System.currentTimeMillis();//1、开始时间
        startTimeThreadLocal.set(beginTime);//线程绑定变量（该数据只有当前请求的线程可见）
        return true;//继续流程
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        long endTime = System.currentTimeMillis();//2、结束时间
        long beginTime = startTimeThreadLocal.get();//得到线程绑定的局部变量（开始时间）
        long consumeTime = endTime - beginTime;//3、消耗的时间
        if (consumeTime > warnSlowTime) {//此处认为处理时间超过500毫秒的请求为慢请求
            //TODO 记录到日志文件
            logger.warn(String.format("%s consume %d millis", request.getRequestURI(), consumeTime));
        }
    }
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-3-24
 *	@email : wangdongxing@jd.com
 */
package com.jd.common.springmvc.interceptor;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import org.apache.log4j.Logger;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControllerAcessLogInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = Logger.getLogger(ControllerAcessLogInterceptor.class);
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod method = (HandlerMethod) handler;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[Request:")
				.append(ComUtils.getLoginName()).append("||")
				.append(request.getRemoteAddr()).append("||")
				.append(request.getRequestURI()).append("||")
				.append(method.getBean().getClass().getName()).append(".")
				.append(method.getMethod().getName()).append("||")
				.append(JsonUtils.toJsonByGoogle(request.getParameterMap()))
				.append("]");
		logger.info(stringBuilder.toString());
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	}
}

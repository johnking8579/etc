package com.jd.common.springmvc.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 控制台显示 请求地址，方便开发人员进行request mapping进行查找
 * @author liub
 *
 */
public class WatchAccessPathHandlerInterceptor extends HandlerInterceptorAdapter{
	@Override   
    public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		System.out.println("request.getServletPath() = " + request.getServletPath());
		return true;
	}
	

}

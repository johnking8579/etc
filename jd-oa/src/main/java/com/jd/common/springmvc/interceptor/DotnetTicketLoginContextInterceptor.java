package com.jd.common.springmvc.interceptor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jd.common.web.DotnetAuthenticationTicket;
import com.jd.common.web.LoginContext;
import com.jd.common.web.cookie.CookieUtils;

/**
 * 保持logincontext中的session和从 dotnet ticket中读取出来的一致
 * <p/>
 * Date: 10-12-16 Time: 上午11:20
 * 
 * port from
 * com.jd.common.struts.interceptor.DotnetTicketLoginContextInterceptor, remove
 * dependency with struts
 * 
 * @see com.jd.common.struts.interceptor.DotnetTicketLoginContextInterceptor
 * @author xiaofei
 */
public class DotnetTicketLoginContextInterceptor extends
        HandlerInterceptorAdapter {
    private final static Log log = LogFactory
            .getLog(DotnetTicketLoginContextInterceptor.class);

    /**
     * 读取cookie
     */
    protected CookieUtils cookieUtils;

    /**
     * 自己的login cookie
     */
    protected String loginCookieKey;

    @Override
    public final boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws ServletException,
            IOException {

        DotnetAuthenticationTicket ticket = DotnetAuthenticationTicket
                .getTicket();
        if (ticket == null) {// 如果没有这个ticket
            removeLoginContext();
        } else {
            LoginContext loginContext = LoginContext.getLoginContext();
            if (loginContext == null) {
                loginContext = getLoginContextFromTicket(ticket);
                cookieUtils.setCookie(response, loginCookieKey,
                        loginContext.toCookieValue());
                setLoginContext(loginContext);
                newLoginContext(ticket);
            } else if (!ticket.getUsername().equals(loginContext.getPin())) {
                loginContextChanged(ticket, loginContext);
                loginContext.setPin(ticket.getUsername());
                cookieUtils.setCookie(response, loginCookieKey,
                        loginContext.toCookieValue());
                setLoginContext(loginContext);
            }
        }

        return true;
    }

    @Override
    public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex)
            throws Exception {
        removeLoginContext();
    }

    /**
     * 表示用户已经发生改变。logincontext的和tickit中的对应不上了
     * 
     * @param ticket
     * @param loginContext
     */
    protected void loginContextChanged(DotnetAuthenticationTicket ticket,
            LoginContext loginContext) {

    }

    /**
     * 预留。表示没有logincontext，从ticket中创建
     * 
     * @param ticket
     */
    protected void newLoginContext(DotnetAuthenticationTicket ticket) {

    }

    /**
     * 将新构造的logincontext放入actioncontext
     * 调用这个方法可能因为有ticket但是没有logincontext。有可能是新用户从passport登录过来
     * 
     * @param loginContext
     */
    protected void setLoginContext(LoginContext loginContext) {
        LoginContext.setLoginContext(loginContext);
    }

    /**
     * 从dotnet 的 ticket中重构出logincontext
     * 
     * @param ticket
     * @return
     */
    protected LoginContext getLoginContextFromTicket(
            DotnetAuthenticationTicket ticket) {
        LoginContext loginContext = new LoginContext();
        loginContext.setPin(ticket.getUsername());
        return loginContext;
    }

    /**
     * 称去登录内容
     */
    protected void removeLoginContext() {
        LoginContext.remove();
    }

    public void setCookieUtils(CookieUtils cookieUtils) {
        this.cookieUtils = cookieUtils;
    }

    public void setLoginCookieKey(String loginCookieKey) {
        this.loginCookieKey = loginCookieKey;
    }
}
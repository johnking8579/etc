
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSys_BumenByIdResult" type="{http://360buy.com/}Sys_bumen" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSysBumenByIdResult"
})
@XmlRootElement(name = "GetSys_BumenByIdResponse")
public class GetSysBumenByIdResponse {

    @XmlElement(name = "GetSys_BumenByIdResult")
    protected SysBumen getSysBumenByIdResult;

    /**
     * Gets the value of the getSysBumenByIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link SysBumen }
     *     
     */
    public SysBumen getGetSysBumenByIdResult() {
        return getSysBumenByIdResult;
    }

    /**
     * Sets the value of the getSysBumenByIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SysBumen }
     *     
     */
    public void setGetSysBumenByIdResult(SysBumen value) {
        this.getSysBumenByIdResult = value;
    }

}

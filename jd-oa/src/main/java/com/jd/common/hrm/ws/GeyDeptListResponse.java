
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GeyDeptListResult" type="{http://360buy.com/}ArrayOfSys_bumen" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geyDeptListResult"
})
@XmlRootElement(name = "GeyDeptListResponse")
public class GeyDeptListResponse {

    @XmlElement(name = "GeyDeptListResult")
    protected ArrayOfSysBumen geyDeptListResult;

    /**
     * Gets the value of the geyDeptListResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSysBumen }
     *     
     */
    public ArrayOfSysBumen getGeyDeptListResult() {
        return geyDeptListResult;
    }

    /**
     * Sets the value of the geyDeptListResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSysBumen }
     *     
     */
    public void setGeyDeptListResult(ArrayOfSysBumen value) {
        this.geyDeptListResult = value;
    }

}


package com.jd.common.hrm.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmployeeQuery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmployeeQuery">
 *   &lt;complexContent>
 *     &lt;extension base="{http://360buy.com/}Employee">
 *       &lt;sequence>
 *         &lt;element name="EnableViewJG" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MultiDepID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MultiPostID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bm_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subcom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmployeeQuery", propOrder = {
    "rest"
})
public class EmployeeQuery
    extends Employee
{

    @XmlElementRefs({
        @XmlElementRef(name = "MultiDepID", namespace = "http://360buy.com/", type = JAXBElement.class),
        @XmlElementRef(name = "EnableViewJG", namespace = "http://360buy.com/", type = JAXBElement.class),
        @XmlElementRef(name = "MultiPostID", namespace = "http://360buy.com/", type = JAXBElement.class),
        @XmlElementRef(name = "subcom", namespace = "http://360buy.com/", type = JAXBElement.class),
        @XmlElementRef(name = "Bm_name", namespace = "http://360buy.com/", type = JAXBElement.class)
    })
    protected List<JAXBElement<? extends Serializable>> rest;

    /**
     * Gets the rest of the content model. 
     * 
     * <p>
     * You are getting this "catch-all" property because of the following reason: 
     * The field name "BmName" is used by two different parts of a schema. See: 
     * line 156 of http://erp1.360buy.com/hrmservice/DeptWebService.asmx?WSDL
     * line 214 of http://erp1.360buy.com/hrmservice/DeptWebService.asmx?WSDL
     * <p>
     * To get rid of this property, apply a property customization to one 
     * of both of the following declarations to change their names: 
     * Gets the value of the rest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends Serializable>> getRest() {
        if (rest == null) {
            rest = new ArrayList<JAXBElement<? extends Serializable>>();
        }
        return this.rest;
    }

}

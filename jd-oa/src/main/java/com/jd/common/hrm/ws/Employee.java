
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Employee">
 *   &lt;complexContent>
 *     &lt;extension base="{http://360buy.com/}PaginationBase1">
 *       &lt;sequence>
 *         &lt;element name="Orgid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Bm_newid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="jbxx_hkxz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_yns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_ynk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_yold" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Onint" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Onin" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cg_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Jbxx_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Bm_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="zw_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="zwu_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="jg_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cs_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="jbxx_xm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_sex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_csrq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="jbxx_rzrq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="jbxx_zzrq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="jbxx_hyzk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_zzmm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_mz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_jg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_xl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_zy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_byyx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_lxdz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_dzxx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_sfzh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_lxdh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_dbr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_dbrsfz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_dbrzz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_dbrdh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_cczm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_fj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_qtlxr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_qtlxrdh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_zzzt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_lzrq" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="jbxx_lzyy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_bz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_zp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jbxx_userpwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ibxx_dabh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bm_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cs_mc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_photo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="BankAccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Qx_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ht_user" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ht_pass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zwu_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zw_mc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Employee", propOrder = {
    "orgid",
    "bmNewid",
    "jbxxHkxz",
    "jbxxYns",
    "jbxxYnk",
    "jbxxYold",
    "onint",
    "onin",
    "cgId",
    "jbxxId",
    "bmId",
    "zwId",
    "zwuId",
    "jgId",
    "csId",
    "jbxxXm",
    "jbxxSex",
    "jbxxCsrq",
    "jbxxRzrq",
    "jbxxZzrq",
    "jbxxHyzk",
    "jbxxZzmm",
    "jbxxMz",
    "jbxxJg",
    "jbxxXl",
    "jbxxZy",
    "jbxxByyx",
    "jbxxLxdz",
    "jbxxDzxx",
    "jbxxSfzh",
    "jbxxLxdh",
    "jbxxDbr",
    "jbxxDbrsfz",
    "jbxxDbrzz",
    "jbxxDbrdh",
    "jbxxCczm",
    "jbxxFj",
    "jbxxQtlxr",
    "jbxxQtlxrdh",
    "jbxxZzzt",
    "jbxxLzrq",
    "jbxxLzyy",
    "jbxxBz",
    "jbxxZp",
    "jbxxUsername",
    "jbxxUserpwd",
    "nf",
    "ibxxDabh",
    "bmName",
    "csMc",
    "jbxxPhoto",
    "bankAccountName",
    "bankAccountNo",
    "qxId",
    "htUser",
    "htPass",
    "zwuName",
    "zwMc"
})
@XmlSeeAlso({
    EmployeeQuery.class
})
public class Employee
    extends PaginationBase1
{

    @XmlElement(name = "Orgid")
    protected int orgid;
    @XmlElement(name = "Bm_newid")
    protected int bmNewid;
    @XmlElement(name = "jbxx_hkxz")
    protected String jbxxHkxz;
    @XmlElement(name = "Jbxx_yns")
    protected String jbxxYns;
    @XmlElement(name = "Jbxx_ynk")
    protected String jbxxYnk;
    @XmlElement(name = "Jbxx_yold")
    protected String jbxxYold;
    @XmlElement(name = "Onint")
    protected int onint;
    @XmlElement(name = "Onin")
    protected int onin;
    @XmlElement(name = "cg_id")
    protected int cgId;
    @XmlElement(name = "Jbxx_id")
    protected int jbxxId;
    @XmlElement(name = "Bm_id")
    protected int bmId;
    @XmlElement(name = "zw_id")
    protected int zwId;
    @XmlElement(name = "zwu_id")
    protected int zwuId;
    @XmlElement(name = "jg_id")
    protected int jgId;
    @XmlElement(name = "cs_id")
    protected int csId;
    @XmlElement(name = "jbxx_xm")
    protected String jbxxXm;
    @XmlElement(name = "Jbxx_sex")
    protected String jbxxSex;
    @XmlElement(name = "jbxx_csrq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jbxxCsrq;
    @XmlElement(name = "jbxx_rzrq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jbxxRzrq;
    @XmlElement(name = "jbxx_zzrq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jbxxZzrq;
    @XmlElement(name = "jbxx_hyzk")
    protected String jbxxHyzk;
    @XmlElement(name = "jbxx_zzmm")
    protected String jbxxZzmm;
    @XmlElement(name = "jbxx_mz")
    protected String jbxxMz;
    @XmlElement(name = "jbxx_jg")
    protected String jbxxJg;
    @XmlElement(name = "jbxx_xl")
    protected String jbxxXl;
    @XmlElement(name = "jbxx_zy")
    protected String jbxxZy;
    @XmlElement(name = "jbxx_byyx")
    protected String jbxxByyx;
    @XmlElement(name = "jbxx_lxdz")
    protected String jbxxLxdz;
    @XmlElement(name = "jbxx_dzxx")
    protected String jbxxDzxx;
    @XmlElement(name = "jbxx_sfzh")
    protected String jbxxSfzh;
    @XmlElement(name = "jbxx_lxdh")
    protected String jbxxLxdh;
    @XmlElement(name = "jbxx_dbr")
    protected String jbxxDbr;
    @XmlElement(name = "jbxx_dbrsfz")
    protected String jbxxDbrsfz;
    @XmlElement(name = "jbxx_dbrzz")
    protected String jbxxDbrzz;
    @XmlElement(name = "jbxx_dbrdh")
    protected String jbxxDbrdh;
    @XmlElement(name = "jbxx_cczm")
    protected String jbxxCczm;
    @XmlElement(name = "jbxx_fj")
    protected String jbxxFj;
    @XmlElement(name = "jbxx_qtlxr")
    protected String jbxxQtlxr;
    @XmlElement(name = "jbxx_qtlxrdh")
    protected String jbxxQtlxrdh;
    @XmlElement(name = "jbxx_zzzt")
    protected String jbxxZzzt;
    @XmlElement(name = "jbxx_lzrq", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jbxxLzrq;
    @XmlElement(name = "jbxx_lzyy")
    protected String jbxxLzyy;
    @XmlElement(name = "jbxx_bz")
    protected String jbxxBz;
    @XmlElement(name = "jbxx_zp")
    protected String jbxxZp;
    @XmlElement(name = "jbxx_username")
    protected String jbxxUsername;
    @XmlElement(name = "jbxx_userpwd")
    protected String jbxxUserpwd;
    protected String nf;
    @XmlElement(name = "ibxx_dabh")
    protected String ibxxDabh;
    @XmlElement(name = "bm_name")
    protected String bmName;
    @XmlElement(name = "cs_mc")
    protected String csMc;
    @XmlElement(name = "Jbxx_photo")
    protected byte[] jbxxPhoto;
    @XmlElement(name = "BankAccountName")
    protected String bankAccountName;
    @XmlElement(name = "BankAccountNo")
    protected String bankAccountNo;
    @XmlElement(name = "Qx_id")
    protected int qxId;
    @XmlElement(name = "ht_user")
    protected String htUser;
    @XmlElement(name = "ht_pass")
    protected String htPass;
    @XmlElement(name = "zwu_name")
    protected String zwuName;
    @XmlElement(name = "zw_mc")
    protected String zwMc;

    /**
     * Gets the value of the orgid property.
     * 
     */
    public int getOrgid() {
        return orgid;
    }

    /**
     * Sets the value of the orgid property.
     * 
     */
    public void setOrgid(int value) {
        this.orgid = value;
    }

    /**
     * Gets the value of the bmNewid property.
     * 
     */
    public int getBmNewid() {
        return bmNewid;
    }

    /**
     * Sets the value of the bmNewid property.
     * 
     */
    public void setBmNewid(int value) {
        this.bmNewid = value;
    }

    /**
     * Gets the value of the jbxxHkxz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxHkxz() {
        return jbxxHkxz;
    }

    /**
     * Sets the value of the jbxxHkxz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxHkxz(String value) {
        this.jbxxHkxz = value;
    }

    /**
     * Gets the value of the jbxxYns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxYns() {
        return jbxxYns;
    }

    /**
     * Sets the value of the jbxxYns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxYns(String value) {
        this.jbxxYns = value;
    }

    /**
     * Gets the value of the jbxxYnk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxYnk() {
        return jbxxYnk;
    }

    /**
     * Sets the value of the jbxxYnk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxYnk(String value) {
        this.jbxxYnk = value;
    }

    /**
     * Gets the value of the jbxxYold property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxYold() {
        return jbxxYold;
    }

    /**
     * Sets the value of the jbxxYold property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxYold(String value) {
        this.jbxxYold = value;
    }

    /**
     * Gets the value of the onint property.
     * 
     */
    public int getOnint() {
        return onint;
    }

    /**
     * Sets the value of the onint property.
     * 
     */
    public void setOnint(int value) {
        this.onint = value;
    }

    /**
     * Gets the value of the onin property.
     * 
     */
    public int getOnin() {
        return onin;
    }

    /**
     * Sets the value of the onin property.
     * 
     */
    public void setOnin(int value) {
        this.onin = value;
    }

    /**
     * Gets the value of the cgId property.
     * 
     */
    public int getCgId() {
        return cgId;
    }

    /**
     * Sets the value of the cgId property.
     * 
     */
    public void setCgId(int value) {
        this.cgId = value;
    }

    /**
     * Gets the value of the jbxxId property.
     * 
     */
    public int getJbxxId() {
        return jbxxId;
    }

    /**
     * Sets the value of the jbxxId property.
     * 
     */
    public void setJbxxId(int value) {
        this.jbxxId = value;
    }

    /**
     * Gets the value of the bmId property.
     * 
     */
    public int getBmId() {
        return bmId;
    }

    /**
     * Sets the value of the bmId property.
     * 
     */
    public void setBmId(int value) {
        this.bmId = value;
    }

    /**
     * Gets the value of the zwId property.
     * 
     */
    public int getZwId() {
        return zwId;
    }

    /**
     * Sets the value of the zwId property.
     * 
     */
    public void setZwId(int value) {
        this.zwId = value;
    }

    /**
     * Gets the value of the zwuId property.
     * 
     */
    public int getZwuId() {
        return zwuId;
    }

    /**
     * Sets the value of the zwuId property.
     * 
     */
    public void setZwuId(int value) {
        this.zwuId = value;
    }

    /**
     * Gets the value of the jgId property.
     * 
     */
    public int getJgId() {
        return jgId;
    }

    /**
     * Sets the value of the jgId property.
     * 
     */
    public void setJgId(int value) {
        this.jgId = value;
    }

    /**
     * Gets the value of the csId property.
     * 
     */
    public int getCsId() {
        return csId;
    }

    /**
     * Sets the value of the csId property.
     * 
     */
    public void setCsId(int value) {
        this.csId = value;
    }

    /**
     * Gets the value of the jbxxXm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxXm() {
        return jbxxXm;
    }

    /**
     * Sets the value of the jbxxXm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxXm(String value) {
        this.jbxxXm = value;
    }

    /**
     * Gets the value of the jbxxSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxSex() {
        return jbxxSex;
    }

    /**
     * Sets the value of the jbxxSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxSex(String value) {
        this.jbxxSex = value;
    }

    /**
     * Gets the value of the jbxxCsrq property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJbxxCsrq() {
        return jbxxCsrq;
    }

    /**
     * Sets the value of the jbxxCsrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJbxxCsrq(XMLGregorianCalendar value) {
        this.jbxxCsrq = value;
    }

    /**
     * Gets the value of the jbxxRzrq property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJbxxRzrq() {
        return jbxxRzrq;
    }

    /**
     * Sets the value of the jbxxRzrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJbxxRzrq(XMLGregorianCalendar value) {
        this.jbxxRzrq = value;
    }

    /**
     * Gets the value of the jbxxZzrq property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJbxxZzrq() {
        return jbxxZzrq;
    }

    /**
     * Sets the value of the jbxxZzrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJbxxZzrq(XMLGregorianCalendar value) {
        this.jbxxZzrq = value;
    }

    /**
     * Gets the value of the jbxxHyzk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxHyzk() {
        return jbxxHyzk;
    }

    /**
     * Sets the value of the jbxxHyzk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxHyzk(String value) {
        this.jbxxHyzk = value;
    }

    /**
     * Gets the value of the jbxxZzmm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxZzmm() {
        return jbxxZzmm;
    }

    /**
     * Sets the value of the jbxxZzmm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxZzmm(String value) {
        this.jbxxZzmm = value;
    }

    /**
     * Gets the value of the jbxxMz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxMz() {
        return jbxxMz;
    }

    /**
     * Sets the value of the jbxxMz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxMz(String value) {
        this.jbxxMz = value;
    }

    /**
     * Gets the value of the jbxxJg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxJg() {
        return jbxxJg;
    }

    /**
     * Sets the value of the jbxxJg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxJg(String value) {
        this.jbxxJg = value;
    }

    /**
     * Gets the value of the jbxxXl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxXl() {
        return jbxxXl;
    }

    /**
     * Sets the value of the jbxxXl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxXl(String value) {
        this.jbxxXl = value;
    }

    /**
     * Gets the value of the jbxxZy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxZy() {
        return jbxxZy;
    }

    /**
     * Sets the value of the jbxxZy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxZy(String value) {
        this.jbxxZy = value;
    }

    /**
     * Gets the value of the jbxxByyx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxByyx() {
        return jbxxByyx;
    }

    /**
     * Sets the value of the jbxxByyx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxByyx(String value) {
        this.jbxxByyx = value;
    }

    /**
     * Gets the value of the jbxxLxdz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxLxdz() {
        return jbxxLxdz;
    }

    /**
     * Sets the value of the jbxxLxdz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxLxdz(String value) {
        this.jbxxLxdz = value;
    }

    /**
     * Gets the value of the jbxxDzxx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxDzxx() {
        return jbxxDzxx;
    }

    /**
     * Sets the value of the jbxxDzxx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxDzxx(String value) {
        this.jbxxDzxx = value;
    }

    /**
     * Gets the value of the jbxxSfzh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxSfzh() {
        return jbxxSfzh;
    }

    /**
     * Sets the value of the jbxxSfzh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxSfzh(String value) {
        this.jbxxSfzh = value;
    }

    /**
     * Gets the value of the jbxxLxdh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxLxdh() {
        return jbxxLxdh;
    }

    /**
     * Sets the value of the jbxxLxdh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxLxdh(String value) {
        this.jbxxLxdh = value;
    }

    /**
     * Gets the value of the jbxxDbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxDbr() {
        return jbxxDbr;
    }

    /**
     * Sets the value of the jbxxDbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxDbr(String value) {
        this.jbxxDbr = value;
    }

    /**
     * Gets the value of the jbxxDbrsfz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxDbrsfz() {
        return jbxxDbrsfz;
    }

    /**
     * Sets the value of the jbxxDbrsfz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxDbrsfz(String value) {
        this.jbxxDbrsfz = value;
    }

    /**
     * Gets the value of the jbxxDbrzz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxDbrzz() {
        return jbxxDbrzz;
    }

    /**
     * Sets the value of the jbxxDbrzz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxDbrzz(String value) {
        this.jbxxDbrzz = value;
    }

    /**
     * Gets the value of the jbxxDbrdh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxDbrdh() {
        return jbxxDbrdh;
    }

    /**
     * Sets the value of the jbxxDbrdh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxDbrdh(String value) {
        this.jbxxDbrdh = value;
    }

    /**
     * Gets the value of the jbxxCczm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxCczm() {
        return jbxxCczm;
    }

    /**
     * Sets the value of the jbxxCczm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxCczm(String value) {
        this.jbxxCczm = value;
    }

    /**
     * Gets the value of the jbxxFj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxFj() {
        return jbxxFj;
    }

    /**
     * Sets the value of the jbxxFj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxFj(String value) {
        this.jbxxFj = value;
    }

    /**
     * Gets the value of the jbxxQtlxr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxQtlxr() {
        return jbxxQtlxr;
    }

    /**
     * Sets the value of the jbxxQtlxr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxQtlxr(String value) {
        this.jbxxQtlxr = value;
    }

    /**
     * Gets the value of the jbxxQtlxrdh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxQtlxrdh() {
        return jbxxQtlxrdh;
    }

    /**
     * Sets the value of the jbxxQtlxrdh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxQtlxrdh(String value) {
        this.jbxxQtlxrdh = value;
    }

    /**
     * Gets the value of the jbxxZzzt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxZzzt() {
        return jbxxZzzt;
    }

    /**
     * Sets the value of the jbxxZzzt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxZzzt(String value) {
        this.jbxxZzzt = value;
    }

    /**
     * Gets the value of the jbxxLzrq property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJbxxLzrq() {
        return jbxxLzrq;
    }

    /**
     * Sets the value of the jbxxLzrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJbxxLzrq(XMLGregorianCalendar value) {
        this.jbxxLzrq = value;
    }

    /**
     * Gets the value of the jbxxLzyy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxLzyy() {
        return jbxxLzyy;
    }

    /**
     * Sets the value of the jbxxLzyy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxLzyy(String value) {
        this.jbxxLzyy = value;
    }

    /**
     * Gets the value of the jbxxBz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxBz() {
        return jbxxBz;
    }

    /**
     * Sets the value of the jbxxBz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxBz(String value) {
        this.jbxxBz = value;
    }

    /**
     * Gets the value of the jbxxZp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxZp() {
        return jbxxZp;
    }

    /**
     * Sets the value of the jbxxZp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxZp(String value) {
        this.jbxxZp = value;
    }

    /**
     * Gets the value of the jbxxUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxUsername() {
        return jbxxUsername;
    }

    /**
     * Sets the value of the jbxxUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxUsername(String value) {
        this.jbxxUsername = value;
    }

    /**
     * Gets the value of the jbxxUserpwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJbxxUserpwd() {
        return jbxxUserpwd;
    }

    /**
     * Sets the value of the jbxxUserpwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJbxxUserpwd(String value) {
        this.jbxxUserpwd = value;
    }

    /**
     * Gets the value of the nf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNf() {
        return nf;
    }

    /**
     * Sets the value of the nf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNf(String value) {
        this.nf = value;
    }

    /**
     * Gets the value of the ibxxDabh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbxxDabh() {
        return ibxxDabh;
    }

    /**
     * Sets the value of the ibxxDabh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbxxDabh(String value) {
        this.ibxxDabh = value;
    }

    /**
     * Gets the value of the bmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBmName() {
        return bmName;
    }

    /**
     * Sets the value of the bmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBmName(String value) {
        this.bmName = value;
    }

    /**
     * Gets the value of the csMc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsMc() {
        return csMc;
    }

    /**
     * Sets the value of the csMc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsMc(String value) {
        this.csMc = value;
    }

    /**
     * Gets the value of the jbxxPhoto property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getJbxxPhoto() {
        return jbxxPhoto;
    }

    /**
     * Sets the value of the jbxxPhoto property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setJbxxPhoto(byte[] value) {
        this.jbxxPhoto = ((byte[]) value);
    }

    /**
     * Gets the value of the bankAccountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountName() {
        return bankAccountName;
    }

    /**
     * Sets the value of the bankAccountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountName(String value) {
        this.bankAccountName = value;
    }

    /**
     * Gets the value of the bankAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankAccountNo() {
        return bankAccountNo;
    }

    /**
     * Sets the value of the bankAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankAccountNo(String value) {
        this.bankAccountNo = value;
    }

    /**
     * Gets the value of the qxId property.
     * 
     */
    public int getQxId() {
        return qxId;
    }

    /**
     * Sets the value of the qxId property.
     * 
     */
    public void setQxId(int value) {
        this.qxId = value;
    }

    /**
     * Gets the value of the htUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHtUser() {
        return htUser;
    }

    /**
     * Sets the value of the htUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHtUser(String value) {
        this.htUser = value;
    }

    /**
     * Gets the value of the htPass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHtPass() {
        return htPass;
    }

    /**
     * Sets the value of the htPass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHtPass(String value) {
        this.htPass = value;
    }

    /**
     * Gets the value of the zwuName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZwuName() {
        return zwuName;
    }

    /**
     * Sets the value of the zwuName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZwuName(String value) {
        this.zwuName = value;
    }

    /**
     * Gets the value of the zwMc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZwMc() {
        return zwMc;
    }

    /**
     * Sets the value of the zwMc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZwMc(String value) {
        this.zwMc = value;
    }

}

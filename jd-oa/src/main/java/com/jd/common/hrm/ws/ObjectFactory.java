
package com.jd.common.hrm.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jd.common.hrm.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EmployeeQuerySubcom_QNAME = new QName("http://360buy.com/", "subcom");
    private final static QName _EmployeeQueryMultiPostID_QNAME = new QName("http://360buy.com/", "MultiPostID");
    private final static QName _EmployeeQueryMultiDepID_QNAME = new QName("http://360buy.com/", "MultiDepID");
    private final static QName _EmployeeQueryEnableViewJG_QNAME = new QName("http://360buy.com/", "EnableViewJG");
    private final static QName _EmployeeQueryBmName_QNAME = new QName("http://360buy.com/", "Bm_name");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jd.common.hrm.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GeyDeptListResponse }
     * 
     */
    public GeyDeptListResponse createGeyDeptListResponse() {
        return new GeyDeptListResponse();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link GeyDeptList }
     * 
     */
    public GeyDeptList createGeyDeptList() {
        return new GeyDeptList();
    }

    /**
     * Create an instance of {@link ArrayOfEmployeeQuery }
     * 
     */
    public ArrayOfEmployeeQuery createArrayOfEmployeeQuery() {
        return new ArrayOfEmployeeQuery();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link ResourceNamesResponse }
     * 
     */
    public ResourceNamesResponse createResourceNamesResponse() {
        return new ResourceNamesResponse();
    }

    /**
     * Create an instance of {@link GetUsersByDeptIdResponse }
     * 
     */
    public GetUsersByDeptIdResponse createGetUsersByDeptIdResponse() {
        return new GetUsersByDeptIdResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSysBumen }
     * 
     */
    public ArrayOfSysBumen createArrayOfSysBumen() {
        return new ArrayOfSysBumen();
    }

    /**
     * Create an instance of {@link ResourceNames }
     * 
     */
    public ResourceNames createResourceNames() {
        return new ResourceNames();
    }

    /**
     * Create an instance of {@link VerifyResponse }
     * 
     */
    public VerifyResponse createVerifyResponse() {
        return new VerifyResponse();
    }

    /**
     * Create an instance of {@link GetNewDeptListByUserpinResponse }
     * 
     */
    public GetNewDeptListByUserpinResponse createGetNewDeptListByUserpinResponse() {
        return new GetNewDeptListByUserpinResponse();
    }

    /**
     * Create an instance of {@link ArrayOfUser }
     * 
     */
    public ArrayOfUser createArrayOfUser() {
        return new ArrayOfUser();
    }

    /**
     * Create an instance of {@link GetBumenInfoByUserPinResponse }
     * 
     */
    public GetBumenInfoByUserPinResponse createGetBumenInfoByUserPinResponse() {
        return new GetBumenInfoByUserPinResponse();
    }

    /**
     * Create an instance of {@link GetUserByNameResponse }
     * 
     */
    public GetUserByNameResponse createGetUserByNameResponse() {
        return new GetUserByNameResponse();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link GetUserByName }
     * 
     */
    public GetUserByName createGetUserByName() {
        return new GetUserByName();
    }

    /**
     * Create an instance of {@link GetLoginCookieInfo }
     * 
     */
    public GetLoginCookieInfo createGetLoginCookieInfo() {
        return new GetLoginCookieInfo();
    }

    /**
     * Create an instance of {@link Employee }
     * 
     */
    public Employee createEmployee() {
        return new Employee();
    }

    /**
     * Create an instance of {@link GetUsersByRes }
     * 
     */
    public GetUsersByRes createGetUsersByRes() {
        return new GetUsersByRes();
    }

    /**
     * Create an instance of {@link GetNewDeptByUserpin }
     * 
     */
    public GetNewDeptByUserpin createGetNewDeptByUserpin() {
        return new GetNewDeptByUserpin();
    }

    /**
     * Create an instance of {@link GetSysBumenById }
     * 
     */
    public GetSysBumenById createGetSysBumenById() {
        return new GetSysBumenById();
    }

    /**
     * Create an instance of {@link GetNewDeptByUserpinResponse }
     * 
     */
    public GetNewDeptByUserpinResponse createGetNewDeptByUserpinResponse() {
        return new GetNewDeptByUserpinResponse();
    }

    /**
     * Create an instance of {@link SysBumen }
     * 
     */
    public SysBumen createSysBumen() {
        return new SysBumen();
    }

    /**
     * Create an instance of {@link GetBumenInfoByUserPin }
     * 
     */
    public GetBumenInfoByUserPin createGetBumenInfoByUserPin() {
        return new GetBumenInfoByUserPin();
    }

    /**
     * Create an instance of {@link Dept }
     * 
     */
    public Dept createDept() {
        return new Dept();
    }

    /**
     * Create an instance of {@link GetUsersByids }
     * 
     */
    public GetUsersByids createGetUsersByids() {
        return new GetUsersByids();
    }

    /**
     * Create an instance of {@link GetSysBumenByIdResponse }
     * 
     */
    public GetSysBumenByIdResponse createGetSysBumenByIdResponse() {
        return new GetSysBumenByIdResponse();
    }

    /**
     * Create an instance of {@link GetLoginCookieInfoResponse }
     * 
     */
    public GetLoginCookieInfoResponse createGetLoginCookieInfoResponse() {
        return new GetLoginCookieInfoResponse();
    }

    /**
     * Create an instance of {@link EmployeeQuery }
     * 
     */
    public EmployeeQuery createEmployeeQuery() {
        return new EmployeeQuery();
    }

    /**
     * Create an instance of {@link GetUsersByDeptId }
     * 
     */
    public GetUsersByDeptId createGetUsersByDeptId() {
        return new GetUsersByDeptId();
    }

    /**
     * Create an instance of {@link GetUsersByidsResponse }
     * 
     */
    public GetUsersByidsResponse createGetUsersByidsResponse() {
        return new GetUsersByidsResponse();
    }

    /**
     * Create an instance of {@link GetNewDeptListByUserpin }
     * 
     */
    public GetNewDeptListByUserpin createGetNewDeptListByUserpin() {
        return new GetNewDeptListByUserpin();
    }

    /**
     * Create an instance of {@link GetUsersByResResponse }
     * 
     */
    public GetUsersByResResponse createGetUsersByResResponse() {
        return new GetUsersByResResponse();
    }

    /**
     * Create an instance of {@link Verify }
     * 
     */
    public Verify createVerify() {
        return new Verify();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://360buy.com/", name = "subcom", scope = EmployeeQuery.class)
    public JAXBElement<String> createEmployeeQuerySubcom(String value) {
        return new JAXBElement<String>(_EmployeeQuerySubcom_QNAME, String.class, EmployeeQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://360buy.com/", name = "MultiPostID", scope = EmployeeQuery.class)
    public JAXBElement<String> createEmployeeQueryMultiPostID(String value) {
        return new JAXBElement<String>(_EmployeeQueryMultiPostID_QNAME, String.class, EmployeeQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://360buy.com/", name = "MultiDepID", scope = EmployeeQuery.class)
    public JAXBElement<String> createEmployeeQueryMultiDepID(String value) {
        return new JAXBElement<String>(_EmployeeQueryMultiDepID_QNAME, String.class, EmployeeQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://360buy.com/", name = "EnableViewJG", scope = EmployeeQuery.class)
    public JAXBElement<Integer> createEmployeeQueryEnableViewJG(Integer value) {
        return new JAXBElement<Integer>(_EmployeeQueryEnableViewJG_QNAME, Integer.class, EmployeeQuery.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://360buy.com/", name = "Bm_name", scope = EmployeeQuery.class)
    public JAXBElement<String> createEmployeeQueryBmName(String value) {
        return new JAXBElement<String>(_EmployeeQueryBmName_QNAME, String.class, EmployeeQuery.class, value);
    }

}

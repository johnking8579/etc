
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNewDeptByUserpinResult" type="{http://360buy.com/}Sys_bumen" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNewDeptByUserpinResult"
})
@XmlRootElement(name = "GetNewDeptByUserpinResponse")
public class GetNewDeptByUserpinResponse {

    @XmlElement(name = "GetNewDeptByUserpinResult")
    protected SysBumen getNewDeptByUserpinResult;

    /**
     * Gets the value of the getNewDeptByUserpinResult property.
     * 
     * @return
     *     possible object is
     *     {@link SysBumen }
     *     
     */
    public SysBumen getGetNewDeptByUserpinResult() {
        return getNewDeptByUserpinResult;
    }

    /**
     * Sets the value of the getNewDeptByUserpinResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SysBumen }
     *     
     */
    public void setGetNewDeptByUserpinResult(SysBumen value) {
        this.getNewDeptByUserpinResult = value;
    }

}

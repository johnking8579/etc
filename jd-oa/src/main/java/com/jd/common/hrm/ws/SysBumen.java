
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Sys_bumen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Sys_bumen">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Bm_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Bm_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bm_num" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bm_man" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Bm_bz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="user_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Jbxx_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="zgid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="upid" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Bm_type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="StoreType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sys_bumen", propOrder = {
    "bmId",
    "bmName",
    "bmNum",
    "bmMan",
    "bmBz",
    "nf",
    "userName",
    "jbxxId",
    "fid",
    "zgid",
    "upid",
    "bmType",
    "storeType"
})
public class SysBumen {

    @XmlElement(name = "Bm_id")
    protected int bmId;
    @XmlElement(name = "Bm_name")
    protected String bmName;
    @XmlElement(name = "Bm_num")
    protected String bmNum;
    @XmlElement(name = "Bm_man")
    protected String bmMan;
    @XmlElement(name = "Bm_bz")
    protected String bmBz;
    protected String nf;
    @XmlElement(name = "user_name")
    protected String userName;
    @XmlElement(name = "Jbxx_id")
    protected int jbxxId;
    protected int fid;
    protected int zgid;
    protected int upid;
    @XmlElement(name = "Bm_type")
    protected int bmType;
    @XmlElement(name = "StoreType")
    protected int storeType;

    /**
     * Gets the value of the bmId property.
     * 
     */
    public int getBmId() {
        return bmId;
    }

    /**
     * Sets the value of the bmId property.
     * 
     */
    public void setBmId(int value) {
        this.bmId = value;
    }

    /**
     * Gets the value of the bmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBmName() {
        return bmName;
    }

    /**
     * Sets the value of the bmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBmName(String value) {
        this.bmName = value;
    }

    /**
     * Gets the value of the bmNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBmNum() {
        return bmNum;
    }

    /**
     * Sets the value of the bmNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBmNum(String value) {
        this.bmNum = value;
    }

    /**
     * Gets the value of the bmMan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBmMan() {
        return bmMan;
    }

    /**
     * Sets the value of the bmMan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBmMan(String value) {
        this.bmMan = value;
    }

    /**
     * Gets the value of the bmBz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBmBz() {
        return bmBz;
    }

    /**
     * Sets the value of the bmBz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBmBz(String value) {
        this.bmBz = value;
    }

    /**
     * Gets the value of the nf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNf() {
        return nf;
    }

    /**
     * Sets the value of the nf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNf(String value) {
        this.nf = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the jbxxId property.
     * 
     */
    public int getJbxxId() {
        return jbxxId;
    }

    /**
     * Sets the value of the jbxxId property.
     * 
     */
    public void setJbxxId(int value) {
        this.jbxxId = value;
    }

    /**
     * Gets the value of the fid property.
     * 
     */
    public int getFid() {
        return fid;
    }

    /**
     * Sets the value of the fid property.
     * 
     */
    public void setFid(int value) {
        this.fid = value;
    }

    /**
     * Gets the value of the zgid property.
     * 
     */
    public int getZgid() {
        return zgid;
    }

    /**
     * Sets the value of the zgid property.
     * 
     */
    public void setZgid(int value) {
        this.zgid = value;
    }

    /**
     * Gets the value of the upid property.
     * 
     */
    public int getUpid() {
        return upid;
    }

    /**
     * Sets the value of the upid property.
     * 
     */
    public void setUpid(int value) {
        this.upid = value;
    }

    /**
     * Gets the value of the bmType property.
     * 
     */
    public int getBmType() {
        return bmType;
    }

    /**
     * Sets the value of the bmType property.
     * 
     */
    public void setBmType(int value) {
        this.bmType = value;
    }

    /**
     * Gets the value of the storeType property.
     * 
     */
    public int getStoreType() {
        return storeType;
    }

    /**
     * Sets the value of the storeType property.
     * 
     */
    public void setStoreType(int value) {
        this.storeType = value;
    }

}

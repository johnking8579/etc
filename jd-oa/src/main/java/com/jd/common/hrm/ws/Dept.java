
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Dept complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Dept">
 *   &lt;complexContent>
 *     &lt;extension base="{http://360buy.com/}PermissionBase">
 *       &lt;sequence>
 *         &lt;element name="IsReserved" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dept", propOrder = {
    "isReserved",
    "parentId",
    "type"
})
public class Dept
    extends PermissionBase
{

    @XmlElement(name = "IsReserved")
    protected String isReserved;
    @XmlElement(name = "ParentId")
    protected int parentId;
    @XmlElement(name = "Type")
    protected int type;

    /**
     * Gets the value of the isReserved property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsReserved() {
        return isReserved;
    }

    /**
     * Sets the value of the isReserved property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsReserved(String value) {
        this.isReserved = value;
    }

    /**
     * Gets the value of the parentId property.
     * 
     */
    public int getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     */
    public void setParentId(int value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the type property.
     * 
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     */
    public void setType(int value) {
        this.type = value;
    }

}

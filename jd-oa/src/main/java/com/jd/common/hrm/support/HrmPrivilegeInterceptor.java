package com.jd.common.hrm.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.jd.common.hrm.HrmPrivilege;
import com.jd.common.hrm.HrmPrivilegeHelper;
import com.jd.common.hrm.IllegalHrmPrivilegeException;
import com.jd.common.web.DotnetAuthenticationTicket;
import com.jd.common.web.LoginContext;

/**
 * 拦截方法执行时的注释。以确定是否有权限执行。 <br/>
 * 做的Logincontext认证<br/>
 * User: yangsiyong@360buy.com<br/>
 * Date: 2010-6-2<br/>
 * Time: 11:09:54<br/>
 * 
 * port from com.jd.common.struts.interceptor.HrmDotnetPrivilegeInterceptor,
 * remove dependency with struts.
 * 
 * @see com.jd.common.struts.interceptor.HrmPrivilegeInterceptor
 * @see com.jd.common.struts.interceptor.HrmDotnetPrivilegeInterceptor
 * @author xiaofei
 */
public class HrmPrivilegeInterceptor extends HandlerInterceptorAdapter {
    
    @Override
    public final boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) {

        HrmPrivilege annotation = null;
        if(handler instanceof HandlerMethod){
            annotation = ((HandlerMethod) handler)
                    .getMethodAnnotation(HrmPrivilege.class);
        }else{
            return true;
            // throw new IllegalStateException(
            // "Please use spring 3.1's prefer implementation of HandlerMapping.");
        }

        if (annotation != null) {
            String code = annotation.value();// 资源需要的权限
            if (StringUtils.isNotEmpty(code)) {
                String username = getUsername();
                if (username == null
                        || !hrmPrivilegeHelper.hasHrmPrivilege(username, code)) {
                    throw new IllegalHrmPrivilegeException(
                            "No enough privilege.", code.split(","));
                }
            }
        }

        return true;
    }

    /**
     * 取得用户名
     * 
     * @return
     */
    protected String getUsername() {
        String username = getDotnetTicketUsername();
        if (username == null) {
            LoginContext context = LoginContext.getLoginContext();// 只取hrm相关的信息
            if (context != null && StringUtils.isNotBlank(context.getPin())) {
                username = context.getPin();
            }
        }
        return username;
    }

    protected String getDotnetTicketUsername() {
        DotnetAuthenticationTicket ticket = DotnetAuthenticationTicket
                .getTicket();
        if (ticket != null && StringUtils.isNotBlank(ticket.getUsername())) {
            return ticket.getUsername();
        } else {
            return null;
        }
    }

    private HrmPrivilegeHelper hrmPrivilegeHelper;

    public void setHrmPrivilegeHelper(HrmPrivilegeHelper hrmPrivilegeHelper) {
        this.hrmPrivilegeHelper = hrmPrivilegeHelper;
    }
}
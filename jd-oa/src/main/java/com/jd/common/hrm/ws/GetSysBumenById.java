
package com.jd.common.hrm.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bm_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bmId"
})
@XmlRootElement(name = "GetSys_BumenById")
public class GetSysBumenById {

    @XmlElement(name = "bm_id")
    protected int bmId;

    /**
     * Gets the value of the bmId property.
     * 
     */
    public int getBmId() {
        return bmId;
    }

    /**
     * Sets the value of the bmId property.
     * 
     */
    public void setBmId(int value) {
        this.bmId = value;
    }

}

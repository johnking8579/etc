package com.jd.oa.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.app.service.ProcessTaskTimerService;
import com.jd.oa.app.service.ProcessTaskService;

/**
 * 
 * @desc 流程执行定时器
 * @author WXJ
 * @date 2014-6-16 下午11:00:14
 *
 */
public class ProcessTaskTimerExecutor implements TaskExecutor<ProcessTaskTimer> {

	private static final Logger logger = LoggerFactory
			.getLogger(ProcessTaskTimerExecutor.class);
	
	@Autowired
	ProcessTaskTimerService processTaskTimerService;
	
	@Autowired
	ProcessTaskService processTaskService;

	@Override
	public boolean execute(List<ProcessTaskTimer> timers) {
		logger.info("Task execute begin[job:ProcessTaskTimer, timers.size:" + timers.size() + "]");
		for (ProcessTaskTimer timer : timers) {
			processTaskService.processTaskTimer(timer);			
		}
		logger.info("Task execute end[job:ProcessTaskTimer, timers.size:" + timers.size() + "]");
		return false;
	}

	@Override
	public List<ProcessTaskTimer> fetch(JobDef job, Set<String> partitions) {
		logger.info("Task fetch begin[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + "]");
		List<ProcessTaskTimer> result = new ArrayList<ProcessTaskTimer>();
		List<ProcessTaskTimer> timersList = processTaskTimerService.find("findExecuteTimer",null);
		for (ProcessTaskTimer timer : timersList) {
			String partition = String
					.valueOf(Math.abs(timer.getTaskId().hashCode()) % 12);
			if (partitions.contains(partition)) {
				result.add(timer);
			}
		}
		logger.info("Task fetch end[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + ", task.size:" + result.size() + "]");
		return result;
	}
}

package com.jd.oa.worker;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;
import com.jd.oa.system.service.SyncFromPsService;

public class SyncFromPsExecutor implements TaskExecutor<String> {
	
	static String clazz = SyncFromPsExecutor.class.getName();
	static Logger log = Logger.getLogger(clazz);
	
	@Resource
	private SyncFromPsService syncFromPsService;
	@Resource
	private ShardedXCommands redisClient;

	/**
	 * 使用redis控制tomcat集群定时器的并发
	 */
	@Override
	public List<String> fetch(JobDef jobDef, Set<String> partitions) {
		try {
			Thread.sleep(new Random().nextInt(10) * 1000);	//休眠1~10秒, 降低并发概率
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(!"true".equals(redisClient.get(clazz)))	{	//redis无该定时器标记时, 开始执行定时器
			try	{
				redisClient.setex(clazz, 60 * 30, "true");	//标记自动失效
				startSync();
			} finally	{
				redisClient.del(clazz);
				log.info("定时器执行完成, 已清除执行标记");
			}
		} else	{
			log.info("redis中有定时器[" + clazz + "]执行标记[" + redisClient.get(clazz) + "], 跳过");
		}
		return Arrays.asList("","");	//向分布式调度服务器返回几个虚值
	}
	
	private void startSync()	{
		log.info("开始从peopleSoft中复制[组织机构|人员|职位]至中间表...");
		long start = System.currentTimeMillis();
		
		DataSourceHandle.setDataSourceType("ps");	//在service层事务开启前切换数据源
		log.info("从ps中查询所有组织机构...");
		List<PsOrg> orgs = syncFromPsService.findOrgFromPs();
		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyOrgToSwapTable(orgs);
		
		DataSourceHandle.setDataSourceType("ps");
		log.info("从ps中查询所有职位...");
		List<PsPosition> pos = syncFromPsService.findPositionFromPs();
		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyPositionToSwapTable(pos);
		
		DataSourceHandle.setDataSourceType("ps");
		log.info("从ps中查询所有员工...");
		List<PsUser> users = syncFromPsService.findUserFromPs();
		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyUserToSwapTable(users);
		log.info("从PeopleSoft复制[组织机构|职位|人员]到OA中间表,已完成,耗时(分钟): " + (System.currentTimeMillis() - start) /1000/60);
		
		syncFromPsService.syncFromSwapTable();
	}
	
	@Override
	public boolean execute(List<String> list) {
		return false;	//不执行分布式调度
	}
}
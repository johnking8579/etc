package com.jd.oa.worker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.dts.JobDef;
import com.jd.dts.TaskExecutor;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.mail.MailReceiveService;
import com.jd.oa.common.mail.MailReceiveService.MailFlag;

/**
 * 邮件审批执行器
 * 
 * @author zhouhuaqi
 * 
 */
public class MailExecutor implements TaskExecutor<Message> {

	private static final Logger logger = LoggerFactory
			.getLogger(MailExecutor.class);

	@Autowired
	MailReceiveService mailReceiveService;

	@Autowired
	ProcessTaskService processTaskService;

	@Override
	public boolean execute(List<Message> messages) {
		
		if (messages != null && messages.size() > 0) {
			logger.info("Task execute begin[job:mailJob, messages.size:"
					+ messages.size() + "]");
			for (Message m : messages) {
				try {
					// 解析待办
//					if (mailReceiveService.getSubject(m).indexOf("流程中心") > -1) {
						String content = mailReceiveService.getContent(m);
						if (content != null) {// 邮件内容
							processTaskService.dealMailTask(content,
									mailReceiveService.getFrom(m));
						}
//					}
//					else{
						mailReceiveService.setMailFlag(m, MailFlag.SEEN);
//					}
				} catch (IOException e) {
					throw new BusinessException(e.getMessage(), e);
				} catch (MessagingException e) {
					throw new BusinessException(e.getMessage(), e);
				} catch (Exception e) {
					throw new BusinessException(e.getMessage(), e);
				}
			}
			logger.info("Task execute end[job:mailJob, messages.size:"
					+ messages.size() + "]");
		}
		
		return false;
	}

	@Override
	public List<Message> fetch(JobDef job, Set<String> partitions) {
		logger.info("Task fetch begin[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + "]");
		List<Message> result = new ArrayList<Message>();
		List<Message> mList = mailReceiveService.getUnreadMails();
		logger.info("未读邮件个数：" + (mList!=null?mList.size():0));
		
		if(mList!=null){
			for (Message message : mList) {
				logger.info("邮件标题：" + mailReceiveService.getSubject(message));
				String partition = String.valueOf(Math.abs(mailReceiveService.getSubject(
						message).hashCode()) % 12);
				if (partitions.contains(partition)) {
					logger.info("partition：" + partition);
					result.add(message);
				}
			}
		}
		logger.info("Task fetch end[job:" + job.getName() + ", partitions:"
				+ partitions.toString() + ", task.size:" + result.size() + "]");
		return result;
	}
}

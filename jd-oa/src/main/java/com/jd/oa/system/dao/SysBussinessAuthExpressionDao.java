package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysBussinessAuthExpression;

public interface SysBussinessAuthExpressionDao extends BaseDao<SysBussinessAuthExpression, String>{

}

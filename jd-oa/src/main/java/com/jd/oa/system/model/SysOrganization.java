package com.jd.oa.system.model;

import java.util.Date;

import com.jd.oa.common.model.IdModel;

public class SysOrganization extends IdModel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7186794120510729971L;

	/**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String organizationType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String organizationCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String organizationName;
    
    private String organizationFullname;
    
    private String organizationFullPath;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.PARENT_ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String parentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.IS_PARENT
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Boolean isParent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String status;

	//人员公司一级部门，二级部门
	private String organizationCompanyCode;
	private String organizationCompanyName;
	private String organization_1_Code;
	private String organization_1_Name;
	private String organization_2_Code;
	private String organization_2_Name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ORGANIZATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date modifyTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.ID
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.ID
     *
     * @param id the value for T_JDOA_SYS_ORGANIZATION.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
    
    
    public String getOrganizationFullPath() {
		return organizationFullPath;
	}

	public void setOrganizationFullPath(String organizationFullPath) {
		this.organizationFullPath = organizationFullPath;
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_TYPE
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.ORGANIZATION_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getOrganizationType() {
        return organizationType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_TYPE
     *
     * @param organizationType the value for T_JDOA_SYS_ORGANIZATION.ORGANIZATION_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType == null ? null : organizationType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_CODE
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.ORGANIZATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getOrganizationCode() {
        return organizationCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_CODE
     *
     * @param organizationCode the value for T_JDOA_SYS_ORGANIZATION.ORGANIZATION_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode == null ? null : organizationCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_NAME
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.ORGANIZATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.ORGANIZATION_NAME
     *
     * @param organizationName the value for T_JDOA_SYS_ORGANIZATION.ORGANIZATION_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName == null ? null : organizationName.trim();
    }

    public String getOrganizationFullname() {
		return organizationFullname;
	}

	public void setOrganizationFullname(String organizationFullname) {
		this.organizationFullname = organizationFullname == null ? null : organizationFullname.trim();
	}

	/**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.PARENT_ID
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.PARENT_ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.PARENT_ID
     *
     * @param parentId the value for T_JDOA_SYS_ORGANIZATION.PARENT_ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.IS_PARENT
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.IS_PARENT
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Boolean getIsParent() {
        return isParent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.IS_PARENT
     *
     * @param isParent the value for T_JDOA_SYS_ORGANIZATION.IS_PARENT
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setIsParent(Boolean isParent) {
        this.isParent = isParent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.STATUS
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.STATUS
     *
     * @param status the value for T_JDOA_SYS_ORGANIZATION.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.CREATOR
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.CREATOR
     *
     * @param creator the value for T_JDOA_SYS_ORGANIZATION.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.CREATE_TIME
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.CREATE_TIME
     *
     * @param createTime the value for T_JDOA_SYS_ORGANIZATION.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.MODIFIER
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.MODIFIER
     *
     * @param modifier the value for T_JDOA_SYS_ORGANIZATION.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ORGANIZATION.MODIFY_TIME
     *
     * @return the value of T_JDOA_SYS_ORGANIZATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ORGANIZATION.MODIFY_TIME
     *
     * @param modifyTime the value for T_JDOA_SYS_ORGANIZATION.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }


	public String getOrganization_1_Code() {
		return organization_1_Code;
	}

	public void setOrganization_1_Code(String organization_1_Code) {
		this.organization_1_Code = organization_1_Code;
	}

	public String getOrganization_1_Name() {
		return organization_1_Name;
	}

	public void setOrganization_1_Name(String organization_1_Name) {
		this.organization_1_Name = organization_1_Name;
	}

	public String getOrganization_2_Code() {
		return organization_2_Code;
	}

	public void setOrganization_2_Code(String organization_2_Code) {
		this.organization_2_Code = organization_2_Code;
	}

	public String getOrganization_2_Name() {
		return organization_2_Name;
	}

	public void setOrganization_2_Name(String organization_2_Name) {
		this.organization_2_Name = organization_2_Name;
	}

	public String getOrganizationCompanyCode() {
		return organizationCompanyCode;
	}

	public void setOrganizationCompanyCode(String organizationCompanyCode) {
		this.organizationCompanyCode = organizationCompanyCode;
	}

	public String getOrganizationCompanyName() {
		return organizationCompanyName;
	}

	public void setOrganizationCompanyName(String organizationCompanyName) {
		this.organizationCompanyName = organizationCompanyName;
	}
}
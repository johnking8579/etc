
package com.jd.oa.system.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysRolePrivilege;

/**
 * @author liub
 *
 */
public interface SysRolePrivilegeService extends BaseService<SysRolePrivilege, String>{
	
}


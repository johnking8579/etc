package com.jd.oa.system.service;

import java.util.List;

import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;

/**
 * 从PEOPLESOFT同步数据
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @author 荆营	部门:综合职能研发部
 * @date 2014年7月31日
 */
public interface SyncFromPsService {

	/**
	 * 从PS中把组织/职位/人员复制到中间表
	 */
	void copyToSwapTable();
	
	/**
	 * 从数据库中间表同步到相关表
	 */
	void syncFromSwapTable();
	
	List<PsOrg> findOrgFromPs();
	
	void copyOrgToSwapTable(List<PsOrg> list);

	void copyUserToSwapTable(List<PsUser> list);

	List<PsUser> findUserFromPs();

	void copyPositionToSwapTable(List<PsPosition> list);

	List<PsPosition> findPositionFromPs();
	
}

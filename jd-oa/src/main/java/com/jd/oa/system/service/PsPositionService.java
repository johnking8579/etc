package com.jd.oa.system.service;

public interface PsPositionService {
	
	void insertOrUpdateSysPosition();

	void deleteSysPosition();

}

package com.jd.oa.system.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.oa.common.utils.ComUtils;

import com.jd.oa.system.model.SysUserRole;
import com.jd.oa.system.service.SysUserRoleService;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.service.SysRoleService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;
import com.alibaba.dubbo.common.utils.StringUtils;

/**
 * @author yujiahe
 * @Description: 系统角色Controller
 */
@Controller
@RequestMapping(value = "/system")
public class SysRoleController {
    private static final Logger logger = Logger.getLogger(SysRoleController.class);
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysUserService sysUerService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private UserAuthorizationService userAuthorizationService;


    /**
     * @Description: 进入到查询系统角色页面
     * @author yujiahe
     */

    @RequestMapping(value = "/sysRole_index", method = RequestMethod.GET)
    public String list() {
        return "system/sysRole_index";
    }

    /**
     * @Description: 查询系统角色列表
     * @author yujiahe
     */
    @RequestMapping(value = "/sysRole_page")
    @ResponseBody
    public Object page(HttpServletRequest request, SysRole sysRole) {

        //创建pageWrapper
        PageWrapper<SysRole> pageWrapper = new PageWrapper<SysRole>(request);
        //添加搜索条件
        String roleName = request.getParameter("roleName");
        String roleType = request.getParameter("roleType");
        if( roleName.contains("%")){
            roleName=roleName.replace("%","\\%");
        }
        if( roleName.contains("_")){
            roleName=roleName.replace("_","\\_");
        }
        if (null != roleName && !roleName.equals("")) {
            pageWrapper.addSearch("roleName", roleName);
        }
        if (null != roleType && !roleType.equals("")) {
            pageWrapper.addSearch("roleType", roleType);
        }

        //后台取值
        sysRoleService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        //返回到页面的数据
        String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
        logger.debug("result:" + json);
        return json;

    }

    /**
     * @Description:进入新建系统角色页面
     * @author yujiahe
     */
    @RequestMapping(value = "/sysRole_add", method = RequestMethod.GET)
    public String sysRoleAdd() {
        return "system/sysRole_add";
    }

    /**
     * @Description:进入编辑系统角色页面
     * @author yujiahe
     */

    @RequestMapping(value = "/sysRole_update", method = RequestMethod.GET)
    public String showSysRole(String sysroleId, Model model) {
        SysRole sysRole = sysRoleService.showSysRole(sysroleId);
        model.addAttribute("sysRole", sysRole);
        return "system/sysRole_update";
    }

    /**
     * @param sysRole
     * @return String
     * @Description: 保存新增角色
     * @author yujiahe
     */
    @RequestMapping(value = "/sysRole_addSave")
    @ResponseBody
    public String saveAddSysRole(SysRole sysRole) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userid = ComUtils.getLoginName();//拿到当前用户
        sysRole.setCreator(userid);//创建者
        sysRole.setStatus("0");//0状态为可用
        sysRole.setCreateTime(new Date());//新增时间
        sysRole.setModifyTime(new Date());//最近修改时间
        sysRoleService.insert(sysRole);
        map.put("operator", true);
        map.put("message", "添加成功");

        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param sysRole
     * @return String
     * @Description: 保存修改系统角色
     * @author yujiahe
     */
    @RequestMapping(value = "/sysRole_updateSave")
    @ResponseBody
    public String saveEditDictType(SysRole sysRole) {
        Map<String, Object> map = new HashMap<String, Object>();
        sysRole.setStatus("0");
        String userid = ComUtils.getLoginName(); //拿到当前用户
        sysRole.setModifier(userid);//操作者
        sysRole.setModifyTime(new Date());//最近修改时间
        sysRoleService.update(sysRole);
        map.put("operator", true);
        map.put("message", "添加成功");

        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param sysRole
     * @return String
     * @Description: 删除单个系统角色
     * @author yujiahe
     */
    @RequestMapping(value = "/sysRole_delete")
    @ResponseBody
    public String deleteSysRole(SysRole sysRole) {
        Map<String, Object> map = new HashMap<String, Object>();
        String userid = ComUtils.getLoginName();//拿到当前用户
        sysRole.setModifier(userid);//操作者
        sysRole.setModifyTime(new Date()); //最近修改时间
        sysRole.setStatus("1");//逻辑删除
        sysRoleService.update(sysRole);
        map.put("operator", true);
        map.put("message", "删除成功");
        return JSONObject.fromObject(map).toString();
    }

    @RequestMapping(value = "/validate_test", method = RequestMethod.GET)
    public String validate() {
        return "validate_test";
    }

    /**
     * @return String
     * @Description: 查询角色类表
     * @author yujiahe
     * @date 2013-8-30下午08:03:40
     * @version V1.0
     */
    @RequestMapping(value = "/sysRole_findSysRoleList")
    @ResponseBody
    public String sysRoleFindSysRoleList() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<SysRole> sysRoleList = sysRoleService.findSysRoleList();
        map.put("sysRoleList", sysRoleList);
        map.put("message", "查询成功");
        String json = JsonUtils.toJsonByGoogle(sysRoleList);
        return json;
    }

    @RequestMapping(value = "/privillege_list", method = RequestMethod.GET)
    public ModelAndView privilleges_list(
            @RequestParam(value = "locale", required = false) Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView("system/privillege_list");
        return mav;
    }

    /**
     * @return String
     * @Description: 根据角色名称查询角色列表，返回个数用于判断新增用户组时判断是否重名
     * @author yujiahe
     * @date 2013-8-30下午08:03:40
     * @version V1.0
     */
    @RequestMapping(value = "/sysRole_checkRoleNameUnique")
    @ResponseBody
    public int sysRoleCheckRoleNameUnique(@RequestParam String roleName) {
//        Map<String, Object> map = new HashMap<String, Object>();
        List<SysRole> sysRoleList = sysRoleService.findSysRoleByName(roleName);
        return sysRoleList.size();
    }

    /**
     * @return String
     * @Description: 根据角色ID查找是否有用户属于该角色，用于删除前的判断
     * @author yujiahe
     * @version V1.0
     */
    @RequestMapping(value = "/sysUserRole_deleteCheck")
    @ResponseBody
    public int sysUserRoleDeleteCheck(@RequestParam String roleId) {
//        Map<String, Object> map = new HashMap<String, Object>();
        List<SysUserRole> sysUserRoleList = sysUserRoleService.findSysUserRoleByRoleId(roleId);
        return sysUserRoleList.size();
    }

    /**
     * @Description:进入编辑系统角色页面
     * @author yujiahe
     */

    @RequestMapping(value = "/sysRole_view", method = RequestMethod.GET)
    public String sysRoleView(String sysroleId, Model model) {
        SysRole sysRole = sysRoleService.showSysRole(sysroleId);
        model.addAttribute("sysRole", sysRole);
        return "system/sysRole_view";
    }


}

package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.system.dao.SysPositionDao;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.service.SysPositionService;

@Service("sysPositionService")
@Transactional

public class SysPositionServiceImpl extends BaseServiceImpl<SysPosition, String> implements SysPositionService{
	@Autowired
	private SysPositionDao sysPositionDao;
	
	public SysPositionDao getDao(){
		return sysPositionDao;
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 下午07:57:10
	 *
	 * @param mp
	 * @return
	 */
	@Override
	public List<SysPosition> selectPosByParentId(Map mp) {
	    return sysPositionDao.selectPosByParentId(mp);
	}
	
	@Override
	public void insertOrUpdate(List<SysPosition> positions)	{
		//以parentId为key，目的检查是否为父级
      	Map<String,SysPosition> parentPosition = new HashMap<String,SysPosition>();
      	for(int i = 0 ; i < positions.size() ; i++){
      		SysPosition position = positions.get(i);
      		if(position != null){
      			parentPosition.put(position.getParentId(), position);
      		}
      	}
      		
		for(int i = 0 ; i < positions.size() ; i++){
			SysPosition position = positions.get(i);
			if(position != null){
				position.setOrganizationType("1");
				String positionCode = position.getPositionCode();
				Map<String, String> map = new HashMap<String, String>() ;
				map.put("positionCode", positionCode);
				List<SysPosition> list = this.find("findByPositionCode" , map);
				if(list != null && list.size() > 0){
					SysPosition newPosition = list.get(0);
					newPosition.setIsParent(this.getIsParent(positionCode, parentPosition));
					newPosition.setPositionName(position.getPositionName());
					newPosition.setOrganizationId(position.getOrganizationId());
					newPosition.setParentId(position.getParentId());
					logger.info("更新SysPosition:"+ newPosition.getPositionName() + "("+newPosition.getPositionCode()+")");
	            	this.sysPositionDao.update(newPosition);
				} else {
					position.setIsParent(this.getIsParent(positionCode, parentPosition));
					position.setId(IdUtils.uuid2());
					logger.info("插入SysPosition:"+ position.getPositionName() + "("+position.getPositionCode()+")");
					this.sysPositionDao.insert(position);
				}
			}
		}
	}
	/**
	 * 定时任务：传输EHR岗位信息数据到OA
	 */
	public void transferPositionInfoFromHR(List<SysPosition> allPositions){
		insertOrUpdate(allPositions);
		//执行删除操作
		getDeletePositionList(allPositions);
	}
	/**
	 * 删除操作
	 * @param ehrPositions ehr数据库中全集对象
	 * @return
	 */
	private List<SysPosition> getDeletePositionList(List<SysPosition> ehrPositions){
		List<SysPosition> deletePositionList = new ArrayList<SysPosition>();
		Map<String,SysPosition> ehrPositionMap = new HashMap<String,SysPosition>();
      	for(int i = 0 ; i < ehrPositions.size() ; i++){
      		SysPosition Position = ehrPositions.get(i);
      		if(Position != null){
      			ehrPositionMap.put(Position.getPositionCode(), Position);
      		}
      	}
      	List<SysPosition> oaPositions = this.find(null);
      	for(int j = 0 ; j < oaPositions.size() ; j++){
      		SysPosition position = oaPositions.get(j);
      		if(position != null){
      			if(ehrPositionMap.get(position.getPositionCode()) == null){
      				System.out.println("\n>>>>>>>>>>>>>>>>>>> delete " + position.getPositionName() + "<<<<<<<<<<<<<<<<<<<<<<<<");
      				logger.info("delete SysPosition:"+ 
      						position.getPositionName() + "("+position.getPositionCode()+")");
      				position.setStatus("1");
      				this.update(position);
      				//this.delete(position, true);
      			}
      		}
      	}
      	return deletePositionList;
	}

	/**
	 * 获取当前的Position是否有子，如果有子，返回true,否则返回为false
	 * @param PositionCode
	 * @param parentPosition
	 * @return
	 */
	private boolean getIsParent(String positionCode,Map<String,SysPosition> parentPosition){
		if(parentPosition.get(positionCode) != null){ 
			return true;
		}
		return false;
	}
}

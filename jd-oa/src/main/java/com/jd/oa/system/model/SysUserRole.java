package com.jd.oa.system.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;

public class SysUserRole extends IdModel {

    //用户角色配置 对应用户ID
    private String userId;

    //用户角色配置 对应角色ID
    private String roleId;

    //用户角色配置 操作用户信息
    private String creator;

    //用户角色配置 操作时间
    private Date createTime;

    //用户角色配置 操作用户信息
    private String modifier;

    //用户角色配置 操作时间
    private Date modifyTime;

    //用户姓名
    private String realName;
    //用户角色拼接，多个角色由逗号分隔
    private String userRole;
    //用户角色 (单个）
    private String roleName;
    //用户角色 (单个）
    private String roleType;
    //组织编码
    private String orgId;
    //组织名称
    private String organizationName;
    
    //组织名称全名
    private String organizationFullName ;


    public String getOrgId() {
        return orgId;
    }


    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }


    public String getRoleId() {
        return roleId;
    }


    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }


    public String getCreator() {
        return creator;
    }


    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }


    public Date getModifyTime() {
        return modifyTime;
    }


    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }



    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }


    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }


	public String getOrganizationFullName() {
		return organizationFullName;
	}


	public void setOrganizationFullName(String organizationFullName) {
		this.organizationFullName = organizationFullName;
	}
}
package com.jd.oa.system.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysLevel;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;

public interface UserAuthorizationService {
	

	//根据某一授权表达式获取符合的用户
	public List<SysUser> getUserListBySysAuthExpression(SysAuthExpression sysAuthExpression);
	
		
	//根据授权表达式列表获取所有符合的用户,同上，参数不同
	public List<SysUser> getUserListBySysAuthExpressionList(List<SysAuthExpression>  sysAuthExpressionList);
	
	//根据授权表达式列表获取所有符合的用户,同上，返回类型不同
	public Map<String, String> getUserMapBySysAuthExpressionList(List<SysAuthExpression>  sysAuthExpressionList);
		
	//根据授权表达式列表获取所有符合的用户,同上，但返回组织的名称
	public List<SysUser> getUserListByMultiSysAuthExpressionList(List<SysAuthExpression>  sysAuthExpressionList);
	
	
	//根据授权表达式列表获取中文名称	
	public List<SysAuthExpression> getAuthExpressionNameListByIds(List<SysAuthExpression> list);
	
	
	//列出某个用户的所有资源
	public List<SysBussinessAuthExpression> getResourceByUserId(String sysUserId, String sysBusinessType);
	 
	 
	//查看某个资源是否属于指定用户
	public boolean isResourceOwnedByUser(String sysBusinessId, String sysBusinessType, String sysUserId);
	 
	 //根据存储过程，获取指定组织架构的所有子架构
	public List<String> getAllChildOrganizationByCode(String organizationCode);
	
	//	根据组织id获取所有的父组织结构id，包括其自身
	public List<String> getAllParentOrganizationByCode(String organizationCode) ;

}

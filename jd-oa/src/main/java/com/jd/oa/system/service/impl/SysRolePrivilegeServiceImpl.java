
package com.jd.oa.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysRolePrivilegeDao;
import com.jd.oa.system.model.SysRolePrivilege;
import com.jd.oa.system.service.SysRolePrivilegeService;

/**
 * @author liub
 *
 */
@Component
public class SysRolePrivilegeServiceImpl extends BaseServiceImpl<SysRolePrivilege, String> implements SysRolePrivilegeService {
	
	@Autowired(required=true)
	private SysRolePrivilegeDao sysRolePrivilegeDao;

	public SysRolePrivilegeDao getDao() {
		return sysRolePrivilegeDao;
	}
}
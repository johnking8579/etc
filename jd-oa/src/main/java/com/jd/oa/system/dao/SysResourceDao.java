package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysResource;

/**
 * 
 * @author xulin
 *
 */
public interface SysResourceDao extends BaseDao<SysResource, String>{
	
	 /**
     * 根据资源编码或名称获取资源
     * @param sysResource 文件夹对象
     * @return  Integer
     */
    public Integer findCountByRes(SysResource sysResource);
    
    /**
	 * 根据用户ID获取用户分配的所有叶子资源信息
	 * @param userId 用户Id，主键ID
	 * 
	 * @author zhouhuaqi
	 * @date 2013-10-08 15:33:00瑟
	 * @version V1.0
	 */
	public List<SysResource> findByUserId(String userId);
}

package com.jd.oa.system.model;

import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.jd.oa.common.model.IdModel;


/**
 * 操作
 * 
 * @author zhouhq
 */
public class Operation extends IdModel {

	private static final long serialVersionUID = -5841215343691242179L;          
	
	public static final String ENTITY_NAME = "操作类型";
	
	/**
	 * 操作编码
	 */
	private String code;
	/**
	 * 操作名称
	 */
	private String name;
	/**
	 * 操作名称
	 */
	private String description;
	
	/**
	 * 包含权限
	 */
	private Set<Privilege> privileges;

	public Operation() {
	}

	public Operation(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Privilege> getPrivileges() {
		return privileges;
	}
	
	public void setPrivileges(Set<Privilege> privileges) {
		this.privileges = privileges;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
		.append("id", getId())
		.append("code", getCode())
        .append("name", getName())
        .toString();
	}
}

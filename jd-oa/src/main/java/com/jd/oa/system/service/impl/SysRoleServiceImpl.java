package com.jd.oa.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysRoleDao;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.service.SysRoleService;

import java.util.List;


/**
 * @author yujiahe
 * 
 */
// Spring Service Bean的标识.
@Service("sysRoleService")
// 默认将类中的所有函数纳入事务管理.
@Transactional
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole, String>
		implements SysRoleService {

	@Autowired
	private SysRoleDao sysRoleDao;

	public SysRoleDao getDao() {
		return sysRoleDao;
	}

	public SysRole showSysRole(String sysroleId) {
		return sysRoleDao.showSysRole(sysroleId);
	}

	@Override
	public List<SysRole> findByUserId(String userId) {
		return sysRoleDao.findByUserId(userId);
	}
    /**
     * @Description: 根据角色类型返回角色列表
     * @return List<SysRole>
     * @author yujiahe
     * @version V1.0
     */
    public List<SysRole> findSysRoleList(){
        return sysRoleDao.findSysRoleList();
    }

    public List<SysRole> findUnUseSysRoleListByUserId(String userId) {
       return sysRoleDao.findUnUseSysRoleListByUserId(userId);
    }


    /**
	 * 
	 * @desc 获取所有角色
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	@Override
	public List<SysRole> findRoleList() {
		// TODO Auto-generated method stub
		return sysRoleDao.findRoleList();
	}
	
	/**
	 * 
	 * @desc 获取所有用户组
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	@Override
	public List<SysRole> findGroupList() {
		// TODO Auto-generated method stub
		return sysRoleDao.findGroupList();
	}
    /**
     * @desc 根据名称查询角色（用户组）列表
     * @author YJH
     * @return
     */
    public List<SysRole> findSysRoleByName(String roleName) {
        return sysRoleDao.findSysRoleByName(roleName);
    }
}
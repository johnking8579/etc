/**
 * 
 */
package com.jd.oa.system.controller;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.form.service.FormService;
import com.jd.oa.system.service.PsService;

/**
 *@Description: 系统功能 Controller
 *@author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class QuerySqlController {
	
	 private static final Logger logger = Logger.getLogger(QuerySqlController.class);
	
	 @Autowired
	 private FormService formService;
	 
	 @Autowired
	 PsService psService;
	 
	 @RequestMapping(value = "/querySql", method = RequestMethod.GET)
	 public String index() throws Exception {
	     return "system/querySql";
	 }
	 
	 @RequestMapping(value = "/querySql_list")
	 @ResponseBody
	 public Object list(HttpServletRequest request,@RequestParam String  sql,@RequestParam String  type,@RequestParam String  datesource) throws Exception {
		 /*PageWrapper<SysOperation> pageWrapper = new PageWrapper<SysOperation>(request);
		 sysOperationService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	     logger.debug("result:"+json);*/
		 Map<String,Object> result = new HashMap<String, Object>();
		 if(datesource.equals("2")){
			 DataSourceHandle.setDataSourceType("ps");
			 result = psService.getList(sql);
		 }else{
			 result = this.formService.getList(sql,type);
		 }
		 
		 if(result.get("operate").equals("select")){
			 String json = JsonUtils.toJsonByGoogle(result.get("data"));
			 result.put("isjson", true);
			 result.put("json", json);
		     return result;
		 } else {
			 //return result.get("data");
			 result.put("isjson", false);
			 return result;
		 }
	 }
}

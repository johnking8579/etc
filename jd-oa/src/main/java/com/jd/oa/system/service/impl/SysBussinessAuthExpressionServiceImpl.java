package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysBussinessAuthExpressionDao;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;


@Service("sysBussinessAuthExpressionService")
@Transactional
public class SysBussinessAuthExpressionServiceImpl extends BaseServiceImpl<SysBussinessAuthExpression, String> implements SysBussinessAuthExpressionService{
	@Autowired
	private SysBussinessAuthExpressionDao sysBussinessAuthExpressionDao;
	
	public SysBussinessAuthExpressionDao getDao(){
		return sysBussinessAuthExpressionDao;
	}
	
	public void deleteBusinessIdKeyAndExpressionId(String businessId,String expressionId){
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("businessId", businessId);
		map.put("expressionId", expressionId);
		find("deleteBusinessIdKeyAndExpressionId", map);
		return ;
	}
	
	
	public void updateByResources(List<SysBussinessAuthExpression> sysBussinessAuthExpressions){
		
		
		List<SysBussinessAuthExpression> list = new ArrayList<SysBussinessAuthExpression>();
		
		//获取文档id和type
		for(SysBussinessAuthExpression sysBussiness:sysBussinessAuthExpressions){
				Map<String, String> map = new HashMap<String, String>();
				map.put("businessId", sysBussiness.getBusinessId());
				map.put("businessType", sysBussiness.getBusinessType());
				find("deleteBusinessIdAndType", map);
		}
//		//根据文档id和type获取文档list
//		List<SysBussinessAuthExpression> temp = find("findByBusinessIdAndBusinessType",map);
//		
//		//批量删除
//		for (SysBussinessAuthExpression item : temp) {
//			Map<String, String> m = new HashMap<String, String>();
//			map.put("businessId", item.getBusinessId());
//			map.put("businessType", item.getBusinessType());
//			find("deleteBusinessIdAndType", m);
//		}
		
		//批量增加
		for (int i=0;i<sysBussinessAuthExpressions.size();i++) {
			if(null!=sysBussinessAuthExpressions.get(i).getAuthExpressionId()&&!sysBussinessAuthExpressions.get(i).getAuthExpressionId().equals("")){
				SysBussinessAuthExpression one = new SysBussinessAuthExpression();
				one.setId(null);
				one.setAuthExpressionId(sysBussinessAuthExpressions.get(i).getAuthExpressionId());
				one.setBusinessId(sysBussinessAuthExpressions.get(i).getBusinessId());
				one.setBusinessType(sysBussinessAuthExpressions.get(i).getBusinessType());
				one.setCreateTime(sysBussinessAuthExpressions.get(i).getCreateTime());
				one.setCreator(sysBussinessAuthExpressions.get(i).getCreator());
				one.setModifier(sysBussinessAuthExpressions.get(i).getModifier());
				one.setModifyTime(sysBussinessAuthExpressions.get(i).getModifyTime());
				
				list.add(one);
			}
		}
		
		insert(list);
	}
	/**
	 * 根据资源ID删除对应的所有权限表达式
	 */
	@Override
	public void deleteByBusinessId(String businessId) {
		sysBussinessAuthExpressionDao.delete("deleteByBusinessId",businessId);
	}
	
	
}

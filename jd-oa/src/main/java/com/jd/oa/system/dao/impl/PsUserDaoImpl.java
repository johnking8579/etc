package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.PsUserDao;
import com.jd.oa.system.model.PsUser;

@Component("psUserDao")
public class PsUserDaoImpl extends MyBatisDaoImpl<PsUser, String> implements PsUserDao{

	private static final String NAMESPACE = PsUser.class.getName();
	
	@Override
	public void truncate() {
		getSqlSession().update(NAMESPACE + ".truncate");
	}
	
	@Override
	public void add(PsUser psUser) {
		getSqlSession().insert(NAMESPACE + ".add", psUser);
	}

	@Override
	public void add(List<PsUser> psUsers) {
		for(PsUser p : psUsers)	{
			this.add(p);
		}
	}

	@Override
	public List<PsUser> findValidUser() {
		return getSqlSession().selectList(NAMESPACE + ".findValidUser");
	}

	@Override
	public int findAllCount() {
		Integer i = (Integer)getSqlSession().selectOne(NAMESPACE + ".findAll-count");
		return i.intValue();
	}

}

package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.PsUser;

public interface PsUserDao extends BaseDao<PsUser, String>{
	
	void truncate();
	
	void add(List<PsUser> psUsers);

	void add(PsUser psUser);
	
	List<PsUser> findValidUser();
	
	int findAllCount();

}

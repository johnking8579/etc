package com.jd.oa.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.PsOrgDao;
import com.jd.oa.system.model.PsOrg;

@Component("psOrgDao")
public class PsOrgDaoImpl extends MyBatisDaoImpl<PsOrg,String> implements PsOrgDao {
	
	private static final String NAMESPACE = PsOrg.class.getName();

	@Override
	public void truncate() {
		getSqlSession().update(NAMESPACE + ".truncate");
	}

	@Override
	public void add(PsOrg psOrg) {
		getSqlSession().insert(NAMESPACE + ".add", psOrg);
	}

	@Override
	public void add(List<PsOrg> psOrgs) {
		for(PsOrg p : psOrgs)	{
			this.add(p);
		}
	}

	@Override
	public List<PsOrg> findAll() {
		return getSqlSession().selectList(NAMESPACE + ".findAll");
	}
	
}

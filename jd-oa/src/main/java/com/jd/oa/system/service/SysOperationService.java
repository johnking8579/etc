
package com.jd.oa.system.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysOperation;

/**
 * 功能操作 service
 * @author liub
 *
 */
public interface SysOperationService extends BaseService<SysOperation, String>{
	
}


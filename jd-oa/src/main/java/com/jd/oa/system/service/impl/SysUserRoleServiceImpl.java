/**
 *
 */
package com.jd.oa.system.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.RemoveCache;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.system.dao.SysUserRoleDao;
import com.jd.oa.system.model.SysUserRole;
import com.jd.oa.system.service.SysUserRoleService;

/**
 * @author yujiahe
 * @date 2013年8月30日
 */
@Service("sysUserRoleService")
@Transactional
@CacheProxy
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRole, Long>
        implements SysUserRoleService {

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    public BaseDao<SysUserRole, Long> getDao() {
        return sysUserRoleDao;
    }

    /**
     * @return String
     * @Description: 根据用户ID查询用户角色配置列表
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> getSysUserRoleListByUserId(String UserId) {
        return sysUserRoleDao.findSysUserRoleListByUserId(UserId);
    }

    public List<SysUserRole> findByUserId(String userId) {
        return sysUserRoleDao.findByUserId(userId);
    }

    @Override
    @RemoveCache(key = {@CacheKey(template = "JDOA/AssignedResources/${p1}", simple = true),@CacheKey(template = "JDOA/AssignedPrivileges/${p1}", simple = true)})
    public void sysUserRolesDeletebyUR(String roleId, String userId) {
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setRoleId(roleId);
        sysUserRole.setUserId(userId);
        sysUserRoleDao.deleteSysUserRoleByUR(sysUserRole);
    }

    /**
     * 根据角色查询到用户列表
     *
     * @param roleName
     * @return
     */
    @Override
    public List<SysUserRole> findUserListByRoleName(String roleName) {
        return sysUserRoleDao.findUserListByRoleName(roleName);
    }

    /**
     * @return String
     * @Description: 针对一个用户新增多个角色
     * @author yujiahe
     * @version V1.0
     */
//    @RemoveCache(key = {@CacheKey(template = "JDOA/AssignedResources/${p1}", simple = true),@CacheKey(template = "JDOA/AssignedPrivileges/${p1}", simple = true)})
    public void saveEditSysUserRole(String roleIds, String UserId) {
        try {
        sysUserRoleDao.deleteUserRoleByUserId(UserId);
        String[] args = roleIds.split(",");
        for (String roleId : args) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(IdUtils.uuid2());
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(UserId);
            String operator = ComUtils.getLoginName();//操作人
            sysUserRole.setCreator(operator);
            sysUserRole.setModifier(operator);
            sysUserRole.setCreateTime(new Date());
            sysUserRole.setModifyTime(new Date());
            sysUserRoleDao.insertSysUserRole(sysUserRole);
        }  } catch (Exception e) {
            throw new BusinessException("单个用户授权失败！", e);

        }
    }

    /**
     * @return String
     * @Description: 批量授权，将一个角色批量授权给多个用户
     * @author yujiahe
     * @version V1.0
     */
//    @RemoveCache(key = {@CacheKey(template = "JDOA/AssignedResources/*", simple = true),@CacheKey(template = "JDOA/AssignedPrivileges/*", simple = true)})
    public void saveEditSysUserRoles(String roleId, String UserIds) {
        String[] args = UserIds.split(",");
        for (String UserId : args) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setId(IdUtils.uuid2());
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(UserId);
            String operator = ComUtils.getLoginName();//操作人
            sysUserRole.setCreator(operator);
            sysUserRole.setModifier(operator);
            sysUserRole.setCreateTime(new Date());
            sysUserRole.setModifyTime(new Date());
            sysUserRoleDao.deleteSysUserRoleByUR(sysUserRole);
            sysUserRoleDao.insertSysUserRole(sysUserRole);
        }
    }

    /**
     * @return String
     * @Description: 根据角色ID查找用户角色配置列表
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> findSysUserRoleByRoleId(String roleId) {
        return sysUserRoleDao.findSysUserRoleListByRoleId(roleId);
    }

}

/**
 * 
 */
package com.jd.oa.system.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysOperationDao;
import com.jd.oa.system.model.SysOperation;
import com.jd.oa.system.model.SysRole;

/**
 * @author liub
 *
 */
@Component("sysOperationDao")
public class SysOperationDaoImpl extends MyBatisDaoImpl<SysOperation, String> implements SysOperationDao {
}
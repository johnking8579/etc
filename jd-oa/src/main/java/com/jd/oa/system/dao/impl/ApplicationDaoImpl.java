package com.jd.oa.system.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.ApplicationDao;
import com.jd.oa.system.model.Application;

@Component("applicationDao")
public class ApplicationDaoImpl extends MyBatisDaoImpl<Application, Long>
		implements ApplicationDao {
}

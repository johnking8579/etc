package com.jd.oa.system.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.service.SysPrivilegeService;
import com.jd.oa.system.service.SysResourceService;

/**
 * 
 * @author: xulin
 */
@Controller
@RequestMapping(value = "/system")
public class SysResourceController {
	private static final Logger logger = Logger.getLogger(SysResourceController.class);

	@Autowired
	private SysResourceService sysResourceService;
	@Autowired
	private SysPrivilegeService sysPrivilegeService;

	/**
	 * 初始化资源树
	 * 
	 * @param locale
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sysResource_index", method = RequestMethod.GET)
	public ModelAndView list(
			@RequestParam(value = "locale", required = false) Locale locale,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("system/sysResource_index");

		TreeNode rootNode = new TreeNode();
		rootNode.setId("root");
		rootNode.setName("京东-办公自动化");
		rootNode.setIsParent(Boolean.TRUE);
		rootNode.setIconSkin("diy1");

		mav.addObject("applications", JsonUtils.toJsonByGoogle(rootNode));
		return mav;
	}

	/**
	 * 资源数异步加载
	 * 
	 * @param request
	 *            请求对象
	 * @param level
	 *            树的层级
	 * @param nodeId
	 *            树节点ID
	 * @param response
	 *            相应对象
	 * @throws Exception
	 */
	@RequestMapping(value = "/sysResource_load", method = RequestMethod.POST)
	@ResponseBody
	public void sysResourceLoad(HttpServletRequest request,
			@RequestParam(value = "level", required = true) int level,
			@RequestParam(value = "nodeId", required = true) String nodeId,
			HttpServletResponse response) {
		try {
			if (!nodeId.equals("undefined")) {
				List<SysResource> resources = null;
				resources = sysResourceService.findByParentId(nodeId);
				response.getWriter().write(
						JsonUtils.toJsonByGoogle(getTreeNodes(resources)));
			}
		} catch (Exception e1) {
			logger.error(e1.getMessage(), e1);
		}
	}

	/**
	 * 资源查看页面
	 * 
	 * @param sysResource
	 *  资源对象
	 */
	@RequestMapping(value = "/sysResource_view", method = RequestMethod.GET)
	public ModelAndView folderView(SysResource sysResource) {
		sysResource = sysResourceService.get(sysResource.getId());
		ModelAndView mav = new ModelAndView("system/sysResource_view");
		mav.addObject("sysResource", sysResource);
		return mav;
	}

	/**
	 * 资源增加页面
	 * 
	 * @param sysResource
	 *            资源对象
	 */
	@RequestMapping(value = "/sysResource_add", method = RequestMethod.GET)
	public ModelAndView sysResourceAdd(SysResource sysResource) {
		ModelAndView mav = new ModelAndView("system/sysResource_add");
		mav.addObject("sysResource", sysResource);
		return mav;
	}

	/**
	 * 资源修改页面
	 * 
	 * @param sysResource
	 *  资源对象
	 * @return
	 */
	@RequestMapping(value = "/sysResource_update", method = RequestMethod.GET)
	public ModelAndView sysResourceUpdate(SysResource sysResource) {
		sysResource = sysResourceService.get(sysResource.getId());
		ModelAndView mav = new ModelAndView("system/sysResource_update");
		mav.addObject("sysResource", sysResource);
		return mav;
	}

	/**
	 * 保存新资源
	 * 
	 * @param sysResource
	 * @return
	 */
	@RequestMapping(value = "/sysResource_addSave")
	@ResponseBody
	public String addSave(SysResource sysResource) {
		Map<String, Object> map = new HashMap<String, Object>();
		int count = this.sysResourceService.findCountByRes(sysResource);
		if (count == 0) {
			sysResource.setStatus("0");
			sysResource.setChildrenNum(0);
			sysResourceService.insertResource(sysResource);
			map.put("check", true);
			map.put("id", sysResource.getId());
		} else {
			map.put("check", false);
		}

		return JSONObject.fromObject(map).toString();
	}

	/**
	 * 修改资源
	 * 
	 * @param sysResource
	 * @return
	 */
	@RequestMapping(value = "/sysResource_updateSave")
	@ResponseBody
	public String updateSave(SysResource sysResource) {
		Map<String, Object> map = new HashMap<String, Object>();
		sysResource.setStatus("0");
		sysResourceService.update(sysResource);
		map.put("check", true);

		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 删除资源
	 * 
	 * @param sysResource
	 * 资源对象
	 */
	@RequestMapping(value = "/sysResource_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String resourceDelete(SysResource sysResource) {
		Map<String, Object> map = new HashMap<String, Object>();
		boolean check = true;
		String msg = null;
		
		//查询当前资源是否使用(T_JDOA_SYS_PRIVILEGE)
		List<SysResource> resList = this.sysResourceService.findByParentId(sysResource.getId());
		if(resList!=null&&resList.size()>0){
			check = false;
			msg = "当前资源下存在子资源，无法删除！";
		} else {
			//查询当前资源是否存在子资源
			SysPrivilege entity = new SysPrivilege();
			entity.setResourceId(sysResource.getId());
			List<SysPrivilege> sysPrivileges = sysPrivilegeService.find(entity);
			if(sysPrivileges!=null && sysPrivileges.size()>0){
				check = false;
				msg = "当前资源已经使用，无法删除！";
			} else {
				SysResource model = this.sysResourceService.get(sysResource.getId());
				model.setStatus("1");
				this.sysResourceService.deleteResource(model);
			}
		}
		
		map.put("check", check);
		map.put("msg", msg);
		
		return JSONObject.fromObject(map).toString();
	}
	
	/**
	 * 整理树节点
	 * @param resources
	 * @return
	 */
	private List<TreeNode> getTreeNodes(List<SysResource> resources) {
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (SysResource resource : resources) {
			TreeNode treeNode = new TreeNode();
			if (resource.getChildrenNum() !=null && resource.getChildrenNum() > 0) {
				treeNode.setIsParent(true);
			} else {
				treeNode.setIsParent(false);
			}

			treeNode.setId(resource.getId());
			treeNode.setpId(resource.getParentId());
			treeNode.setName(resource.getResourceName());
			treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
			
			Map<String, Object> props = new HashMap<String, Object>();
			props.put("id", resource.getId());
			//增加节点的父节点
			props.put("parentid", resource.getParentId());
			treeNode.setProps(props);

			treeNodes.add(treeNode);
		}
		return treeNodes;
	}
}

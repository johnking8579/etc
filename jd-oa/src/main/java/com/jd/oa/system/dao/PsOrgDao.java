package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.PsOrg;


public interface PsOrgDao extends BaseDao<PsOrg,String>{
	
	void truncate();
	
	void add(PsOrg psOrg);
	
	void add(List<PsOrg> psOrgs);
	
	List<PsOrg> findAll();

}

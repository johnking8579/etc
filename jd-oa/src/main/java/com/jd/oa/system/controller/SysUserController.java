package com.jd.oa.system.controller;

import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.model.SysUserRole;
import com.jd.oa.system.service.SysPositionService;
import com.jd.oa.system.service.SysUserRoleService;
import com.jd.oa.system.service.SysUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yujiahe
 * Date: 13-9-9
 * Time: 下午1:35
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/system")
public class SysUserController {
    private static final Logger logger = Logger
            .getLogger(SysRoleController.class);
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    /**
     * @Description: 进入到查询用户详情页面
     * @author yujiahe
     */

    @RequestMapping(value = "/sysUser_view", method = RequestMethod.GET)
    public String sysUserGetById(@RequestParam String userId,Model model) {
        SysUser sysUser = sysUserService.get(userId);
        model.addAttribute("sysUser", sysUser);
        List<SysUserRole> sysUserRoleList = sysUserRoleService
                .getSysUserRoleListByUserId(userId);
        model.addAttribute("sysUserRoleList", sysUserRoleList);
        return "system/sysUser_view";
    }
    
    @RequestMapping(value = "/sysUserRole_showParent", method = RequestMethod.GET)
    public String sysUserParent(@RequestParam String userId,Model model){
    	
    	List<SysUser> users = new ArrayList<SysUser>();
    	users = sysUserService.getParentUser(userId);
    	 
    	 model.addAttribute("sysUsers", users);
    	 return "system/sysParentUser_view";
    }
}

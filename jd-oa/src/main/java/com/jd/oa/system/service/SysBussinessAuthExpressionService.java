package com.jd.oa.system.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysBussinessAuthExpression;

public interface SysBussinessAuthExpressionService  extends BaseService<SysBussinessAuthExpression, String>{
	 void deleteBusinessIdKeyAndExpressionId(String businessId,String expressionId);
	
	 void updateByResources(List<SysBussinessAuthExpression> sysBussinessAuthExpressions);
	 
	 void deleteByBusinessId(String businessId);
}

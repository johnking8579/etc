package com.jd.oa.system.dao;

import java.util.List;

import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;

public interface PeoplesoftDao {

	List<PsOrg> findAllOrg();
	
	List<PsPosition> findAllPosition();
	
	List<PsUser> findAllUser();

}

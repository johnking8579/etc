package com.jd.oa.system.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysOrganization;

public interface SysOrganizationDao extends BaseDao<SysOrganization, String> {
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 上午11:33:04
	 *
	 * @param mp
	 * @return
	 */
	public List<SysOrganization> selectOrgByParentId(Map mp);
	
	/**
	 * 依据组织架构类型获取其下的所有组织架构信息
	 * @param organizationType
	 * @return
	 */
	public List<SysOrganization> findByOrganizationType(String organizationType);
	/**
	 * 获取最大的ID
	 * @return
	 */
	public String getMaxId();

	public Map<String,String> getOrganizationLevelMap(String organizationFullPath);

	/**
	 * 查询在peoplesoft中存在, 在OA中不存在的记录
	 * @return
	 */
	public List<SysOrganization> findNotInPs();

}

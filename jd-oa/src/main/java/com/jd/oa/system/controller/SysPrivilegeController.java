/**
 * 
 */
package com.jd.oa.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.JDOAException;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.system.model.SysOperation;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.service.SysOperationService;
import com.jd.oa.system.service.SysPrivilegeService;
import com.jd.oa.system.service.SysResourceService;

/**
 * 资源功能 controller
 * 该类提供资源功能的查询,添加,修改,删除操作
 * @author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class SysPrivilegeController {
	private static final Logger logger = Logger.getLogger(SysPrivilegeController.class);
	
	 @Autowired(required=true)
	 private SysPrivilegeService sysPrivilegeService;
	 
	 @Autowired
	 private SysResourceService sysResourceService;
	 
	 @Autowired
	 private SysOperationService sysOperationService;
	 
	 private static List<SysOperation> sysOperations;
	 
	 /**
	  * 功能权限 
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/sysPrivilege_index", method = RequestMethod.GET)
	 public ModelAndView list() throws Exception {
		ModelAndView mav = new ModelAndView("system/sysPrivilege_index");
		TreeNode rootNode = new TreeNode();
		rootNode.setId("root");
		rootNode.setName("京东-办公自动化");
		rootNode.setIsParent(Boolean.TRUE);
		rootNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/1_close.png");
		rootNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/1_open.png");
			
		mav.addObject("treeNode", JsonUtils.toJsonByGoogle(rootNode));
		return mav;
	 }
	 /**
	  * 
	  * @param request
	  * @param level
	  * @param nodeId
	  * @param response
	  */
	 @RequestMapping(value = "/sysPrivilege_getTreeNodesJson", method = RequestMethod.POST)
	 @ResponseBody
	 public void sysResourceLoad(HttpServletRequest request,
			@RequestParam(value = "level", required = true) int level,
			@RequestParam(value = "nodeId", required = true) String nodeId,
			HttpServletResponse response) {
			 try {
				if (!nodeId.equals("undefined")) {
					nodeId = StringUtils.substring(nodeId,StringUtils.indexOf(nodeId, "_") + 1);
					List<SysResource> resources = null;
					resources = sysResourceService.findByParentId(nodeId);
					response.getWriter().write(JsonUtils.toJsonByGoogle(getTreeNodes(resources)));
				}
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
	}
	private List<TreeNode> getTreeNodes(List<SysResource> resources) {
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (SysResource resource : resources) {
			TreeNode treeNode = new TreeNode();
			treeNode.setIsParent(true);
			treeNode.setId(SysResource.class.getSimpleName() + "_"  + resource.getId());
			treeNode.setpId(resource.getParentId() != null ? SysResource.class.getSimpleName() + "_" + resource.getParentId() : "Application_1");
			treeNode.setName(resource.getResourceName());

			Map<String, Object> props = new HashMap<String, Object>();
			props.put("id", resource.getId());
			props.put("operations", sysOperations);
			props.put("url", resource.getResourceUrl());
			props.put("code", resource.getResourceCode());
			
			SysPrivilege entity = new SysPrivilege();
			entity.setResourceId(resource.getId());
			treeNode.setProps(props);
			
			treeNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			treeNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			
			treeNode.setProps(props);
				
			if( resource.getChildrenNum() != null && resource.getChildrenNum() > 0){
				treeNode.setIsParent(true);
			} else {
				treeNode.setIsParent(false);
			}
				
			treeNodes.add(treeNode);
			}
			return treeNodes;
	}
	 
	 /**
	  * 保存操作
	  * @param
	  * @return
	  * @throws JDOAException
	  */
	 @RequestMapping(value="/sysPrivilege_save",method=RequestMethod.POST)
	 @ResponseBody
	 private String save(SysPrivilege entity) throws JDOAException{
		 if(entity.getId() != null && entity.getId().trim().length() > 0){
			 sysPrivilegeService.update(entity);
		 } else {
			 sysPrivilegeService.insert(entity);
		 }
		 
		 Map<String,Object> map = new HashMap<String,Object>();
		 map.put("operator", true);
		 map.put("message", "保存成功");
		 return JSONObject.fromObject(map).toString();
	 }
	 /**
	  * 根据id 获取　SysPrivilege　对象.
	  * @param id
	  * @return
	  * @throws JDOAException
	  */
	 @RequestMapping(value="/sysPrivilege_edit",method=RequestMethod.GET)
	 private ModelAndView get(String id,String resourceCode,String resourceName) throws JDOAException{
		 ModelAndView mav = new ModelAndView("system/sysPrivilege_edit");
		 SysPrivilege sysPrivilege = sysPrivilegeService.get(id);
		 if(sysPrivilege != null){
			 SysOperation sysOperation = sysOperationService.get(sysPrivilege.getOperationId());
			 if(sysOperation != null){
				 mav.addObject("selectOperation", getOperationSelectTagHtml(sysOperation.getId()));
				 mav.addObject("operationCode", sysOperation.getOperationCode());
				 mav.addObject("code", sysPrivilege.getCode());
				 mav.addObject("name", sysPrivilege.getName());
				 mav.addObject("url", sysPrivilege.getUrl());
				 mav.addObject("id", id);
				 mav.addObject("operationId", sysPrivilege.getOperationId());
				 mav.addObject("resourceId", sysPrivilege.getResourceId());
			 }
		 }
		 mav.addObject("resourceCode", resourceCode);
		 mav.addObject("resourceName", resourceName);
		 return mav;
	 }
	 
	 @RequestMapping(value="/sysPrivilege_remove",method=RequestMethod.POST)
	 @ResponseBody
	 private String remove(String id) throws JDOAException{
		 SysPrivilege sysPrivilege = sysPrivilegeService.get(id);
		 if(sysPrivilege != null){
			 sysPrivilegeService.delete(sysPrivilege,true);
		 }
		 Map<String,Object> map = new HashMap<String,Object>();
		 map.put("operator", true);
		 map.put("message", "保存成功");
		 return JSONObject.fromObject(map).toString();
	 }
	 /**
	  * 打开 添加operation界面
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/sysPrivilege_add", method = RequestMethod.GET)
	 public ModelAndView addOperation(String resourceId,String resourceCode,String resourceName) throws Exception {
		 ModelAndView mav = new ModelAndView("system/sysPrivilege_add");
		 mav.addObject("selectOperation", this.getOperationSelectTagHtml());
		 mav.addObject("resourceId", resourceId);
		 mav.addObject("resourceCode", resourceCode);
		 mav.addObject("resourceName", resourceName);
		 mav.addObject("url", "");
		 
		 return mav;
	 }
	 /**
	  * 获取针对资源ID的功能有权限数据json
	  * @param request
	  * @param resourceId
	  * @return
	  * @throws Exception
	  */
	 @RequestMapping(value = "/getOperationDataJson", method = RequestMethod.GET)
	 @ResponseBody
	 public Object list(HttpServletRequest request,String resourceId) throws Exception {
		 PageWrapper<SysPrivilege> pageWrapper = new PageWrapper<SysPrivilege>(request);
		 pageWrapper.addSearch("resourceId",resourceId);
		 sysPrivilegeService.find(pageWrapper.getPageBean(),"findSysPrivilegeSysOperationVo",pageWrapper.getConditionsMap());
		 pageWrapper.addResult("returnKey","returnValue");
		 Map<String,Object> result = pageWrapper.getResult();
		 String json = JsonUtils.toJsonByGoogle(result);
	     logger.debug("result:"+json);
	     return json;
	 }
	 private String getOperationSelectTagHtml(){
		 return getOperationSelectTagHtml("");
	 }
	 /**
	  * 获取 选择select html标签片段
	  * @return
	  */
	 private String getOperationSelectTagHtml(String selectedValue){
		 if(sysOperations == null){
			sysOperations = sysOperationService.find(new SysOperation());
		 }
		 StringBuffer html = new StringBuffer();
		 if(sysOperations != null && sysOperations.size() > 0){
			 html.append("<select id=\"operationName\" class=\"\" onchange=\"selectChange(this)\">\n");
			 html.append("<option value=\"\">").append("请选择").append("</option>\n");
			 for(int  i = 0 ; i < sysOperations.size() ; i++){
				 SysOperation sysOperation = sysOperations.get(i);
				 if(sysOperation != null){
					 if(selectedValue.equals(sysOperation.getId())){
						 html.append("<option selected operationCode=\"").append(sysOperation.getOperationCode()).append("\" value=\"").append(sysOperation.getId()).append("\">").append(sysOperation.getOperationName()).append("</option>\n");
					 } else {
						 html.append("<option operationCode=\"").append(sysOperation.getOperationCode()).append("\" value=\"").append(sysOperation.getId()).append("\">").append(sysOperation.getOperationName()).append("</option>\n");
					 }
				 }
			 }
			 html.append("</select>\n");
		 }
		 return html.toString();
	 }
}

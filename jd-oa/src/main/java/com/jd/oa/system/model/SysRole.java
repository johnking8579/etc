package com.jd.oa.system.model;

import java.util.Date;

public class SysRole {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.ROLE_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String roleCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.ROLE_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String roleName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.ROLE_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String roleType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_JDOA_SYS_ROLE.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    private Date modifyTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.ID
     *
     * @return the value of T_JDOA_SYS_ROLE.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.ID
     *
     * @param id the value for T_JDOA_SYS_ROLE.ID
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.ROLE_CODE
     *
     * @return the value of T_JDOA_SYS_ROLE.ROLE_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.ROLE_CODE
     *
     * @param roleCode the value for T_JDOA_SYS_ROLE.ROLE_CODE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode == null ? null : roleCode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.ROLE_NAME
     *
     * @return the value of T_JDOA_SYS_ROLE.ROLE_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.ROLE_NAME
     *
     * @param roleName the value for T_JDOA_SYS_ROLE.ROLE_NAME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.ROLE_TYPE
     *
     * @return the value of T_JDOA_SYS_ROLE.ROLE_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getRoleType() {
        return roleType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.ROLE_TYPE
     *
     * @param roleType the value for T_JDOA_SYS_ROLE.ROLE_TYPE
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setRoleType(String roleType) {
        this.roleType = roleType == null ? null : roleType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.STATUS
     *
     * @return the value of T_JDOA_SYS_ROLE.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.STATUS
     *
     * @param status the value for T_JDOA_SYS_ROLE.STATUS
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.CREATOR
     *
     * @return the value of T_JDOA_SYS_ROLE.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.CREATOR
     *
     * @param creator the value for T_JDOA_SYS_ROLE.CREATOR
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.CREATE_TIME
     *
     * @return the value of T_JDOA_SYS_ROLE.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.CREATE_TIME
     *
     * @param createTime the value for T_JDOA_SYS_ROLE.CREATE_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.MODIFIER
     *
     * @return the value of T_JDOA_SYS_ROLE.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.MODIFIER
     *
     * @param modifier the value for T_JDOA_SYS_ROLE.MODIFIER
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_JDOA_SYS_ROLE.MODIFY_TIME
     *
     * @return the value of T_JDOA_SYS_ROLE.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_JDOA_SYS_ROLE.MODIFY_TIME
     *
     * @param modifyTime the value for T_JDOA_SYS_ROLE.MODIFY_TIME
     *
     * @mbggenerated Tue Aug 27 11:37:43 CST 2013
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
/**
 * 
 */
package com.jd.oa.system.controller;

import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.system.model.*;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liub
 * @Description: 公用的组织结构树封装
 *
 */
@Controller
@RequestMapping(value="/system")
public class SysAddressController {
	 private static final Logger logger = Logger.getLogger(SysAddressController.class);
	 @Autowired
	 private SysOrganizationService sysOrganizationService;
	 
	 @Autowired
	 private SysUserService sysUserService;
	 
	 @Autowired
	 private ProcessSearchService processSearchService;
	 
	 @Autowired
	 private SysAuthExpressionService sysAuthExpressionService;
	 
	 @Autowired
	 private UserAuthorizationService userAuthorizationService;
	 
	 
	 /** 
      * @Description: 进入到查询分配角色主页面
      * @author yujiahe
     */
	 @RequestMapping(value = "/sysAddress_getOrgUser", method = RequestMethod.GET)
	 public ModelAndView getOrgUser(String addressConfig,String processInstanceId,String isMultiSelect){
		 String url ="system/sysAddress_orgUser"; 
		 
		 //转发时选择转发人
		 if(processInstanceId != null){
			 ProcessDefinition processDefinition = processSearchService.findProcessDefByInstance(processInstanceId);		
				
			//取流程定义的加签人的表达式列表
			List<SysAuthExpression> list = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType( SysBussinessAuthExpression.BUSSINESS_TYPE_ADDSIGN, processDefinition.getId());
			
			if(list !=null && list.size() >0)
				url = "system/sysUser_getByProcessInstanceId";
			
			
		 
		 }
		 
		 ModelAndView mav = new ModelAndView(url);
		 mav.addObject("addressConfig", addressConfig);
		 mav.addObject("currentProcessInstanceId", processInstanceId);
		 if(isMultiSelect == null || isMultiSelect.trim().length() == 0){
			 isMultiSelect="0";
		 }
		 mav.addObject("isMultiSelect", isMultiSelect);
		 return mav;
		 
	 }
	 
	 @RequestMapping(value = "/sysUser_getByProcessInstanceId", method = RequestMethod.GET)
	 public ModelAndView sysUser_getByProcessInstanceId(String addressConfig){
		 ModelAndView mav = new ModelAndView("system/sysUser_getByProcessInstanceId");
		 return mav;
	 }
	 
	 @RequestMapping(value = "/sysAddress_getOrgDept", method = RequestMethod.GET)
	 public ModelAndView getOrgDept(boolean isMulti){
		 ModelAndView mav = new ModelAndView("system/sysAddress_orgDept");
		 mav.addObject("isMulti", isMulti);
		 return mav;
	 }
	 /**
	  * 服务端返回 tree node data json
	  * @param sysOrganization
	  * @return
	  */
	 @RequestMapping(value = "/sysAddress_getTreeNodeDataJson", method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public List<TreeNode> getTreeNodeData(SysOrganization sysOrganization){
	    Map mp = new HashMap();
	    mp.put("id", sysOrganization.getId());
	    List<SysOrganization> sysOrganizationList = sysOrganizationService.selectOrgByParentId(mp);
	    List<TreeNode> listData= this.getTreeNodes(sysOrganizationList);    //转化成Ztree支持的数据格式
	    return listData;
	 }
	 @RequestMapping(value = "/sysAddress_getOrgUserDataJson", method = RequestMethod.GET)
	 @ResponseBody
	 public Object list(HttpServletRequest request,String organizationId,String userName,String processInstanceId) throws Exception {
		 
		 if(processInstanceId == null){
			 PageWrapper<SysUser> pageWrapper = new PageWrapper<SysUser>(request);
			 
			 if(organizationId !=null && ! "".equals(organizationId)){
				SysOrganization organization = sysOrganizationService.get(organizationId) ; 
				List<String> allOrgIds = userAuthorizationService.getAllChildOrganizationByCode(organization.getOrganizationCode()) ;
				pageWrapper.addSearch("allOrgIds", allOrgIds);
			 }
			 
//			 pageWrapper.addSearch("organizationId",organizationId);
	         if( userName.contains("%")){
	             userName=userName.replace("%","\\%");
	         }
			 pageWrapper.addSearch("userName", userName);
			 sysUserService.find(pageWrapper.getPageBean(),"findBySysOrganizationId",pageWrapper.getConditionsMap());
			 pageWrapper.addResult("returnKey","returnValue");
			 String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			 
			 
//			List<SysUser> users  = getAddsignUsersByProcessInstanceId(processInstanceId, (List<SysUser>)pageWrapper.getResult().get("aaData"));
			
			 
		     logger.debug("result:"+json);
		     return json;
		     
		 }else{
			 
			 PageWrapper<SysUser> pageWrapper = new PageWrapper<SysUser>(request);
			 
			 int sStart = Integer.parseInt(request.getParameter("iDisplayStart")); 
			 int sLength = Integer.parseInt(request.getParameter("iDisplayLength")); 
			 List<SysUser> u = getAddsignUsersByProcessInstanceId(processInstanceId);
			 List<SysUser> users = null ;
			 if(userName != null && ! "".equals(userName)){
				 u = filterByUserName(u,userName);
			 	 
			 }
			 try{
				 users = u.subList(sStart, sStart+sLength);
			 }catch(IndexOutOfBoundsException e){
				 users = u.subList(sStart, u.size());
			 }
			 Map<String, Object> result = pageWrapper.getResultMap() ;
			 result.put("aaData", users);
			 result.put("iTotalRecords", u.size());
			 result.put("iTotalDisplayRecords", u.size());
			 result.put("current_page", sStart + 1);
			 return  JsonUtils.toJsonByGoogle(result);
		 }
	 }
	 
	 /**
	  * 获取层级tree node collections
	  * @param sysOrganizationList
	  * @return
	  */
	 private List<TreeNode> getTreeNodes(List<SysOrganization> sysOrganizationList){
	        List<TreeNode> treeNodes = new ArrayList<TreeNode>();
	        for(SysOrganization sysOrganization : sysOrganizationList){
	            TreeNode treeNode = new TreeNode();
	            treeNode.setId(sysOrganization.getOrganizationCode());
	            treeNode.setpId(sysOrganization.getParentId());
	            treeNode.setName(sysOrganization.getOrganizationName());
	            treeNode.setIsParent(sysOrganization.getIsParent());  
	            treeNode.setIconSkin(SysResource.class.getSimpleName().toLowerCase());
	            Map<String, Object> props = new HashMap<String, Object>();
	            props.put("id", sysOrganization.getId());
//	            props.put("parentId", sysOrganization.getParentId());
//	            props.put("orgFullname", sysOrganization.getOrganizationFullname());
	            props.put("sysOrganization", sysOrganization);
	            treeNode.setProps(props);
	            treeNodes.add(treeNode);
	        }
	        return treeNodes;
		}
    
	 
	 private List<SysUser> getAddsignUsersByProcessInstanceId(String processInstanceId){
			//渠道流程实例对应的流程定义id
			ProcessDefinition processDefinition = processSearchService.findProcessDefByInstance(processInstanceId);		
			if(processDefinition== null) return null;
			
			//取流程定义的加签人的表达式列表
			List<SysAuthExpression> list = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType( SysBussinessAuthExpression.BUSSINESS_TYPE_ADDSIGN, processDefinition.getId());
			
			//获取所有用户
//			List<SysUser> u = userAuthorizationService.getUserListBySysAuthExpressionList(list);
			List<SysUser> u2 = userAuthorizationService.getUserListByMultiSysAuthExpressionList(list);
			
			return u2;
	 }
	 
	 private List<SysUser> filterByUserName(List<SysUser> users,String name){
		 List<SysUser> temp = new ArrayList<SysUser>();
		 if(users != null && users.size() > 0){
			 for (SysUser sysUser : users) {
				if( (sysUser.getUserName() != null && sysUser.getUserName().indexOf(name) >= 0 ) || ( sysUser.getRealName() != null && sysUser.getRealName().indexOf(name) >= 0  )){

					temp.add(sysUser);
				}
			}
			 
		 }
		 
		 return temp;
	 }
}


package com.jd.oa.system.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysRole;

/**
 * @author yujiahe
 *
 */
public interface SysRoleService extends BaseService<SysRole, String>{
	public SysRole showSysRole(String sysroleId);
	
	/**
	 * 根据用户名获取用户的关联角色列表信息
	 * @param userId 用户ID
	 * 
	 * @author zhouhuaqi
	 * @date 2013-09-02 15:11:00
	 * @version V1.0
	 */
	public List<SysRole> findByUserId(String userId);
    /**
     * @Description: 根据角色类型返回角色列表
     * @return List<SysRole>
     * @author yujiahe
     * @version V1.0
     */
    public List<SysRole> findSysRoleList();
    /**
     * @Description: 查找用户不属于的角色列表
     * @return List<SysRole>
     * @author yujiahe
     * @version V1.0
     */
    public List<SysRole> findUnUseSysRoleListByUserId(String userId);
	
	/**
	 * 
	 * @desc 获取所有角色
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	public List<SysRole> findRoleList();
	
	/**
	 * 
	 * @desc 获取所有用户组
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	public List<SysRole> findGroupList();
    /**
     * @desc 根据名称查询角色（用户组）列表
     * @author YJH
     * @return
     */
    public List<SysRole> findSysRoleByName(String roleName);
}


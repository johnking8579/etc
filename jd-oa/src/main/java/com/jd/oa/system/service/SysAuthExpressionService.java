package com.jd.oa.system.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysAuthExpression;

public interface SysAuthExpressionService extends BaseService<SysAuthExpression, String>{
	//删除未引用的权限表达式
	 List<SysAuthExpression>  deleteOnlyUnused(String ql, Object... values);
	 
	//根据授权表达式ID获取所有其详细的记录
	List<SysAuthExpression> findAuthExpressionListByIds(final String... values);
		
	//根据授权表达式对象获取所有其详细的记录
	List<SysAuthExpression> findAuthExpressionListByIds(final SysAuthExpression... values);
	
	 /**
	 * 
	 * @desc 获得指定业务数据对应的表达式列表
	 * @author WXJ
	 * @date 2013-9-25 上午11:29:20
	 *
	 * @param businessType
	 * @param businessId
	 * @return
	 */
	List<SysAuthExpression> findExpressionByBusinessIdAndBusinessType(String businessType,
			String businessId);
}

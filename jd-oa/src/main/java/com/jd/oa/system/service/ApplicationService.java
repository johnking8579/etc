package com.jd.oa.system.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.Application;

public interface ApplicationService extends BaseService<Application, Long>{
}

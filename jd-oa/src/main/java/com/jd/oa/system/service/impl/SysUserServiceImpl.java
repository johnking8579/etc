package com.jd.oa.system.service.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.system.dao.SysOrganizationDao;
import com.jd.oa.system.dao.SysPositionDao;
import com.jd.oa.system.dao.SysRoleDao;
import com.jd.oa.system.dao.SysUserDao;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

@Service("sysUserService")
//默认将类中的所有函数纳入事务管理.
@Transactional
@CacheProxy
public class SysUserServiceImpl extends BaseServiceImpl<SysUser, String>
		implements SysUserService {

	@Autowired
	private SysUserDao sysUserDao;
	
	@Autowired
	private SysRoleDao sysRoleDao;
	
	@Autowired
	private SysOrganizationDao sysOrganizationDao;
	
	@Autowired
	private SysPositionDao sysPositionDao;

	public SysUserDao getDao() {
		return sysUserDao;
	}

	public SysUser getByUserName(String userName) {
		return getByUserName(userName, "1");
	}
	
	@CachePut(key = @CacheKey(template = "JDOA/Users/${p0}/${p1}"), expire = 3600)
	public SysUser getByUserName(String userName, String organizationType) {
		return getDao().getByUserName(userName, organizationType);
	}
	
	public SysUser getByEmailAndMobile(String email, String mobile) {
		return getByEmailAndMobile(email, mobile, "1");
	}
	
	public SysUser getByEmailAndMobile(String email, String mobile, String organizationType) {
		return getDao().getByEmailAndMobile(email, mobile, organizationType);
	}
	
	public String getUserIdByUserName(String userName) {
		return getUserIdByUserName(userName, "1");
	}

	public String getUserIdByUserName(String userName, String organizationType) {
		SysUser sysUser = getByUserName(userName);
		return sysUser.getId();
	}

	@Override
	public String getRealName(String userName) {
		SysUser sysUser = getByUserName(userName);
		return sysUser.getRealName();
	}

	
	public SysUser getUserMailAndPhoneByUserName(String userName, String organizationType) {
		return getDao().getUserMailAndPhoneByUserName(userName, organizationType);
	}
	
	@Override
	public boolean isAdministrator(String userName) {
		SysUser sysUser = getByUserName(userName);
		boolean result = false;
		if(sysUser != null){
			List<SysRole> roles = sysRoleDao.findByUserId(sysUser.getId());
			for(SysRole role : roles){
				if(role.getRoleName().contains("管理员")){
					result = true;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public boolean isPowerUser(String userName) {
		SysUser sysUser = getByUserName(userName);
		boolean result = false;
		if(sysUser != null){
			List<SysRole> roles = sysRoleDao.findByUserId(sysUser.getId());
			for(SysRole role : roles){
				if(role.getRoleName().equals("Power User")){
					result = true;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public void transferUserInfoFromHR(List<SysUser> allUser) {
		//待插入的用户
		List<SysUser> insertUserList = new ArrayList<SysUser>();
		//待更新的用户
		List<SysUser> updateUserList = new ArrayList<SysUser>();
		//待删除的用户
		List<SysUser> deleteUserList = new ArrayList<SysUser>();
				
		//全量读取EHR的人员信息
		//List<SysUser> allUser = OtherDbUtil.getAllUserFromEHR();
		
		//组织架构信息
		List<SysOrganization> allOrganization = this.sysOrganizationDao.findByOrganizationType(SystemConstant.ORGNAIZATION_TYPE_HR);
		Map<String,String> mapOrg = new HashMap<String, String>();
		if(allOrganization != null && allOrganization.size() > 0){
			for(SysOrganization bean : allOrganization){
				mapOrg.put(bean.getOrganizationCode(), bean.getId());
			}
		}
		
		//获得oa系统中的所有用户
		List<SysUser> oaAllUser = this.sysUserDao.find(null);
		Map<String,SysUser> oaMap = new HashMap<String,SysUser>();
		if(oaAllUser != null && oaAllUser.size() > 0){
			for(SysUser oaSysUser : oaAllUser){
//				if(!ehrMap.containsKey(oaSysUser.getUserCode())){
//					//整理新读取的信息中不包含的用户
//					deleteUserList.add(oaSysUser);
//				}
				oaMap.put(oaSysUser.getUserCode(), oaSysUser);
			}
		}
		
		//遍历从ehr数据库中获取的全部用户
		//结果以userCode为key的map对象
		Map<String,SysUser> ehrMap = new HashMap<String,SysUser>();
		if(allUser != null && allUser.size() > 0){
			for(SysUser ehrSysUser : allUser){
				//整理新读取的信息中不包含的用户
				/*if ("301".equals(ehrSysUser.getStatus()) || "302".equals(ehrSysUser.getStatus())){
					if (oaMap.containsKey(ehrSysUser.getUserCode())){
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>userName[" + ehrSysUser.getRealName() + "],userCode[" + ehrSysUser.getUserCode() + "]用户离职，执行删除操作<<<<<<<<<<<<<<<<<<<<");
						logger.info("userName["+ ehrSysUser.getRealName() + "],userCode[" + ehrSysUser.getUserCode() + "]用户离职，执行删除操作");
						SysUser su = oaMap.get(ehrSysUser.getUserCode());
						su.setStatus("1");
						deleteUserList.add(oaMap.get(ehrSysUser.getUserCode()));						
					}
				}else{
					ehrSysUser.setOrganizationId(mapOrg.get(ehrSysUser.getOrganizationId()));
					ehrSysUser.setStatus("0");
					ehrMap.put(ehrSysUser.getUserCode(), ehrSysUser);
				}*/
				ehrSysUser.setOrganizationId(mapOrg.get(ehrSysUser.getOrganizationId()));
				ehrMap.put(ehrSysUser.getUserCode(), ehrSysUser);
			}
		}
		
//		for(SysUser newBean : allUser){
//			if(oaMap.containsKey(newBean.getUserCode())){//oaMap中存在该usercode,代表该用户已存在，进行更新操作，反之进行插入操作
////				if(!oaMap.get(newBean.getUserCode()).getOrganizationId().equals(newBean.getOrganizationId())){
////					oaMap.get(newBean.getUserCode()).setOrganizationId(newBean.getOrganizationId());
////				}
//				SysUser erhSysUser = ehrMap.get(newBean.)
//				updateUserList.add(oaMap.get(newBean.getUserCode()));
//			} else {
//				insertUserList.add(newBean);
//			}
//		}
		Iterator iterator = ehrMap.entrySet().iterator();
		String excludsProperty[] = {"ID"};
		String changeExcludsProperty[] = {"ID","userCode","creator","createTime","modifier","modifyTime"};
		while(iterator.hasNext()) { 
		    Entry entry = (Entry)iterator.next(); 
		    String key = entry.getKey().toString();
		    SysUser ehrSysUser = (SysUser)ehrMap.get(key); 
		    if(ehrSysUser!=null){
		    	 /*if ("301".equals(ehrSysUser.getStatus()) || "302".equals(ehrSysUser.getStatus())){
				    	continue;
				    }*/
				    if(oaMap.containsKey(key)){//oaMap中存在该usercode,代表该用户已存在，进行更新操作，反之进行插入操作
				    	 SysUser oaSysUser = oaMap.get(key);
						 try {
							 System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>userName[" + oaSysUser.getRealName() + "],userCode[" + oaSysUser.getUserCode() + "]用户已存在<<<<<<<<<<<<<<<<<<<<");
							 logger.info("userName["+ oaSysUser.getRealName() + "],userCode[" + oaSysUser.getUserCode() + "]用户已存在");
							 if(ehrSysUser != null && oaSysUser != null && !checkSysUserPropertyChange(ehrSysUser,oaSysUser,changeExcludsProperty)){
								 System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>userName[" + oaSysUser.getRealName() + "],userCode[" + oaSysUser.getUserCode() + "]用户已存在，ehr数据已变更,执行更新操作<<<<<<<<<<<<<<<<<<<<");
								 logger.info("userName["+ oaSysUser.getRealName() + "],userCode[" + oaSysUser.getUserCode() + "]用户已存在，ehr数据已变更,执行更新操作");
								 ComUtils.copyPropertiesExclude(ehrSysUser, oaSysUser, excludsProperty);
								 updateUserList.add(oaSysUser);
							 }
						} catch (Exception e) {
							e.printStackTrace();
						}
				    } else {
				    	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>userName[" + ehrSysUser.getRealName() + "],userCode[" + ehrSysUser.getUserCode() + "]用户不存在，执行新增操作<<<<<<<<<<<<<<<<<<<<");
				    	logger.info("userName["+ ehrSysUser.getRealName() + "],userCode[" + ehrSysUser.getUserCode() + "]用户不存在，执行新增操作");
				    	insertUserList.add(ehrSysUser);
				    }
		    }
		   
	    }
		if(insertUserList != null && insertUserList.size() > 0){
			this.sysUserDao.insert(insertUserList);
		}
		if(updateUserList != null && updateUserList.size() > 0){
			this.sysUserDao.update(updateUserList);
		}
		
		//delete不存在的用户（排除lczxgly）
		for(int j = 0 ; j < oaAllUser.size() ; j++){
      		SysUser user = oaAllUser.get(j);
      		if(user != null && !"lczxgly".equals(user.getUserName())){
      			if(ehrMap.get(user.getUserCode()) == null){
      				System.out.println("\n>>>>>>>>>>>>>>>>>>> delete " + user.getUserName() + "<<<<<<<<<<<<<<<<<<<<<<<<");
      				logger.info("userName["+ user.getRealName() + "],userCode[" + user.getUserCode() + "]用户离职，执行删除操作");
      				user.setStatus("1");
      				this.update(user);
      			}
      		}
      	}
		/*if(deleteUserList != null && deleteUserList.size() > 0){
			//this.sysUserDao.delete(deleteUserList,true);
			this.sysUserDao.update(deleteUserList);
		}*/
	}
	/**
	 * 检查两个相同对象的property数据是否一直，一直返回true,否则返回false
	 * @param ehrSysUser
	 * @param oaSysUser
	 * @return
	 */
	private boolean checkSysUserPropertyChange(SysUser ehrSysUser,SysUser oaSysUser, String[] includsArray) throws Exception{
		List<String> includesList = null;  
	    if(includsArray != null && includsArray.length > 0) {  
	          includesList = Arrays.asList(includsArray); //构造列表对象  
	    }  
		Method[] fromMethods = ehrSysUser.getClass().getDeclaredMethods();  
        Method[] toMethods = oaSysUser.getClass().getDeclaredMethods();  
        Method fromMethod = null, toMethod = null;  
        String fromMethodName = null, toMethodName = null;  
        for (int i = 0; i < fromMethods.length; i++) {  
            fromMethod = fromMethods[i];  
            toMethod = toMethods[i];
            fromMethodName = fromMethod.getName();  
            if (!fromMethodName.contains("get")){
            	continue;  
            }
           //排除列表检测  
            String str = fromMethodName.substring(3);  
            if(includesList.contains(str.substring(0,1).toLowerCase() + str.substring(1))) {  
                continue;  
            }  
            toMethod = ComUtils.findMethodByName(toMethods, fromMethodName);  
            if (toMethod == null) {
            	continue;  
            }
            Object ehrValue = fromMethod.invoke(ehrSysUser, new Object[0]);  
            Object oaValue = toMethod.invoke(oaSysUser, new Object[0]);
            
            if((ehrValue != null && oaValue == null) || (ehrValue == null && oaValue != null)){
            	return false;
            }
            if(ehrValue != null && oaValue != null && !ehrValue.equals(oaValue)){
            	return false;
            }  
        }  
		return true;
	}

	/**
	 * 取回用户的一级部门，二级部门，三级部门等
	 * @param userErp
	 * @return
	 */
	@Override
	public Map<String, String> getOrganizationMap(String userErp) {
		SysUser sysUser = this.getByUserName(userErp);
		return getOrganizationMap(sysUser);
	}

	/**
	 * 取回用户的一级部门，二级部门，三级部门等
	 * @param sysUser
	 * @return
	 */
	@Override
	public Map<String, String> getOrganizationMap(SysUser sysUser) {
		
		String userErp = "";
		if(sysUser!=null){
			userErp = sysUser.getUserName();
		}else{
			throw new BusinessException("没有查到相关ERP信息");
		}
		if(sysUser.getOrganizationId() == null || sysUser.getOrganizationId().equals("")) throw new BusinessException("ERP["+userErp+"]用户部门信息为空");
		return getOrganizationMapByOrganizationId(sysUser.getOrganizationId());
	}

	public Map<String, String> getOrganizationMapByOrganizationId(String sysOrganizationId){
		SysOrganization sysOrganization = sysOrganizationDao.get(sysOrganizationId);
		if(sysOrganization == null ) throw new BusinessException("没有查到部门["+sysOrganizationId+"]的相关信息");
		return sysOrganizationDao.getOrganizationLevelMap(sysOrganization.getOrganizationFullPath());
	}
	
	@Override
	public List<SysUser> getParentUser(String userId){
		List<SysUser> parentUsers = new ArrayList<SysUser>();
		SysUser sysUser = sysUserDao.get(userId);
		String parentPositionCode = sysUser.getPositionParentCode();
		
		if(null != parentPositionCode){
			parentUsers = this.selectParentUserResult(parentPositionCode);
		}
		return parentUsers;
	}
	
	/**
	 * 递归查询上级
	 * 
	 * @param positionCode
	 * @return
	 */
	private List<SysUser> selectParentUserResult(String positionCode){
		List<SysUser> users = new ArrayList<SysUser>();
		SysPosition pos = new SysPosition();
   	 	pos.setPositionCode(positionCode);
   	    List<SysPosition> poslist = this.sysPositionDao.find(pos);
   	    if(null != poslist && !poslist.isEmpty()){
   	    	SysUser user = new SysUser();
   	    	user.setPositionCode(positionCode);
   	    	users = sysUserDao.find(user);
   	    	if(null == users || users.isEmpty()){//递归处理，如果直接上级没有，再查上级的上级
   	    		String parentPositionCode = poslist.get(0).getParentId();
   	    		if(null != parentPositionCode){
   	    			users = selectParentUserResult(parentPositionCode);
   	    		}
   	    	}
   	    }
   	    return users;
	}
	

}

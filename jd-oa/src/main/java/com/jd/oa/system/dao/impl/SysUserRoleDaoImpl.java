/**
 * 
 */
package com.jd.oa.system.dao.impl;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysUserRoleDao;
import com.jd.oa.system.model.SysUserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujiahe
 * @date 2013年8月30日
 *
 */
@Component("sysUserRoleDao")
public class SysUserRoleDaoImpl extends MyBatisDaoImpl<SysUserRole, Long>
implements SysUserRoleDao {
    /**
     * @Description: 新增用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public void insertSysUserRole(SysUserRole sysUserRole){
        this.getSqlSession().selectList("com.jd.oa.system.model.SysUserRole.insert",sysUserRole);
    }
    /**
     * @Description: 根据用户ID获取用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public List<SysUserRole> findSysUserRoleListByUserId(String userId) {
        List<SysUserRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysUserRole.findSysUserRoleListByUserId",userId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    /**
     * @Description: 根据D用户ID，删除用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    public int deleteUserRoleByUserId(String userId) {
        int result;
        try {
            result = this.getSqlSession().delete("com.jd.oa.system.model.SysUserRole.deleteUserRoleByUserId",userId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    /**
     * @Description: 根据角色ID用户ID，删除用户角色配置
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    @Override
    public int deleteSysUserRoleByUR(SysUserRole sysUserRole) {
        int result;
        try {
            result = this.getSqlSession().delete("com.jd.oa.system.model.SysUserRole.deleteUserRoleByUR",sysUserRole);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    /**
     * @Description: 根据角色ID查找用户角色配置列表
     * @return String
     * @author yujiahe
     * @version V1.0
     */
    @Override
    public List<SysUserRole> findSysUserRoleListByRoleId(String roleId) {
        List<SysUserRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysUserRole.findSysUserRoleListByRoleId",roleId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    /**
     * 根据角色查询到用户列表
     * @param roleName
     * @return
     */
    @Override
    public List<SysUserRole> findUserListByRoleName(String roleName) {
        List<SysUserRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysUserRole.findUserListByRoleName",roleName);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }

    @Override
	public List<SysUserRole> findByUserId(String userId) {
		// TODO Auto-generated method stub
		List<SysUserRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysUserRole.findByUserId",userId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}


}

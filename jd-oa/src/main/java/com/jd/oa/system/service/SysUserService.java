package com.jd.oa.system.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.system.model.SysUser;

public interface SysUserService extends BaseService<SysUser, String> {

	/**
	 * 根据用户名获取用户信息
	 * 
	 * @param userName
	 *            用户名
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-09-02 13:23:00
	 * @version V1.0
	 */
	public SysUser getByUserName(String userName);

	/**
	 * 根据用户名和组织机构类别获取用户信息
	 * 
	 * @param userName
	 *            用户名
	 * @param organizationType
	 *            组织机构类型
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-10-23 10:07:00
	 * @version V1.0
	 */
	public SysUser getByUserName(String userName, String organizationType);

	/**
	 * 根据邮箱、手机获取用户信息
	 * 
	 * @param email
	 *            邮箱
	 * @param mobile
	 *            手机
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-11-20 23:48:00
	 * @version V1.0
	 */
	public SysUser getByEmailAndMobile(String email, String mobile);
	
	/**
	 * 根据邮箱、手机和组织机构类别获取用户信息
	 * 
	 * @param email
	 *            邮箱
	 * @param mobile
	 *            手机
	 * @param organizationType
	 *            组织机构类型
	 * @return 用户信息
	 * 
	 * @author zhouhuaqi
	 * @date 2013-11-20 23:48:00
	 * @version V1.0
	 */
	public SysUser getByEmailAndMobile(String email, String mobile, String organizationType);
	/**
	 * 根据用户名获取用户ID
	 * 
	 * @param userName
	 *            用户名，erpId
	 * @return
	 */
	public String getUserIdByUserName(String userName);

	/**
	 * 根据用户名和组织机构类型获取用户ID
	 * 
	 * @param userName
	 *            用户名，erpId
	 * @param organizationType
	 *            组织机构类型
	 * @return
	 */
	public String getUserIdByUserName(String userName, String organizationType);
	
	/**
	 * 根据用户名获取用户的真实姓名
	 * @param userName 用户名，erpId
	 * @return
	 */
	public String getRealName(String userName);
	
	/**
	 * 
	 * 判断用户是否是管理员用户
	 * @param userName 用户名，erpId
	 * @return
	 */
	public boolean isAdministrator(String userName);
	
	/**
	 * 
	 * 判断用户是否是Power User
	 * @param userName 用户名，erpId
	 * @return
	 */
	public boolean isPowerUser(String userName);

	
	/**
	 * 根据用户编码和组织机构类型获取用户mail和mobile
	 * @param userName 用户名称，erpId
	 * @param organizationType 组织机构类型
	 * @return
	 */
	public SysUser getUserMailAndPhoneByUserName(String userName, String organizationType);
	
	/**
	 * 定时任务：传输EHR人员信息到OA
	 */
	public void transferUserInfoFromHR(List<SysUser> allUser);

	/**
	 * 取回用户的一级部门，二级部门，三级部门等
	 * @param userErp
	 * @return
	 */
	public Map<String,String> getOrganizationMap(String userErp);
	/**
	 * 取回用户的一级部门，二级部门，三级部门等
	 * @param sysUser
	 * @return
	 */
	public Map<String,String> getOrganizationMap(SysUser sysUser);
	
	/**
	 * 得到当前用户的上级直属领导信息
	 * 
	 * @param userId
	 * @return
	 */
	public List<SysUser> getParentUser(String userId);

}
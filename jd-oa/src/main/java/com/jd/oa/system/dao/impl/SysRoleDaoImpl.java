/**
 * 
 */
package com.jd.oa.system.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.system.dao.SysRoleDao;
import com.jd.oa.system.model.SysRole;
import com.jd.oa.system.model.SysUser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujiahe
 *
 */
@Component("sysRoleDao")
public class SysRoleDaoImpl extends MyBatisDaoImpl<SysRole, String>
		implements SysRoleDao {
	public SysRole showSysRole(String sysroleId){
		SysRole result = null;
	        try {
	            result = (SysRole) this.getSqlSession().selectOne("com.jd.oa.system.model.SysRole.selectByPrimaryKey", sysroleId);
	        } catch (DataAccessException e) {
	            throw e;
	        }
	        return result;
	}

	@Override
	public List<SysRole> findByUserId(String userId) {
		return getSqlSession().selectList(sqlMapNamespace +".findByUserId",userId);
	}

	/**
	 * 
	 * @desc 获取所有角色
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	@Override
	public List<SysRole> findRoleList() {
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList("com.jd.oa.system.model.SysRole.findRoleList");
	}
	
	/**
	 * 
	 * @desc 获取所有用户组
	 * @author WXJ
	 * @date 2013-9-3 下午05:33:34
	 *
	 * @return
	 */
	@Override
	public List<SysRole> findGroupList() {
		// TODO Auto-generated method stub
		return this.getSqlSession().selectList("com.jd.oa.system.model.SysRole.findGroupList");
	}
    /**
     *
     * @desc 获取用户不属于的角色
     * @author YJH
     *
     * @return
     */
    @Override
    public List<SysRole> findUnUseSysRoleListByUserId(String userId) {
        List<SysRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysRole.findUnUseSysRoleListByUserId",userId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    /**
     * @desc 根据名称查询角色（用户组）列表
     * @author YJH
     * @return
     */
    @Override
    public List<SysRole> findSysRoleByName(String roleName) {
        List<SysRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysRole.findSysRoleByName",roleName);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }

    public List<SysRole> findSysRoleList(){
        List<SysRole> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.system.model.SysRole.selectByRoleType");
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
}
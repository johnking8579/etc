/**
 * 
 */
package com.jd.oa.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.JDOAException;
import com.jd.oa.common.tree.TreeNode;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.system.model.SysOperation;
import com.jd.oa.system.model.SysPrivilege;
import com.jd.oa.system.model.SysResource;
import com.jd.oa.system.model.SysRolePrivilege;
import com.jd.oa.system.service.SysOperationService;
import com.jd.oa.system.service.SysPrivilegeService;
import com.jd.oa.system.service.SysResourceService;
import com.jd.oa.system.service.SysRolePrivilegeService;

/**
 * 角色功能权限 controller
 * 该类提供授权的查询,添加,修改,删除操作
 * @author liub
 *
 */
@Controller
@RequestMapping(value="/system")
public class SysRolePrivilegeController {
	private static final Logger logger = Logger.getLogger(SysRolePrivilegeController.class);
	
	 @Autowired(required=true)
	 private SysRolePrivilegeService sysRolePrivilegeService;
	 
	 @Autowired(required=true)
	 private SysPrivilegeService sysPrivilegeService;
	 
	 @Autowired
	 private SysResourceService sysResourceService;
	 
	 @Autowired
	 private SysOperationService sysOperationService;
	 
	 private static List<SysOperation> sysOperations;
	 
	 @RequestMapping(value = "/sysRolePrivilege_index", method = RequestMethod.GET)
	 public ModelAndView list(String sysRoleId) throws Exception {
		ModelAndView mav = new ModelAndView("system/sysRolePrivilege");
		TreeNode rootNode = new TreeNode();
		rootNode.setId("root");
		rootNode.setName("京东-办公自动化");
		rootNode.setIsParent(Boolean.TRUE);
		rootNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/1_close.png");
		rootNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/1_open.png");
			
		mav.addObject("sysRoleId", sysRoleId);
		mav.addObject("treeNode", JsonUtils.toJsonByGoogle(rootNode));
		return mav;
	 }
	 /**
	  * 
	  * @param request
	  * @param level
	  * @param nodeId
	  * @param response
	  */
	 @RequestMapping(value = "/sysRolePrivilege_getTreeNodesJson", method = RequestMethod.POST)
	 @ResponseBody
	 public void sysResourceLoad(HttpServletRequest request,
			 @RequestParam(value = "sysRoleId", required = true) String sysRoleId,
			@RequestParam(value = "level", required = true) int level,
			@RequestParam(value = "nodeId", required = true) String nodeId,
			HttpServletResponse response) {
			 try {
				if (!nodeId.equals("undefined")) {
					nodeId = StringUtils.substring(nodeId,StringUtils.indexOf(nodeId, "_") + 1);
					List<SysResource> resources = null;
					resources = sysResourceService.findByParentId(nodeId);
					response.getWriter().write(JsonUtils.toJsonByGoogle(getTreeNodes(resources,sysRoleId)));
				}
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
	}
	private List<TreeNode> getTreeNodes(List<SysResource> resources,String sysRoleId) {
		if(sysOperations == null){
			sysOperations = sysOperationService.find(new SysOperation());
		}
		List<TreeNode> treeNodes = new ArrayList<TreeNode>();
		for (SysResource resource : resources) {
			TreeNode treeNode = new TreeNode();
			treeNode.setIsParent(true);
			treeNode.setId(SysResource.class.getSimpleName() + "_"  + resource.getId());
			treeNode.setpId(resource.getParentId() != null ? SysResource.class.getSimpleName() + "_" + resource.getParentId() : "Application_1");
			treeNode.setName(resource.getResourceName());

			Map<String, Object> props = new HashMap<String, Object>();
			props.put("id", resource.getId());
			props.put("url", resource.getResourceUrl());
			props.put("code", resource.getResourceCode());
			
			SysPrivilege entity = new SysPrivilege();
			entity.setResourceId(resource.getId());
			List<SysPrivilege> sysPrivileges = sysPrivilegeService.find(entity);
			props.put("privileges", sysPrivileges);
			
			props.put("operations", getSysResourceForSysOperation(sysPrivileges));
			//设置是否选中
			if(isTreeNodeChecked(sysPrivileges, sysRoleId)){
				treeNode.setChecked(true);
			}
			treeNode.setProps(props);
			
			treeNode.setIconClose("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			treeNode.setIconOpen("../static/zTree/css/zTreeStyle/img/diy/sysresource.png");
			
			treeNode.setProps(props);
				
			if(resource.getChildrenNum() > 0){
				treeNode.setIsParent(true);
			} else {
				treeNode.setIsParent(false);
			}
				
			treeNodes.add(treeNode);
			}
			return treeNodes;
	}
	private List<SysOperation> getSysResourceForSysOperation(List<SysPrivilege> sysPrivileges){
		List<SysOperation> oper = new ArrayList<SysOperation>();
		for(int i = 0 ; i < sysPrivileges.size() ; i++){
			SysPrivilege sysPrivilege = sysPrivileges.get(i);
			if(sysPrivilege != null){
				for(int j = 0 ; j < sysOperations.size() ; j++){
					SysOperation sysOperation = sysOperations.get(j);
					if(sysOperation != null){
						if(sysPrivilege.getOperationId().equals(sysOperation.getId())){
							oper.add(sysOperation);
							break;
						}
					}
				}
			}
		}
		return oper;
	}
	/**
	 * 判断treeNode是否选中
	 * @param sysPrivileges
	 * @param sysRoleId
	 * @return
	 */
	private boolean isTreeNodeChecked(List<SysPrivilege> sysPrivileges,String sysRoleId){
		 SysRolePrivilege entity = new SysRolePrivilege();
		 entity.setRoleId(sysRoleId);
		 List<SysRolePrivilege> records = sysRolePrivilegeService.find(entity);
		 
		 for(int i = 0 ; i < sysPrivileges.size() ; i++){
			 SysPrivilege sysPrivilege = sysPrivileges.get(i);
			 if(sysPrivilege != null){
				 for(int j = 0 ; j < records.size() ; j++){
					 SysRolePrivilege sysRolePrivilege = records.get(j);
					 if(sysRolePrivilege != null){
						 if(sysPrivilege.getId().equals(sysRolePrivilege.getPrivilegeId())){
							 return true;
						 }
					 }
				 }
			 }
		 }
		 return false;
	}
	 
	 /**
	  * 对授权信息进行保存
	  * @return
	  */
	 @RequestMapping(value="/sysRolePrivilege_save",method=RequestMethod.POST)
	 @ResponseBody
	 private String save(String sysRoleId,String operationsJson) throws JDOAException{
		 SysRolePrivilege deletEntity = new SysRolePrivilege();
		 deletEntity.setRoleId(sysRoleId);
		 sysRolePrivilegeService.delete(deletEntity,true);
		 JSONArray jsonArray = JSONArray.fromObject(operationsJson);
		 for(int i = 0 ; i < jsonArray.size() ; i++){
			 JSONArray jsonArray1 = JSONArray.fromObject(jsonArray.get(i));
			 for(int j = 0 ; j < jsonArray1.size() ; j++){
				 JSONObject jsonObject = jsonArray1.getJSONObject(j);  
				 String resourceId = String.valueOf(jsonObject.get("resourceId"));//资源ID
				 JSONArray jsonArray2 = JSONArray.fromObject(jsonObject.get("checkedResourceOperationArray"));
				 for(int k = 0 ; k < jsonArray2.size() ; k++){
					 JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
					 //操作数据
					 String operationId = String.valueOf(jsonObject2.get("operationId"));
					 SysPrivilege sysPrivilege = new SysPrivilege();
					 sysPrivilege.setResourceId(resourceId);
					 sysPrivilege.setOperationId(operationId);
					 List<SysPrivilege> records = sysPrivilegeService.find(sysPrivilege);
					 if(records != null && records.size() > 0){
						 String privilegeId = records.get(0).getId();
						 
						 SysRolePrivilege sysRolePrivilege = new SysRolePrivilege();
						 sysRolePrivilege.setPrivilegeId(privilegeId);
						 sysRolePrivilege.setRoleId(sysRoleId);
						 
						 sysRolePrivilegeService.insert(sysRolePrivilege);//写入功能权限表
					 }
				 }
			 }
    	 }
		 Map<String,Object> map = new HashMap<String,Object>();
		 map.put("operator", true);
		 map.put("message", "授权成功");
		 return JSONObject.fromObject(map).toString();
	 }
}

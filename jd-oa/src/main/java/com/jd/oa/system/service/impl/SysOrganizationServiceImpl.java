package com.jd.oa.system.service.impl;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.system.dao.SysOrganizationDao;
import com.jd.oa.system.dao.SysPositionDao;
import com.jd.oa.system.dao.SysUserDao;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("sysOrganizationService")

@Transactional
public class SysOrganizationServiceImpl extends BaseServiceImpl<SysOrganization, String> implements SysOrganizationService{
	@Autowired
	private SysOrganizationDao sysOrganizationDao;
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysPositionDao sysPositionDao;

	public SysOrganizationDao getDao(){
		return sysOrganizationDao;
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-2 上午11:33:04
	 *
	 * @param
	 * @return
	 */
	@Override
	public List<SysOrganization> selectOrgByParentId(Map mp) {
	    return sysOrganizationDao.selectOrgByParentId(mp);
	}
	
	@Override
	public void insertOrUpdate(List<SysOrganization> allOrg)	{
		//以parentId为key，目的检查是否为父级
      	Map<String,SysOrganization> parentOrg = new HashMap<String,SysOrganization>();
      	for(int i = 0 ; i < allOrg.size() ; i++){
      		SysOrganization org = allOrg.get(i);
      		if(org != null){
      			parentOrg.put(org.getParentId(), org);
      		}
      	}
      		
		for(int i = 0 ; i < allOrg.size() ; i++){
			SysOrganization org = allOrg.get(i);
			if(org != null){
				org.setOrganizationType("1");
				String orgCode = org.getOrganizationCode();
				if(orgCode == null){
					continue;
				}
				Map<String, String> map = new HashMap<String, String>() ;
				map.put("organizationCode", orgCode);
				List<SysOrganization> list = this.find("findByOrganizationCode" , map);
				if(list != null && list.size() > 0){
					SysOrganization newOrg = list.get(0);
					newOrg.setIsParent(this.getIsParent(orgCode, parentOrg));
					newOrg.setOrganizationFullname(org.getOrganizationFullname());
					newOrg.setOrganizationName(org.getOrganizationName());
					newOrg.setOrganizationFullPath(org.getOrganizationFullPath());
	            	newOrg.setParentId(org.getParentId());
					setOrganizationLevelCode(newOrg, org.getOrganizationFullPath());
					logger.info("更新SysOrganization:"+ newOrg.getOrganizationName() + "("+newOrg.getOrganizationCode() +")");
					this.sysOrganizationDao.update(newOrg);
				} else {
					org.setIsParent(this.getIsParent(orgCode, parentOrg));
					org.setId(IdUtils.uuid2());
					setOrganizationLevelCode(org,org.getOrganizationFullPath());
					logger.info("插入SysOrganization:"+ org.getOrganizationName() + "("+org.getOrganizationCode() +")");
					this.sysOrganizationDao.insert(org);
				}
			}
		}
	}
	
	@Override
	public void transferOrgInfoFromHR(List<SysOrganization> allOrg){
		insertOrUpdate(allOrg);
		//执行删除操作
		getDeleteOrgList(allOrg);
	}

	private void setOrganizationLevelCode(SysOrganization newOrg,String organizationFullPath){
		Map<String,String> orgLevelMap = sysOrganizationDao.getOrganizationLevelMap(organizationFullPath);
		if(orgLevelMap != null && orgLevelMap.size() > 0){
			newOrg.setOrganizationCompanyCode(orgLevelMap.get("ORG_0_CODE"));
			newOrg.setOrganizationCompanyName(orgLevelMap.get("ORG_0_NAME"));
			newOrg.setOrganization_1_Code(orgLevelMap.get("ORG_1_CODE"));
			newOrg.setOrganization_1_Name(orgLevelMap.get("ORG_1_NAME"));
			newOrg.setOrganization_2_Code(orgLevelMap.get("ORG_2_CODE"));
			newOrg.setOrganization_2_Name(orgLevelMap.get("ORG_2_NAME"));
		}
	}
	/**
	 * 删除操作
	 * @param ehrOrgs ehr数据库中全集对象
	 * @return
	 */
	private List<SysOrganization> getDeleteOrgList(List<SysOrganization> ehrOrgs){
		List<SysOrganization> deleteOrgList = new ArrayList<SysOrganization>();
		Map<String,SysOrganization> ehrOrgMap = new HashMap<String,SysOrganization>();//以orgCode为key的ehr数 据对象
      	for(int i = 0 ; i < ehrOrgs.size() ; i++){
      		SysOrganization org = ehrOrgs.get(i);
      		if(org != null){
      			ehrOrgMap.put(org.getOrganizationCode(), org);
      		}
      	}
      	List<SysOrganization> oaOrgs = this.find(null);
      	for(int j = 0 ; j < oaOrgs.size() ; j++){
      		SysOrganization org = oaOrgs.get(j);
      		if(org != null){
      			if(ehrOrgMap.get(org.getOrganizationCode()) == null){
      				System.out.println("\n>>>>>>>>>>>>>>>>>>> delete " + org.getOrganizationName() + "<<<<<<<<<<<<<<<<<<<<<<<<");
      				logger.info("delete SysOrganization:"+ 
      						org.getOrganizationName() + "("+org.getOrganizationCode()+")");
      				org.setStatus("1");
      				this.update(org);
      				//this.delete(org, true);
      			}
      		}
      	}
      	return deleteOrgList;
	}

	/**
	 * 获取当前的org是否有子，如果有子，返回true,否则返回为false
	 * @param orgCode
	 * @param parentOrg
	 * @return
	 */
	private boolean getIsParent(String orgCode,Map<String,SysOrganization> parentOrg){
		if(parentOrg.get(orgCode) != null){
			return true;
		}
		return false;
	}

	@Override
	public List<String> findUnderlingList(String userName) {
		// TODO Auto-generated method stub
		List<String> orgCodeList = new ArrayList();
		SysUser sysUser = sysUserDao.getByUserName(userName, "1");
		SysPosition sysPosition = new SysPosition();
		sysPosition.setPositionCode(sysUser.getPositionCode());
//		sysPositionDao.find(sysPosition)
		List<SysPosition> sysPositionUnderlingList = new ArrayList();
		for (SysPosition SO : findSubPosList(sysPositionDao.find(sysPosition).get(0),sysPositionUnderlingList)) {
			orgCodeList.add(SO.getPositionCode());
		}
		return orgCodeList;

	}

	public List<SysPosition> findSubPosList(SysPosition sysPositon,List<SysPosition> sysPositionUnderlingList) {

		Map mp = new HashMap();
		mp.put("id", sysPositon.getPositionCode());
		List<SysPosition> sysPositionList = sysPositionDao.selectPosByParentId(mp);
		for (SysPosition SP : sysPositionList) {
			sysPositionUnderlingList.add(SP);
			findSubPosList(SP,sysPositionUnderlingList);
		}
		return sysPositionUnderlingList;
	}

	@Override
	public Map<String, String> getOrganizationLevelMap(String organizationFullPath) {
		return sysOrganizationDao.getOrganizationLevelMap(organizationFullPath);
	}
}

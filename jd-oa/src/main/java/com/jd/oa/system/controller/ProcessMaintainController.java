package com.jd.oa.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.web.LoginContext;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;

@Controller
@RequestMapping(value = "/system")
public class ProcessMaintainController {
	private static final Logger logger = Logger
			.getLogger(ProcessMaintainController.class);
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private OaPafService oaPafService;

	/**
	 * @Description: 进入到维护流程页面
	 * @author yujiahe
	 */

	@RequestMapping(value = "/processMaintain_index", method = RequestMethod.GET)
	public String processMaintainIndex() {
		return "system/processMaintain_index";
	}

	/**
	 * 查询流程实例分页
	 * @param request
	 * @param processInstance
	 * @return
	 */
	@RequestMapping(value = "/processMaintain_page")
	@ResponseBody
	public Object processMaintainPage(HttpServletRequest request,
			ProcessInstance processInstance) {

		PageWrapper<ProcessInstance> pageWrapper = new PageWrapper<ProcessInstance>(
				request);

		if (null != processInstance.getProcessDefinitionName()
				&& !processInstance.getProcessDefinitionName().equals("")) {
			pageWrapper.addSearch("processDefinitionName",
					processInstance.getProcessDefinitionName());
		}
		if (null != processInstance.getProcessInstanceName()
				&& !processInstance.getProcessInstanceName().equals("")) {
			pageWrapper.addSearch("processInstanceName",
					processInstance.getProcessInstanceName());
		}
		if (null != processInstance.getStatus()
				&& !processInstance.getStatus().equals("")) {
			pageWrapper.addSearch("status", processInstance.getStatus());
		}
		if (null != processInstance.getProcessInstanceId()
				&& !processInstance.getProcessInstanceId().equals("")) {
			pageWrapper.addSearch("processInstanceId",
					processInstance.getProcessInstanceId());
		}
		processInstanceService.find(pageWrapper.getPageBean(), "findByMap",
				pageWrapper.getConditionsMap());
		String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		logger.debug("result:" + json);
		return json;

	}

	/**
	 * 跳转到修改 流程实例当前代办人页面
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/processMaintain_edit", method = RequestMethod.GET)
	public ModelAndView processMaintainEdit(String id) {
		ModelAndView mav = new ModelAndView("system/processMaintain_edit");
		if (null != id && !("").equals(id)) {
			ProcessInstance processInstance = processInstanceService.get(id);
			if (null != processInstance) {
				List<OaTaskInstance> oaTaskInstanceList = oaPafService.getActiveTaskByInstance(processInstance.getProcessInstanceId());
                List<Object> subTaskList = new ArrayList<Object>();
                    for (OaTaskInstance oti :oaTaskInstanceList){
                        OaTaskInstance subOTI= oaPafService.getTaskDetail(ComUtils.getLoginNamePin(), oti.getId());
                        if ("true".equals(subOTI.getUncompletedSubTaskExist())){
                            subTaskList .addAll( subOTI.getSubTaskList());
                        }
                    }
                mav.addObject("processInstance", processInstance);
				mav.addObject("oaTaskInstanceList", oaTaskInstanceList);//主任务
                mav.addObject("subTaskList", subTaskList);//子任务
			}
		}
		return mav;
	}

	/**
	 * 保存修改 某个流程实例 当前的办理人
	 * @author yujiahe 
	 * @param taskId 待重新分配的任务Id
	 * @param origAssignee 当前的任务分配用户
	 * @param newAssignee  新的任务分配用户，必须为小写
	 * @return
	 */
	@RequestMapping(value = "/processMaintain_editSave", method = RequestMethod.GET)
	@ResponseBody
	public Object processMaintainEditSave(String taskId, String origAssignee,
			String newAssignee) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		LoginContext context = LoginContext.getLoginContext();
		try {
			if (context != null) {
				if (oaPafService.reassignTask(ComUtils.getLoginNamePin(),
						taskId, origAssignee, newAssignee, "user")) {
					resultMap.put("operator", "success");
					resultMap.put("message", "修改当前节点审批人成功");
				} else {
					resultMap.put("operator", "success");
					resultMap.put("message", "修改当前节点审批人失败");
				}
			} else {
				resultMap.put("operator", "success");
				resultMap.put("message", "修改失败，获取登录erpID失败。");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("operator", false);
			resultMap.put("message","修改失败"+ e.getMessage());
		}
		return resultMap;

	}

}

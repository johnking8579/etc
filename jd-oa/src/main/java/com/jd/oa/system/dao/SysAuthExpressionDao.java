package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.SysAuthExpression;

public interface SysAuthExpressionDao extends BaseDao<SysAuthExpression, String> {

}

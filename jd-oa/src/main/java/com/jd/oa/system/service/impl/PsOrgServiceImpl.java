package com.jd.oa.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jd.oa.system.dao.PsOrgDao;
import com.jd.oa.system.dao.SysOrganizationDao;
import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.service.PsOrgService;
import com.jd.oa.system.service.SysOrganizationService;

@Component("psOrgService")
public class PsOrgServiceImpl implements PsOrgService{
	
	static Logger log = Logger.getLogger(PsOrgServiceImpl.class);
	@Resource
	private SysOrganizationDao sysOrgDao;
	@Resource
	private PsOrgDao psOrgDao;
	@Resource
	private SysOrganizationService sysOrgService;
	
	@Override
	public void insertOrUpdateSysOrg()	{
		List<PsOrg> psOrgs = psOrgDao.findAll();
		List<SysOrganization> sysOrgs = new ArrayList<SysOrganization>();
		for(PsOrg p : psOrgs)	{
			sysOrgs.add(this.toSysOrg(p));
		}
		psOrgs.clear();
		
		sysOrgService.insertOrUpdate(sysOrgs);		//调用原有代码
		sysOrgs.clear();
	}
	
	@Override
	public void deleteSysOrg()	{
		List<SysOrganization> delOrgs = sysOrgDao.findNotInPs();
		for(SysOrganization p : delOrgs)	{
			p.setStatus("1");
			sysOrgDao.update(p);
		}
		delOrgs.clear();
	}
	
	private SysOrganization toSysOrg(PsOrg p) {
		SysOrganization sys = new SysOrganization();
		sys.setOrganizationCode(p.getHrOrgOrgId());
		sys.setOrganizationName(p.getHrOrgName());
		sys.setOrganizationFullPath(p.getHrOrgRank());
		sys.setParentId(p.getHrOrgParentId());
		sys.setStatus("0");
		return sys;
	}

}

package com.jd.oa.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.system.dao.SysLevelDao;
import com.jd.oa.system.model.SysLevel;
import com.jd.oa.system.service.SysLevelService;

@Service("sysLevelService")
@Transactional
public class SysLevelServiceImpl extends BaseServiceImpl<SysLevel, String> implements SysLevelService{
	@Autowired
	private SysLevelDao sysLevelDao;
	
	public SysLevelDao getDao(){
		return sysLevelDao;
	}
	
	/**
	 * 
	 * @desc 获得1级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	@Override
	public List<SysLevel> findLevel1List() {
		// TODO Auto-generated method stub
		return sysLevelDao.findLevel1List();
	}
	
	/**
	 * 
	 * @desc 获得2级职级
	 * @author WXJ
	 * @date 2013-9-3 下午05:02:51
	 *
	 * @return
	 */
	@Override
	public List<SysLevel> findLevel2List() {
		// TODO Auto-generated method stub
		return sysLevelDao.findLevel2List();
	}

}

package com.jd.oa.system.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.system.dao.PeoplesoftDao;
import com.jd.oa.system.dao.PsOrgDao;
import com.jd.oa.system.dao.PsPositionDao;
import com.jd.oa.system.dao.PsUserDao;
import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;
import com.jd.oa.system.service.PsOrgService;
import com.jd.oa.system.service.PsPositionService;
import com.jd.oa.system.service.PsUserService;
import com.jd.oa.system.service.SyncFromPsService;

@Component("syncFromPsService")
@Transactional
public class SyncFromPsServiceImpl implements SyncFromPsService{
	static Logger log = Logger.getLogger(SyncFromPsServiceImpl.class);
	
	@Resource
	private PsOrgDao psOrgDao;
	@Resource
	private PsPositionDao psPositionDao;
	@Resource
	private PsUserDao psUserDao;
	@Resource
	private PeoplesoftDao peoplesoftDao;
	@Resource
	private PsOrgService psOrgService;
	@Resource
	private PsPositionService psPositionService;
	@Resource
	private PsUserService psUserService;
	
	
	@Override
	public void syncFromSwapTable()	{
		long start = System.currentTimeMillis();
		log.info("开始从peopleSoft中间表同步[组织机构]...");
		psOrgService.insertOrUpdateSysOrg();
		psOrgService.deleteSysOrg();
		log.info("开始从peopleSoft中间表同步[职位]...");
		psPositionService.insertOrUpdateSysPosition();
		psPositionService.deleteSysPosition();
		log.info("开始从peopleSoft中间表同步[人员]...");
		psUserService.insertOrUpdateSysUser();
		psUserService.deleteSysUser();
		log.info("[组织机构|职位|人员]中间表同步完成,耗时(分钟): " + (System.currentTimeMillis() - start) /1000/60);
	}
	
	/**
	 * @deprecated 事务问题导致无法正确路由到相关数据源
	 */
	@Override
	public void copyToSwapTable() {
		DataSourceHandle.setDataSourceType("ps");
		List<PsOrg> orgs = peoplesoftDao.findAllOrg();
		DataSourceHandle.clearDataSourceType();
		psOrgDao.truncate();
		psOrgDao.add(orgs);
		
		DataSourceHandle.setDataSourceType("ps");
		List<PsPosition> pos = peoplesoftDao.findAllPosition();
		DataSourceHandle.clearDataSourceType();
		psPositionDao.truncate();
		psPositionDao.add(pos);
		
		DataSourceHandle.setDataSourceType("ps");
		List<PsUser> users = peoplesoftDao.findAllUser();
		DataSourceHandle.clearDataSourceType();
		psUserDao.truncate();
		psUserDao.add(users);
	}

	@Override
	public List<PsOrg> findOrgFromPs() {
		return peoplesoftDao.findAllOrg();
	}

	@Override
	public void copyOrgToSwapTable(List<PsOrg> list) {
		psOrgDao.truncate();
		psOrgDao.add(list);
	}
	
	@Override
	public List<PsPosition> findPositionFromPs() {
		return peoplesoftDao.findAllPosition();
	}
	
	@Override
	public void copyPositionToSwapTable(List<PsPosition> list) {
		psPositionDao.truncate();
		psPositionDao.add(list);
	}
	
	@Override
	public List<PsUser> findUserFromPs() {
		return peoplesoftDao.findAllUser();
	}
	
	@Override
	public void copyUserToSwapTable(List<PsUser> list) {
		psUserDao.truncate();
		psUserDao.add(list);
	}

}

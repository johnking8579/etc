package com.jd.oa.system.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.system.model.Application;

public interface ApplicationDao extends BaseDao<Application, Long>{

}

package com.jd.oa.system.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.system.dao.PeoplesoftDao;
import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;

@Component("peoplesoftDao")
public class PeoplesoftDaoImpl extends MyBatisDaoImpl implements PeoplesoftDao {
	
	private static String NAMESPACE = PeoplesoftDao.class.getName();
	
	@Override
	public List<PsOrg> findAllOrg()	{
		return getSqlSession().selectList(NAMESPACE + ".findAllOrg");
	}
	
	@Override
	public List<PsPosition> findAllPosition()	{
		return getSqlSession().selectList(NAMESPACE + ".findAllPosition");
	}
	
	@Override
	public List<PsUser> findAllUser()	{
		return getSqlSession().selectList(NAMESPACE + ".findAllUser");
	}
}

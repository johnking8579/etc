package com.jd.oa.form.execute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;



import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictTypeService;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;

public class FormUIUtil {
	private RuntimeFormManager rfm;
	
	public FormUIUtil(RuntimeFormManager rfm){
		this.rfm = rfm;
	}
	
	public FormUIUtil(){
		
	}
	/**
	 * 表单类型隐藏域变量，特殊情况区别用
	 * 
	 * @param hiddenValue
	 * @author Administrator
	 */
	public static void appendFormTypeHidden(StringBuffer hiddenValue, int type) {
		hiddenValue.append("<input type=hidden name=formType value=" + type + ">\n");
	}
	/**
	 * 替换模板变量成默认值或数据为编辑状态
	 * 
	 * @param sheetModel
	 * @param hashTags
	 */
	/**
	 * 替换模板变量成默认值或数据为编辑状态
	 * @param formBusinessField
	 * @param maps
	 * @param pageNow
	 */
	public Map<String,String> buildMasterHashtableOfVerify(String contextPath,List<FormItem> items,boolean isEdit) {
		// 拼装一个获得表单值的js
		StringBuffer jsGetBindValue = new StringBuffer("\n function getBindValue(){var bv='';\n");
//		// 拼装一个校验表单数据合法性的js
		StringBuffer jsCheckvalue = new StringBuffer("\n function checkValue(buttonType){");
		//拼状列规则js
		StringBuffer jsComputeExpress = new StringBuffer("function updateComputeExpress(formCode){\n");
		//拼装附件保存attachment
		StringBuffer jsFileAttachmentSave = new StringBuffer("function saveFileAttachment(businessObjectId){\n");
		//组件的内部扩展事件
		StringBuffer jsExtendEvents = new StringBuffer();
		
		//表单组件实例化参数准备  hidden变最
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("processDefinitionId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getId() : "");//当前流程定义ID
		params.put("businessObjectId", this.rfm._businessObjectId);//当前业务数据ID
		params.put("businessTableName", this.rfm._businessTableName);//当前业务数据表名称
		params.put("processDefinitionKey", this.rfm._processDefinition != null ? this.rfm._processDefinition.getPafProcessDefinitionId() : "");//当前流程定义Key
		params.put("processNodeId", this.rfm._processNodeId);//当前流程定义Key
		params.put("formId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
		StringBuffer subSheetStr = new StringBuffer();
		if(this.rfm._subSheets != null && this.rfm._subSheets.size() > 0){//拼凑子表名称列表
			for(int j = 0 ; j < this.rfm._subSheets.size() ; j++){
				FormBean formBean = this.rfm._subSheets.get(j);
				if(formBean != null){
					subSheetStr.append(formBean.getFormCode()).append(",");
				}
			}
		}
		if(subSheetStr.length() > 0){
			subSheetStr.setLength(subSheetStr.length() - 1);
		}
		params.put("subSheetStr", subSheetStr.toString());
		params.put("processInstanceKey", this.rfm._processInstance != null ? this.rfm._processInstance.getId() : "");//流程实例Key
		params.put("processInstanceStatus", this.rfm._processInstance != null ? this.rfm._processInstance.getStatus() : "");//流程任务状态
		
		Map<String,String> mapUi = new HashMap<String,String>();
		List<BusinessBindValueModel> jsonData = new ArrayList<BusinessBindValueModel>();
		RuntimeComputeExpressManager rcem = new RuntimeComputeExpressManager(this.rfm);//列规则实现类
		Map<String,String> formItemMap = getSelectList(items);//tag
    	for(int  i = 0 ; i< items.size() ; i++){
    		FormItem formItem = items.get(i);
    		if(formItem != null){
    			FormBusinessField metaDataMapModel = new FormBusinessField();
    			try {
					ComUtils.copyProperties(formItem, metaDataMapModel);
				} catch (Exception e) {
					throw new BusinessException("对象模复制失败：" + e.getMessage());
				}
    			metaDataMapModel.setId(formItem.getId());
    			if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))){
    				metaDataMapModel.setInputType("日期");
    				metaDataMapModel.setInputTypeConfig("");
    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATETIME))){
    				metaDataMapModel.setInputType("日期时间");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			
    			if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
    				metaDataMapModel.setInputType("单行");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			
    			UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
    			Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
    			ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(metaDataMapModel.getInputType());
    			uiComponent.setFormId(this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
    			// 获取表单记录数据
    			String fieldValue = "";
    			
    			if(this.rfm._businessData != null){
    				fieldValue = String.valueOf(this.rfm._businessData.get(formItem.getFieldName()));
    			}
    			if(fieldValue.equals("null")){
    				fieldValue = "";
    			}
    			//加载自定义的扩展JavaScript function 代码
    			if(uiComponent.getJavaScriptExtendEvents() != null && uiComponent.getJavaScriptExtendEvents().trim().length() > 0){
    				jsExtendEvents.append(uiComponent.getJavaScriptExtendEvents()).append("\n");
    			}
//    			如果值为空或要求强行@命令默认,否则仍然给出默认值
    			if(isEdit && fieldValue == null || fieldValue.trim().length() == 0){
    				//替换@命
    				String defaultValue = metaDataMapModel.getDefaultValue();
    				if(defaultValue != null && defaultValue.trim().length() > 0){
//    					StringUtils util = new StringUtils(defaultValue);
    					uiComponent.setValue(rfm.convertMacrosValue(defaultValue));
    				}
    			} else {
    				uiComponent.setValue(fieldValue);//设置数据库存储的值
    			}
    			
    			// 如果已知的UIComponent不存在,返回
    			if (configComponentModel == null) {
    				mapUi.put(metaDataMapModel.getFieldName(), "尚未实现的ui组件");
    			} else {
    				if(isEdit){
    					if(uiComponent.isHidden()){//隐藏
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getHiddenHtmlDefine(null));
    					} else if(!uiComponent.isEdit()){//只读
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getReadHtmlDefine(null));
    						if(!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_PROCESSINSTANCENAME) && 
    								!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_TASKPRIORITY)){
    							putTogetherJsGetBindValueJsonData(jsonData,uiComponent);
    							if(uiComponent.getMetaDataMapModel().getInputType().equals("附件")){
    								jsFileAttachmentSave.append(" updateFileAttachment(businessObjectId,'").append(metaDataMapModel.getFieldName()).append("');\n");
    							}
    						}
    					} else {
    						try {
    							if(null!=formItemMap && formItemMap.size()>0){
        							if(formItemMap.containsKey(formItem.getFieldName())){//父select
        								params.put("#MAIN_FORM_SELECT_FATHER_SELECTID", formItem.getFieldName());
        								params.put("#MAIN_FORM_SELECT_SON_SELECTID", formItemMap.get(formItem.getFieldName()));
        							}
        							if(formItemMap.containsValue(formItem.getFieldName())){//子select
        								params.put("#MAIN_FORM_SELECT_SON_TYPECODE", formItem.getInputTypeConfig().replace("dataDict#",""));
        							}
        						}
							} catch (Exception e) {
								throw new BusinessException("复选框级联失败",e);
							}
    						
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getModifyHtmlDefine(params));//拼元素html
    						String currentJSCheckValue = uiComponent.getValidateJavaScript(params);
    						jsCheckvalue.append(currentJSCheckValue);//校验规则
    						if(!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_PROCESSINSTANCENAME) && 
    								!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_TASKPRIORITY)){
    							putTogetherJsGetBindValueJsonData(jsonData,uiComponent);
    							if(uiComponent.getMetaDataMapModel().getInputType().equals("附件")){
    								jsFileAttachmentSave.append(" updateFileAttachment(businessObjectId,'").append(metaDataMapModel.getFieldName()).append("');\n");
    							}
    						}
    					}
    				} else {//只读
    					mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getReadHtmlDefine(null));
    				}
    			}
    			//加入列规则解析
				if(formItem.getComputeExpress() != null && formItem.getComputeExpress().trim().length() > 0){
					jsComputeExpress.append(rcem.getColOfRulesJavascript(formItem));
				}
    		}
    	}
    	
    	jsComputeExpress.append("\n}\n");
    	
    	//加入子表校验
    	if(subSheetStr.toString().trim().length() > 0){
    		jsCheckvalue.append(" var checkSubSheetFlag = checkSubSheetValue();\n");
    		jsCheckvalue.append(" if(!checkSubSheetFlag){\n 	return false;\n }\n");
    	}
    	jsFileAttachmentSave.append("}\n");
        jsCheckvalue.append("   try{\n");
        jsCheckvalue.append("       if(checkValueExtendCallback){\n");
        jsCheckvalue.append("           return checkValueExtendCallback(buttonType)\n ");
        jsCheckvalue.append("       }\n");
        jsCheckvalue.append("   }catch(e){\n ");
        jsCheckvalue.append(" }\n");
    	jsCheckvalue.append("return true;\n}\n");


    	jsGetBindValue.append("var jsonDatas = ").append(JsonUtils.toJsonByGoogle(jsonData)).append(";\n");
    	jsGetBindValue.append(" return jsonDatas;\n");
    	jsGetBindValue.append("}\n");
    	
    	mapUi.put("JDOA", getFormJavascript(jsGetBindValue.toString(),jsCheckvalue.toString() + jsComputeExpress.toString() + jsFileAttachmentSave.toString() + "\n" + jsExtendEvents.toString()) + getFormParam(params));
    	return mapUi;
	}
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 替换模板变量成默认值或数据为编辑状态,申请节点使用
	 * @date 2014年7月30日 下午4:33:45
	 * @return Map<String,String>
	 */

	public Map<String,String> buildMasterHashtableOfVerifyForApply(String contextPath,List<FormItem> items,boolean isEdit) {
		// 拼装一个获得表单值的js
		StringBuffer jsGetBindValue = new StringBuffer("\n function getBindValue(){var bv='';\n");
//		// 拼装一个校验表单数据合法性的js
		StringBuffer jsCheckvalue = new StringBuffer("\n function checkValue(buttonType){");
		//拼状列规则js
		StringBuffer jsComputeExpress = new StringBuffer("function updateComputeExpress(formCode){\n");
		//拼装附件保存attachment
		StringBuffer jsFileAttachmentSave = new StringBuffer("function saveFileAttachment(businessObjectId){\n");
		
		//拼装需要获取默认数据
		StringBuffer jsAjaxGetData = new StringBuffer("function getApplyMTableData(){ getMTableDefaultValue(");
		
		StringBuffer paramDatas = new StringBuffer("'");
		//组件的内部扩展事件
		StringBuffer jsExtendEvents = new StringBuffer();
		
		//表单组件实例化参数准备  hidden变最
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("processDefinitionId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getId() : "");//当前流程定义ID
		params.put("businessObjectId", this.rfm._businessObjectId);//当前业务数据ID
		params.put("businessTableName", this.rfm._businessTableName);//当前业务数据表名称
		params.put("processDefinitionKey", this.rfm._processDefinition != null ? this.rfm._processDefinition.getPafProcessDefinitionId() : "");//当前流程定义Key
		params.put("processNodeId", this.rfm._processNodeId);//当前流程定义Key
		params.put("formId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
		StringBuffer subSheetStr = new StringBuffer();
		if(this.rfm._subSheets != null && this.rfm._subSheets.size() > 0){//拼凑子表名称列表
			for(int j = 0 ; j < this.rfm._subSheets.size() ; j++){
				FormBean formBean = this.rfm._subSheets.get(j);
				if(formBean != null){
					subSheetStr.append(formBean.getFormCode()).append(",");
				}
			}
		}
		if(subSheetStr.length() > 0){
			subSheetStr.setLength(subSheetStr.length() - 1);
		}
		params.put("subSheetStr", subSheetStr.toString());
		params.put("processInstanceKey", this.rfm._processInstance != null ? this.rfm._processInstance.getId() : "");//流程实例Key
		params.put("processInstanceStatus", this.rfm._processInstance != null ? this.rfm._processInstance.getStatus() : "");//流程任务状态
		
		Map<String,String> mapUi = new HashMap<String,String>();
		List<BusinessBindValueModel> jsonData = new ArrayList<BusinessBindValueModel>();
		RuntimeComputeExpressManager rcem = new RuntimeComputeExpressManager(this.rfm);//列规则实现类
		Map<String,String> formItemMap = getSelectList(items);//tag
    	for(int  i = 0 ; i< items.size() ; i++){
    		FormItem formItem = items.get(i);
    		
    		if(formItem != null){
    			boolean hasDefaultValue = false;
    			String tempInputType = "";
        		String tempDefaultValue = "";
        		String tempFieldName = formItem.getFieldName();
    			if("followCode".equals(formItem.getFieldName())){//缓存中不存放流水单号
    				formItem.setDefaultValue("");
    			}
    			FormBusinessField metaDataMapModel = new FormBusinessField();
    			try {
					ComUtils.copyProperties(formItem, metaDataMapModel);
				} catch (Exception e) {
					throw new BusinessException("对象模复制失败：" + e.getMessage());
				}
    			metaDataMapModel.setId(formItem.getId());
    			if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))){
    				metaDataMapModel.setInputType("日期");
    				metaDataMapModel.setInputTypeConfig("");
    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATETIME))){
    				metaDataMapModel.setInputType("日期时间");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			
    			if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
    				metaDataMapModel.setInputType("单行");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			
    			UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
    			Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
    			ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(metaDataMapModel.getInputType());
    			uiComponent.setFormId(this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
    			// 获取表单记录数据
    			String fieldValue = "";
    			
    			if(this.rfm._businessData != null){
    				fieldValue = String.valueOf(this.rfm._businessData.get(formItem.getFieldName()));
    			}
    			if(fieldValue.equals("null")){
    				fieldValue = "";
    			}
    			//加载自定义的扩展JavaScript function 代码
    			if(uiComponent.getJavaScriptExtendEvents() != null && uiComponent.getJavaScriptExtendEvents().trim().length() > 0){
    				jsExtendEvents.append(uiComponent.getJavaScriptExtendEvents()).append("\n");
    			}
//    			如果值为空或要求强行@命令默认,否则仍然给出默认值
    			if(isEdit && fieldValue == null || fieldValue.trim().length() == 0){
    				//替换@命
    				String defaultValue = metaDataMapModel.getDefaultValue();
    				if(defaultValue != null && defaultValue.trim().length() > 0){
//    					StringUtils util = new StringUtils(defaultValue);
    					if(defaultValue.startsWith("@")){
    						tempDefaultValue = defaultValue;
    						tempInputType = metaDataMapModel.getInputType();
    						hasDefaultValue = true;
    						uiComponent.setValue("");
    					}else{
    						uiComponent.setValue(rfm.convertMacrosValue(defaultValue));
    					}
    					
    				}
    			} else {
//    				uiComponent.setValue(fieldValue);//设置数据库存储的值
    			}
    			
    			// 如果已知的UIComponent不存在,返回
    			if (configComponentModel == null) {
    				mapUi.put(metaDataMapModel.getFieldName(), "尚未实现的ui组件");
    			} else {
    				if(isEdit){
    					if(uiComponent.isHidden()){//隐藏
    						hasDefaultValue = false;//隐藏的属性不提交，所以不取默认值
    						tempInputType = "单行";
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getHiddenHtmlDefine(null));
    					} else if(!uiComponent.isEdit()){//只读
    						if("日期".equals(tempInputType)||"日期时间".equals(tempInputType)){
    							tempInputType = "单行";
    						}
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getReadHtmlDefine(null));
    						if(!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_PROCESSINSTANCENAME) && 
    								!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_TASKPRIORITY)){
    							putTogetherJsGetBindValueJsonData(jsonData,uiComponent);
    							if(uiComponent.getMetaDataMapModel().getInputType().equals("附件")){
    								jsFileAttachmentSave.append(" updateFileAttachment(businessObjectId,'").append(metaDataMapModel.getFieldName()).append("');\n");
    							}
    						}
    					} else {
    						try {
    							if(null!=formItemMap && formItemMap.size()>0){
        							if(formItemMap.containsKey(formItem.getFieldName())){//父select
        								params.put("#MAIN_FORM_SELECT_FATHER_SELECTID", formItem.getFieldName());
        								params.put("#MAIN_FORM_SELECT_SON_SELECTID", formItemMap.get(formItem.getFieldName()));
        							}
        							if(formItemMap.containsValue(formItem.getFieldName())){//子select
        								params.put("#MAIN_FORM_SELECT_SON_TYPECODE", formItem.getInputTypeConfig().replace("dataDict#",""));
        							}
        						}
							} catch (Exception e) {
								throw new BusinessException("复选框级联失败",e);
							}
    						
    						mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getModifyHtmlDefine(params));//拼元素html
    						String currentJSCheckValue = uiComponent.getValidateJavaScript(params);
    						jsCheckvalue.append(currentJSCheckValue);//校验规则
    						if(!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_PROCESSINSTANCENAME) && 
    								!uiComponent.getMetaDataMapModel().getFieldName().equals(RuntimeDefaultFormUIComponent.DEFAULT_FIELDNAME_TASKPRIORITY)){
    							putTogetherJsGetBindValueJsonData(jsonData,uiComponent);
    							if(uiComponent.getMetaDataMapModel().getInputType().equals("附件")){
    								jsFileAttachmentSave.append(" updateFileAttachment(businessObjectId,'").append(metaDataMapModel.getFieldName()).append("');\n");
    							}
    						}
    					}
    				} else {//只读
    					mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getReadHtmlDefine(null));
    				}
    			}
    			if(hasDefaultValue){//如果有缺省值
    				String jsParams = tempFieldName+":"+tempDefaultValue+":"+tempInputType+"|";
    				paramDatas.append(jsParams);
    			}
    			
    			//加入列规则解析
				if(formItem.getComputeExpress() != null && formItem.getComputeExpress().trim().length() > 0){
					jsComputeExpress.append(rcem.getColOfRulesJavascript(formItem));
				}
    		}
    	}
    	paramDatas.append("'");
    	jsComputeExpress.append("\n}\n");
    	jsAjaxGetData.append(paramDatas.toString()+");}\n");
    	//加入子表校验
    	if(subSheetStr.toString().trim().length() > 0){
    		jsCheckvalue.append(" var checkSubSheetFlag = checkSubSheetValue();\n");
    		jsCheckvalue.append(" if(!checkSubSheetFlag){\n 	return false;\n }\n");
    	}
    	jsFileAttachmentSave.append("}\n");
        jsCheckvalue.append("   try{\n");
        jsCheckvalue.append("       if(checkValueExtendCallback){\n");
        jsCheckvalue.append("           return checkValueExtendCallback(buttonType)\n ");
        jsCheckvalue.append("       }\n");
        jsCheckvalue.append("   }catch(e){\n ");
        jsCheckvalue.append(" }\n");
    	jsCheckvalue.append("return true;\n}\n");


    	jsGetBindValue.append("var jsonDatas = ").append(JsonUtils.toJsonByGoogle(jsonData)).append(";\n");
    	jsGetBindValue.append(" return jsonDatas;\n");
    	jsGetBindValue.append("}\n");
    	
    	mapUi.put("JDOA", getFormJavascript(jsGetBindValue.toString(),jsCheckvalue.toString() + jsAjaxGetData.toString()+ jsComputeExpress.toString() + jsFileAttachmentSave.toString() + "\n" + jsExtendEvents.toString()) + getFormParam(params));
    	return mapUi;
	}
	
	/**查询条件动态增加代码返回
	 * @author yujiahe
	 * @param contextPath
	 * @param items
	 * @return
	 */
	public String getHtmlForQueryCondition(String contextPath,List<FormItem> items){
		//表单组件实例化参数准备
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("processDefinitionId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getId() : "");//当前流程定义ID
		params.put("businessObjectId", this.rfm._businessObjectId);//当前业务数据ID
		params.put("businessTableName", this.rfm._businessTableName);//当前业务数据表名称
		params.put("processDefinitionKey", this.rfm._processDefinition != null ? this.rfm._processDefinition.getPafProcessDefinitionId() : "");//当前流程定义Key
		params.put("processNodeId", this.rfm._processNodeId);//当前流程定义Key
		params.put("formId", this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
		String htmlCode = "";
    	for(int  i = 0 ; i< items.size() ; i++){
    		FormItem formItem = items.get(i);
    		if(formItem != null){
    			FormBusinessField metaDataMapModel = new FormBusinessField();
    			try {
					ComUtils.copyProperties(formItem, metaDataMapModel);
				} catch (Exception e) {
					throw new BusinessException("对象模复制失败：" + e.getMessage());
				}
    			metaDataMapModel.setId(formItem.getId());
    			if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))){
    				metaDataMapModel.setInputType("日期");
    				metaDataMapModel.setInputTypeConfig("");//getQueryHtmlDefine
    				
    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATETIME))){
    				metaDataMapModel.setInputType("日期时间");
    				metaDataMapModel.setInputTypeConfig("");
    			}
//    			uiComponent.getMetaDataMapModel().getInputType().equals("附件")
    			if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
    				metaDataMapModel.setInputType("单行");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			
    			UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
    			Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
    			ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(metaDataMapModel.getInputType());
    			uiComponent.setFormId(this.rfm._processDefinition != null ?  this.rfm._processDefinition.getFormId() : "");//当前流程绑定的formId
    			if(!uiComponent.getMetaDataMapModel().getInputType().equals("附件")){//如果为附件，不返回任何html
    				if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))
        					||metaDataMapModel.getDataType().equals(String.valueOf(DataType.NUMBER))){
        				htmlCode +=  uiComponent.getQueryHtmlDefine(params);
        			}else if (configComponentModel != null) {
        				htmlCode +=  uiComponent.getModifyHtmlDefine(params);
        			}
    				
    			}
    			
    		}}
    	return htmlCode;
	}
	/**
	 * 非运行时该表单ui构造
	 * @param request 
	 * @param item formItem object
	 * @param fieldValue field value for database
	 * @return
	 */
	public Map<String,String> buildMasterHashtableOfVerify(String contextPath,FormItem item,String fieldValue) {
		Map<String,String> mapUi = new HashMap<String,String>();
		FormBusinessField metaDataMapModel = new FormBusinessField();
		try {
			ComUtils.copyProperties(item, metaDataMapModel);
		} catch (Exception e) {
			throw new BusinessException("对象模复制失败：" + e.getMessage());
		}
		if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
			metaDataMapModel.setInputType("单行");
			metaDataMapModel.setInputTypeConfig("");
		}
		UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
		Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
		ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(metaDataMapModel.getInputType());
		if (configComponentModel == null) {
			mapUi.put(metaDataMapModel.getFieldName(), "尚未实现的ui组件");
		}  else {
//			如果值为空或要求强行@命令默认,否则仍然给出默认值
			if(fieldValue == null || fieldValue.trim().length() == 0){
				//替换@命
				String defaultValue = metaDataMapModel.getDefaultValue();
				if(defaultValue != null && defaultValue.trim().length() > 0){
//					StringUtils util = new StringUtils(defaultValue);
					uiComponent.setValue(rfm.convertMacrosValue(defaultValue));
				}
			} else {
				uiComponent.setValue(fieldValue);//设置数据库存储的值
			}
			
			mapUi.put(metaDataMapModel.getFieldName(), uiComponent.getTrueValue());
		}	
		return mapUi;
	}
	public String buildMasterHashtableOfVerify1(String contextPath,FormItem item,String fieldValue) {
		Map<String,String> map = this.buildMasterHashtableOfVerify(contextPath, item, fieldValue);
		return map.get(item.getFieldName()).toString();
	}
	/**
	 * 拼凑需要保存的字段json
	 * @param jsonData
	 * @param uiComponent
	 */
	private void putTogetherJsGetBindValueJsonData(List<BusinessBindValueModel> jsonData,UIComponentInterface uiComponent){
		BusinessBindValueModel model = new BusinessBindValueModel();
		model.setFieldName(uiComponent.getMetaDataMapModel().getFieldName());
		model.setFieldType(uiComponent.getMetaDataMapModel().getDataType());
		model.setFieldInputType(uiComponent.getMetaDataMapModel().getInputType());
		jsonData.add(model);
	}
	/**
	 * 表单数据值，及校验拼凑
	 * @param jsBindValue
	 * @param checkBindValue
	 * @return
	 */
	private String getFormJavascript(String jsBindValue,String checkBindValue){
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n<script type=\"text/javascript\">\n");
		buffer.append(jsBindValue).append("\n");
		buffer.append(checkBindValue).append("\n");
		buffer.append("<!--扩展按钮-->\n");
		buffer.append(JavascriptEscape.unescape(this.rfm.getExtendButtonJavascriptContext())).append("\n");
		buffer.append("<!--end 扩展按钮-->\n");
		buffer.append("</script>\n");
		return buffer.toString();
	}
	/**
	 * 实例化form hidden 变理参数
	 * @param params
	 * @return
	 */
	private String getFormParam(Map<String,Object> params){
		StringBuffer buffer = new StringBuffer();
		Iterator iterator = params.entrySet().iterator(); 
		while(iterator.hasNext()) { 
		    Entry entry = (Entry)iterator.next(); 
		    String key = entry.getKey().toString();
		    String value = String.valueOf(params.get(key)); 
		    buffer.append("<input type='hidden' id='").append(key).append("' name='").append(key).append("' value='").append(value).append("' />\n");
		} 
		return buffer.toString();
	}
	
	/**
	 * 
	 * @author zhengbing
	 * @desc 返回主表所有未隐藏的select下拉菜单，为级联做准备
	 * @date 2014年7月8日 下午4:40:06
	 * @return List
	 */
	private Map<String, String> getSelectList(List<FormItem> items) {
		List<FormItem> selects = new ArrayList<FormItem>();
		List<String> dictTypeCodes = new ArrayList<String>();
		for (FormItem fi : items) {
			try {
				// 添加select菜单
				if (null != fi.getInputTypeConfig()
						&& fi.getInputTypeConfig().indexOf("dataDict#") > -1
						&& (!"S".equals(fi.getTableType()))
						&& "列表".equals(fi.getInputType())
						&& (!"1".equals(fi.getIsHidden()))) {
					selects.add(fi);
					dictTypeCodes.add(fi.getInputTypeConfig().trim()
							.substring(9));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (dictTypeCodes.size() <= 1)
			return null;
		DictTypeService dictTypeService = SpringContextUtils
				.getBean("dictTypeService");
		List<DictType> dictTypeList = dictTypeService
				.getDictTypeCodes(dictTypeCodes);
		if (null == dictTypeList || dictTypeList.size() == 0)
			return null;

		Map<String, String> cascadeSelectMap = new HashMap<String, String>();
		Map<String, String> cascadeSelectFormItemMap = new HashMap<String, String>();
		
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < dictTypeList.size(); i++) {// 构造父子表DictType Map
			DictType compareDt = dictTypeList.get(i);
			for (int j = i + 1; j < dictTypeList.size(); j++) {
				DictType beCompareDt = dictTypeList.get(j);
				if (compareDt.getId().equals(beCompareDt.getParentId())) {
					cascadeSelectMap.put(compareDt.getDicttypeCode(),
							beCompareDt.getDicttypeCode());// 父--子
					sb.append(compareDt.getDicttypeCode()+":"+beCompareDt.getDicttypeCode()+",");
				} else if (beCompareDt.getId().equals(compareDt.getParentId())) {
					cascadeSelectMap.put(beCompareDt.getDicttypeCode(),
							compareDt.getDicttypeCode());// 父--子
					sb.append(beCompareDt.getDicttypeCode()+":"+compareDt.getDicttypeCode()+",");
				}
			}

		}
		String cascadeSelectMapString = sb.toString();//父子map字符串
		
		List<FormItem> tempFormItems = new ArrayList<FormItem>();
		tempFormItems.addAll(selects);
		if(null!=tempFormItems&&tempFormItems.size()>0){
			getSelectCascadeFromItem(cascadeSelectFormItemMap, tempFormItems.get(0), tempFormItems, cascadeSelectMapString);
		}
		
		return cascadeSelectFormItemMap;
	}

	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 递归找出父子关联，放入map
	 * @date 2014年7月10日 下午5:10:53
	 * @return List<FormItem>
	 */
	private void getSelectCascadeFromItem(Map<String,String> map,FormItem fatherFromItem,List<FormItem> formItems,String cascadeSelectMapString){
		try {
			String fartherCode = fatherFromItem.getInputTypeConfig().replace("dataDict#", "").trim();
			for (Iterator it = formItems.iterator(); it.hasNext();) {
				FormItem formItem = (FormItem)it.next();
				if(null==formItem.getInputTypeConfig()){ 
					it.remove();
					continue;
				}
				if(formItem.getId().equals(fatherFromItem.getId())){
					it.remove();
					continue;
				}
				String sonCode = formItem.getInputTypeConfig().replace("dataDict#", "").trim();
				if(cascadeSelectMapString.contains(fartherCode+":"+sonCode+",")){//找到子节点
					map.put(fatherFromItem.getFieldName(), formItem.getFieldName());
					it.remove();
					getSelectCascadeFromItem(map,formItem,formItems,cascadeSelectMapString);
					break;
				}
				if(cascadeSelectMapString.contains(sonCode+":")){//找到与自己类型相同的节点
					getSelectCascadeFromItem(map,formItem,formItems,cascadeSelectMapString);
					break;
				}
			}
			if(null!=formItems&&formItems.size()>0){
				getSelectCascadeFromItem(map,formItems.get(0),formItems,cascadeSelectMapString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
	
	
}


package com.jd.oa.form.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormSequence;

public interface FormSequenceDao extends BaseDao<FormSequence,String> {
	/**
	 * 获取sequenceValue;
	 * @param typeId
	 * @param sequenceType
	 * @return
	 */
	public int getFormSequenceValue(String typeId,String sequenceType);
}

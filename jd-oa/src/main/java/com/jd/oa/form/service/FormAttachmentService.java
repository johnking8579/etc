package com.jd.oa.form.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormAttachment;

import java.io.InputStream;

public interface FormAttachmentService extends BaseService<FormAttachment, String>{
	public String uploadFileToJssService(MultipartFile uploadFile) throws BusinessException;

	public String uploadFileToJssService(FormAttachment formAttachment,InputStream inputStream) throws BusinessException;
	
	public void downloadfile(HttpServletRequest request,HttpServletResponse response,String id);
	
	public void downloadExcelImportModel(HttpServletRequest request,HttpServletResponse response,String tableName);
	
	public int deleteFile(String id);
}

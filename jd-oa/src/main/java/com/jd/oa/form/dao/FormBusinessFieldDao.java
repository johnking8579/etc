package com.jd.oa.form.dao;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormBusinessField;

/** 
 * @Description: 自定义表字段Dao
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */
public interface FormBusinessFieldDao extends BaseDao<FormBusinessField, String>{

	/**
	  * @Description: 根据ID查询自定义表字段信息
	  * @param formBusinessFieldId
	  * @return FormBusinessField
	  * @author guoqingfu
	  * @date 2013-9-13上午10:11:33 
	  * @version V1.0
	 */
	public FormBusinessField getFormBusinessFieldInfo(String formBusinessFieldId);
	/**
	  * @Description: 验证是否被表单引用
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-13上午11:10:00 
	  * @version V1.0
	 */
	public boolean isUsedByForm(Map<String, Object> map);
	/**
	  * @Description: 删除自定义字段
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-13上午11:17:09 
	  * @version V1.0
	 */
	public boolean deleteFormBusinessField(Map<String,Object> map);
	
	/**
	  * @Description: 判断是否存在自定义表字段名
	  * @param fieldName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-13下午01:39:41 
	  * @version V1.0
	 */
	public boolean isExistFieldName(FormBusinessField formBusinessField);
	/**
	  * @Description: 判断是否存在自定义表字段显示名
	  * @param formBusinessField
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-13下午02:07:28 
	  * @version V1.0
	 */
	public boolean isExistFieldChineseName(FormBusinessField formBusinessField);
	/**
	  * @Description: 根据自定义表ID查询表和字段信息
	  * @param businessTableId
	  * @return List<FormBusinessField>
	  * @author guoqingfu
	  * @date 2013-9-17下午01:54:17 
	  * @version V1.0
	 */
	public List<FormBusinessField> findTableFieldList(String businessTableId);

	/**
	  * @Description: 查询业务表是否已被创建过
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-23下午04:35:08 
	  * @version V1.0
	 */
	public boolean isExistBusinessTable(Map<String,Object> map);

	/**
	 * @Description: 根据表名查看该表是否已经有数据
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-27上午10:37:59 
	  * @version V1.0
	 */
	public boolean isExistData(String tableName);
	/**
	  * @Description: 高级模式创建表单时 根据自定义表ID查询未选择的字段信息
	  * @param businessTableId
	  * @return List<FormBusinessField>
	  * @author 许林
	  * @date 2013-9-22下午01:54:17 
	  * @version V1.0
	 */
	public List<FormBusinessField> findTableFieldListNotSelect(String businessTableId);

	/**
	 * @Description: 根据自定义表ID查询表和字段信息(包含子表)
	  * @param businessTableId
	  * @return List<FormBusinessField>
	  * @author guoqingfu
	  * @date 2013-9-25上午10:44:15 
	  * @version V1.0
	 */
	public List<FormBusinessField> findFieldListIncludeChild(String businessTableId);
	
	/**
	 * @Description: 根据tableId查询字段信息
	 * @param tableId
	 * @return List<FormBusinessField>
	 * @author xulin
	 * @date 2013-9-25上午10:44:15 
	 * @version V1.0
	 */
	public List<FormBusinessField> selectTableFieldsByTableId(String tableId);
	
	/**
	 * @Description: 根据字段ID查询业务表信息
	  * @param bussinessFieldId
	  * @return FormBusinessField
	  * @author guoqingfu
	  * @date 2013-10-21上午10:23:50 
	  * @version V1.0
	 */
	public FormBusinessField getTableInfoByFieldId(String bussinessFieldId);
	
	/**
	 * @Description: 查询被创建过的业务表名
	  * @param tableName
	  * @return List<String>
	  * @author guoqingfu
	  * @date 2013-10-22下午05:56:41 
	  * @version V1.0
	 */
	public List<String> findCreatedTable(Map<String,Object> map);
}

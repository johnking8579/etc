package com.jd.oa.form.design.style.impl;

import java.util.HashMap;
import java.util.Map;

import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.util.HtmlModelFactory;
import com.jd.oa.form.util.RepleaseKey;

/**
 * 通过表单日数据项的定义,结合表单指定模板生成表单项
 * @author liub
 *
 */
public class FormStyle_2_Impl extends FormStyleAbst{
	private boolean noColor = false;
	public FormStyle_2_Impl() {
		
	}
	@Override
	public String getWeb() {
		// TODO Auto-generated method stub
		Map<String,String> paramMapTags = new HashMap<String,String>();
		paramMapTags.put("PART1", getPart());//主表
		paramMapTags.put("SUBSHEET", getSubSheetPart());//子表
		paramMapTags.put("FORMTITLE", getFormTemplateName() == null ? getForm().getFormName() : getFormTemplateName());
		String formTemplatePath = getHttpServletRequest().getSession().getServletContext().getRealPath("/");
		return RepleaseKey.replace(HtmlModelFactory.getModel(formTemplatePath,getHtmlName()), paramMapTags, "[@", "]");
	}
	/**
	 * 主表数据
	 * @return
	 */
	private String getPart() {
		StringBuffer str = new StringBuffer();
		for(int i = 0 ; i < getFormItem().size() ; i++){
			FormItem item = getFormItem().get(i);
			if(item != null){
//				if( i % 2 == 0){
//					str.append(" <tr class = '").append((noColor ? "tr1 " : "tr2 ")).append("'>\n");
//					noColor = !noColor;
//				} 
////				str.append("<td height='25' align=right nowrap=\"nowrap\" width='15%' valign=\"middle\">")
//				str.append("<td height='25' align=right  width='15%' valign=\"middle\">")
//					.append(item.getFieldChineseName()).append(" : ")
//					.append("</td>")
////					.append("<td height='25' align=left nowrap=\"nowrap\" width='30%' valign=\"middle\"> &nbsp;")
//					.append("<td height='25' align=left  width='30%' valign=\"middle\"> &nbsp;")
//					.append(super.getFormItemTags(item))
//					.append("</td>\n");
//				if( i % 2 == 1){
//					str.append("</tr>\n");
//				}
//				if( i % 2 == 0){
//					str.append(" <div class = '").append((noColor ? "tr1" : "tr2")).append("' style='width:100%;border:0px red dotted;height:40px;'>\n");
//					noColor = !noColor;
//				}
				//内容
				if( i % 2 == 0){
					noColor = !noColor;
				}
				str.append("<div class = '").append((noColor ? "tr1" : "tr2")).append("' style='width:49%;float:left;line-height:30px;height:40px;display:inline-table;' id='").append(item.getFieldName()).append("_div'>").append(getItemLabelAndFieldConent(item)).append("</div>");
//				if( i % 2 == 1){
//					str.append("</div>\n");
//				}
			}
		}
		return str.toString();
	}
	private String getItemLabelAndFieldConent(FormItem item){
		StringBuffer str = new StringBuffer();
		str.append("<div style='width:30%;float:left;text-align:right'>").append(item.getFieldChineseName()).append(":</div>");
		str.append("<div style='width:70%;float:right;text-align:left;'>").append(super.getFormItemTags(item)).append("</div>");
		return str.toString();
	}
	/**
	 * 子表
	 * @return
	 */
	private String getSubSheetPart(){
		StringBuffer str = new StringBuffer();
		for(int i = 0 ; i < getSubSheet().size() ; i++){
			str.append(getSubSheet().get(i)).append("<br />");
		}
		return str.toString();
	}
}

package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.form.execute.expression.ExpressionAbst;
/**
 * 无效的表达式
 * @author birkhoff
 *
 */
public class NoneExpressionImpl extends ExpressionAbst{
	public NoneExpressionImpl(Map<String,Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
	}

	public String expressionParse(String expression) {
		return expression;
	}
}

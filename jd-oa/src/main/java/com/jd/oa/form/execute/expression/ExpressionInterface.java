package com.jd.oa.form.execute.expression;

/**
 * 系统运行时刻@变量 定义
 * @author birkhoff
 *
 */
public interface ExpressionInterface {
	public abstract String expressionParse(String expression);
}

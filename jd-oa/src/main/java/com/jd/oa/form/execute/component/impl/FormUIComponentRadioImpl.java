package com.jd.oa.form.execute.component.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.dict.dao.DictTypeDao;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.dict.service.DictTypeService;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.system.service.SysUserService;
public class FormUIComponentRadioImpl extends FormUIComponentAbst {

	public FormUIComponentRadioImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}

	/**
	 * 获取Key-value形式的映射关系
	 * @param params
	 * @return
	 */
	private String getMappingData(Map<String,Object> params){
		StringUtils util = new StringUtils(getMetaDataMapModel().getInputTypeConfig());
		if(util.startsWith("dataDict#")){
			String dictCode = util.replace("dataDict#", "");
			DictDataService dictDataService = SpringContextUtils.getBean("dictDataService");
			List<DictData> list = dictDataService.findDictDataList(dictCode);
			StringBuffer sb = new StringBuffer();
			for (DictData dictData : list) {
				sb.append(dictData.getDictCode());
				sb.append(":");
				sb.append(dictData.getDictName());
				sb.append("|");
			}
			String temp = sb.toString();
			if(temp != null && temp.length() > 0)
				temp = temp.substring(0, temp.length()-1);
			return temp;
		}else if (util.startsWith("dataSource#")) {
			DiDatasourceService diDatasourceService = SpringContextUtils.getBean("diDatasourceService");
			DiDatasource datasource = diDatasourceService.get( util.replace("dataSource#", ""));

			List<Map<String,Object>>  list = null;
			if (datasource.getDataAdapterType()==1) {
				list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), datasource.getExecSql(), "0");
			} else if (datasource.getDataAdapterType()==2) {
				list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, null, "0");
			}

			StringBuffer sb = new StringBuffer();
			if(list!=null){
				for (Map<String, Object> map : list) {
					int length = map.values().size();
					Iterator iterator = map.values().iterator();
					if(length == 0){
					}else if(length == 1){
						Object temp = iterator.next();
						sb.append(temp);
						sb.append(":");
						sb.append(temp);
						sb.append("|");
					}else if(length >= 2){
						Object temp = iterator.next();
						Object temp2=  iterator.next();
						sb.append(temp);
						sb.append(":");
						sb.append(temp2);
						sb.append("|");
					}
				}
			}
			
			String temp = sb.toString();
			temp = temp.substring(0, temp.length()-1);
			return temp;
		}else{
			return getMetaDataMapModel().getInputTypeConfig();
		}
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		getDisplayRadioOfRead(html, params);
		return html.toString();
	}

	private void getDisplayRadioOfRead(StringBuffer html,Map<String,Object> params){
		String value = getValue();
//		if(value ==null || value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}

		StringUtils util = new StringUtils(this.getMappingData(params));
		List<String> v = util.split("|");
		for(int i = 0 ; i < v.size() ; i++){
			String tmpGetValue = v.get(i);
			String tmpGetName = "";
			if(tmpGetValue.indexOf(":") != -1){
				StringUtils util2 = new StringUtils(tmpGetValue);
				List<String> v2 = util2.split(":");
				if(v2.size() == 2){
					if(v2.get(0) != null){
						tmpGetValue = v2.get(0);
					} else {
						tmpGetName = tmpGetValue;
					}
					if(v2.get(1) != null){
						tmpGetName = v2.get(1);
					}else {
						tmpGetName = tmpGetValue;
					}
				}
			} else {
				tmpGetName = tmpGetValue;
			}
			if(value.equals(tmpGetValue)){
				html.append("<label style='cursor: default;'><input disabled type=radio onclick='return radioClick(event)' ")
						.append(" checked  name=").append(getMetaDataMapModel().getFieldName())
						.append(" value='").append(tmpGetValue)
						.append("'>&nbsp;<span  class='label label-warning'>").append(tmpGetName)
						.append("</span></label> &nbsp;");
			}else {
				html.append("<label style='cursor: default;'><input disabled type=radio onclick='return radioClick(event)' ")
						.append("  name=").append(getMetaDataMapModel().getFieldName())
						.append(" value='").append(tmpGetValue)
						.append("'>&nbsp;<span>").append(tmpGetName)
						.append("</span></label> &nbsp;");
			}
		}
		if(!isNull()){
			html.append("<font color=red>*</font>");
		}
	}
	public String getModifyHtmlDefine(Map<String,Object> params) {
		String value =  getValue();
//		if(value == null || value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		StringBuffer fieldHtml = new StringBuffer();
		StringUtils util = new StringUtils(getMappingData(params));
		List<String> v = util.split("|");
//		fieldHtml.append("<div style='width:100%'>");
		for(int i = 0 ; i < v.size() ; i++){
			String tmpGetValue = v.get(i);
			String tmpGetName = "";
			if(tmpGetValue.indexOf(":") != -1){
				StringUtils util2 = new StringUtils(tmpGetValue);
				List<String> v2 = util2.split(":");
				if(v2.size() == 2){
					if(v2.get(0) != null){
						tmpGetValue = v2.get(0);
					} else {
						tmpGetName = tmpGetValue;
					}
					if(v2.get(1) != null){
						tmpGetName = v2.get(1);
					}else {
						tmpGetName = tmpGetValue;
					}
				}
			} else {
				tmpGetName = tmpGetValue;
			}
			if(value.equals(tmpGetValue)){
				fieldHtml.append("<label><input type=radio onclick='return radioClick(event)'")
                .append(getHtmlInner())
				.append(" checked  name=").append(getMetaDataMapModel().getFieldName())
				.append(" value='").append(tmpGetValue)
				.append("'>&nbsp;<span  class='label label-warning'>").append(tmpGetName)
				.append("</span></label> &nbsp;");
			}else {
				fieldHtml.append("<label><input type=radio onclick='return radioClick(event)'")
                .append(getHtmlInner())
				.append("  name=").append(getMetaDataMapModel().getFieldName())
				.append(" value='").append(tmpGetValue)
				.append("'>&nbsp;<span>").append(tmpGetName)
				.append("</span></label> &nbsp;");
			}
		}
		if(!isNull()){
			fieldHtml.append("<font color=red>*</font>");
		}
//		fieldHtml.append("</div>");
		return fieldHtml.toString();
	}
	public String getValidateJavaScript(Map<String,Object> params){
		FormBusinessField metaDataMapModel = getMetaDataMapModel();
		StringBuffer jsCheckValue = new StringBuffer();
		jsCheckValue.append("var ").append(metaDataMapModel.getFieldName()).append(" = ").append("$(\"input[name='").append(metaDataMapModel.getFieldName()).append("']\");\n");
		jsCheckValue.append("\n var radio_").append(metaDataMapModel.getFieldName()).append(" = '';\n")
		            .append("  try{\n")
		            .append("     for(var i = 0;i < ").append(metaDataMapModel.getFieldName()).append(".length ; i++){\n")
		            .append("        if(").append(metaDataMapModel.getFieldName()).append("[i].checked){\n")
		            .append("			radio_").append(metaDataMapModel.getFieldName()).append(" = ").append(metaDataMapModel.getFieldName()).append("[i].value;\n")
		            .append(         "}\n")
		            .append("	  }\n ")
		            .append("}catch(e){}\n");
		// 非空校验
		if (!isNull()) {
			jsCheckValue.append("\n try{ \n")
						.append("     if(radio_").append(metaDataMapModel.getFieldName()).append(".length==0)").append("{\n")
						.append(" 	     alert ('请给字段:[").append(metaDataMapModel.getFieldChineseName()).append("]选择一个值，该字段值不允许为空！');\n")
						.append("     	 return false; \n")
						.append("     } \n")
						.append("}catch(e){}\n\n ");
		}
		return jsCheckValue.toString();
	}
	public String getSettingWeb() {
		StringBuffer settingHtml = new StringBuffer();
		 DictTypeService dictTypeService= SpringContextUtils.getBean(DictTypeService.class);
//		 DiDatasourceService diDatasourceService= SpringContextUtils.getBean(DiDatasourceService.class);
		 List<DictType> dictTypeList= dictTypeService.findByParentId("DICTTYPE_00002");
//		 List<DiDatasource> diDatasourceList = diDatasourceService.find(null);
		settingHtml.append("<table width=\"100%\" align=\"center\">");
		settingHtml.append("<tr><td width=\"18%\" nowrap>");
				
		String typeConfig = getMetaDataMapModel().getInputTypeConfig();
		String[] typeConfigArray = typeConfig.split("#");
		String consVal = "";
		String dataDictVal = "";
		String dataSourceVal = "";
		
		if (typeConfigArray[0].equals("dataDict")) {
			dataDictVal = typeConfigArray[1];		
		} 
//		else if (typeConfigArray[0].equals("dataSource")) {
//			dataSourceVal = typeConfigArray[1];				
//		} 
		else {
			consVal = typeConfig;
		}				
		
		String cons = "	<input type=\"radio\" name=\"upType\" value=\"cons\" onclick=\"visableCombox(this);\" />常量";
		String consChecked = "	<input type=\"radio\" name=\"upType\" value=\"cons\" onclick=\"visableCombox(this);\" checked/>常量";
		String dataDict = "	<input type=\"radio\" name=\"upType\" value=\"dataDict\" onclick=\"visableCombox(this);\" />数据字典";
		String dataDictChecked = "	<input type=\"radio\" name=\"upType\" value=\"dataDict\" onclick=\"visableCombox(this);\" checked/>数据字典";
//		String dataSource = "	<input type=\"radio\" name=\"upType\" value=\"dataSource\" onclick=\"visableCombox(this);\" />数据源";
//		String dataSourceChecked = "	<input type=\"radio\" name=\"upType\" value=\"dataSource\" onclick=\"visableCombox(this);\" checked/>数据源";
				
		StringBuffer consHideHtml = new StringBuffer();
		consHideHtml.append("<tr id=\"consTrea\" style=\"display:none\"><td colspan=\"2\">");
		consHideHtml.append("<input type=text  name='COMBOX_CONS'  id='COMBOX_CONS' value='").append(consVal).append("' size=70 >");
		consHideHtml.append("</td></tr>");
		consHideHtml.append("<tr id=\"consText\" style=\"display:none\" ><td colspan=\"2\">语法格式=[值1:]显示1|[值2:]显示2...</td></tr>");
		
		StringBuffer consShowHtml = new StringBuffer();
		consShowHtml.append("<tr id=\"consTrea\" style=\"display:block\"><td colspan=\"2\">");
		consShowHtml.append("<input type=text  name='COMBOX_CONS'  id='COMBOX_CONS' value='").append(consVal).append("' size=70 >");
		consShowHtml.append("</td></tr>");
		consShowHtml.append("<tr id=\"consText\" style=\"display:block\" ><td colspan=\"2\">语法格式=[值1:]显示1|[值2:]显示2...</td></tr>");
		
		StringBuffer dataDictHideHtml = new StringBuffer();
		dataDictHideHtml.append("<tr id=\"dataDictTrea\" style=\"display:none\"><td colspan=\"2\">");
		dataDictHideHtml.append("<select name='COMBOX_DATADICT'  id='COMBOX_DATADICT' >");
		dataDictHideHtml.append("<option value=''>请选择</option>");
		for(DictType DT :dictTypeList){
			if (DT.getDicttypeCode().equals(dataDictVal)){
				dataDictHideHtml.append("<option value='"+DT.getDicttypeCode()+"' selected>"+DT.getDicttypeName()+"</option>");
			}else{
				dataDictHideHtml.append("<option value='"+DT.getDicttypeCode()+"' >"+DT.getDicttypeName()+"</option>");
			}
		 } 
		dataDictHideHtml.append("</select></td></tr>");
		
		StringBuffer dataDictShowHtml = new StringBuffer();
		dataDictShowHtml.append("<tr id=\"dataDictTrea\" style=\"display:block\"><td colspan=\"2\">");
		dataDictShowHtml.append("<select name='COMBOX_DATADICT'  id='COMBOX_DATADICT' >");
		dataDictShowHtml.append("<option value=''>请选择</option>");
		for(DictType DT :dictTypeList){
			if (DT.getDicttypeCode().equals(dataDictVal)){
				dataDictShowHtml.append("<option value='"+DT.getDicttypeCode()+"' selected>"+DT.getDicttypeName()+"</option>");
			}else{
				dataDictShowHtml.append("<option value='"+DT.getDicttypeCode()+"' >"+DT.getDicttypeName()+"</option>");
			}
		 } 
		dataDictShowHtml.append("</select></td></tr>");
		
//		StringBuffer dataSourceHideHtml = new StringBuffer();
//		dataSourceHideHtml.append("<tr id=\"dataSourceTrea\" style=\"display:none\"><td colspan=\"2\">");
//		dataSourceHideHtml.append("<select name='COMBOX_DATASOURCE'  id='COMBOX_DATASOURCE'  >");
//		dataSourceHideHtml.append("<option value=''>请选择</option>");
//		for(DiDatasource DS :diDatasourceList){
//			if (DS.getId().equals(dataSourceVal)){
//				dataSourceHideHtml.append("<option value='"+DS.getId()+"' selected>"+DS.getName()+"</option>");
//			}else{
//				dataSourceHideHtml.append("<option value='"+DS.getId()+"' >"+DS.getName()+"</option>");
//			}
//		 } 
//		dataSourceHideHtml.append("</select></td></tr>");
//		
//		StringBuffer dataSourceShowHtml = new StringBuffer();
//		dataSourceShowHtml.append("<tr id=\"dataSourceTrea\" style=\"display:block\"><td colspan=\"2\">");
//		dataSourceShowHtml.append("<select name='COMBOX_DATASOURCE'  id='COMBOX_DATASOURCE' >");
//		dataSourceShowHtml.append("<option value=''>请选择</option>");
//		for(DiDatasource DS :diDatasourceList){
//			if (DS.getId().equals(dataSourceVal)){
//				dataSourceShowHtml.append("<option value='"+DS.getId()+"' selected>"+DS.getName()+"</option>");
//			}else{
//				dataSourceShowHtml.append("<option value='"+DS.getId()+"' >"+DS.getName()+"</option>");
//			}
//		 } 
//		dataSourceShowHtml.append("</select></td></tr>");						
		
		if (typeConfigArray[0].equals("dataDict")) {
			settingHtml.append(cons);
			settingHtml.append(dataDictChecked);
//			settingHtml.append(dataSource);
			settingHtml.append("</td></tr>");
			settingHtml.append(consHideHtml);
			settingHtml.append(dataDictShowHtml);
//			settingHtml.append(dataSourceHideHtml);
		} else if (typeConfigArray[0].equals("dataSource")) {
			settingHtml.append(cons);
			settingHtml.append(dataDict);
//			settingHtml.append(dataSourceChecked);
			settingHtml.append("</td></tr>");
			settingHtml.append(consHideHtml);
			settingHtml.append(dataDictHideHtml);
//			settingHtml.append(dataSourceShowHtml);
		} else {
			settingHtml.append(consChecked);
			settingHtml.append(dataDict);
//			settingHtml.append(dataSource);	
			settingHtml.append("</td></tr>");
			settingHtml.append(consShowHtml);
			settingHtml.append(dataDictHideHtml);
//			settingHtml.append(dataSourceHideHtml);
		}
		
		
		settingHtml.append("<tr id=\"sqlTrea\" style=\"display:none\"><td colspan=\"2\">");
		settingHtml.append("<table>");
		settingHtml.append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;显示字段名&nbsp;&nbsp;&nbsp;&nbsp;<input type='text'  class ='actionsoftInput'  name='AWS_COMBOX_SQLSHOW' id='AWS_COMBOX_SQLSHOW' ></td></tr>");
		settingHtml.append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;取值字段名(<font color=red>*</font>)<input class ='actionsoftInput' type='text'   name='AWS_COMBOX_SQLGET' id='AWS_COMBOX_SQLGET'></td></tr>");
		settingHtml
				.append("<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SQL语句&nbsp;&nbsp;(<font color=red>*</font>)&nbsp;<input type='text'  class ='actionsoftInput'  name='AWS_COMBOX_SQLSELECT' id='AWS_COMBOX_SQLSELECT' size='50'><a href=\"###\" onclick=\"openAnalog(frmMain,'combox');return false;\"><img src='../aws_img/admin.gif' title='模拟使用'>测试</a></td></tr>");
		settingHtml.append("</table>");
		settingHtml.append("</td></tr>");
		settingHtml.append("</table></td></tr>");
		
		settingHtml.append("<script>");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append("	var upType; \n");
		settingHtml.append("	var upTypeArray = document.getElementsByName('upType'); \n");
		settingHtml.append("	for(var i=0;i<upTypeArray.length;i++){ \n");
		settingHtml.append("	if(upTypeArray[i].checked) upType = upTypeArray[i].value; \n");
		settingHtml.append("	} \n");
		
		settingHtml.append("    if (upType=='cons'){ ");
		settingHtml.append("    	return document.getElementById('COMBOX_CONS').value; \n");
		settingHtml.append("    } \n");
		settingHtml.append("    if (upType=='dataDict'){ ");
		settingHtml.append("    	return upType+'#'+document.getElementById('COMBOX_DATADICT').value; \n");
		settingHtml.append("    } \n");
		settingHtml.append("    if (upType=='dataSource'){ ");
		settingHtml.append("    	return upType+'#'+document.getElementById('COMBOX_DATASOURCE').value; \n");
		settingHtml.append("    } \n");
		
	    settingHtml.append("}\n"); 
		settingHtml.append("</script>\n");
		
		settingHtml.append("<script>");
		settingHtml.append(" document.getElementById('COMBOX_CONS').focus();\n");
		settingHtml.append("function visableCombox(obj){\n");
		settingHtml.append("    var upType = obj.value; ");
		settingHtml.append("    if (upType=='cons'){ ");
		settingHtml.append("    document.getElementById('dataDictTrea').style.display = 'none'; ");
//		settingHtml.append("    document.getElementById('dataSourceTrea').style.display = 'none'; ");
		settingHtml.append("    document.getElementById('consTrea').style.display = 'block'; ");
		settingHtml.append("    document.getElementById('consTrea').style.display = 'block'; } \n");
		
		settingHtml.append("	if (upType=='dataDict'){ ");
		settingHtml.append("	document.getElementById('consTrea').style.display = 'none'; ");
		settingHtml.append("	document.getElementById('consText').style.display = 'none'; ");
//		settingHtml.append("	document.getElementById('dataSourceTrea').style.display = 'none'; ");
		settingHtml.append("	document.getElementById('dataDictTrea').style.display = 'block'; } \n");
		
		settingHtml.append("	if (upType=='dataSource'){ ");
		settingHtml.append("	document.getElementById('consTrea').style.display = 'none'; ");
		settingHtml.append("	document.getElementById('consText').style.display = 'none'; ");
//		settingHtml.append("	document.getElementById('dataSourceTrea').style.display = 'block'; ");
		settingHtml.append("	document.getElementById('dataDictTrea').style.display = 'none'; } \n");
		
		settingHtml.append("}\n"); 
		settingHtml.append("</script>\n");
		return settingHtml.toString();
	}
	@Override
	public String getTrueValue() {
		String value=getValue();
//		if(value == null || value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		StringUtils util = new StringUtils(getMappingData(null));
		List<String> v = util.split("|");
		String trueValue=value;
		for(int i = 0 ; i < v.size() ; i++){
			String tmpGetValue = v.get(i);
			if(tmpGetValue.indexOf(":") != -1){
				StringUtils util2 = new StringUtils(tmpGetValue);
				List<String> v2 = util2.split(":");
				if(v2.size() == 2){
					if(value.equals(v2.get(0))){
						trueValue=v2.get(1);
						break;
					}
				}
			} else {
				if(value.equals(tmpGetValue)){
					trueValue=value;
					break;
				}
			}
		}
		return trueValue;
	}
	
}
package com.jd.oa.form.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

public class TmpFile {
	/**
	 * 获得专为excel中生成的临时文件路径，如果目录不存在，则创建一个临时目录
	 * 
	 * @param suffix
	 *            后缀名，可以为null
	 * @return 临时文件路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getExcelTmpFilePath(HttpServletRequest request,String suffix) {
		return getTmpFilePath(request,"Excel", "_", suffix);
	}
	/**
	 * 获得一个临时文件名的全路径,如果目录不存在，则创建一个临时目录
	 * 
	 * @param group
	 *            临时文件的类别，默认是default路径
	 * @param subGroup
	 *            临时文件的小类，默认是default路径
	 * @param suffix
	 *            临时文件名后缀,默认没有
	 * @return 临时文件路径及名称全路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getTmpFilePath(HttpServletRequest request,String group, String subGroup, String suffix) {
		if (group == null || group.equals(""))
			group = "groupDefault";
		if (subGroup == null || subGroup.equals(""))
			subGroup = "fileDefault";
		if (suffix == null)
			suffix = "";
		if (suffix.length() > 0)
			suffix = "." + suffix;
		String tmpPath = request.getSession().getServletContext().getRealPath("/");
		try {
			File file = new File(tmpPath);
			if (!file.exists())
				file.mkdirs();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		return System.currentTimeMillis() + suffix;
	}
	/**
	 * 获得专为excel中生成的临时文件夹路径
	 * 
	 * @return 临时文件夹路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getExcelTmpDirectoryPath(HttpServletRequest request) {
		return getTmpDirectoryPath(request,"Excel", "_");
	}
	/**
	 * 获得一个临时目录名
	 * 
	 * @param group
	 *            临时文件的类别，默认是default路径
	 * @param subGroup
	 *            临时文件的小类，默认是default路径
	 * @return
	 */
	public static String getTmpDirectoryPath(HttpServletRequest request,String group, String subGroup) {
		String tmpPath = request.getSession().getServletContext().getRealPath("/") + "WEB-INF/uploads";
		File file = new File(tmpPath);
		if(!file.exists()){
			file.mkdir();
		}
		return  tmpPath ;
	}
	/**
	 * 获得专为Word中生成的临时文件路径，如果目录不存在，则创建一个临时目录
	 * 
	 * @param suffix
	 *            后缀名，可以为null
	 * @return 临时文件路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getWordTmpFilePath(HttpServletRequest request,String suffix) {
		return getTmpFilePath(request,"Word", "_", suffix);
	}

	/**
	 * 获得专为Word中生成的临时文件夹路径
	 * 
	 * @return 临时文件夹路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getWordTmpDirectoryPath(HttpServletRequest request) {
		return getTmpDirectoryPath(request,"Word", "_");
	}

	/**
	 * 获得专为pdf中生成的临时文件路径，如果目录不存在，则创建一个临时目录
	 * 
	 * @param suffix
	 *            后缀名，可以为null
	 * @return 临时文件路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getPDFTmpFilePath(HttpServletRequest request,String suffix) {
		return getTmpFilePath(request,"PDF", "_", suffix);
	}

	/**
	 * 获得专为Word中生成的临时文件夹路径
	 * 
	 * @return 临时文件夹路径
	 * @preserve 声明此方法不被JOC混淆
	 */
	public static String getPDFTmpDirectoryPath(HttpServletRequest request) {
		return getTmpDirectoryPath(request,"PDF", "_");
	}
}

package com.jd.oa.form.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormTemplate;

public interface FormTemplateDao extends BaseDao<FormTemplate,String>{
	public FormTemplate getByFormId(String formId);
}

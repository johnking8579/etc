package com.jd.oa.form.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IOUtils;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.form.design.constant.FormStyleConstant;
import com.jd.oa.form.design.style.FormStyleInterface;
import com.jd.oa.form.design.style.impl.FormStyle_1_Impl;
import com.jd.oa.form.design.style.impl.FormStyle_2_Impl;
import com.jd.oa.form.design.style.impl.FormStyle_3_Impl;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.form.util.RepleaseKey;

@Controller
@RequestMapping(value = "/form")
public class FormTemplateController {
	
	  @Autowired
	  private FormItemService formItemService;
	    
	  @Autowired
	  private FormTemplateService formTemplateService;
	    
	  @Autowired
	  private FormService formService;
	  
	  @Autowired
	  private JssService jssService;
	
	/**
     * 打开表单设计器编辑页面
     * @param formId
     * @return
     */
    @RequestMapping(value = "/formTemplate_edit" ,  method = RequestMethod.GET)
    public ModelAndView getFormTemplate(HttpServletResponse response, String formId,String templateId,String templateName,boolean isPrint) {
    	Form form = formService.get(formId);
    	String formName = form.getFormName();
    	List<FormTemplate> formTemplates = formTemplateService.find(formId,isPrint ? "1" : "0");
    	String formTemplateSelectOption = getFormTelmplateSelectOption(formTemplates, templateId,isPrint);
    	String templateContext = "";
    	if(templateId != null){
    		FormTemplate formTemplate = formTemplateService.get(templateId);
    		if(formTemplate != null){
    			try {
					templateContext = getTemplateContext(formTemplate);
				} catch (IOException e) {
					throw new BusinessException("获取表单模板失败!",e);
				}
    			templateId = formTemplate.getId();
        		templateName = formTemplate.getTemplateName();
    		}
    		
    	} else {
    		try {
				templateContext = getTemplateContext(formTemplates);
			} catch (IOException e) {
				throw new BusinessException("获取表单模板失败!",e);
			}
    		templateId = getTemplateId(formTemplates);
    		templateName = getTemplateName(formTemplates);
    	}
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("formId", formId);
    	mv.addObject("formName", formName);
    	mv.addObject("templateName", templateName);
    	mv.addObject("templateId", templateId);
    	mv.addObject("formTelmplateSelectOption", formTemplateSelectOption);
    	mv.addObject("templateId", templateId == null ? "" : templateId);
    	mv.addObject("isPrint", isPrint);
    	if(isPrint){
    		mv.addObject("templateContext","<textarea id=\"printTemplate\" name=\"printTemplate\" style=\"width:99%;height:500px;visibility:hidden;\">" + templateContext + "</textarea>");
    	} else {
    		mv.addObject("templateContext","<textarea id=\"editTemplate\" name=\"editTemplate\" style=\"width:99%;height:500px;visibility:hidden;\">" + templateContext + "</textarea>");
    	}
    	mv.setViewName("form/formTemplate");
    	return mv;
    }
    /**
     * 打开表单设计器编辑页面
     * @param formId
     * @return
     */
    @RequestMapping(value = "/formTemplate_javascript" ,  method = RequestMethod.GET)
    public ModelAndView getFormTemplateJavascript(HttpServletResponse response,String formId) {
    	String javascriptContext = "";
    	if(formId != null){
    		javascriptContext = formTemplateService.getJavascriptValue(formId);
    	}
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("formId",formId);
    	mv.addObject("javascriptTemplate","<textarea id=\"javascriptTemplate\" name=\"javascriptTemplate\" style=\"width:98%;height:265px;\">" + JavascriptEscape.unescape(javascriptContext) + "</textarea>");
    	mv.setViewName("form/formTemplateJavascript");
    	return mv;
    }
    /**
     * 保存表单
     * @param response
     * @param formId
     * @param templateName
     * @param templateValue
     */
    @RequestMapping(value = "/formTemplate_javascript_save" , method = RequestMethod.POST)
    @ResponseBody
    public void saveFormTemplateJavascript(String formId,String javascriptContext) {
    	formTemplateService.saveJavascript(formId,JavascriptEscape.escape(javascriptContext));
    }
    /**
     * 返回modeAndview 对象
     * @param formId
     * @param templateContext
     * @param formName
     * @param templateId
     * @param formTelmplateSelectOption
     * @return
     */
    private ModelAndView getReturn(String formId,String templateContext,String templateName,String templateId,String formTelmplateSelectOption,boolean isPrint,String formName){
    	ModelAndView mv = new ModelAndView();
    	mv.addObject("formId", formId);
    	mv.addObject("formName", formName);
    	mv.addObject("templateContext", templateContext);
    	mv.addObject("templateName", templateName);
    	mv.addObject("templateId", templateId);
    	mv.addObject("formTelmplateSelectOption", formTelmplateSelectOption);
    	mv.addObject("templateId", templateId == null ? "" : templateId);
    	mv.addObject("isPrint", isPrint);
    	
    	mv.setViewName("form/formTemplate");
    	return mv;
    }
    private String getTemplateId(List<FormTemplate> formTemplates){
    	if(formTemplates == null || formTemplates.size() == 0){
    		return "";
    	} else {
    		if(formTemplates.get(0) != null){
    			return formTemplates.get(0).getId();
    		}else {
    			return "";
    		}
    	}
    }
    private String getTemplateName(List<FormTemplate> formTemplates){
    	if(formTemplates == null || formTemplates.size() == 0){
    		return "";
    	} else {
    		if(formTemplates.get(0) != null){
    			return formTemplates.get(0).getTemplateName();
    		}else {
    			return "";
    		}
    	}
    }
    /**
     * 获取表单内容
     * @param formTemplates
     * @return
     */
    private String getTemplateContext(List<FormTemplate> formTemplates) throws IOException{
    	if(formTemplates == null || formTemplates.size() == 0){//模板不存
    		return "<span style=\"font-size:23px;\">当前表单模板不存在,占击[自动生成]可以根据当前数据源自动构建出用户界面</span>";
    	} else {
    		return getTemplateContext(formTemplates.get(0));
    	}
    }
    /**
     * 获取表单内容
     * @param formTemplate
     * @return
     */
    private String getTemplateContext(FormTemplate formTemplate) throws IOException{
    	if(formTemplate != null){
			String templateContext = "";
			templateContext = formTemplateService.getTemplateValue(formTemplate.getTemplatePath());
			if(templateContext.trim().length() > 0){
				templateContext = JavascriptEscape.unescape(templateContext);
				if(templateContext.indexOf("</form>") > 0){
					int subLength = templateContext.substring(templateContext.indexOf("</form>") + "</form>".length()).length();
					StringBuffer sb = new StringBuffer(templateContext);
					sb.setLength(sb.length() - subLength);
					templateContext = sb.toString();
				}
				return templateContext;
			} else {
				return "<span style=\"color:red;font-size:23px;\">当前模版已不存在，请重新选择!</span>";
			}
		} else {
			return "<span style=\"font-size:23px;\">当前表单模板不存在</span>";
		}
    }
    /**
     * 拼凑表单option html片段落
     * @param formId
     * @return
     */
    private String getFormTelmplateSelectOption(List<FormTemplate> formTemplates,String defaultFormTemplateId,boolean isPrint){
    	StringBuffer optionHtml = new StringBuffer();
    	if(formTemplates != null && formTemplates.size() > 0){
    		for(int i = 0 ; i < formTemplates.size() ; i++){
    			FormTemplate formTemplate = formTemplates.get(i);
    			if(formTemplate != null && formTemplate.getTemplateType().equals(isPrint ? "1" : "0")){
    				if(defaultFormTemplateId != null && defaultFormTemplateId.equals(formTemplate.getId())){
    					optionHtml.append("<option selected value='").append(formTemplate.getId()).append("'>").append(formTemplate.getTemplateName()).append("\n");
    				}else {
    					optionHtml.append("<option value='").append(formTemplate.getId()).append("'>").append(formTemplate.getTemplateName()).append("\n");
    				}
    			}
    		}
    	}
    	return optionHtml.toString();
    }
    /**
     * 自动生成表单
     * @param formId
     * @return
     */
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/formTemplate_createAuto" , method = RequestMethod.GET)
    public void createFormTemplate(HttpServletResponse response, HttpServletRequest request, String formId,String templateValue,String templateName) {
    	FormStyleInterface style = null;
    	if(FormStyleConstant.FORM_STYLE_1.equals(templateValue)){
    		style = new FormStyle_1_Impl();//电子表单模板风络一
    	} else if(FormStyleConstant.FORM_STYLE_2.equals(templateValue)){
    		style = new FormStyle_2_Impl();//电子表单模板风络二
    	} else if(FormStyleConstant.FORM_STYLE_3.equals(templateValue)){
    		style = new FormStyle_3_Impl();//电子表单模板风络三
    	} 
    	if(style != null){
    		style.setHtmlName(templateValue);
    		style.setFormTemplateName(templateName);
    		style.setFormItem(formItemService.getFormItemByFormIdAndType(formId,"M"));
    		style.setSubSheets(formItemService.getFormItemByFormIdAndType(formId, "S"));
    		style.setForm(formService.get(formId));
    		style.setHttpServletRequest(request);
    		try {
    			response.getWriter().write(String.valueOf(style.getWeb()));
    		} catch (IOException e) {
    			
    		}
    	} else {
    		try {
    			response.getWriter().write("请实现表单风格");
    		} catch (IOException e) {
    			
    		}
    	}
    }
    /**
     * 新建设表单
     * @param response
     * @param formId
     * @param templateValue
     * @throws Exception 
     */
    @RequestMapping(value = "/formTemplate_createNew" , method = RequestMethod.GET)
    public void createNewFormTemplate(HttpServletRequest request,HttpServletResponse response, String formId,String templateName,boolean isPrint) throws BusinessException {
    	FormStyleInterface style = new FormStyle_1_Impl();
    	style.setHtmlName(FormStyleConstant.FORM_STYLE_1);
    	style.setFormItem(formItemService.getFormItemByFormIdAndType(formId,"M"));
		style.setSubSheets(formItemService.getFormItemByFormIdAndType(formId, "S"));
		Form form = formService.get(formId);
		if(form == null){
			throw new BusinessException("当前指定的form数据异常！");
		}
		style.setForm(form);
		style.setFormTemplateName(templateName);
		style.setHttpServletRequest(request);
		String templateContext = String.valueOf(style.getWeb());
		try {
			response.getWriter().write(templateContext);
		} catch (IOException e) {
			throw new BusinessException("获取表单模板失败!",e);
		}
    }
    /**
     * 保存表单
     * @param response
     * @param formId
     * @param templateName
     * @param templateValue
     * @return
     */
    @RequestMapping(value = "/formTemplate_save" , method = RequestMethod.POST)
    @ResponseBody
    public String saveFormTemplate(String formId,String templateName,String templateValue,String templateId,boolean isPrint,String formName) {
    	if(templateId != null && templateId.trim().length() > 0){
    		FormTemplate formTemplate = formTemplateService.get(templateId);
    		formTemplateService.update(formTemplate,JavascriptEscape.escape(templateValue));
    	} else {
    		FormTemplate formTemplate = new FormTemplate();
        	formTemplate.setFormId(formId);
        	formTemplate.setTemplateName(templateName);
        	formTemplate.setTemplateType(isPrint ? "1" : "0");
    		templateId = formTemplateService.save(formTemplate,JavascriptEscape.escape(templateValue));
    	}
    	try {
			String templateContext = getTemplateContext(formTemplateService.get(templateId));
		} catch (IOException e) {
			throw new BusinessException("获取表单模板失败!",e);
		}
    	String formTelmplateSelectOption = "";
    	return templateId;
    }
    /**
     * 重命名
     * @param formId
     * @param templateName
     * @param templateValue
     * @param templateId
     * @return
     */
    @RequestMapping(value = "/formTemplate_rename" , method = RequestMethod.POST)
    public ModelAndView renameFormTemplate(String formId,String templateName,String templateId,boolean isPrint,String formName) {
    	if(templateId != null && templateId.trim().length() > 0){
    		FormTemplate formTemplate = formTemplateService.get(templateId);
    		formTemplate.setTemplateName(templateName);
    		formTemplateService.update(formTemplate);
    	}
    	String templateContext = "";
		try {
			templateContext = getTemplateContext(formTemplateService.get(templateId));
		} catch (IOException e) {
			throw new BusinessException("获取表单模板失败!",e);
		}
    	String formTelmplateSelectOption = "";
    	return getReturn(formId, templateContext, templateName, templateId, formTelmplateSelectOption,isPrint,formName);
    }
    /**
     * 另存为
     * @param formId
     * @param templateName
     * @param templateId
     * @return
     */
    @RequestMapping(value = "/formTemplate_saveAs" , method = RequestMethod.GET)
    @ResponseBody
    public String saveAsFormTemplate(HttpServletResponse response,String formId,String templateName,String templateId) {
    	FormTemplate formTemplate = formTemplateService.get(templateId);
    	if(formTemplate != null){
    		FormTemplate newFormTemplate = new FormTemplate();
    		try {
				ComUtils.copyProperties(formTemplate, newFormTemplate);
				newFormTemplate.setId(null);
				newFormTemplate.setTemplateName(templateName);
				return formTemplateService.insert(newFormTemplate);
			} catch (Exception e) {
				throw new BusinessException("表单模板数据插入失败!",e);
			}
    	}
    	return "";
    }
    /**
     * 删除
     * @param formId
     * @param templateId
     * @return
     */
    @RequestMapping(value = "/formTemplate_delete" , method = RequestMethod.GET)
    @ResponseBody
    public String deleteFormTemplate(String formId,String templateId,boolean isPrint) {
    	return String.valueOf(formTemplateService.delete(templateId));
    }
    /**
     * 预览表单模板
     * @param templateValue
     * @return
     */
    @RequestMapping(value = "/formTemplate_review" , method = RequestMethod.GET)
    public ModelAndView formTemplateReview(HttpServletRequest request,String templateId,String formId) {
    	ModelAndView mv = new ModelAndView();
    	FormTemplate formTemplate = formTemplateService.get(templateId);
    	mv.setViewName("form/formTemplate_review");
    	String templateContext = "";
		try {
			templateContext = RepleaseKey.replace(getTemplateContext(formTemplate), formTemplateService.getRuntimeForm(request,formId), "[@", "]");
		} catch (IOException e) {
			throw new BusinessException("获取表单模板失败!",e);
		}
    	mv.addObject("templateHtml", templateContext);
    	return mv;
    }
    
    /**
     * 测试jss获取文件速度
     * @param templateValue
     * @return
     */
    @RequestMapping(value = "/formTemplate_testJssExecute" , method = RequestMethod.GET)
    @ResponseBody
    public String testJssExecute(String templatePath,int exeTimes){
    	if(exeTimes>1000||exeTimes<=0)return "执行次数过大或过小，请重新输入！";
    	if(null==templatePath||"".equals(templatePath))return "未获取到流程模板文件路径！";
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < exeTimes; i++) {
    		long begin = System.currentTimeMillis();
    		jssExecute(templatePath);
    		long end = System.currentTimeMillis();
    		String fix = "";
    		if(i<9)fix = "0";
    		sb.append("获取流程表单模板时间"+fix+(i+1)+"：       "+(end-begin)+" 毫秒******************************\n");
		}
    	return sb.toString();
    }
    
    private void jssExecute(String templatePath){
		InputStream in = null;
		try{
			in = jssService.downloadFile(SystemConstant.BUCKET, templatePath);
		} catch(Exception e){
        }finally {
        	try{
        		in.close();
        	}catch(IOException e) {
        	} 
    }}
    
   /**
    * 获取模板名称和路径selects
    * @return
    */
   @RequestMapping(value = "/formTemplate_getTelmplateSelect" , method = RequestMethod.GET)
   @ResponseBody
   private String getTelmplateSelect(){
	   	StringBuffer optionHtml = new StringBuffer();
	   	FormTemplate ft = new FormTemplate();
	   	ft.setYn(0);
	    List<FormTemplate> formTemplates = formTemplateService.find(ft);
	   	if(null != formTemplates && formTemplates.size() > 0){
	   		for (FormTemplate formTemplate : formTemplates) {
	   			optionHtml.append("<option value='").append(formTemplate.getTemplatePath()).append("'>").append(formTemplate.getTemplateName()).append("</option>\n");
			}
	   	}
	   	return optionHtml.toString();
   }
   
   /**
    * 测试JSS表单
    * @param templateValue
    * @return
    */
   @RequestMapping(value = "/jss_test" , method = RequestMethod.GET)
   public ModelAndView jssTemplate() {
   	ModelAndView mv = new ModelAndView();
   	mv.setViewName("form/jss_test");
   	return mv;
   }
}

package com.jd.oa.form.model.bean;

import java.io.Serializable;
import java.util.List;

import com.jd.oa.form.model.FormItem;

public class FormBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5770839986113120071L;
	
	private String formId;//表单ID
	private String formName;//表单名称
	private String formCode;//对应的业务物理表名
	
	private List<FormItem> listFormItems;
	
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public List<FormItem> getListFormItems() {
		return listFormItems;
	}
	public void setListFormItems(List<FormItem> listFormItems) {
		this.listFormItems = listFormItems;
	}
	public String getFormCode() {
		return formCode;
	}
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
}
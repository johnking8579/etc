package com.jd.oa.form.execute.expression.impl;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;

import java.util.Map;

public class OrganizationNameExpressionImpl  extends ExpressionAbst{
	public OrganizationNameExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private SysUserService sysUserService;
	private SysOrganizationService sysOrganizationService;
	public String expressionParse(String expression) {
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean("sysUserService");
		}
		if(sysOrganizationService == null){
			sysOrganizationService = SpringContextUtils.getBean("sysOrganizationService");
		}
		if(expression.contains("#")){
			String num = expression.substring(expression.lastIndexOf("(#")+2,expression.lastIndexOf(")"));
			Map<String,String> map = sysUserService.getOrganizationMap(ComUtils.getLoginNamePin());
			return map.get("ORG_"+num+"_NAME")==null?"":map.get("ORG_"+num+"_NAME");
		}else{
			SysUser user = (SysUser) sysUserService.getByUserName(ComUtils.getLoginNamePin());
			if(user != null){
				SysOrganization organization = sysOrganizationService.get(user.getOrganizationId());
				return organization.getOrganizationName();
			}
		}
		return "";
	}
}

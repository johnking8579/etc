package com.jd.oa.form.execute.component.impl;

import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Map;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentMoneyImpl extends FormUIComponentAbst {

	public FormUIComponentMoneyImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}
//
//	public String getModifyHtmlDefine(Hashtable params) {
//		UserContext me = (UserContext) params.get("me");
//		String lang = me!=null?me.getLanguage():"cn";
//		String fieldLength = "";
//		String cascadeType = "FORM";
//		if(params.get("cascadeType")!=null){
//			cascadeType = params.get("cascadeType").toString();
//		}
//		if (!getMetaDataMapModel()._fieldLenth.equals("0")) {
//			if (getMetaDataMapModel()._fieldLenth.indexOf(",") != -1) {
//				int length = Integer.parseInt(getMetaDataMapModel()._fieldLenth.substring(0, getMetaDataMapModel()._fieldLenth.indexOf(",")));
//				length = length + length / 6 + 3;
//				fieldLength = "maxlength=" + length;
//			} else {
//				int length = Integer.parseInt(getMetaDataMapModel()._fieldLenth);
//				length = length + length / 6 + 3;
//				fieldLength = " maxlength=" + length;
//			}
//		}
//
//		StringBuffer fieldHtml = new StringBuffer();
//		String innerHtml = getMetaDataMapModel()._htmlInner;
//		if (innerHtml.toLowerCase().indexOf(" readonly ") > 1 || innerHtml.toLowerCase().indexOf("disabled") > -1) {
////			innerHtml += " style='border-width: 0px 0px 1px 0px'";
//			innerHtml +=" class='actionsoftInputDisabled'";
//		} else {
//			innerHtml += "  class ='actionsoftInput' ";
//		}
//		String strValue = getValue();
//		String value = "";
//		MetaDataMapModel metaDataMapModel = getMetaDataMapModel();
//		if (strValue != null && strValue.trim().length() > 0) {
//			NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
//			double number =0;
//			try{
//				number=Double.parseDouble(getValue());
//			}catch(Exception e){}
//			value = currencyFormat.format(number);
//			if (value.indexOf("-") == 0) {
//				value = "-"+value.substring(2, value.length());
//			} else {
//				value = value.substring(1, value.length());
//			}
//		}
// 		String moneyType= "" ;
//		fieldHtml.append("<input type='text'  ").append(innerHtml).append(" name='")
//					.append(getMetaDataMapModel()._fieldName).append("' ")
//					.append(fieldLength).append(" size='")
//					.append(getMetaDataMapModel()._inputWidth)
//					.append("' ");
//		//�Ƿ������˼���
//		if(metaDataMapModel._displaySQL.indexOf(CASCADE)>-1){
//			moneyType = metaDataMapModel._displaySQL.split("\\>")[1];
//		}else{
//			moneyType = metaDataMapModel._displaySQL==null||metaDataMapModel._displaySQL.trim().length()==0?"��":metaDataMapModel._displaySQL;
//		}
//		value=moneyType+value;
//		fieldHtml.append(" value='").append(value).append("' onchange='number2MoneyOfMoneyFormUIComponent(\""+moneyType+"\",this);' ").append(">");
//		if (getMetaDataMapModel()._isNotNull && params.get("bindId") != null) {
//			fieldHtml.append("<img src=../aws_img/notNull.gif alt="+I18nRes.findValue(lang, "������д")+">");
//		} else {
//			fieldHtml.append("<img src=../aws_img/null.gif alt="+I18nRes.findValue(lang, "�����")+">");
//		}
//		return fieldHtml.toString();
//	}
//
//	/**
//	 * @preserve �����˷�������JOC����.
//	 */
//	public String getReadHtmlDefine(Hashtable params) {
//		StringBuffer html = new StringBuffer();
//		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
//
//		String strValue = getValue();
//		String value = "";
//		if (strValue != null && strValue.trim().length() > 0) {
//			double number = Double.parseDouble(getValue());
//			value = currencyFormat.format(number);
//			if (value.indexOf("-") == 0) {
//				value = "-"+value.substring(2, value.length());
//			} else {
//				value = value.substring(1, value.length());
//			}
//		}
//		MetaDataMapModel metaDataMapModel = getMetaDataMapModel();
//		if(metaDataMapModel._displaySQL.toUpperCase().indexOf(CASCADE)>-1)
//			metaDataMapModel._displaySQL = metaDataMapModel._displaySQL.split("\\>")[1];
//		else
//			metaDataMapModel._displaySQL = metaDataMapModel._displaySQL;
//		String moneyType=metaDataMapModel._displaySQL==null||metaDataMapModel._displaySQL.trim().length()==0?"��":metaDataMapModel._displaySQL;
//		value=moneyType+value;
//		html.append("<input type=hidden name='").append(getMetaDataMapModel()._fieldName).append("' value=\"").append(getValue()).append("\">").append(value);
//		return html.toString();
//	}
//
//	/**
//	 * ��ȡ��ǰ���ֵ�Ŀͻ��˱���ֵ��BO��JavaScript����Ƭ��
//	 * 
//	 * @param params
//	 * @return
//	 * @preserve �����˷�������JOC����.
//	 */
//	public String getValueJavaScript(Hashtable params) {
//		StringBuffer javaScript = new StringBuffer();
//		MetaDataMapModel metaDataMapModel = getMetaDataMapModel();
//		String tmp = "";
//		if(metaDataMapModel._displaySQL.indexOf(CASCADE)>-1){
//			tmp = metaDataMapModel._displaySQL.split("\\>")[1];
//		}else{
//			tmp = metaDataMapModel._displaySQL;
//		}
//		String moneyType=tmp==null||tmp.trim().length()==0?"��":tmp;
//		javaScript.append("try{\nvar _TMP_MONEY=frmMain.").append(metaDataMapModel._fieldName).append(".value;\n");
//		if(moneyType.equals("$")){
//			moneyType="\\"+moneyType;
//		}
//		javaScript.append("_TMP_MONEY=replaceAll2(_TMP_MONEY,\""+moneyType+"\",\"\");\n");
//		javaScript.append("_TMP_MONEY=replaceAll2(_TMP_MONEY,\",\",\"\");\n");
//		javaScript.append("\n try{ bv=bv+'_'+'").append(metaDataMapModel._fieldName).append("'+'{'+_TMP_MONEY+'}'+'").append(metaDataMapModel._fieldName).append("'+'_  ';\n }catch(e){}}catch(e){}\n");
//		return javaScript.toString();
//	}
//	public String getSettingWeb() {
//		StringBuffer settingHtml = new StringBuffer();
//		settingHtml.append("<tr><td colspan=\"2\" ><input type=text  name='AWS_CHECK_CONS' onkeypress='if(event.keyCode==13||event.which==13){return false;}' id='AWS_CHECK_CONS' class ='actionsoftInput' value='' size=70 ></td></tr>");
//		settingHtml.append("<tr >");
//		settingHtml.append("<td colspan=\"2\" ><I18N#���ҷ��:Ĭ�ϲ���дʱ���ҷ��Ϊ����ҡ�����></td></tr>");
//		settingHtml.append("<script>");
//		settingHtml.append("function initEditor(){return getUIComponentConfig('" + getMetaDataMapModel()._displayType + "');}\n");
//		settingHtml.append("try{initInputConfig(escape('").append(getMetaDataMapModel()._displaySQL.indexOf(CASCADE)>-1?getMetaDataMapModel()._displaySQL.split(">")[1]:getMetaDataMapModel()._displaySQL).append( "'),'AWS_CHECK_CONS');}catch(e){alert(e.description);};");
//		settingHtml.append("</script>\n"); 
//		
//		return settingHtml.toString();
//	}
//	//override
//	public String getSettingWeb(Map fields) {
//		StringBuffer settingHtml = new StringBuffer();
//		String displaySql = getMetaDataMapModel()._displaySQL;
//		StringBuffer fieldArray = new StringBuffer("var fieldArray=[");
//		String cscade = "";
//		settingHtml.append("</td>"); 
//		settingHtml.append("</tr>");
//		settingHtml.append("<tr>");
//		settingHtml.append("<td>");
//		settingHtml.append("<table  width=\"100%\" align=\"center\" cellpadding=\"3\" id='cascadeTable' >");
//		settingHtml.append("<tr>");
//		settingHtml.append("<td width=\"11%\" nowrap><I18N#���ü����ֶ�></td>");
//		settingHtml.append("<td nowrap><div id='cascadeField' />");
//		settingHtml.append("</td>");
//		settingHtml.append("</tr>");
//		settingHtml.append("</table>");
//		if(displaySql.indexOf("CASCADE")>-1){
//			cscade = displaySql.split("=")[1];
//			if(cscade.indexOf(">")>-1){
//				cscade = cscade.substring(0,cscade.indexOf(">"));
//			}
//		}
//		java.util.Iterator it = fields.entrySet().iterator();
//		while(it.hasNext()){
//			java.util.Map.Entry entry = (java.util.Map.Entry)it.next();
//			fieldArray.append("['").append( entry.getValue() ).append("',").append( "'").append( entry.getValue() ).append("'],");
//		}
//		if( fieldArray.toString().endsWith(",")){
//			fieldArray.setLength( fieldArray.length()-1);
//		}
//		fieldArray.append("];");
//		settingHtml.append("<script>").append("\n");
//		settingHtml.append(fieldArray.toString()).append("\n");
//		settingHtml.append("var cascadeWho = '").append(cscade).append("';\n");
//		settingHtml.append("Ext.onReady(InitCascade.init,InitCascade,true);").append("\n");
//		settingHtml.append("</script>");
//		return settingHtml.toString();
//	}
//	public static void main(String[] args) {
//		double number = Double.parseDouble("-99811111111111.22");
//		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
//		System.out.println(currencyFormat.format(number));
//	}

	@Override
	public String getTrueValue() {
		return getValue();
	}
}

package com.jd.oa.form.dao.impl;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.dao.FormSequenceDao;
import com.jd.oa.form.model.FormSequence;
@Component
public class FormSequenceDaoImpl  extends MyBatisDaoImpl<FormSequence,String> implements FormSequenceDao {
   
	 private static final int DEFAULT_SEQUENCE_VALUE = 1;//黔认值
	 
	 private static final int DEFAULT_SEQUENCE_STEP_VALUE = 1;//step默认值
	 /**
	  * @see
	  */
	 @Override
	 public int getFormSequenceValue(String typeId,String sequenceType){
			int result = 0;
	        try {
	        	FormSequence param = new FormSequence();
	        	param.setSequenceType(sequenceType);
	        	param.setTypeId(typeId);
	        	FormSequence formSequence = (FormSequence) this.getSqlSession().selectOne("com.jd.oa.form.model.FormSequence.getByCondition", param);
	        	if(formSequence == null){//不存在的sequence;插入值
	        		formSequence = new  FormSequence();
	        		formSequence.setSequenceType(sequenceType);
	        		formSequence.setTypeId(typeId);
	        		formSequence.setSequenceValue(DEFAULT_SEQUENCE_VALUE);
	        		formSequence.setSequenceStep(DEFAULT_SEQUENCE_STEP_VALUE);
	        		super.insert(formSequence);
	        		result =  formSequence.getSequenceValue();
	        	} else {
	        		result = formSequence.getSequenceValue();
	        	}
				//取值完后，根据step进行更新
				int step = formSequence.getSequenceStep();
				formSequence.setSequenceValue(result + step);
				super.update(formSequence);
	        } catch (DataAccessException e) {
	            throw e;
	        }
	        return result;
		}
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-6-4
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.form.execute.expression.impl;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

import java.util.Map;

public class UserCompanyExpressionImpl extends ExpressionAbst {
	private SysUserService sysUserService;
	public UserCompanyExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	public String expressionParse(String expression) {
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean(SysUserService.class);
		}
		SysUser user = (SysUser) sysUserService.getByUserName(ComUtils.getLoginNamePin());
		if(user != null) return user.getCodename();
		return "";
	}
}

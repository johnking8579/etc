package com.jd.oa.form.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormItemBean;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单Service
 * User: yujiahe
 * Date: 13-9-12
 * Time: 上午11:56
 * To change this template use File | Settings | File Templates.
 */
public interface FormService extends BaseService<Form, String> {
    /**
     * @param form
     * @return String
     * @Description: 根据表单名称查看表单是否存在
     * @author yujiahe
     */
    boolean isExistForm(Form form);

    /**
     * @param owner
     * @param ids
     * @return String
     * @Description: 保存变更所有人
     * @author yujiahe
     */
    public Map<String,Object> updateOwner(String owner, String ids);
    /**
     * @Description:删除表单支持批量
     * @author yujiahe
     */
    void mutidelete(String ids,String formType);
    
    /**
	  * @Description: 根据ID查询信息
	  * @param id
	  * @return Form
	  * @author xulin
	  * @date 2013-9-16下午01:49:54 
	  * @version V1.0
	 */
	public Form selectByPrimaryKey(String id);
	
	/**
	 * 
	 * @param tableId
	 * @author xulin
	 * @return
	 */
	public List<FormItemBean> findFormItemsByTableId(String tableId);
	
	/**
	 * 高级模式下更新表单和表单明细项
	 * @param mainFormModel
	 * @param subFormModelList
	 * @param formItemList
	 * @author xulin
	 */
	public void operateFormAndFormItem(Form mainFormModel,List<Form> subFormModelList,List<FormItem> formItemList);
	
	/**
	 * 高级模式下删除表单下的子表单和表单明细项
	 * @param formId
	 * @author xulin
	 */
	public void operateFormAndFormItem(String formId);
	
	/**
	  * @Description: 自动生成业务表及其下属字段
	  * @param tableId
	  * @param resultFormTable
	  * @param resultTableFields
	  * @author xulin
	  * @date 2013-9-25下午06:22:13 
	  * @version V1.0
	 */
	public Map<String,Object> createBusinessTableAuto(String tableId,List<FormBusinessTable> resultFormTable,
			List<FormBusinessField> resultTableFields,List<Form> resultForm,boolean check);
	
	/**
	  * @Description: 复制模式时创建表单及其表单项目
	  * @param newFormList
	  * @param newFormItemList
	  * @author xulin
	  * @date 2013-9-26下午02:09:13 
	  * @version V1.0
	 */
	public void createFormAndFormItem(List<Form> newFormList,List<FormItem> newFormItemList);
	
	/**
	 * 
	 * @param processDefintonId
	 * @author xulin
	 * @return
	 */
	public String getFormCodeByProDId(String processDefintonId);
	
	public Map<String,Object> getList(String sql,String type);
	
	public List<Form> findFormWithSubForm(String formId);
}


package com.jd.oa.form.execute.expression.impl;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;

public class YearExpressionImpl extends ExpressionAbst {

	public YearExpressionImpl(Map<String,Object> paramMaps, String expressionValue) {
		super(paramMaps, expressionValue);
	}

	public String expressionParse(String expression) {
		return DateUtils.yearFormat(new Timestamp(System.currentTimeMillis()));
	}

}

package com.jd.oa.form.execute.component;

import java.util.Map;

import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.form.model.FormBusinessField;
/**
 * 抽取组件抽象部分代码
 * @author liub
 *
 */
public abstract class FormUIComponentAbst implements UIComponentInterface {

	/**
	 * 一个字段模型的描述
	 */
	private FormBusinessField _metaDataMapModel;

	/**
	 * 组件值
	 */
	private String _value;
	
	private String _formId;
	
	/**
	 * 内置css/javascript
	 */
	protected String getHtmlInner(){
		return this._metaDataMapModel.getHtmlInner() == null ? "" : this._metaDataMapModel.getHtmlInner(); 
	}
	
	protected final String CASCADE="CASCADE";
	/**
	 * 构造
	 * 
	 * @param metaDataMapModel
	 *            一个字段模型的描述
	 * @param value
	 *            当前组件值
	 */
	public FormUIComponentAbst(FormBusinessField metaDataMapModel, String value) {
		setMetaDataMapModel(metaDataMapModel);
		setValue(value);
	}
	
	public String getJavaScriptExtendEvents(){
		return "";
	}

	/**
	 * 获取只读状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	public String getReadHtmlDefine(Map<String,Object> params) {
		return _value;
	}

	/**
	 * 获取编辑状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	public String getModifyHtmlDefine(Map<String,Object> params) {
		return _value;
	}
	/**
	 * 查询业务表数据查询调整生成 获取组件html代码，该方法被重载  应用与日期与数字类型
	 * @param params
	 * @return
	 * @author yujiahe
	 */
	public String getQueryHtmlDefine(Map<String,Object> params){
		return _value;
	}
	public String getHiddenHtmlDefine(Map<String,Object> params){
//		getMetaDataMapModel().getFieldName()
		StringBuffer html = new StringBuffer();
		html.append("<input type='hidden' name='" + getMetaDataMapModel().getFieldName() + "' id='" + getMetaDataMapModel().getFieldName() + "' value='" + getValue() + "'>");
		html.append("<script>")
			.append("$('#").append(getMetaDataMapModel().getFieldName()).append("_div').hide();")
			.append("</script>");
		return html.toString();
	}

	/**
	 * 获取当前组件的客户端JavaScript校验代码，包括字段超长、是否允许为空、常规值校验
	 * 
	 * @param params
	 * @return
	 */
	public String getValidateJavaScript(Map<String,Object> params) {
		FormBusinessField metaDataMapModel = getMetaDataMapModel();
		StringBuffer jsCheckvalue = new StringBuffer();
		jsCheckvalue.append(" var ").append(getMetaDataMapModel().getFieldName()).append(" = ").append("$('#").append(getMetaDataMapModel().getFieldName()).append("');");
		// 非空校验
		if (!isNull()) {
			jsCheckvalue.append("\n try{\n")
						.append("     if(").append(getMetaDataMapModel().getFieldName()).append(".val().length == 0)").append("{\n")
						.append(" 	    alert('请给字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入一个值，该字段值不允许为空！');\n")
						.append("	    try{ \n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("	   }catch(e){}\n")
						.append("	   return false; \n")
						.append("    }\n ")
						.append("  }catch(e){}\n\n ");
		}
		// 字段超长校验
		if (metaDataMapModel.getLength() != null && !metaDataMapModel.getLength().equals("0")) { // 如果等于0，表示不限制宽度
			if (metaDataMapModel.getLength().indexOf(",") != -1) {
				int fLength = Integer.parseInt(metaDataMapModel.getLength().substring(0, metaDataMapModel.getLength().indexOf(",")));
				int bLength = Integer.parseInt(metaDataMapModel.getLength().substring(metaDataMapModel.getLength().indexOf(",") + 1, metaDataMapModel.getLength().length()));
				int length = fLength - bLength;
				jsCheckvalue.append("\n try{ \n")
							.append("		if(").append(getMetaDataMapModel().getFieldName()).append(".val().indexOf('.')==-1 && length2(").append(getMetaDataMapModel().getFieldName()).append(".val())>").append(length).append("){\n")
							.append("     		alert('字段:").append(metaDataMapModel.getFieldChineseName()).append("值太长了，此字段最多可输入").append(length).append("位数字！');")
							.append(getErrorTip(metaDataMapModel.getFieldName()))
							.append(" 			return false; \n")
							.append("        } \n")
							.append("   }catch(e){}\n\n ");
				jsCheckvalue.append("\n try{ \n")
							.append("      if(").append(getMetaDataMapModel().getFieldName()).append(".val().substring(0,").append(getMetaDataMapModel().getFieldName()).append(".val().indexOf('.')).length>").append(length).append("){\n")
							.append("  		    alert('字段:").append(metaDataMapModel.getFieldName()).append("值太长了，此字段最多可输入").append(length).append("个整数字符" + bLength + "个小数字符！');\n")
							.append(getErrorTip(metaDataMapModel.getFieldName()))
							.append("           return false; \n")
							.append("       }\n")
							.append("   }catch(e){}\n\n ");
			} else {
				jsCheckvalue.append("\n try{ \n")
				            .append(" 		if(length2(").append(getMetaDataMapModel().getFieldName()).append(".val()) > ").append(metaDataMapModel.getLength()).append("){\n")
				            .append("      		alert('字段:").append(metaDataMapModel.getFieldChineseName()).append("值太长了，此字段最多可输入").append(metaDataMapModel.getLength()).append("个字符！');\n")
				            .append(getErrorTip(metaDataMapModel.getFieldName()))
							.append("      		return false; \n")
							.append("        }\n")
							.append("   }catch(e){}\n\n ");
			}
		}

		// 基本类型校验
		if (metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))) {
			jsCheckvalue.append("\n try{\n")
						.append(" 		if(!IsDate(").append(getMetaDataMapModel().getFieldName()).append(".val()))").append("{ \n")
						.append("           alert('字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入的值是一个非法的日期格式！');\n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("      		return false; \n")
						.append("        }\n")
						.append("   }catch(e){}\n\n ");
		}
		if (metaDataMapModel.getDataType().equals(String.valueOf(DataType.NUMBER))) {
			jsCheckvalue.append("\n try{ \n")
						.append("      if(!IsNumber(").append(getMetaDataMapModel().getFieldName()).append(".val()))").append("{")
						.append("           alert('字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入的值是一个非法的数字格式！');\n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("      		return false; \n")
						.append("        }\n")
						.append("   }catch(e){}\n\n ");
		}

		return jsCheckvalue.toString();
	}

	/**
	 * 获取当前组件值的客户端JavaScript代码
	 * 
	 * @param params
	 * @return
	 */
	public String getValueJavaScript(Map<String,Object> params) {
		StringBuffer javaScript = new StringBuffer();
//		FormBusinessField metaDataMapModel = getMetaDataMapModel();
//		javaScript.append("\n try{ bv=bv+'_'+'").append(metaDataMapModel._fieldName).append("'+'{'+form.").append(metaDataMapModel._fieldName).append(".value+'}'+'").append(metaDataMapModel._fieldName).append("'+'_  ';\n }catch(e){}\n");
		return javaScript.toString();
	}

	/**
	 * 获取当前字段模型的编辑界面
	 * 
	 * @return
	 */
	public String getSettingWeb() {
		return "";
	}

	/**
	 * 获取当前字段模型的描述
	 * 
	 * @return
	 */
	public FormBusinessField getMetaDataMapModel() {
		return _metaDataMapModel;
	}

	/**
	 * set MetaDataMapModel
	 * 
	 * @param metaDataMapModel
	 */
	public void setMetaDataMapModel(FormBusinessField metaDataMapModel) {
		_metaDataMapModel = metaDataMapModel;
	}
	
	
	@Override
	public String getFormId() {
		return _formId;
	}
	
	@Override
	public void setFormId(String _formId) {
		this._formId = _formId;
	}

	/**
	 * 获取当前组件的值
	 * 
	 * @return
	 */
	public String getValue() {
		return _value;
	}

	/**
	 * 设置当前组件的值
	 * 
	 * @param value
	 */
	public void setValue(String value) {
		_value = value;
	}

	/**
	 * 转换参数中可能存在的@命令，此封装提供给UI扩展开发者，封装内部调用
	 * @param params read、modify方法传入的参数
	 * @param str 可能含有@命令的str
	 * @return 解析结果
	 */
//	public String convertATScript(Hashtable params, String str) {
//		if (str == null || str.indexOf("@") == -1)
//			return str;
//		RuntimeFormManager rfm = (RuntimeFormManager) params.get("runtimeFormManager");
//		return rfm.convertMacrosValue(str);
//	}
	protected final String getErrorTip(String fieldName){
		StringBuffer errorTip = new StringBuffer();
		errorTip.append("		   ").append(fieldName).append(".css('border','3 dotted green');\n");
		errorTip.append("		   ").append(fieldName).append(".focus();\n");
		errorTip.append("		   ").append(fieldName).append(".select();\n");
		return errorTip.toString();
	}
	/**
	 * 是否为空
	 * @return true : 允许为空，false : 不允许 
	 */
	@Override
	public boolean isNull(){
		if(getMetaDataMapModel().getIsNull() != null && getMetaDataMapModel().getIsNull().equals("0")){
			return false;
		} else {
			return true;
		}
	}
	/**
	 * 是否隐藏
	 * @return true : 隐藏，false : 显示
	 */
	@Override
	public boolean isHidden(){
		if(getMetaDataMapModel().getIsHidden() != null && getMetaDataMapModel().getIsHidden().equals("1")){
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 是否编辑
	 * @return true : 编辑,false : 只读
	 */
	@Override
	public boolean isEdit(){
		if(getMetaDataMapModel().getIsEdit() != null && getMetaDataMapModel().getIsEdit().equals("1")){
			return true;
		} else {
			return false;
		}
	}

}

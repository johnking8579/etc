package com.jd.oa.form.execute.component.impl.sub;

import java.util.List;

import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentSubSheetNumberImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetNumberImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
//	@Override
//	public String getFormate(){
//		return "";
//	}
	@Override
	public String getEditor(){
		String length = getMetaDataMapModel().getLength();
		String precision = "8";
		if(length != null && length.indexOf(",") != -1){
			StringUtils util = new StringUtils(getMetaDataMapModel().getLength());
			List<String> list = util.split(",");
			precision = "max : " + Math.pow(10, Integer.valueOf(list.get(0).toString())) + ",precision :" + list.get(1); 
		} else {
			precision = "max:" + Math.pow(10, Integer.valueOf(length));
		}
		return "editor:{type:'numberbox',options:{" + precision + "}}";
	}
}

package com.jd.oa.form.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.dao.FormBusinessTableDao;
import com.jd.oa.form.model.FormBusinessTable;

/** 
 * @Description: 自定义表Dao实现
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */

@Repository("formBusinessTableDao")
public class FormBusinessTableDaoImpl extends MyBatisDaoImpl<FormBusinessTable, String>
		implements FormBusinessTableDao {
	
	/**
	  * @Description: 查询主表列表
	  * @return List<FormBusinessTable>
	  * @author guoqingfu
	  * @date 2013-9-12下午01:53:44 
	  * @version V1.0
	 */
	public List<FormBusinessTable> findParentTableList(){
		List<FormBusinessTable> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.form.model.FormBusinessTable.findParentTableList");
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	/**
	  * @Description:根据ID查询数据表信息
	  * @param formBusinessTableId
	  * @return FormBusinessTable
	  * @author guoqingfu
	  * @date 2013-9-12下午02:44:03 
	  * @version V1.0
	 */
	public FormBusinessTable getFormBusinessTableInfo(String formBusinessTableId){
		FormBusinessTable result = null;
        try {
            result = (FormBusinessTable) this.getSqlSession().selectOne("com.jd.oa.form.model.FormBusinessTable.getFormBusinessTableInfo", formBusinessTableId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	
	/**
	  * @Description: 判断是否有字段或是已被表单引用
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午03:59:31 
	  * @version V1.0
	 */
	public boolean isUsed(Map<String, Object> map){
		List<FormBusinessTable> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.form.model.FormBusinessTable.isUsed",map);
            if(result != null && result.size()>0){
            	return true;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return false;
	}
	/**
	  * @Description: 删除自定义表
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午04:27:02 
	  * @version V1.0
	 */
	public boolean deleteFormBusinessTable(Map<String, Object> map){
		 boolean flag = false;
	        try {
	            flag = this.getSqlSession().update("com.jd.oa.form.model.FormBusinessTable.deleteFormBusinessTable", map) > 0 ? true : false;
	        } catch (DataAccessException e) {
	            flag = false;
	            throw e;
	        }
	        return flag;
	}
	/**
	  * @Description: 判断是否存在数据表名
	  * @param tableChineseName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:00:24 
	  * @version V1.0
	 */
	public boolean isExistTableChineseName(FormBusinessTable formBusinessTable){
		List<FormBusinessTable> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.form.model.FormBusinessTable.isExistTableChineseName",formBusinessTable);
            if(result != null && result.size()>0){
            	return true;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return false;
	}
	/**
	  * @Description: 判断是否存在数据库表名
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:33:41 
	  * @version V1.0
	 */
	public boolean isExistTableName(FormBusinessTable formBusinessTable){
		List<FormBusinessTable> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.form.model.FormBusinessTable.isExistTableName",formBusinessTable);
            if(result != null && result.size()>0){
            	return true;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return false;
	}
	/**
	 * @Description:根据表ID列表查询表名称列表
	  * @param map
	  * @return List<String>
	  * @author guoqingfu
	  * @date 2013-10-23上午10:53:43 
	  * @version V1.0
	 */
	public List<String> findTableNameList(Map<String,Object> map){
		List<String> resultList = null;
		try{
			resultList = this.getSqlSession().selectList("com.jd.oa.form.model.FormBusinessTable.findTableNameList",map);
		}catch(DataAccessException e){
			throw e;
		}
		return resultList;
	}
}

package com.jd.oa.form.execute.component.impl;

import java.text.ParseException;
import java.util.Hashtable;
import java.util.Map;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.util.Html;

public class FormUIComponentDateTimeImpl extends FormUIComponentAbst {
	public FormUIComponentDateTimeImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		html.append("<input type=text readonly name='").append(getMetaDataMapModel().getFieldName()).append("'  id='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(Html.escape(value)).append("\">");
		return html.toString();
	}
	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
		String value =  getValue();
		String inputTypeConfig = "yyyy-MM-dd hh:mm:ss";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() >0){
			inputTypeConfig =getMetaDataMapModel().getInputTypeConfig();
		}
		try{
			value = DateUtils.dateTimeFormat(value, inputTypeConfig);
			fieldHtml.append("<input id=\"").append(getMetaDataMapModel().getFieldName()).append("\"></input>");
			if(!isNull()){
				fieldHtml.append("<font color=red>*</font>");
			}
		    fieldHtml.append(" <script type=\"text/javascript\">\n");
		    fieldHtml.append(" var formatDate = '';\n");
			fieldHtml.append("$(function(){\n");
			fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datetimebox({\n");
			fieldHtml.append("formatter : formatD_").append(getMetaDataMapModel().getFieldName()).append(",\n");
			fieldHtml.append("onSelect : function(date){formatDate = ''}\n");
			fieldHtml.append("});\n");
			//设置值
			fieldHtml.append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datetimebox('setValue','").append(value).append("');\n");
			fieldHtml.append("function formatD_").append(getMetaDataMapModel().getFieldName()).append("(date){\n");
			fieldHtml.append("  if(formatDate != '')return formatDate;\n");
			fieldHtml.append("	var date = new Date(date);\n");
			fieldHtml.append("	formatDate = date.formatDate('").append(inputTypeConfig).append("');\n");
			fieldHtml.append("	return formatDate;\n");
			fieldHtml.append("}\n");
			fieldHtml.append("})\n");
			fieldHtml.append("</script> \n");
		}catch(ParseException e){
			fieldHtml.append("<font color='red'>无法解析该[").append(value).append("]值</font>");
		}
		
		return fieldHtml.toString();
	}
	@Override
    public String getValidateJavaScript(Map<String,Object> params) {
    	FormBusinessField metaDataMapModel = getMetaDataMapModel();
		StringBuffer jsCheckvalue = new StringBuffer();
		jsCheckvalue.append(" var ").append(getMetaDataMapModel().getFieldName()).append(" = ").append("$('#").append(getMetaDataMapModel().getFieldName()).append("').datetimebox('getValue');\n");
		// 非空校验
		if (!isNull()) {
			jsCheckvalue.append("\n try{\n")
						.append("     if(").append(getMetaDataMapModel().getFieldName()).append(".length == 0)").append("{\n")
						.append(" 	    alert ('请给字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入一个值，该字段值不允许为空！');\n")
						.append("	    try{ \n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("	   }catch(e){}\n")
						.append("	   return false; \n")
						.append("    }\n ")
						.append("  }catch(e){}\n\n ");
		}
		// 基本类型校验
		if (metaDataMapModel.getInputType().equals("日期时间")) {
				jsCheckvalue.append("if(!IsDateTime(").append(getMetaDataMapModel().getFieldName()).append("))").append("{ \n")
							.append("\n try{\n")
							.append("           alert ('字段:[").append(metaDataMapModel.getFieldChineseName()).append("]输入的值是一个非法的日期时间格式！');\n")
							.append(getErrorTip(metaDataMapModel.getFieldName()))
							.append("   }catch(e){}\n\n ")
							.append("      		return false; \n")
							.append("        }\n");
		}
		return jsCheckvalue.toString();
	}
	public String getReadHtmlDefine(Hashtable params) {
		StringBuffer html = new StringBuffer();
		html.append("<input type=hidden name='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(getValue()).append("\">").append(getValue());
		return html.toString();
	}
    public String getValidateJavaScript(Hashtable params) {
        StringBuffer jsCheckvalue = new StringBuffer();
        return jsCheckvalue.toString();
    }
    public String getSettingWeb(){
    	String value = getMetaDataMapModel().getInputTypeConfig();
    	StringBuffer settingHtml = new StringBuffer();
    	settingHtml.append("<table>");
		settingHtml.append("<tr>");
		settingHtml.append("<td>格式</td>");
		settingHtml.append("<td>");
		settingHtml.append("<select id='selectData'>");
		settingHtml.append("<option value='yyyy-MM-dd HH:mm:ss' ").append("yyyy-MM-dd HH:mm:ss".equals(value) ? "selected" : "").append(">yyyy-MM-dd HH:mm:ss");
//		settingHtml.append("<option value='yyyy/MM/dd HH:mm:ss a' ").append("yyyy/MM/dd HH:mm:ss a".equals(value) ? "selected" : "").append(">yyyy/MM/dd HH:mm:ss a");
//		settingHtml.append("<option value='yyyy年MM月dd日 HH:mm:ss' ").append("yyyy年MM月dd日 HH:mm:ss".equals(value) ? "selected" : "").append(">yyyy年MM月dd日 HH:mm:ss");
//		settingHtml.append("<option value='yyyy年MM月dd日 HH时mm分ss秒' ").append("yyyy年MM月dd日 HH时mm分ss秒".equals(value) ? "selected" : "").append(">yyyy年MM月dd日 HH时mm分ss秒");
		settingHtml.append("</select>");
		settingHtml.append("<td>");
		settingHtml.append("</tr>");
		settingHtml.append("</table>");
		
		settingHtml.append("<script>");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append("	return getSelectValue('selectData')\n");
	    settingHtml.append("}\n"); 
	    settingHtml.append("</script>\n"); 
    	return settingHtml.toString();
    }
	@Override
	public String getTrueValue() {
		return getValue();
	}
	
}
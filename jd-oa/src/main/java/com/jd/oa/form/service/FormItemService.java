package com.jd.oa.form.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单项Service
 * User: 许林
 * Date: 13-9-12
 * Time: 上午11:56
 * To change this template use File | Settings | File Templates.
 */
public interface FormItemService extends BaseService<FormItem,String> {
	
	/**
	 * 根据formId获取表单详细项目信息
	 */
	public List<FormItem> selectByFormId(String formId);
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2014-3-20 下午04:10:07
	 *
	 * @param formId
	 * @param nodeId
	 * @return
	 */
	public List<FormItem> selectEditItemByFormIdAndNodeId(String formId,String nodeId);

    /**
     *  获取表单下所有的明细项信息
     * @param formId
     * @return
     */
	public List<FormItem> getFormItemByFormId(String formId);
	

    /**
     * 根据FormId和表类型获取明细项信息
     * @param formId
     * @param formType
     * @return
     */
	@SuppressWarnings("rawtypes")
	public List getFormItemByFormIdAndType(String formId,String formType);
	
	/**
	 * 
	 * @desc 根据FormId、processNodeId和表类型获取明细项信息
	 * @author WXJ
	 * @date 2014-3-20 上午11:21:58
	 *
	 * @param formId
	 * @param formType
	 * @param processNodeId
	 * @return
	 */
	public List getFormItemByFormIdAndType(String formId,String formType,String processNodeId);
	
	 /**
     * 根据FormId和表类型获取明细项信息
     * @param formId
     * @param formType
     * @return List<FormBean>
     */
	public List<FormBean> getFormAndItemsByFormId(String formId,String formType);

    /**
     * 批量删除表单项
     * @param ids
     */
    void mutidelete(String ids);
    public List<FormItem> selectItemByFormIdAndFieldName(String formId,String fieldName);
    
    public List<FormItem> findFromAndSubItems(List<String> formIds);
}

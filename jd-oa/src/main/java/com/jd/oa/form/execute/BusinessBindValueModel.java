package com.jd.oa.form.execute;

public class BusinessBindValueModel {
	private String fieldName;
	private String fieldType;
	private String fieldValue;
	private String fieldInputType;
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getFieldInputType() {
		return fieldInputType;
	}
	public void setFieldInputType(String fieldInputType) {
		this.fieldInputType = fieldInputType;
	}
	
}

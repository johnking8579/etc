package com.jd.oa.form.execute.component.impl;

import java.util.Hashtable;
import java.util.Map;

import net.sf.json.JSONObject;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.util.Html;
public class FormUIComponentTextAreaImpl extends FormUIComponentAbst {

	public FormUIComponentTextAreaImpl(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
	public String getReadHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		String inputLength = "30";
		String inputRow = "3";
		if(getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(getMetaDataMapModel().getInputTypeConfig());
			if(jsonObject != null){
				if(jsonObject.get("inputLength") != null){
					inputLength = jsonObject.get("inputLength").toString();
				}
				if(jsonObject.get("inputRow") != null){
					inputRow = jsonObject.get("inputRow").toString();
				}
			}
		}
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
//		String fieldName = getMetaDataMapModel().getFieldName();
		StringBuffer fieldHtml = new StringBuffer();
		fieldHtml.append("<textarea wrap='physical' id='").append(getMetaDataMapModel().getFieldName()).append("' name='").append(getMetaDataMapModel().getFieldName()).append("' readonly  rows='").append(inputRow).append("' cols='").append(inputLength).append("' onkeypress='if(event.keyCode==13||event.which==13){return false;}' >");
		fieldHtml.append(Html.escape(value));
		fieldHtml.append("</textarea>");
		return fieldHtml.toString();
	}
	public String getModifyHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		String inputLength = "30";
		String inputRow = "3";
		if(getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(getMetaDataMapModel().getInputTypeConfig());
			if(jsonObject != null){
				if(jsonObject.get("inputLength") != null){
					inputLength = jsonObject.get("inputLength").toString();
				}
				if(jsonObject.get("inputRow") != null){
					inputRow = jsonObject.get("inputRow").toString();
				}
			}
		}
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
//		String fieldName = getMetaDataMapModel().getFieldName();
		StringBuffer fieldHtml = new StringBuffer();
		fieldHtml.append("<textarea wrap='physical' ").append(super.getHtmlInner()).append(" id='").append(getMetaDataMapModel().getFieldName()).append("' name='").append(getMetaDataMapModel().getFieldName()).append("' rows='").append(inputRow).append("' cols='").append(inputLength).append("' >");
		fieldHtml.append(Html.escape(value));
		fieldHtml.append("</textarea>");
		if(!isNull()){
			fieldHtml.append("<font color=red>*</font>");
		}
		return fieldHtml.toString();
	}
	public String getSettingWeb() {
		String value = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			value = getMetaDataMapModel().getInputTypeConfig();
		}
		String inputLength = "";
		String inputRow = "";
		if(value.trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(value);
			if(jsonObject != null){
				if(jsonObject.get("inputLength") != null){
					inputLength = jsonObject.get("inputLength").toString();
				}
				if(jsonObject.get("inputRow") != null){
					inputRow = jsonObject.get("inputRow").toString();
				}
			}
		}
		StringBuffer settingHtml = new StringBuffer();
		settingHtml.append("<table  width=\"100%\" align=\"center\" cellpadding=\"3\">");
		settingHtml.append("<tr>");
		settingHtml.append("<td align='right' width='15%' nowrap>录入长度 : </td>");
		settingHtml.append("<td align='left'><input type='text' id='inputLength' name='inputLength' style='width:100px' value='").append(inputLength).append("'></td>");
		settingHtml.append("</tr>");
		settingHtml.append("<tr>");
		settingHtml.append("<td align='right'>行&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;高 : </td>");
		settingHtml.append("<td align='left'><input type='text' id='inputRow' name='inputRow' style='width:100px' value='").append(inputRow).append("'></td>");
		settingHtml.append("</tr>");
		settingHtml.append("</table>");
		
		settingHtml.append("<script type='text/javascript'>\n");
		settingHtml.append(" document.getElementById('inputLength').focus();\n");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append(" if(!checkNum('inputRow')){parent.Dialog.alert('提示','基本配置【录入长度】输入值不合法!');return;}\n");
		settingHtml.append(" if(!checkNum('inputLength')){parent.Dialog.alert('提示','基本配置【行       高】输入值不合法!');return;}\n");
		settingHtml.append("var jsonData = {\n");
		settingHtml.append("		inputRow : document.getElementById('inputRow').value,\n");
		settingHtml.append("		inputLength : document.getElementById('inputLength').value\n");
		settingHtml.append("	};\n");
		settingHtml.append("return JSON.stringify(jsonData);\n");
		settingHtml.append("}\n");
		settingHtml.append("</script>\n");
		return settingHtml.toString();
	}
	@Override
	public String getTrueValue() {
		return getValue();
	}


}
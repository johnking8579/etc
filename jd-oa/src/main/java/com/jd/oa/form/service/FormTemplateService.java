package com.jd.oa.form.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormTemplate;

public interface FormTemplateService extends BaseService<FormTemplate, String>{
	
	public static final String FORMTEMPLATE_TYPE_FORM = "0";
	
	public static final String FORMTEMPLATE_TYPE_PRINT = "1";
	
	public FormTemplate getByFormId(String formId);
	
	public String save(FormTemplate formTemplate,String templateValue);
	
	public int update(FormTemplate formTemplate,String templateValue);
	
	public List<FormTemplate> find(String formId,String formTemplateType);
	
	public List<FormTemplate> find(String formId);
	
	public String getTemplateValue(String templatePath) throws IOException;
	
	public Map<String,String> getRuntimeForm(HttpServletRequest request,String formId);
	
	public void saveJavascript(String formId,String javascriptContext);
	
	public String getJavascriptValue(String formId);
	
	
}
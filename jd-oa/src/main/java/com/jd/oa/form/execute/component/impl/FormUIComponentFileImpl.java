package com.jd.oa.form.execute.component.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.service.FormAttachmentService;
/**
 * 附件上传
 * @author birkhoff
 *
 */
public class FormUIComponentFileImpl extends FormUIComponentAbst{

	public FormUIComponentFileImpl(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		if(formAttachmentService == null){
			formAttachmentService = SpringContextUtils.getBean(FormAttachmentService.class);
		}
		StringBuffer html = new StringBuffer();
		html.append("<input type=hidden id='").append(getMetaDataMapModel().getFieldName()).append("' name='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(getValue()).append("\">");
		html.append("<table width='100%'><tr valign='top'><td width=100%>");
		String fileAttachMentValue = getValue();
		if(fileAttachMentValue != null && fileAttachMentValue.trim().length() > 0){
			html.append("<table width='100%' id='").append(getMetaDataMapModel().getFieldName()).append("_flexUpload'>");
			StringUtils util = new StringUtils(fileAttachMentValue);
			List<String> utils = util.split("@@@@");
			for(int i = 0 ; i < utils.size() ; i++){
				String value = utils.get(i);
				FormAttachment formAttachment = formAttachmentService.get(value);
				if(formAttachment != null){
					String fileIcon = FileUtil.getFileIcon(formAttachment.getAttachmentName());
					html.append("<tr valign='top'><td width=100%>");
					html.append("<a href=\"javascript:void(0);\" onClick=\"downloadfile('").append(value).append("');return false;\"><img src='/static/fileupload/image/file/")
						.append(fileIcon).append("' border=0 />").append(formAttachment.getAttachmentName());
					html.append("</a>");
					html.append("</td>");
					html.append("</tr>");
				}
			}
			html.append("</table>");
		} 
		html.append("</tr>");
		html.append("</table>");
		return html.toString();
	}
	private FormAttachmentService formAttachmentService;
	/**
	 * 获取编辑状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		if(formAttachmentService == null){
			formAttachmentService = SpringContextUtils.getBean(FormAttachmentService.class);
		}
		StringBuffer html = new StringBuffer();
		html.append("<input type=hidden id='").append(getMetaDataMapModel().getFieldName()).append("' name='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(getValue()).append("\">");
		html.append("<table width='95%' height='100%' border='0'>");
		html.append("<tr valign='top'>");
		html.append("<td style='width:100%;' align='left' nowrap>");
		html.append("<button title=\"上传附件\"  onClick=\"upfile('").append(getMetaDataMapModel().getFieldName()).append("');return false;\">上传</button>");
		if(!isNull()){
			html.append("<font color=red>*</font>");
		}
		html.append("</td>");
		html.append("</tr>");
		html.append("<tr valign='top'><td width=100%>");
		String fileAttachMentValue = getValue();
		if(fileAttachMentValue != null && fileAttachMentValue.trim().length() > 0){
			html.append("<div style=\" HEIGHT: 140px; OVERFLOW: auto;overflow-x:hidden; scrollbar-face-color: #9EBFE8; scrollbar-shadow-color: #FFFFFF; scrollbar-highlight-color: #FFFFFF; scrollbar-3dlight-color: #9EBFE8; scrollbar-darkshadow-color: #9EBFE8; scrollbar-track-color: #FFFFFF; scrollbar-arrow-color: #FFFFFF\">"+
					"<table width='100%' id='").append(getMetaDataMapModel().getFieldName()).append("_flexUpload'>");
			StringUtils util = new StringUtils(fileAttachMentValue);
			List<String> utils = util.split("@@@@");
			for(int i = 0 ; i < utils.size() ; i++){
				String value = utils.get(i);
				FormAttachment formAttachment = formAttachmentService.get(value);
				if(formAttachment != null){
					String fileIcon = FileUtil.getFileIcon(formAttachment.getAttachmentName());
					html.append("<tr valign='top' id='").append(value).append("'>");
					//文件内容
					html.append("<td >");
					html.append("<a href=\"javascript:void(0);\" onClick=\"downloadfile('").append(value).append("');return false;\"><img src='/static/fileupload/image/file/")
						 .append(fileIcon).append("' border=0 />").append(formAttachment.getAttachmentName());
					html.append("</a>");
					html.append("</td>");
					//end 文件内容
					//文件删除
					html.append("<td width=30 nowrap>");
					html.append("<a href=\"javascript:void(0);\" onClick=\"deletefile('").append(value).append("','").append(formAttachment.getAttachmentName()).append("','").append(getMetaDataMapModel().getFieldName()).append("');return false;\">删除");
					html.append("</a>");
					html.append("</td>");
					html.append("</tr>");
				}
			}
			html.append("</table></div>");
		} else {
			html.append("<div style=\"HEIGHT: 120px;OVERFLOW: auto;overflow-x:hidden;scrollbar-face-color: #9EBFE8; scrollbar-shadow-color: #FFFFFF; scrollbar-highlight-color: #FFFFFF; scrollbar-3dlight-color: #9EBFE8; scrollbar-darkshadow-color: #9EBFE8; scrollbar-track-color: #FFFFFF; scrollbar-arrow-color: #FFFFFF\">"+
					"<table width='100%' id='").append(getMetaDataMapModel().getFieldName()).append("_flexUpload'>");
			html.append("</table></div>");
		}
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		return html.toString();
	}
	@Override
	public String getValidateJavaScript(Map<String,Object> params) {
		FormBusinessField metaDataMapModel = getMetaDataMapModel();
		StringBuffer jsCheckvalue = new StringBuffer();
		jsCheckvalue.append(" var ").append(getMetaDataMapModel().getFieldName()).append(" = ").append("$('#").append(getMetaDataMapModel().getFieldName()).append("');");
		// 非空校验
		if (!isNull()) {
			jsCheckvalue.append("\n try{\n")
						.append("     if(").append(getMetaDataMapModel().getFieldName()).append(".val().length == 0)").append("{\n")
						.append(" 	    alert('请给字段:[").append(metaDataMapModel.getFieldChineseName()).append("]上传一个附件，该字段值不允许为空！');\n")
						.append("	    try{ \n")
						.append(getErrorTip(metaDataMapModel.getFieldName()))
						.append("	   }catch(e){}\n")
						.append("	   return false; \n")
						.append("    }\n ")
						.append("  }catch(e){}\n\n ");
		}
		return jsCheckvalue.toString();
	}
	@Override
	public String getSettingWeb() {
		String value = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			value = getMetaDataMapModel().getInputTypeConfig();
		}
		String upNum = "";
		String upSize = "";
		String upExt = "";
		if(value.trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(value);
//			if(jsonObject != null){
//				if(jsonObject.get("upNum") != null){
//					upNum = jsonObject.get("upNum").toString();
//				}
//				if(jsonObject.get("upSize") != null){
//					upSize = jsonObject.get("upSize").toString();
//				}
//				if(jsonObject.get("upExt") != null){
//					upExt = jsonObject.get("upExt").toString();
//				}
//			}
		}
		StringBuffer settingHtml=new StringBuffer();
//		settingHtml.append("<tr>");
//		settingHtml.append("<td>");
//		settingHtml.append("<table  width=\"100%\" align=\"center\" cellpadding=\"3\">");
		
//		settingHtml.append("<tr>");
//		settingHtml.append("<td nowrap>最大上传个数</td>");
//		settingHtml.append("<td nowrap colspan=3><input type=\"text\" class ='actionsoftInput' name = \"upNum\" id = \"upNum\" value ='").append(upNum).append("' size=4 /></td>");
//		settingHtml.append("</tr>");
//		settingHtml.append("<tr>");
//		settingHtml.append("<td nowrap>单个文件大小</td>");
//		settingHtml.append("<td nowrap colspan=3><input type=\"text\" class ='actionsoftInput' name = \"upSize\"  id = \"upSize\" value = '").append(upNum).append("' size=4/>默认"+ 5 +"M(单位M)</td>");
//		settingHtml.append("</tr>");
//		
//		
//		settingHtml.append("<tr>");
//		settingHtml.append("<td width=\"18%\" nowrap>特定文件名后缀</td>");
//		settingHtml.append("<td colspan=\"3\" nowrap><input type='text' id='upExt' name='upExt' value='").append(upExt).append("'></td>");
//		settingHtml.append("</tr>");
//		
//		
		settingHtml.append("<script>\n");
		//保存property function
		settingHtml.append("function returnSettingConfig(){\n");
//		settingHtml.append(" if(!checkNum('upNum')){parent.Dialog.alert('提示','基本配置【最大上传个数】输入值不合法!');return;}\n");
//		settingHtml.append(" if(!checkNum('upSize')){parent.Dialog.alert('提示','基本配置【单个文件大小】输入值不合法!');return;}\n");
//		settingHtml.append("var jsonData = {\n");
//		settingHtml.append("		upNum : document.getElementById('upNum').value,\n");
//		settingHtml.append("		upSize : document.getElementById('upSize').value,\n");
//		settingHtml.append("		upExt : document.getElementById('upExt').value\n");
//		settingHtml.append("	};\n");
//		settingHtml.append("return JSON.stringify(jsonData);\n");
		settingHtml.append("return '';\n");
		settingHtml.append("}\n");
		settingHtml.append("</script>\n");
		return settingHtml.toString();
	}
	@Override
	public String getTrueValue() {
		if(formAttachmentService == null){
			formAttachmentService = SpringContextUtils.getBean(FormAttachmentService.class);
		}
		StringBuffer html = new StringBuffer();
		if(getValue() != null && getValue().trim().length() > 0){
			FormAttachment formAttachment = formAttachmentService.get(getValue());
			if(formAttachment != null){
				String fileIcon = FileUtil.getFileIcon(formAttachment.getAttachmentName());
				html.append("<a href=\"javascript:void(0);\" onClick=\"downloadfile('").append(getValue()).append("');return false;\"><img src='/static/fileupload/image/file/").append(fileIcon).append("' border=0 />").append(formAttachment.getAttachmentName());
				html.append("</a>");
			}
		}
		return html.toString();
	}
}

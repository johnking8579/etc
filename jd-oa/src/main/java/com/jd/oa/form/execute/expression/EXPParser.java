package com.jd.oa.form.execute.expression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.event.ReferenceInsertionEventHandler.referenceInsertExecutor;
import org.aspectj.weaver.patterns.ThisOrTargetAnnotationPointcut;

import com.jd.oa.form.execute.RuntimeFormManager;

public class EXPParser {
	public static final String ERROR = "[Formula Error]";

	private static List SYNTAXLIST = new ArrayList();
	private static String contextPath;
	public static EXPParser _instance;
	
	public static EXPParser getInstance(String contextPath){
		if(_instance == null){
			_instance = new EXPParser(contextPath);
		}
		return _instance;
	}
	private EXPParser(String contextPath){
		this.contextPath = contextPath;
		initRulePrefix();
		Collections.sort(SYNTAXLIST, new Comparator() {

			public int compare(Object o1, Object o2) {
				return ((String) o2).length() - ((String) o1).length();
			}
		});
	}

	public static String executeRule(String exp, RuntimeFormManager r) {
		StringBuilder sb = new StringBuilder();
		int s = parseRuleStart(exp, -1);
		int ae = 0;
		while (s != -1) {
			int ruleStart = exp.lastIndexOf('@', s);
			int ruleEnd = parseRuleEnd(exp, s);
			if (ruleEnd == -1) {
				break;
			}

			String formula = exp.substring(ruleStart, ruleEnd);
			sb.append(exp.substring(ae, ruleStart) + getFormulaValue(formula, r));
			ae = ruleEnd;
			s = parseRuleStart(exp, ruleEnd);
		}

		sb.append(exp.substring(ae));
		return sb.toString();
	}

	private static String getFormulaValue(String formula, RuntimeFormManager r) {
		String oldFormula = formula;
		Map<String,Object> paramMaps = new HashMap<String,Object>();
//		paramMaps.put("UserContext", r.get_me());
//		paramMaps.put("headMessageModel", r.get_headMessageModel());
//		paramMaps.put("activityInstanceId", new Integer(r.get_activityInstanceId()));
//		paramMaps.put("workFlowStepModel", r.get_workFlowStepModel());
//		paramMaps.put("workFlowModel", r.get_workFlowModel());
//		paramMaps.put("RuntimeFormManager", r);
//		paramMaps.put("processInstanceId", new Integer(r.get_processInstanceId()));
//		paramMaps.put("businessObjectId", new Integer(r.get_businessObjectId()));
//		paramMaps.put("subSheetId", new Integer(r.get_subSheetId()));
//		if(r.getRuleParam()!=null){
//			paramMaps.putAll(r.getRuleParam());
//		}
		ExpressionInterface expression = ExpressionFactory.getInstance(r._contextPath).getExpressionInstance(paramMaps, formula.toLowerCase());
		//String contextPath = "";
		try {
			// 获得公式的解析
			String result = expression.expressionParse(formula);
			if (formula.equals(result)) {// 没有公式被执行
				return oldFormula;
			} else {
				return result;
			}
		} catch (Exception e) {
			return ERROR;
		}
	}

	// @sequence:<#key>类型@命令识别
	private static boolean isSequence(String exp, int s) {
		return exp.startsWith("<#", s + 1) && exp.length() > 8 && exp.substring(s - 8, s).equalsIgnoreCase("sequence");
	}

	private static int parseRuleEnd(String exp, int s) {
		if (exp.length() == s + 1) {
			return s + 1;
		}

		// 不带参数并且不是sequence公式
		boolean sequence = false;
		if (exp.charAt(s + 1) != '(' && !(sequence = isSequence(exp, s))) {
			return s + 1;
		}

		if (sequence) {
			int i = exp.indexOf('>', s);
			return i == -1 ? -1 : i + 1;
		}

		int left = 1;
		for (int i = s + 2; i < exp.length(); i++) {
			if (exp.charAt(i) == ')') {
				if (--left == 0) {
					return i + 1;
				}
			}

			if (exp.charAt(i) == '(') {
				left++;
			}
		}

		return -1;
	}

	// 找到@命令语法的尾部，例如@sidAAA，找到@sid中的d位置
	private static int parseRuleStart(String exp, int start) {
		int s = exp.indexOf('@', start);
		if (s != -1) {
			Iterator it = SYNTAXLIST.iterator();
			while (it.hasNext()) {
				String pre = (String) it.next();
				if (exp.toLowerCase().startsWith(pre, s)) {
					return s + pre.length() - 1;
				}
			}

			return parseRuleStart(exp, s + 1);
		}

		return -1;
	}

	private void initRulePrefix() {
		Map<String,Object> h = ExpressionFactory.getInstance(contextPath).getExpressionSortList();
		if (h != null) {
			for (int i = 0; i < h.size(); i++) {
				ConfigExpressionModel cfg = (ConfigExpressionModel) h.get(Integer.valueOf(i));
				int param = cfg.getSyntax().indexOf('(');
				SYNTAXLIST.add((param != -1 ? cfg.getSyntax().substring(0, param) : cfg.getSyntax()).toLowerCase());
			}
		}
	}

	public static void main(String[] args) {
		String s = "123@bsh((1+2)/3-@sid)ABC@bsh((1+2)/3-@sidadasd@e(.";
//		System.out.println(executeRule(s, null));
	}
}

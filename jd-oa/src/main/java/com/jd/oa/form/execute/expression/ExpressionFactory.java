package com.jd.oa.form.execute.expression;

import java.lang.reflect.Constructor;
import java.util.*;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.ReflectionUtils;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.expression.impl.NoneExpressionImpl;

/**
 * @变量解析工厂类
 * @author liub
 *
 */
public class ExpressionFactory {
	private static HashMap _expressionList = null;

	private static HashMap _expressionGroup = null;

	private static HashMap _expressionSortList = null;
	
	private static String contextPath;
	private static ExpressionFactory _instance;
	public static ExpressionFactory getInstance(String contextPath){
		if(_instance == null){
			_instance = new ExpressionFactory(contextPath);
		}
		return _instance;
	}
	private ExpressionFactory(String contextPath){
		this.contextPath = contextPath;
		loadExpression();
	} 

	public ExpressionInterface getExpressionInstance(Map<String,Object> paramMaps, String value) {
		String tempvalue = value.substring(value.indexOf("@") + 1, value.length());
		ConfigExpressionModel configExpressionModel = (ConfigExpressionModel) _expressionList.get(tempvalue);
		if (configExpressionModel != null) {
			Constructor cons = null;
			ExpressionInterface ui;
			try {
				cons = configExpressionModel.getCons();
				if (cons != null) {
					Object[] params = { paramMaps, value };
					ui = (ExpressionInterface) cons.newInstance(params);
					return ui;
				}
			} catch (Exception e) {
				throw new BusinessException("表达式解析失败：" + e.getMessage());
			}
		} else {
			String tempvalue2 = "";
			if (value.indexOf("(") != -1) {
				tempvalue2 = value.substring(value.indexOf("@") + 1, value.indexOf("(", value.indexOf("@")));
			} else if (value.indexOf("<") != -1) {
				tempvalue2 = value.substring(value.indexOf("@") + 1, value.indexOf("<"));
			}
			configExpressionModel = (ConfigExpressionModel) _expressionList.get(tempvalue2);
			if (configExpressionModel != null) {
				Constructor cons = null;
				ExpressionInterface ui;
				try {
					cons = configExpressionModel.getCons();
					if (cons != null) {
						Object[] params = { paramMaps, value };
						ui = (ExpressionInterface) cons.newInstance(params);
						return ui;
					}
				} catch (Exception e) {
					throw new BusinessException("表达式解析失败：" + e.getMessage());
				}
			} else {
				return new NoneExpressionImpl(paramMaps, value);
			}
		}
		return new NoneExpressionImpl(paramMaps, value);
	}

	/**
	 * 重新加载rule库
	 * 
	 */
	private static void reloadComponent() {
		loadExpression();
	}

	/**
	 * 
	 * @return
	 */
	private static HashMap getExpressionList() {
		// loadExpression();
		return _expressionList;
	}

	/**
	 * @return
	 */
	public static HashMap getExpressionSortList() {
		return _expressionSortList;
	}

	/**
	 * 获取组件分类列表
	 * 
	 * @preserve 声明此方法不被JOC混淆.
	 * @return
	 */
	private static HashMap getExpressionGroupList() {
		return _expressionGroup;
	}

	private static void loadExpression() {
		_expressionList = new HashMap();
		_expressionGroup = new HashMap();
		_expressionSortList = new HashMap();
		Class[] parameterTypes = { Map.class, String.class };
		String xml = "eform-expression.xml";
		SAXReader saxreader = new SAXReader();
		Document doc = DocumentFactory.getInstance().createDocument();
		try {
			doc =saxreader.read(contextPath + "WEB-INF" + System.getProperties().getProperty("file.separator") + "classes/" + xml);
			Iterator it = doc.getRootElement().elementIterator();
//			Hashtable hs = new Hashtable();
			it = doc.getRootElement().elementIterator("formula");
			int i = 0;
			while (it.hasNext()) {
				Element element = (Element) it.next();
				if (element.getName().equals("formula")) {
					Iterator iit = element.elementIterator();
					ConfigExpressionModel configExpressionModel = new ConfigExpressionModel();
					while (iit.hasNext()) {
						Element ielement = (Element) iit.next();
						if (ielement.getName().equals("id"))
							configExpressionModel.setId(ielement.getText());
						if (ielement.getName().equals("title"))
							configExpressionModel.setTitle(ielement.getText());
						if (ielement.getName().equals("interface-class"))
							configExpressionModel.setInterfaceClass(ielement.getText());
						if (ielement.getName().equals("implements-class"))
							configExpressionModel.setImplementsClass(ielement.getText());
						if (ielement.getName().equals("desc"))
							configExpressionModel.setDesc(ielement.getText());
						if (ielement.getName().equals("groupname"))
							configExpressionModel.setGroupName(ielement.getText());
						if (ielement.getName().equals("syntax"))
							configExpressionModel.setSyntax(ielement.getText());
					}
					// 建立反射
					Constructor cons = null;
					try {
						cons = ReflectionUtils.getConstructor(configExpressionModel.getImplementsClass(), parameterTypes);
					} catch (ClassNotFoundException ce) {
						System.err.println("ClassLoader>>未定义的规则配置！[" + configExpressionModel.getImplementsClass() + "]没有找到!");
					} catch (NoSuchMethodException ne) {
						System.err.println("ClassLoader>>未定义的规则配置！[" + configExpressionModel.getImplementsClass() + "]构造方法不匹配!");
					} catch (Exception e) {
						System.err.println("ClassLoader>>未定义的规则配置！[" + configExpressionModel.getImplementsClass() + "]");
					}
					// 设置构造组件的构造函数对象
					configExpressionModel.setCons(cons);
					_expressionList.put(configExpressionModel.getId().toLowerCase(), configExpressionModel);
					_expressionSortList.put(Integer.valueOf(_expressionSortList.size()), configExpressionModel);
					// 是否属于一个新组
					if (!_expressionGroup.containsValue(configExpressionModel.getGroupName())) {
						_expressionGroup.put(Integer.valueOf(_expressionGroup.size()), configExpressionModel.getGroupName());
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException("表达式实现类加载失败：" + e.getMessage());
		}
	}
}	

package com.jd.oa.form.execute.component;

import java.util.Map;

import com.jd.oa.form.model.FormBusinessField;

public interface UIComponentInterface {
	/**
	 * 获取只读状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getReadHtmlDefine(Map<String,Object> params);

	/**
	 * 获取编辑状态下的组件HTML代码,该方法应该被重载
	 * 
	 * @param params
	 *            额外的参数列表，可以为NULL
	 * @return
	 */
	public abstract String getModifyHtmlDefine(Map<String,Object> params);
	/**
	 * 获取隐藏状态下的组件HTML代码,该方法应该被重载
	 * @param params 额外的参数列表，可以为NULL
	 * @return
	 */
	
	public abstract String getHiddenHtmlDefine(Map<String,Object> params);

	/**
	 * 获取当前组件的客户端JavaScript校验代码，包括字段超长、是否允许为空、常规值校验
	 * 
	 * @param params
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getValidateJavaScript(Map<String,Object> params);

	/**
	 * 获取当前组件值的客户端JavaScript代码
	 * 
	 * @param params
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getValueJavaScript(Map<String,Object> params);
	

	/**
	 * 获取当前组件值的扩展JavaScript代码
	 * 
	 * @param params
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getJavaScriptExtendEvents();

	/**
	 * 获取当前字段模型的描述
	 * 
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract FormBusinessField getMetaDataMapModel();

	/**
	 * 获取当前字段模型的编辑界面
	 * 
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getSettingWeb();

	/**
	 * set MetaDataMapModel
	 * 
	 * @param metaDataMapModel
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract void setMetaDataMapModel(FormBusinessField metaDataMapModel);

	/**
	 * 获取当前form id
	 * @return
	 */
	public abstract String getFormId();
	/**
	 * 设置当前form id
	 * @param formId
	 */
	public abstract void setFormId(String formId);
	/**
	 * 获取当前组件的值
	 * 
	 * @return
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract String getValue();

	/**
	 * 设置当前组件的值
	 * 
	 * @param value
	 * @preserve 声明此方法不被JOC混淆.
	 */
	public abstract void setValue(String value);
	/**
	 * 允许编辑
	 * @return
	 */
	public abstract boolean isEdit();
	/**
	 * 隐藏
	 * @return
	 */
	public abstract boolean isHidden();
	/**
	 * 允许为空
	 * @return
	 */
	public abstract boolean isNull();
	
	public abstract String getTrueValue();

	public abstract String getQueryHtmlDefine(Map<String, Object> params);
}
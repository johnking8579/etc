package com.jd.oa.form.execute.component.impl;

import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

import java.util.Map;

public class FormUIComponentAchorImpl extends FormUIComponentAbst {

	public FormUIComponentAchorImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}

	@Override
	public String getTrueValue() {
		return getValue();
	}

	@Override
	public String getReadHtmlDefine(Map<String, Object> params) {
		StringBuilder readerBuilder = new StringBuilder();
		String value = getMetaDataMapModel().getInputTypeConfig();
		readerBuilder.append("<a href='"+value+"' target='_blank'>"+value+"</a>");
		return readerBuilder.toString();
	}

	@Override
	public String getModifyHtmlDefine(Map<String, Object> params) {
		return getReadHtmlDefine(params);
	}

	@Override
	public String getSettingWeb() {
		String value = getMetaDataMapModel().getInputTypeConfig();
		StringBuilder settingBuilder = new StringBuilder();
		settingBuilder.append("URL地址:<br/>");
		settingBuilder.append("<input id='urlField' type='text' value='"+value+"' />");
		settingBuilder.append("<br/><script>");
		settingBuilder.append("function returnSettingConfig(){\n" +
				"\tvar url = $(\"input#urlField\").val();\n" +
				"\tif(url == null || url == ''){\n" +
				"\t\talert(\"未填写URL\");\n" +
				"\t\treturn;\n" +
				"\t}\n" +
				"\tif(url.indexOf(\"http://\")!=0 && url.indexOf(\"https://\")!=0){\n" +
				"\t\talert(\"URL请以http://或者https://开头\");\n" +
				"\t\treturn;\n" +
				"\t}\n" +
				"\treturn url;\n" +
				"}");
		settingBuilder.append("</script><br/>");
		return settingBuilder.toString();
	}
}
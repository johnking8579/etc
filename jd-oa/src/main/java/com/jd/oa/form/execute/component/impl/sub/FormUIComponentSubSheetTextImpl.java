package com.jd.oa.form.execute.component.impl.sub;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.model.FormBusinessField;

public class FormUIComponentSubSheetTextImpl extends FormUISubSheetComponentAbst{

	public FormUIComponentSubSheetTextImpl(FormBusinessField metaDataMapModel,String value) {
		super(metaDataMapModel, value);
		// TODO Auto-generated constructor stub
	}
	public String getEditor(){
		String fieldLength = "";
//		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		String configValue = "";
		String inputLength = "";
		String dataSourceId = "";
		String dataSourceName = "";
		String quickSearch = "";
		String inputTypeConfig = getMetaDataMapModel().getInputTypeConfig();
		if(inputTypeConfig != null && inputTypeConfig.trim().length() > 0){
			try{
				JSONObject jsonObject = JSONObject.fromObject(inputTypeConfig);
				if(jsonObject != null){
//					if(jsonObject.get("configValue") != null){
//						configValue = jsonObject.get("configValue").toString();
//					}
//					if(jsonObject.get("inputLength") != null){
//						inputLength = jsonObject.get("inputLength").toString();
//					}
					if(jsonObject.get("dataSourceId") != null){
						dataSourceId = jsonObject.get("dataSourceId").toString();
					}
					if(jsonObject.get("dataSourceName") != null){
						dataSourceName = jsonObject.get("dataSourceName").toString();
					}
//					if(jsonObject.get("quickSearch") != null){
//						quickSearch = jsonObject.get("quickSearch").toString();
//					}
				}
			}catch(JSONException e){
				
			}
			if(dataSourceId == null || dataSourceId.equals("")){
				return "editor:{type:'text',options:{fieldName:'" + getFieldId() + "'}}";
			}
			return "editor:{type:'dictionary',options:{dataSourceId:'" + dataSourceId + "',dataSourceName:'" + dataSourceName + "',formItemId:'" + getMetaDataMapModel().getId() + "',formItemName:'" + getMetaDataMapModel().getFieldName() + "'}}";
		} else {
			return "editor:{type:'text',options:{fieldName:'" + getFieldId() + "'}}";
		}
	}
	
}

package com.jd.oa.form.execute.expression;

import java.util.Map;

import com.jd.oa.form.execute.RuntimeFormManager;

/**
 * 系统运行时刻@变量 抽象类
 * @author birkhoff
 *
 */
public class ExpressionAbst implements ExpressionInterface{
	
	private String _expressionValue = "";
	
	private Map<String,Object> _paramMaps;
	public ExpressionAbst(Map<String,Object> paramMaps,String expressionValue){
		_paramMaps = paramMaps;
		_expressionValue = expressionValue;
	}
	
	/**
	 * 解析@变量公式 
	 */
	@Override
	public String expressionParse(String expression) {
		// TODO Auto-generated method stub
		return _expressionValue;
	}
	/**
	 * 获取运行时刻的各种@变量参数 
	 * @return
	 */
	public Map<String,Object> getParamMaps(){
		return _paramMaps;
	}
	/**
	 * 解析函数参数,识别潜逃子函数
	 * @param str
	 * @param index 1开始
	 * @return
	 */
	public String getParameter(String str,int index){
		// 完整的参数串
		String strLine = str.substring(str.indexOf("(") + 1, str.lastIndexOf(")"));
		String param = "";
		for (int i = 1; i <= index; i++) {
			param = findParameter(strLine);
			// laster @ parameter
			if (strLine.length() == param.length()) {
				if (i < index){
					param = "";
				}
				break;
			}
			strLine = strLine.substring(param.length() + 1);
		}

		if (param.indexOf("@") > -1) {// 参数包含了子函数
			RuntimeFormManager r = (RuntimeFormManager) _paramMaps.get("RuntimeFormManager");
			if (r != null) {
				param = EXPParser.executeRule(param, r);
			}
		}

		param = param.trim();
		return param;
	}
	private String findParameter(String strLine){
		// 完整的参数串
		String param = "";
		StringBuilder paramSb = new StringBuilder("");
		int subFunction = 0;// 包含(
		for (int i = 0; i < strLine.length(); i++) {
			String c = strLine.substring(i, i + 1);
			if (c.equals("("))
				subFunction++;// 发现一个肯能含有,的嵌套函数
			if (c.equals(")"))
				subFunction--;// 该嵌套函数参数结束
			if (subFunction == 0 && c.equals(","))
				break;
//			param = param + c;
			paramSb.append(c);
		}
		param = paramSb.toString();
		return param;
	}
	
}

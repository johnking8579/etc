package com.jd.oa.form.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.constant.FormBusinessTableConstant;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.service.FormBusinessTableService;

/** 
 * @Description: 自定义表controller
 * @author guoqingfu
 * @date 2013-9-12 上午10:46:38 
 * @version V1.0 
 */
@Controller
@RequestMapping(value="/form")
public class FormBusinessTableController {

	 private static final Logger logger = Logger.getLogger(FormBusinessTableController.class);

	@Autowired
	private FormBusinessTableService formBusinessTableService;


    @Autowired
    private DictDataService dictDataService;


    /**
	  * @Description:进入到查询数据库表列表页面
	  * @return
	  * @throws Exception ModelAndView
	  * @author guoqingfu
	  * @date 2013-9-12下午01:15:35 
	  * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessTable_index", method = RequestMethod.GET)
	public ModelAndView index(String tableId, String tableTypeFlag) throws Exception {
        //数据表类型
        List<DictData> dictDataList = dictDataService.findDictDataList(FormBusinessTableConstant.TableTypeKey);

        ModelAndView mav = new ModelAndView("form/formBusinessTable_index");
        mav.addObject("dictDataList",dictDataList);
        mav.addObject("tableId",tableId);
        mav.addObject("tableType",tableTypeFlag);
		return mav;
	}
	/**
	  * @Description: 查询数据库表列表
	  * @param request
	  * @return
	  * @throws Exception Object
	  * @author guoqingfu
	  * @date 2013-9-12下午01:24:53 
	  * @version V1.0
	 */
	@RequestMapping(value = "/formBusinessTable_findPage", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public Object findPage(HttpServletRequest request) throws Exception{
    	//创建pageWrapper
		PageWrapper<FormBusinessTable> pageWrapper=new PageWrapper<FormBusinessTable>(request);
		try{
			//添加搜索条件
			String tableChineseName = request.getParameter("tableChineseName"); //数据表名
			String tableName = request.getParameter("tableName");    //数据库表明
			String tableType = request.getParameter("tableType");    //表类型
			String createType = request.getParameter("createType");  //创建类型
			String parentId = request.getParameter("parentId");  //创建类型

			if(StringUtils.isNotEmpty(tableChineseName)){
				pageWrapper.addSearch("tableChineseName",tableChineseName);
			}
			if(StringUtils.isNotEmpty(tableName)){
				pageWrapper.addSearch("tableName",tableName);
			}
			if(StringUtils.isNotEmpty(tableType)){
				pageWrapper.addSearch("tableType",tableType);
			}
			if(StringUtils.isNotEmpty(createType)){
				pageWrapper.addSearch("createType",createType);
			}
	        if(StringUtils.isNotEmpty(parentId)){
	            pageWrapper.addSearch("parentId",parentId);
	        }
			formBusinessTableService.find(pageWrapper.getPageBean(),"findByPage",pageWrapper.getConditionsMap());
			
		    //返回到页面的数据
			return pageWrapper.getResult();
//			String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
//	        logger.debug(json);
//	        return json;
		}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("查询数据库表列表失败",e);
        }
    }
	/**
	  * @Description: 进入数据表库新增页面
	  * @param model
	  * @return String
	  * @author guoqingfu
	  * @date 2013-9-12下午02:28:14 
	  * @version V1.0
	 */
	 @RequestMapping(value = "/formBusinessTable_add",method=RequestMethod.GET)
	    public String addFormBusinessTable(Model model) {
	    	//用于新增页面显示可以关联的主表
		 	List<FormBusinessTable> parentTableList = formBusinessTableService.findParentTableList();
		 	model.addAttribute("parentTableList", parentTableList);
		 	
	        return "form/formBusinessTable_add";
	    }
	 /**
	   * @Description: 保存新增
	   * @param formBusinessTable
	   * @return String
	   * @author guoqingfu
	   * @date 2013-9-12下午02:36:10 
	   * @version V1.0
	  */
	 @RequestMapping(value="/formBusinessTable_addSave" ,method = RequestMethod.POST, produces = "application/json")
	    @ResponseBody
	    public String saveAddFormBusinessTable(FormBusinessTable formBusinessTable){
		 	Map<String,Object> map = new HashMap<String,Object>();
		 	try{
			 	//获取当前操作人
			 	String userName = ComUtils.getLoginName();
			 	formBusinessTable.setCreator(userName);
			 	formBusinessTable.setCreateTime(new Date());
			 	formBusinessTable.setModifier(userName);
			 	formBusinessTable.setModifyTime(new Date());
			 	formBusinessTable.setYn(0);  //有效
			 	formBusinessTable.setCreateType("1");  //创建类型：0 自动；1 手动
			 	String primaryKey = formBusinessTableService.insert(formBusinessTable);
			 	if(StringUtils.isNotEmpty(primaryKey)){
			 		map.put("operator", true);
			        map.put("message", "添加成功");
	                map.put("primaryKey",primaryKey);
			 	}else{
			 		map.put("operator", false);
	                map.put("primaryKey",primaryKey);
			        map.put("message", "添加失败");
			 	}
			 	
		        return JSONObject.fromObject(map).toString();
		 	}catch(Exception e){
	        	logger.error(e);
				throw new BusinessException("保存新增数据库表失败",e);
	        }
	    }
	 /**
	   * @Description: 进入自定义表查看页面
	   * @param formBusinessTableId
	   * @param model
	   * @return String
	   * @author guoqingfu
	   * @date 2013-9-12下午02:43:12 
	   * @version V1.0
	  */
	 @RequestMapping(value = "/formBusinessTable_view",method=RequestMethod.GET)
	  public String viewFormBusinessTable(String formBusinessTableId, Model model) {
		// 根据ID查询数据表信息
		FormBusinessTable formBusinessTable = formBusinessTableService.getFormBusinessTableInfo(formBusinessTableId);
		String createType = formBusinessTable.getCreateType();
		if("1".equals(createType)){
			formBusinessTable.setCreateType("手动创建");
		}else if("0".equals(createType)){
			formBusinessTable.setCreateType("自动创建");
		}
		model.addAttribute("formBusinessTable", formBusinessTable);
		return "form/formBusinessTable_view" ;
	  }
	 /**
	   * @Description: 进入自定义表修改页面
	   * @param formBusinessTableId
	   * @param model
	   * @return String
	   * @author guoqingfu
	   * @date 2013-9-12下午03:24:54 
	   * @version V1.0
	  */
	 @RequestMapping(value = "/formBusinessTable_update",method=RequestMethod.GET)
	  public String updateFormBusinessTable(String formBusinessTableId, Model model) {
		// 根据ID查询数据表信息
		FormBusinessTable formBusinessTable = formBusinessTableService.getFormBusinessTableInfo(formBusinessTableId);
		model.addAttribute("formBusinessTable", formBusinessTable);
	    return "form/formBusinessTable_update";
	  }
	 
	/**
	  * @Description: 保存修改自定义表信息
	  * @param formBusinessTable
	  * @return String
	  * @author guoqingfu
	  * @date 2013-9-12下午03:30:31 
	  * @version V1.0
	 */
	 @RequestMapping(value="/formBusinessTable_updateSave" ,method = RequestMethod.POST, produces = "application/json")
	    @ResponseBody
	    public String saveUpdateFormBusinessTable(FormBusinessTable formBusinessTable){
	        Map<String,Object> map = new HashMap<String,Object>();
	        try{
	        	//获取当前操作人
		        String userName = ComUtils.getLoginName();
		        formBusinessTable.setModifier(userName);
		        formBusinessTable.setModifyTime(new Date());

		        int i = formBusinessTableService.update(formBusinessTable);
		        if(i>0){
		        	map.put("operator", true);
		 	        map.put("message", "修改成功");
		        }else{
		        	map.put("operator", false);
		 	        map.put("message", "修改失败");
		        }
		        return JSONObject.fromObject(map).toString();
	        }catch(Exception e){
	        	logger.error(e);
				throw new BusinessException("保存修改数据库表失败",e);
	        }
	    }
	 
	 /**
	   * @Description: 删除自定义表
	   * @param ids
	   * @param length
	   * @return String
	   * @author guoqingfu
	   * @date 2013-9-12下午04:29:52 
	   * @version V1.0
	  */
	 @RequestMapping(value="/formBusinessTable_delete",method = RequestMethod.POST, produces = "application/json")
	    @ResponseBody
	    public String deleteFormBusinessTable(String ids,Integer length){
		 	List<String> formBusinessTableIds = new ArrayList<String>();
		 	if(null == length || length.intValue() == 1){
		 		formBusinessTableIds.add(ids);
		 	}else{
		 		String[] batchIds = ids.split(",");
                CollectionUtils.addAll(formBusinessTableIds, batchIds);
		 	}
		 	Map<String,Object> map = formBusinessTableService.deleteFormBusinessTable(formBusinessTableIds);
		 	
		 	return JSONObject.fromObject(map).toString();
		 	 
	    }
	 
	 /**
	   * @Description: 判断数据表名是否存在（修改时）
	   * @param formBusinessTable
	   * @return String
	   * @author guoqingfu
	   * @date 2013-9-13上午10:49:35 
	   * @version V1.0
	  */
	 @RequestMapping(value="/formBusinessTable_isExistTableChineseName",method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public String isExistTableChineseName(FormBusinessTable formBusinessTable){
		 Map<String,Object> map = new HashMap<String,Object>();
	      boolean flag = formBusinessTableService.isExistTableChineseName(formBusinessTable);
	      if (flag) {
				map.put("operator", true);
				map.put("message", "存在数据表名");
	      }else{
	    	  map.put("operator", false);
			  map.put("message", "不存在数据表名");
	      }
	      return JSONObject.fromObject(map).toString();
	 }
	 
	 /**
	   * @Description: 验证数据表名或是数据库表名是否存在(新增时)
	   * @param formBusinessTable
	   * @return Map
	   * @author guoqingfu
	   * @date 2013-9-12下午06:38:57 
	   * @version V1.0
	  */
	 @RequestMapping(value="/formBusinessTable_isExistTable",method = RequestMethod.POST, produces = "application/json")
	 @ResponseBody
	 public String isExistTable(FormBusinessTable formBusinessTable){
	      Map<String,Object> map = formBusinessTableService.isExistTable(formBusinessTable);
	      return JSONObject.fromObject(map).toString();
	 }
}

package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
/**
 * 表单中产生用户手机变量
 * @author liub
 *
 */
public class UserMobileExpressionImpl extends ExpressionAbst{

	public UserMobileExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private SysUserService sysUserService;
	public String expressionParse(String expression) {
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean(SysUserService.class);
		}
		SysUser user = (SysUser) sysUserService.getByUserName(ComUtils.getLoginNamePin());
		if(user != null && user.getMobile() != null && user.getMobile().trim().length() > 0){
			return user.getMobile();
		}
		return "";
	}
}

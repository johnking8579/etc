package com.jd.oa.form.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.form.dao.FormDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.bean.FormItemBean;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单DaoImpl
 * User: yujiahe
 * Date: 13-9-12
 * Time: 下午12:55
 * To change this template use File | Settings | File Templates.
 */
@Component("formDao")
public class FormDaoImpl  extends MyBatisDaoImpl<Form,String> implements FormDao {
   
	@Override
    public boolean isExisForm( Form form){
        List<Form> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.form.model.Form.isExistForm",form);
            if(result != null && result.size()>0){
                return true;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return false;
    }
	
	@Override
	public List<Form> findFormWithSubForm(Form form){
		List<Form> result = null;
		try {
			result = this.getSqlSession().selectList("com.jd.oa.form.model.Form.findFormList",form);
		} catch (DataAccessException e) {
			throw e;
		}
		return result;
	}
	
	/**
	  * @Description: 根据ID查询信息
	  * @param id
	  * @return Form
	  * @author xulin
	  * @date 2013-9-16下午01:49:54 
	  * @version V1.0
	 */
	public Form selectByPrimaryKey(String id){
		Form result = null;
        try {
            result = (Form) this.getSqlSession().selectOne("com.jd.oa.form.model.Form.selectByPrimaryKey", id);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	
	/**
	 * 
	 * @param tableId
	 * @author xulin
	 * @return
	 */
	public List<FormItemBean> findFormItemsByTableId(String tableId) {
		List<FormItemBean> result = null;
        try {
            result =  this.getSqlSession().selectList("com.jd.oa.form.model.Form.findFormItemsByTableId", tableId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	
	/**
	 * 
	 * @param processDefintonId
	 * @author xulin
	 * @return
	 */
	public String getFormCodeByProDId(String processDefintonId){
		String result = null;
        try {
            result =  this.getSqlSession().selectOne("com.jd.oa.form.model.Form.getFormCodeByProDId", processDefintonId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	
	public Map<String,Object> getList(String sql,String type){
		Connection conn = null;
		ResultSet result = null;
		int updateRows = 0;
		Map<String,Object> mapResult = new HashMap<String, Object>(2);
		try{
			conn = this.getSqlSession().getConnection();
			if("0".equals(type)){
				if(sql.trim().toUpperCase().contains("SELECT") || sql.trim().toUpperCase().contains("DESC")){
					result = conn.createStatement().executeQuery(sql);
					mapResult.put("operate", "select");
					
					List<Map<String,String>> mapList = null;
					ResultSetMetaData rsmd=result.getMetaData();
					int size = result.getMetaData().getColumnCount();
				    if(result != null){
				    	mapList = new ArrayList<Map<String,String>>();
				    	Map<String,String> map = null;
			        	while(result.next()){
			        		map = new HashMap<String, String>(size);
			        		for(int i = 1; i <= size; i++){
			        			map.put(rsmd.getColumnName(i), result.getString(i));
			        		}
			        		mapList.add(map);
			        	}
			        	mapResult.put("data", mapList);
				    }
				}else{
					mapResult.put("operate", "error");
					mapResult.put("data", "请选择更新操作！");
				}
			} else {
				if(sql.trim().toUpperCase().contains("UPDATE")){//过滤掉drop,delete
					if(sql.trim().toUpperCase().contains("WHERE")){
						updateRows = conn.createStatement().executeUpdate(sql);
						mapResult.put("operate", "update");
						mapResult.put("data", updateRows);
					}else{
						mapResult.put("operate", "error");
						mapResult.put("data", "请添加where条件！");
					}
				}else if(sql.trim().toUpperCase().contains("SELECT") || sql.trim().toUpperCase().contains("DESC")){
					mapResult.put("operate", "error");
					mapResult.put("data", "请选择查询操作！");
				}else if(sql.trim().toUpperCase().contains("INSERT")){
					updateRows = conn.createStatement().executeUpdate(sql);
					mapResult.put("operate", "update");
					mapResult.put("data", updateRows);
				}else{
					mapResult.put("operate", "error");
					mapResult.put("data", "不允许删除操作！");
				}
			}
		     return mapResult;
		} catch(Exception ex){
			mapResult.put("operate", "error");
			mapResult.put("data", "表或视图不存在！");
			return mapResult;
			//throw new BusinessException("执行sql失败",ex);
		}
	}
}

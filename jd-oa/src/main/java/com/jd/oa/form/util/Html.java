package com.jd.oa.form.util;
/**
 * html 值转换
 * @author liubo
 *
 */
public class Html {
	public static String escape(String v) {
		return v == null ? v : escapeFull(v);
	}

	private static String escapeFull(String s) {
		StringBuilder b = new StringBuilder(s.length());
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (ch == '&' || ch == '\"' || ch == '<' || ch == '>') {
				b.append("&#").append((int) ch).append(";");
			} else {
				b.append(ch);
			}
		}

		return b.toString();
	}
}

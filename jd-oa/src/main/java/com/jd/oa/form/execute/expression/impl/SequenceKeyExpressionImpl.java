package com.jd.oa.form.execute.expression.impl;

import java.util.Map;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.form.service.FormSequenceService;
/***
 * 
 * @author birkhoff
 *
 */
public class SequenceKeyExpressionImpl extends ExpressionAbst{

	private static final String com = null;
	public SequenceKeyExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private FormSequenceService formSequenceService;
	public String expressionParse(String expression) {
		
		int sequenceNo = 0;
		if(formSequenceService == null){
			formSequenceService = SpringContextUtils.getBean(FormSequenceService.class);
		}
		if (expression.indexOf("(#") != -1) {
			String sequenceKey = expression.substring(expression.indexOf("(#") + 2, expression.indexOf(")"));
			sequenceNo = formSequenceService.getFormSequenceValue("BPM:" + sequenceKey);
		} else {
			String sequenceKey = expression.substring(expression.indexOf("<#") + 3, expression.indexOf(">"));
			sequenceNo = formSequenceService.getFormSequenceValue("BPM:" + sequenceKey);
		}

		return Integer.toString(sequenceNo);
	}
}

package com.jd.oa.form.metadata.constant;

/**
 * 元数据字段描述相关的类型参考
 * 
 */
public interface MetaDataMapConst {

	
	public static final String MAP_MAP_ENTITY = "实体字段";

	public static final String MAP_MAP_VIRTUAL = "虚拟字段";

	public static final String MAP_TYPE_NUMBER = "数值";

	public static final String MAP_TYPE_TEXT = "文本";

	public static final String MAP_TYPE_DATE = "日期";
	
	public static final String MAP_TYPE_MEMO = "备注";

	public static final String MAP_TYPE_LONG = "LONG";

	public static final String MAP_DISPLAY_NUMBER = "数值";

	public static final String MAP_DISPLAY_DATE = "日期";
	
	public static final String MAP_DISPLAY_DATETIME = "日期时间";

	public static final String MAP_DISPLAY_TIME = "时间";

	public static final String MAP_DISPLAY_MULTI_LINE = "多行";

	public static final String MAP_DISPLAY_HTML_EDIT = "HTML排版";

	public static final String MAP_DISPLAY_LINE = "单行";

	public static final String MAP_DISPLAY_RADIO = "单选按纽组";

	public static final String MAP_DISPLAY_CHECK = "复选框";

	public static final String MAP_DISPLAY_COMBOX = "列表";

	public static final String MAP_DISPLAY_REF_COMBOX = "参考列表";

	public static final String MAP_DISPLAY_BUTTON = "按钮";
	
	public static final String MAP_SLIDER = "滑杆";
	
	public static final String MAP_DISPLAY_MONEY = "货币";

	public static final String MAP_DISPLAY_FILE = "附件";

	public static final String MAP_DISPLAY_URL = "URL";

	public static final String MAP_DISPLAY_CACHET = "电子印章";
	
	public static final String MAP_DISPLAY_IWEBOFFICE = "iWebOffice";

	public static final String MAP_DISPLAY_DICT = "数据字典";

	public static final String MAP_DISPLAY_DEPRT_TREE = "部门字典";

	public static final String MAP_DISPLAY_XML_REPORT = "动态报表";
	
	public static final String MAP_DISPLAY_FORM_TRANSCATION = "表单数据传输";

	public static final String MAP_DISPLAY_ADDRESS = "地址簿";

	public static final String MAP_DISPLAY_PLAN = "协同日程";

	public static final String MAP_SUB_SHEET = "字段子表";
	
	public static final String MAP_DISPLAY_USERAC="用户扩展授权";
	

}
package com.jd.oa.form.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.RelationalOperator;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.dao.FormBusinessFieldDao;
import com.jd.oa.form.dao.FormBusinessTableDao;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;

/** 
 * @Description: 自定义表 Service实现
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */

@Service("formBusinessTableService")
public class FormBusinessTableServiceImpl extends BaseServiceImpl<FormBusinessTable, String>
		implements FormBusinessTableService {
	
	private static final Logger logger = Logger.getLogger(FormBusinessTableServiceImpl.class);
	
	@Autowired
	private FormBusinessTableDao formBusinessTableDao;
	@Autowired
	private FormBusinessFieldDao formBusinessFieldDao;
	@Autowired
	private FormItemService formItemService;
	
	public FormBusinessTableDao getDao() {
		return formBusinessTableDao;
	}
	/**
	  * @Description: 查询主表列表
	  * @return List<FormBusinessTable>
	  * @author guoqingfu
	  * @date 2013-9-12下午01:53:44 
	  * @version V1.0
	 */
	public List<FormBusinessTable> findParentTableList(){
		try{
			return formBusinessTableDao.findParentTableList();
		}catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询主表列表失败",e);
        }
	}
	/**
	  * @Description:根据ID查询数据表信息
	  * @param formBusinessTableId
	  * @return FormBusinessTable
	  * @author guoqingfu
	  * @date 2013-9-12下午02:44:03 
	  * @version V1.0
	 */
	public FormBusinessTable getFormBusinessTableInfo(String formBusinessTableId){
		try{
			return formBusinessTableDao.getFormBusinessTableInfo(formBusinessTableId);
		}catch (Exception e) {
            logger.error(e);
            throw new BusinessException("根据ID查询数据表信息失败",e);
        }
	}
	/**
	  * @Description: 删除自定义表
	  * @param formBusinessTableIds
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-12下午03:46:14 
	  * @version V1.0
	 */
	@Transactional
	public Map<String,Object> deleteFormBusinessTable(List<String> formBusinessTableIds){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean flag= false;
		map.put("formBusinessTableIds", formBusinessTableIds);
        try{
        	flag = formBusinessTableDao.isUsed(map);
        	if(flag){
				map.put("operator", false);
	            map.put("message", "存在子表、字段配置或已被表单引用,删除失败");
	            return map;
			}
        	flag =  formBusinessTableDao.deleteFormBusinessTable(map);
        	if(flag){
        		List<String> tableNameList = formBusinessTableDao.findTableNameList(map);
        		map.put("tableSchema", SystemConstant.TABLESCHEMA);
    			map.put("tableNameList", tableNameList);
    			List<String> createdTableNameList = formBusinessFieldDao.findCreatedTable(map);
    			for(String createdTableName : createdTableNameList){
    				MySqlAdapter adapter = new MySqlAdapter();
    				adapter.setSqlType(SqlType.DDL_DROP_TABLE).addTable(createdTableName);
    				sql(adapter);
    			}
    			
                map.put("operator", true);
                map.put("message", "删除成功");
            }else{
                map.put("operator", false);
                map.put("message", "删除失败");
            }
          return map;
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("删除失败",e);
        }
	}
	
	/**
	  * @Description: 判断是否存在数据表名
	  * @param tableChineseName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:00:24 
	  * @version V1.0
	 */
	public boolean isExistTableChineseName(FormBusinessTable formBusinessTable){
		try{
			return formBusinessTableDao.isExistTableChineseName(formBusinessTable);
		}catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询是否存在数据表名失败",e);
        }
	}
	
	/**
	  * @Description: 判断是否存在数据库表名
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:33:41 
	  * @version V1.0
	 */
	public boolean isExistTableName(FormBusinessTable formBusinessTable){
		try{
			return formBusinessTableDao.isExistTableName(formBusinessTable);
		}catch (Exception e) {
            logger.error(e);
            throw new BusinessException("查询是否存在数据库表名失败",e);
        }
	}
	/**
	  * @Description: 验证数据表名或是数据库表名是否存在
	  * @param formBusinessTable
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-12下午06:22:13 
	  * @version V1.0
	 */
	public Map<String,Object> isExistTable(FormBusinessTable formBusinessTable){
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag = false;
		//判断是否存在数据表名
		flag = this.isExistTableChineseName(formBusinessTable);
		if (flag) {
			map.put("operator", true);
			map.put("message", "存在数据表名");
			return map;
		}
		//判断是否存在数据库表名
		flag = this.isExistTableName(formBusinessTable);
		if(flag){
			map.put("operator", true);
			map.put("message", "存在数据库表名");
			return map;
		}
		map.put("operator", false);
		map.put("message", "不存在数据表名或是数据库表名");
		return map;
	}
	/**
	 * 根据业务ID获取业务对象
	 * @param boTableName
	 * @param businessObjectId
	 * @return
	 */
	public Map<String,Object> getBusinessData(String boTableName,String businessObjectId){
		if(businessObjectId == null || businessObjectId.trim().length() == 0){//不存的业务数据
			return null;
		}
		SqlAdapter adpater = new MySqlAdapter();
		adpater.setSqlType(SqlType.DML_SELECT);
		adpater.addTable(boTableName);
		adpater.addAndCondition("ID", RelationalOperator.EQ, businessObjectId, DataType.STRING);
		List<Map<String,Object>> maps = (ArrayList<Map<String,Object>>)super.sql(adpater);
		if(maps != null && maps.size() > 0){
			return maps.get(0);
		}
		return null;
	}
	
	@Override
	public List<Map<String, Object>> getBusinessData(String boTableName,List<String> columns, String businessObjectId) {

		SqlAdapter adapter = new MySqlAdapter();
		try {
			adapter.setSqlType(SqlType.DML_SELECT).addTable(boTableName);
			for(String c:columns){
				adapter.addColumn(c);
			}
			adapter.addAndCondition("YN", RelationalOperator.EQ, "0", DataType.STRING);
			adapter.addAndCondition("ID", RelationalOperator.EQ,businessObjectId, DataType.STRING);
			
			return (List<Map<String, Object>>) sql(adapter);
		}catch(Exception e){
			return null;
		}
		
	}
	/**
	 * 查询多个businessObjectId对应的数据
	 */
	public List<Map<String, Object>> getBusinessData(String boTableName,List<String> columns,List<String> businessObjectIds) {
		SqlAdapter adapter = new MySqlAdapter();
		try {
			adapter.setSqlType(SqlType.DML_SELECT).addTable(boTableName);
			for(String c:columns){
				adapter.addColumn(c);
			}
			adapter.addAndCondition("YN", RelationalOperator.EQ, "0", DataType.STRING);
			StringBuilder sb=new StringBuilder();
			for(String str:businessObjectIds){
				sb.append(" '"+str+"' , ");
			}
			sb.append(" '1' ");
			adapter.addAndInCondition("ID",sb.toString());
			return (List<Map<String, Object>>) sql(adapter);
		}catch(Exception e){
			return null;
		}
		
	}
	/**
	 * 根据业务ID保存当前业务数据对象
	 */
	@Override
	public String saveBusinessData(String boTableName,String datas, String businessObjectId,List<FormItem> formItems) {
		// TODO Auto-generated method stub
		SqlAdapter adapter = new MySqlAdapter();
		if(businessObjectId == null || businessObjectId.trim().length() == 0){//insert 操作
			adapter.setSqlType(SqlType.DML_INSERT);
		} else {//update操作
			adapter.setSqlType(SqlType.DML_UPDATE);
			adapter.addAndCondition("ID",RelationalOperator.EQ, businessObjectId, DataType.STRING);
		}
		Map<String,FormItem> _formItem = transformListToMapForFormItem(formItems);
		adapter.addTable(boTableName);
		JSONArray array = JSONArray.fromObject(datas);
		for(int i = 0 ; i < array.size() ; i++){
			JSONObject data = array.getJSONObject(i);
			if(data != null){
				String fieldName = data.getString("fieldName");
				String value = data.getString("value");
				if(value == null || value.trim().length() == 0 || value.equals("null")){
					continue;
				}
				FormItem formItem = _formItem.get(fieldName);//通过key获取字段类型
				if(formItem != null && formItem.getDataType() != null){
					if(value.trim().length() > 0){ // 字符串
				    	if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.STRING))){//字符
				    		adapter.addColumn(fieldName, value, DataType.STRING);
				    	} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.NUMBER))){//数值
				    		adapter.addColumn(fieldName, value, DataType.NUMBER);
				    	} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.DATE))){//日期
				    		value = DateUtils.dateFormat(value, formItem.getInputTypeConfig());
				    		adapter.addColumn(fieldName, value, DataType.DATE);
				    	} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.DATETIME))){//日期时间
				    		try {
								value = DateUtils.dateTimeFormat(value, formItem.getInputTypeConfig());
							} catch (ParseException e) {
								throw new BusinessException("日期值解析失败!" ,e);
							}
				    		adapter.addColumn(fieldName, value, DataType.DATETIME);
				    	} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.TIME))){//时间
				    		adapter.addColumn(fieldName, value, DataType.TIME);
				    	} 
				    }
				}
			}
		}
		adapter.addColumn("YN", "0", DataType.NUMBER);
		String executeFlag = String.valueOf(super.sql(adapter));
		if(businessObjectId == null || businessObjectId.trim().length() == 0){
			return executeFlag;
		} else {
			return businessObjectId;
		}
	}
	/**
	 * 保存子表数据
	 */
	public String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,List<FormItem> formItems){
		String ids = "";
		SqlAdapter adapter;
		JSONArray array = JSONArray.fromObject(datas);
		Map<String,FormItem> _formItem = transformListToMapForFormItem(formItems);
		for(int i = 0 ; i < array.size() ; i++){
			JSONObject data = array.getJSONObject(i);
			if(data != null){
				adapter = new MySqlAdapter();
				adapter.addTable(boTableName);
				if(data.get("ID") == null || data.get("ID").toString().trim().length() == 0){//新增数据
					adapter.setSqlType(SqlType.DML_INSERT);
					adapter.addColumn("PARENT_ID", businessObjectId, DataType.STRING);
				}else {//更新数据
					adapter.setSqlType(SqlType.DML_UPDATE);
					adapter.addAndCondition("id", RelationalOperator.EQ, data.get("ID").toString(), DataType.STRING);
				}
				Iterator iterator = data.entrySet().iterator(); 
				while(iterator.hasNext()) { 
				    Entry entry = (Entry)iterator.next(); 
				    String key = entry.getKey().toString();
				    if(key.toUpperCase().equals("ID") || key.toUpperCase().equals("YN") || key.equals("CREATE_TIME") || key.equals("CREATOR") || key.equals("MODIFY_TIME")){
				    	continue;
				    }
				    FormItem formItem = _formItem.get(key);//通过key获取字段类型
				    if(formItem != null && formItem.getDataType() != null){
				    	String value = String.valueOf(data.get(key));
				    	if(value != null){ // 字符串
				    		if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.STRING))){//字符
				    			adapter.addColumn(key, value, DataType.STRING);
				    		} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.NUMBER))){//数值
				    			if(value.length()==0){
				    				value = "NULL";
				    			}
				    			adapter.addColumn(key, value, DataType.NUMBER);
				    		} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.DATE))){//日期
				    			adapter.addColumn(key, value, DataType.DATE);
				    		} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.DATETIME))){//日期时间
				    			adapter.addColumn(key, value, DataType.DATETIME);
				    		} else if(formItem.getDataType().toUpperCase().equals(String.valueOf(DataType.TIME))){//时间
				    			adapter.addColumn(key, value, DataType.TIME);
				    		} 
				    	}
				    }
				} 
				adapter.addColumn("YN", "0", DataType.NUMBER);
				ids +="," + super.sql(adapter);
			}
		}
		if(ids.length()>0) ids = ids.substring(1);
		return ids;
	}
	/**
	 * formItem list 转换成  Map<fieldName,formItem>
	 * @param processNodeFormPrivileges
	 */
	private Map<String ,FormItem> transformListToMapForFormItem(List<FormItem> formItems){
		Map<String ,FormItem> _formItem = new HashMap<String,FormItem>();
		if(formItems != null){
			for(int i = 0 ; i < formItems.size() ; i++){
				FormItem formItem = formItems.get(i);
				if(formItem != null){
					_formItem.put(formItem.getFieldName(), formItem);
				}
			}
		}
		return _formItem;
	}
	/**
	 * @see
	 */
	public List<Map<String,Object>> getBusinessDatas(String boTableName,String businessObjectId){
		if(businessObjectId == null || businessObjectId.trim().length() == 0){//不存的业务数据
			return null;
		}
		SqlAdapter adpater = new MySqlAdapter();
		adpater.setSqlType(SqlType.DML_SELECT);
		adpater.addTable(boTableName);
		adpater.addAndCondition("PARENT_ID", RelationalOperator.EQ, businessObjectId, DataType.STRING);
		adpater.addAscOrderBy("id");
		return (ArrayList<Map<String,Object>>)super.sql(adpater);
	}
	/**
	 * @see
	 */
	public String deleteBindReportData(String boTableName,String id){
		SqlAdapter adpater = new MySqlAdapter();
		adpater.setSqlType(SqlType.DML_DELETE);
		adpater.addTable(boTableName);
		adpater.addAndCondition("id", RelationalOperator.EQ, id, DataType.STRING);
		return String.valueOf(super.sql(adpater));
	}
	/**
	 * 根据参数删除指定的业务数据
	 */
	@Override
	public String deleteBusinessObjectById(String boTableName,String businessObjectId,String formId) {
		SqlAdapter adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_DELETE);
		adapter.addTable(boTableName);
		adapter.addAndCondition("ID",RelationalOperator.EQ, businessObjectId, DataType.STRING);
		List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(formId,SystemConstant.FORM_TYPE_S);//表单子表
		if(subSheets != null && subSheets.size() > 0){
			for(int i = 0 ; i < subSheets.size(); i++){
				FormBean formBean = subSheets.get(i);
				if(formBean != null){
					deleteBusinessObjectByParentId(formBean.getFormCode(),businessObjectId);
				}
			}
		}
		// TODO Auto-generated method stub
		return String.valueOf(super.sql(adapter));
		
//		
	}
	/**
	 * 根据parentId删除指定的业务数据
	 */
	@Override
	public String deleteBusinessObjectByParentId(String boTableName,String parentId){
		SqlAdapter adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_DELETE);
		adapter.addTable(boTableName);
		adapter.addAndCondition("PARENT_ID",RelationalOperator.EQ, parentId, DataType.STRING);
		// TODO Auto-generated method stub
		return String.valueOf(super.sql(adapter));
	}
}

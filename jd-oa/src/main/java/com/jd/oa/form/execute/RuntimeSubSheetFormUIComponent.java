package com.jd.oa.form.execute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictTypeService;
import com.jd.oa.form.execute.component.FormUISubSheetComponentAbst;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetComboxImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetDateImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetDateTimeImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetFileImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetNumberImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetTextAreaImpl;
import com.jd.oa.form.execute.component.impl.sub.FormUIComponentSubSheetTextImpl;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;

/**
 * 子表UI COMPONENT类
 * @author birkhoff
 * @date   2013年10月29日
 *
 */
public class RuntimeSubSheetFormUIComponent {
	private RuntimeFormManager _rfm;
	public RuntimeSubSheetFormUIComponent(RuntimeFormManager rfm){
		this._rfm = rfm;
	}
	/**
	 * 构造子表UI
	 * @return
	 */
	public String buildSubSheetOfVerify(List<FormItem> items,String formCode){
		StringBuffer sheetBody = new StringBuffer();
		Map<String,String> cascadeMap = getSelectList(items);
		for(int i = 0 ; i < items.size() ; i++){	
    		FormItem formItem = items.get(i);
    		if(formItem != null && formItem.getFormId() != null){
    			FormBusinessField metaDataMapModel = new FormBusinessField();
    			try {
					ComUtils.copyProperties(formItem, metaDataMapModel);
				} catch (Exception e) {
					throw new BusinessException("对象模复制失败：" + e.getMessage());
				}
    			metaDataMapModel.setTableId(formItem.getFormId());
    			metaDataMapModel.setTableName(formCode);
    			if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
    				metaDataMapModel.setInputType("单行");
    				metaDataMapModel.setInputTypeConfig("");
    			}
    			metaDataMapModel.setId(formItem.getId());
    			UIComponentInterface uiComponent;
    			if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.NUMBER))){//数值
    				uiComponent = new FormUIComponentSubSheetNumberImpl(metaDataMapModel, "");
    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))){//日期
    				uiComponent = new FormUIComponentSubSheetDateImpl(metaDataMapModel, "");
    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATETIME))){//日期时间
    				uiComponent = new FormUIComponentSubSheetDateTimeImpl(metaDataMapModel, "");
    			} else {
    				if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.STRING))){
    					if(metaDataMapModel.getInputType().equals("多行")){
    						uiComponent = new FormUIComponentSubSheetTextAreaImpl(metaDataMapModel, "");
    					} else if(metaDataMapModel.getInputType().equals("列表")){
    						metaDataMapModel.setCascadeComBox(cascadeMap);//加入级联map
    						uiComponent = new FormUIComponentSubSheetComboxImpl(metaDataMapModel, "");
    					} else if(metaDataMapModel.getInputType().equals("单选按纽组")){
    						uiComponent = new FormUIComponentSubSheetComboxImpl(metaDataMapModel, "");
    					} else if(metaDataMapModel.getInputType().equals("附件")){
    						uiComponent = new FormUIComponentSubSheetFileImpl(metaDataMapModel, "");
    					} else {
    						uiComponent = new FormUIComponentSubSheetTextImpl(metaDataMapModel,"");
    					}
    				} else {
    					uiComponent = new FormUISubSheetComponentAbst(metaDataMapModel,"");
    				}
    			}
    			sheetBody.append("\n").append(uiComponent.toString()).append(",");
    		}
		}
		sheetBody.setLength(sheetBody.length() - 1);
		sheetBody.append("\n    ]]\n");
		return sheetBody.toString();
	}
	
	/**
	 * 
	 * @author zhengbing
	 * @desc 返回主表所有未隐藏的select下拉菜单，为级联做准备
	 * @date 2014年7月8日 下午4:40:06
	 * @return List
	 */
	private Map<String, String> getSelectList(List<FormItem> items) {
		List<FormItem> selects = new ArrayList<FormItem>();
		List<String> dictTypeCodes = new ArrayList<String>();
		for (FormItem fi : items) {
			try {
				// 添加select菜单
				if (null != fi.getInputTypeConfig()
						&& fi.getInputTypeConfig().indexOf("dataDict#") > -1
						&& (!"M".equals(fi.getTableType()))
						&& "列表".equals(fi.getInputType())
						&& (!"1".equals(fi.getIsHidden()))) {
					selects.add(fi);
					dictTypeCodes.add(fi.getInputTypeConfig().trim()
							.substring(9));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (dictTypeCodes.size() <= 1)
			return null;
		DictTypeService dictTypeService = SpringContextUtils
				.getBean("dictTypeService");
		List<DictType> dictTypeList = dictTypeService
				.getDictTypeCodes(dictTypeCodes);
		if (null == dictTypeList || dictTypeList.size() == 0)
			return null;

		Map<String, String> cascadeSelectMap = new HashMap<String, String>();
		Map<String, String> cascadeSelectFormItemMap = new HashMap<String, String>();
		
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < dictTypeList.size(); i++) {// 构造父子表DictType Map
			DictType compareDt = dictTypeList.get(i);
			for (int j = i + 1; j < dictTypeList.size(); j++) {
				DictType beCompareDt = dictTypeList.get(j);
				if (compareDt.getId().equals(beCompareDt.getParentId())) {
					cascadeSelectMap.put(compareDt.getDicttypeCode(),
							beCompareDt.getDicttypeCode());// 父--子
					sb.append(compareDt.getDicttypeCode()+":"+beCompareDt.getDicttypeCode()+",");
				} else if (beCompareDt.getId().equals(compareDt.getParentId())) {
					cascadeSelectMap.put(beCompareDt.getDicttypeCode(),
							compareDt.getDicttypeCode());// 父--子
					sb.append(beCompareDt.getDicttypeCode()+":"+compareDt.getDicttypeCode()+",");
				}
			}

		}
		String cascadeSelectMapString = sb.toString();//父子map字符串
		
		List<FormItem> tempFormItems = new ArrayList<FormItem>();
		tempFormItems.addAll(selects);
		if(null!=tempFormItems&&tempFormItems.size()>0){
			getSelectCascadeFromItem(cascadeSelectFormItemMap, tempFormItems.get(0), tempFormItems, cascadeSelectMapString);
		}
		
		return cascadeSelectFormItemMap;
	}

	
	/**
	 * 
	 * @author zhengbing 
	 * @desc 递归找出父子关联，放入map
	 * @date 2014年7月10日 下午5:10:53
	 * @return List<FormItem>
	 */
	private void getSelectCascadeFromItem(Map<String,String> map,FormItem fatherFromItem,List<FormItem> formItems,String cascadeSelectMapString){
		try {
			String fartherCode = fatherFromItem.getInputTypeConfig().replace("dataDict#", "").trim();
			for (Iterator it = formItems.iterator(); it.hasNext();) {
				FormItem formItem = (FormItem)it.next();
				if(null==formItem.getInputTypeConfig()){ 
					it.remove();
					continue;
				}
				if(formItem.getId().equals(fatherFromItem.getId())){
					it.remove();
					continue;
				}
				String sonCode = formItem.getInputTypeConfig().replace("dataDict#", "").trim();
				if(cascadeSelectMapString.contains(fartherCode+":"+sonCode+",")){//找到子节点
					map.put(fatherFromItem.getFieldName(), formItem.getFieldName()+":"+sonCode);
					it.remove();
					getSelectCascadeFromItem(map,formItem,formItems,cascadeSelectMapString);
					break;
				}
				if(cascadeSelectMapString.contains(sonCode+":")){//找到与自己类型相同的节点
					getSelectCascadeFromItem(map,formItem,formItems,cascadeSelectMapString);
					break;
				}
			}
			if(null!=formItems&&formItems.size()>0){
				getSelectCascadeFromItem(map,formItems.get(0),formItems,cascadeSelectMapString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
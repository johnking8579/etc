package com.jd.oa.form.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;

/** 
 * @Description: 自定义表 Service
 * @author guoqingfu
 * @date 2013-9-12 上午10:49:56 
 * @version V1.0 
 */
public interface FormBusinessTableService extends BaseService<FormBusinessTable, String>{

	/**
	  * @Description: 查询主表列表
	  * @return List<FormBusinessTable>
	  * @author guoqingfu
	  * @date 2013-9-12下午01:53:44 
	  * @version V1.0
	 */
	public List<FormBusinessTable> findParentTableList();
	/**
	  * @Description:根据ID查询数据表信息
	  * @param formBusinessTableId
	  * @return FormBusinessTable
	  * @author guoqingfu
	  * @date 2013-9-12下午02:44:03 
	  * @version V1.0
	 */
	public FormBusinessTable getFormBusinessTableInfo(String formBusinessTableId);
	/**
	  * @Description: 删除自定义表
	  * @param formBusinessTableIds
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-12下午03:46:14 
	  * @version V1.0
	 */
	public Map<String,Object> deleteFormBusinessTable(List<String> formBusinessTableIds);
	/**
	  * @Description: 判断是否存在数据表名
	  * @param tableChineseName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:00:24 
	  * @version V1.0
	 */
	public boolean isExistTableChineseName(FormBusinessTable formBusinessTable);
	/**
	  * @Description: 判断是否存在数据库表名
	  * @param tableName
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-12下午05:33:41 
	  * @version V1.0
	 */
	public boolean isExistTableName(FormBusinessTable formBusinessTable);
	/**
	  * @Description: 验证数据表名或是数据库表名是否存在
	  * @param formBusinessTable
	  * @return Map<String,Object>
	  * @author guoqingfu
	  * @date 2013-9-12下午06:22:13 
	  * @version V1.0
	 */
	public Map<String,Object> isExistTable(FormBusinessTable formBusinessTable);
	
	/**
	 * 根据业务ID获取业务对象
	 * @param boTableName
	 * @param businessObjectId
	 * @return
	 */
	public Map<String,Object> getBusinessData(String boTableName,String businessObjectId);
	/**
	 * 根据业务物理表名,字段名,业务ID查询业务数据
	 * @param boTableName
	 * @param columns
	 * @param businessObjectId
	 * @return
	 */
	public List<Map<String,Object>> getBusinessData(String boTableName,List<String> columns,String businessObjectId);
	/**
	 * 根据业务物理表名,字段名,多个业务ID查询业务数据
	 * @param boTableName
	 * @param columns
	 * @param businessObjectIds
	 * @return
	 */
	public List<Map<String,Object>> getBusinessData(String boTableName,List<String> columns,List<String> businessObjectIds);
	/**
	 * 根据businessObjectId 保存业务数据
	 * @param datas
	 * @param businessObjectId
	 * @return
	 */
	public String saveBusinessData(String boTableName,String datas, String businessObjectId,List<FormItem> formItems);
	/**
	 * 根据参数删除指定的业务数据
	 * @param boTableName
	 * @param businessObjectId
	 * @return
	 */
	public String deleteBusinessObjectById(String boTableName,String businessObjectId,String formId);
	/**
	 * 根据parentId删除指定的业务数据
	 */
	public String deleteBusinessObjectByParentId(String boTableName,String parentId);
	/**
	 * 保存子表数据
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @return
	 */
	public String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,List<FormItem> formItems);
	/**
	 * 获取子表数据
	 * @param boTableName
	 * @param businessObjectId
	 * @return
	 */
	public List<Map<String,Object>> getBusinessDatas(String boTableName,String businessObjectId);
	
	/**
	 * 删除指定boTableName的业务数据
	 * @param boTableName
	 * @param id
	 * @return
	 */
	public String deleteBindReportData(String boTableName,String id);
}


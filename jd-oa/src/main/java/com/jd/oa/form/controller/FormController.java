package com.jd.oa.form.controller;

import com.jd.common.web.LoginContext;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.system.model.SysUserRole;
import com.jd.oa.system.service.SysUserRoleService;
import com.jd.oa.system.service.SysUserService;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.velocity.runtime.directive.Parse;
import org.apache.ws.commons.schema.constants.Constants.SystemConstants;
import org.apache.zookeeper.client.FourLetterWordMain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.internet.NewsAddress;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单Controller
 * User: yujiahe
 * Date: 13-9-12
 * Time: 上午11:52
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "/form")
public class FormController {

    private static final Logger logger = Logger.getLogger(FormBusinessTableController.class);

    @Autowired
    private FormService formService;
    @Autowired
    private DictDataService dictDataService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private FormItemService formItemService;

    /**
     * 表单库首页
     */

    @RequestMapping(value = "/form_index", method = RequestMethod.GET)
    public String formIndex( Model model) {
        String erpId=ComUtils.getLoginNamePin();
        boolean isAdministrator=sysUserService.isAdministrator(erpId);
//        boolean isPowerUser=sysUserService.isPowerUser(erpId);
        model.addAttribute("isAdministrator", isAdministrator);
        return "form/form_index";
    }

    /**
     * 流程基本属性设置表单查询
     */

    @RequestMapping(value = "/formSearch_index", method = RequestMethod.GET)
    public String formSearchIndex() {
        return "form/formSearch_index";
    }

    /**
     * @Description: 查询表单列表
     * @author yujiahe
     */
    @RequestMapping(value = "/form_page", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> page(HttpServletRequest request, @RequestParam String createType, @RequestParam String owner, @RequestParam String formName, Form form) {
        //创建pageWrapper
        PageWrapper<Form> pageWrapper = new PageWrapper<Form>(request);
        pageWrapper.addSearch("formType", "M");
        if (formName.contains("%")) {
            formName = formName.replace("%", "\\%");
        }
        if( formName.contains("_")){
            formName=formName.replace("_","\\_");
        }
        if (null != createType && !createType.equals("")) {
            pageWrapper.addSearch("createType", createType);
        }
        if (null != owner && !owner.equals("")) {
            pageWrapper.addSearch("owner", owner);
        }
        if (null != formName && !formName.equals("")) {
            pageWrapper.addSearch("formName", formName);
        }
        String erpId=ComUtils.getLoginNamePin();
        boolean isAdministrator=sysUserService.isAdministrator(erpId);
        boolean isPowerUser=sysUserService.isPowerUser(erpId);
        if(isAdministrator){
            formService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        }else if(isPowerUser){
            pageWrapper.addSearch("owner",sysUserService.getUserIdByUserName(erpId,"1"));
            formService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        }
        return pageWrapper.getResult();
    }
    
    /**
     * @Description: 查询表单列表
     * @author yujiahe
     */
    @RequestMapping(value = "/form_page_def", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> pageDefUser(HttpServletRequest request, @RequestParam String createType, @RequestParam String owner, @RequestParam String formName, Form form) {
        //创建pageWrapper
        PageWrapper<Form> pageWrapper = new PageWrapper<Form>(request);
        pageWrapper.addSearch("formType", "M");
        if (formName.contains("%")) {
            formName = formName.replace("%", "\\%");
        }
        if( formName.contains("_")){
            formName=formName.replace("_","\\_");
        }
        if (null != createType && !createType.equals("")) {
            pageWrapper.addSearch("createType", createType);
        }
        if (null != owner && !owner.equals("")) {
            pageWrapper.addSearch("owner", owner);
        }
        if (null != formName && !formName.equals("")) {
            pageWrapper.addSearch("formName", formName);
        }
        //后台取值

        String erpId=ComUtils.getLoginNamePin();
        boolean isAdministrator=sysUserService.isAdministrator(erpId);
        boolean isPowerUser=sysUserService.isPowerUser(erpId);
        if(isAdministrator){
            formService.find(pageWrapper.getPageBean(), "findByMapDefUser", pageWrapper.getConditionsMap());
        }else if(isPowerUser){
            pageWrapper.addSearch("owner",sysUserService.getUserIdByUserName(erpId,"1"));
            formService.find(pageWrapper.getPageBean(), "findByMapDefUser", pageWrapper.getConditionsMap());
        }



        //返回到页面的额外数据
        //pageWrapper.addResult("returnKey","returnValue");
        return pageWrapper.getResult();
    }

    /**
     * @param parentId 如果为0则插入为主表否则为子表
     * @Description:进入新建表单页面
     * @author yujiahe
     */
    @RequestMapping(value = "/form_add", method = RequestMethod.GET)
    public String formAdd(String parentId, Model model) {
        model.addAttribute("parentId", parentId);
        return "form/form_add";
    }

    /**
     * @param  form
     * @return String
     * @Description: 根据表单名称查看表单是否存在
     * @author yujiahe
     */
    @RequestMapping(value = "/form_isExistForm", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean isExistForm(Form form) {
        if((!form.getParentId().equals("0"))&&(null!=form.getParentId())&&(!"".equals(form.getParentId()))){ //查询子表是否重名 则查询主表所有人下是否有重名
            Form parentForm=formService.get( form.getParentId()) ;
            form.setOwner(parentForm.getOwner());
        }else{     //主表是否重名 则查询 登陆用户下的表单是否有重名
            String loginUserId = "";
            LoginContext context = LoginContext.getLoginContext();
            if (context != null) {
                loginUserId =sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin(),"1");
            }
            form.setOwner(loginUserId);
        }

        boolean flag = formService.isExistForm(form);
        return flag;
    }

    /**
     * @param form
     * @return String
     * @Description: 新增表单
     * @author yujiahe
     */
    @RequestMapping(value = "/form_addSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String formAddSave(Form form) {
        Map<String, Object> map = new HashMap<String, Object>();
        String loginUserId = "";
        LoginContext context = LoginContext.getLoginContext();
        if (context != null) {
            loginUserId =sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin(),"1");
        }
        List<Form> newFormList = null;
        List<FormItem> newFormItemList = null;
        String mainUUID = IdUtils.uuid2();
        if (SystemConstant.FORM_TYPE_CM.equals(form.getCreateType())) {
            //复制模式的场合
            String formId = form.getFormId();
            Form fromForm = this.formService.get(formId);
            boolean isFromFormNull = true;
            if(fromForm!=null){
            	isFromFormNull = false;
            	form.setTableId(fromForm.getTableId());
            }
            

            List<FormItem> formItemList = this.formItemService.selectByFormId(formId);
            newFormList = new ArrayList<Form>(fromForm.getSubTables() != null ? fromForm.getSubTables().size() + 1 : 1);
            newFormItemList = new ArrayList<FormItem>();
            //主表字段
            if (formItemList != null && formItemList.size() > 0) {
                for (FormItem formItem : formItemList) {
                    formItem.setId(IdUtils.uuid2());
                    formItem.setFormId(mainUUID);
                    newFormItemList.add(formItem);
                }
            }
            //子表单字段
            if (!isFromFormNull) {
                List<Form> subForms = fromForm.getSubTables();
                if (subForms != null && subForms.size() > 0) {
                    Form subForm = null;
                    String subFormUUID = null;
                    for (int i = 0; i < subForms.size(); i++) {
                        subForm = subForms.get(i);
                        formItemList = this.formItemService.selectByFormId(subForm.getId());
                        subFormUUID = IdUtils.uuid2();
                        subForm.setId(subFormUUID);
                        subForm.setParentId(mainUUID);
                        subForm.setFormCode(SystemConstant.T_JDOA_.concat(Long.toString(IdUtils.randomLong())));
                        subForm.setFormName(form.getFormName().concat("-子表").concat(Integer.toString(i)));
                        subForm.setCreateType(SystemConstant.FORM_TYPE_CM);
                        subForm.setTableId("");
                        form.setOwner(loginUserId);//默认创建者为表单所有人
                        newFormList.add(subForm);
                        //子表字段
                        if (formItemList != null && formItemList.size() > 0) {
                            for (FormItem formItem : formItemList) {
                                formItem.setId(IdUtils.uuid2());
                                formItem.setFormId(subFormUUID);
                                newFormItemList.add(formItem);
                            }
                        }
                    }
                }
            }
        } else {
            newFormList = new ArrayList<Form>(1);
        }
        form.setId(mainUUID);
        form.setTableId("");
        form.setFormCode(SystemConstant.T_JDOA_.concat(Long.toString(IdUtils.randomLong())));
        if (SystemConstant.FORM_TYPE_M.equals(form.getFormType())) {
            form.setParentId(null);
        }
        // TODO
        if (SystemConstant.FORM_TYPE_S.equals(form.getFormType())) { //创建子表时，所有人为其主表的所有人
            Form parentForm=formService.get( form.getParentId()) ;
          form.setOwner(parentForm.getOwner());
        }else{
            form.setOwner(loginUserId);//默认创建者为表单所有人
        }

        newFormList.add(form);

        formService.createFormAndFormItem(newFormList, newFormItemList);

        map.put("operator", true);
        map.put("message", "添加成功");
        map.put("formId", form.getId());
        return JSONObject.fromObject(map).toString();
    }

    /**
     * @Description:进入查看表单详情页面
     * @author yujiahe
     */
    @RequestMapping(value = "/form_view", method = RequestMethod.GET)
    public String formView(String formId, Model model) {
        //拿到表单信息
        Form form = formService.get(formId);
        model.addAttribute(form);
        //拿到表单类型数据字典
        List<DictData> dictDataList = null;
        dictDataList = dictDataService.findDictDataList("createType");
        model.addAttribute(dictDataList);
        return "form/form_view";
    }

    /**
     * @Description:进入编辑表单详情页面
     * @author yujiahe
     */
    @RequestMapping(value = "/form_update", method = RequestMethod.GET)
    public String formUpdate(String formId, Model model) {
        //拿到表单信息
        Form form = formService.get(formId);
        model.addAttribute(form);
        //拿到表单类型数据字典
        List<DictData> dictDataList = null;
        dictDataList = dictDataService.findDictDataList("createType"); //拿到数据字典
        model.addAttribute(dictDataList);
        return "form/form_update";
    }

    /**
     * @param form
     * @return String
     * @Description: 编辑表单后保存
     * @author yujiahe
     */
    @RequestMapping(value = "/form_updateSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String formUpdateSave(Form form) {
        Map<String, Object> map = new HashMap<String, Object>();
        formService.update(form);
        map.put("operator", true);
        map.put("message", "添加成功");
        return JSONObject.fromObject(map).toString();
    }

    /**
     * @Description:删除表单支持批量
     * @author yujiahe
     */
    @RequestMapping(value = "/form_delete")
    @ResponseBody
    public String formDelete(String ids,String formType) {
        Map<String, Object> map = new HashMap<String, Object>();
            formService.mutidelete(ids,formType);//批量删除
        map.put("operator", true);
        map.put("message", "删除成功");
        return JSONObject.fromObject(map).toString();
    }

    /**
     * @Description:进入变更所有人页面
     * @author yujiahe
     */
    @RequestMapping(value = "/formOwner_update", method = RequestMethod.GET)
    public String formOwnerUpdate(String ids, Model model) {
        //根据角色名称拿到用户列表
        List<SysUserRole> userList = sysUserRoleService.findUserListByRoleName("Power User");
        model.addAttribute("userList", userList);

        model.addAttribute("ids", ids);
        return "form/formOwner_update";
    }

    @RequestMapping(value = "/findPowerUserList")
    @ResponseBody
    public String findPowerUserList() {
        //创建pageWrapper
        List<SysUserRole> userList = sysUserRoleService.findUserListByRoleName("Power User");
        String json = JsonUtils.toJsonByGoogle(userList);
        return json;
    }


    /**
     * @param owner
     * @param ids
     * @return String
     * @Description: 保存变更所有人
     * @author yujiahe
     */
    @RequestMapping(value = "/formOwner_updateSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String formOwnerUpdateSave(@RequestParam String owner, @RequestParam String ids) {
        Map<String, Object> map= formService.updateOwner(owner, ids);
        return JSONObject.fromObject(map).toString();
    }

}

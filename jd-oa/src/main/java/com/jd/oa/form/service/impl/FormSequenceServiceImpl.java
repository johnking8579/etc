package com.jd.oa.form.service.impl;

import com.jd.common.util.StringUtils;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.form.dao.FormSequenceDao;
import com.jd.oa.form.model.FormSequence;
import com.jd.oa.form.service.FormSequenceService;
import com.jd.oa.process.service.ProcessDefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service
public class FormSequenceServiceImpl extends BaseServiceImpl<FormSequence,String> implements FormSequenceService {
    @Autowired
    private  FormSequenceDao formSequenceDao;
	@Autowired
	private ProcessDefService processDefService;
    
    public FormSequenceDao getDao() {
        return formSequenceDao;
    }

	@Override
	public int getFormSequenceValue(String typeId, String sequenceType) {
		// TODO Auto-generated method stub
		return formSequenceDao.getFormSequenceValue(typeId, sequenceType);
	}

	@Override
	public int getFormSequenceValue(String typeId) {
		// TODO Auto-generated method stub
		return formSequenceDao.getFormSequenceValue(typeId, FormSequenceService.FORM_SEQUENCE_TYPE_PROCESS);
	}

	@Override
	public String getProcessSequenceCode(String followPrefix) {
		if(StringUtils.isEmpty(followPrefix)){
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar cal   =   Calendar.getInstance();
		//今天 格式如:20140514
		String toady = format.format(cal.getTime());
		//删除以前的计数
		FormSequence formSequence = new FormSequence();
		formSequence.setTypeId(followPrefix);
		List<FormSequence> formSequenceList = formSequenceDao.find("getLikeTypeId",followPrefix);
		for(FormSequence item:formSequenceList){
			if(!item.getTypeId().equals(followPrefix + toady)){
				formSequenceDao.delete("deleteByTypeId",item);
			}
		}
		//返回今天的计数
		int num = formSequenceDao.getFormSequenceValue(followPrefix+toady,FormSequenceService.FORM_SEQUENCE_TYPE_PROCESS);
		return followPrefix+toady+String.format("%1$04d", num);
	}
}

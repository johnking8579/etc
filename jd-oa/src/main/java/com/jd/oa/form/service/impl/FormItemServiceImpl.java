package com.jd.oa.form.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.form.dao.FormDao;
import com.jd.oa.form.dao.FormItemDao;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description: 表单项ServiceImpl
 * User: 许林
 * Date: 13-9-12
 * Time: 上午11:56
 * To change this template use File | Settings | File Templates.
 */
// Spring Service Bean的标识.
@Service("formItemService")
// 默认将类中的所有函数纳入事务管理.
@Transactional
public class FormItemServiceImpl extends BaseServiceImpl<FormItem,String> implements FormItemService {
    @Autowired
    private  FormItemDao formItemDao;
    @Autowired
    private  FormDao formDao;
    @Autowired
    private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
    
    public FormItemDao getDao() {
        return formItemDao;
    }
    
    /**
	 * 根据formId获取表单详细项目信息
	 */
	public List<FormItem> selectByFormId(String formId){
		return this.formItemDao.selectByFormId(formId);
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2014-3-20 下午04:10:07
	 *
	 * @param formId
	 * @param nodeId
	 * @return
	 */
	public List<FormItem> selectEditItemByFormIdAndNodeId(String formId,String nodeId){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("formId", formId);
		map.put("nodeId", nodeId);
		return this.formItemDao.selectEditItemByFormIdAndNodeId(map);
	}
	
	/**
	 *  获取表单下所有的明细项信息包含子表
	 * @param formId
	 * @return
	 */
	public List<FormItem> getFormItemByFormId(String formId){
		Form formModel = this.formDao.selectByPrimaryKey(formId);
		List<FormItem> result = this.selectByFormId(formId);;
		if(result!=null&&result.size()>0){
			for(FormItem item:result){
				item.setTableType(formModel.getFormType());
			}
		}
		if(formModel != null){
			List<Form> listForm= formModel.getSubTables();
			if(listForm != null && listForm.size() > 0){
				for(Form form:listForm){
					List<FormItem> formItemList = this.selectByFormId(form.getId());
					if(formItemList != null&& formItemList.size()>0){
						for(FormItem item:formItemList){
							item.setTableType(form.getFormType());
							result.add(item);
						}
					}
				}
			}
		}
		return result;
	}
	
	/**
	 *  根据FormId和表类型获取明细项信息
	 * @param formId
	 * @param formType M:主表 S:字表
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getFormItemByFormIdAndType(String formId,String formType){
		Form formModel = this.formDao.selectByPrimaryKey(formId);
		List result = new ArrayList();
		if(SystemConstant.FORM_TYPE_M.equals(formType)){
			List<FormItem> listFormItem = this.selectByFormId(formId);
			if(null!=listFormItem && listFormItem.size()>0){
				return listFormItem;
			}
		} else {
			if(formModel != null){
				List<Form> listForm= formModel.getSubTables();
				if(listForm != null && listForm.size() > 0){
					String subFormId = null;
					Map<String,List<FormItem>> map = null;
					map = new HashMap(listForm.size());
					for(Form form:listForm){
						subFormId = form.getId();
						List<FormItem> formItemList = this.selectByFormId(form.getId());
						if(formItemList != null&& formItemList.size()>0){
							map.put(subFormId, formItemList);
						}
					}
					result.add(map);
				}
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @desc 根据FormId、processNodeId和表类型获取明细项信息
	 * @author WXJ
	 * @date 2014-3-20 上午11:21:58
	 *
	 * @param formId
	 * @param formType
	 * @param processNodeId
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List getFormItemByFormIdAndType(String formId,String formType,String processNodeId){
//		ProcessNodeFormPrivilege processNodeFormPrivilege = new ProcessNodeFormPrivilege();
//		processNodeFormPrivilege.setFormId(formId);
//		processNodeFormPrivilege.setNodeId(processNodeId);
//		processNodeFormPrivilege.setEdit("1");
//		List<ProcessNodeFormPrivilege> pnfpList = processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
				
		Form formModel = this.formDao.selectByPrimaryKey(formId);
		List<FormItem> listFormItem = this.selectEditItemByFormIdAndNodeId(formId,processNodeId);
		List result = new ArrayList();
		if(SystemConstant.FORM_TYPE_M.equals(formType)){
			if(listFormItem!=null&&listFormItem.size()>0){
				for(FormItem item:listFormItem){
					result.add(item);
				}
			}
		} else {
			if(formModel != null){
				List<Form> listForm= formModel.getSubTables();
				if(listForm != null && listForm.size() > 0){
					String subFormId = null;
					Map<String,List<FormItem>> map = null;
					map = new HashMap(listForm.size());
					for(Form form:listForm){
						subFormId = form.getId();
						List<FormItem> formItemList = this.selectEditItemByFormIdAndNodeId(form.getId(),processNodeId);
						if(formItemList != null&& formItemList.size()>0){
							map.put(subFormId, formItemList);
						}
					}
					result.add(map);
				}
			}
		}
		return result;
	}
	
	 /**
     * 根据FormId和表类型获取明细项信息
     * @param formId
     * @param formType
     * @return List<FormBean>
     */
	public List<FormBean> getFormAndItemsByFormId(String formId,String formType) {
		Form formModel = this.formDao.selectByPrimaryKey(formId);
		List<FormBean> result = new ArrayList<FormBean>();
		FormBean formBean = null;
		if(SystemConstant.FORM_TYPE_M.equals(formType)){
			List<FormItem> listFormItem = this.selectByFormId(formId);
			if(null != listFormItem && listFormItem.size() > 0){
				formBean = new FormBean();
				formBean.setFormId(formId);
				formBean.setFormName(formModel.getFormName());
				formBean.setFormCode(formModel.getFormCode());
				formBean.setListFormItems(listFormItem);
				result.add(formBean);
			}
		} else {
			if(null != formModel){
				List<Form> listForm= formModel.getSubTables();
				if(null != listForm && listForm.size() > 0){
					String subFormId = null;
					String subTableName = null;
					for(Form form:listForm){
						subFormId = form.getId();
						subTableName = form.getFormName();
						List<FormItem> formItemList = this.selectByFormId(form.getId());
						if(null != formItemList && formItemList.size() > 0){
							formBean = new FormBean();
							formBean.setFormId(subFormId);
							formBean.setFormName(subTableName);
							formBean.setFormCode(form.getFormCode());
							formBean.setListFormItems(formItemList);
							result.add(formBean);
						}
					}
				}
			}
		}
		return result;
	}
		
    /**
     * 
     * @desc 根据FormId、processNodeId和表类型获取明细项信息
     * @author WXJ
     * @date 2014-3-20 下午03:37:04
     *
     * @param formId
     * @param formType
     * @param processNodeId
     * @return
     */
	public List<FormBean> getFormAndItemsByFormId(String formId,String formType,String processNodeId) {
		Form formModel = this.formDao.selectByPrimaryKey(formId);
		List<FormItem> listFormItem = this.selectEditItemByFormIdAndNodeId(formId,processNodeId);
		List<FormBean> result = new ArrayList<FormBean>();
		FormBean formBean = null;
		if(SystemConstant.FORM_TYPE_M.equals(formType)){
			if(listFormItem != null && listFormItem.size() > 0){
				formBean = new FormBean();
				formBean.setFormId(formId);
				formBean.setFormName(formModel.getFormName());
				formBean.setFormCode(formModel.getFormCode());
				formBean.setListFormItems(listFormItem);
				result.add(formBean);
			}
		} else {
			if(formModel != null){
				List<Form> listForm= formModel.getSubTables();
				if(listForm != null && listForm.size() > 0){
					String subFormId = null;
					String subTableName = null;
					for(Form form:listForm){
						subFormId = form.getId();
						subTableName = form.getFormName();
						List<FormItem> formItemList = this.selectEditItemByFormIdAndNodeId(form.getId(),processNodeId);
						if(formItemList != null && formItemList.size() > 0){
							formBean = new FormBean();
							formBean.setFormId(subFormId);
							formBean.setFormName(subTableName);
							formBean.setFormCode(form.getFormCode());
							formBean.setListFormItems(formItemList);
							result.add(formBean);
						}
					}
				}
			}
		}
		return result;
	}
	
    /**
     * 批量删除表单项
     * @param ids
     */
    @Override
    public void mutidelete(String ids) {
        String[] args = ids.split(",");
        for (String id : args) {
            formItemDao.delete(id);
        }
    }

    public List<FormItem> selectItemByFormIdAndFieldName(String formId,String fieldName){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("formId", formId);
        map.put("fieldName", fieldName);
        return this.formItemDao.selectItemByFormIdAndFieldName(map);
    }
    
    public List<FormItem> findFromAndSubItems(List<String> formIds){
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("formIds", formIds);
    	return this.formItemDao.findFromAndSubItems(map);
    }
}

package com.jd.oa.form.execute.component.impl;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.di.model.DiDatasourceFieldMap;
import com.jd.oa.di.service.DiDatasourceFieldMapService;
import com.jd.oa.form.execute.component.FormUIComponentAbst;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.util.Html;

public class FormUIComponentTextImpl extends FormUIComponentAbst{
	private DiDatasourceFieldMapService diDatasourceFieldMapService;
	public FormUIComponentTextImpl(FormBusinessField metaDataMapModel, String value) {
		super(metaDataMapModel, value);
	}
	@Override
	public String getReadHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		StringBuffer html = new StringBuffer();
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		html.append("<input type=text readonly name='").append(getMetaDataMapModel().getFieldName()).append("'  id='").append(getMetaDataMapModel().getFieldName()).append("' value=\"").append(Html.escape(value)).append("\">");
		return html.toString();
	}

	@Override
	public String getModifyHtmlDefine(Map<String,Object> params) {
		// TODO Auto-generated method stub
		String fieldLength = "";
		String value =  getValue();
//		if(value.trim().length() == 0){
//			value = getMetaDataMapModel().getDefaultValue() == null ? "" : getMetaDataMapModel().getDefaultValue();
//		}
		String configValue = "";
		String inputLength = "";
		String dataSourceId = "";
		String dataSourceName = "";
		String quickSearch = "";
		String inputTypeConfig = getMetaDataMapModel().getInputTypeConfig();
		if(inputTypeConfig != null && inputTypeConfig.trim().length() > 0){
			try{
				JSONObject jsonObject = JSONObject.fromObject(inputTypeConfig);
				if(jsonObject != null){
					if(jsonObject.get("configValue") != null){
						configValue = jsonObject.get("configValue").toString();
					}
//					if(jsonObject.get("inputLength") != null){
//						inputLength = jsonObject.get("inputLength").toString();
//					}
					if(jsonObject.get("dataSourceId") != null){
						dataSourceId = jsonObject.get("dataSourceId").toString();
					}
					if(jsonObject.get("dataSourceName") != null){
						dataSourceName = jsonObject.get("dataSourceName").toString();
					}
					if(jsonObject.get("quickSearch") != null){
						quickSearch = jsonObject.get("quickSearch").toString();
					}
				}
			}catch(JSONException e){
				
			}
		}
		if(getMetaDataMapModel().getLength() != null){
			if (!getMetaDataMapModel().getLength().equals("0")) {
				if (!getMetaDataMapModel().getLength().equals("-1") ) {
					fieldLength = "maxlength=100";// + getMetaDataMapModel().getLength();
				} else {
					fieldLength = "maxlength=60";
				}
			}
		} else {
			fieldLength = "maxlength=60";
		}
		String fieldName = getMetaDataMapModel().getFieldName();
		StringBuffer fieldHtml = new StringBuffer();
		fieldHtml.append("<input type='text' onkeypress=\"if(event.keyCode==13||event.which==13){");
		if(quickSearch.equals("true")){
			fieldHtml.append(" quickSearch_").append(getMetaDataMapModel().getFieldName()).append("(this);");
		}
		fieldHtml.append(" return false;}\" ");
		fieldHtml.append(" name='").append(fieldName).append("' id='").append(fieldName).append("' ")
				 .append(fieldLength)
				 .append("' ")
				 .append( super.getHtmlInner());
//		if(inputLength!=null&&inputLength!=""){
//			fieldHtml.append(" style = 'width :").append(inputLength).append("px'");
//		}
			
		fieldHtml .append(" value=\"")
				 .append(Html.escape(value)).append("\" >");
		if(!isNull()){
			fieldHtml.append("<font color=red>*</font>");
		}
		if(configValue.equals("1")){//数据字典
			fieldHtml.append("<img src='../static/common/img/find.gif' align='absmiddle' style='margin-bottom: 10px;cursor:pointer' onclick=\"openDictionary('").append(dataSourceId).append("','")
			         .append(dataSourceName).append("','").append(getMetaDataMapModel().getId()).append("',false,'").append(getMetaDataMapModel().getFieldName()).append("');return false;\" border='1' title='动态数据选择器'>");
		}
		return fieldHtml.toString();
	}
	@Override
	public String getJavaScriptExtendEvents(){
		String configValue = "";
		String inputLength = "";
		String dataSourceId = "";
		String dataSourceName = "";
		String quickSearch = "";
		String inputTypeConfig = getMetaDataMapModel().getInputTypeConfig();
		if(inputTypeConfig != null && inputTypeConfig.trim().length() > 0){
			try{
				JSONObject jsonObject = JSONObject.fromObject(inputTypeConfig);
				if(jsonObject != null){
//					if(jsonObject.get("configValue") != null){
//						configValue = jsonObject.get("configValue").toString();
//					}
//					if(jsonObject.get("inputLength") != null){
//						inputLength = jsonObject.get("inputLength").toString();
//					}
					if(jsonObject.get("dataSourceId") != null){
						dataSourceId = jsonObject.get("dataSourceId").toString();
					}
					if(jsonObject.get("dataSourceName") != null){
						dataSourceName = jsonObject.get("dataSourceName").toString();
					}
					if(jsonObject.get("quickSearch") != null){
						quickSearch = jsonObject.get("quickSearch").toString();
					}
				}
			}catch(JSONException e){
				
			}
		}
		if(quickSearch.equals("true")){
			DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
			entity.setDatasourceId(dataSourceId);
			entity.setFormId(getFormId());
			entity.setFormItemId(getMetaDataMapModel().getId());
			if(diDatasourceFieldMapService == null){
				diDatasourceFieldMapService = SpringContextUtils.getBean(DiDatasourceFieldMapService.class);
			}
			List<DiDatasourceFieldMap> entities = diDatasourceFieldMapService.find(entity);

			StringBuffer dictionary = new StringBuffer();
			dictionary.append(" /*快速检索*/\n");
			dictionary.append(" function quickSearch_").append(getMetaDataMapModel().getFieldName()).append("(obj){\n");
			for(int i = 0 ; i < entities.size() ; i++){
				DiDatasourceFieldMap map = entities.get(i);
				if(map != null && !map.getTargetName().equals(getMetaDataMapModel().getFieldName())){
					dictionary.append("        		try{\n");
					dictionary.append("             		document.getElementById('").append(map.getTargetName()).append("').value = '';\n");
					dictionary.append("        		}catch(e){}\n");
					map.getTargetName();
				}
			}
			dictionary.append("  $.ajax({\n");
			dictionary.append("      type: 'POST',\n");
			dictionary.append("      async:true,\n");
			dictionary.append("      url: '/di/datasource/mapping_getQuickSearchData',\n");
			dictionary.append("      data: 'dataSourceId=").append(dataSourceId)
					.append("&formId=").append(getFormId())
					.append("&formItemId=").append(getMetaDataMapModel().getId())
					.append("&fieldName=").append(getMetaDataMapModel().getFieldName())
					.append("&condition=' + obj.value,\n");
			dictionary.append("	     success: function(msg){\n");
			dictionary.append("	       if(msg == null || msg.rows == null ||  msg.rows.length == 0){\n");
			dictionary.append("        		alert('没有检索到对应的数据，请重新输入!')\n");
			dictionary.append("        } else if(msg.rows.length == 1){\n");
			//回填处理
			for(int i = 0 ; i < entities.size() ; i++){
				DiDatasourceFieldMap map = entities.get(i);
				if(map != null){
					dictionary.append("        		try{\n");
					dictionary.append("             		document.getElementById('").append(map.getTargetName()).append("').value = msg.rows[0].").append(map.getName()).append(";\n");
					dictionary.append("        		}catch(e){}\n");
					map.getTargetName();
				}
			}
			dictionary.append("try{\n");
			dictionary.append(" extendCallBack(msg.rows[0]);\n");
			dictionary.append("}catch(e){}\n");

			dictionary.append("        } else {\n");
			dictionary.append("           openDictionary('").append(dataSourceId).append("','").append(dataSourceName).append("','").append(getMetaDataMapModel().getId()).append("','").append("false").append("','").append(getMetaDataMapModel().getFieldName()).append("');\n");
			dictionary.append("        }\n");
			dictionary.append("	     },\n");
			dictionary.append("       error : function(msg){\n");
			dictionary.append("	     }\n");
			dictionary.append("	 });\n");
			dictionary.append("	}");
			return dictionary.toString();
		}
		return "";
	}
	public String getSettingWeb(){
		StringBuffer settingHtml = new StringBuffer();
		String value = "";
		if(getMetaDataMapModel().getInputTypeConfig() != null && getMetaDataMapModel().getInputTypeConfig().trim().length() > 0){
			value = getMetaDataMapModel().getInputTypeConfig();
		}
		String configValue = "0";
		String inputLength = "";
		String dataSourceId = "";
		String dataSourceName = "";
		String quickSearch = "";
		if(value.trim().length() > 0){
			JSONObject jsonObject = JSONObject.fromObject(value);
			if(jsonObject != null){
				if(jsonObject.get("configValue") != null){
					configValue = jsonObject.get("configValue").toString();
				}
				if(jsonObject.get("inputLength") != null){
					inputLength = jsonObject.get("inputLength").toString();
				}
				if(jsonObject.get("dataSourceId") != null){
					dataSourceId = jsonObject.get("dataSourceId").toString();
				}
//				if(jsonObject.get("dataSourceName") != null){
//					dataSourceName = jsonObject.get("dataSourceName").toString();
//				}
				if(jsonObject.get("quickSearch") != null){
					quickSearch = jsonObject.get("quickSearch").toString();
				}
			}
		}
		settingHtml.append("<table  border=\"0\">\n");
		settingHtml.append("  <tr >\n");
		settingHtml.append("    <td onclick=\"visableTextFormUIComponentSetting('config');\">&nbsp;&nbsp;<span><input type=\"radio\" name=\"config\" ").append(configValue.equals("0") ? "checked" : "").append(" value='0'>常规</span></td>\n");
		settingHtml.append("    <td onclick=\"visableTextFormUIComponentSetting('config');\"><span><input type=\"radio\" name=\"config\" ").append(configValue.equals("1") ? "checked" : "").append(" value='1'>数据源</span></td>\n");
		settingHtml.append("    <td><span>&nbsp;</span></td>\n");
		settingHtml.append("  </tr>\n");
		settingHtml.append("  <tr>\n");
		settingHtml.append("    <td align='right'>录入长度:</td>\n");
		settingHtml.append("    <td colspan='2' valign=\"middle\">&nbsp;<input type='text' id='inputLength' name='inputLength' style='width:100px' data-rule='required;tableLength' value='").append(inputLength).append("'></td>\n");
		settingHtml.append("  </tr>\n");
		settingHtml.append("  <tr id='trSelectDataSource' ").append(configValue.equals("0") ? "style='display:none'" : "").append(">\n");
		settingHtml.append("    <td align='right'>数据源：</td>\n");
		settingHtml.append("    <td><input id='selectDataSource' style='width:150px'></input></td>\n");
		settingHtml.append("	<td><span style='cursor:pointer' onClick='openDataSourceMappingDialog();return false;'>&nbsp;<img src='../static/common/img/set.png' border='0' />&nbsp;配置映射</span></td>");
		settingHtml.append("  </tr>\n");
		settingHtml.append("  <tr id='trSelectDataSource' ").append(configValue.equals("0") ? "style='display:none'" : "").append(">\n");
		settingHtml.append("    <td align='right'>&nbsp;</td>\n");
		settingHtml.append("    <td colspan='2'><span><input type='checkbox' ").append(quickSearch.equals("true") ? "checked" : "").append(" id='quickSearch' name='quickSearch'>快速检索</span></td>\n");
		settingHtml.append("  </tr>\n");
		settingHtml.append("</table>\n");
		
		settingHtml.append("<script type='text/javascript'>\n");
		settingHtml.append(" document.getElementById('inputLength').focus();\n");
		settingHtml.append("$(function(){\n");
		settingHtml.append(" $('#selectDataSource').combogrid('setValue','").append(dataSourceId).append("');\n");
		settingHtml.append("});\n");
		settingHtml.append("function returnSettingConfig(){\n");
		settingHtml.append(" var configValue = getRadioValue('config');\n");
		settingHtml.append(" if(!checkNum('inputLength')){parent.Dialog.alert('提示','基本配置【录入长度】输入值不合法!');return;} else {\n");
		settingHtml.append("   if(configValue == 0){\n");
		settingHtml.append("		var jsonData = {\n");
		settingHtml.append("			configValue : configValue,\n");
		settingHtml.append("			inputLength : document.getElementById('inputLength').value\n");
		settingHtml.append("		};\n");
		settingHtml.append("		return JSON.stringify(jsonData);\n");
		settingHtml.append("    }else{\n");
		settingHtml.append("		var dataSource = $('#selectDataSource').combogrid('grid').datagrid('getSelected');\n");
		settingHtml.append("		var jsonData = {\n");
		settingHtml.append("			configValue : configValue,\n");
		settingHtml.append("			inputLength : document.getElementById('inputLength').value,\n");
		settingHtml.append("			dataSourceId : dataSource.id,\n");
		settingHtml.append("			dataSourceName : dataSource.name,\n");
		settingHtml.append("			quickSearch : document.getElementById('quickSearch').checked\n");
		settingHtml.append("		};\n");
		settingHtml.append("		return JSON.stringify(jsonData);\n");
		settingHtml.append("    }\n");
		settingHtml.append("  }\n");
		settingHtml.append("}\n");
		settingHtml.append("</script>\n");
		return settingHtml.toString();
	}
	@Override
	public String getTrueValue() {
		return getValue();
	}
	
	@Override
	public String getQueryHtmlDefine(Map<String,Object> params) {
		StringBuffer fieldHtml = new StringBuffer();
		fieldHtml.append("<input type = \"text\"  id=\"").append(getMetaDataMapModel().getFieldName()+"_Min").append("\" ></input> 至 ");
		fieldHtml.append("<input type = \"text\"  id=\"").append(getMetaDataMapModel().getFieldName()+"_Max").append("\" ></input>");
		return fieldHtml.toString();
	}
    

}

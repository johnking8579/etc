package com.jd.oa.form.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormSequence;

public interface FormSequenceService extends BaseService<FormSequence, String>{
	public static final String FORM_SEQUENCE_TYPE_PROCESS = "1";//流程定义
	public static final String FORM_SEQUENCE_TYPE_BUSINESS = "0";//业务定义
	/**
	 * 获取value
	 * @param typeId
	 * @param sequenceType
	 * @return
	 */
	public int getFormSequenceValue(String typeId,String sequenceType);
	
	public int getFormSequenceValue(String typeId);

	public String getProcessSequenceCode(String processDefinitionId);
}
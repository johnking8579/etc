package com.jd.oa.form.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Form implements Serializable{
	private static final long serialVersionUID = 1L;
    //表单ID
    private String id;
    //表单编码
    private String formCode;
    //表单名称
    private String formName;
    //表单类型，主表单：M 子表单：S
    private String formType;
    //创建类型
    private String createType;
    //对应的主表单
    private String parentId;
    //对应的业务表ID
    private String tableId;
    //表单描述
    private String formDesc;
    //逻辑删除标志位 0：有效 1：删除
    private int yn;
    //表单所有人，存储用户ID
    private String owner;
    //表单创建人
    private String creator;
    //表单创建时间
    private Date createTime;
    //表单修改人
    private String modifier;
    //表单修改时间
    private Date modifyTime;
    //所有人真实姓名
    private String ownerName;
    
    //复制表单时元表单的ID
    private String formId;
	private List<Form> subTables;

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    public String getFormCode() {
        return formCode;
    }


    public void setFormCode(String formCode) {
        this.formCode = formCode == null ? null : formCode.trim();
    }


    public String getFormName() {
        return formName;
    }


    public void setFormName(String formName) {
        this.formName = formName == null ? null : formName.trim();
    }


    public List<Form> getSubTables() {
		return subTables;
	}


	public void setSubTables(List<Form> subTables) {
		this.subTables = subTables;
	}


	public String getFormType() {
        return formType;
    }


    public void setFormType(String formType) {
        this.formType = formType == null ? null : formType.trim();
    }


    public String getFormDesc() {
        return formDesc;
    }


    public void setFormDesc(String formDesc) {
        this.formDesc = formDesc == null ? null : formDesc.trim();
    }


    public int getYn() {
        return yn;
    }


    public void setYn(int yn) {
        this.yn = yn;
    }


    public String getOwner() {
        return owner;
    }


    public void setOwner(String owner) {
        this.owner = owner == null ? null : owner.trim();
    }


    public String getCreator() {
        return creator;
    }


    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public String getModifier() {
        return modifier;
    }


    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }


    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

	public String getCreateType() {
		return createType;
	}

	public void setCreateType(String createType) {
		this.createType = createType;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	public String getFormId() {
		return formId;
	}
    
	public void setFormId(String formId) {
		this.formId = formId;
	}
}
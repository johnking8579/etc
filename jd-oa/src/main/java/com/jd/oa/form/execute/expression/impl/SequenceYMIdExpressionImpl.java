package com.jd.oa.form.execute.expression.impl;

import java.sql.Timestamp;
import java.util.Map;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.expression.ExpressionAbst;
import com.jd.oa.form.service.FormSequenceService;

public class SequenceYMIdExpressionImpl extends ExpressionAbst{

	public SequenceYMIdExpressionImpl(Map<String, Object> paramMaps,String expressionValue) {
		super(paramMaps, expressionValue);
		// TODO Auto-generated constructor stub
	}
	private FormSequenceService formSequenceService;
	public String expressionParse(String expression) {
		int sequenceNo = 0;
		if(formSequenceService == null){
			formSequenceService = SpringContextUtils.getBean("FormSequenceService");
		}
		try {
			sequenceNo = formSequenceService.getFormSequenceValue(expression);
		} catch (Exception e) {
		}
		return DateUtils.yearFormat(new Timestamp(System.currentTimeMillis())) + DateUtils.monthFormat(new Timestamp(System.currentTimeMillis())) + "-" + sequenceNo;
	}

}

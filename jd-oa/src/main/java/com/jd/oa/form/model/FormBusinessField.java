package com.jd.oa.form.model;

import java.util.Date;
import java.util.Map;

import com.jd.oa.common.model.IdModel;

public class FormBusinessField extends IdModel{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3019349273151413516L;
	//自定义表ID
	private String tableId;
	//字段名
    private String fieldName;
    //字段显示名称
    private String fieldChineseName;
    //数据类型（对应新增时字典数据项编码）
    private String dataType;
    //是否为空（先不设置）
    private String isNull;
    //长度
    private String length;

    //默认值（只有时间可以有默认值显示）
    private String defaultValue;
    //输入类型：0 input；1 select；2 checkbox；3 radiobox；4 textarea；5 dictpop；6 Date；7  File；8 compute
    private String inputType;
    //
    private String inputTypeConfig;
    //字典数据项编码（不是字典类别编码）
    private String dictTypeCode;
    //显示顺序
    private float sortNo;
    //逻辑删除 0:启用 1:删除
    private int yn;
    //创建人
    private String creator;
    //创建时间
    private Date createTime;
    //最后修改人
    private String modifier;
    //最后修改时间
    private Date modifyTime;

    //用于创建物理表时使用Begin
    //数据库表名
    private String tableName;
    //数据表名
    private String tableChineseName;
    //表类型：m 主表；s 子表
    private String tableType;
    //End
    
    private String isHidden;
    private String isEdit;
    
    private String htmlInner;
    
    private Map cascadeComBox;
    
        
    public String getHtmlInner() {
		return htmlInner;
	}

	public void setHtmlInner(String htmlInner) {
		this.htmlInner = htmlInner;
	}

	public String getIsHidden() {
		return isHidden;
	}

	public void setIsHidden(String isHidden) {
		this.isHidden = isHidden;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}
	
    public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}
	
    public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableChineseName() {
		return tableChineseName;
	}

	public void setTableChineseName(String tableChineseName) {
		this.tableChineseName = tableChineseName;
	}

	public String getInputTypeConfig() {
		return inputTypeConfig;
	}

	public void setInputTypeConfig(String inputTypeConfig) {
		this.inputTypeConfig = inputTypeConfig;
	}

	public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId == null ? null : tableId.trim();
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName == null ? null : fieldName.trim();
    }

    public String getFieldChineseName() {
		return fieldChineseName;
	}

	public void setFieldChineseName(String fieldChineseName) {
		this.fieldChineseName = fieldChineseName;
	}

	public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType == null ? null : dataType.trim();
    }

    public String getIsNull() {
        return isNull;
    }

    public void setIsNull(String isNull) {
        this.isNull = isNull == null ? null : isNull.trim();
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue == null ? null : defaultValue.trim();
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType == null ? null : inputType.trim();
    }

    public String getDictTypeCode() {
        return dictTypeCode;
    }

    public void setDictTypeCode(String dictTypeCode) {
        this.dictTypeCode = dictTypeCode == null ? null : dictTypeCode.trim();
    }

    public float getSortNo() {
		return sortNo;
	}

	public void setSortNo(float sortNo) {
		this.sortNo = sortNo;
	}

	public int getYn() {
        return yn;
    }

    public void setYn(int yn) {
        this.yn = yn;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getNull() {
        return isNull;
    }

    public void setNull(String aNull) {
        isNull = aNull;
    }

	public Map getCascadeComBox() {
		return cascadeComBox;
	}

	public void setCascadeComBox(Map cascadeComBox) {
		this.cascadeComBox = cascadeComBox;
	}
    
    
    

}
package com.jd.oa.dict.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.dict.model.DictType;

/** 
 * @Description: 数据字典类别Service
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */
public interface DictTypeService extends BaseService<DictType, String>{
	/**
	  * @Description: 获取父类别List
	  * @return List<DictType>
	  * @author guoqingfu
	  * @date 2013-8-27下午04:02:40 
	  * @version V1.0
	 */
	public List<DictType> findParentDictTypeList();
	/**
	  * @Description: 根据数据字典类别ID查询类别信息
	  * @param dicttypeId
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-8-27下午04:39:51 
	  * @version V1.0
	 */
	public DictType getDictType(String dicttypeId);
	/**
	  * @Description: 根据类别ID删除数据字典类别
	  * @param dicttypeId
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-27下午05:55:48 
	  * @version V1.0
	 */
	public Map<String,Object> deleteDictType(String dicttypeId);
	
	/**
	 * 根据父ID查找子类别列表
	 * @param parentId 父ID
	 * @return 子资源列表
	 */
	public List<DictType> findByParentId(String parentId);
	/**
	  * @Description: 根据类别编码查看类别是否存在
	  * @param dictTypeCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2上午09:54:32 
	  * @version V1.0
	 */
	public Map<String,Object> isExistDictTypeCodeOrName(DictType dictType);
	/**
	  * @Description: 根据类别实体查询类别信息（如可通过ID，也可通过Code等进行查询）
	  * @param dictType
	  * @return DictType
	  * @author guoqingfu
	  * @date 2013-9-4下午03:30:48 
	  * @version V1.0
	 */
	public DictType getDictTypeByModel(DictType dictType);
	
	/** 
	 * @author zhengbing 
	 * @desc 根据codes查询类别信息list
	 * @date 2014年7月8日 下午5:37:52
	 * @return List<DictType>
	 */
	public List<DictType> getDictTypeCodes(List<String> dictTypeCodes) ;
	/**
	 * 
	 * @author zhengbing 
	 * @desc 根据父id递归查询类别tree
	 * @date 2014年7月9日 下午7:23:10
	 * @return DictType
	 */
	public List<DictType> getDictTypeChildren(String parentId);
 	
}

package com.jd.oa.dict.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.dict.dao.DictDataDao;
import com.jd.oa.dict.model.DictData;
/**
 * @Description: 数据字典数据DAO实现类
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */
@Repository("dictDataDao")
public class DictDataDaoImpl extends MyBatisDaoImpl<DictData, String>
		implements DictDataDao {
	

	/**
	  * @Description: 根据数据项ID查询信息
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午01:49:54 
	  * @version V1.0
	 */
	public DictData getDictData(String dictDataId){
		DictData result = null;
        try {
            result = (DictData) this.getSqlSession().selectOne("com.jd.oa.dict.model.DictData.selectByPrimaryKey", dictDataId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	/**
	  * @Description: 查询父编号（新增数据项时使用）
	  * @param dictTypeId
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午04:19:11 
	  * @version V1.0
	 */
	public List<DictData> findParentDictDataList(String dictTypeId){
		List<DictData> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.findParentDictDataList",dictTypeId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	/**
	 * 
	 * @author zhengbing 
	 * @desc  根据字典类别编码和父id查询数据项
	 * @date 2014年7月9日 下午1:45:15
	 * @return List<DictData>
	 */
	public List<DictData> findDictDataListByCodeAndParentId(Map map){
		List<DictData> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.findDictDataListByCodeAndParentId",map);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	
	/**
	  * @Description: 根据字典类别编码查询数据项
	  * @param dictCode
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午07:51:46 
	  * @version V1.0
	 */
	public List<DictData> findDictDataList(String dictCode){
		List<DictData> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.findDictDataList",dictCode);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}

    @Override
    public List<DictData> findDictDataListByPid(String pid) {
        List<DictData> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.findDictDataListByPid",pid);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }

    /**
	  * @Description: 根据数据项ID查询数据项信息、父数据项、字典类别名称
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午08:58:27 
	  * @version V1.0
	 */
	public DictData getViewDictInfo(String dictDataId){
		DictData result = null;
        try {
            result = (DictData) this.getSqlSession().selectOne("com.jd.oa.dict.model.DictData.getViewDictInfo", dictDataId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
	}
	/**
	  * @Description: 逻辑删除数据项
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-31下午01:46:11 
	  * @version V1.0
	 */
	public boolean deleteDictData(Map map){
		 boolean flag = false;
	        try {
	            flag = this.getSqlSession().update("com.jd.oa.dict.model.DictData.deleteDictData", map) > 0 ? true : false;
	        } catch (DataAccessException e) {
	            flag = false;
	            throw e;
	        }
	        return flag;
	}
	/**
	  * @Description: 根据数据项编码查看数据是否存在
	  * @param dictDataCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2下午06:03:23 
	  * @version V1.0
	 */
	public boolean isExistDictDataCode(DictData dictData){
		 List<DictData> result = null;
	        try {
	            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.isExistDictDataCode",dictData);
	            if(result != null && result.size()>0){
	            	return true;
	            }
	        } catch (DataAccessException e) {
	            throw e;
	        }
	        return false;
	}
	
	/**
	  * @Description: 判断是否存在子数据或是已被表单引用（表单以后待开发）
	  * @param map
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-6上午10:43:55 
	  * @version V1.0
	 */
	public boolean isOwnSubDataOrForm(Map<String, Object> map){
		List<DictData> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.dict.model.DictData.isOwnSubDataOrForm",map);
            if(result != null && result.size()>0){
            	return true;
            }
        } catch (DataAccessException e) {
            throw e;
        }
        return false;
	}
}

package com.jd.oa.dict.service;

import java.util.List;
import java.util.Map;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.dict.model.DictData;

/** 
 * @Description: 数据字典数据Service
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0 
 */
public interface DictDataService extends BaseService<DictData, String>{
	/**
	  * @Description: 根据数据项ID查询信息
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午01:49:54 
	  * @version V1.0
	 */
	public DictData getDictData(String dictDataId);
	/**
	  * @Description: 查询父编号（新增数据项时使用）
	  * @param dictTypeId
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午04:19:11 
	  * @version V1.0
	 */
	public List<DictData> findParentDictDataList(String dictTypeId);
	/**
	  * @Description: 根据字典类别编码查询数据项
	  * @param dictCode
	  * @return List<DictData>
	  * @author guoqingfu
	  * @date 2013-8-30下午07:51:46 
	  * @version V1.0
	 */
	public List<DictData> findDictDataList(String dictCode);
    /**
     * @Description: 根据数据项编码查询子数据项列表
     * @param pid
     * @return List<DictData>
     * @author guoqingfu
     * @date 2013-8-30下午07:51:46
     * @version V1.0
     */
    public List<DictData> findDictDataListByPid(String pid);
	/**
	  * @Description: 根据数据项ID查询数据项信息、父数据项、字典类别名称
	  * @param dictDataId
	  * @return DictData
	  * @author guoqingfu
	  * @date 2013-8-30下午08:58:27 
	  * @version V1.0
	 */
	public DictData getViewDictInfo(String dictDataId);
	/**
	  * @Description: 逻辑删除数据项
	  * @param dictDataIds
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-8-31下午01:46:11 
	  * @version V1.0
	 */
	public Map<String,Object> deleteDictData(List<String> dictDataIds);
	/**
	  * @Description: 根据数据项编码查看数据是否存在
	  * @param dictDataCode
	  * @return boolean
	  * @author guoqingfu
	  * @date 2013-9-2下午06:03:23 
	  * @version V1.0
	 */
	public boolean isExistDictDataCode(DictData dictData);
	
	/**
	 * 
	 * @author zhengbing 
	 * @desc  根据字典类别编码和父id查询数据项
	 * @date 2014年7月9日 下午1:45:15
	 * @return List<DictData>
	 */
	public List<DictData> findDictDataListByCodeAndParentId(Map map);
}

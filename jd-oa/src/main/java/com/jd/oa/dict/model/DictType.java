package com.jd.oa.dict.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;
import java.util.List;

public class DictType extends IdModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6588215698776679579L;
	
	//父类别ID
    private String parentId;
    //父类别名称
    private String parentName;
    //类别编码
    private String dicttypeCode;
    //类别名称
    private String dicttypeName;
    //类别描述
    private String dicttypeDesc;
    //创建人
    private String creator;
    //创建时间
    private Date createTime;
    //修改人
    private String modifier;
    //修改时间
    private Date modifyTime;
    //是否有效0有效，1删除
    private int yn;
    //子节点集合  
    private List<DictType> children;
    
    public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public int getYn() {
		return yn;
	}

	public void setYn(int yn) {
		this.yn = yn;
	}

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public String getDicttypeCode() {
        return dicttypeCode;
    }

    public void setDicttypeCode(String dicttypeCode) {
        this.dicttypeCode = dicttypeCode == null ? null : dicttypeCode.trim();
    }

    public String getDicttypeName() {
        return dicttypeName;
    }

    public void setDicttypeName(String dicttypeName) {
        this.dicttypeName = dicttypeName == null ? null : dicttypeName.trim();
    }

    public String getDicttypeDesc() {
        return dicttypeDesc;
    }

    public void setDicttypeDesc(String dicttypeDesc) {
        this.dicttypeDesc = dicttypeDesc == null ? null : dicttypeDesc.trim();
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

	public List<DictType> getChildren() {
		return children;
	}

	public void setChildren(List<DictType> children) {
		this.children = children;
	}
    
    
}
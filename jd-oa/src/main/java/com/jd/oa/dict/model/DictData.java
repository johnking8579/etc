package com.jd.oa.dict.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;

public class DictData extends IdModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4046537957139043287L;
	
	//数据字典类别ID
    private String dictTypeId;
    //父数据ID
    private String parentId;
    //父数据名称
    private String parentName;
    //数据字典数据编码
    private String dictCode;
    //数据字典数据名称
    private String dictName;
    //序号
    private Integer sortNo;
    //创建人
    private String creator;
    //创建时间
    private Date createTime;
    //修改人
    private String modifier;
    //修改时间
    private Date modifyTime;
    //是否有效：0有效；1无效
    private int yn;
    //字典类别名称
    private String dictTypeName;
    //最后操作时间
    private String lastOperateTime;
    
    public String getLastOperateTime() {
		return lastOperateTime;
	}


	public void setLastOperateTime(String lastOperateTime) {
		this.lastOperateTime = lastOperateTime;
	}


	public String getDictTypeName() {
		return dictTypeName;
	}


	public void setDictTypeName(String dictTypeName) {
		this.dictTypeName = dictTypeName;
	}


	public String getParentName() {
		return parentName;
	}


	public void setParentName(String parentName) {
		this.parentName = parentName;
	}


	public String getDictTypeId() {
        return dictTypeId;
    }

    
    public void setDictTypeId(String dictTypeId) {
        this.dictTypeId = dictTypeId == null ? null : dictTypeId.trim();
    }

    
    public String getParentId() {
        return parentId;
    }

   
    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    
    public String getDictCode() {
        return dictCode;
    }

    
    public void setDictCode(String dictCode) {
        this.dictCode = dictCode == null ? null : dictCode.trim();
    }

    
    public String getDictName() {
        return dictName;
    }

   
    public void setDictName(String dictName) {
        this.dictName = dictName == null ? null : dictName.trim();
    }

    public int getYn() {
		return yn;
	}


	public void setYn(int yn) {
		this.yn = yn;
	}


	public Integer getSortNo() {
        return sortNo;
    }

    
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    
    public String getCreator() {
        return creator;
    }

    
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public String getModifier() {
        return modifier;
    }

    
    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    
    public Date getModifyTime() {
        return modifyTime;
    }

   
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
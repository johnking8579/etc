package com.jd.oa.dict.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.model.DictType;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.dict.service.DictTypeService;

/**
 * @Description: 数据字典数据Controller
 * @author guoqingfu
 * @date 2013-8-27 上午11:35:31 
 * @version V1.0
 */

@Controller
@RequestMapping(value="/dict")
public class DictDataController {
    private static final Logger logger = Logger.getLogger(DictDataController.class);

    @Autowired
    private DictDataService dictDataService;

    @Autowired
    private DictTypeService dictTypeService;

    /**
     * @Description: 根据数据字典类别编号，查询数据项列表
     * @param dictTypeCode
     * @return String
     * @author guoqingfu
     * @date 2013-8-30下午08:03:40 
     * @version V1.0
    */
   @RequestMapping(value = "/dictData_findDictDataList",method=RequestMethod.POST, produces = "application/json" )
   @ResponseBody
   public Object findDictDataList(String dictTypeCode) {
       List<DictData> dictDataList = null;
	   if(StringUtils.isEmpty(dictTypeCode) ){
	   	 	return dictDataList;
	    }
	   dictDataList = dictDataService.findDictDataList(dictTypeCode);
       String json=JsonUtils.toJsonByGoogle(dictDataList);
	   	return json;
   }
   
   @RequestMapping(value = "/dictData_findDictDataListByCodeAndParentId",method=RequestMethod.POST, produces = "application/json" )
   @ResponseBody
   public Object findDictDataListByCodeAndParentId(String dictTypeCode,String dictDataId) {
       List<DictData> dictDataList = null;
	   if(StringUtils.isEmpty(dictTypeCode)||StringUtils.isEmpty(dictDataId)){
	   	 	return dictDataList;
	    }
	   Map<String,String> map = new HashMap<String,String>();
	   map.put("dictTypeCode", dictTypeCode);
	   map.put("parentId", dictDataId);
	   dictDataList = dictDataService.findDictDataListByCodeAndParentId(map);
       String json=JsonUtils.toJsonByGoogle(dictDataList);
	   return json;
   }
//	public List<DictData> findDictDataListByCodeAndParentId(Map map){
    /**
     * @Description: 根据数据项编码查询子数据项
     * @param pid
     * @return boolean
     * @author guoqingfu
     * @date 2013-9-2下午06:02:31
     * @version V1.0
     */
    @RequestMapping(value="/dictData_findDictDataListByPid",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findDictDataListByPid(String pid){
        List<DictData> dictDataList = null;
        if(StringUtils.isEmpty(pid) ){
            return dictDataList;
        }
        dictDataList = dictDataService.findDictDataListByPid(pid);
        String json=JsonUtils.toJsonByGoogle(dictDataList);
        return json;
    }
   
    /**
      * @Description: 进入到查询数据字典数据页面
      * @return
      * @throws Exception ModelAndView
      * @author guoqingfu
      * @date 2013-8-30上午10:52:56 
      * @version V1.0
     */
    @RequestMapping(value = "/dictData_index", method = RequestMethod.GET)
    public ModelAndView index()
            throws Exception {
        ModelAndView mav = new ModelAndView("dict/dictdata_index");
        return mav;
    }

    /**
     * @Description: 进入到数据字典数据列表页面
     * @return
     * @throws Exception ModelAndView
     * @author guoqingfu
     * @date 2013-8-30上午10:52:56
     * @version V1.0
     */
    @RequestMapping(value = "/dictData_list", method = RequestMethod.GET)
    public ModelAndView dictDataList()
            throws Exception {
        ModelAndView mav = new ModelAndView("dict/dictdata_list");
        return mav;
    }

    /**
      * @Description: 查询数据字典数据列表
      * @param request
      * @return Object
      * @author guoqingfu
      * @date 2013-8-30上午11:39:39 
      * @version V1.0
     */
    @RequestMapping(value = "/dictData_findDictDataPage", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public Object findDictDataPage(HttpServletRequest request) throws Exception{
    	//创建pageWrapper
		PageWrapper<DictData> pageWrapper=new PageWrapper<DictData>(request);
		try{
			//添加搜索条件
			String dictDataName = request.getParameter("dictDataName");
			String dictTypeId = request.getParameter("dictTypeId");
			
			if(StringUtils.isNotEmpty(dictDataName)){
				pageWrapper.addSearch("dictDataName",dictDataName);
			}
			if(StringUtils.isNotEmpty(dictTypeId)){
				pageWrapper.addSearch("dictTypeId",dictTypeId);
			}
			
			dictDataService.find(pageWrapper.getPageBean(),"findByPage",pageWrapper.getConditionsMap());
		    //返回到页面的数据
			String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
	        logger.debug(json);
	        return json;
		}catch(Exception e){
			logger.error(e);
			throw new BusinessException ("查询数据字典数据列表失败",e);
		}
    }
  
    /**
      * @Description: 进入数据字典数据项新增页面
      * @param model
      * @param dictTypeId
      * @return String
      * @author guoqingfu
      * @date 2013-8-30下午04:05:10 
      * @version V1.0
     */
    @RequestMapping(value = "/dictData_add",method=RequestMethod.GET)
    public String addDictData(Model model,String dictTypeId) {
    	//用于新增页面显示字典类别
        DictType dictType = dictTypeService.getDictType(dictTypeId);
        //显示父编号
        List<DictData> parentDictDatalist = dictDataService.findParentDictDataList(dictTypeId);
       
        model.addAttribute("dictType", dictType);
        model.addAttribute("parentDictDatalist", parentDictDatalist);
        return "dict/dictdata_add";
    }
    
   /**
     * @Description: 保存新增数据项
     * @param dictData
     * @param dictTypeId
     * @return String
     * @author guoqingfu
     * @date 2013-8-30下午05:13:26 
     * @version V1.0
    */
    @RequestMapping(value="/dictData_addSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveAddDictData(DictData dictData,String dictTypeId){
        Map<String,Object> map = new HashMap<String,Object>();
        try{
        	//获取操作人姓名
            String userName = ComUtils.getLoginName();
            dictData.setDictTypeId(dictTypeId);
            dictData.setCreator(userName);
            dictData.setCreateTime(new Date());
            dictData.setModifier(userName);
            dictData.setModifyTime(new Date());
            dictData.setYn(0);   //有效
            String primaryKey = dictDataService.insert(dictData);
            if(StringUtils.isNotEmpty(primaryKey)){
    	 		map.put("operator", true);
    	        map.put("message", "添加成功");
    	 	}else{
    	 		map.put("operator", false);
    	        map.put("message", "添加失败");
    	 	}

            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存新增数据字典数据失败",e);
        }
    }
    /**
      * @Description: 查看数据项信息
      * @param dictDataId
      * @param model
      * @return String
      * @author guoqingfu
      * @date 2013-8-30下午09:10:53 
      * @version V1.0
     */
    @RequestMapping(value = "/dictData_view",method=RequestMethod.GET)
    public String viewDictData(String dictDataId, Model model) {
    	//根据数据项ID查询数据项信息、父数据项、字典类别名称
        DictData dictData = dictDataService.getViewDictInfo(dictDataId);
        model.addAttribute("dictData", dictData);
    	return "dict/dictdata_view" ;
    }
    /**
      * @Description: 进入修改数据项信息页面
      * @param dictDataId
      * @param model
      * @return String
      * @author guoqingfu
      * @date 2013-8-31下午01:13:10 
      * @version V1.0
     */
    @RequestMapping(value = "/dictData_update",method=RequestMethod.GET)
    public String updateDictData(String dictDataId, Model model) {
    	//根据数据项ID查询数据项信息、父数据项、字典类别名称
        DictData dictData = dictDataService.getViewDictInfo(dictDataId);
        //显示父编号
        List<DictData> parentDictDatalist = dictDataService.findParentDictDataList(dictData.getDictTypeId());
        
        model.addAttribute("dictData", dictData);
        model.addAttribute("parentDictDatalist", parentDictDatalist);
    	return "dict/dictdata_update" ;
    }
    /**
      * @Description: 保存修改的数据项信息
      * @param dictData
      * @return String
      * @author guoqingfu
      * @date 2013-8-31下午01:27:49 
      * @version V1.0
     */
    @RequestMapping(value="/dictData_updateSave" ,method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String saveUpdateDictData(DictData dictData){
        Map<String,Object> map = new HashMap<String,Object>();

        try{
        	//获取操作人姓名
            String userName = ComUtils.getLoginName();
            dictData.setModifier(userName);
            dictData.setModifyTime(new Date());

            int i = dictDataService.update(dictData);
            if(i>0){
            	map.put("operator", true);
     	        map.put("message", "修改成功");
            }else{
            	map.put("operator", false);
     	        map.put("message", "修改失败");
            }

            return JSONObject.fromObject(map).toString();
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("保存修改数据字典数据失败",e);
        }
    }
    
    @RequestMapping(value="/dictData_delete",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String deleteDictData(String ids,Integer length){
        List<String> dictDataIds = new ArrayList<String>();
        if(null == length || length.intValue() == 1){
            dictDataIds.add(ids);
        }
        else{
			String[] batchIds = ids.split(",");
			CollectionUtils.addAll(dictDataIds, batchIds);
        }
        Map<String,Object> map = dictDataService.deleteDictData(dictDataIds);

        return JSONObject.fromObject(map).toString();
    }
    /**
      * @Description: 根据数据项编码查看数据是否存在
      * @param dictDataCode
      * @return boolean
      * @author guoqingfu
      * @date 2013-9-2下午06:02:31 
      * @version V1.0
     */
    @RequestMapping(value="/dictData_isExistDictDataCode",method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean isExistDictDataCode(DictData dictData){
        boolean flag = dictDataService.isExistDictDataCode(dictData);
        return flag;
    }
}

package com.jd.oa.doc.service;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.system.model.SysAuthExpression;

/**
 * 文档管理Service接口
 * User: yaohaibin
 * Date: 13-8-28
 * Time: 下午4:49
 * To change this template use File | Settings | File Templates.
 */
public interface WfFileService extends BaseService<WfFile, Long>{

    /**
     * 新增文件
     * @param wfFile 文件对象
     * @param file 上传文件对象
     * @return 文件对象
     */
    public WfFile insertFile(WfFile wfFile,MultipartFile file);

    /**
     * 修改文件
     * @param wfFile 文件对象
     * @param file 上传文件对象
     * @return 文件对象
     */
    public WfFile updateByPrimaryKey(WfFile wfFile, MultipartFile file);

    /**
     * 根据主键删除文件（逻辑删除）
     * @param wfFileList 文件对象列表
     */
    public void deleteFileByPrimaryKey(List<WfFile> wfFileList);

    /**
     * 根据主键删除文件（物理删除）
     * @param wfFile 文件对象
     */
    public void deleteByPrimaryKey(WfFile wfFile);

    /**
     * 根据文件ID查询文件夹
     * @param wfFile 文件对象
     * @return 文件对象
     */
    public WfFile selectByPrimaryKey(WfFile wfFile);

    /**
     * 获取同一桶下相同文件的数量
     * @param wfFile 文件对象
     * @return 相同文件数量
     */
    public Integer findFileCount(WfFile wfFile);

    /**
     * 下载文档
     */
    public ResponseEntity<byte[]> fileDownload(WfFile wffile,HttpHeaders headers);

    /**
     * 下载文档
     */
    public byte[] downloadFile(WfFile wffile);

    /**
     * 获取相同文件夹下同名的文档数量
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     * @return  相同文档数量
     */
     public Integer getFileCount(String ids, String folderId);

    /**
     * 移动文档
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     */
    public void fileMoveSave(String ids, String folderId);
     /**
      * 获取文件的权限表达式名称
      * @param	ids
      * @return 权限表达式list
      */
     public String findExpression(String ids);
     
     /**
      * 获取文件的权限表达式
      * @param	map
      * @return 权限表达式list
      */
     public List<SysAuthExpression> findBusinessExpression(Map map);
}

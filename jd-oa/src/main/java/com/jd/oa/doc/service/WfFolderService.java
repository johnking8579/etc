package com.jd.oa.doc.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.doc.model.WfFolder;

import java.util.List;

/**
 * 文件夹管理Service接口
 * User: yaohaibin
 * Date: 13-8-28
 * Time: 下午4:49
 * To change this template use File | Settings | File Templates.
 */
public interface WfFolderService extends BaseService<WfFolder, String> {

    /**
     * 文件夹树数据加载
     * @param id 文件夹ID
     * @return 文件夹列表
     */
    public List<WfFolder> selectFolderByParentId(String id);

    /**
     * 获取同一节点下同名文件夹数量
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    public Integer findFolderCount(WfFolder wfFolder);

    /**
     * 创建文件夹
     * @param wfFolder 文件夹对象
     * @return 文件夹对象
     * @throws RuntimeException
     */
    public WfFolder folderInsert(WfFolder wfFolder);

    /**
     * 修改文件夹
     * @param folder 文件夹对象
     * @return 文件夹对象
     * @throws RuntimeException
     */
    public WfFolder updateByPrimaryKey(WfFolder folder);

    /**
     * 删除文件夹(逻辑删除)
     * @param folder 文件夹对象
     * @throws RuntimeException
     */
    public void deleteFolderById(WfFolder folder);

    /**
     * 文件夹查看页面
     * @param id 文件夹ID
     */
    public WfFolder selectByPrimaryKey(String id);
    /**
     * 获取文件夹下的子文件夹和文件的数量
     * @param folder
     * @return
     */
    public Integer getChildCount(WfFolder folder);
}

package com.jd.oa.doc.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.doc.model.WfFolder;

import java.util.List;
/**
 * 文件夹管理Dao接口
 * @version 1.0
 * @author yaohaibin
 */
public interface WfFolderDao extends BaseDao<WfFolder, String>{

    /**
     * 文件夹树数据加载
     * @param id 文件夹ID
     * @return 文件夹列表
     */
    public List<WfFolder> selectFolderByParentId(String id);

    /**
     * 获取同一节点下同名文件夹数量
     * @param wfFolder 文件夹对象
     * @return  文件夹对象
     */
    public Integer findFolderCount(WfFolder wfFolder);

    /**
     * 创建文件夹
     * @param wfFolder 文件夹对象
     */
    public void folderInsert(WfFolder wfFolder);

    /**
     * 修改文件夹
     * @param wfFolder 文件夹对象
     */
	public void updateByPrimaryKey(WfFolder wfFolder);

    /**
     * 删除文件夹(逻辑删除)
     * @param folder 文件夹对象
     */
    public void deleteFolderById(WfFolder folder);

    /**
     * 删除文件夹(物理删除)
     * @param id 文件夹主键ID
     */
    public void deleteByPrimaryKey(String id);

    /**
     * 文件夹查看页面
     * @param id 文件夹ID
     */
    public WfFolder selectByPrimaryKey(String id);

}

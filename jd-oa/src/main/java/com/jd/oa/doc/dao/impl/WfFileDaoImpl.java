package com.jd.oa.doc.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.doc.dao.WfFileDao;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.system.model.SysAuthExpression;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
/**
 * 文档管理DAO实现类
 * @author yaohaibin
 * @version 1.0
 */
@Repository("wfFileDao")
public class WfFileDaoImpl extends MyBatisDaoImpl<WfFile, Long> implements WfFileDao {

    /**
     * 新增文件
     * @param wfFile 文件对象
     */
    @Override
    public void insertFile(WfFile wfFile) {
        this.getSqlSession().insert("com.jd.oa.doc.model.WfFile.insert", wfFile);
    }

    /**
     * 更新文件信息
     * @param wfFile 文件对象
     */
    @Override
    public void updateByPrimaryKey(WfFile wfFile) {
        this.getSqlSession().update("com.jd.oa.doc.model.WfFile.updateByPrimaryKey", wfFile);
    }

    /**
     * 根据主键删除文件（逻辑删除）
     * @param wfFile 文件对象
     */
    @Override
    public void deleteFileByPrimaryKey(WfFile wfFile) {
        this.getSqlSession().update("com.jd.oa.doc.model.WfFile.deleteFileByPrimaryKey", wfFile);
    }

    /**
     * 根据主键删除文件（物理删除）
     * @param wfFile 文件对象
     */
    @Override
    public void deleteByPrimaryKey(WfFile wfFile) {
        this.getSqlSession().update("com.jd.oa.doc.model.WfFile.deleteByPrimaryKey", wfFile.getId());
    }

	 /**
     * 根据文件ID查询文件夹
     * @param wfFile 文件对象
     * @return 文件对象
     */
	@Override
	public WfFile selectByPrimaryKey(WfFile wfFile) {
		return this.getSqlSession().selectOne("com.jd.oa.doc.model.WfFile.selectByPrimaryKey", wfFile.getId());
	}

    /**
     * 获取相同文件夹下同名的文档数量
     * @param wfFile 文件对象
     * @return 相同文件数量
     */
    public Integer findFileCount(WfFile wfFile) {
        return this.getSqlSession().selectOne("com.jd.oa.doc.model.WfFile.findFileCount", wfFile);
    }
    /**
     * 获取文件的权限表达式
     * @param	权限表达式
     * @return 权限表达式list
     */
	public List<SysAuthExpression> findBusinessExpression(Map map) {
		return this.getSqlSession().selectList("com.jd.oa.doc.model.WfFile.findBusinessExpression", map);
	}
	/**
     * 获取文件的权限表达式名称
     * @param	权限表达式ID
     * @return 权限表达式list
     */
	public List<SysAuthExpression> findExpression(Map map) {
		return this.getSqlSession().selectList("com.jd.oa.doc.model.WfFile.findExpression", map);
	}
}

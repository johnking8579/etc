package com.jd.oa.doc.controller;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.doc.service.WfFileService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文档管理
 * @author yaohaibin
 * @version 1.0
 * @date 2013-8-27
 */
@Controller
@RequestMapping(value="/doc")
public class WfFileController {

	private static final Logger logger = Logger.getLogger(WfFileController.class);

    @Autowired
    private WfFileService wfFileService;
    @Autowired
    private DictDataService dictDataService;
	@Autowired
	private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
    /**
     * 文档管理首页
     */
	@RequestMapping(value = "/wffile_index", method = RequestMethod.GET)
    public ModelAndView index(){	
		ModelAndView mav = new ModelAndView("doc/file/wffile_index");
		return mav;
    }

    /**
     * 文档管理首页-加载文档类型列表
     * @param dictTypeCode 文档类型代码
     * @return 文档类型列表
     */
    @RequestMapping(value = "/wffile_loadDocType", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<DictData> loadDocType(String dictTypeCode){
        return dictDataService.findDictDataList(dictTypeCode);
    }

    /**
     * 文档列表查询
     * @param request HttpServletRequest
     * @param wfFile 文档对象
     * @return 页面数据
     */
    @RequestMapping(value = "/wffile_findFileList", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Map<String, Object> findWfFileList(HttpServletRequest request,WfFile wfFile){
        //创建pageWrapper
        PageWrapper<WfFile> pageWrapper = new PageWrapper<WfFile>(request);
        //添加搜索条件
        pageWrapper.addSearch("folderId", wfFile.getFolderId());
        pageWrapper.addSearch("fileName",wfFile.getFileName());
        pageWrapper.addSearch("fileSuffix",wfFile.getFileSuffix());
        //后台取值
        try{
            wfFileService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
        }catch(Exception e){
            logger.error(e);
            throw new BusinessException("文档列表查询失败!",e);
        }
        return pageWrapper.getResult();
    }

    /**
     * 修改文档页面
     * @param wfFile 文档对象
     */
    @RequestMapping(value = "wffile_update", method = RequestMethod.GET)
    public ModelAndView updateWfFile(WfFile wfFile){
        wfFile = wfFileService.selectByPrimaryKey(wfFile);
        ModelAndView mav = new ModelAndView("doc/file/wffile_update");
        mav.addObject("wfFile",wfFile);
        return mav;
    }
    /**
     * 添加文档页面
     */
    @RequestMapping(value = "/wffile_add", method = RequestMethod.GET)
    public ModelAndView addWfFile(){
        return new ModelAndView("doc/file/wffile_add");
    }

    /**
     * 获取同一文件夹下相同文档的数量
     * @param wfFile 文档对象
     * @return  相同文档数量
     */
    @RequestMapping(value="/wffile_getFileCount", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Integer getWfFileCount(WfFile wfFile){
        int i=0;
        try{
           i= wfFileService.findFileCount(wfFile);
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("获取文档数量失败!",e);
        }
        return i;
    }

    /**
     * 新建文档
     * @param wfFile 文档对象
     * @param file 上传文档对象
     */
    @RequestMapping(value = "/wffile_addSave", method = RequestMethod.POST)
    public ModelAndView addSaveWfFile(WfFile wfFile,@RequestParam(value="file", required=false) MultipartFile file){
        if(null!=file){
            if(file.getSize()==0) {
                throw  new BusinessException("选择的文件不存在,请确认文件名称或文件路径。");
            }
        }
        wfFile = wfFileService.insertFile(wfFile,file);
        ModelAndView mav = new ModelAndView("doc/file/wffile_index");
        mav.addObject("wfFile",wfFile);
        return mav;
    }

    /**
     * 修改文档
     * @param wfFile 文档对象
     * @param file 上传文档对象
     */
	@RequestMapping(value = "/wffile_updateSave", method = RequestMethod.POST)
	@ResponseBody
	public Object updateSaveWfFile(WfFile wfFile, @RequestParam(value="file", required=false) MultipartFile file){
        wfFile = wfFileService.updateByPrimaryKey(wfFile,file);
        ModelAndView mav = new ModelAndView("doc/file/wffile_index");
        mav.addObject("wfFile",wfFile);
        return mav;
	}

    /**
     * 删除文档(单个删除与批量删除共用一个方法)
     * @param id 文档对象
     */
	@RequestMapping(value = "/wffile_delete", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public void deleteWfFile(String id){
        List<WfFile> wfFileList = JsonUtils.fromJsonByGoogle(id, new TypeToken<List<WfFile>>() {});
        wfFileService.deleteFileByPrimaryKey(wfFileList);
	}

	/**
	 * 查看文档详细信息
	 * @param wfFile 文档对象
	 */
	@RequestMapping(value = "/wffile_view", method = RequestMethod.GET)
    @ResponseBody
	public ModelAndView viewWfFile(WfFile wfFile){
        wfFile = wfFileService.selectByPrimaryKey(wfFile);
		ModelAndView mav = new ModelAndView("doc/file/wffile_view");
		mav.addObject("wfFile", wfFile);
		return mav;
	}

    /**
     * 下载-springMVC方式
     * @param wfFile 文件对象
     */
    @RequestMapping(value = "/download_file")
    public ResponseEntity<byte[]> download(WfFile wfFile){
        wfFile = wfFileService.selectByPrimaryKey(wfFile);
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDispositionFormData("attachment", new String(wfFile.getFileName().getBytes("gbk"),"iso-8859-1")+"."+wfFile.getFileSuffix());
            return wfFileService.fileDownload(wfFile,headers);
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("下载文件失败!",e);
        }

    }

    /**
     * 下载-response方式
     * @param wfFile 文件对象
     */
    @RequestMapping(value = "/wffile_download")
    public void downloadFile(WfFile wfFile, final HttpServletResponse response){
        try {
            wfFile = wfFileService.selectByPrimaryKey(wfFile);
            byte[] data = wfFileService.downloadFile(wfFile);
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(wfFile.getFileName().getBytes("gbk"),"iso-8859-1")+"."+wfFile.getFileSuffix() + "\"");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream;charset=UTF-8");
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);

            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("下载文件失败!",e);
        }

    }

    /**
     * 移动文件夹
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     */
    @RequestMapping(value = "/wffile_moveSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void saveMoveWfFile(String ids, String folderId){
        wfFileService.fileMoveSave(ids,folderId);
    }

    /**
     * 获取相同文件夹下同名的文档数量
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     * @return  相同文档数量
     */
    @RequestMapping(value="/wffile_getFileNumbers", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Integer getWfFileNumbers(String ids, String folderId){
        return wfFileService.getFileCount(ids,folderId);
    }
    
    /**
     * 文档权限共享
     * @param id 权限表达式对象集合
     * @return  
     */
	@RequestMapping(value="/wffile_shared", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void shared(String id){
        try{
            //不传权限为取消权限
            //转换成SysBussinessAuthExpression List
        	List<SysBussinessAuthExpression> bussinessList = JsonUtils.fromJsonByGoogle(id, new TypeToken<List<SysBussinessAuthExpression>>() {});
        	sysBussinessAuthExpressionService.updateByResources(bussinessList);
        } catch (Exception e){
            logger.error(e);
            throw new BusinessException("权限分配失败!",e);
        }
    }
	/**
     * 获取表达式的名称
     * @param ids 权限表达式ID集合
     * @return  
     */
	@RequestMapping(value="/wffile_getSysBussinessName", method = RequestMethod.POST)
    @ResponseBody
    public Object getSysBussinessName(String ids){
        	String bussinessName=wfFileService.findExpression(ids);
        	Map<String, String>  map = new HashMap<String, String>();
        	map.put("bussinessName", bussinessName);
        	return map;
    }
	/**
     * 获取文件的权限表达式
     * @param fileId 权限表达式类型和资源ID
     * @return 权限表达式list
     */
	@RequestMapping(value="/wffile_findBusinessExpression", method = RequestMethod.POST)
    @ResponseBody
    public List<SysAuthExpression> findBusinessExpression(String fileId){
			Map<String, String> map = new HashMap<String, String>();
	    	map.put("fileId", fileId);
	    	map.put("busType", "1");
	    	List<SysAuthExpression> list=wfFileService.findBusinessExpression(map);
        	return list;
    }
    /**
     * 删除权限表达式
     * @param fileId 资源ID
     * @return 权限表达式list
     */
    @RequestMapping(value="/wffile_DeleteBusinessExpression", method = RequestMethod.POST)
    @ResponseBody
    public void DeleteBusinessExpression(String fileId){
        try {
            List<SysBussinessAuthExpression> bussinessList = JsonUtils.fromJsonByGoogle(fileId, new TypeToken<List<SysBussinessAuthExpression>>() {});
            for (SysBussinessAuthExpression authExpression:bussinessList) {
                sysBussinessAuthExpressionService.deleteByBusinessId(authExpression.getId());
            }
        }catch (Exception e) {
          logger.error(e);
            throw new BusinessException("删除权限表达式失败!",e);
        }
    }

}

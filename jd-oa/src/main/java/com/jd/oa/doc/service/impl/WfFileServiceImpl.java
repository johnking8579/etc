package com.jd.oa.doc.service.impl;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.doc.dao.WfFileDao;
import com.jd.oa.doc.dao.WfFolderDao;
import com.jd.oa.doc.model.WfFile;
import com.jd.oa.doc.model.WfFolder;
import com.jd.oa.doc.service.WfFileService;
import com.jd.oa.system.dao.SysBussinessAuthExpressionDao;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

/**
 * 文档管理Service实现类
 * User: yaohaibin
 * Date: 13-8-28
 * Time: 下午4:49
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class WfFileServiceImpl extends BaseServiceImpl<WfFile, Long> implements WfFileService {

	private static final Logger logger = Logger.getLogger(WfFileServiceImpl.class);

	@Autowired
	private WfFileDao wfFileDao;
	@Autowired
	private WfFolderDao wfFolderDao;
	@Autowired
	private JssService jssService;
    @Autowired
    private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
	//权限表达式
	@Autowired
	private SysBussinessAuthExpressionDao sysBussinessAuthExpressionDao;
    /**
     * 新增文件
     * @param wfFile 文件对象
     * @return 文件对象
     */
	@Override
	public WfFile insertFile(WfFile wfFile,MultipartFile file){
        String fileId = IdUtils.uuid2();
        WfFolder wfFolder = wfFolderDao.selectByPrimaryKey(wfFile.getFolderId());
        String bucket = wfFolder.getBucket();                                                   //桶
        if(wfFile.getFileType()!=null && wfFile.getFileType().equals("0")){
            //上传方式
            String fileName = file.getOriginalFilename();
            String fileKey = fileId + "." + StringUtils.substringAfterLast(fileName,".");
            long fileSize = 0;
            try {
                fileSize = (long)file.getBytes().length;
                jssService.uploadFile(bucket,fileKey, file.getInputStream());                    //上传文件到云端
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("上传文件失败！",e);
            }
            wfFile.setFileKey(fileKey);                                                           //文件KEY
            wfFile.setFileName(StringUtils.substringBeforeLast(fileName, "."));                   //文件名
            wfFile.setFileSuffix(StringUtils.substringAfterLast(fileName,"."));                   //扩展名
            wfFile.setFileSize(fileSize);                                                         //文件尺寸
        }else{
            //富文本编辑器方式
            String fileName = wfFile.getFileName()+".html";
            String fileKey = fileId + "." + StringUtils.substringAfterLast(fileName,".");
            String fileContent = wfFile.getFileContent();
            long fileSize = 0;
            InputStream inputStringStream = null;
            try {
                fileSize = fileContent.getBytes("GBK").length;
                inputStringStream = new ByteArrayInputStream(fileContent.getBytes("GBK"));
                jssService.uploadFile(bucket, fileKey, inputStringStream);                               //上传文本到云端
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("上传文件失败！",e);
            } finally {
	        	try{
	        		if(inputStringStream!=null){
	        			inputStringStream.close();
	        		}
	        	} catch(IOException e) {
	        	}
	        }

            wfFile.setFileKey(fileKey);                                                        //文件KEY
            wfFile.setFileName(StringUtils.substringBeforeLast(fileName, "."));                //文件名
            wfFile.setFileSuffix(StringUtils.substringAfterLast(fileName,"."));                //扩展名
            wfFile.setFileSize(fileSize);                                                      //文件尺寸
        }
        wfFile.setId(fileId);                                                                  //文件ID
        wfFile.setBucket(bucket);                                                              //桶名
        String loginName=ComUtils.getLoginName();                                              //登陆人
        wfFile.setCreator(loginName);                                                            //创建人
        wfFile.setCreateTime(new Date());                                                      //创建时间
        wfFile.setModifier(loginName);                                                           //修改人
        wfFile.setModifyTime(new Date());                                                      //修改时间
        try{
           wfFileDao.insert(wfFile);
           //权限入库
          if(StringUtils.isNotEmpty(wfFile.getAuthExpressionId())){
        	  List<SysBussinessAuthExpression> bussinessList = JsonUtils.fromJsonByGoogle(wfFile.getAuthExpressionId(), new TypeToken<List<SysBussinessAuthExpression>>() {});
              for(SysBussinessAuthExpression bussiness:bussinessList){
            	  bussiness.setBusinessType("1");
            	  bussiness.setBusinessId(fileId);
            	  bussiness.setCreateTime(new Date());
            	  bussiness.setCreator(loginName);
            	  sysBussinessAuthExpressionDao.insert(bussiness);
              }
          }       
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("创建文档失败!",e);
        }
		return wfFile;
	}

    /**
     * 更新文件信息
     * @param wfFile 文件对象
     * @return 文件对象
     */
    @Override
    public WfFile updateByPrimaryKey(WfFile wfFile, MultipartFile file){

        WfFolder wfFolder = wfFolderDao.selectByPrimaryKey(wfFile.getFolderId());
        String bucket = wfFolder.getBucket();                                                   //桶
        if(wfFile.getFileType()!=null && wfFile.getFileType().equals("0")){
            if(!file.isEmpty()){   //没有上传文件时修改基本信息
                WfFile oldWfFile = wfFileDao.selectByPrimaryKey(wfFile);
                jssService.deleteObject(oldWfFile.getBucket(),oldWfFile.getFileKey());     //删除云端存储的文件
                //上传方式
                String fileName = file.getOriginalFilename();
                String fileKey = wfFile.getId() + "." + StringUtils.substringAfterLast(fileName,".");
                long fileSize = 0;
                try {
                    fileSize = (long)file.getBytes().length;
                    jssService.uploadFile(bucket,fileKey, file.getInputStream());                    //上传文件到云端
                } catch (Exception e) {
                    logger.error(e);
                    throw new BusinessException("上传文件失败！",e);
                }
                wfFile.setFileKey(fileKey);                                                           //文件KEY
                wfFile.setFileName(StringUtils.substringBeforeLast(fileName, "."));                   //文件名
                wfFile.setFileSuffix(StringUtils.substringAfterLast(fileName,"."));                   //扩展名
                wfFile.setFileSize(fileSize);                                                         //文件尺寸
            }
        }else{
            WfFile oldWfFile = wfFileDao.selectByPrimaryKey(wfFile);
            jssService.deleteObject(oldWfFile.getBucket(),oldWfFile.getFileKey());     //删除云端存储的文件
            //富文本编辑器方式
            String fileName = wfFile.getFileName()+".html";
            String fileKey = wfFile.getId() + "." + StringUtils.substringAfterLast(fileName,".");
            String fileContent = wfFile.getFileContent();
            long fileSize = 0;
            InputStream inputStringStream = null;
            try {
                fileSize = fileContent.getBytes("GBK").length;
                inputStringStream = new ByteArrayInputStream(fileContent.getBytes("GBK"));
                jssService.uploadFile(bucket, fileKey, inputStringStream);                               //上传文本到云端
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("上传文件失败！",e);
            } finally {
	        	try{
	        		if(inputStringStream!=null){
	        			inputStringStream.close();
	        		}
	        	} catch(IOException e) {
	        	}
	        }

            wfFile.setFileKey(fileKey);                                                        //文件KEY
            wfFile.setFileName(StringUtils.substringBeforeLast(fileName, "."));                //文件名
            wfFile.setFileSuffix(StringUtils.substringAfterLast(fileName,"."));                //扩展名
            wfFile.setFileSize(fileSize);                                                      //文件尺寸
        }

        wfFile.setFolderName(wfFolder.getFolderName());                                        //文件夹名称
        wfFile.setBucket(bucket);                                                              //桶名
        wfFile.setModifier(ComUtils.getLoginName());                                           //修改人
        wfFile.setModifyTime(new Date());                                                      //修改时间
        try {
            wfFileDao.updateByPrimaryKey(wfFile);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("更新文档失败！",e);
        }
        return wfFile;
    }

    /**
     * 根据主键删除文件（逻辑删除）
     * @param wfFileList 文件对象列表
     */
    @Override
	public void deleteFileByPrimaryKey(List<WfFile> wfFileList) {
        for(WfFile wfFile : wfFileList){
            wfFile = wfFileDao.selectByPrimaryKey(wfFile);
            try {
                jssService.deleteObject(wfFile.getBucket(),wfFile.getFileKey());          //删除云端存储的文件
                //登录人
                wfFile.setModifier(ComUtils.getLoginName());
                wfFile.setModifyTime(new Date());
                wfFileDao.deleteFileByPrimaryKey(wfFile);
                sysBussinessAuthExpressionService.deleteByBusinessId(wfFile.getId());
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("删除文件失败！",e);
            }
        }

	}

    /**
     * 根据主键删除文件（物理删除）
     * @param wfFile 文件对象
     */
    public void deleteByPrimaryKey(WfFile wfFile) {
        try {
            WfFolder wfFolder = wfFolderDao.selectByPrimaryKey(wfFile.getFolderId());
            String bucket = wfFolder.getBucket();
            String objKey = wfFile.getFileName() + "." + wfFile.getFileSuffix();
            jssService.deleteObject(bucket,objKey);          //删除云端存储的文件
            wfFile.setModifier(ComUtils.getLoginName());
            wfFile.setModifyTime(new Date());
            wfFileDao.deleteByPrimaryKey(wfFile);
            sysBussinessAuthExpressionService.deleteByBusinessId(wfFile.getId());
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("删除文件失败！",e);
        }
    }

    /**
     * 根据文件ID查询文件
     * @param wfFile 文件对象
     * @return 文件对象
     */
    public WfFile selectByPrimaryKey(WfFile wfFile){
        wfFile = wfFileDao.selectByPrimaryKey(wfFile);
        //查询文件对应表达式的参数
        Map<String, String> map = new HashMap<String, String>();
        map.put("busType", "1");
        map.put("fileId", wfFile.getId());
        List<SysAuthExpression> list= wfFileDao.findBusinessExpression(map);
        wfFile.setList(list);
        if(wfFile.getFileType().equals("1")){
        	InputStreamReader is = null;
            try {
                is = new InputStreamReader(jssService.downloadFile(wfFile.getBucket(), wfFile.getFileKey()), "GBK");
                StringBuffer buffer = new StringBuffer();
                int i;
                char[] b = new char[1024];
                while((i = is.read(b)) != -1){
                    buffer.append(new String(b).trim());
                }
                wfFile.setFileContent(buffer.toString());
            } catch (Exception e) {
                logger.error(e);
                throw new BusinessException("读取文件失败！",e);
            } finally {
	        	try{
	        		if(is!=null){
	        			is.close();
	        		}
	        	} catch(IOException e) {
	        	}
	        }
        }
        return wfFile;
    }

    /**
     * 获取同一桶下相同文件的数量
     * @param wfFile 文件对象
     * @return 相同文件数量
     */
    public Integer findFileCount(WfFile wfFile){
        return wfFileDao.findFileCount(wfFile);
    }

    /**
     * 下载文档
     * @param wfFile 文档对象
     * @param headers head头信息
     */
    @Override
    public ResponseEntity<byte[]> fileDownload(WfFile wfFile,HttpHeaders headers) {
        InputStream in = jssService.downloadFile(wfFile.getBucket(), wfFile.getFileKey());

        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(in));
//            StringBuffer sb = new StringBuffer();
//            String data = "";
//            while((data = br.readLine())!=null)
//            {
//                sb.append(data);
//            }
//            System.out.println(new String(sb.toString()));
//            System.out.println(new String(sb.toString().getBytes("UTF-8")));
//            System.out.println(new String(sb.toString().getBytes("GBK")));
            return new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
        } catch (IOException e) {
            logger.error(e);
            throw new BusinessException("下载文档失败！",e);
        } finally {
        	try{
        		in.close();
        	} catch(IOException e) {
        	}
        }
    }

    /**
     * 下载文档
     */
    public byte[] downloadFile(WfFile wfFile){
        byte[] data = null;
        InputStream in = null;
        try {
            in = jssService.downloadFile(wfFile.getBucket(), wfFile.getFileKey());
            data = new byte[in.available()];
            in.read(data);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("下载文档失败！",e);
        } finally {
        	try{
        		if(in!=null){
        			in.close();
        		}
        	} catch(IOException e) {
        	}
        }
        return data;
    }

    /**
     * 获取相同文件夹下同名的文档数量
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     * @return  相同文档数量
     */
    public Integer getFileCount(String ids, String folderId) {
        Integer count = 0;
        try{
            List<WfFile> wfFileList = JsonUtils.fromJsonByGoogle(ids, new TypeToken<List<WfFile>>() { });

            for(WfFile wfFile : wfFileList){
                wfFile = wfFileDao.selectByPrimaryKey(wfFile);
                wfFile.setFolderId(folderId);
                count = count + wfFileDao.findFileCount(wfFile);
            }
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("获取同名文档数量失败！",e);
        }
		return count;
	}
	/**
     * 获取文件的权限表达式名称
     * @param ids	权限表达式ID
     * @return 权限表达式list
     */
	public String  findExpression(String ids){
//		String authExpressionName="";
		StringBuilder authExpressionNameSb = new StringBuilder("");
		String [] array= null;
        try{
            array=ids.split(",");
            List<String> list = new ArrayList<String>();
            for(int i=0;i<array.length;i++){
                list.add(array[i]);
            }
            Map<String, List<String>> map= new HashMap<String, List<String>>();
            map.put("list", list);
            List<SysAuthExpression> resultList=wfFileDao.findExpression(map);
            for(SysAuthExpression sys:resultList){
//                authExpressionName+=sys.getAuthExpressionName()+";  ";
                authExpressionNameSb.append(sys.getAuthExpressionName()+";  ");
            }
        }catch (Exception e) {
            logger.error(e);
            throw  new BusinessException("获取文件表达式名称失败！",e);
        }
		return authExpressionNameSb.toString();
	}
    /**
     * 移动文档
     * @param ids 支持文件对象WfFile的JSON串
     * @param folderId 文件夹ID
     */
    public void fileMoveSave(String ids, String folderId) {

        try {
            WfFolder wfFolder = wfFolderDao.selectByPrimaryKey(folderId);
            List<WfFile> wfFileList = JsonUtils.fromJsonByGoogle(ids, new TypeToken<List<WfFile>>() {});
            for(WfFile wfFile : wfFileList){
                wfFile.setFolderId(wfFolder.getId());
                wfFile.setFolderName(wfFolder.getFolderName());
                //当前登录人
                wfFile.setModifier( ComUtils.getLoginName());
                wfFile.setModifyTime(new Date());
                wfFileDao.updateByPrimaryKey(wfFile);
            }
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("移动文件夹失败！",e);
        }
    }
    /**
     * 获取文件的权限表达式
     * @param
     * @return 权限表达式list
     */
	public List<SysAuthExpression> findBusinessExpression(Map map) {
		return wfFileDao.findBusinessExpression(map);
	}
    public WfFileDao getDao() {
        return wfFileDao;
    }
}

package com.jd.oa.process.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessNodeExtendButton;

public interface ProcessNodeExtendButtonDao extends BaseDao<ProcessNodeExtendButton, String> {
	
	public List<ProcessNodeExtendButton> selectByCondition(ProcessNodeExtendButton processNodeExtendButton);

}

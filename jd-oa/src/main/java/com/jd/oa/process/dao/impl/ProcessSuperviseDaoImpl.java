package com.jd.oa.process.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.process.dao.ProcessDefDao;
import com.jd.oa.process.dao.ProcessSuperviseDao;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessSupervise;


@Repository("processSuperviseDao")
public class ProcessSuperviseDaoImpl extends  MyBatisDaoImpl<ProcessSupervise, String> implements ProcessSuperviseDao{
	
	/**
     * 根据督办人ID获取流程流程实例ID（代表已督办过的流程）
     * @param userId
     * @return
     */
    public List<ProcessSupervise> findProcessInstanceByUserId(String userId) {
    	List<ProcessSupervise> result = null;
    	
    	result = this.getSqlSession().selectList("com.jd.oa.process.model.ProcessSupervise.findProcessInstanceByUserId", userId);
    	return result;
    }
}

package com.jd.oa.process.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessType;

import java.util.List;

public interface ProcessDefService extends BaseService<ProcessDefinition,String>{
	/**
	 * @author wangdongxing
	 * @description 添加和保存流程定义实体
	 * @param processDefinition
	 * @return
	 */
	public String addOrUpdateProcessDefinition(ProcessDefinition processDefinition);
	/**
	 * @author wangdongxing
	 * @description 根据流程模板添加流程定义
	 * @param processDefinition
	 * @param templeteId
	 * @return
	 */
	public String addOrUpdateProcessDefinition(ProcessDefinition processDefinition,String templeteId);
	/**
	 * @author wangdongxing
	 * @description 删除多条流程定义
	 * @param ids 
	 * @return
	 */
	public void deleteProcessDefinition(String[] ids);
	/**
	 * @author wangdongxing
	 * @description 删除一条流程定义
	 * @param id
	 * @return
	 */
	public void deleteProcessDefinition(String id);
	/**
	 * @author wangdongxing
	 * @description 根据流程类别查找流程定义列表
	 * @param typeId
	 * @return
	 */
	public List<ProcessDefinition> getPrcocessDefinitionListByType(String typeId);
	/**
	 * @author wangdongxing
	 * @description 发布多条流程定义
	 * @param processDefinitionList
	 * @return
	 */
	public void deployProccessDef(List<ProcessDefinition> processDefinitionList);

	/**
	 * @author wangdongxing
	 * @description 批量保存流程定义为模板
	 * @param ids
	 * @return
	 */
	public void saveToProcessDefTemplete(String[] ids);
	/**
	 * @author wangdongxing
	 * @description 保存流程定义为模板
	 * @param id
	 * @return
	 */
	public void changeOwner(String[] resIds,String ownerId);
	
	public void saveToProcessDefTemplete(String id);

    /**
     * 保存流程定义文件到JSS
     * @param processDefinition 流程定义实体对象
     */
    public void saveProcessDefinitionToJss(ProcessDefinition processDefinition);

    /**
     * 读取流程定义文件
     * @param processDefinition 流程定义实体对象
     */
    public ProcessDefinition loadProcessDefinitionFile(ProcessDefinition processDefinition);
    
    ProcessDefinition getByDefinitionId(String processDefinitionId);
    
    /**
	 * 
	 * @desc 根据流程类别Id获取流程定义列表
	 * @author zhouhuaqi
	 * @date 2013-10-26 下午10:56:59
	 *
	 * @return
	 */
	public List<ProcessDefinition> findByProcessTypeIds(List<String> processTypeIds);


	/**
	 * 根据流程类别ID获取流程定义列表
	 * @param processType
	 * @return
	 */
	public List<ProcessDefinition> findByProcessType(String processType);
	
	
}

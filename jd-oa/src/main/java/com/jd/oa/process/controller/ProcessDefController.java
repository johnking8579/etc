package com.jd.oa.process.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.oa.di.model.DiDatasource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.app.constant.AppConstant;
import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.sms.SmsService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IOUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.common.utils.PlatformProperties;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessTypeService;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.transfer.exp.service.ProcessDefinitionExportService;
import com.jd.oa.transfer.exp.service.impl.ProcessDefinitionExportServiceImpl;
import com.jd.oa.transfer.imp.service.ProcessDefinitionImportService;

@Controller
@RequestMapping(value="/process")
public class ProcessDefController {
	
	private static final Logger logger = Logger.getLogger(ProcessDefController.class);
	
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
	
	@Autowired
	private FormService formService;
    @Autowired
    private JssService jssService;
    @Autowired
    private ProcessDefinitionConfigService processDefinitionConfigService;
    @Autowired
	private SmsService smsService;
    @Autowired
	private MailSendService mailSendService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ProcessTypeService processTypeService;
    @Autowired
    private ProcessDefinitionExportService processDefinitionExportService;  //流程导出服务
    @Autowired
    private ProcessDefinitionImportService processDefinitionImportService; //流程导入服务
    
    @Autowired
    private ProcessDefinitionConfigDetailService processDefinitionConfigDetailService ;
    
    @Autowired
    private DictDataService dictDataService;
    
    @Autowired
	private  DiDatasourceService diDatasouceService;
    
    @Autowired
	private ApplicationService applicationService;
	/**
	 * @author wangdongxing
	 * @description 流程定义入口页面
	 * @return
	 */
	@RequestMapping(value="/processDef_index",method=RequestMethod.GET)
	public ModelAndView processDefIndex(){
		boolean meIsAdmin=sysUserService.isAdministrator(ComUtils.getLoginNamePin());
		ModelAndView mav=new ModelAndView();
		mav.setViewName("process/processDef_index");
		mav.addObject("meIsAdmin", meIsAdmin);
		return mav;
	}
	
	/**
	 * @author wangdongxing
	 * @description 流程模版入口页面
	 * @return
	 */
	@RequestMapping(value="/processTemplate_index",method=RequestMethod.GET)
	public ModelAndView processTemplateIndex(){
		boolean meIsAdmin=sysUserService.isAdministrator(ComUtils.getLoginNamePin());
		ModelAndView mav=new ModelAndView();
		mav.setViewName("process/processTemplate_index");
		mav.addObject("meIsAdmin", meIsAdmin);
		return mav;
	}
	/**
	 * @author wangdongxing
	 * @description 流程定义的分页展示
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/processDef_page",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefPage(HttpServletRequest request){
		PageWrapper<ProcessDefinition> pageWrapper=new PageWrapper<ProcessDefinition>(request);
		
		//添加搜索条件
		String processTypeId=request.getParameter("processTypeId");
		String childProcessTypeId=request.getParameter("childProcessTypeId");
		String processDefinitionName=request.getParameter("processDefinitionName");
		String isIssue=request.getParameter("isIssue");
		String isOuter=request.getParameter("isOuter");
		String owner=request.getParameter("owner");
		String organizationType=request.getParameter("organizationType");
		String isTemplate=request.getParameter("isTemplate");
		//处理流程类别和子类别的查询逻辑
		if(childProcessTypeId!=null&&!childProcessTypeId.equals("")){
			//使用子类别查询
			pageWrapper.addSearch("processTypeId", childProcessTypeId);
		}else if(processTypeId!=null&&!processTypeId.equals("")){
			//使用父类别查询
			Map<String, String> map = new HashMap<String, String>();
			map.put("parentId", processTypeId);
			List<ProcessType> list = processTypeService.find("findProcessName",map);
			if(list!=null&&list.size()!=0){
				List<String> childTypeIds=new ArrayList<String>();
				childTypeIds.add(processTypeId);
				for(ProcessType type:list){
					childTypeIds.add(type.getId());
				}
				pageWrapper.addSearch("childProcessTypeId", childTypeIds);
			}else{
				pageWrapper.addSearch("processTypeId", processTypeId);
			}
		}else{
			//空查询返回所有的结果
			pageWrapper.addSearch("processTypeId", processTypeId);
		}
		
		pageWrapper.addSearch("processDefinitionName", processDefinitionName);
		pageWrapper.addSearch("isIssue", isIssue);
		pageWrapper.addSearch("isOuter", isOuter);
		pageWrapper.addSearch("organizationType", organizationType);
		if(null!=owner&&!owner.equals("")){
			//按照真是名字进行所有人模糊查找
			pageWrapper.addSearch("realName",owner);
		}else{
			//根据所有人名字进行唯一查找
			boolean meIsAdmin=sysUserService.isAdministrator(ComUtils.getLoginNamePin());
			if(!meIsAdmin){
				pageWrapper.addSearch("owner",sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
			}
		}
		pageWrapper.addSearch("isTemplate", isTemplate+"");
//		pageWrapper.addSearch("creator",ComUtils.getLoginName());
		
		//默认按照时间降序
		pageWrapper.addSort("createTime","desc");
		//后台取值存入pageWrapper
		processDefService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		//返回结果
		return pageWrapper.getResult();
	}
	
	/**
	 * @author wangdongxing
	 * @description 跳转流程定义增加页面
	 * @return
	 */
	@RequestMapping(value="/processDef_add",method=RequestMethod.GET)
	public ModelAndView processDefAdd(String isTemplate){
		ModelAndView mav=new ModelAndView("process/processDef_add");
		mav.addObject("processDefinition",JsonUtils.toJsonByGoogle(new ProcessDefinition()));
		mav.addObject("processOrTemplate",(isTemplate==null||isTemplate.equals(""))?"流程":"模板");
		return mav;
	}
	/**
	 * @author wangdongxing
	 * @description 跳转流程定义增加页面
	 * @return
	 */
	@RequestMapping(value="/processDef_update",method=RequestMethod.GET)
	public ModelAndView processDefUpdate(String id,String isTemplate){
		ModelAndView mav=new ModelAndView("process/processDef_add");
		if(null!=id&&!("").equals(id)){
			ProcessDefinition processDefinition=processDefService.get(id);
			if(null!=processDefinition){
				mav.addObject("processDefinition",JsonUtils.toJsonByGoogle(processDefinition));
			}
		}
		mav.addObject("processOrTemplate",(isTemplate==null||isTemplate.equals(""))?"流程":"模板");
		return mav;
	}
	/**
	 * @author wangdongxing
	 * @description 流程定义
	 * @param processDefinition
	 * @return
	 */
	@RequestMapping(value="/processDef_addSave",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefAddSave(ProcessDefinition processDefinition,String processDefId){
		String id="";
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			id=processDefService.addOrUpdateProcessDefinition(processDefinition,processDefId);
			if(id.equals("-1")){
				resultMap.put("operator", false);
				resultMap.put("message","存在重名数据，请修改名称！");
			}else{
				resultMap.put("operator", true);
				resultMap.put("message","操作成功");
				resultMap.put("processId",id);
				resultMap.put("isOuter",processDefinition.getIsOuter());
			}
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","操作失败");
		}
		return resultMap;
	}
	
	@RequestMapping(value="/processDef_setting",method=RequestMethod.GET)
	public ModelAndView processDefSetting(String id){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("process/processDef_setting");
		mav.addObject("processDefId",id==null?"":id);
		return mav;
	}
	
	/**
	 * @author qiuyang
	 * @description 统一待办配置
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/processDef_settingBusinessConfig",method=RequestMethod.GET)
	public ModelAndView processDefSettingBusinessConfig(String id){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("process/processDef_settingBusinessConfig");
		
		
		ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
    	pdc.setProcessDefinitionId(id);
    	pdc.setConfigType("1");
    	List<ProcessDefinitionConfig> processDefConfiglist = processDefinitionConfigService.find(pdc);
    	StringBuffer sb = new StringBuffer("");
    	for(ProcessDefinitionConfig processDefConfig:processDefConfiglist){
    		sb.append(processDefConfig.getConfigItem()).append(",");
    	}
		List<DiDatasource> DiDatasourcelist = diDatasouceService.find(new DiDatasource());
		ProcessDefinition processDef = processDefService.get(id);
		String datasourceId = processDef.getDataSourceId();
		mav.addObject("configItems",sb.toString());
		mav.addObject("diDatasourcelist", DiDatasourcelist);
		mav.addObject("rejectDatasourceId", processDef.getRejectDataSourceId());
		mav.addObject("datasourceId",datasourceId);
		mav.addObject("processDefId",id==null?"":id);
		return mav;
	}
	
	/**
	 * @author qiuyang
	 * @description 统一待办配置更新
	 * @param processDefId
	 * @param dataSourceId
	 * @param configItems
	 * @return
	 */
	@RequestMapping(value="/processDef_addSettingBusinessConfig",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefSettingBusinessConfigSave(String processDefId,String dataSourceId,String rejectDataSourceId,String configItems){
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> configItemList = new ArrayList<String>();
		if(configItems.contains(",")){
			String[] items = configItems.split(",");
			for(int i=0; i<items.length; i++){
				configItemList.add(items[i]);
			}
		}else{
			configItemList.add(configItems);
		}
		
		if(null != configItemList && !configItemList.isEmpty()){
			//删除主表字段
			ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
	    	pdc.setProcessDefinitionId(processDefId);
	    	pdc.setConfigType("1");
	    	List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc);
	    	processDefinitionConfigService.delete(list,true);
	    	
	    	//更新主表字段
	    	int count = 0;
	    	for(String configItem:configItemList){
	    		ProcessDefinitionConfig config = new ProcessDefinitionConfig();
	        	config.setProcessDefinitionId(processDefId);
	        	config.setConfigType("1");
	        	config.setSortNo(count);
	        	config.setConfigItem(configItem);
	        	config.setYn(0);
	        	processDefinitionConfigService.insert(config);
	        	count++;
	    	}
		}
		ProcessDefinition processDef = new ProcessDefinition();
		processDef.setId(processDefId);
		processDef.setDataSourceId(dataSourceId);
		processDef.setRejectDataSourceId(rejectDataSourceId);
		processDefService.update(processDef);
		resultMap.put("operator", true);
		resultMap.put("message","操作成功");
		return resultMap;
	}
	
	
	/**
	 * @author wangdongxing 
	 * @description 批量删除操作
	 * @param
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/processDef_delete",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefDelete(String idstr){
		String[] ids=JsonUtils.fromJsonByGoogle(idstr,new TypeToken<String[]>(){});
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			processDefService.deleteProcessDefinition(ids);
			resultMap.put("operator", true);
			resultMap.put("message","删除成功");
		} catch (Exception e) {
			if(e.getMessage().equals("isSue")){
				throw new BusinessException("流程定义已发布不允许删除!");
			}
			resultMap.put("operator", false);
			resultMap.put("message","删除失败");
		}
		return resultMap;
	}
	/**
	 * @author wangdongxing
	 * @description 另存为模板
	 * @param idstr
	 * @return
	 */
	@RequestMapping(value="/processDef_saveToTemplete",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefSaveToTemplete(String idstr){
		String[] ids=JsonUtils.fromJsonByGoogle(idstr,new TypeToken<String[]>(){});
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			processDefService.saveToProcessDefTemplete(ids);
			resultMap.put("operator", true);
			resultMap.put("message","操作成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","操作失败");
		}
		return resultMap;
	}
	/**
	 * @author wangdongxing
	 * @description 变更所有人操作
	 * @param resIds
	 * @param ownerId
	 * @return
	 */
	@RequestMapping(value="/processDef_changeOwner",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefChangeOwner(String resIds,String ownerId){
		String[] ids=JsonUtils.fromJsonByGoogle(resIds,new TypeToken<String[]>(){});
		Map<String,Object> resultMap=new HashMap<String, Object>();
//		String myUserId=sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin());
		if(ownerId==null||ownerId.equals("")){
			resultMap.put("operator", false);
			resultMap.put("message","变更所有人ID为空");
			return resultMap;
		}
		//变更人的重新处理
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("owner", ownerId);
		map.put("resIds", ids);
		List<String> list=processDefService.find("findSameProcessDefinationNameByOwner", map);
		if(list!=null&&list.size()>0){
			StringBuilder sb=new StringBuilder();
			sb.append("变更人已存在同名流程");
			for(String str:list){
				sb.append("["+str+"]");
			}
			sb.append(",请修改流程名称");
			resultMap.put("operator", false);
			resultMap.put("message",sb.toString());
			return resultMap;
		}
		try {
			processDefService.changeOwner(ids, ownerId);
			resultMap.put("operator", true);
			resultMap.put("message","操作成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","操作失败");
		}
		return resultMap;
	}
	
	/**
	 * @author wangdongxing
	 * @description 获取资源权限ID
	 * @param
	 * @return
	 */
	@RequestMapping(value="/processDef_findAuthIds",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefAssgin(String id,String businessType){
		List<String> businessIds=new ArrayList<String>();
		//转化任务类型
		String busType="";
		if("process".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_PROCESS;
		if("node".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_NODE;
		if("supervise".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_SUPERVISE;
		if("addsign".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_ADDSIGN;
		if("variable".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_VARIABLE;
		if("administrator".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_ADMINISTRATOR;
		
		//取到资源的表达式列表
		Map<String,String> map=new HashMap<String, String>();
		map.put("businessId",id);
		map.put("businessType",busType);
		List<SysBussinessAuthExpression> list=sysBussinessAuthExpressionService.find("findByBusinessIdAndBusinessType",map);
		for(SysBussinessAuthExpression bussinessAuthExpression:list){
			businessIds.add(bussinessAuthExpression.getAuthExpressionId());
		}
		return businessIds;
	}
	/**
	 * @author wangdongxing
	 * @description 用户授权操作
	 * @param
	 * @return
	 */
	@RequestMapping(value="/processDef_assgin",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefAssgin(String resIds,String authIds,String businessType){
		//得到资源ID数组
		String[] resIdss=JsonUtils.fromJsonByGoogle(resIds, new TypeToken<String[]>() {});
		//得到授权表达式的ID数组
		String[] authIdss=JsonUtils.fromJsonByGoogle(authIds, new TypeToken<String[]>() {});
		
		String busType="";
		if("process".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_PROCESS;
		if("node".equals(businessType))  busType=SysBussinessAuthExpression.BUSSINESS_TYPE_NODE;
		if("supervise".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_SUPERVISE;
		if("addsign".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_ADDSIGN;
		if("variable".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_VARIABLE;
		if("administrator".equals(businessType)) busType=SysBussinessAuthExpression.BUSSINESS_TYPE_ADMINISTRATOR;
		
		List<SysBussinessAuthExpression> bussinessAuthExpressions=new ArrayList<SysBussinessAuthExpression>();
		SysBussinessAuthExpression authExpression;
		for(String rId:resIdss){
			//如果表达式数组为空说明要清空资源的说有表达式
			if(authIdss==null||authIdss.length==0){
				authExpression=new SysBussinessAuthExpression();
				authExpression.setAuthExpressionId(null);
				authExpression.setBusinessId(rId);
				authExpression.setBusinessType(busType);
				bussinessAuthExpressions.add(authExpression);
			}else{
				for(String aId:authIdss){
					authExpression=new SysBussinessAuthExpression();
					authExpression.setAuthExpressionId(aId);
					authExpression.setBusinessId(rId);
					authExpression.setBusinessType(busType);
					bussinessAuthExpressions.add(authExpression);
				}
			}
		}
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			sysBussinessAuthExpressionService.updateByResources(bussinessAuthExpressions);
			resultMap.put("operator", true);
			resultMap.put("message","操作成功");
		} catch (Exception e) {
			throw new BusinessException("用户授权操作失败",e);
		}
		return resultMap;
	}
	/**
	 * @author wangdongxing
	 * @description 流程定义发布
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processDef_sue",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processDefSue(String ids){
        List<ProcessDefinition> processDefinitionList = JsonUtils.fromJsonByGoogle(ids,new TypeToken<List<ProcessDefinition>>(){});
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			processDefService.deployProccessDef(processDefinitionList);
			resultMap.put("operator", true);
			resultMap.put("message","发布成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","发布失败");
			
		}
		return resultMap;
	}
	/**
	 * @author wangdongxing
	 * @description 根据流程类别取出流程定义数据
	 * @param typeId 流程类别ID
	 * @return
	 */
	@RequestMapping(value="/processDef_getByType",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getprocessDefByType(String typeId){
		//如果typeId为空或空字串，将默认取出所有数据
		return processDefService.getPrcocessDefinitionListByType(typeId);
	}
	/**
	 * @author wangdongxing
	 * @description 取出流程定义模板
	 * @return
	 */
	@RequestMapping(value="/processDef_getTemplete",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getprocessDefByType(){
		//如果typeId为空或空字串，将默认取出所有数据
		ProcessDefinition processDefinition=new ProcessDefinition();
		processDefinition.setIsTemplate("1");
		processDefinition.setCreator(ComUtils.getLoginName());
		return processDefService.find(processDefinition);
	}
	
	/**
	 * 流程定义的导出操作
	 * @param response
	 * @param ids
	 */
	@RequestMapping(value="/processDef_export",method=RequestMethod.GET)
	public void exportProcessDef(HttpServletRequest request,HttpServletResponse response,String ids){
		String contextPath=request.getSession().getServletContext().getRealPath("/");
		ProcessDefinitionExportServiceImpl.contextPath = contextPath;
		String[] pdfIds = JsonUtils.fromJsonByGoogle(ids,new TypeToken<String[]>(){});
		
		File file = processDefinitionExportService.exportProcessDefinition(pdfIds[0]);
		if(file==null){
			throw new BusinessException("生成打包文件失败！");
		}
		FileInputStream fin=null;
		ServletOutputStream os=null;
		try{
			fin=new FileInputStream(file);
			os=response.getOutputStream();
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName());  
			response.setContentType("application/octet-stream; charset=utf-8");  
			
			int len=0;
			byte[] buffer=new byte[1024];
			while((len=fin.read(buffer))!=-1){
				os.write(buffer,0, len);
			}
			os.flush();
		}catch (IOException e) {
			throw new BusinessException("导出数据失败！",e);
		}finally{
				try {
					if(os!=null) os.close();
					if(fin!=null) fin.close();
					file.delete();
				} catch (IOException e) {
					throw new BusinessException("导出数据失败！",e);
				}
		}
	}
	/**
	 * 流程定义的导入操作
	 * @param
	 * @param
	 */
	@RequestMapping(value="/processDef_import",method=RequestMethod.POST,produces = "application/json")
	@ResponseBody
	public Object importProcessDef(String fileName){
		Map<String,Object> resultMap=new HashMap<String, Object>();
//		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath().substring(1);
//		String impHistoryPath = path + "imp" + FileUtil.getSep() + "history" + FileUtil.getSep() + fileName;
		String path = PlatformProperties.getProperty("jd-oa.jss.temp.directory");
		File folder = new File(path);
		if(!folder.exists()){
			folder.mkdir();
		}
		File outFile = new File(folder.getAbsolutePath() + FileUtil.getSep() + fileName);
		if(outFile.exists()){
			FileInputStream in = null;
	        try {
	        	in = new FileInputStream(outFile);
	        	processDefinitionImportService.importProcessDefinition(in);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e);
			} finally{
				try {
					if(in!=null){
						in.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			resultMap.put("operator", true);
		}else{
			throw new BusinessException("导入失败！");
		}
		return resultMap;
	}
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-23 上午09:36:42
	 *
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value="/processDef_processIndex",method=RequestMethod.GET)
	public ModelAndView processDef_processIndex(HttpServletRequest request, HttpServletResponse response, String processDefinitionId, String formId, String pafProcessDefinitionId) throws IOException{
//		ModelAndView mav = new ModelAndView("design/index.jsp");
//		mav.addObject("id",id);
//		mav.addObject("formId",formId);
		response.sendRedirect(request.getContextPath()+"/design/index.jsp?processDefinitionId="+processDefinitionId+"&formId="+formId+"&pafProcessDefinitionId="+pafProcessDefinitionId);
		return null;
	}
	
	/*
	 * 配置流程基本属性
	 */
	@RequestMapping(value="/processBasicProperties_index")
	public ModelAndView processBasicProperties_index(
			@RequestParam(value = "processId", required = false) String  processId){
		ModelAndView mav = new ModelAndView("process/processBasicProperties_index");
		ProcessDefinition processDefinition = null;
		if(processId!=null){
			processDefinition = processDefService.get(processId);
			 mav.addObject("isEmail",getAlertMode(processDefinition.getAlertMode(),0));
			 mav.addObject("isSms",getAlertMode(processDefinition.getAlertMode(),1));
			 mav.addObject("isLync",getAlertMode(processDefinition.getAlertMode(),2));
		 	 mav.addObject("isComment",processDefinition.getIsComment());
			 mav.addObject("isUploadAttach",processDefinition.getIsUploadAttach());
			 mav.addObject("isRelateDoc",processDefinition.getIsRelateDoc());
			 mav.addObject("isRelateProcessInstance",processDefinition.getIsRelateProcessInstance());
			 
			 Form form = formService.get(processDefinition.getFormId());
			 if(form!=null){
				 mav.addObject("formId",form.getId());
				 mav.addObject("formName",form.getFormName());
			 }
			 
			 mav.addObject("topLevelCode",processDefinition.getTopLevelCode());
			 
			 List<DictData> topLevelList = dictDataService.findDictDataList("topLevel");
			 mav.addObject("topLevelList", topLevelList);

			mav.addObject("followCodePrefix",processDefinition.getFollowCodePrefix());
		}
		return mav ;
	}
	
	
	/*
	 * 配置流程基本属性
	 */
	@RequestMapping(value="/processBasicProperties_variables")
	public ModelAndView processBasicProperties_variables(
			@RequestParam(value = "processId", required = false) String  processId){
				
		ModelAndView mav = new ModelAndView("process/processBasicProperties_variables");
		

		List<DiDatasource> DiDatasourcelist = diDatasouceService.find(new DiDatasource());
		mav.addObject("diDatasourcelist", DiDatasourcelist);
		return mav;
	}
	
	@RequestMapping(value="/processBasicProperties_administrator")
	public ModelAndView processBasicProperties_administrator(
			@RequestParam(value = "processId", required = false) String  processId){
				
		ModelAndView mav = new ModelAndView("process/processBasicProperties_administrator");
		
		return mav;
		
	}
	
	@RequestMapping(value="/processBasicPropertiesById_view")
	@ResponseBody
	public String processBasicPropertiesById_view(
			@RequestParam(value = "processId", required = false) String  processId){
		
		if(processId!=null){
			ProcessDefinition processDefinition = processDefService.get(processId);
			return JsonUtils.toJsonByGoogle(processDefinition);
		}
		
		return "no data";
	}
	
	@RequestMapping(value="/processBasicProperties_insert")
	@ResponseBody
	public String processBasicProperties_insert(
			@RequestParam(value = "processId", required = false) String  processId,
			@RequestParam(value = "formId", required = false) String  formId,
			@RequestParam(value = "isEmail", required = false) String  isEmail,
			@RequestParam(value = "isSms", required = false) String  isSms,
			@RequestParam(value = "isLync", required = false) String  isLync,
			@RequestParam(value = "isComment", required = false) String  isComment,
			@RequestParam(value = "isUploadAttach", required = false) String  isUploadAttach,
			@RequestParam(value = "isRelateDoc", required = false) String  isRelateDoc,
			@RequestParam(value = "isRelateProcessInstance", required = false) String  isRelateProcessInstance,
			@RequestParam(value = "topLevelCode", required = false) String  topLevelCode,
			@RequestParam(value = "followCodePrefix", required = false) String  followCodePrefix){
		
		ProcessDefinition processDefinition = new ProcessDefinition();
        processDefinition.setSortNo(-1);

		int num=0;
		if(processId!=null && !"".equals(processId)){
			processDefinition.setId(processId);
			if(formId!=null)
				processDefinition.setFormId(formId);
			if(isEmail==null)
				isEmail="0";
			if(isSms==null)
				isSms="0";
			if(isLync==null)
				isLync="0";
			//获取并集合
			processDefinition.setAlertMode(getAlertMode(isEmail,isSms,isLync));
			
			if(isComment==null)
				isComment="0";
			processDefinition.setIsComment(isComment);
			if(isUploadAttach==null)
				isUploadAttach="0";
			processDefinition.setIsUploadAttach(isUploadAttach);
			if(isRelateDoc==null)
				isRelateDoc="0";
			processDefinition.setIsRelateDoc(isRelateDoc);
			if(isRelateProcessInstance==null)
				isRelateProcessInstance="0";
			processDefinition.setIsRelateProcessInstance(isRelateProcessInstance);
			
			processDefinition.setTopLevelCode(topLevelCode);
			//添加流水单号前缀，只允许唯一
			if(followCodePrefix != null && !followCodePrefix.equals("")){
				ProcessDefinition sameFollowCodePrefixProcessDefinition = new ProcessDefinition();
				sameFollowCodePrefixProcessDefinition.setFollowCodePrefix(followCodePrefix);
				List<ProcessDefinition> sameFollowCodePrefixProcessDefinitionList = processDefService.find(sameFollowCodePrefixProcessDefinition);
				if(sameFollowCodePrefixProcessDefinitionList == null || sameFollowCodePrefixProcessDefinitionList.size() == 0){
					processDefinition.setFollowCodePrefix(followCodePrefix);
				}else if(sameFollowCodePrefixProcessDefinitionList.size() == 1){
					if(sameFollowCodePrefixProcessDefinitionList.get(0).getId().equals(processId)){
						processDefinition.setFollowCodePrefix(followCodePrefix);
					}else{
						throw new BusinessException("流水单号前缀【"+followCodePrefix+"】与流程【"+sameFollowCodePrefixProcessDefinitionList.get(0).getProcessDefinitionName()+"】重复");
					}
				}else if (sameFollowCodePrefixProcessDefinitionList.size() >1){
					throw new BusinessException("流水单号前缀【"+followCodePrefix+"】与流程【"+sameFollowCodePrefixProcessDefinitionList.get(1).getProcessDefinitionName()+"】重复");
				}
			}
			num = processDefService.update(processDefinition);
		}
		
		
		return Integer.valueOf(num).toString();
	}
	
	//由于提醒模式只有一个字段，这里做一些特别的处理
	//根据值存入数据库
	private String getAlertMode(String isEmail,String isSms,String isLync){
		int email = new Integer(isEmail).intValue();
		int sms = new Integer(isSms).intValue();
		int lync = new Integer(isLync).intValue();
		
		int value= email | sms<<1 | lync<<2;
		return Integer.valueOf(value).toString();
	}
	
	//从数据库取值后处理
	private int getAlertMode(String mode,int offset){
		if(mode!=null && !"".equals(mode)){
			int temp = new Integer(mode).intValue();
			return (temp>>offset) & 1;
			
		}
		return 0;
	}

    /**
     * 保存流程定义文件到JSS
     * @param processDefinition 流程定义实体对象
     */
    @RequestMapping(value="/saveProcessDefinitionToJss", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void saveProcessDefinitionToJss(ProcessDefinition processDefinition){
    	
        processDefService.saveProcessDefinitionToJss(processDefinition);
        
        //删除在processDefinition.getAllUserTask();中不存在的数据库节点定义
        
    }


    /**
     * 读取流程定义文件
     * @param processDefinition 流程定义实体对象
     */
    @RequestMapping(value="/loadProcessDefinitionFile", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ProcessDefinition loadProcessDefinitionFile(ProcessDefinition processDefinition){

        return processDefService.loadProcessDefinitionFile(processDefinition);
    }
    
    
    //配置流程表单的显示项目
    @RequestMapping(value="/configColumns")
    @ResponseBody
    public void configColumns(
    		@RequestParam(value = "processId", required = false) String  processId,
			@RequestParam(value = "formId", required = false) String  formId,
			@RequestParam(value = "items[]", required = false) String[]  items,
			@RequestParam(value = "subItems[]", required = false) String[]  subItems){

    	if(processId != null && ! "".equals(processId) && formId != null && ! "".equals(formId) && items != null && items.length > 0){
    		Map<String,List<String>> subitems = new HashMap<String,List<String>>();
    		//组织子表字段
    		if(subItems != null && subItems.length > 0){
	    		for (String subitem : subItems) {
	    			String[] strs = subitem.split(":");
	    			String key = strs[0];
	    			if(subitems.containsKey(key)){
	    				subitems.get(key).add(strs[1]);
	    			}else{
	    				List<String> values = new ArrayList<String>();
	    				values.add(strs[1]);
	    				subitems.put(key, values);
	    			}
	    		}
	    	}
    		
    		
    		//1.删除原先流程定义的配置列
    		//主表
	    	ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
	    	pdc.setProcessDefinitionId(processId);
	    	pdc.setConfigType("1");
	    	
	    	List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc);
	    	processDefinitionConfigService.delete(list,true);
	    	
	    	//子表
	    	ProcessDefinitionConfig pdcsub = new ProcessDefinitionConfig();
	    	pdcsub.setProcessDefinitionId(processId);
	    	pdcsub.setConfigType("3");
	    	List<ProcessDefinitionConfig> sublist = processDefinitionConfigService.find(pdcsub);
	    	if(null != sublist && !sublist.isEmpty()){
	    		for(ProcessDefinitionConfig config: sublist){
	    			ProcessDefinitionConfigDetail detail = new ProcessDefinitionConfigDetail();
	    			detail.setConfigId(config.getId());
	    			List<ProcessDefinitionConfigDetail> detailList = processDefinitionConfigDetailService.find(detail);
	    			processDefinitionConfigDetailService.delete(detailList,true);
	    		}
	    		processDefinitionConfigService.delete(sublist,true);
	    	}
	    	
	    	//2.开始更新
	    	//更新主表
	    	int count = 0;
	    	for (String item : items) {
	    		ProcessDefinitionConfig config = new ProcessDefinitionConfig();
	        	config.setProcessDefinitionId(processId);
	        	config.setConfigType("1");
	        	config.setSortNo(count);
	        	config.setConfigItem(item);
	        	config.setYn(0);
	        	processDefinitionConfigService.insert(config);
	        	count ++;
			}
	    	
	    	//更新子表
	    	if(!subitems.isEmpty()){
	    		Iterator it = subitems.keySet().iterator();
	    		while(it.hasNext()){
	    			String configItem = (String)it.next();
	    			List<String> detailItems = subitems.get(configItem);
	    			ProcessDefinitionConfig config = new ProcessDefinitionConfig();
	    			config.setProcessDefinitionId(processId);
	    			config.setConfigType("3");
	    			config.setConfigItem(configItem);
	    			config.setYn(0);
	    			String configId = processDefinitionConfigService.insert(config);
	    			
	    			if(null != configId){
	    				for(String detailItem:detailItems){
	    					ProcessDefinitionConfigDetail configDetail = new ProcessDefinitionConfigDetail();
	    					configDetail.setConfigId(configId);
	    					configDetail.setValNo(detailItem);
	    					configDetail.setYn(0);
	    					processDefinitionConfigDetailService.insert(configDetail);
	    				}
	    			}
	    		}
	    	}
	    	
	    	//3.更新一下流程定义表里的formid
	    	ProcessDefinition processDefinition = processDefService.get(processId);
	    	processDefinition.setFormId(formId);
	    	processDefService.update(processDefinition);
	    	
	    }
    	
        //return new Integer(count).toString();
    }
    
    
  //表单改变时，删除显示列
    @RequestMapping(value="/formChanged")
    @ResponseBody
    public String formChanged(
    		@RequestParam(value = "processId", required = false) String  processId,
			@RequestParam(value = "formId", required = false) String  formId){
    	
    	int count = 0;
    	
    	ProcessDefinition processDefinition = processDefService.get(processId);
    	if(processDefinition!=null){
    		//如果流程的formId改变了，则更新formId,并删除表单的显示列
    		if(formId != null && ! formId.equals(processDefinition.getFormId())){
    			//更新formID
    			processDefinition.setFormId(formId);
    	    	processDefService.update(processDefinition);
    	    	
    	    	//删除原先流程定义的配置列
    	    	ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
    	    	pdc.setProcessDefinitionId(processId);
    	    	pdc.setConfigType("1");
    	    	
    	    	List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc);
    	    	processDefinitionConfigService.delete(list,true);
    		}
    	}
    	
    	return Integer.valueOf(count).toString();
    	
    }
    
    
    
  //清除表单
    @RequestMapping(value="/formIdClear")
    @ResponseBody
    public String formIdClear(
    		@RequestParam(value = "processId", required = false) String  processId,
			@RequestParam(value = "formId", required = false) String  formId){
    	
    	int count = 0;
    	
    	ProcessDefinition processDefinition = processDefService.get(processId);
    	if(processDefinition!=null){
    	
    			//更新formID为空
    			processDefinition.setFormId("");
    	    	processDefService.update(processDefinition);
    	    	
    	    	//删除原先流程定义的配置列
    	    	//主表
    	    	ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
    	    	pdc.setProcessDefinitionId(processId);
    	    	pdc.setConfigType("1");
    	    	
    	    	List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc);
    	    	processDefinitionConfigService.delete(list,true);

    	    	//子表
    	    	ProcessDefinitionConfig pdcsub = new ProcessDefinitionConfig();
    	    	pdcsub.setProcessDefinitionId(processId);
    	    	pdcsub.setConfigType("3");
    	    	List<ProcessDefinitionConfig> sublist = processDefinitionConfigService.find(pdcsub);
    	    	if(null != sublist && !sublist.isEmpty()){
    	    		for(ProcessDefinitionConfig config: sublist){
    	    			ProcessDefinitionConfigDetail detail = new ProcessDefinitionConfigDetail();
    	    			detail.setConfigId(config.getId());
    	    			List<ProcessDefinitionConfigDetail> detailList = processDefinitionConfigDetailService.find(detail);
    	    			processDefinitionConfigDetailService.delete(detailList,true);
    	    		}
    	    		processDefinitionConfigService.delete(sublist,true);
    	    	}
    		
    	}
    	
    	
    	return Integer.valueOf(count).toString();
    	
    }
    
    
    @RequestMapping(value="/variable_list", produces = "application/json")
    @ResponseBody
    public Object variable_index(HttpServletRequest request ,
    		@RequestParam(value = "processId", required = false) String  processId){
    	
    	PageWrapper<ProcessDefinitionConfigDetail> pageWrapper=new PageWrapper<ProcessDefinitionConfigDetail>(request);
    	
    	
    	if(processId != null && !processId.equals("")){
    		pageWrapper.addSearch("processId", processId) ;
    	}
    	
    	pageWrapper.addSort("name","asc");
    	pageWrapper.addSort("val_no", "asc");
    	
    	processDefinitionConfigDetailService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
    	
//    	Map<String, String> map = new HashMap<String, String>() ;
//    	map.put("processId", processId) ;
//    	
//    	List<ProcessDefinitionConfigDetail> list = processDefinitionConfigDetailService.find("findByMap",map);
    	
//    	return  JsonUtils.toJsonByGoogle(list);
//    	return pageWrapper.getResult() ;
//    	Map<String, Object> result = new HashMap<String, Object>();
//    	result.put("sEcho",9);//原值返回
//		result.put("iTotalRecords",9); //过滤前总记录数
//		result.put("iTotalDisplayRecords",1);//过滤后总记录数
//		result.put("current_page",1);
//		result.put("aaData", list ); //返回的列表数据
		
    	
    	//返回结果
    	return pageWrapper.getResult();
    }

    @RequestMapping(value="/variable_insert",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object variable_insert(
    		@RequestParam(value = "processId", required = false) String  processId,
    		@RequestParam(value = "variables_name", required = false) String  variables_name,
			@RequestParam(value = "variables_no", required = false) String  variables_no){
    	if ( variables_no!=null &&  ! variables_no.equals("")  && !variables_no.equals("")){
    		
    		
    		String configId = getProcessDefinitionConfigId(processId, variables_name, 2) ;
    		
    		String[] arr ;
    		if(variables_no.indexOf("dataSource#") > -1)
    			arr =new String[]{variables_no};
    		else {
    			arr = variables_no.split(",") ;
			}
    		
    		if(arr != null ){
    			List<ProcessDefinitionConfigDetail> details = new ArrayList<ProcessDefinitionConfigDetail>() ;
    			for(int i = 0 ; i < arr.length ; i ++ ){
    				ProcessDefinitionConfigDetail p = new ProcessDefinitionConfigDetail() ;
    				p.setConfigId(configId) ;
    				p.setValNo(arr[i]) ;
    				p.setYn(0) ;
    				details.add(p) ;
    			}
    			List<String> ids  = processDefinitionConfigDetailService.insert(details) ;
    			if(ids != null)
    				return Integer.valueOf(ids.size()).toString();
    		}
    	}
    	
    	return null ;
    }
    
    //查找ProcessDefinitionConfig是否有数据，有就直接返回，没有先新增一条
    public String getProcessDefinitionConfigId(String  processId,String  variables_name, int type){
    	ProcessDefinitionConfig pdc = new ProcessDefinitionConfig() ;
		pdc.setConfigType("2") ;
		pdc.setYn(0) ;
		pdc.setConfigItem(variables_name) ;
		pdc.setProcessDefinitionId(processId) ;
		
		List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc) ;
		if( list==null || list.size() <= 0 ){
			String pdcId = processDefinitionConfigService.insert(pdc) ;
			return pdcId ;
		}else{
			return list.get(0).getId() ;
		}
		
    }
    
    @RequestMapping(value="/variable_update")
    @ResponseBody
    public String variable_update(
    		@RequestParam(value = "id", required = false) String  id,
    		@RequestParam(value = "expression", required = false) String  expression){
    	ProcessDefinitionConfigDetail temp = processDefinitionConfigDetailService.get(id) ;
    	int count = 0 ;
    	if( temp != null){
    		temp.setExpression(expression);
    		count = processDefinitionConfigDetailService.update(temp) ;
    	}
    	
    	return Integer.valueOf(count).toString();
    }
    
    
    @RequestMapping(value="/variable_delete",method=RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String variable_delete(
    		@RequestParam(value = "id", required = false) String  id){
    	if(id != null){
    		int count =  processDefinitionConfigDetailService.delete(id) ;
    		return Integer.valueOf(count).toString();
    	}
    	
    	return "0" ;
    }
    
    
    @RequestMapping(value="/variableExpressionById")
    @ResponseBody
    public String getVariableExpressionById(String  id){
    	ProcessDefinitionConfigDetail temp = processDefinitionConfigDetailService.get(id) ; 
		if(temp == null || temp.getExpression()== null ){
			return "" ;
		}else{
			return temp.getExpression() ;
		}
		
    }
    
    /**
     * @author zhengbing 
     * @desc 申请节点整个表单缓存至JSS服务器
     * @date 2014年8月1日 下午3:55:47
     * @return Object
     */
    @RequestMapping(value = "/uploadWholeApplyForm2JSS",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object uploadWholeApplyForm2JSS(HttpServletRequest request,String ids) {
    	if(null==ids||ids.trim().length()==0){
    		return "未获取到流程定义key!";
    	}
    	String contextPath = request.getSession().getServletContext().getRealPath("/");
    	String[] keys = ids.split(",");
    	if(null==keys||keys.length==0) return "未获取到流程定义key!";
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < keys.length; i++) {
    		try {
	    		String key = keys[i];
	    		if(null==key||key.length()==0){
	    			continue;
	    		}
	    		String formContent = applicationService.getExecuteWorklistBindReportFormContextForApply(contextPath, key);
	    		key = key + SystemConstant.JDOA_APPLY_FORM_JSS_KEY_FIX;
	    		jssService.deleteObject(SystemConstant.BUCKET, key);
	    		jssService.uploadFile(SystemConstant.BUCKET, key, formContent);
			} catch (Exception e) {
				sb.append(keys[i]+",");
				logger.error("JSS缓存失败,key:"+sb.toString(), e);
			}
		}
    	String returnStr = "JSS缓存成功！";
    	if(sb.toString().contains(",")){
    		returnStr = "JSS缓存失败,key:"+sb.toString();
    	}
        return returnStr;
    }
    
    /**
     * @author zhengbing 
     * @desc 预览JSS上的申请表单
     * @date 2014年8月1日 下午5:36:15
     * @return ModelAndView
     */
    @RequestMapping(value = "/openApplyFormFromJss", method=RequestMethod.GET)
	public ModelAndView openApplyFormFromJss(HttpServletRequest request,
			String processDefinitionKey) throws Exception{
	    ModelAndView mv = new ModelAndView();
	    mv.setViewName("process/jss_apply_form_view");
	    String result = null;
		InputStream in = null;
		if(null==processDefinitionKey||"".equals(processDefinitionKey)){
			throw new BusinessException("未获取到流程定义key！");
		}
		if(!jssService.exist(SystemConstant.BUCKET, processDefinitionKey + SystemConstant.JDOA_APPLY_FORM_JSS_KEY_FIX)){
			throw new BusinessException("JSS上没有该申请表单的缓存！");
		}
		try{
			in = jssService.downloadFile(SystemConstant.BUCKET, processDefinitionKey + SystemConstant.JDOA_APPLY_FORM_JSS_KEY_FIX);
			result = IOUtils.toString(in);
		} catch(IOException e){
			throw new BusinessException("预览JSS申请表单缓存失败！",e);
		} finally {
        	try{
        		in.close();
        	} catch(IOException e) {
        		throw new BusinessException("预览JSS申请表单缓存失败！",e);
        	} catch(NullPointerException e){
        		throw new BusinessException("预览JSS申请表单缓存失败！",e);
        	}
        }
    	mv.addObject("templateHtml", result);
    	mv.addObject("toolbar","");
    	return mv;
	}
}

package com.jd.oa.process.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;

public interface ProcessDefinitionConfigDetailDao extends BaseDao<ProcessDefinitionConfigDetail, String> {
	/**
	 * 
	 * @desc 获取流程配置子项
	 * @author WXJ
	 * @date 2014-6-5 下午12:22:32
	 *
	 * @param processDefinitionId
	 * @param configItem
	 * @param configType
	 * @return
	 */
	public List<ProcessDefinitionConfigDetail> findByMap(String processDefinitionId, String configItem, String configType);
}

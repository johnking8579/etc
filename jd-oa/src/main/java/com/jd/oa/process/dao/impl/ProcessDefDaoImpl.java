package com.jd.oa.process.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessDefDao;
import com.jd.oa.process.model.ProcessDefinition;


@Repository("processDefDao")
public class ProcessDefDaoImpl extends  MyBatisDaoImpl<ProcessDefinition, String> implements ProcessDefDao{

	@Override
	public ProcessDefinition findProcessDefByInstance(String processInstance) {
		// TODO Auto-generated method stub
		Map mp = new HashMap<String, String>();
		mp.put("processInstance", processInstance);
		List<ProcessDefinition> pdList  = this.find("findProcessDefByInstance", mp);
		if (pdList!=null && pdList.size()>0)
			return (ProcessDefinition)pdList.get(0);
		else
			return null;
	}
	
	@Override
	public List<ProcessDefinition> findProcessDefAll() {
		// TODO Auto-generated method stub
		Map mp = new HashMap<String, String>();
		List<ProcessDefinition> pdList = this.find("findAll", mp);
		return pdList;
			
	}
	
	/**
	 * 
	 * @desc 根据流程定义Id获取流程定义列表
	 * @author zhouhuaqi
	 * @date 2013-10-26 下午05:56:59
	 *
	 * @return
	 */
	public List<ProcessDefinition> findByProcessTypeIds(List<String> processTypeIds) {
		List<ProcessDefinition> result = null;
		result = this.getSqlSession().selectList(sqlMapNamespace+".findByProcessTypeIds", processTypeIds);
		return result;
	}

    /**
     * 删除流程节点配置
     * @param processDefinition 流程定义实体
     */
    public void  deleteProcessNodeConfig(ProcessDefinition processDefinition){
        this.getSqlSession().update("com.jd.oa.process.model.ProcessDefinition.deleteProcessNodeConfig", processDefinition);
    }

    /**
     * 删除流程节点表单个性化配置配置
     * @param processDefinition 流程定义实体
     */
    public void  deleteProcessNodeFormPrivilegeConfig(ProcessDefinition processDefinition){
        this.getSqlSession().update("com.jd.oa.process.model.ProcessDefinition.deleteProcessNodeFormPrivilegeConfig", processDefinition);
    }

	@Override
	public ProcessDefinition findByPafDefId(String pafProcessDefId) {
		ProcessDefinition param = new ProcessDefinition();
		param.setPafProcessDefinitionId(pafProcessDefId);
		List<ProcessDefinition> list = this.find(param);
		return list.isEmpty() ? null : list.get(0);
	}
}

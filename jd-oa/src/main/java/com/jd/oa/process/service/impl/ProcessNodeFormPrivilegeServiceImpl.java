package com.jd.oa.process.service.impl;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.form.util.RepleaseKey;
import com.jd.oa.process.dao.ProcessNodeFormPrivilegeDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeService;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 节点表单权限配置Service实现类
 * User: zhaoming
 * Date: 13-9-12
 * Time: 下午4:31
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProcessNodeFormPrivilegeServiceImpl implements ProcessNodeFormPrivilegeService {

    private static final Logger logger = Logger.getLogger(ProcessNodeFormPrivilegeServiceImpl.class);

    @Resource
    private ProcessNodeFormPrivilegeDao processNodeFormPrivilegeDao;
    @Resource
    private ProcessNodeService processNodeService;
    @Resource
    private FormItemService formItemService;
    @Resource
    private FormTemplateService formTemplateService;


    /**
     * 根据主键查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置实体类
     */
    public ProcessNodeFormPrivilege selectByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        return processNodeFormPrivilegeDao.selectByPrimaryKey(processNodeFormPrivilege);
    }

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置列表
     */
    public List<ProcessNodeFormPrivilege> selectByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege){
        return processNodeFormPrivilegeDao.selectByCondition(processNodeFormPrivilege);
    }

    /**
     * 新增节点表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    @Transactional
    public void insertProcessNodeFormPrivilege(ProcessNodeFormPrivilege processNodeFormPrivilege){
        processNodeFormPrivilegeDao.insertProcessNodeFormPrivilege(processNodeFormPrivilege);
    }

    /**
     * 保存节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体对象
     * @param processNodeFormPrivileges 节点表单权限配置JSON
     * @param processNode 流程节点实体对象
     */
    @Transactional
    public void saveProcessNodeFormPrivileges(ProcessNodeFormPrivilege processNodeFormPrivilege, String processNodeFormPrivileges, ProcessNode processNode){
        List<ProcessNodeFormPrivilege> processNodeFormPrivilegeList = JsonUtils.fromJsonByGoogle(processNodeFormPrivileges, new TypeToken<List<ProcessNodeFormPrivilege>>() {});
        for(ProcessNodeFormPrivilege pnfp : processNodeFormPrivilegeList){
            pnfp.setProcessDefinitionId(processNodeFormPrivilege.getProcessDefinitionId());
            pnfp.setNodeId(processNodeFormPrivilege.getNodeId());
            pnfp.setModifier(ComUtils.getLoginName());
            pnfp.setModifyTime(new Date());
            if(pnfp.getId()==null || "".equals(pnfp.getId())){
                pnfp.setId(IdUtils.uuid2());
                pnfp.setCreator(ComUtils.getLoginName());
                pnfp.setCreateTime(new Date());
                processNodeFormPrivilegeDao.insertProcessNodeFormPrivilege(pnfp);
            }else{
               processNodeFormPrivilegeDao.updateByPrimaryKey(pnfp);
            }
        }
//        更新流程节点配置
        ProcessNode pn = new ProcessNode();
        pn.setProcessDefinitionId(processNodeFormPrivilege.getProcessDefinitionId());
        pn.setNodeId(processNodeFormPrivilege.getNodeId());
        List<ProcessNode> processNodeList = processNodeService.find(pn);
        if(processNodeList!=null && processNodeList.size()>0){
            pn = processNodeList.get(0);
            pn.setFormTemplateId(processNode.getFormTemplateId());
            pn.setPrintTemplateId(processNode.getPrintTemplateId());
            processNodeService.update(pn);
        }else{
            pn.setFormTemplateId(processNode.getFormTemplateId());
            pn.setPrintTemplateId(processNode.getPrintTemplateId());
            processNodeService.insert(pn);
        }
    }

    /**
     * 根据主键修改表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    @Transactional
    public void updateByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        try{
            processNodeFormPrivilegeDao.updateByPrimaryKey(processNodeFormPrivilege);
        }catch (Exception e){
            logger.error(e) ;
            throw new BusinessException("修改表单权限配置失败!",e);
        }
    }

    /**
     * 根据主键删除节点表单权限配置（逻辑删除）
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    @Transactional
    public void deleteByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege){
        try{
        processNodeFormPrivilegeDao.deleteByPrimaryKey(processNodeFormPrivilege);
        }catch (Exception e){
            logger.error(e);
            throw  new BusinessException("删除节点表单权限配置失败!",e);
        }
    }
    /**
     * 运行时刻表单权限
     * @param request
     * @param formTemplateId
     * @param formId
     * @return
     */
    @Override
    public String getTemplateContextRuntime(HttpServletRequest request,List<ProcessNodeFormPrivilege> processNodeFormPrivileges,String formTemplateId,String formId){
    	String contextPath = request.getSession().getServletContext().getRealPath("/");
    	FormTemplate formTemplate = formTemplateService.get(formTemplateId);
    	RuntimeFormManager runtime = new RuntimeFormManager(contextPath);
    	List<FormItem> items = formItemService.getFormItemByFormIdAndType(formId.trim(),SystemConstant.FORM_TYPE_M);
    	Map<String , ProcessNodeFormPrivilege> maps = runtime.getProcessNodeFormPrivilegeItemListToMap(processNodeFormPrivileges);
    	for(int i = 0 ; i < items.size() ; i++){
    		FormItem formItem = items.get(i);
    		if(formItem != null){
    			ProcessNodeFormPrivilege processNodeFormPrivilege = maps.get(formItem.getId());
    			
				formItem.setIsHidden(processNodeFormPrivilege.getHidden());
				formItem.setIsEdit(processNodeFormPrivilege.getEdit());
				formItem.setIsNull(processNodeFormPrivilege.getNull());
    		}
    	}
    	Map<String,String> uiTags = runtime.getRuntimeForm(contextPath,items);
    	uiTags.putAll(runtime.getRuntimeFormSubSheet(formItemService.getFormAndItemsByFormId(formId.trim(),SystemConstant.FORM_TYPE_S),false));
    	String templateContext = RepleaseKey.replace(getTemplateContext(formTemplate), uiTags, "[@", "]");
    	return templateContext;
    }
    /**
     * 获取表单内容
     * @param formTemplate
     * @return
     */
    private String getTemplateContext(FormTemplate formTemplate) throws BusinessException{
    	if(formTemplate != null){
			String templateContext = "";
			try {
				templateContext = formTemplateService.getTemplateValue(formTemplate.getTemplatePath());
			} catch (IOException e) {
				throw new BusinessException("获取表单模板失败!",e);
			}
			if(templateContext.trim().length() > 0){
				templateContext = JavascriptEscape.unescape(templateContext);
				if(templateContext.indexOf("</form>") > 0){
					int subLength = templateContext.substring(templateContext.indexOf("</form>") + "</form>".length()).length();
					StringBuffer sb = new StringBuffer(templateContext);
					sb.setLength(sb.length() - subLength);
					templateContext = sb.toString();
				}
				return templateContext;
			} else {
				throw new BusinessException("当前表单模板不存在");
			}
		} else {
			throw new BusinessException("当前表单模板不存在");
		}
    }

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置数
     */
	public Integer countByCondition(
			ProcessNodeFormPrivilege processNodeFormPrivilege) {
		  return processNodeFormPrivilegeDao.countByCondition(processNodeFormPrivilege);
	}
}

package com.jd.oa.process.dao;

import com.jd.oa.process.model.ProcessNodeFormPrivilege;

import java.util.List;

/**
 * 节点表单权限配置Dao接口类
 * User: zhaoming
 * Date: 13-9-12
 * Time: 下午3:54
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessNodeFormPrivilegeDao {

    /**
     * 根据主键查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置实体类
     */
    public ProcessNodeFormPrivilege selectByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置列表
     */
    public List<ProcessNodeFormPrivilege> selectByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 新增节点表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void insertProcessNodeFormPrivilege(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 根据主键修改表单权限配置
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void updateByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);

    /**
     * 根据主键删除节点表单权限配置（逻辑删除）
     * @param processNodeFormPrivilege  节点表单权限配置实体类
     */
    public void deleteByPrimaryKey(ProcessNodeFormPrivilege processNodeFormPrivilege);
    
    /**
     * 根据条件查询节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体类
     * @return 节点表单权限配置数
     */
    public Integer countByCondition(ProcessNodeFormPrivilege processNodeFormPrivilege);


}

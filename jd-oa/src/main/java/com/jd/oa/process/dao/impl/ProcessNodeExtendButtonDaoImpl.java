package com.jd.oa.process.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessNodeExtendButtonDao;
import com.jd.oa.process.model.ProcessNodeExtendButton;

@Component("processNodeExtendButtonDao")
public class ProcessNodeExtendButtonDaoImpl extends MyBatisDaoImpl<ProcessNodeExtendButton, String>
		implements ProcessNodeExtendButtonDao {
	
	/**
	 * 根据条件查询结点扩展按钮配置
	 */
	public List<ProcessNodeExtendButton> selectByCondition(ProcessNodeExtendButton processNodeExtendButton){
		return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessNodeExtendButton.selectByCondition",processNodeExtendButton);
	}
}

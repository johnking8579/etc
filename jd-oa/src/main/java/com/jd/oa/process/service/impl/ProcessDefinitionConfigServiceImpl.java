package com.jd.oa.process.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessDefinitionConfigDao;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeService;


@Service("processDefinitionConfigService")
@Transactional
public class ProcessDefinitionConfigServiceImpl extends BaseServiceImpl<ProcessDefinitionConfig, String> implements ProcessDefinitionConfigService{
	@Autowired
	private ProcessDefinitionConfigDao processDefinitionConfigDao;
	
	public ProcessDefinitionConfigDao getDao(){
		return processDefinitionConfigDao;
	}
}

package com.jd.oa.process.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jd.oa.process.dao.ProcessTypeDao;
import com.jd.oa.process.model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.paf.service.PafProcessDeployService;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.UniqueStringGenerator;
import com.jd.oa.process.dao.ProcessDefDao;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.SysUserService;

@Service("processDefService")
public class ProcessDefServiceImpl extends BaseServiceImpl<ProcessDefinition,String> implements ProcessDefService{
	
	private static final Logger logger = Logger.getLogger(ProcessDefServiceImpl.class);
	
	@Autowired
	private ProcessDefDao processDefDao;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	@Autowired
	private SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
    @Autowired
    private JssService jssService;
    @Autowired
    private PafProcessDeployService pafProcessDeployService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysAuthExpressionService authExpressionService;
    @Autowired
    private ProcessDefinitionConfigService processDefinitionConfigService;
    @Autowired
    private ProcessDefinitionConfigDetailService processDefinitionConfigDetailService;
	@Autowired
	private ProcessTypeDao processTypeDao;

	
	@Override
	public BaseDao<ProcessDefinition, String> getDao() {
		return processDefDao;
	}

	@Override
	@Transactional
	public String addOrUpdateProcessDefinition(ProcessDefinition processDefinition) {
		if(null==processDefinition.getId()||("").equals(processDefinition.getId())){
			//判断重名
			processDefinition.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
			List<ProcessDefinition> pdfList=processDefDao.find("findByDefinitionByName",processDefinition);
			if(null!=pdfList&&pdfList.size()>0){
				return "-1";
			}
			//添加一条数据
			processDefinition.setId(null);
			if (processDefinition.getIsOuter().equals("1")||processDefinition.getIsOuter().equals("2")){
				processDefinition.setIsIssue("1");
			}
			if(null==processDefinition.getIsTemplate()) processDefinition.setIsTemplate("0");
			return processDefDao.insert(processDefinition);
		}else{
			ProcessDefinition newProcessDefinition=processDefDao.get(processDefinition.getId());
			if(null==newProcessDefinition){
				//判断重名
				processDefinition.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
				List<ProcessDefinition> pdfList=processDefDao.find("findByDefinitionByName",processDefinition);
				if(null!=pdfList&&pdfList.size()>0){
					return "-1";
				}
				if (processDefinition.getIsOuter().equals("1")){
					processDefinition.setIsIssue("1");
				}
				if(null==processDefinition.getIsTemplate()) processDefinition.setIsTemplate("0");
				return processDefDao.insert(processDefinition);
			}else{
				processDefinition.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
				//判断重名,只针对流程修改页面有效，从别的地方传入的流程，不走此处理，以流程名有无来判断
				if(processDefinition.getProcessDefinitionName()!=null&&!processDefinition.getProcessDefinitionName().equals("")){
					List<ProcessDefinition> pdfList=processDefDao.find("findByDefinitionByName",processDefinition);
					if(null!=pdfList&&pdfList.size()>0){
						if(!pdfList.get(0).getId().equals(processDefinition.getId())){
							return "-1";
						}
					}
				}
				try {
					ComUtils.copyProperties(processDefinition,newProcessDefinition);
					if(processDefDao.update(newProcessDefinition)==1){
						return processDefinition.getId();
					}
				} catch (Exception e) {
					throw new BusinessException("更新失败",e);
				}
			}
		}
		return null;
	}
	@Override
	@Transactional
	public String addOrUpdateProcessDefinition(ProcessDefinition processDefinition,String templeteId) {
		if(processDefinition==null)throw new BusinessException("保存失败");
		String newPafId="process"+Calendar.getInstance().getTimeInMillis();
		if("2".equals(processDefinition.getIsOuter()) && (null==processDefinition.getId()||"".equals(processDefinition.getId().trim()))){
			processDefinition.setPafProcessDefinitionId("process"+UniqueStringGenerator.getUniqueString());
		}
		if(null==templeteId||"".equals(templeteId)) return this.addOrUpdateProcessDefinition(processDefinition);
		//流程定义使用的模板记录
		ProcessDefinition processDefinitionTemplete=null;    //参照的流程模板
		ProcessDefinition newProcessDefinition=new ProcessDefinition();         //需要保存的流程
		String templatePafId="";
		
		//如果是另存为模板，直接使用模板记录，减少一次数据库操作
		if(null!=processDefinition.getId()&&templeteId.equals(processDefinition.getId())){
			processDefinitionTemplete=processDefinition;
			try {
				ComUtils.copyProperties(processDefinitionTemplete, newProcessDefinition);
			} catch (Exception e) {
				throw new BusinessException("模型对象复制失败",e);
			}
			templatePafId=processDefinitionTemplete.getPafProcessDefinitionId();
			newProcessDefinition.setPafProcessDefinitionId(newPafId);
			newProcessDefinition.setIsTemplate("1");
			newProcessDefinition.setIsIssue("0");
			newProcessDefinition.setSortNo(0);
		}else{//如果是从模板生成
			processDefinitionTemplete=processDefDao.get(templeteId);
			try {
				ComUtils.copyProperties(processDefinitionTemplete, newProcessDefinition);
			} catch (Exception e) {
				throw new BusinessException("模型对象复制失败",e);
			}
			templatePafId=processDefinitionTemplete.getPafProcessDefinitionId();
			newProcessDefinition.setPafProcessDefinitionId(newPafId);
			newProcessDefinition.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
			newProcessDefinition.setProcessTypeId(processDefinition.getProcessTypeId());
			newProcessDefinition.setOrganizationType(processDefinition.getOrganizationType());
			newProcessDefinition.setIsTemplate("0");
			newProcessDefinition.setIsIssue("0");
			newProcessDefinition.setSortNo(0);
		}
//		if(null==newProcessDefinition) return null;
		
		//保存一条新纪录到主表
		newProcessDefinition.setId(null);
		newProcessDefinition.setModifyTime(new Date());
		newProcessDefinition.setModifier(ComUtils.getLoginNamePin());
		String newProcessDefId=this.addOrUpdateProcessDefinition(newProcessDefinition);
		if(null==newProcessDefId||"".equals(newProcessDefId)) return null;
		//取得流程定义的结点
		ProcessNode node=new ProcessNode();
		node.setProcessDefinitionId(templeteId);
		List<ProcessNode> nodeList=processNodeService.find(node);
		//遍历流程结点保存
		for(ProcessNode processNode:nodeList){
			//保存结点的ID方便后续使用
			String oldId=processNode.getId();
			//插入一条新结点记录
			processNode.setId(null);
			processNode.setProcessDefinitionId(newProcessDefId);
			String newNodeID=processNodeService.insert(processNode);
			
			//取得流程节点授权列表
			List<SysBussinessAuthExpression> nodeExpressionList=sysBussinessAuthExpressionService.find("findByBusinessId",oldId);
			for(SysBussinessAuthExpression authExpression:nodeExpressionList){
				authExpression.setId(null);
				authExpression.setBusinessId(newNodeID);
			}
			sysBussinessAuthExpressionService.insert(nodeExpressionList);
		}
		
		//取得流程定义的结点权限
		ProcessNodeFormPrivilege privilege=new ProcessNodeFormPrivilege();
		privilege.setProcessDefinitionId(templeteId);
		List<ProcessNodeFormPrivilege> privilegeList=processNodeFormPrivilegeService.selectByCondition(privilege);
		//保存结点的权限属性
		for(ProcessNodeFormPrivilege processNodeFormPrivilege:privilegeList){
			processNodeFormPrivilege.setId(IdUtils.uuid2());
			processNodeFormPrivilege.setProcessDefinitionId(newProcessDefId);
			processNodeFormPrivilegeService.insertProcessNodeFormPrivilege(processNodeFormPrivilege);
		}
		
		//取得流程定义授权列表
		List<SysBussinessAuthExpression> authExpressionList=sysBussinessAuthExpressionService.find("findByBusinessId", templeteId);
		for(SysBussinessAuthExpression authExpression:authExpressionList){
			authExpression.setId(null);
			authExpression.setBusinessId(newProcessDefId);
		}
		sysBussinessAuthExpressionService.insert(authExpressionList);
		
		//保存流程关键字段的配置表
		ProcessDefinitionConfig processDefinitionConfig=new ProcessDefinitionConfig();
		processDefinitionConfig.setProcessDefinitionId(templeteId);
		List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(processDefinitionConfig);
		for(ProcessDefinitionConfig cfg:configs){
			List<ProcessDefinitionConfigDetail> details=null;
			if(cfg.getConfigType().equals("2")){ //流程变量
				ProcessDefinitionConfigDetail processDefinitionConfigDetail=new ProcessDefinitionConfigDetail();
				processDefinitionConfigDetail.setConfigId(cfg.getId());
				details=processDefinitionConfigDetailService.find(processDefinitionConfigDetail);
			}
			cfg.setId(null);
			cfg.setProcessDefinitionId(newProcessDefId);
			cfg.setCreateTime(null);
			cfg.setCreator(ComUtils.getLoginName());
			String newConfigId=processDefinitionConfigService.insert(cfg);
			//如果存在流程配置详情表，则处理，只针对configType==2有效
			if(details!=null&&details.size()>0){
				for(ProcessDefinitionConfigDetail detail:details){
					//流程变量绑定的权限表达式
					List<SysBussinessAuthExpression> sysBussinessAuthExpressions4Detail=sysBussinessAuthExpressionService.find("findByBusinessId",detail.getId());
					detail.setId(null);
					detail.setConfigId(newConfigId);
					String newDetailId=processDefinitionConfigDetailService.insert(detail);
					//插入流程变量对应的权限表达式
					for(SysBussinessAuthExpression authExpression:sysBussinessAuthExpressions4Detail){
						authExpression.setId(null);
						authExpression.setBusinessId(newDetailId);
					}
					sysBussinessAuthExpressionService.insert(sysBussinessAuthExpressions4Detail);
				}
			}
		}
		
		//保存一份流程图设计
		if(processDefinitionTemplete.getFilePath()!=null){
			processDefinitionTemplete=this.loadProcessDefinitionFile(processDefinitionTemplete);
			newProcessDefinition=processDefDao.get(newProcessDefId);  //重新获取一次记录，用于刷新数据的回显
			String fileString=processDefinitionTemplete.getProcessDefinitionFile();
//			fileString=fileString.replaceAll("process[0-9]{13}","process"+Calendar.getInstance().getTimeInMillis());
			fileString=fileString.replaceAll(templatePafId,newPafId);
			newProcessDefinition.setProcessDefinitionFile(fileString);
			this.saveProcessDefinitionToJss(newProcessDefinition);   //保存流程图到JSS
		}
		
		return newProcessDefId;
	}

	@Override
	@Transactional
	public void deleteProcessDefinition(String[] ids) {
		for(String id:ids){
			deleteProcessDefinition(id);
		}
	}

	@Override
	@Transactional
	public void deleteProcessDefinition(String id) {
		if(null==id||("").equals(id)) return;
		ProcessDefinition processDefinition=processDefDao.get(id);
		if(null==processDefinition) return;
		//判断是不是外部流程,外部流程直接删除
		if(processDefinition.getIsOuter().equals("1")){
			processDefDao.delete(processDefinition);
			return;
		}
		//查验改流程定义是否已发布
		if(processDefinition.getIsIssue()!=null && processDefinition.getIsIssue().equals("1")){
			throw new BusinessException("isSue");
		}
		
		//取得流程定义的结点列表
		ProcessNode node=new ProcessNode();
		node.setProcessDefinitionId(id);
		List<ProcessNode> nodeList=processNodeService.find(node);
		
		//遍历流程结点删除
		for(ProcessNode processNode:nodeList){
			
			//取得结点的权限表达式删除
			List<SysBussinessAuthExpression> nodeExpressionsList=sysBussinessAuthExpressionService.find("findByBusinessId",processNode.getId());
			sysBussinessAuthExpressionService.delete(nodeExpressionsList,true);
			
			//删除结点
			processNodeService.delete(processNode);
		}
		
		//取得流程定义的结点权限
		ProcessNodeFormPrivilege privilege=new ProcessNodeFormPrivilege();
		privilege.setProcessDefinitionId(id);
		List<ProcessNodeFormPrivilege> privilegeList=processNodeFormPrivilegeService.selectByCondition(privilege);
		//删除结点权限
		for(ProcessNodeFormPrivilege processNodeFormPrivilege:privilegeList){
			processNodeFormPrivilegeService.deleteByPrimaryKey(processNodeFormPrivilege);
		}
		
		//删除流程定义的权限表达式
//		sysBussinessAuthExpressionService.deleteByBusinessId(id);
		List<SysBussinessAuthExpression> list=sysBussinessAuthExpressionService.find("findByBusinessId",id);
		sysBussinessAuthExpressionService.delete(list,true);
		//删除流程关键字的配置
		ProcessDefinitionConfig processDefinitionConfig=new ProcessDefinitionConfig();
		processDefinitionConfig.setProcessDefinitionId(id);
		List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(processDefinitionConfig);
		List<ProcessDefinitionConfigDetail> details=new ArrayList<ProcessDefinitionConfigDetail>();
		for(ProcessDefinitionConfig cfg:configs){
			if(cfg.getConfigType().equals("2") || cfg.getConfigType().equals("3")){
				ProcessDefinitionConfigDetail processDefinitionConfigDetail=new ProcessDefinitionConfigDetail();
				processDefinitionConfigDetail.setConfigId(cfg.getId());
				details.addAll(processDefinitionConfigDetailService.find(processDefinitionConfigDetail));
			}
		}
		if(details.size()>0){
			for(ProcessDefinitionConfigDetail detail:details){
				List<SysBussinessAuthExpression> authlist=sysBussinessAuthExpressionService.find("findByBusinessId",detail.getId());
				sysBussinessAuthExpressionService.delete(authlist,true);
			}
			processDefinitionConfigDetailService.delete(details);
		}
		if(configs != null && configs.size() > 0){
			processDefinitionConfigService.delete(configs,true);
		}
		//删除JSS上流程图
		String key = processDefinition.getId()+".bpmn20.xml";
		jssService.deleteObject(SystemConstant.BUCKET, key);
		//删除流程定义主表
		processDefDao.delete(processDefinition);
	}

	@Override
	public List<ProcessDefinition> getPrcocessDefinitionListByType(String typeId) {
		ProcessDefinition processDefinition=new ProcessDefinition();
		processDefinition.setProcessTypeId(typeId);
		processDefinition.setIsTemplate("1");
//		processDefinition.setCreator(ComUtils.getLoginName());
		processDefinition.setOwner(sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin()));
		return processDefDao.find(processDefinition);
	}

	@Override
	@Transactional
	public void deployProccessDef(List<ProcessDefinition> processDefinitionList) {
		for(ProcessDefinition processDefinition : processDefinitionList){
            processDefinition = processDefDao.get(processDefinition.getId());
            File file = jssService.getFile(SystemConstant.BUCKET, processDefinition.getFilePath());
            boolean flag = pafProcessDeployService.deploy(file);
            if(!flag){
                throw new BusinessException("流程发布失败。");
            }
            processDefinition.setIsIssue("1");
            processDefDao.update(processDefinition);
		}
	}

	@Override
	@Transactional
	public void saveToProcessDefTemplete(String[] ids) {
		for(String id:ids){
			saveToProcessDefTemplete(id);
		}
	}

	@Override
	@Transactional
	public void saveToProcessDefTemplete(String id) {
		if(null==id||("").equals(id)) return;
		ProcessDefinition processDefinition=processDefDao.get(id);
		if(null==processDefinition) return;
		
		//保存为一份模板
		processDefinition.setProcessDefinitionName("[模板]["+DateUtils.datetimeFormat(new Date())+"]"+processDefinition.getProcessDefinitionName());
		this.addOrUpdateProcessDefinition(processDefinition,processDefinition.getId());
	}

    /**
     * 保存流程定义文件到JSS
     * @param processDefinition 流程定义实体对象
     */
    @Transactional
    public void saveProcessDefinitionToJss(ProcessDefinition processDefinition){
        String key = processDefinition.getId()+".bpmn20.xml";
        jssService.deleteObject(SystemConstant.BUCKET, key);
        jssService.uploadFile(SystemConstant.BUCKET, key, processDefinition.getProcessDefinitionFile());
        processDefinition.setFilePath(key);
        processDefinition.setModifyTime(new Date());
        processDefDao.update(processDefinition);

        List<ProcessNode> processNodeList = JsonUtils.fromJsonByGoogle(processDefinition.getAllUserTask(), new TypeToken<List<ProcessNode>>() {
        });
        if(processNodeList != null && processNodeList.size()>0){
            processDefinition.setProcessNodeList(processNodeList);
			for(ProcessNode processNode:processNodeList){
				processNode.setProcessDefinitionId(processDefinition.getId());
				processNode.setModifier(ComUtils.getLoginName());
				processNode.setModifyTime(new Date());
				processNodeService.update("updateCountSignNode",processNode);
			}
            processDefDao.deleteProcessNodeConfig(processDefinition);                              //删除流程节点配置
            processDefDao.deleteProcessNodeFormPrivilegeConfig(processDefinition);                 //删除流程节点表单个性化配置配置
        }
    }

    /**
     * 读取流程定义文件
     * @param processDefinition 流程定义实体对象
     */
    public ProcessDefinition loadProcessDefinitionFile(ProcessDefinition processDefinition){
        processDefinition = processDefDao.get(processDefinition.getId());
        if(processDefinition.getFilePath()==null) return processDefinition;
        InputStream inputStream = jssService.downloadFile(SystemConstant.BUCKET,processDefinition.getFilePath());
        StringBuffer buffer = new StringBuffer();
        try {
            byte[] bytes = new byte[inputStream.available()];
            while(inputStream.read(bytes) != -1){
                buffer.append(new String(bytes,"UTF-8"));
            }            
            processDefinition.setProcessDefinitionFile(buffer.toString());
        } catch (IOException e) {
              throw new BusinessException("读取流程定义文件失败", e);
        } finally {
        	try{
        		inputStream.close();
        	} catch(IOException e) {
        	}
        }
        return processDefinition;
    }

	@Override
	public void changeOwner(String[] resIds, String ownerId) {
		for(String id:resIds){
			ProcessDefinition processDefinition=processDefDao.get(id);
			//需要变更所有的权限表达式给需要变更的人
			List<SysBussinessAuthExpression> authList=sysBussinessAuthExpressionService.find("findByBusinessId",id);
			for(SysBussinessAuthExpression bussinessAuthExpression :authList){
				SysAuthExpression expression=authExpressionService.get(bussinessAuthExpression.getAuthExpressionId());
				expression.setId(null);
				SysUser sysUser=sysUserService.get(expression.getOwner());
				expression.setAuthExpressionName(expression.getAuthExpressionName()+"[From:"+sysUser.getRealName()+"]");
				expression.setOwner(ownerId);
				String newId=authExpressionService.insert(expression);
				bussinessAuthExpression.setAuthExpressionId(newId);
				sysBussinessAuthExpressionService.update(bussinessAuthExpression);
			}
			processDefinition.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
			processDefinition.setOwner(ownerId);
			processDefDao.update(processDefinition);
		}
	}
	
	public ProcessDefinition getByDefinitionId(String pafProcessDefinitionId){
		ProcessDefinition processDefinition = new ProcessDefinition();
		processDefinition.setPafProcessDefinitionId(pafProcessDefinitionId);
		List<ProcessDefinition> list = find("findByDefinitionId",processDefinition);
		if(list!=null && list.size()>0)
			return list.get(0);
		
		return null;
		
	}

	@Override
	public List<ProcessDefinition> findByProcessTypeIds(
			List<String> processTypeIds) {
		return processDefDao.findByProcessTypeIds(processTypeIds);
	}

	@Override
	public List<ProcessDefinition> findByProcessType(String processType) {
		List<String> processTypeIdList = new ArrayList<String>();
		processTypeIdList.add(processType);
		ProcessType parentProcessType = new ProcessType();
		parentProcessType.setParentId(processType);
		List<ProcessType> sonProcessTypeList = processTypeDao.find(parentProcessType);
		if(sonProcessTypeList !=null && sonProcessTypeList.size() > 0){
			for(ProcessType type:sonProcessTypeList){
				processTypeIdList.add(type.getId());
			}
		}
		List<ProcessDefinition> processDefinitions = this.findByProcessTypeIds(processTypeIdList);
		return processDefinitions;
	}
}

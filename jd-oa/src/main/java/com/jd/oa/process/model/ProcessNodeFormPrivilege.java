package com.jd.oa.process.model;

import com.jd.oa.common.model.IdModel;

import java.util.Date;

/**
 * 节点表单权限配置实体类
 */
public class ProcessNodeFormPrivilege  extends IdModel {

    /**
     * 流程定义ID
     */
    private String processDefinitionId;

    /**
     * 节点ID
     */
    private String nodeId;

    /**
     * 表单ID
     */
    private String formId;

    /**
     * 表单项ID
     */
    private String itemId;

    /**
     * 是否可修改
     */
    private String isEdit;

    /**
     * 是否必填
     */
    private String isNull;

    /**
     * 是否可查看
     */
    private String isHidden;

    /**
     * 是否是流程变量
     */
    private String isVariable;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 逻辑标识
     */
    private int yn;

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getEdit() {
        return isEdit;
    }

    public void setEdit(String edit) {
        isEdit = edit;
    }

    public String getNull() {
        return isNull;
    }

    public void setNull(String aNull) {
        isNull = aNull;
    }

    public String getHidden() {
        return isHidden;
    }

    public void setHidden(String hidden) {
        isHidden = hidden;
    }

    public String getVariable() {
        return isVariable;
    }

    public void setVariable(String variable) {
        isVariable = variable;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getYn() {
        return yn;
    }

    public void setYn(int yn) {
        this.yn = yn;
    }
}
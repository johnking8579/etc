package com.jd.oa.process.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessDefinitionConfigDetailDao;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.system.service.UserAuthorizationService;

@Service("processDefinitionConfigDetailService")
@Transactional
public class ProcessDefinitionConfigDetailServiceImpl extends BaseServiceImpl<ProcessDefinitionConfigDetail, String> implements ProcessDefinitionConfigDetailService{
	@Autowired
	private ProcessDefinitionConfigDetailDao processDefinitionConfigDetailDao;
	
	@Autowired
	private SysAuthExpressionService sysAuthExpressionService;
	@Autowired
	private UserAuthorizationService userAuthorizationService;
	
	@Override
	public ProcessDefinitionConfigDetailDao getDao() {
		return processDefinitionConfigDetailDao;
	}
	
	public List<ProcessDefinitionConfigDetail> findByMap(String processDefinitionId, String configItem, String configType) {
		return processDefinitionConfigDetailDao.findByMap(processDefinitionId, configItem, configType);
	}
	
	public String getSubmitUsers(String configId, Map<String,Object> businessData){
		String ConfigDetailId = getConfigDetailId(configId, businessData);
		if (StringUtils.isNotEmpty(ConfigDetailId)){
			List<SysAuthExpression> saeList = sysAuthExpressionService.findExpressionByBusinessIdAndBusinessType(SysBussinessAuthExpression.BUSSINESS_TYPE_VARIABLE, ConfigDetailId);
			if (saeList!=null && saeList.size()>0){
				Map<String, String> map = userAuthorizationService.getUserMapBySysAuthExpressionList(saeList);
				if (map != null) 
					return map.get("usersErpid");
				else
					return null;
			}else{
				return null;
			}
		}else{
			return null;
		}			
	}	
	
	
	public String getConfigDetailId(String configId, Map<String,Object> businessData){
		ProcessDefinitionConfigDetail pdcdEntity = new ProcessDefinitionConfigDetail();
		pdcdEntity.setConfigId(configId);
    	List<ProcessDefinitionConfigDetail> pdcdList = processDefinitionConfigDetailDao.find(pdcdEntity);
    	if (pdcdList!= null && pdcdList.size()>0){
    		for (ProcessDefinitionConfigDetail pdcd : pdcdList){
    			if (StringUtils.isNotEmpty(pdcd.getExpression())){
	    			boolean bln = parseExp(pdcd.getExpression(), businessData);
	    			
	    			if (bln) 
	    				return pdcd.getId();
    			}else{
    				return null;
    			}
    		}    		
    	}
    	return null;
	}
	
	/**
	 * 
	 * @desc 拆分与或表达式,递归调用
	 * @author WXJ
	 * @date 2013-12-13 下午01:18:47
	 *
	 * @param first
	 * @param exp
	 * @param businessData
	 * @return
	 */
	private boolean parseExp(String exp, Map<String,Object> businessData){    	
    	boolean bln = true;
    	String andStr = " and ";
    	String orStr = " or ";    	   	
    	String[] tempArray;
    	
    	exp = exp.trim();
		tempArray = exp.split(andStr);
		if (tempArray.length>1){
			bln = true;
			for (int i=0;i<tempArray.length;i++){
				bln = bln && parseExp(tempArray[i], businessData);
			}
		}else{			
			tempArray = exp.split(orStr);
			if (tempArray.length>1){
				bln = false;
				for (int i=0;i<tempArray.length;i++){
					bln = bln || parseExp(tempArray[i], businessData);
				}
			}else{
				bln = compareResult(exp, businessData);
			}
		}	
		
		return bln;
    }
    
	/**
	 * 
	 * @desc 拆分单个比较表达式，进行比较
	 * @author WXJ
	 * @date 2013-12-13 下午01:19:57
	 *
	 * @param exp
	 * @param businessData
	 * @return
	 */
    private boolean compareResult(String exp, Map<String,Object> businessData){    		
    	String[] compareVal = {">",">=","<","<=","==","!="};
    	for (int i=0;i<compareVal.length;i++){
    		int pos = exp.indexOf(compareVal[i]);
    		if (pos>0){
    			if (exp.substring(pos+compareVal[i].length()).indexOf("=")<0){
	    			String key = exp.substring(0,pos).trim();
	    			String oper = exp.substring(pos,pos+compareVal[i].length()).trim();
	    			String val = exp.substring(pos+compareVal[i].length()).trim();
	    			if (businessData.get(key)!=null)
	    				return doCalc(businessData.get(key).toString(), val, oper);
	    			else
	    				return false;
    			}
    		}
    	}
    	return false;
    }
    
    /**
     * 
     * @desc 值比较
     * @author WXJ
     * @date 2013-12-13 下午01:20:45
     *
     * @param d1
     * @param d2
     * @param oper
     * @return
     */
    private boolean doCalc(String d1, String d2, String oper) { 
    	try {
        	String[] compareVal = {">",">=","<","<=","==","!="};
            if (">".equals(oper)){
            	return Double.parseDouble(d1) > Double.parseDouble(d2);
            }
            if (">=".equals(oper)){
            	return Double.parseDouble(d1) >= Double.parseDouble(d2);
            }
            if ("<".equals(oper)){
            	return Double.parseDouble(d1) < Double.parseDouble(d2);
            }
            if ("<=".equals(oper)){
            	return Double.parseDouble(d1) <= Double.parseDouble(d2);
            }
            if ("==".equals(oper)){
	            try {
	            	 double dd1 = Double.parseDouble(d1);
	            	 double dd2 = Double.parseDouble(d2);
	                 return dd1 == dd2;	                 
				} catch (NumberFormatException e) {
	                return d1.equals(d2);
				}
            }
            if ("!=".equals(oper)){
            	 try {
	            	 double dd1 = Double.parseDouble(d1);
	            	 double dd2 = Double.parseDouble(d2);
	                 return dd1 != dd2;	                 
				} catch (NumberFormatException e) {
	                return !d1.equals(d2);
				}
            }
		} catch (Exception e) {
			return false;     
		}
            
		return false;     
    }  
    
}

package com.jd.oa.process.dao;

import java.util.List;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessDefinition;

public interface ProcessDefDao extends BaseDao<ProcessDefinition, String> {
	/**
	 * 
	 * @desc 通过流程实例获得流程定义相关信息
	 * @author WXJ
	 * @date 2013-10-14 下午03:14:11
	 *
	 * @param ProcessInstance
	 * @return
	 */
	public ProcessDefinition findProcessDefByInstance(String processInstance);
	
	/**
	 * 
	 * @desc 查询出所有正常状态的流程
	 * @author WXJ
	 * @date 2013-10-14 下午05:56:59
	 *
	 * @return
	 */
	public List<ProcessDefinition> findProcessDefAll();
	
	/**
	 * 
	 * @desc 根据流程定义Id获取流程定义列表
	 * @author zhouhuaqi
	 * @date 2013-10-26 下午05:56:59
	 *
	 * @return
	 */
	public List<ProcessDefinition> findByProcessTypeIds(List<String> processTypeIds);

    /**
     * 删除流程节点配置
     * @param processDefinition 流程定义实体
     */
    public void  deleteProcessNodeConfig(ProcessDefinition processDefinition);

    /**
     * 删除流程节点表单个性化配置配置
     * @param processDefinition 流程定义实体
     */
    public void  deleteProcessNodeFormPrivilegeConfig(ProcessDefinition processDefinition);
    
    ProcessDefinition findByPafDefId(String pafProcessDefId);
	
}

package com.jd.oa.process.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessNodeExtendButton;

public interface ProcessNodeExtendButtonService extends BaseService<ProcessNodeExtendButton, String> {
	
	/**
	 * 根据条件查询结点扩展按钮配置
	 * 
	 * @param processNodeExtendButton
	 * @return
	 */
	public List<ProcessNodeExtendButton> selectByCondition(ProcessNodeExtendButton processNodeExtendButton);
	
	/**
	 * 自动创建标准按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 */
	public void createStandardButtons(String formId,String processId,String nodeId);

}

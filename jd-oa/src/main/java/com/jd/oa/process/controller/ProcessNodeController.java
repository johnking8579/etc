package com.jd.oa.process.controller;

import com.jd.common.util.StringUtils;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.common.utils.PlatformProperties;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeListenerService;
import com.jd.oa.process.service.ProcessNodeService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;




@Controller
@RequestMapping("/process")
public class ProcessNodeController {
	private static final Logger logger = Logger.getLogger(ProcessNodeController.class);
	
	@Autowired
	private ProcessNodeService processNodeService;
	
	@Autowired
	private ProcessDefService processDefService ;
	
	@Autowired
	private DiCcInterfaceService diCcInterfaceService ;
	
	@Autowired
	private ProcessNodeListenerService processNodeListenerService ;
	
	@Resource
    private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
    @Resource
    private FormItemService formItemService;
    @Resource
    private FormTemplateService formTemplateService;
    
    @Resource
    private ProcessDefinitionConfigService processDefinitionConfigService;
    
    @Resource
    private DiDatasourceService diDatasourceService;
    
    @Resource
    private ProcessNodeExtendButtonService processNodeExtendButtonService;
    
    @Resource
    private FormService formService;
    
  //查询URL
	@RequestMapping(value = "/processDef_getVariables")
	@ResponseBody
	public List<ProcessDefinitionConfig> processDef_getVariables(String processDefId) {
		ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
		pdc.setProcessDefinitionId(processDefId);
		pdc.setConfigType("2");
		
		List<ProcessDefinitionConfig> list = processDefinitionConfigService.find(pdc);
	    return list;
	}
	
    
	
	@RequestMapping(value = "/processNode_index")
	 public ModelAndView processNode_index(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
	    
	    	ModelAndView mav = new ModelAndView("process/processNode_index");
			
			return mav;
	    }
	
	
	//查询URL
	@RequestMapping(value = "/processNode_view")
	@ResponseBody
	 public String processNode_view(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
			//创建pageWrapper
			PageWrapper<ProcessNode> pageWrapper=new PageWrapper<ProcessNode>(request);
			
			//添加搜索条件
			String processDefinitionId=request.getParameter("processDefinitionId");
			if(processDefinitionId!=null && !("").equals(processDefinitionId))
				pageWrapper.addSearch("processDefinitionId", processDefinitionId);
			processNodeService.find(pageWrapper.getPageBean(),"findByMap", pageWrapper.getConditionsMap());
			
			 //返回到页面的额外数据
		    //pageWrapper.addResult("returnKey","returnValue");

		    String json=JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			
			
	        logger.debug(json);
	        return json;
	    }
	
	
	
	@RequestMapping(value = "/processNode_addSign")
	 public ModelAndView processNode_addSign(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response,
	    		@RequestParam(value = "nodeId", required = false) String nodeId,
	    		@RequestParam(value = "processId", required = false) String processId) throws Exception {
	    
	    	ModelAndView mav = new ModelAndView("process/processNode_addSign");
			
	    	Map map = new HashMap<String, String>();
	    	if(nodeId!=null && !"".equals(nodeId) && processId!=null && !"".equals(processId)){
	    		map.put("nodeId", nodeId);
	    		map.put("processDefinitionId", processId);
	    		List<ProcessNode> list = processNodeService.find("findByMap",map);
	    		if(list.size()>0){
	    			mav.addObject("addSignRule",list.get(0).getAddsignRule());
	    		}
	    	}
	    	
			return mav;
	    }

	//获取当前加签规则
	@RequestMapping(value = "/processNode_addSignView")
	 public ModelAndView processNode_addSignView(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response,
	    		@RequestParam(value = "nodeId", required = false) String nodeId) throws Exception {
	    
	    	ModelAndView mav = new ModelAndView("process/processNode_addSign");

			return mav;
	    }
	
	//加签规则Ajax，新增或更新流程节点
	@RequestMapping(value = "/processNode_addSignUpdate")
	@ResponseBody
	 public String processNode_addSignUpdate(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		@RequestParam(value = "nodeId", required = false) String nodeId,
	    		@RequestParam(value = "processId", required = false) String processId,
	    		@RequestParam(value = "managerNodeId", required = false) String managerNodeId,
	    		@RequestParam(value = "candidateUser", required = false) String candidateUser,	    		
	    		@RequestParam(value = "isFirstNode", required = false) String isFirstNode,
	    		@RequestParam(value = "addsignRule", required = false) String addsignRule,
	    		@RequestParam(value = "allUpdate", required = false) String allUpdate,
	    		@RequestParam(value = "timerDelay", required = false) String timerDelay,
	    		@RequestParam(value = "candidateUserType", required = false) String candidateUserType,
	    		HttpServletRequest request,
	    		HttpServletResponse response) throws Exception {
	    	
			int num=0;
			Map map = new HashMap<String, String>();
    		map.put("nodeId", nodeId);
    		map.put("processDefinitionId", processId);
    		List<ProcessNode> list = processNodeService.find("findByMap",map);
    		ProcessNode node=null ;
    		if(list!=null && list.size() >0)
    			node= list.get(0);
    		
    		
    		//如果数据库没有该节点，则新插入一个
    		if(node==null){
    			node = new ProcessNode();
    			node.setNodeId(nodeId);
    			node.setProcessDefinitionId(processId);
    			
    			processNodeService.insert(node);
    		}
    		
    		//空字符串表示办理人为表达式范围
    		if(managerNodeId!=null )
				node.setManagerNodeId(managerNodeId);
    		//办理人变量
			if(candidateUser!=null )
				node.setCandidateUser(candidateUser);
			
			if(isFirstNode!=null ){
				node.setIsFirstNode(isFirstNode);
				//如果设置为首节点，需要进行判断
				if(isFirstNode.equals("1")){
					if(checkIfHasFirstNode(processId , nodeId))
						return Integer.valueOf(-1).toString();
				}
			}
    		
			//设置自动执行延时
			if(timerDelay!=null ){
				node.setTimerDelay(Integer.valueOf(timerDelay));
			}
			
			if(candidateUserType != null)
				node.setCandidateUserType(candidateUserType);
			
			//如果仅一个节点需要更新加签规则，直接更新即可
	    	if(allUpdate==null ){
	    		node.setAddsignRule(addsignRule);
	    		num = processNodeService.update("updateAddSignRuleByNodeId", node);
	    	}
	    	//如果更新到所有节点，则需要检索出所有的节点
	    	else if( ("1").equals(allUpdate)){

	    		//根据流程定义id查出所有符合的节点
	    		map.clear();
	    		if(node.getProcessDefinitionId()!=null){
		    		map.put("processDefinitionId",node.getProcessDefinitionId());
		    		List<ProcessNode> list2 = processNodeService.find("findByMap",map);
		    		for (ProcessNode item : list2) {
						item.setAddsignRule(addsignRule);
						num= num+ processNodeService.update("updateAddSignRuleByNodeId", item);
					}
	    		
	    		}
	    	}
			
			return new Integer(num).toString();
	    }
		
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-23 16下午08:30:17
	 *
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value = "/processNode_candidateUser")
	public ModelAndView processNode_candidateUser(String formId, String processNodeId, String processId, String nodeId){
        ModelAndView mav = new ModelAndView("/process/processNode_candidateUser");
        mav.addObject("formId", formId);
        mav.addObject("processNodeId", processNodeId);
        mav.addObject("processId", processId);
        mav.addObject("nodeId", nodeId);
        
        //查看办理人类型
        Map map = new HashMap<String, String>();
		map.put("nodeId", nodeId);
		map.put("processDefinitionId", processId);
		List<ProcessNode> list = processNodeService.find("findByMap",map);
		ProcessNode node=null ;
		if(list!=null && list.size() >0){
			node= list.get(0);
			mav.addObject("managerNodeId",node.getManagerNodeId());
			mav.addObject("candidateUser",node.getCandidateUser());
			mav.addObject("candidateUserType",node.getCandidateUserType());
		}
		return mav;
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-23 下午08:31:39
	 *
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value = "/processNode_email")
	public ModelAndView processNode_email(String formId, String processId, String nodeId){
        ModelAndView mav = new ModelAndView("/process/processNode_email");
        mav.addObject("formId", formId);
        mav.addObject("processId", processId);
        mav.addObject("nodeId", nodeId);
        
        String mailFrom = PlatformProperties.getProperty("mail.smtp.default.from");
        mav.addObject("mailFrom", mailFrom);
        
		return mav;
	}	
	
	
	
	@RequestMapping(value = "/processNode_listener")
	public ModelAndView processNode_listener(String formId, String processId, String nodeId){
        ModelAndView mav = new ModelAndView("/process/processNode_listener");

        List<DiDatasource> list = diDatasourceService.find(new DiDatasource());
        
        
        ProcessNodeListener listener = new ProcessNodeListener();
		listener.setProcessDefinitionId(processId);
		listener.setNodeId(nodeId);
		List<ProcessNodeListener> list2 = processNodeListenerService.find(listener);
		List<ProcessNodeListener> beginEventListenrs = new ArrayList<ProcessNodeListener>();
		List<ProcessNodeListener> endEventListenrs = new ArrayList<ProcessNodeListener>();
		List<ProcessNodeListener> rollbackEventListenrs = new ArrayList<ProcessNodeListener>();
		if(list2 != null){
			for (ProcessNodeListener processNodeListener : list2) {
				if("b".equals(processNodeListener.getEvent())){
					beginEventListenrs.add(processNodeListener);
				}else if("e".equals(processNodeListener.getEvent())){
					endEventListenrs.add(processNodeListener);
				}else if("r".equals(processNodeListener.getEvent())){
					rollbackEventListenrs.add(processNodeListener);
				}
			}
		}
		mav.addObject("beginEvent", beginEventListenrs);
		mav.addObject("endEvent", endEventListenrs);
		mav.addObject("rollbackEvent", rollbackEventListenrs);
        mav.addObject("diDatasourceList", list);
        mav.addObject("processId", processId);
        mav.addObject("nodeId", nodeId);
		return mav;
	}
	
	@RequestMapping(value = "/processNode_timer")
	 public ModelAndView processNode_timer(
	    		@RequestParam(value = "locale", required = false) Locale locale,
	    		HttpServletRequest request,
	    		HttpServletResponse response,
	    		@RequestParam(value = "nodeId", required = false) String nodeId,
	    		@RequestParam(value = "processId", required = false) String processId) throws Exception {
	    
    	ModelAndView mav = new ModelAndView("process/processNode_timer");
		
    	Map map = new HashMap<String, String>();
    	if(nodeId!=null && !"".equals(nodeId) && processId!=null && !"".equals(processId)){
    		map.put("nodeId", nodeId);
    		map.put("processDefinitionId", processId);
    		List<ProcessNode> list = processNodeService.find("findByMap",map);
    		if(list.size()>0){
    			mav.addObject("timerDelay",list.get(0).getTimerDelay());
    		}
    	}
    	
		return mav;
    }
	
	@RequestMapping(value = "/processNodeListener_addOrUpdate")
	@ResponseBody
	 public String processNodeListener_addOrUpdate(
	    		@RequestParam(value = "processId", required = false) String processId,
	    		@RequestParam(value = "nodeId", required = false) String nodeId,
	    		@RequestParam(value = "begin_interface", required = false) String begin_interface,	    		
	    		@RequestParam(value = "end_interface", required = false) String end_interface,
	    		@RequestParam(value = "rollback_interface", required = false) String rollback_interface) throws Exception {
		
		
		List<ProcessNodeListener> list ;
		
		int count = 0;
		if(begin_interface !=null ){

			ProcessNodeListener blistener = new ProcessNodeListener();
			blistener.setProcessDefinitionId(processId);
			blistener.setNodeId(nodeId);
			blistener.setEvent("b");
			
			list = processNodeListenerService.find(blistener);
			if(list != null){
				for (ProcessNodeListener processNodeListener : list) {
					processNodeListenerService.delete(processNodeListener.getId());
				}
			}
			if(!StringUtils.isEmpty(begin_interface)){
				String[] beginInterfaceIds = begin_interface.split(",");
				for(String s: beginInterfaceIds){
					if(!StringUtils.isEmpty(s)){
						blistener.setId(null);
						blistener.setInterfaceName(s);
						String id= processNodeListenerService.insert(blistener);
						if(id != null) count ++ ;
					}
				}
			}
		}
		
		if(end_interface !=null ){
			ProcessNodeListener elistener = new ProcessNodeListener();
			elistener.setProcessDefinitionId(processId);
			elistener.setNodeId(nodeId);
			elistener.setEvent("e");
			
			list = processNodeListenerService.find(elistener);
			if(list != null){
				for (ProcessNodeListener processNodeListener : list) {
					processNodeListenerService.delete(processNodeListener.getId());
				}
			}
			if(!StringUtils.isEmpty(end_interface)){
				String[] endInterfaceIds = end_interface.split(",");
				for(String s: endInterfaceIds){
					if(!StringUtils.isEmpty(s)){
						elistener.setId(null);
						elistener.setInterfaceName(s);
						String id= processNodeListenerService.insert(elistener);
						if(id != null) count ++ ;
					}
				}
			}

		}
		
		if(rollback_interface !=null ){
			ProcessNodeListener elistener = new ProcessNodeListener();
			elistener.setProcessDefinitionId(processId);
			elistener.setNodeId(nodeId);
			elistener.setEvent("r");
			
			list = processNodeListenerService.find(elistener);
			if(list != null){
				for (ProcessNodeListener processNodeListener : list) {
					processNodeListenerService.delete(processNodeListener.getId());
				}
			}
			if(!StringUtils.isEmpty(rollback_interface)){
				String[] rollbackInterfaceIds = rollback_interface.split(",");
				for(String s: rollbackInterfaceIds){
					if(!StringUtils.isEmpty(s)){
						elistener.setId(null);
						elistener.setInterfaceName(s);
						String id= processNodeListenerService.insert(elistener);
						if(id != null) count ++ ;
					}
				}
			}
		}
		
		return Integer.valueOf(count).toString();
	}
	
	@RequestMapping(value = "/processNode_dynamicButton")
	public ModelAndView processNode_dynamicButton(String formId, String processDefinitionId, String nodeId){
		ModelAndView mav = new ModelAndView("/process/processNode_dynamicButton");
		
		mav.addObject("formId", formId);
		mav.addObject("processId", processDefinitionId);
        mav.addObject("nodeId", nodeId);
		return mav;
	}
	
	@RequestMapping(value="/processDef_getFullForms",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getFullForms(String formId){
		List<Form> formlist = formService.findFormWithSubForm(formId);
		return formlist;
	}
	
	/**
	 * 动态按钮配置列表
	 * @param request
	 * @param nodeId
	 * @param processDefinitionId
	 * @return
	 */
	@RequestMapping(value = "/processNode_dynamicButtonPage",produces = "application/json")
	@ResponseBody
	public Object page(HttpServletRequest request,@RequestParam String nodeId,@RequestParam String processDefinitionId){
		//创建pageWrapper
        PageWrapper<ProcessNodeExtendButton> pageWrapper = new PageWrapper<ProcessNodeExtendButton>(request);
        if (null != nodeId && !nodeId.equals("")) {
            pageWrapper.addSearch("nodeId", nodeId);
        }
        if (null != processDefinitionId && !processDefinitionId.equals("")) {
            pageWrapper.addSearch("processDefinitionId", processDefinitionId);
        }
        //后台取值
        processNodeExtendButtonService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        
		//String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
		//logger.debug("result:" + json);
		//return json;
        return pageWrapper.getResult();
	}
	
	/**
	 * 动态按钮添加页面
	 * @param response
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value = "/extendButton_add")
	public ModelAndView extendButtonAdd(HttpServletResponse response, String formId,String processId,String nodeId){
		ModelAndView mav = new ModelAndView("/process/processNode_dynamicButtonAddPage");
		mav.addObject("processNodeExtendButton", JsonUtils.toJsonByGoogle(new ProcessNodeExtendButton()));
		mav.addObject("processId", processId);
		mav.addObject("nodeId", nodeId);
		mav.addObject("formId", formId);
		return mav;
	}
	
	/**
	 * 动态按钮修改页面
	 * @param id
	 * @param formId
	 * @return
	 */
	@RequestMapping(value="/extendButton_update",method=RequestMethod.GET)
	public ModelAndView extendButtonUpdate(String id,String formId){
		ModelAndView mav=new ModelAndView("process/processNode_dynamicButtonAddPage");
		if(null!=id&&!("").equals(id)){
			ProcessNodeExtendButton processNodeExtendButton = processNodeExtendButtonService.get(id);
			if(null!=processNodeExtendButton){
				String str = "";
				try {
					str = JsonUtils.toJson(processNodeExtendButton);
				} catch (IOException e) {
					e.printStackTrace();
				}
				mav.addObject("processNodeExtendButton", str);
				mav.addObject("processId", processNodeExtendButton.getProcessDefinitionId());
				mav.addObject("nodeId", processNodeExtendButton.getNodeId());
			}
		}
		mav.addObject("formId", formId);
		return mav;
	}
	
	/**
	 * 动态按钮添加、修改
	 * @param entity
	 * @return
	 */
	@RequestMapping(value="/extendButton_insert",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object extendButtonAddSave(ProcessNodeExtendButton entity){
		Map<String,Object> resultMap=new HashMap<String, Object>();
		String id = entity.getId();
		entity.setYn(0);
		entity.setStatus("0");
		try {
			if(id.equals("")){
				entity.setId(null);
				processNodeExtendButtonService.insert(entity);
			}else{
				processNodeExtendButtonService.update(entity);
			}
			resultMap.put("operator", true);
			resultMap.put("message","操作成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","操作失败");
		}
		return resultMap;
	}
	
	/**
	 * 自动创建标准按钮
	 * 
	 * @param formId
	 * @param processId
	 * @param nodeId
	 * @return
	 */
	@RequestMapping(value = "/extendButton_addStandardButtons",method=RequestMethod.POST)
	@ResponseBody
	public Object addStandardButtons(String formId,String processId,String nodeId){
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			processNodeExtendButtonService.createStandardButtons(formId, processId, nodeId);
			resultMap.put("operator", true);
			resultMap.put("message","操作成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message","操作失败");
		}
		return resultMap;
	}
	
	/**
	 * 动态按钮删除
	 * @param entity
	 * @return
	 */
	@RequestMapping(value="/extendButton_delete",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object extendButtonDelete(ProcessNodeExtendButton entity){
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			//逻辑删除
			processNodeExtendButtonService.delete(entity,false);
			resultMap.put("operator", true);
			resultMap.put("message","删除成功");
		} catch (Exception e) {
			logger.error("error", e);
			resultMap.put("operator", false);
			resultMap.put("message","删除失败");
			
		}
		return resultMap;
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-9-23 上午09:36:42
	 *
	 * @return
	 */
	@RequestMapping(value="/processDef_nodeTab",method=RequestMethod.GET)
	public ModelAndView processDef_nodeTab(String formId, String processId, String nodeId, String allUserTask){
        ModelAndView mav = new ModelAndView("/process/processDef_nodeTab");
//        String all=null;
//        try {
//			 all = new String(allUserTask.getBytes("ISO-8859-1") ,"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}   
        
        try {
			allUserTask = java.net.URLDecoder.decode(allUserTask,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new BusinessException("流程节点配置页面加载失败", e);
		} 
        
        //新增Node基础数据，并返回processNodeId
        Map map = new HashMap<String, String>();
        map.put("processDefinitionId", processId);
		map.put("nodeId", nodeId);
    	List<ProcessNode> pNList = processNodeService.find("findByMap",map);
    	if (pNList!=null && pNList.size()>0) {
    		ProcessNode processNodeInstance = pNList.get(0);
    		mav.addObject("processNodeId", processNodeInstance.getId());
    		mav.addObject("isFirstNode", processNodeInstance.getIsFirstNode());
    	} else {
    		ProcessNode node = new ProcessNode();
    		node.setProcessDefinitionId(processId);
			node.setNodeId(nodeId);			
			String processNodeId = processNodeService.insert(node);
			mav.addObject("processNodeId", processNodeId);
    	}
    	
        mav.addObject("formId", formId);
        mav.addObject("processId", processId);
        mav.addObject("nodeId", nodeId);
        mav.addObject("allUserTask", allUserTask);
       
		return mav;
	}
	
	/**
     * 节点表单权限配置页
     */
    @RequestMapping(value = "/processNode_FormPrivilegeIndex", method = RequestMethod.GET)
    public ModelAndView formPrivilegeIndex(ProcessNodeFormPrivilege processNodeFormPrivilege,String formId){
        ProcessNode processNode = new ProcessNode();
        processNode.setProcessDefinitionId(processNodeFormPrivilege.getProcessDefinitionId());
        processNode.setNodeId(processNodeFormPrivilege.getNodeId());
        List<ProcessNode> processNodeList = processNodeService.find(processNode);
        if(processNodeList!=null && processNodeList.size()>0){
            processNode = processNodeList.get(0);
        }
        // 没有定义表单模板。则默认使用申请节点的表单模板 -修改 yhb
        if(StringUtils.isEmpty(processNode.getFormTemplateId()))   {
            ProcessNode firstNode = new ProcessNode();
            firstNode.setProcessDefinitionId(processNodeFormPrivilege.getProcessDefinitionId());
            firstNode.setIsFirstNode("1");
            List<ProcessNode> firstNodeList = processNodeService.find(firstNode);
            if(firstNodeList!=null && firstNodeList.size()>0){
                firstNode = firstNodeList.get(0);
            }
            processNode.setPrintTemplateId(firstNode.getPrintTemplateId());
            processNode.setFormTemplateId(firstNode.getFormTemplateId());
        }
//        根据表单ID获取表单模板列表
        List<FormTemplate> formTemplateList = formTemplateService.find(processNodeFormPrivilege.getFormId(),"0");
//        根据表单ID获取打印模板列表
        List<FormTemplate> printTemplateList = formTemplateService.find(processNodeFormPrivilege.getFormId(),"1");

//        根据表单ID获取表单主表对象
        List<FormItem> formItemList = formItemService.getFormItemByFormIdAndType(processNodeFormPrivilege.getFormId(), SystemConstant.FORM_TYPE_M);
        for(FormItem formItem : formItemList){
            processNodeFormPrivilege.setItemId(formItem.getId());
            List<ProcessNodeFormPrivilege> processNodeFormPrivilegeList = processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
            for(ProcessNodeFormPrivilege pnfp : processNodeFormPrivilegeList){
                formItem.setProcessNodeFormPrivilegeId(pnfp.getId());
                formItem.setIsHidden(pnfp.getHidden());
                formItem.setIsNull(pnfp.getNull());
                formItem.setIsEdit(pnfp.getEdit());
                formItem.setIsVariable(pnfp.getVariable());
            }
        }
//        根据表单ID获取表单子表对象
        List<HashMap> hashMapList = formItemService.getFormItemByFormIdAndType(processNodeFormPrivilege.getFormId(), SystemConstant.FORM_TYPE_S);
        HashMap formItemHashMap = null;
        if(hashMapList!=null && hashMapList.size()>0){
            formItemHashMap = hashMapList.get(0);
            Iterator it = formItemHashMap.entrySet().iterator();
            while(it.hasNext()){
                Map.Entry entry = (Map.Entry)it.next();
                List<FormItem> sFormItemList = (List<FormItem>) entry.getValue();
                for(FormItem formItem : sFormItemList){
                    processNodeFormPrivilege.setFormId(formItem.getFormId());
                    processNodeFormPrivilege.setItemId(formItem.getId());
                    List<ProcessNodeFormPrivilege> processNodeFormPrivilegeList = processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
                    for(ProcessNodeFormPrivilege pnfp : processNodeFormPrivilegeList){
                        formItem.setProcessNodeFormPrivilegeId(pnfp.getId());
                        formItem.setIsHidden(pnfp.getHidden());
                        formItem.setIsNull(pnfp.getNull());
                        formItem.setIsEdit(pnfp.getEdit());
                        formItem.setIsVariable(pnfp.getVariable());
                    }
                }
            }
        }

        ModelAndView mav = new ModelAndView("process/process_node_form_privilege");
        mav.addObject("processNodeFormPrivilege", processNodeFormPrivilege);
        mav.addObject("formItemList", formItemList);
        mav.addObject("formItemHashMap", formItemHashMap);
        mav.addObject("formTemplateList", formTemplateList);
        mav.addObject("printTemplateList", printTemplateList);
        mav.addObject("processNode", processNode);
        mav.addObject("formId", formId);
        return mav;
    }

    /**
     * 保存节点表单权限配置
     * @param processNodeFormPrivilege 节点表单权限配置实体对象
     * @param processNodeFormPrivileges 节点表单权限配置JSON
     * @param processNode 流程节点实体对象
     */
    @RequestMapping(value="/processNode_FormPrivilegeSave", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void saveProcessNodeFormPrivileges(ProcessNodeFormPrivilege processNodeFormPrivilege, String processNodeFormPrivileges, ProcessNode processNode){
       try{
        processNodeFormPrivilegeService.saveProcessNodeFormPrivileges(processNodeFormPrivilege, processNodeFormPrivileges, processNode);
       }catch (Exception e){
           logger.error(e);
           throw new BusinessException("保存节点表单权限配置失败!",e);
       }
    }
    /**
     * 预览表单
     * @param processDefinitionId
     * @return
     */
    @RequestMapping(value = "/processNode_FormPrivilegeReview" , method = RequestMethod.GET)
    public ModelAndView formTemplateReview(HttpServletRequest request,String processDefinitionId,String nodeId,String formTemplateId,String formId) {
    	ModelAndView mv = new ModelAndView();
    	mv.setViewName("form/formTemplate_review");
    	List<ProcessNodeFormPrivilege> processNodeFormPrivileges = getProcessNodeFormPrivileges(processDefinitionId, nodeId, formId.trim());
    	mv.addObject("templateHtml", processNodeFormPrivilegeService.getTemplateContextRuntime(request, processNodeFormPrivileges,formTemplateId, formId));
    	return mv;
    }
    /**
     * 获取节点表单权限配置
     * @param processDefinitionId
     * @param nodeId
     * @param formId
     * @return
     */
    private List<ProcessNodeFormPrivilege> getProcessNodeFormPrivileges(String processDefinitionId,String nodeId,String formId){
    	ProcessNodeFormPrivilege processNodeFormPrivilege = new ProcessNodeFormPrivilege();
    	processNodeFormPrivilege.setProcessDefinitionId(processDefinitionId);
    	processNodeFormPrivilege.setNodeId(nodeId);
    	processNodeFormPrivilege.setFormId(formId);
    	return processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
    }
    
    
    private boolean checkIfHasFirstNode(String processId, String nodeId){
    	Map m = new HashMap<String, String>();
//		m.put("nodeId", nodeId);
		m.put("processDefinitionId", processId);
		m.put("isFirstNode", "1");
		List<ProcessNode> processNodes = processNodeService.find("findByMap",m);
		if(processNodes!=null && processNodes.size() > 0){
			for (ProcessNode p : processNodes) {
				if(processId.equals(p.getProcessDefinitionId()) && ! nodeId.equals(p.getNodeId()))
					return true ;
			}
		}
		
		return false ;
    }
    
    
    
  //根据流程定义id获取isEmail,isSms,isLync
  	@RequestMapping(value = "/getAlertModeByProcessId")
  	@ResponseBody
  	 public String getAlertModeByProcessId(
  			 @RequestParam(value = "locale", required = false) Locale locale,
	    	 @RequestParam(value = "processId", required = false) String processId){
  		ProcessDefinition def = processDefService.get(processId);
  		if(def != null ){
  			int[] modes = new int[]{getAlertMode(def.getAlertMode(),0),getAlertMode(def.getAlertMode(),1),getAlertMode(def.getAlertMode(),2) } ;
  			return JsonUtils.toJsonByGoogle(modes) ;
  		}
  		
  		return JsonUtils.toJsonByGoogle(new int[]{})  ;
  			
  	}
    
   
    
	//从数据库取值后处理
	private int getAlertMode(String mode,int offset){
		if(mode!=null && !"".equals(mode)){
			int temp = new Integer(mode).intValue();
			return (temp>>offset) & 1;
			
		}
		return 0;
	}
}

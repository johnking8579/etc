package com.jd.oa.process.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessSupervise;
import com.jd.oa.process.model.ProcessType;

public interface ProcessSuperviseService extends BaseService<ProcessSupervise, String>{

}

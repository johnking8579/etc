package com.jd.oa.process.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.model.ProcessNode;

@Component("processNodeDao")
public class ProcessNodeDaoImpl extends MyBatisDaoImpl<ProcessNode, String>implements ProcessNodeDao  {

    /**
     * 查询存在上级节点的配置的数量
     * @param processNode 流程节点对象
     * @return 数量
     */
    public Integer findManagerNodeCount(ProcessNode processNode){
        return this.getSqlSession().selectOne("com.jd.oa.process.model.ProcessNode.findManagerNodeCount", processNode);
    }
    
    /**
     * 查询被引用的节点
     * @param processNode 流程节点对象
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findByManagerNode(String managerNodeId){
//    	Map<String,String> mp = new HashMap<String,String>();
//    	mp.put("managerNodeId", managerNodeId);
    	return this.find("findByManagerNode", managerNodeId);
    }
    
    /**
     * 查询所有被引用的节点
     * @param processDefinitionId 流程定义ID
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findAllManagerNode(String processDefinitionId){
    	return this.find("findAllManagerNode", processDefinitionId);
    }
    
}

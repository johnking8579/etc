package com.jd.oa.process.service.impl;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessNodeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service("processNodeService")
@Transactional
public class ProcessNodeServiceImpl extends BaseServiceImpl<ProcessNode, String> implements ProcessNodeService{
	@Autowired
	private ProcessNodeDao processNodeDao;
	
	public ProcessNodeDao getDao(){
		return processNodeDao;
	}

	@Override
	public ProcessNode getFirstNode(String processDefinitionId) {
		ProcessNode processNode = new ProcessNode();
		processNode.setProcessDefinitionId(processDefinitionId);
		processNode.setIsFirstNode("1");
		
		List<ProcessNode> list = find(processNode);
		if(list!=null && list.size()>=1)
			return list.get(0);
		return null;
	}

    /**
     * 查询存在上级节点的配置的数量
     * @param processNode 流程节点对象
     * @return 数量
     */
    public Integer findManagerNodeCount(ProcessNode processNode){
        return processNodeDao.findManagerNodeCount(processNode);
    }
    
    /**
     * 查询被引用的节点
     * @param processNode 流程节点对象
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findByManagerNode(String managerNodeId){
    	return processNodeDao.findByManagerNode(managerNodeId);
    }
    
    /**
     * 查询所有被引用的节点
     * @param processDefinitionId 流程定义ID
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findAllManagerNode(String processDefinitionId){
    	return this.find("findAllManagerNode", processDefinitionId);
    }
    
	@Override
	public ProcessNode findAddsignRule(String processDefinitionId, String nodeId) {
		Map<String,String> map = new HashMap<String, String>();
		map.put("nodeId", nodeId);
		map.put("processDefinitionId", processDefinitionId);
		List<ProcessNode> list = find("findByMap",map);
		if(list != null && list.size() > 0 ){
			return list.get(0);
		}
		
		return null;
	}
}

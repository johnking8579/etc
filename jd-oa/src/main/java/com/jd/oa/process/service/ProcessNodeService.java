package com.jd.oa.process.service;

import java.util.List;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessNode;

public interface ProcessNodeService extends BaseService<ProcessNode, String>{
	ProcessNode getFirstNode(String processDefinitionId);

    /**
     * 查询存在上级节点的配置的数量
     * @param processNode 流程节点对象
     * @return 数量
     */
    public Integer findManagerNodeCount(ProcessNode processNode);
    
    /**
     * 查询被引用的节点
     * @param processNode 流程节点对象
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findByManagerNode(String managerNodeId);
    
    /**
     * 查询所有被引用的节点
     * @param processDefinitionId 流程定义ID
     * @return List<ProcessNode>
     */
    public List<ProcessNode> findAllManagerNode(String processDefinitionId);
    
    public ProcessNode findAddsignRule(String processDefinitionId, String nodeId);
}

package com.jd.oa.process.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessProxy;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午4:33
 * 代办人
 */
public interface ProcessProxyDao extends BaseDao<ProcessProxy ,String> {
    /**
     * 根据办理人ID查询代办信息
     * @param processProxy
     * @return
     */
    public List<ProcessProxy> findProcessProxysByUserId(ProcessProxy processProxy) ;

    /**
     * 获取相同条件的代办人数量
     * @param processProxy
     * @return
     */
    public Integer getCount(ProcessProxy  processProxy);
    
    /**
     * 根据代理人ID查询对应的办理人
     * @param processProxy
     * @return
     */
    public List<ProcessProxy> findListByproxyId(String proxyId,String byProxyUserId);

   
    /**
     * 根据被代理人的ID查询,有时间条件限制
     * @param assigneeId
     * @return
     */
    List<ProcessProxy> findByAssigneeId(String assigneeId);

}

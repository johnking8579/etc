package com.jd.oa.process.dao.impl;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.dao.ProcessAddsignerDao;
import com.jd.oa.process.dao.ProcessProxyDao;
import com.jd.oa.process.model.ProcessAddsigner;
import com.jd.oa.process.model.ProcessProxy;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: xulin
 * Date: 13-9-17
 * Time: 下午4:33
 * 代办人
 */
@Repository("processAddsignerDao")
public class ProcessAddsignerDaoImpl  extends MyBatisDaoImpl<ProcessAddsigner, String> implements ProcessAddsignerDao {
	
	/**
     * 根据加签人ID获取还未办理的流程实例
     * @param userId
     * @param status
     * @return
     */
    public List<ProcessAddsigner> findProcessInstanceByAddUserId(String userId,String status){
    	try {
    		Map<String,String> parameters = new HashMap<String,String>();
    		parameters.put("userId", userId);
    		parameters.put("status", status);
        	return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessAddsigner.findProcessInstanceByAddUserId",parameters);
    	} catch (DataAccessException e) {
    		throw e;
    	}
    }

	@Override
	public ProcessAddsigner getAddSignTask(String processInstanceId, String taskId, String nodeId) {
		// TODO Auto-generated method stub
		Map<String,String> parameters = new HashMap<String,String>();
		parameters.put("processInstanceId", processInstanceId);
		parameters.put("taskId", taskId);
		parameters.put("nodeId", nodeId);
		List<ProcessAddsigner> processAddsignerList = this.find("getAddSignTask", parameters);
		if (processAddsignerList!=null && processAddsignerList.size()>0)
			return processAddsignerList.get(0);
		else
			return null;
	}
	
	@Override
	public List<ProcessAddsigner> findProcessInstanceByAddUserId(String userId,
			String status, String processDefinitionId) {
    	try {
    		Map<String,String> parameters = new HashMap<String,String>();
    		parameters.put("userId", userId);
    		parameters.put("status", status);
    		parameters.put("processDefinitionId", processDefinitionId);
        	return this.getSqlSession().selectList("com.jd.oa.process.model.ProcessAddsigner.findProcessInstanceByAddUserId",parameters);
    	} catch (DataAccessException e) {
    		throw e;
    	}
	}
	
}

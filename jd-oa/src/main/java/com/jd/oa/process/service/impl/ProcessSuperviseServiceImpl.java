package com.jd.oa.process.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessNodeDao;
import com.jd.oa.process.dao.ProcessSuperviseDao;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessSupervise;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.process.service.ProcessSuperviseService;


@Service("processSuperviseService")
@Transactional
public class ProcessSuperviseServiceImpl extends BaseServiceImpl<ProcessSupervise, String> implements ProcessSuperviseService{
	@Autowired
	private ProcessSuperviseDao processSuperviseDao;
	
	public ProcessSuperviseDao getDao(){
		return processSuperviseDao;
	}
}

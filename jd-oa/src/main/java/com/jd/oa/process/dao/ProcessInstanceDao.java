package com.jd.oa.process.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessInstance;

import java.util.List;

public interface ProcessInstanceDao extends BaseDao<ProcessInstance, String> {
	
	/**
	 * @desc 通过流程key获取流程实例
	 * @author WXJ
	 * @date 2013-10-15 下午01:23:03
	 * @param processDefKey
	 * @return
	 */
	public List<ProcessInstance> findProcessInstanceByKey(String processDefKey);
	
	/**
	 * 
	 * @desc 通过发起人获得所有的流程实例
	 * @author WXJ
	 * @date 2013-11-25 下午04:21:07
	 *
	 * @param processDefKey
	 * @return
	 */
	public List<String> findAllInstanceByStartId(String starterId, List<String> statusList);
	
    /**
     * 根据流程实例ID查询流程实例信息
     * @param processInstance 流程实例实体类
     * @return 流程实例信息
     */
    public ProcessInstance selectByProcessInstanceId(ProcessInstance processInstance);
    
    /**
     * 根据流程实例ID查询流程实例信息(一次性获取多个)
     * @param processInstance 流程实例实体类
     * @return 流程实例信息
     */
    public List<ProcessInstance> selectByProcessInstanceIds(List<String> processInstanceIds);

    /**
     * 取消流程实例
     * @param processInstance 流程实例实体类
     */
    public void cancelByProcessInstanceId(ProcessInstance processInstance);

    /**
     * 根据processInstanceId获取唯一值
     * @param processInstanceId
     * @return
     */
	public ProcessInstance findByInstanceId(String processInstanceId);
}

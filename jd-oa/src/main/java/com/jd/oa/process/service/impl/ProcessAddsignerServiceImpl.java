package com.jd.oa.process.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;







import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.process.dao.ProcessAddsignerDao;
import com.jd.oa.process.dao.ProcessTypeDao;
import com.jd.oa.process.model.ProcessAddsigner;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessAddsignerService;
import com.jd.oa.process.service.ProcessTypeService;

@Service("processAddsignerService")
@Transactional
public class ProcessAddsignerServiceImpl extends BaseServiceImpl<ProcessAddsigner, String> implements ProcessAddsignerService{
	@Autowired
	private ProcessAddsignerDao processAddsignerDao;
	
	public ProcessAddsignerDao getDao(){
		return processAddsignerDao;
	}

	@Override
	public ProcessAddsigner getAddSignTask(String processInstanceId, String taskId,
			String nodeId) {
		// TODO Auto-generated method stub
		return processAddsignerDao.getAddSignTask(processInstanceId, taskId, nodeId);
	}

	
	
}

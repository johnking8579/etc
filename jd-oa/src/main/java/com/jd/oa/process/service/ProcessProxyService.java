package com.jd.oa.process.service;
import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessProxy;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午4:20
 * 代办人Service.
 */
public interface ProcessProxyService extends BaseService<ProcessProxy, String> {
    /**
     * 查询代理人
     * @param processProxy
     * @return
     */
   public List<ProcessProxy> findList(ProcessProxy processProxy);

    /**
     * 查询相同条件的代办人数量
     * @param processProxy
     * @return
     */
   public int getCount(ProcessProxy processProxy)  ;
    /**
     * 新增保存代理人
     * @param processProxy
     * @return
     */
    public void insertProcessProxyInfo(ProcessProxy processProxy);

    /**
     *删除代理人信息
     * @param processProxy
     */
    public void deleteProcessProxyById (ProcessProxy processProxy);
    
}

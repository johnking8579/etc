package com.jd.oa.upgrade.biz;

import java.util.List;

import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.upgrade.core.AutoUpgradeBizServerPackAbst;

public class AutoUpgradeBizServerPackImp_201407171629_UpgradeUpdateProcessNodeCandUserType extends AutoUpgradeBizServerPackAbst{

	@Override
	public String getServerPackName() {
		// TODO Auto-generated method stub
		return _PACK_INFO + "初始化CandUserType" + _PACK_INFO;
	}

	@Override
	public boolean forPlatform() {
		ProcessDefService processDefService = SpringContextUtils.getBean(ProcessDefService.class);
		ProcessNodeService processNodeService = SpringContextUtils.getBean(ProcessNodeService.class);
		
		ProcessDefinition processDefinition = new ProcessDefinition();
		processDefinition.setIsOuter("0");
		List<ProcessDefinition> processDefinitions = processDefService.find(processDefinition);
		System.out.println("processDefSize="+processDefinitions.size());
		
		if(null != processDefinitions && !processDefinitions.isEmpty()){
			for(ProcessDefinition processDef:processDefinitions){
				String processDefinitionId = processDef.getId();
				ProcessNode processNode = new ProcessNode();
				processNode.setProcessDefinitionId(processDefinitionId);
				List<ProcessNode> processNodes = processNodeService.find(processNode);
				if(null != processNodes && !processNodes.isEmpty()){
					for(ProcessNode node:processNodes){
						String candidateUserType = "";
						if(null != node.getIsFirstNode())
							continue;
						if((node.getManagerNodeId() == null || node.getManagerNodeId().equals("")) 
								&& (node.getCandidateUser() == null || node.getCandidateUser().equals(""))){
							candidateUserType = "4";
						}else if(null != node.getCandidateUser() && !node.getCandidateUser().equals("")){
							candidateUserType = "3";
						}else if(null != node.getManagerNodeId() && !node.getManagerNodeId().equals("")){
							String managerNodeId = node.getManagerNodeId();
							if(managerNodeId.contains("_assignee")){
								candidateUserType = "1";
							}else if(managerNodeId.contains("_manager")){
								
							}else{
								candidateUserType = "2";
							}
						}
						System.out.println("node_id="+node.getNodeId()+":[ManagerNodeId="+node.getManagerNodeId()+
								", CandidateUser="+node.getCandidateUser()+", CandidateUserType="+candidateUserType+"]");
						node.setCandidateUserType(candidateUserType);
						processNodeService.update(node);
					}
				}
			}
			
		}
		return true;
	}

}

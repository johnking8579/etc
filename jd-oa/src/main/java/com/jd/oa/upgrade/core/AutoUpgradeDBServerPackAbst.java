package com.jd.oa.upgrade.core;

import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AutoUpgradeDBServerPackAbst implements UpgradeServerPackInterface {

	public final static String _PACK_INFO = "\n*********DataBase PACK Info*************\n";

	/**
	 * 升级包机制,只针对数据库的升级，不对biz操作的升级补丁继承该抽象类
	 */
	public boolean execute(JdbcTemplate jdbcTemplate) {
		String dbSupply = "mysql";
		try {
			dbSupply = jdbcTemplate.getDataSource().getConnection().getMetaData().getDatabaseProductName();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (dbSupply.toLowerCase().equals("oracle")) {
			return forOracle();
		} else if (dbSupply.toLowerCase().equals("sqlserver")) {
			return forSQLServer();
		} else if (dbSupply.toLowerCase().equals("mysql")) {
			return forMYSQL();
		} else if (dbSupply.toLowerCase().equals("db2")) {
			return forDB2();
		} else if (dbSupply.toLowerCase().equals("oscar")) {
			return forOscar();			
		}
		return true;
	}

	/**
	 * for oracle script
	 */
	public abstract boolean forOracle();

	/**
	 * for sqlserver script
	 */
	public abstract boolean forSQLServer();

	/**
	 * for mysql script
	 */
	public abstract boolean forMYSQL();
	
	/**
	 * for db2 script 
	 */
	public boolean forDB2(){
		forSQLServer();
		System.out.println("一个未实现DB2的Pack，若看到该提示，请务必与开发人员联系!\n\n\n\n");
		return true;
	}
	
	/**
	 * for oscar script
	 */
	public boolean forOscar(){
		forSQLServer();
		System.out.println("一个未实现Oscar的Pack，若看到该提示，请务必与开发人员联系!\n\n\n\n");
		return true;
	}

}

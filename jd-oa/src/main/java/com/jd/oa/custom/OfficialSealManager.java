package com.jd.oa.custom;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.jd.oa.common.utils.SpringContextUtils;

/**
 * 公章申请
 *
 * @author yujiahe
 */
public class OfficialSealManager {
static 	JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");

	/**
	 * 查找四级orgCode
	 *
	 * @param erpId
	 * @return
	 */
	public String get4thOrg(String erpId) {
		return getOrgCode(erpId, 4);
	}

	/**
	 * 查找三级orgCode
	 *
	 * @param erpId
	 * @return
	 */
	public String get3thOrg(String erpId) {
		return getOrgCode(erpId, 3);
	}

	/**
	 * 是否为大客户销售部
	 *
	 * @param erpId
	 * @return
	 */
	public String isVipCustomDept(String erpId) {
		if (get4thOrg(erpId).equals("00007988") //总部
				|| get3thOrg(erpId).equals("00008003")//华东
				|| get3thOrg(erpId).equals("00008004")//华南
				|| get3thOrg(erpId).equals("00008005")//西南
				|| get3thOrg(erpId).equals("00008271")//华中
				|| get3thOrg(erpId).equals("00008007")) {//东北
			return "1";
		} else {
			return "0";
		}
	}

	public static String getOrgCode(String erpId, Integer num) {
		String orgCode = "";
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(" select ORGANIZATION_FULLPATH ");
		sqlQuery.append(" from T_JDOA_SYS_USER U   ");
		sqlQuery.append(" left join T_JDOA_SYS_ORGANIZATION O on U.ORGANIZATION_ID = O.ID");
		sqlQuery.append(" where U.USER_NAME = '" + erpId + "'");
		List rows = jdbcTemplate.queryForList(sqlQuery.toString());
		if (rows != null && rows.size() > 0) {
			Map map = (Map) rows.get(0);
			String organizationFullpath = map.get("ORGANIZATION_FULLPATH").toString();
			String[] orgFullPathArray = organizationFullpath.split("/");
			if (orgFullPathArray.length > num) {
				orgCode = orgFullPathArray[num];
			}
		}
		return orgCode;
	}
}

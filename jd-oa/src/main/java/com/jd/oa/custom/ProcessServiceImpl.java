package com.jd.oa.custom;

import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.jws.WebService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;


@WebService(endpointInterface = "com.jd.oa.custom.ProcessService", serviceName = "ProcessService", targetNamespace = "http://custom.oa.jd.com/")
public class ProcessServiceImpl implements ProcessService {

	private static final Logger logger = Logger
			.getLogger(ProcessServiceImpl.class);
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private ProcessTaskHistoryService processTaskHistoryService;
	
	JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");

	@Override
	public String findProcessTaskHistoryByBusinessId(String businessId) {
		// TODO Auto-generated method stub
		String result = "";
		ProcessInstance PI = new ProcessInstance();
		PI.setBusinessInstanceId(businessId);
		List<ProcessInstance> processiInstanceList = processInstanceService
				.find(PI);
		if (processiInstanceList.size() > 0) {
			String processInstanceId = processiInstanceList.get(0)
					.getProcessInstanceId();
			if (null != processInstanceId) {
				ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
				processTaskHistory.setProcessInstanceId(processInstanceId);
				List<ProcessTaskHistory> processTaskHistoryList = processTaskHistoryService
						.selectByCondition(processTaskHistory);
				int i = 0;
				for (ProcessTaskHistory PTH : processTaskHistoryList) {
					if(i==0){
						result = "";//第一条不要
					}else{
						result += PTH.getProcessTaskHistoryForCustomService()
						+ "/r";
					}
					i++;
				}
			}
		}
		return result;

	}

	@Override
	@Transactional
	public String updatePurchaseDetail(String purchaseDetail) {
		// TODO Auto-generated method stub
		String result = "";
		logger.info("updatePurchaseDetail接收参数：" + purchaseDetail);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document purchaseDetailDoc = builder
					.parse(new ByteArrayInputStream(purchaseDetail.getBytes("utf-8")));

			NodeList purchaseDetailList = purchaseDetailDoc
					.getElementsByTagName("purchaseDetail");

			int len = purchaseDetailList.getLength();
			for (int i = 0; i < len; i++) {
				// 遍历每一个元素
				Element elt = (Element) purchaseDetailList.item(i);
				// 取得元素节点
				Node eltId = elt.getElementsByTagName("id").item(0);// ID
				Node eltComments = elt.getElementsByTagName("comments").item(0);// 备注
				Node eltProductStatus = elt.getElementsByTagName(// 产品状态
						"productStatus").item(0);
				Node eltInputNumCount = elt.getElementsByTagName(// 采购数目
						"inputNumCount").item(0);
				Node eltPurchaseLogs = elt.getElementsByTagName("purchaseLogs").item(0);
				Node eltPurchasePeople = elt.getElementsByTagName("purchasePeople").item(0);
				Node eltPurchaseArea = elt.getElementsByTagName("purchaseArea").item(0);
				Node eltStatusReason = elt.getElementsByTagName("statusReason").item(0);
				Node eltPurchasePurposeCode = null;
				boolean hasPurchasePurposeNode = false;
				if(null!=elt.getElementsByTagName("purchasePurposeCode") && null!=elt.getElementsByTagName("purchasePurposeCode").item(0)){
					eltPurchasePurposeCode = elt.getElementsByTagName("purchasePurposeCode").item(0);
					hasPurchasePurposeNode = true;
				}
				
				// 取得节点中的内容InputNumCount
				String  comments="";
				String id ="";
				String productStatus  = "";
				String inputNumCount = "" ;
				String purchaseLogs = "";
				String purchasePeople = "";
				String purchaseArea = "";
				String statusReason ="";
				String purchasePurposeCode = "";
//				"<purchaseLogs></purchaseLogs>"+//采购员操作日志
//			    "<purchasePeople></purchasePeople>"+//采购员
//			    "<purchaseArea></purchaseArea>"+//采购区域				
				if(eltId.getFirstChild()!=null){
					id = eltId.getFirstChild().getNodeValue();// ID
					if(eltComments.getFirstChild()!=null){
						 comments = eltComments.getFirstChild().getNodeValue();// 备注
					}
					if( eltProductStatus.getFirstChild()!=null){
						 productStatus = eltProductStatus.getFirstChild()// 产品状态
						.getNodeValue();
					}
					if(eltPurchaseLogs.getFirstChild()!=null){
						purchaseLogs = eltPurchaseLogs.getFirstChild()// 采购数目
						.getNodeValue();
					}
					if(eltInputNumCount.getFirstChild()!=null){
						 inputNumCount = eltInputNumCount.getFirstChild()// 采购数目
						.getNodeValue();
					}
					if(eltPurchasePeople.getFirstChild()!=null){
						purchasePeople = eltPurchasePeople.getFirstChild()// 采购数目
						.getNodeValue();
					}
					if(eltPurchaseArea.getFirstChild()!=null){
						purchaseArea = eltPurchaseArea.getFirstChild()// 采购数目
						.getNodeValue();
					}
					if(eltStatusReason.getFirstChild()!=null){
						statusReason = eltStatusReason.getFirstChild()// 状态理由
						.getNodeValue();
					}
					if(eltPurchasePurposeCode!=null && eltPurchasePurposeCode.getFirstChild()!=null){
						purchasePurposeCode = eltPurchasePurposeCode.getFirstChild()// 采购用途
						.getNodeValue();
					}
					String purchasePurposeSql = "";// 采购用途
					if(hasPurchasePurposeNode){
						purchasePurposeSql = "' ,cChr_PurchasePurposeCode = '" + purchasePurposeCode;
					}

					String sql = " update T_JDOA_DEPT_PURCHASE_DETAIL set"
							+ " cInt_InputNumCount= '" + inputNumCount// 采购数目
							+ "' ,cChr_Comments = '" + comments // 备注
							+ "' ,cInt_ProductStatus = '" + productStatus // 产品状态
							+ purchasePurposeSql
							+ "' ,cChr_PurchasePeople = '" + purchasePeople// 采购员
							+ "' , cChr_PurchaseArea ='" + purchaseArea// 采购区域
							+ "' ,cChr_PurchaseLogs = concat(IFNULL(cChr_PurchaseLogs,''),'" + purchaseLogs  // 采购员操作日志
							+"') , cChr_StatusReason = '" + statusReason +
							"' where ID = '" + id + "'";
					logger.info("修改T_JDOA_DEPT_PURCHASE_DETAIL SQL语句：" + sql);
					int updateNum = jdbcTemplate.update(sql);
					result = "" + updateNum + "";
				}else{
					result = "ID不能为空";
				}
				
				

			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = e.toString();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = e.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = e.toString();
		}
		return result;
	}

}

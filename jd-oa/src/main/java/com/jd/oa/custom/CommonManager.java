package com.jd.oa.custom;

import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.mail.MailSendService;
import com.jd.oa.common.utils.DesUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-5-5 下午02:47:55
 *
 */
public class CommonManager {

    private static final Logger logger = Logger.getLogger(CommonManager.class);

    public String  sendMailToSomeone(String submitUser, String processInstanceId,String processInstanceName,String businessInstanceId,String flag,String name){
        ProcessTaskService processTaskService = SpringContextUtils.getBean("processTaskService");
        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
        if(! "".equals( name)){
            SysUser u = sysUserService.getByUserName(name);
            if(u != null && !"".equals(u.getEmail())){
                List<String > mails = new ArrayList<String>();
                mails.add(u.getEmail());
                processTaskService.sendMailToSomeone( submitUser,  processInstanceId, processInstanceName, businessInstanceId,flag,   mails,null);
            }

        }

        return "success";
    }
	
	public String sendMailPrepare(String startId,String applyName,String status) {

		String mailSubject = "【京东企业门户-流程中心】";
        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");

        SysUser sysUser = sysUserService.get(startId);
        if(sysUser == null) return "success";

        String toMail = sysUser.getEmail();
        if(toMail == null || "".equals(toMail)) return "success";

        StringBuilder sb = new StringBuilder();
        sb.append(sysUser.getRealName());
        sb.append("，您好！\n您的申请【");
        sb.append(applyName);
        sb.append("】被");
        sb.append("审批，当前状态为：");
        if("1".equals(status))
            sb.append("审批中");
        else if("2".equals(status))
            sb.append("拒绝");
        else if("3".equals(status))
            sb.append("驳回");
        else if("4".equals(status))
            sb.append("取消");
        else if("5".equals(status))
            sb.append("归档");
        else if("0".equals(status))
            sb.append("草稿");
        sb.append("。详情请登录Erp流程中心查看。");
		String mailContent = sb.toString() ;

		sendMail(toMail,mailSubject,mailContent);

        return "success";
	}
	
    public String sendMail(String toMail,String mailSubject,String mailContent) {
        MailSendService mailSendService = SpringContextUtils.getBean("mailSendService");
        mailSendService.sendSimpleMailByAsyn(new String[]{toMail}, mailSubject, mailContent);
        return "success";
    }

    //获取人员职级
    public String getUserLevelCode(String userName) {
        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
        SysUser su = sysUserService.getByUserName(userName);
        return su.getLevelCode();
    }
    
    //是否为总部人员：1 是；0 不是
    public String isHeadquarter(String userName) {
        SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
        SysUser su = sysUserService.getByUserName(userName);
        SysOrganizationService sysOrganizationService = SpringContextUtils.getBean("sysOrganizationService");
        SysOrganization so = sysOrganizationService.get(su.getOrganizationId());
        if (so.getOrganizationFullPath().contains("00000840")) 
        	return "1";
        else
        	return "0";
    }

	/**
	 * 获取指定erp的一级部门，二级 部门等
	 * @param userErp
	 * @return
	 */
	public Map<String,String> getOrganizationMap(String userErp){
		SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
		return sysUserService.getOrganizationMap(userErp);
	}

	public String get3DesEncryptData(String key,String content){
		String result="";
		try {
			content= java.net.URLEncoder.encode(content, "utf-8");
			result = DesUtils.toHexString(DesUtils.encrypt(content, key));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

    /**
     * 专供内部类接口使用  为修改业务数据
     * @param businessId 业务表ID
     * @param businessTableName  业务表表明
     * @param columns  修改列名（多个用,隔开）
     * @param values 修改列的值（多个用,隔开）
     * @param isMainTable （1：修改主表）
     */
    public void modifyBusinessData(String businessId, String businessTableName, String columns, String values, String isMainTable,String conditions,String conditionsValues) {
        if (StringUtils.isNotBlank(businessId)
                && StringUtils.isNotBlank(businessTableName)
                && StringUtils.isNotBlank(columns)
                && StringUtils.isNotBlank(values)) {
            String[] columnList = columns.split(",");
            String[] valueList = values.split(",");
            String[] conditionsList = conditions.split(",");
            String[] conditionsValuesList = conditionsValues.split(",");
            if ((columnList.length == valueList.length)&&(conditionsList.length ==conditionsValuesList.length) ) {
                JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
                StringBuffer sqlQuery = new StringBuffer();
                sqlQuery.append(" update  " + businessTableName + " set ");// 采购类型 使用人部门组织编码
                for (int i = 0; i < columnList.length; i++) {
                    sqlQuery.append("" + columnList[i] + " = '" + valueList[i] + "'");
                    if (i > 0) {
                        sqlQuery.append(" ,");
                    }
                }
                if (isMainTable.equals("1")) {//如果是修改主业务表
                    sqlQuery.append(" where id =  '" + businessId+"'");
                } else {
                    sqlQuery.append(" where parent_Id = '" + businessId+"'");
                }
                if(conditions!=null){
                    for(int j =0;j<conditionsList.length;j++){
                        sqlQuery.append(" and "+conditionsList[j]+" = '"+conditionsValuesList[j]+"'");
                    }
                }
                logger.debug("修改业务表语句：" + sqlQuery);
                int row = jdbcTemplate.update(sqlQuery.toString());
            }else{
                throw new BusinessException("modifyBusinessData方法的参数 String columns, String values 个数不同");
            }
        } else {
            throw new BusinessException("modifyBusinessData方法的参数String businessId, String businessTableName, String columns, String values 都不能为空");
        }
    }
    
    /**
     * @author zhengbing 
     * @desc 是否为管理岗：1 是；0 不是
     * @date 2014年8月4日 下午4:38:59
     * @return String
     */
    public String isManagerPosition(String userName) {
    	try {
    		SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
            SysUser su = sysUserService.getByUserName(userName);
            String levelCode = su.getLevelCode();
            if(levelCode.trim().toUpperCase().startsWith("M")){
            	return "1";
            }
		} catch (Exception e) {
			logger.error("判读是否为管理岗失败！：", e);
		}
        return "0";
    }
}


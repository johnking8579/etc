package com.jd.oa.custom;

import java.math.BigDecimal;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.google.gson.GsonBuilder;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.custom.dao.DeptBudgetDao;
import com.jd.oa.custom.model.DeptBudget;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

/**
 * @desc 行政差旅申请流程
 * @author qiuyang
 *
 */
public class AdminBusinessTripManager {
	private static final Logger logger = Logger.getLogger(AdminBusinessTripManager.class);
	
	private DeptBudgetDao deptBudgetDao = SpringContextUtils.getBean(DeptBudgetDao.class);
//    @Autowired
//    private ProcessInstanceService processInstanceService;
//    @Autowired
//    private ProcessTaskHistoryService processTaskHistoryService;
    JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
	//头信息定义
	private Map<String,Object> soapHeaderMap=new HashMap<String, Object>();
	public AdminBusinessTripManager(){
		//处理头信息
		soapHeaderMap.put("userName","oa_ws");
		soapHeaderMap.put("password","oa@123#");
		soapHeaderMap.put("authentication","");
		soapHeaderMap.put("locale","");
		soapHeaderMap.put("timeZone","");
	}
	
	public String createBusinessTripInfo(String BusinessId,String MainTable,String DetailTable1,String DetailTable2,
			String DetailTable3,String wsdl,String subwsdl1,String subwsdl2,String subwsdl3){
		CallerInfo info = Profiler.registerInfo("wfp_createbusinesstripinfo",false,true); //方法性能监控
		try {
			//加载需要使用的service
			ProcessInstanceService processInstanceService = SpringContextUtils.getBean("processInstanceService");
			FormBusinessTableService formBusinessTableService= SpringContextUtils.getBean("formBusinessTableService");
			
			//test
			//BusinessId = "126175718341465";
			Map<String,Object> mainRecored=formBusinessTableService.getBusinessData(MainTable, BusinessId);
			if(mainRecored == null) {
				throw new BusinessException("找不到行政差旅申请记录");
			}
			//取回流程实例信息
			ProcessInstance processInstance = new ProcessInstance();
			processInstance.setBusinessInstanceId(mainRecored.get("ID").toString());
			List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
			if(processInstances == null || processInstances.size() == 0){
				throw new BusinessException("找不到流程实例信息!");
			}
			String processInstanceId = processInstances.get(0).getProcessInstanceId();
			//test
			//String processInstanceId = "test01";
			//取回子表记录
			List<Map<String,Object>> sub1RecoredList=formBusinessTableService.getBusinessDatas(DetailTable1, mainRecored.get("ID").toString());
			List<Map<String,Object>> sub2RecoredList=formBusinessTableService.getBusinessDatas(DetailTable2, mainRecored.get("ID").toString());
			List<Map<String,Object>> sub3RecoredList=formBusinessTableService.getBusinessDatas(DetailTable3, mainRecored.get("ID").toString());
			
			createTravelRequest(mainRecored,wsdl,processInstanceId);
			createTravelPartners(sub1RecoredList,subwsdl1,processInstanceId);
			createTvlScheduleInfo(sub2RecoredList,subwsdl2,processInstanceId);
			createTvlAssigneeWorkLog(sub3RecoredList,subwsdl3,processInstanceId);
			return "sucess";
		} catch (Exception e) {
			logger.error("jme_createBusinessTripInfo error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return "error";
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}
	
	/**
	 * 差旅申请单信息创建接口
	 * 
	 * @param mainRecored
	 * @param wsdl
	 * @param processInstanceId
	 */
	private void createTravelRequest(Map<String,Object> mainRecored,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		//给数组赋值
		Object[] params = new Object[22];
//		params[0] = processInstanceId;
        params[0] =mainRecored.get("ID").toString();
		params[1] = mainRecored.get("cChr_InvoiceHead");
		if(mainRecored.get("cDpl_Inland") != null){
			params[2] = Integer.parseInt((String)mainRecored.get("cDpl_Inland"));
		}
		if(mainRecored.get("cDpl_TravelType") != null){
			params[3] = Integer.parseInt((String)mainRecored.get("cDpl_TravelType"));
		}
		params[4] = mainRecored.get("cChr_T_TvlReason");
		if(mainRecored.get("cDpl_B_Whole") != null){
			params[5] = Integer.parseInt((String)mainRecored.get("cDpl_B_Whole"));
		}
		if(mainRecored.get("cDel_B_TotalMoney") != null){
			params[6] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_TotalMoney").toString()));
		}
		if(mainRecored.get("cDel_B_TollMoney") != null){
			params[7] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_TollMoney").toString()));
		}
//		if(mainRecored.get("cDpl_B_TollDetail_Flights") != null){
//			params[8] = Integer.parseInt((String)mainRecored.get("cDpl_B_TollDetail_Flights"));
//		}
//		if(mainRecored.get("cDpl_B_TollDetail_Train") != null){
//			params[9] = Integer.parseInt((String)mainRecored.get("cDpl_B_TollDetail_Train"));
//		}
//		if(mainRecored.get("cDpl_B_TollDetail_Bus") != null){
//			params[10] = Integer.parseInt((String)mainRecored.get("cDpl_B_TollDetail_Bus"));
//		}
//		if(mainRecored.get("cDpl_B_TollDetail_Other") != null){
//			params[11] = Integer.parseInt((String)mainRecored.get("cDpl_B_TollDetail_Other"));
//		}

        if(mainRecored.get("cDpl_B_TollDetail_Flights") != null){
            params[8] = mainRecored.get("cDpl_B_TollDetail_Flights");
        }
        if(mainRecored.get("cDpl_B_TollDetail_Train") != null){
            params[9] = mainRecored.get("cDpl_B_TollDetail_Train");
        }
        if(mainRecored.get("cDpl_B_TollDetail_Bus") != null){
            params[10] =mainRecored.get("cDpl_B_TollDetail_Bus");
        }
        if(mainRecored.get("cDpl_B_TollDetail_Other") != null){
            params[11] = mainRecored.get("cDpl_B_TollDetail_Other");
        }

		params[12] = mainRecored.get("cChr_Other");
		if(mainRecored.get("cDel_B_StayMoney") != null){
			params[13] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_StayMoney").toString()));
		}
		if(mainRecored.get("cDel_B_CityTrafficMoney") != null){
			params[14] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_CityTrafficMoney").toString()));
		}
		if(mainRecored.get("cDel_B_TvlAllowanceMoney") != null){
			params[15] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_TvlAllowanceMoney").toString()));
		}
		if(mainRecored.get("cDel_B_CityTrafficMoney2") != null){
			params[16] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_CityTrafficMoney2").toString()));
		}
		if(mainRecored.get("cDel_B_OtherMoney") != null){
			params[17] = BigDecimal.valueOf(Double.parseDouble(mainRecored.get("cDel_B_OtherMoney").toString()));
		}
		params[18] = mainRecored.get("cChr_ChangeReason");
		params[19] = mainRecored.get("cChr_RequestERP");
		params[20] = mainRecored.get("cChr_SubmitterName");
		if(mainRecored.get("cDT_RequestTime") != null){
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime((java.sql.Timestamp)mainRecored.get("cDT_RequestTime"));
				params[21] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		try {
			Object[] res=client.invoke("Create",params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
	
	/**
	 * 出差人信息创建接口
	 * 
	 * @param subRecoredList
	 * @param wsdl
	 */
	private void createTravelPartners(List<Map<String,Object>> subRecoredList,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[12];
//			params[0] = processInstanceId;
            params[0] =subRecored.get("PARENT_ID");
			params[1] = subRecored.get("cChr_P_Company");
			params[2] = subRecored.get("cChr_P_Org");
			params[3] = subRecored.get("cChr_P_Dept");
			params[4] = subRecored.get("cChr_P_Name");
			params[5] = subRecored.get("cChr_P_Email");
			params[6] = subRecored.get("cChr_P_ERP");
			params[7] = subRecored.get("cChr_S_Mobile");
			if(subRecored.get("cDpl_Male") != null){
				if(subRecored.get("cDpl_Male").equals("男")){
					params[8] = 0;
				}else if(subRecored.get("cDpl_Male").equals("女")){
					params[8] = 1;
				}
			}
			params[9] = subRecored.get("cChr_P_Rank");
			if(subRecored.get("cDpl_S_CredentialsType") != null){
				params[10] = Integer.parseInt((String)subRecored.get("cDpl_S_CredentialsType"));
			}
			params[11] = subRecored.get("cChr_S_CredentialsNo");
			try {
				Object[] res = client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
	}
	
	/**
	 * 行程信息创建接口
	 * 
	 * @param subRecoredList
	 * @param wsdl
	 */
	private void createTvlScheduleInfo(List<Map<String,Object>> subRecoredList,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[9];
//			params[0] = processInstanceId;
            params[0] =subRecored.get("PARENT_ID");
			params[1] = subRecored.get("cInt_FlightsCount");
			params[2] = subRecored.get("cChr_S_From");
			params[3] = subRecored.get("cChr_S_Destination");
			params[4] = Integer.parseInt(subRecored.get("cInt_S_Distance").toString());
			if(subRecored.get("cDT_S_StartDate") != null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime((java.sql.Date)subRecored.get("cDT_S_StartDate"));
					params[5] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			params[6] = subRecored.get("cChr_S_DepartureTime");
			if(subRecored.get("cDpl_FlightDone") != null){
				params[7] = Integer.parseInt((String)subRecored.get("cDpl_FlightDone"));
			}
			if(subRecored.get("cDec_ChangeCost") != null){
				params[8] = BigDecimal.valueOf(Double.parseDouble(subRecored.get("cDec_ChangeCost").toString()));
			}
			try {
				Object[] res = client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
	}
	
	/**
	 * 机票信息创建接口
	 * 
	 * @param subRecoredList
	 * @param wsdl
	 */
	private void createTvlAssigneeWorkLog(List<Map<String,Object>> subRecoredList,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[16];
//			params[0] = processInstanceId;
            params[0] =subRecored.get("PARENT_ID");
			if(subRecored.get("cDpl_FlightCheckStatus") != null){
				params[1] = Integer.parseInt((String)subRecored.get("cDpl_FlightCheckStatus"));
			}
			params[2] = subRecored.get("cInt_FlightsCount");
			if(subRecored.get("cDec_Money") != null){
				params[3] = BigDecimal.valueOf(Double.parseDouble(subRecored.get("cDec_Money").toString()));
			}
			params[4] = subRecored.get("cChr_SubmitterName");
			if(subRecored.get("cDT_ProcessDate") !=null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime((java.sql.Timestamp)subRecored.get("cDT_ProcessDate"));
					params[5] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			params[6] = subRecored.get("cChr_FlightPName");
			params[7] = subRecored.get("cChr_FlightCity");
			if(subRecored.get("cDt_FlightDate") !=null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime((java.sql.Date)subRecored.get("cDt_FlightDate"));
					params[8] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			params[9] = subRecored.get("cChr_FlightCompany");
			params[10] = subRecored.get("cChr_FlightNO");
			params[11] = subRecored.get("cChr_FlightTerminal");
            params[12] = null;
            params[13] =null;
//			if(subRecored.get("cChr_time") !=null){
//				try {
//					GregorianCalendar cal = new GregorianCalendar();
//					String timestr = subRecored.get("cChr_time").toString();
//					Time btime = Time.valueOf(timestr+":00");
//					cal.setTime(btime);
//					params[12] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
//				} catch (DatatypeConfigurationException e) {
//					e.printStackTrace();
//				}
//			}
//			if(subRecored.get("cChr_time") !=null){
//				try {
//					GregorianCalendar cal = new GregorianCalendar();
//					String timestr = subRecored.get("cChr_time").toString();
//					Time etime = Time.valueOf(timestr+":00");
//					cal.setTime(etime);
//					params[13] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
//				} catch (DatatypeConfigurationException e) {
//					e.printStackTrace();
//				}
//			}
            params[14] = subRecored.get("cChr_time");
			params[15] = subRecored.get("cChr_F_Remark");
			try {
				Object[] res = client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
	}
	
	/**
	 * 查询部门预算信息
	 * @return
	 */
	public String findDeptBudget(String orgCode, String yearMonth)	{
		try {
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(yearMonth);
			yearMonth = new SimpleDateFormat("yyyyMM").format(date);
		} catch (ParseException e) {
			throw new BusinessException("不识别的日期格式:" + yearMonth);
		}
		List<DeptBudget> list = deptBudgetDao.findByOrgcode(orgCode, yearMonth);
		BigDecimal usedMoney = deptBudgetDao.calcTotalMoneyFromBizTrip(new Date(), orgCode);
		for(DeptBudget d : list){
			d.setUsedBudget(usedMoney.floatValue());
			d.setRemainBudget(d.getMonthlyBudget() - usedMoney.floatValue());
		}
		return new GsonBuilder().serializeNulls().create().toJson(list);
	}

    /**
     * 差旅申请-推送remedy最近一条审批记录
     * @param businessId
     * @param wsdl   url
     * @param methodName  方法名
     */
    public void travelRequestApprovalOperate(String businessId,String wsdl,String methodName){
        ProcessInstanceService processInstanceService = SpringContextUtils.getBean(ProcessInstanceService.class);
        ProcessTaskHistoryService processTaskHistoryService = SpringContextUtils.getBean(ProcessTaskHistoryService.class);
        CXFDynamicClient client=new CXFDynamicClient(wsdl);
        client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
        ProcessInstance PI = new ProcessInstance();
        PI.setBusinessInstanceId(businessId);
        List<ProcessInstance> processiInstanceList = processInstanceService.find(PI);
        if (processiInstanceList.size() > 0) {
            String processInstanceId = processiInstanceList.get(0)
                    .getProcessInstanceId();
            if (null != processInstanceId) {
                ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
                processTaskHistory.setProcessInstanceId(processInstanceId);
                List<ProcessTaskHistory> processTaskHistoryList = processTaskHistoryService
                        .selectByCondition(processTaskHistory);
                int i = 0;
                ProcessTaskHistory PTH= new ProcessTaskHistory();
                if(processTaskHistoryList.size()>0){
                    PTH = processTaskHistoryList.get(processTaskHistoryList.size()-1);
                    Object[] params = new Object[6];
                    params[0] =businessId;// cChr_TRGuid                    OA差旅申请单ID
                    params[1] = PTH.getUserId();// cChr_Approver                     审批人姓名
                    params[2] = PTH.getUserName();
                    if(PTH.getBeginTime() !=null){
                        try {
                            GregorianCalendar cal = new GregorianCalendar();
//                            String timestr = PTH.getBeginTime().toString();
//                            Time etime = Time.valueOf(timestr+":00");
//                            cal.setTime(etime);
                            cal.setTime( PTH.getBeginTime());
                            params[3] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                        } catch (DatatypeConfigurationException e) {
                            e.printStackTrace();
                        }
                    }
                    if(PTH.getEndTime() !=null){
                        try {
                            GregorianCalendar cal = new GregorianCalendar();
//                            String timestr = PTH.getEndTime().toString();
//                            Time etime = Time.valueOf(timestr+":00");
//                            cal.setTime(etime);
                            cal.setTime( PTH.getEndTime());
                            params[4] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
                        } catch (DatatypeConfigurationException e) {
                            e.printStackTrace();
                        }
                    }


//                    params[2] = PTH.getBeginTime().toString();//                    cDT_ApprovalStartTime                            到达审批时间
//                    params[3] = PTH.getEndTime().toString();//                    cDT_ApprovalEndTime                             审批完成时间
                    //                    cInt_ApprovalStatus 审批状态(1已批准[1]，2已拒绝(2)，3待决，4已取消，5其它)
//                      0:草稿 1:审批中 2:退回  3:驳回 4:取消  5:归档
                    if(PTH.getResult().equals("1")||PTH.getResult().equals("2")||PTH.getResult().equals("4")){
                      params[5] = Integer.parseInt((String)PTH.getResult());
                    }
                    try {
                        client.invoke(methodName,params);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new BusinessException(e.getMessage());
                    }
                }
            }
        }
    }
    
    /**
	 * 
	 * @desc 获取指定人员上级经理
	 * @author WXJ
	 * @date 2014-7-5 上午11:28:20
	 *
	 * @param submitUser
	 * @return
	 */
	public String getManager(String submitUser) {
		String managerUser = "";
		
		ProcessTaskService processTaskService = SpringContextUtils.getBean("processTaskService");
				
		managerUser = processTaskService.getManagerUser(submitUser);
		
		return managerUser;
		
	}
    /**
	 * 
	 * @desc 获取指定人员上级经理职级
	 * @author WXJ
	 * @date 2014-7-4 上午11:28:20
	 *
	 * @param submitUser
	 * @return
	 */
	public String getManagerLevel(String submitUser) {
		String managerUser = "";
		String managerLevel = "";
		
		SysUserService sysUserService = SpringContextUtils.getBean("sysUserService");
		ProcessTaskService processTaskService = SpringContextUtils.getBean("processTaskService");
											 
		managerUser = processTaskService.getManagerUser(submitUser);
		
		if (StringUtils.isNotEmpty(managerUser)) {
			SysUser sysUser = sysUserService.getByUserName(managerUser);
			
			if (sysUser!=null) {
				managerLevel = sysUser.getLevelCode();
			}				
		}
		
		if (!managerLevel.startsWith("M")) {
			managerLevel = managerLevel.substring(0,1);
		}
					
		return managerLevel;
		
	}
	
}

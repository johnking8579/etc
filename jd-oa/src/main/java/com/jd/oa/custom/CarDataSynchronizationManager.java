package com.jd.oa.custom;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @desc 车辆借用申请流程
 * @author qiuyang
 *
 */
public class CarDataSynchronizationManager {
	private static final Logger logger = Logger.getLogger(CarDataSynchronizationManager.class);
	//头信息定义
	private Map<String,Object> soapHeaderMap=new HashMap<String, Object>();
	public CarDataSynchronizationManager(){
		//处理头信息
		soapHeaderMap.put("userName","oa_ws");
		soapHeaderMap.put("password","oa@123#");
		soapHeaderMap.put("authentication","");
		soapHeaderMap.put("locale","");
		soapHeaderMap.put("timeZone","");
	}

	public String createCarInfoAllStringParams(String BusinessId,String MainTable,String DetailTable1,String DetailTable2,
								String wsdl,String subwsdl1,String subwsdl2,String beginTime,String endTime,String submitUser,String submitUserRealName){
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		SimpleDateFormat format2 = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a", Locale.ENGLISH);
		Date beginDate = null;
		Date endDate = null;
		try {
			beginDate = format1.parse(beginTime);
			endDate = format1.parse(endTime);
		} catch (ParseException e) {
			try {
				beginDate = format2.parse(beginTime);
				endDate = format2.parse(endTime);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
		return createCarInfo(BusinessId, MainTable, DetailTable1, DetailTable2, wsdl, subwsdl1, subwsdl2, beginDate, endDate, submitUser, submitUserRealName);
	}

	/**
	 * 向remdy推送车辆申请单信息
	 * 
	 * @param BusinessId
	 * @param MainTable
	 * @param DetailTable1
	 * @param DetailTable2
	 * @param wsdl
	 * @param subwsdl1
	 * @param subwsdl2
	 * @param beginTime
	 * @param endTime
	 * @param submitUser
	 * @param submitUserRealName
	 * @return
	 */
	public String createCarInfo(String BusinessId,String MainTable,String DetailTable1,String DetailTable2,
			String wsdl,String subwsdl1,String subwsdl2,Date beginTime,Date endTime,String submitUser,String submitUserRealName){
		CallerInfo info = Profiler.registerInfo("wfp_createcarinfo",false,true); //方法性能监控
		try {
			//加载需要使用的service
			ProcessInstanceService processInstanceService = SpringContextUtils.getBean("processInstanceService");
			FormBusinessTableService formBusinessTableService= SpringContextUtils.getBean("formBusinessTableService");
			
			//test
			//BusinessId = "122703384041961";
			//取回主表记录
			Map<String,Object> mainRecored=formBusinessTableService.getBusinessData(MainTable, BusinessId);
			if(mainRecored == null) {
				throw new BusinessException("找不到行政车辆申请记录");
			}
			//取回流程实例信息
			ProcessInstance processInstance = new ProcessInstance();
			processInstance.setBusinessInstanceId(mainRecored.get("ID").toString());
			List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
			if(processInstances == null || processInstances.size() == 0){
				throw new BusinessException("找不到流程实例信息!");
			}
			String processInstanceId = processInstances.get(0).getProcessInstanceId();
			
			//取回子表记录
			List<Map<String,Object>> sub1RecoredList=formBusinessTableService.getBusinessDatas(DetailTable1, mainRecored.get("ID").toString());
			List<Map<String,Object>> sub2RecoredList=formBusinessTableService.getBusinessDatas(DetailTable2, mainRecored.get("ID").toString());
			
			//test
			/*beginTime = new Date();
			endTime = new Date();
			submitUser = "zhouhuaqi";
			submitUserRealName = "周华旗";*/
			createCarApplyOperate(mainRecored,wsdl,processInstanceId,beginTime,endTime,submitUser,submitUserRealName);
			createCarApplyDetailOperate(sub1RecoredList,subwsdl1,processInstanceId);
			createCarApplyAssocOperate(mainRecored,sub2RecoredList,subwsdl2,processInstanceId,beginTime);
			
			return "sucess";
		} catch (Exception e) {
			logger.error("jme_createCarInfo error!", e);
			Profiler.functionError(info);  //方法可用率监控
			return "error";
		} finally {
			Profiler.registerInfoEnd(info);
		}
	}
	/**
	 * 车辆申请单创建接口
	 * 
	 * @param mainRecored
	 * @param wsdl
	 * @param processInstanceId
	 * @param beginTime
	 * @param endTime
	 * @param submitUser
	 * @param submitUserRealName
	 */
	private void createCarApplyOperate(Map<String,Object> mainRecored,String wsdl,String processInstanceId,
			Date beginTime,Date endTime,String submitUser,String submitUserRealName){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		//给数组赋值
		Object[] params = new Object[26];
		params[0] = processInstanceId;
		params[1] = mainRecored.get("cChr_RequesterPhone");
		params[2] = Integer.parseInt(mainRecored.get("cInt_PersonNumber")+"");
		if(mainRecored.get("start") !=null){
			try {
				String time = new SimpleDateFormat("yyyy-MM-dd").format((java.sql.Date) mainRecored.get("start"));
				String hour = "0"+mainRecored.get("start_h");
				time+= " "+hour.substring(hour.length() - 2)+":00:00";
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(time));
				params[3] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				String time = new SimpleDateFormat("yyyy-MM-dd").format((java.sql.Date) mainRecored.get("start"));
				String hour = "0"+mainRecored.get("end_h");
				time+= " "+hour.substring(hour.length()-2)+":00:00";
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(time));
				params[4] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		params[5] = mainRecored.get("cChr_From");
		params[6] = mainRecored.get("cChr_Destination");
		params[7] = mainRecored.get("cChr_Description");
		params[8] = mainRecored.get("cChr_RequesterCompany");
		params[9] = mainRecored.get("cChr_RequesterOrganization");
		params[10] = mainRecored.get("cChr_RequesterDepartment");
		params[11] = mainRecored.get("cChr_Requester");
		params[12] = mainRecored.get("cChr_RequesterERP");
		params[13] = mainRecored.get("cChr_RequesterEmail");
		
		params[21] = mainRecored.get("cChr_DealInfo");
		if(beginTime !=null){
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(beginTime);
				params[22] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		if(endTime !=null){
			try {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(endTime);
				params[23] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		params[24] = submitUserRealName;
		params[25] = submitUser;
		try {
			Object[] res=client.invoke("Create",params);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
	/**
	 * 车辆申请-期望车辆创建接口
	 * 
	 * @param subRecoredList
	 * @param wsdl
	 * @param processInstanceId
	 */
	private void createCarApplyDetailOperate(List<Map<String,Object>> subRecoredList,String wsdl,String processInstanceId){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[5];
			params[0] = processInstanceId;
			params[1] = subRecored.get("jd_carId");
			params[2] = subRecored.get("jd_carNo");
			params[3] = subRecored.get("jd_carName");
			params[4] = subRecored.get("jd_seatNo");
			try {
				Object[] res = client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
		
	}
	
	/**
	 * 车辆申请-派出车辆创建接口
	 * 
	 * @param mainRecored
	 * @param subRecoredList
	 * @param wsdl
	 * @param processInstanceId
	 * @param beginTime
	 */
	private void createCarApplyAssocOperate(Map<String,Object> mainRecored,List<Map<String,Object>> subRecoredList,
			String wsdl,String processInstanceId,Date beginTime){
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		client.addSoapHeader("AuthenticationInfo",soapHeaderMap);
		
		for(Map<String,Object> subRecored:subRecoredList){
			//给数组赋值
			Object[] params = new Object[14];
			params[0] = processInstanceId;
			params[1] = subRecored.get("jd_carId");
			params[2] = subRecored.get("jd_carNo");
			params[3] = subRecored.get("jd_carName");
			params[4] = Integer.parseInt((String)subRecored.get("jd_seatNo"));
			if(subRecored.get("jd_driver") != null && (subRecored.get("jd_driver").toString()).contains("(")){
				String str = subRecored.get("jd_driver").toString();
				String carDriver = str.substring(0,str.indexOf("("));
				String driverPhone = str.substring(str.indexOf("(")+1,str.indexOf(")"));
				params[5] = carDriver;
				params[6] = driverPhone;
			}
			if(mainRecored.get("start") !=null){
				try {
					String time = new SimpleDateFormat("yyyy-MM-dd").format((java.sql.Date) mainRecored.get("start"));
					String hour = "0"+mainRecored.get("start_h");
					time+= " "+hour.substring(hour.length()-2)+":00:00";
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(time));
					params[7] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					String time = new SimpleDateFormat("yyyy-MM-dd").format((java.sql.Date) mainRecored.get("start"));
					String hour = "0"+mainRecored.get("end_h");
					time+= " "+hour.substring(hour.length()-2)+":00:00";
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(time));
					params[8] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			params[9] = mainRecored.get("cChr_From");
			params[10] = mainRecored.get("cChr_Destination");
			params[11] = mainRecored.get("cChr_Requester");
			if(beginTime !=null){
				try {
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(beginTime);
					params[12] = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}
			params[13] = mainRecored.get("cChr_RequesterPhone");
			try {
				Object[] res = client.invoke("Create",params);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
	}
}

package com.jd.oa.transfer.exp.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.process.xpackage.DiCcInterfaceDocument;
import org.apache.xmlbeans.process.xpackage.DiCcInterfacesDocument;
import org.apache.xmlbeans.process.xpackage.DiDatasourceDocument;
import org.apache.xmlbeans.process.xpackage.DiDatasourcesDocument;
import org.apache.xmlbeans.process.xpackage.ExtendedAttributeDocument.ExtendedAttribute;
import org.apache.xmlbeans.process.xpackage.ExtendedAttributesDocument;
import org.apache.xmlbeans.process.xpackage.FormBusinessFieldsDocument;
import org.apache.xmlbeans.process.xpackage.FormBusinessTableDocument;
import org.apache.xmlbeans.process.xpackage.FormDocument;
import org.apache.xmlbeans.process.xpackage.FormItemsDocument;
import org.apache.xmlbeans.process.xpackage.FormTemplateDocument;
import org.apache.xmlbeans.process.xpackage.FormTemplatesDocument;
import org.apache.xmlbeans.process.xpackage.NodeDocument.Node;
import org.apache.xmlbeans.process.xpackage.NodesDocument;
import org.apache.xmlbeans.process.xpackage.PackageDocument;
import org.apache.xmlbeans.process.xpackage.PackageDocument.Package;
import org.apache.xmlbeans.process.xpackage.PackageHeaderDocument.PackageHeader;
import org.apache.xmlbeans.process.xpackage.PrintTemplateDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDetailDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDetailsDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigsDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeExtendButtonDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeExtendButtonsDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeFormPrivilegesDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeListenerDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeListenersDocument;
import org.apache.xmlbeans.process.xpackage.ProcessNodeListenersDocument.ProcessNodeListeners;
import org.apache.xmlbeans.process.xpackage.SonFormsDocument.SonForms;
import org.apache.xmlbeans.process.xpackage.SysAuthExpressionDocument;
import org.apache.xmlbeans.process.xpackage.SysAuthExpressionsDocument;
import org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionDocument;
import org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionsDocument;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.doc.controller.WfFileController;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessDefinitionConfigDetail;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeExtendButton;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigDetailService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeExtendButtonService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeListenerService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysAuthExpression;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysAuthExpressionService;
import com.jd.oa.system.service.SysBussinessAuthExpressionService;
import com.jd.oa.transfer.exp.ExportContext;
import com.jd.oa.transfer.exp.dto.FormInfoTransferDto;
import com.jd.oa.transfer.exp.dto.ProcessDefinitionTransferDto;
import com.jd.oa.transfer.exp.service.ProcessDefinitionExportService;

/**
 * 流程导出服务
 * 
 * @author wangdongxing
 * 
 */
public class ProcessDefinitionExportServiceImpl implements ProcessDefinitionExportService {
	public static String contextPath = "";
	public final static String WORKFLOW_INIT_XML_PATH = "workflow_init.xml";
	private static final Logger logger = Logger.getLogger(ProcessDefinitionExportServiceImpl.class);
	@Autowired
	ProcessDefService processDefService;
	@Autowired
	ProcessNodeService processNodeService;
	@Autowired
	ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	@Autowired
	ProcessNodeExtendButtonService processNodeExtendButtonService;
	@Autowired
	ProcessDefinitionConfigService processDefinitionConfigService;
	@Autowired
	SysBussinessAuthExpressionService sysBussinessAuthExpressionService;
	@Autowired
	ProcessDefinitionConfigDetailService processDefinitionConfigDetailService;
	@Autowired
	SysAuthExpressionService sysAuthExpressionService;
	@Autowired
	JssService jssService;
	@Autowired
	ProcessNodeListenerService processNodeListenerService;
	@Autowired
	DiDatasourceService diDatasourceService;
	@Autowired
	DiCcInterfaceService diCcInterfaceService;

	// form 相关
	@Autowired
	private FormService formService;

	@Autowired
	private FormItemService formItemService;

	@Autowired
	private FormBusinessTableService formBusinessTableService;

	@Autowired
	private FormBusinessFieldService formBusinessFieldService;

	@Autowired
	private FormTemplateService formTemplateService;

	private String tempDir; // 打包文件的临时目录

	public String getTempDir() {
		return tempDir;
	}

	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}

	/**
	 * 导出流程定义
	 */
	@Override
	public File exportProcessDefinition(String processdefId) {
		if (processdefId == null || processdefId.equals(""))
			return null;
		ProcessDefinition processDefinition = processDefService.get(processdefId);

		ProcessDefinitionTransferDto dto = new ProcessDefinitionTransferDto();
		dto.setProcessDefinition(processDefinition);

		/* 绑定流程定义操作数据 */
		exportProcessDefinition(processDefinition, dto);

		/* 绑定表单操作数据 */
		exportForm(processDefinition, dto);

		/* 生成xml部分 */
		String createWorkflowXmlName = tempDir + "workflow_" + processdefId	+ ".definition.xml";
		String jpdlXmlName = tempDir + "workflow_" + processdefId+ ".bpmn20.xml";
		String templateXmlName = tempDir + "workflow_";
		String zipName = tempDir + "workflow_" + processdefId + ".zip";
		

		List<File> files = new ArrayList<File>(); // 需要打包的文件列表

		// 创建流程定义主文件
		PackageDocument doc = this.parseXml(this.contextPath + "/WEB-INF/classes/workflow_init.xml");
		createDocument(doc, createWorkflowXmlName, dto);
		files.add(new File(createWorkflowXmlName));

		// 创建流程图文件
		File processRouteFile = FileUtil
				.createFile(jpdlXmlName, (dto.getProcessDefinition()
						.getProcessDefinitionFile() == null) ? "" : dto
						.getProcessDefinition().getProcessDefinitionFile());
		files.add(processRouteFile);

		// 创建表单模板文件
		try {
			File templateFile;
			for (FormTemplate template : dto.getFormTemplates()) {
				templateFile = FileUtil.createFile(templateXmlName + template.getTemplatePath(),JavascriptEscape.unescape(formTemplateService.getTemplateValue(template.getTemplatePath())));
				files.add(templateFile);
			}
		} catch (Exception e) {
			throw new BusinessException("创建表单模板失败！", e);
		}
		// 创建js事件文件
		try {
			File formJsFile;
			Form form = dto.getFormInfoTransferDto().getForm();
			formJsFile = FileUtil.createFile(templateXmlName + form.getId()+".js",JavascriptEscape.unescape(formTemplateService.getJavascriptValue(form.getId())));
			files.add(formJsFile);
		} catch (Exception e) {
			throw new BusinessException("创建js事件文件失败！", e);
		}
		
		// 打包文件下载
		if (FileUtil.zipFiles(zipName, files)) {
			return new File(zipName);
		}
		return null;
	}
	/**
	 * 导出流程定义
	 * @param processDefinition
	 * @param dto
	 */
	private void exportProcessDefinition(ProcessDefinition processDefinition,ProcessDefinitionTransferDto dto) {
		List<String> diDataSourceIds = new ArrayList<String>();//数据源ID
		
		if(processDefinition.getIsOuter().equals("2")){   //统一待办流程
			diDataSourceIds.add(processDefinition.getDataSourceId());
			diDataSourceIds.add(processDefinition.getRejectDataSourceId());
		}else{  //内部流程
			// 取得流程定义结点
			ProcessNode node = new ProcessNode();
			node.setProcessDefinitionId(processDefinition.getId());
			List<ProcessNode> processNodes = processNodeService.find(node);
			dto.setProcessNodes(processNodes);
			for (ProcessNode currentNode : processNodes) {
				// 取得流程定义节点的UI模板
				dto.getFormTemplate4ProcessNodeMap().put(currentNode, formTemplateService.get(currentNode.getFormTemplateId()));
				// 取得流程定义结点的打印模板
				dto.getPrintTemplate4ProcessNodeMap().put(currentNode, formTemplateService.get(currentNode.getPrintTemplateId()));
				// 取得流程定义结点的权限表达式
				List<SysBussinessAuthExpression> nodeExpressionList = sysBussinessAuthExpressionService
						.find("findByBusinessId", currentNode.getId());
				dto.getSysBussinessAuthExpression4ProcessNodeMap().put(currentNode,
						nodeExpressionList);
			}

			// 取得流程定义结点个性化设置
			ProcessNodeFormPrivilege privilege = new ProcessNodeFormPrivilege();
			privilege.setProcessDefinitionId(processDefinition.getId());
			List<ProcessNodeFormPrivilege> processNodeFormPrivileges = processNodeFormPrivilegeService
					.selectByCondition(privilege);
			dto.setProcessNodeFormPrivileges(processNodeFormPrivileges);
			
			// 取得流程定义结点动态按钮配置
			ProcessNodeExtendButton extendButton = new ProcessNodeExtendButton();
			extendButton.setProcessDefinitionId(processDefinition.getId());
			//extendButton.setIsExtendButton(1); // 只导出扩展按钮
			List<ProcessNodeExtendButton> processNodeExtendButtons = processNodeExtendButtonService
					.selectByCondition(extendButton);
			dto.setProcessNodeExtendButtons(processNodeExtendButtons);
			
			//取得流程结点事件接口
			ProcessNodeListener nodeListener = new ProcessNodeListener();
			nodeListener.setProcessDefinitionId(processDefinition.getId()); 
			nodeListener.setYn(0);
			List<ProcessNodeListener> nodeListeners = processNodeListenerService.find(nodeListener); 
			dto.setProcessNodeListeners(nodeListeners);
			for(ProcessNodeListener currentNodeListener:nodeListeners){
				if(!diDataSourceIds.contains(currentNodeListener.getInterfaceName()))
					diDataSourceIds.add(currentNodeListener.getInterfaceName());
			}
			
			
		}
		
		//取得数据源
		List<DiDatasource> diDatasources = new ArrayList<DiDatasource>();
		List<String> diCcInterfaceIds = new ArrayList<String>();
		for(String diDataSourceId:diDataSourceIds){
			DiDatasource diDatasource = diDatasourceService.get(diDataSourceId);
			diDatasources.add(diDatasource);
			if(!diCcInterfaceIds.contains(diDatasource.getDataAdapter()))
				diCcInterfaceIds.add(diDatasource.getDataAdapter());
		}
		dto.setDiDataSources(diDatasources);
		
		//取得接口
		List<DiCcInterface> diCcInterfaces = new ArrayList<DiCcInterface>();
		for(String diCcInterfaceId:diCcInterfaceIds){
			DiCcInterface diCcInterface = diCcInterfaceService.get(diCcInterfaceId);
			diCcInterfaces.add(diCcInterface);
		}
		dto.setDiCcInterfaces(diCcInterfaces);
		

		// 取得流程定义字段配置
		ProcessDefinitionConfig processDefinitionConfig = new ProcessDefinitionConfig();
		processDefinitionConfig.setProcessDefinitionId(processDefinition
				.getId());
		List<ProcessDefinitionConfig> configs = processDefinitionConfigService
				.find(processDefinitionConfig);
		dto.setProcessDefinitionConfigs(configs);
		for(ProcessDefinitionConfig cfg:configs){
			List<ProcessDefinitionConfigDetail> details=null;
			if(cfg.getConfigType() != null && cfg.getConfigType().equals("2")){
				ProcessDefinitionConfigDetail processDefinitionConfigDetail=new ProcessDefinitionConfigDetail();
				processDefinitionConfigDetail.setConfigId(cfg.getId());
				details = processDefinitionConfigDetailService.find(processDefinitionConfigDetail);
			}
			dto.getConfigDetails4ProcessdefinitionConfigMap().put(cfg,details);
			//遍历流程变量取得流程变量绑定的办理人权限表达式映射
			if(details != null && details.size() > 0){
				for(ProcessDefinitionConfigDetail detail:details){
					List<SysBussinessAuthExpression> sysBussinessAuthExpressions4Detail=sysBussinessAuthExpressionService.find("findByBusinessId",detail.getId());
					dto.getSysBusinessExpressions4ConfigDetailMap().put(detail,sysBussinessAuthExpressions4Detail);
				}
			}
		}

		// 取得流程定义的权限表达式
		List<SysBussinessAuthExpression> authExpressionList = sysBussinessAuthExpressionService
				.find("findByBusinessId", processDefinition.getId());
		dto.setSysBussinessAuthExpressions(authExpressionList);
		
		//取得流程需要使用权限表达式列表
		List<SysAuthExpression> sysAuthExpressionsList=null;
		List<String> idList=new ArrayList<String>();
		for(SysBussinessAuthExpression bussinessAuthExpression:dto.getSysBussinessAuthExpressions()){
			idList.add(bussinessAuthExpression.getAuthExpressionId());
		}
		for(Entry<ProcessNode,List<SysBussinessAuthExpression>> nodeMap : dto.getSysBussinessAuthExpression4ProcessNodeMap().entrySet()){
			for(SysBussinessAuthExpression bussinessAuthExpression:nodeMap.getValue()){
				idList.add(bussinessAuthExpression.getAuthExpressionId());
			}
		}
		for(Entry<ProcessDefinitionConfigDetail,List<SysBussinessAuthExpression>> configDetailMap : dto.getSysBusinessExpressions4ConfigDetailMap().entrySet()){
			for(SysBussinessAuthExpression bussinessAuthExpression:configDetailMap.getValue()){
				idList.add(bussinessAuthExpression.getAuthExpressionId());
			}
		}
		Map<String,List<String>> map=new HashMap<String, List<String>>();
		map.put("idList",idList);
		sysAuthExpressionsList=sysAuthExpressionService.find("findByIds",map);
		dto.setSysAuthExpressions(sysAuthExpressionsList);
		
		// 取得jss上保存的流程图
		if (processDefinition.getFilePath() != null
				&& !processDefinition.getFilePath().equals("")) {
			processDefinition = processDefService
					.loadProcessDefinitionFile(processDefinition);
			dto.setProcessDefinition(processDefinition);
		}
	}
	/**
	 * 表单相关数据放入dto
	 * @param processDefinition
	 * @param dto
	 * @author yujiahe
	 */

	private void exportForm(ProcessDefinition processDefinition,
			ProcessDefinitionTransferDto dto) {
		//主表相关数据
		String formId = processDefinition.getFormId();
		dto.setFormInfoTransferDto(
				new FormInfoTransferDto(formService.get(formId), 
				formItemService.selectByFormId(formId),
				formBusinessTableService.get(formService.get(formId).getTableId()), 
				formBusinessFieldService.selectTableFieldsByTableId(formService.get(formId).getTableId()))
				);
		//表单模板
		dto.setFormTemplates(formTemplateService.find(formId));
		//子表相关数据
		Form sonForm=new Form();
		sonForm.setParentId(formId);
		List<Form> sonFormList=formService.find(sonForm);
		for(Form f:sonFormList){
			
			dto.getSonFormInfoTransferDto().add(
					new FormInfoTransferDto(formService.get(f.getId()), 
					formItemService.selectByFormId(f.getId()),
					formBusinessTableService.get(formService.get(f.getId()).getTableId()), 
					formBusinessFieldService.selectTableFieldsByTableId(formService.get(f.getId()).getTableId()))
					);
			
		}
		

	}
	/**
	 * 根据 ProcessDefinitionTransferDto创建流程导出schema文件
	 * @param doc
	 * @param file
	 * @param dto
	 */
	public void createDocument(PackageDocument doc, String file,ProcessDefinitionTransferDto dto) {
		PackageDocument.Package packageElement = doc.getPackage();
		// 生成流程定义主数据
		ProcessDefinition processDefinition = dto.getProcessDefinition();
		packageElement.setId(processDefinition.getId());
		packageElement.setName(processDefinition.getProcessDefinitionName());
		// insert packageHeader attribute
		PackageHeader packageHeaderAttribute = packageElement.addNewPackageHeader();
		packageHeaderAttribute.setBPDLModel("eForm(View) Model");
		packageHeaderAttribute.setBPDLVersion(ExportContext.getVersion());
		packageHeaderAttribute.setBPDLVendor(ExportContext.getVendor());
		packageHeaderAttribute.setCreateDate(new Date().toString());
		packageHeaderAttribute.setUpdataDate(new Date().toString());
		packageHeaderAttribute.setDescription("");
		// process start
		packageHeaderAttribute.setProcessTypeId(processDefinition.getProcessTypeId());
		packageHeaderAttribute.setPafProcessDefinitionId(processDefinition.getPafProcessDefinitionId());
		packageHeaderAttribute.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
		packageHeaderAttribute.setOrganizationType(processDefinition.getOrganizationType());
		packageHeaderAttribute.setFilePath(processDefinition.getFilePath());
		packageHeaderAttribute.setIsIssue(processDefinition.getIsIssue());
		packageHeaderAttribute.setFormId(processDefinition.getFormId());
		packageHeaderAttribute.setAlertMode(processDefinition.getAlertMode());
		packageHeaderAttribute.setIsComment(processDefinition.getIsComment());
		packageHeaderAttribute.setIsUploadAttach(processDefinition.getIsUploadAttach());
		packageHeaderAttribute.setIsRelateDoc(processDefinition.getIsRelateDoc());
		packageHeaderAttribute.setIsRelateProcessInstance(processDefinition.getIsRelateProcessInstance());
		packageHeaderAttribute.setIsTemplate(processDefinition.getIsTemplate());
		packageHeaderAttribute.setIsManagerInspect(processDefinition.getIsManagerInspect());
		packageHeaderAttribute.setIsOuter(processDefinition.getIsOuter());
		packageHeaderAttribute.setLinkUrl(processDefinition.getLinkUrl());
		packageHeaderAttribute.setSortNo(processDefinition.getSortNo());
		packageHeaderAttribute.setYn(processDefinition.getYn());
		packageHeaderAttribute.setOwner(processDefinition.getOwner());
		packageHeaderAttribute.setTopLevelCode(processDefinition.getTopLevelCode());
		packageHeaderAttribute.setFollowCodePrefix(processDefinition.getFollowCodePrefix());
		packageHeaderAttribute.setDataSourceId(processDefinition.getDataSourceId());
		packageHeaderAttribute.setRejectDataSourceId(processDefinition.getRejectDataSourceId());

		createDocumentProcessNode(packageElement, dto);//创建流程节点schema
		createDocumentProcessNodeFormPrivilege(packageElement, dto);//创建流程节点表单个性化schema
		createDocumentProcessNodeExtendButton(packageElement, dto);//创建流程节点动态按钮schema
		createDocumentSysBussinessAuthExpression(packageElement,dto.getSysBussinessAuthExpressions());//创建流程绑定的表达式schema(申请，加签)
		createDocumentProcessDefinitionConfig(packageElement, dto);//创流程定义配置schema
		createDocumentSysAuthExpressions(packageElement,dto);//创建权限表单式schema
		createDocumentForm(packageElement, dto);//创建表单schema
		createDocumentProcessNodeListener(packageElement, dto);//创建流程节点事件接口
		createDocumentDiDatasource(packageElement, dto);//创建数据源
		createDocumentDiCcInterface(packageElement, dto);//创建接口


		ExtendedAttributesDocument.ExtendedAttributes extendedAttributesElement = packageElement.addNewExtendedAttributes();
		ExtendedAttribute extendedAttribute = extendedAttributesElement.addNewExtendedAttribute();
		extendedAttribute.setName("");
		extendedAttribute.setValue("");
		/** process end * */

		//生成schema后，进行schema文件校验
		boolean isXmlValid = validateXml(doc, file);
		if (isXmlValid) {
			File f = new File(file);
			File parent = new File(f.getParent());
			if (parent != null && !parent.exists()) {
				parent.mkdirs();
			}
			try {
				// Writing the XML Instance to a file.
				doc.save(f, ExportContext.getDefaultXmlOptions());
				logger.error("导出workflow process模型[ " + f.getPath() + "] --成功!");
			} catch (IOException e) {
				logger.error("导出workflow process模型[ " + f.getPath() + "] --失败!");
				throw new BusinessException("导出workflow process模型[ " + f.getPath() + "] --失败!", e);
			}
		} else {
			throw new BusinessException("导出workflow process模型校验完整性失败:" + file);
		}
	}
	/**
	 * 创建流程所需要的权限表达式
	 * @param packageElement
	 * @param dto
	 */
	private void createDocumentSysAuthExpressions(Package packageElement,ProcessDefinitionTransferDto dto) {
		List<SysAuthExpression> sysAuthExpressions = dto.getSysAuthExpressions();
		SysAuthExpressionsDocument.SysAuthExpressions sysAuthExpressionsElement = packageElement.addNewSysAuthExpressions();
		for(SysAuthExpression expression : sysAuthExpressions){
			if(expression != null){
				SysAuthExpressionDocument.SysAuthExpression sysAuthExpressionAttribute = sysAuthExpressionsElement.addNewSysAuthExpression();
				sysAuthExpressionAttribute.setId(expression.getId());
				sysAuthExpressionAttribute.setMode(expression.getMode());
				sysAuthExpressionAttribute.setAuthExpressionName(expression.getAuthExpressionName());
				sysAuthExpressionAttribute.setOrganizationId(expression.getOrganizationId());
				sysAuthExpressionAttribute.setLevelAbove(expression.getLevelAbove());
				sysAuthExpressionAttribute.setLevelDown(expression.getLevelDown());
				sysAuthExpressionAttribute.setLevelUp(expression.getLevelUp());
				sysAuthExpressionAttribute.setUserId(expression.getUserId());
				sysAuthExpressionAttribute.setGroupId(expression.getGroupId());
				sysAuthExpressionAttribute.setRoleId(expression.getRoleId());
				sysAuthExpressionAttribute.setPositionId(expression.getPositionId());
				sysAuthExpressionAttribute.setOwner(expression.getOwner());
			}
		}
	}

	/**
	 * form 表单相关
	 * 
	 * @param packageElement
	 * @param dto
	 * @author yujiahe
	 */
	private void createDocumentForm(PackageDocument.Package packageElement,ProcessDefinitionTransferDto dto) {
		//主表相关
		Form form = dto.getFormInfoTransferDto().getForm();
		FormDocument.Form formElement = null;
		if (form != null) {
			formElement = packageElement.addNewForm();
			formElement.setId(form.getId());
			formElement.setFormCode(form.getFormCode());
			formElement.setFormName(form.getFormName());
			formElement.setFormType(form.getFormType());
			formElement.setParentId(form.getParentId());
			formElement.setTableId(form.getTableId());
			formElement.setFormDesc(form.getFormDesc());
			formElement.setYn(form.getYn());
			formElement.setOwner(form.getOwner());
			formElement.setCreateType(form.getCreateType());
			createDocumentFormItem(formElement,  dto.getFormInfoTransferDto().getFormItems());//创建表单数据项
			createDocumentFormBusinessTable(formElement, dto.getFormInfoTransferDto().getFormBusinessTable());//创建表单数据表
			createDocumentFormBusinessField(formElement,  dto.getFormInfoTransferDto().getFormBusinessFields());//创建表单字段
			createDocumentFormTemplate4Form(formElement, dto);//创建表单模板
			
			//如果表单有子表的情况
			if(dto.getSonFormInfoTransferDto() != null && dto.getSonFormInfoTransferDto().size() > 0){
				SonForms sonFormElements =formElement.addNewSonForms();
				for(FormInfoTransferDto sf : dto.getSonFormInfoTransferDto()){
					if(sf != null && sf.getForm() != null ){
						FormDocument.Form sonFormElement = sonFormElements.addNewForm();//.addNewForm();
						
						sonFormElement.setId(sf.getForm().getId());
						sonFormElement.setFormCode(sf.getForm().getFormCode());
						sonFormElement.setFormName(sf.getForm().getFormName());
						sonFormElement.setFormType(sf.getForm().getFormType());
						sonFormElement.setParentId(sf.getForm().getParentId());
						sonFormElement.setTableId(sf.getForm().getTableId());
						sonFormElement.setFormDesc(sf.getForm().getFormDesc());
						sonFormElement.setYn(sf.getForm().getYn());
						sonFormElement.setOwner(sf.getForm().getOwner());
						sonFormElement.setCreateType(sf.getForm().getCreateType());
						createDocumentFormItem(sonFormElement,  sf.getFormItems());
						createDocumentFormBusinessTable(sonFormElement, sf.getFormBusinessTable());
						createDocumentFormBusinessField(sonFormElement,  sf.getFormBusinessFields());
					}
					
				}
			}
		}
	 

	}

	/**
	 * 创建表单对应的表单模板
	 * 
	 * @param formElement
	 * @param dto
	 * @author yujiahe
	 */
	private void createDocumentFormTemplate4Form(
			org.apache.xmlbeans.process.xpackage.FormDocument.Form formElement,
			ProcessDefinitionTransferDto dto) {
		List<FormTemplate> formTemplates = dto.getFormTemplates();
		if (formTemplates != null && formTemplates.size() > 0) {
			FormTemplatesDocument.FormTemplates formTemplatesElement = formElement
					.addNewFormTemplates();
			for (int i = 0; i < formTemplates.size(); i++) {
				FormTemplateDocument.FormTemplate formTemplateAttribute = formTemplatesElement
						.addNewFormTemplate();
				formTemplateAttribute.setId(formTemplates.get(i).getId());
				formTemplateAttribute.setFormId(formTemplates.get(i)
						.getFormId());
				formTemplateAttribute.setTemplateDesc(formTemplates.get(i)
						.getTemplateDesc());
				formTemplateAttribute.setTemplateName(formTemplates.get(i)
						.getTemplateName());
				formTemplateAttribute.setTemplatePath(formTemplates.get(i)
						.getTemplatePath());
				formTemplateAttribute.setTemplateType(formTemplates.get(i)
						.getTemplateType());
				formTemplateAttribute.setYn(formTemplates.get(i).getYn());
			}
		}
	}

	/**
	 * formItem 表单项
	 * 
	 * @param formElement
	 * @param dto
	 * @author yujiahe
	 */
	private void createDocumentFormItem(FormDocument.Form formElement,
			List<FormItem> formItems) {
		//List<FormItem> formItems = dto.getFormInfoTransferDto().getFormItems();

		if (formItems != null && formItems.size() > 0) {

			FormItemsDocument.FormItems formItemsElement = formElement
					.addNewFormItems();
			for (int i = 0; i < formItems.size(); i++) {
				FormItem formItem = formItems.get(i);
				if (formItem != null) {
					org.apache.xmlbeans.process.xpackage.FormItemDocument.FormItem formItemAttribute = formItemsElement
							.addNewFormItem();
					formItemAttribute.setId(formItem.getId());
					formItemAttribute.setFormId(formItem.getFormId());
					formItemAttribute.setFieldName(formItem.getFieldName());
					formItemAttribute.setFieldChineseName(formItem
							.getFieldChineseName());
					formItemAttribute.setTableType(formItem.getTableType());
					formItemAttribute.setFieldId(formItem.getFieldId());
					formItemAttribute.setDataType(formItem.getDataType());
					formItemAttribute.setLength(formItem.getLength());
					formItemAttribute.setDefaultValue(formItem
							.getDefaultValue());
					formItemAttribute.setInputType(formItem.getInputType());
					formItemAttribute.setInputTypeConfig(formItem
							.getInputTypeConfig());
					formItemAttribute.setDictTypeCode(formItem
							.getDictTypeCode());
					formItemAttribute.setSortNo(formItem.getSortNo());
					formItemAttribute.setValidateRule(formItem
							.getValidateRule());
					formItemAttribute.setYn(formItem.getYn());
					formItemAttribute.setComputeExpress(formItem
							.getComputeExpress());
					formItemAttribute.setComputeExpressDesc(formItem
							.getComputeExpressDesc());
				}
			}
		}

	}

	/**
	 * 业务表相关信息
	 * 
	 * @param formElement
	 * @param dto
	 * @author yujiahe
	 */

	private void createDocumentFormBusinessTable(FormDocument.Form formElement,
			FormBusinessTable formBusinessTable) {
		if (formBusinessTable != null) {
			FormBusinessTableDocument.FormBusinessTable formBusinessTableElement = formElement
					.addNewFormBusinessTable();
			formBusinessTableElement.setId(formBusinessTable
					.getId());
			formBusinessTableElement.setTableName(formBusinessTable
					.getTableName());
			formBusinessTableElement.setTableChineseName(formBusinessTable
					.getTableChineseName());
			formBusinessTableElement.setTableDesc(formBusinessTable
					.getTableDesc());
			formBusinessTableElement.setTableType(formBusinessTable
					.getTableType());
			formBusinessTableElement.setParentId(formBusinessTable
					.getParentId());
			formBusinessTableElement.setCreateType(formBusinessTable
					.getCreateType());
			formBusinessTableElement.setYn(formBusinessTable.getYn());
			formBusinessTableElement.setOwner(formBusinessTable.getOwner());

		}

	}

	/**
	 * 业务表项相关信息
	 * 
	 * @param formElement
	 * @param dto
	 * @author yujiahe
	 */
	private void createDocumentFormBusinessField(FormDocument.Form formElement,
			List<FormBusinessField> formBusinessFields) {

		if (formBusinessFields != null && formBusinessFields.size() > 0) {

			FormBusinessFieldsDocument.FormBusinessFields formBusinessFieldsElement = formElement
					.addNewFormBusinessFields();
			for (int i = 0; i < formBusinessFields.size(); i++) {
				FormBusinessField formBusinessField = formBusinessFields.get(i);
				if (formBusinessField != null) {
					org.apache.xmlbeans.process.xpackage.FormBusinessFieldDocument.FormBusinessField formBusinessFieldAttribute = formBusinessFieldsElement
							.addNewFormBusinessField();
					formBusinessFieldAttribute.setId(formBusinessField.getId());
					formBusinessFieldAttribute.setTableId(formBusinessField
							.getTableId());
					formBusinessFieldAttribute.setFieldName(formBusinessField
							.getFieldName());
					formBusinessFieldAttribute
							.setFieldChineseName(formBusinessField
									.getFieldChineseName());
					formBusinessFieldAttribute.setDataType(formBusinessField
							.getDataType());
					formBusinessFieldAttribute.setIsNull(formBusinessField
							.getIsNull());
					formBusinessFieldAttribute.setLength(formBusinessField
							.getLength());
					formBusinessFieldAttribute
							.setDefaultValue(formBusinessField
									.getDefaultValue());
					formBusinessFieldAttribute.setInputType(formBusinessField
							.getInputType());
					formBusinessFieldAttribute
							.setInputTypeConfig(formBusinessField
									.getInputTypeConfig());
					formBusinessFieldAttribute
							.setDictTypeCode(formBusinessField
									.getDictTypeCode());
					formBusinessFieldAttribute.setSortNo(formBusinessField
							.getSortNo());
					formBusinessFieldAttribute.setYn(formBusinessField.getYn());

				}
			}
		}

	}

	// 生成流程定义关键字段数据
	private void createDocumentProcessDefinitionConfig(Package packageElement,ProcessDefinitionTransferDto dto) {
		List<ProcessDefinitionConfig> processDefinitionConfigs = dto.getProcessDefinitionConfigs();
		if (processDefinitionConfigs != null && processDefinitionConfigs.size() > 0) {
			ProcessDefinitionConfigsDocument.ProcessDefinitionConfigs processDefinitionConfigsElement = packageElement.addNewProcessDefinitionConfigs();
			for (int i = 0; i < processDefinitionConfigs.size(); i++) {
				ProcessDefinitionConfig processDefinitionConfig = processDefinitionConfigs.get(i);
				if (processDefinitionConfig != null) {
					org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDocument.ProcessDefinitionConfig definitionConfigAttribute = processDefinitionConfigsElement
							.addNewProcessDefinitionConfig();
					definitionConfigAttribute
							.setProcessDefinitionId(processDefinitionConfig
									.getProcessDefinitionId());
					definitionConfigAttribute
							.setConfigType(processDefinitionConfig
									.getConfigType());
					definitionConfigAttribute
							.setConfigItem(processDefinitionConfig
									.getConfigItem());
					definitionConfigAttribute.setSortNo(processDefinitionConfig.getSortNo()==null?0:processDefinitionConfig.getSortNo());
					definitionConfigAttribute.setYn(processDefinitionConfig
							.getYn());
					if(processDefinitionConfig.getConfigType() != null && processDefinitionConfig.getConfigType().equals("2")){
						List<ProcessDefinitionConfigDetail> configDetails = dto.getConfigDetails4ProcessdefinitionConfigMap().get(processDefinitionConfig);
						if(configDetails != null && configDetails.size() > 0){
							ProcessDefinitionConfigDetailsDocument.ProcessDefinitionConfigDetails processDefinitionConfigDetailsElement
													= definitionConfigAttribute.addNewProcessDefinitionConfigDetails();
							for(int j = 0 ; j < configDetails.size() ; j++){
								ProcessDefinitionConfigDetailDocument.ProcessDefinitionConfigDetail detailAtrribute 
												= processDefinitionConfigDetailsElement.addNewProcessDefinitionConfigDetail();
								ProcessDefinitionConfigDetail cd=configDetails.get(j);
								if(cd != null){
									detailAtrribute.setId(cd.getId());
									detailAtrribute.setConfigId(cd.getConfigId());
									detailAtrribute.setName(cd.getName());
									detailAtrribute.setExpression(cd.getExpression());
									detailAtrribute.setValNo(cd.getValNo());
									detailAtrribute.setYn(cd.getYn());
									//继续导出流程变量对应的权限表达式映射
									List<SysBussinessAuthExpression> sysBussinessAuthExpressions = dto.getSysBusinessExpressions4ConfigDetailMap().get(configDetails.get(j));
									if(sysBussinessAuthExpressions != null && sysBussinessAuthExpressions.size() > 0){
										SysBussinessAuthExpressionsDocument.SysBussinessAuthExpressions sysBusAuthExpsElement = detailAtrribute.addNewSysBussinessAuthExpressions();
										for(SysBussinessAuthExpression expression : sysBussinessAuthExpressions){
											if(expression != null){
												SysBussinessAuthExpressionDocument.SysBussinessAuthExpression sysBusAuthExpsAttribute=sysBusAuthExpsElement.addNewSysBussinessAuthExpression();
												sysBusAuthExpsAttribute.setId(expression.getId());
												sysBusAuthExpsAttribute.setBusinessType(expression.getBusinessType());
												sysBusAuthExpsAttribute.setBusinessId(expression.getBusinessId());
												sysBusAuthExpsAttribute.setAuthExpressionId(expression.getAuthExpressionId());
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// 生成流程绑定的权限表达式数据
	private void createDocumentSysBussinessAuthExpression(Package packageElement,List<SysBussinessAuthExpression> sysBussinessAuthExpressions) {
		if (sysBussinessAuthExpressions != null && sysBussinessAuthExpressions.size() > 0) {
			SysBussinessAuthExpressionsDocument.SysBussinessAuthExpressions sysBussinessAuthExpressionsElement = packageElement
					.addNewSysBussinessAuthExpressions();
			for (int i = 0; i < sysBussinessAuthExpressions.size(); i++) {
				SysBussinessAuthExpression sysBussinessAuthExpression = sysBussinessAuthExpressions
						.get(i);
				if (sysBussinessAuthExpression != null) {
					org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionDocument.SysBussinessAuthExpression sysBussinessAuthExpressionAttribute = sysBussinessAuthExpressionsElement
							.addNewSysBussinessAuthExpression();
					sysBussinessAuthExpressionAttribute
							.setId(sysBussinessAuthExpression.getId());
					sysBussinessAuthExpressionAttribute
							.setBusinessType(sysBussinessAuthExpression
									.getBusinessType());
					sysBussinessAuthExpressionAttribute
							.setBusinessId(sysBussinessAuthExpression
									.getBusinessId());
					sysBussinessAuthExpressionAttribute
							.setAuthExpressionId(sysBussinessAuthExpression
									.getAuthExpressionId());
				}
			}
		}
	}

	// 生成流程结点个性化数据
	private void createDocumentProcessNodeFormPrivilege(Package packageElement,ProcessDefinitionTransferDto dto) {
		List<ProcessNodeFormPrivilege> processNodeFormPrivileges = dto.getProcessNodeFormPrivileges();
		if (processNodeFormPrivileges != null && processNodeFormPrivileges.size() > 0) {
			ProcessNodeFormPrivilegesDocument.ProcessNodeFormPrivileges processNodeFormPrivilegesElement = packageElement
					.addNewProcessNodeFormPrivileges();
			for (int i = 0; i < processNodeFormPrivileges.size(); i++) {
				ProcessNodeFormPrivilege processNodeFormPrivilege = processNodeFormPrivileges.get(i);
				if (processNodeFormPrivilege != null) {
					org.apache.xmlbeans.process.xpackage.ProcessNodeFormPrivilegeDocument.ProcessNodeFormPrivilege processNodeFormPrivilegeAttribute = processNodeFormPrivilegesElement
							.addNewProcessNodeFormPrivilege();
					processNodeFormPrivilegeAttribute
							.setFormId(processNodeFormPrivilege.getFormId());
					processNodeFormPrivilegeAttribute
							.setNodeId(processNodeFormPrivilege.getNodeId());
					processNodeFormPrivilegeAttribute
							.setProcessDefinitionId(processNodeFormPrivilege
									.getProcessDefinitionId());
					processNodeFormPrivilegeAttribute
							.setItemId(processNodeFormPrivilege.getItemId());
					processNodeFormPrivilegeAttribute
							.setIsEdit(processNodeFormPrivilege.getEdit());
					processNodeFormPrivilegeAttribute
							.setIsHidden(processNodeFormPrivilege.getHidden());
					processNodeFormPrivilegeAttribute
							.setIsNull(processNodeFormPrivilege.getNull());
					processNodeFormPrivilegeAttribute
							.setIsVariable(processNodeFormPrivilege
									.getVariable());
					processNodeFormPrivilegeAttribute
							.setYn(processNodeFormPrivilege.getYn());
				}
			}
		}
	}
	
	//生成结点动态按钮配置数据
	private void createDocumentProcessNodeExtendButton(Package packageElement,ProcessDefinitionTransferDto dto){
		List<ProcessNodeExtendButton> processNodeExtendButtons = dto.getProcessNodeExtendButtons();
		if (null != processNodeExtendButtons && !processNodeExtendButtons.isEmpty()) {
			ProcessNodeExtendButtonsDocument.ProcessNodeExtendButtons processNodeExtendButtonsElement = 
					packageElement.addNewProcessNodeExtendButtons();
			for(ProcessNodeExtendButton processNodeExtendButton:processNodeExtendButtons){
				if(null != processNodeExtendButton){
					ProcessNodeExtendButtonDocument.ProcessNodeExtendButton processNodeExtendButtonAttribute = 
							processNodeExtendButtonsElement.addNewProcessNodeExtendButton();
					processNodeExtendButtonAttribute.setProcessDefinitionId(processNodeExtendButton.getProcessDefinitionId());
					processNodeExtendButtonAttribute.setNodeId(processNodeExtendButton.getNodeId());
					processNodeExtendButtonAttribute.setFormId(processNodeExtendButton.getFormId());
					processNodeExtendButtonAttribute.setIsExtendButton(processNodeExtendButton.getIsExtendButton());
					processNodeExtendButtonAttribute.setButtonName(processNodeExtendButton.getButtonName());
					processNodeExtendButtonAttribute.setButtonExegesis(processNodeExtendButton.getButtonExegesis());
					processNodeExtendButtonAttribute.setFunctionName(processNodeExtendButton.getFunctionName());
					processNodeExtendButtonAttribute.setStatus(processNodeExtendButton.getStatus());
					processNodeExtendButtonAttribute.setOrderNo(processNodeExtendButton.getOrderNo());
					processNodeExtendButtonAttribute.setYn(processNodeExtendButton.getYn());
				}
			}
		}
		
	}
	
	//生成流程节点事件接口数据
	private void createDocumentProcessNodeListener(Package packageElement,ProcessDefinitionTransferDto dto){
		List<ProcessNodeListener> processNodeListeners = dto.getProcessNodeListeners();
		if(null != processNodeListeners && !processNodeListeners.isEmpty()){
			ProcessNodeListenersDocument.ProcessNodeListeners processNodeListenersElement = 
					packageElement.addNewProcessNodeListeners();
			for(ProcessNodeListener processNodeListener:processNodeListeners){
				if(null != processNodeListener){
					ProcessNodeListenerDocument.ProcessNodeListener processNodeListenerAttribute = 
							processNodeListenersElement.addNewProcessNodeListener();
					processNodeListenerAttribute.setProcessDefinitionId(processNodeListener.getProcessDefinitionId());
					processNodeListenerAttribute.setNodeId(processNodeListener.getNodeId());
					processNodeListenerAttribute.setEvent(processNodeListener.getEvent());
					processNodeListenerAttribute.setInterfaceName(processNodeListener.getInterfaceName());
					processNodeListenerAttribute.setYn(processNodeListener.getYn());
				}
			}
		}
	}
	
	//生成数据源数据
	private void createDocumentDiDatasource(Package packageElement,ProcessDefinitionTransferDto dto){
		List<DiDatasource> diDatasources = dto.getDiDataSources();
		if(null != diDatasources && !diDatasources.isEmpty()){
			DiDatasourcesDocument.DiDatasources diDatasourcesElement = 
					packageElement.addNewDiDatasources();
			for(DiDatasource diDatasource:diDatasources){
				if(null != diDatasource){
					DiDatasourceDocument.DiDatasource diDatasourceAttribute = 
							diDatasourcesElement.addNewDiDatasource();
					diDatasourceAttribute.setId(diDatasource.getId());
					diDatasourceAttribute.setName(diDatasource.getName());
					diDatasourceAttribute.setDataAdapter(diDatasource.getDataAdapter());
					diDatasourceAttribute.setExecSql(diDatasource.getExecSql());
					diDatasourceAttribute.setDataAdapterType(diDatasource.getDataAdapterType());
					diDatasourceAttribute.setYn(diDatasource.getYn());
				}
			}
		}
	}
	
	//生成接口数据
	private void createDocumentDiCcInterface(Package packageElement,ProcessDefinitionTransferDto dto){
		List<DiCcInterface> diCcInterfaces = dto.getDiCcInterfaces();
		if(null != diCcInterfaces && !diCcInterfaces.isEmpty()){
			DiCcInterfacesDocument.DiCcInterfaces diCcInterfacesElement = 
					packageElement.addNewDiCcInterfaces();
			for(DiCcInterface diCcInterface:diCcInterfaces){
				if(null != diCcInterface){
					DiCcInterfaceDocument.DiCcInterface diCcInterfaceAttribute = 
							diCcInterfacesElement.addNewDiCcInterface();
					diCcInterfaceAttribute.setId(diCcInterface.getId());
					diCcInterfaceAttribute.setInterfaceName(diCcInterface.getInterfaceName());
					diCcInterfaceAttribute.setInterfaceType(diCcInterface.getInParameterType());
					diCcInterfaceAttribute.setInterfaceDesc(diCcInterface.getInterfaceDesc());
					diCcInterfaceAttribute.setUrl(diCcInterface.getUrl());
					diCcInterfaceAttribute.setMethod(diCcInterface.getMethod());
					diCcInterfaceAttribute.setAuthHeader(diCcInterface.getAuthHeader());
					diCcInterfaceAttribute.setInParameterType(diCcInterface.getInParameterType());
					diCcInterfaceAttribute.setInParameterTemplate(diCcInterface.getInParameterTemplate());
					diCcInterfaceAttribute.setOutParameterType(diCcInterface.getOutParameterType());
					diCcInterfaceAttribute.setOutParameterTemplate(diCcInterface.getOutParameterTemplate());
					diCcInterfaceAttribute.setYn(diCcInterface.getYn());
				}
			}
		}
	}

	// 生成流程结点数据
	private void createDocumentProcessNode(PackageDocument.Package packageElement,ProcessDefinitionTransferDto dto) {
		List<ProcessNode> nodes = dto.getProcessNodes();
		if (nodes != null && nodes.size() > 0) {
			NodesDocument.Nodes nodesElement = packageElement.addNewNodes();
			for (int i = 0; i < nodes.size(); i++) {
				ProcessNode node = nodes.get(i);
				if (node != null) {
					Node nodeAttribute = nodesElement.addNewNode();
					nodeAttribute.setId(node.getId());
					nodeAttribute.setProcessDefinitionId(node.getProcessDefinitionId());
					nodeAttribute.setNodeId(node.getNodeId());
					nodeAttribute.setFormTemplateId(node.getFormTemplateId());
					nodeAttribute.setPrintTemplateId(node.getPrintTemplateId());
					nodeAttribute.setIsFirstNode(node.getIsFirstNode());
					nodeAttribute.setAddsignRule(node.getAddsignRule());
					nodeAttribute.setManagerNodeId(node.getManagerNodeId());
					nodeAttribute.setCandidateUser(node.getCandidateUser());
					nodeAttribute.setYn(node.getYn());
					createDocumentFormTemplate(nodeAttribute, dto.getFormTemplate4ProcessNodeMap().get(node));// 创建节点关联的表单模板
					createDocumentPrintTemplate(nodeAttribute, dto.getPrintTemplate4ProcessNodeMap().get(node));// 创建节点关联的打印表单模板
					createDocumentSysBussinessAuthExpression4Node(nodeAttribute,dto.getSysBussinessAuthExpression4ProcessNodeMap().get(node));//创建节点对应的任务办理者权限schema
				}
			}
		}
	}

	// 生成打印模板数据
	private void createDocumentPrintTemplate(Node nodeAttribute,FormTemplate formTemplate) {
		if (formTemplate != null) {
			PrintTemplateDocument.PrintTemplate printTemplateElement = nodeAttribute.addNewPrintTemplate();
			printTemplateElement.setId(formTemplate.getId());
			printTemplateElement.setFormId(formTemplate.getFormId());
			printTemplateElement.setTemplateDesc(formTemplate.getTemplateDesc());
			printTemplateElement.setYn(formTemplate.getYn());
			printTemplateElement.setTemplateName(formTemplate.getTemplateName());
			printTemplateElement.setTemplatePath(formTemplate.getTemplatePath());
			printTemplateElement.setTemplateType(formTemplate.getTemplatePath());
		}
	}

	// 生成模版数据
	private void createDocumentFormTemplate(Node nodeAttribute,FormTemplate formTemplate) {
		if (formTemplate != null) {
			FormTemplateDocument.FormTemplate formTemplateElement = nodeAttribute.addNewFormTemplate();
			formTemplateElement.setId(formTemplate.getId());
			formTemplateElement.setFormId(formTemplate.getFormId());
			formTemplateElement.setTemplateDesc(formTemplate.getTemplateDesc());
			formTemplateElement.setYn(formTemplate.getYn());
			formTemplateElement.setTemplateName(formTemplate.getTemplateName());
			formTemplateElement.setTemplatePath(formTemplate.getTemplatePath());
			formTemplateElement.setTemplateType(formTemplate.getTemplatePath());
		}
	}

	// 生成流程结点绑定的权限表达式
	private void createDocumentSysBussinessAuthExpression4Node(	Node nodeAttribute,List<SysBussinessAuthExpression> sysBussinessAuthExpressions) {
		if (sysBussinessAuthExpressions != null	&& sysBussinessAuthExpressions.size() > 0) {
			SysBussinessAuthExpressionsDocument.SysBussinessAuthExpressions sysBussinessAuthExpressionsElement = nodeAttribute
					.addNewSysBussinessAuthExpressions();
			for (int i = 0; i < sysBussinessAuthExpressions.size(); i++) {
				SysBussinessAuthExpression sysBussinessAuthExpression = sysBussinessAuthExpressions.get(i);
				if (sysBussinessAuthExpression != null) {
					org.apache.xmlbeans.process.xpackage.SysBussinessAuthExpressionDocument.SysBussinessAuthExpression sysBussinessAuthExpressionAttribute = sysBussinessAuthExpressionsElement
							.addNewSysBussinessAuthExpression();
					sysBussinessAuthExpressionAttribute.setId(sysBussinessAuthExpression.getId());
					sysBussinessAuthExpressionAttribute.setBusinessType(sysBussinessAuthExpression.getBusinessType());
					sysBussinessAuthExpressionAttribute.setBusinessId(sysBussinessAuthExpression.getBusinessId());
					sysBussinessAuthExpressionAttribute.setAuthExpressionId(sysBussinessAuthExpression.getAuthExpressionId());
				}
			}
		}
	}

	public boolean validateXml(XmlObject xml, String fileName) {
		boolean isXmlValid = false;
		ArrayList validationMessages = new ArrayList();
		isXmlValid = xml.validate(new XmlOptions()
				.setErrorListener(validationMessages));
		if (!isXmlValid) {
			logger.error("XML文件完整性错误:" + fileName);
			for (int i = 0; i < validationMessages.size(); i++) {
				XmlError error = (XmlError) validationMessages.get(i);
				// System.out.println(xml.toString());
				logger.error(error.getColumn());
				logger.error(error.getMessage());
				logger.error(error.getObjectLocation());
			}
		}
		return isXmlValid;
	}

	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;

		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (XmlException e) {
			logger.error("file【" + file + "】校验失败 : " + e.getMessage(),e);
		} catch (IOException e) {
			logger.error("file【" + file + "】错误 : " + e.getMessage(),e);
		}
		return doc;
	}

}

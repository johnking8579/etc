package com.jd.oa.transfer.imp.service;

import java.io.InputStream;

public interface ProcessDefinitionImportService {
	public void importProcessDefinition(InputStream inputStream);
}

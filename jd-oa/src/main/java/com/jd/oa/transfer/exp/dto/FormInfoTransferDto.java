/**
 * 
 */
package com.jd.oa.transfer.exp.dto;

import java.util.List;

import org.hamcrest.core.IsNull;

import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;

/**
 * 表单相关数据
 * 
 * @author yujiahe
 * @date 2013-12-14
 * 
 */
public class FormInfoTransferDto {

	private Form form; // 流程绑定的表单

	private List<FormItem> formItems; // 表单设置的字段

	private FormBusinessTable formBusinessTable;// 表单数据表

	private List<FormBusinessField> formBusinessFields;// 表单数据表

	public FormInfoTransferDto() {

	}

	public FormInfoTransferDto(Form form, List<FormItem> formItems,
			FormBusinessTable formBusinessTable,
			List<FormBusinessField> formBusinessFields) {
		this.form = form;
		this.formBusinessFields = formBusinessFields;
		this.formBusinessTable = formBusinessTable;
		this.formItems = formItems;

	}

	/**
	 * @return the form
	 */
	public Form getForm() {
		return form;
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public void setForm(Form form) {
		this.form = form;
	}

	/**
	 * @return the formItems
	 */
	public List<FormItem> getFormItems() {
		return formItems;
	}

	/**
	 * @param formItems
	 *            the formItems to set
	 */
	public void setFormItems(List<FormItem> formItems) {
		this.formItems = formItems;
	}

	/**
	 * @return the formBusinessTable
	 */
	public FormBusinessTable getFormBusinessTable() {
		return formBusinessTable;
	}

	/**
	 * @param formBusinessTable
	 *            the formBusinessTable to set
	 */
	public void setFormBusinessTable(FormBusinessTable formBusinessTable) {
		this.formBusinessTable = formBusinessTable;
	}

	/**
	 * @return the formBusinessFields
	 */
	public List<FormBusinessField> getFormBusinessFields() {
		return formBusinessFields;
	}

	/**
	 * @param formBusinessFields
	 *            the formBusinessFields to set
	 */
	public void setFormBusinessFields(List<FormBusinessField> formBusinessFields) {
		this.formBusinessFields = formBusinessFields;
	}

	public boolean equalFormBusinessFieldList(
			List<FormBusinessField> formBusinessFields) {
		if (this.formBusinessFields.size() == formBusinessFields.size()) {
			for (FormBusinessField formBusinessField : formBusinessFields) {
				if ( this
						.containsFormBusinessField(formBusinessField)) {
					continue;
				} else {
					return false;
				}
			}
			return true;

		} else {
			return false;
		}

	}

	public boolean containsFormBusinessField(FormBusinessField formBusinessField) {
		
		for (FormBusinessField fbf : this.formBusinessFields) {
			if (stringEquals(fbf.getFieldName(),formBusinessField.getFieldName())
					&& stringEquals(fbf.getFieldChineseName(),
							formBusinessField.getFieldChineseName())
					&& stringEquals(fbf.getDataType() 
							,formBusinessField.getDataType())
					&& stringEquals(fbf.getIsNull(),formBusinessField.getIsNull())
					&& stringEquals(fbf.getLength(),formBusinessField.getLength())
					&& stringEquals(fbf.getDefaultValue(),
							formBusinessField.getDefaultValue())
					&& stringEquals(fbf.getInputType(),
							formBusinessField.getInputType())
					&& stringEquals(fbf.getInputTypeConfig(),
							formBusinessField.getInputTypeConfig())
					&& stringEquals(fbf.getDictTypeCode(),
							formBusinessField.getDictTypeCode())
					&& fbf.getSortNo() == formBusinessField.getSortNo()
					&& fbf.getYn() == formBusinessField.getYn()) {
				return true;
			} else {
				continue;
			}

		}
		return false;


	}

	public boolean equalFormBusinessTable(FormBusinessTable formBusinessTable) {
		
		if (stringEquals(this.formBusinessTable.getTableName(),
				formBusinessTable.getTableName())
				&& stringEquals(this.formBusinessTable.getTableChineseName(),
						formBusinessTable.getTableChineseName())
				&& stringEquals(this.formBusinessTable.getTableDesc(),
						formBusinessTable.getTableDesc())
				&&stringEquals( this.formBusinessTable.getTableType(),
						formBusinessTable.getTableType())
				&& stringEquals(this.formBusinessTable.getCreateType(),
						formBusinessTable.getCreateType())
				&& this.formBusinessTable.getYn() == formBusinessTable.getYn()) {
			return true;
		} else {
			return false;
		}



	}

	public boolean equalForm(Form form) {
		if (stringEquals(this.form.getFormCode(),form.getFormCode())
				&& stringEquals(this.form.getFormName(),form.getFormName())
				&& stringEquals(this.form.getFormType(),form.getFormType())
				&& stringEquals(this.form.getFormDesc(),form.getFormDesc())
				&& stringEquals(this.form.getCreateType(),form.getCreateType())
				&& this.form.getYn() == form.getYn()) {
			return true;
		} else {
			return false;
		}

	}

	public boolean equalFormItemList(List<FormItem> formItems) {
		if (this.formItems.size() == formItems.size()) {
			for (FormItem formItem : formItems) {
				if ( this
						.containsFormItem(formItem)) {
					continue;
				} else {
					return false;
				}
			}
			return true;

		} else {
			return false;
		}

	}
/**
 * 
 * @param formItem
 * @return
 */
	public boolean containsFormItem(FormItem formItem) {
		
		for (FormItem fi : this.formItems) {
			if (stringEquals(fi.getFieldName(),formItem.getFieldName())
					&& stringEquals(fi.getFieldChineseName(),
							formItem.getFieldChineseName())
//					&&stringEquals( fi.getTableType(),formItem.getTableType())
					&&stringEquals( fi.getFieldId(),formItem.getFieldId())
					&&stringEquals( fi.getDataType(),formItem.getDataType())

					&&stringEquals( fi.getLength(),formItem.getLength())
					&&stringEquals( fi.getDefaultValue(),formItem.getDefaultValue())
					&& stringEquals(fi.getInputType(),formItem.getInputType())
					&&stringEquals( fi.getInputTypeConfig(),
							formItem.getInputTypeConfig())
					&&stringEquals( fi.getDictTypeCode(),formItem.getDictTypeCode())
//					&&fi.getSortNo() == formItem.getSortNo()
					&&stringEquals( fi.getComputeExpress(),
							formItem.getComputeExpress())
					&& stringEquals(fi.getComputeExpressDesc(),
							formItem.getComputeExpressDesc())
//					&& fi.getYn() == formItem.getYn()
					) {
				return true;
			} else {
				continue;
			}

		}
		return false;

	}
	public  boolean stringEquals(String a, String b){
		if(a == null || "".equals(a) || null == b || "".equals(b)){
			if( (null == b || "".equals(b)) && (null == a || "".equals(a))){
				return true;
			}else{
				return false;
			}
		}
		return a.equals(b);
	}
	
	
	
	

}

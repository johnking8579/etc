package com.jd.oa.di.controller;

import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcDbService;
import com.jd.oa.di.service.DiDatasourceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Controller
@RequestMapping("/di/cc/db")
public class DiCcDbController {

	@Autowired
	private DiCcDbService diCcDbService;

	@RequestMapping(value = "db_index")
	public String index(){
		return "di/cc/db_index";

	}

	@RequestMapping(value = "db_index_bak")
	public String db_index_bak(){
		return "di/cc/db_index_bak";
	}

	@RequestMapping(value = "db_add")
	public String db_add(){
		return "di/cc/db_add";
	}
	
	
	
	@RequestMapping(value = "db_update")
	public ModelAndView db_update(@RequestParam String id){
		DiCcDb db = diCcDbService.get(id) ;
		ModelAndView mav = new ModelAndView("di/cc/db_update") ;
		
		mav.addObject("ccdb", db);
		return mav;
	}
	
	
	@RequestMapping(value = "db_addAjax")
	@ResponseBody
	public String db_addAjax(DiCcDb db){
		String id = diCcDbService.insert(db);
		return id;
	}
	
	
	
	@RequestMapping(value = "db_updateAjax")
	@ResponseBody
	public String db_updateAjax(DiCcDb db){
		int count = diCcDbService.update(db);
		return Integer.valueOf(count).toString();
	}
	
	@RequestMapping(value = "db_deleteAjax")
	@ResponseBody
	public String db_edit(DiCcDb db){
		int count = diCcDbService.delete(db.getId());
		return Integer.valueOf(count).toString();
	}

	/**
	 * @author wangdongxing
	 * @description 流程定义的分页展示
	 * @param request
	 * @return json数据
	 */
	@RequestMapping(value="/db_list", produces = "application/json")
	@ResponseBody
	public Object datasourcePage(HttpServletRequest request){
		PageWrapper<DiCcDb> pageWrapper=new PageWrapper<DiCcDb>(request);
		diCcDbService.find(pageWrapper.getPageBean(),"findByMap",pageWrapper.getConditionsMap());
		return pageWrapper.getResult();
	}

}

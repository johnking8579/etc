package com.jd.oa.di.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.dao.Page;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.di.dao.DiCcDbDao;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.service.ConnectionParam;
import com.jd.oa.di.service.ConnectionPool;
import com.jd.oa.di.service.DiCcDbService;

@Service("diCcDbService")
public class DiCcDbServiceImpl extends BaseServiceImpl<DiCcDb, String> implements
		DiCcDbService {

	@Autowired
	private DiCcDbDao diCcDbDao ;

	public DiCcDbDao getDao(){
		return diCcDbDao;
	}

	@Override
	public DiCcDb getByUnique(String dbName) {
		DiCcDb entity = new DiCcDb();
		entity.setDbName(dbName);
		List<DiCcDb> diCcDbList = this.find(entity);
		if (diCcDbList!=null && diCcDbList.size()>0){
			return diCcDbList.get(0);
		}
		// TODO Auto-generated method stub
		return null;
	}	
	
}

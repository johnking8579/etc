package com.jd.oa.di.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.JsonParseException;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.dao.Page;
import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.RelationalOperator;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.model.BootStrapDataGridColumnModel;
import com.jd.oa.common.model.EasyUIDataGirdColumnModel;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.di.dao.DiDatasourceConditionDao;
import com.jd.oa.di.dao.DiDatasourceFieldMapDao;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.model.DiDatasourceFieldMap;
import com.jd.oa.di.service.DiDatasourceConditionService;
import com.jd.oa.di.service.DiDatasourceFieldMapService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.form.execute.component.FormUIFactory;
import com.jd.oa.form.execute.component.UIComponentInterface;
import com.jd.oa.form.execute.component.model.ConfigComponentModel;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Service("diDatasouceFieldMapService")
public class DiDatasouceFieldMapServiceImpl extends BaseServiceImpl<DiDatasourceFieldMap,String> implements DiDatasourceFieldMapService {

	@Autowired
	private DiDatasourceFieldMapDao diDatasourceFieldMapDao;

	public DiDatasourceFieldMapDao getDiDatasourceConditionDao(){
		return diDatasourceFieldMapDao;
	}

	public void setDiDatasourceConditionDao(DiDatasourceFieldMapDao diDatasourceFieldMapDao) {
		this.diDatasourceFieldMapDao = diDatasourceFieldMapDao;
	}

	@Override
	public BaseDao<DiDatasourceFieldMap, String> getDao() {
		return diDatasourceFieldMapDao;
	}
	
	@Autowired
	public DiDatasourceService diDatasourceService;
	
	@Autowired
	public DiDatasourceConditionService diDatasourceConditionService;
	
	@Autowired
	public FormItemService formItemService;
	
	@Autowired
	public FormService formService;
	

	@Override
	public String save(String datas, String formId,String dataSourceId,String formItemId) {
		// TODO Auto-generated method stub
		List<Map<String,Object>> list = JsonUtils.parseJSON2List(datas);
		for(int i = 0 ; i < list.size() ; i++){
			Map<String,Object> map = list.get(i);
			if(map != null){
				String id = map.get("id") == null ? "" : map.get("id").toString();
				DiDatasourceFieldMap diDatasourceFieldMap = new DiDatasourceFieldMap();
				diDatasourceFieldMap.setFormId(formId);
				diDatasourceFieldMap.setFormItemId(formItemId);
				diDatasourceFieldMap.setDispaly(map.get("dispaly").toString());
				diDatasourceFieldMap.setTargetName(map.get("targetName").toString());
//				diDatasourceFieldMap.setFieldType(map.get("fieldType").toString());
				diDatasourceFieldMap.setName(map.get("name").toString());
				diDatasourceFieldMap.setFilter(Integer.parseInt(map.get("filter").toString()));
				diDatasourceFieldMap.setIshidden(Integer.parseInt(map.get("ishidden").toString()));
				diDatasourceFieldMap.setWidth(Integer.parseInt(map.get("width") == null ? "100" : map.get("width").toString()));
				diDatasourceFieldMap.setDatasourceId(dataSourceId);
				if(id != null && id.trim().length() > 0){
					diDatasourceFieldMap.setId(id);
					this.diDatasourceFieldMapDao.update(diDatasourceFieldMap);
				} else {
					this.diDatasourceFieldMapDao.insert(diDatasourceFieldMap);
				}
			}
		}
		return "";

	}
	/**
	 * 表格定义
	 * isSubSheet 是否子表字典
	 */
	@Override
	public String getDataGridDefinition(String dataSourceId, String formId,String formItemId,boolean isSubSheet) {
		// TODO Auto-generated method stub
		StringBuffer grid = new StringBuffer();
		grid.append("$('#dynDictionaryDataGrid').datagrid({\n");
		grid.append("           url:'/di/datasource/mapping_getBusinessData?dataSourceId=' + dataSourceId ,\n");
		grid.append("           rownumbers : true,\n");
		grid.append("           height : 220,\n");
		grid.append("           singleSelect : true,\n");
		grid.append("           columns:[").append(this.getDataGirdColumnsJson(dataSourceId, formId,formItemId)).append("],\n");
		grid.append("           pagination:true,\n");
		//wangdongxing add
		//--start--
		grid.append("           queryParams:getQueryParams('"+dataSourceId+"'),\n");
		//--end--
		//wangdongxing add

		grid.append("           fitColumns:true,\n");
		grid.append("           onDblClickRow:function(rowIndex, rowData){\n");
		grid.append("                callBack();\n");
		grid.append("                parent.$('#dictionaryDialog').dialog('close');\n");
		grid.append("           }\n");
		grid.append("       });\n");
		grid.append("var p = $('#dynDictionaryDataGrid').datagrid('getPager');\n");
		grid.append("$(p).pagination({\n");
		grid.append("        pageSize: 10,//每页显示的记录条数，默认为10 \n"); 
		grid.append("        pageList: [10,15,30],//可以设置每页记录条数的列表  \n");
		grid.append("        beforePageText: '第',//页数文本框前显示的汉字  \n");
		grid.append("        afterPageText: '页    共 {pages} 页',  \n");
		grid.append("        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录' \n");
		grid.append("});\n");
		if(isSubSheet){
			//子表回填
			grid.append(this.getMappingReturnCallBackJavascriptFunctionForSubSheet(dataSourceId, formItemId));
		} else {
			//主表回填
			grid.append(this.getMappingReturnCallBackJavascriptFunction(dataSourceId, formId,formItemId));
		}
		grid.append(this.getQueryExecuteJavascriptFunction(dataSourceId));
		return grid.toString();
	}
	/**
	 * 查询条件区域
	 * @param dataSourceId
	 * @param formId
	 * @return
	 */
	@Override
	public String getQueryConditionDefinition(HttpServletRequest request,String dataSourceId,String formId,String formItemId,String targetName,String likeConditionValue){
		StringBuffer condition = new StringBuffer();
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		RuntimeFormManager rfm = new RuntimeFormManager(contextPath);
		
		//获取tagetName所映射的name
		String conditionFieldName = "";
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setDatasourceId(dataSourceId);
		entity.setFormId(formId);
		entity.setFormItemId(formItemId);
		List<DiDatasourceFieldMap> entities = this.find(entity);
		for(int i = 0 ; i < entities.size() ; i++){
			DiDatasourceFieldMap map = entities.get(i);
			if(map != null){
				if(targetName.equals(map.getTargetName())){
					conditionFieldName = map.getName();
					break;
				}
			}
		}
		
		//查询
		DiDatasourceCondition diDatasourceCondition = new DiDatasourceCondition();
		diDatasourceCondition.setDatasourceId(dataSourceId);
		List<DiDatasourceCondition> diDatasourceConditions = diDatasourceConditionService.find(diDatasourceCondition);//通过dataSourceId获取DiDatasourceCondition的list对象
		if(diDatasourceConditions != null && diDatasourceConditions.size() > 0){
			condition.append("<fieldset style=\"border:1px solid #DDDDDD;-moz-border-radius:8px;border-radius:10px;\">\n");
			condition.append("<legend style=\"font-family: 'Microsoft YaHei';font-size: 14px; line-height: 20px; margin-bottom: 0px; padding:0;width: 40px;\">查询</legend>\n");
			condition.append("<table width=\"100%\" border=\"0\">\n");
			String defaultValue = "";
			for(int i = 0 ,j = 0; i < diDatasourceConditions.size() ; i++){
				diDatasourceCondition = diDatasourceConditions.get(i);
				if(diDatasourceCondition != null){
					if( j % 2 == 0){
						condition.append("  <tr>\n");
					}
					condition.append("    <td align='right'>").append(diDatasourceCondition.getFieldTitle()).append(":</td>\n");
					FormBusinessField metaDataMapModel = new FormBusinessField();
					metaDataMapModel.setInputType(diDatasourceCondition.getUiType());
					metaDataMapModel.setDataType(diDatasourceCondition.getFieldType());
					metaDataMapModel.setFieldName(diDatasourceCondition.getFieldName());
					
					if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATE))){
	    				metaDataMapModel.setInputType("日期");
	    				metaDataMapModel.setInputTypeConfig("");
	    			} else if(metaDataMapModel.getDataType().equals(String.valueOf(DataType.DATETIME))){
	    				metaDataMapModel.setInputType("日期时间");
	    				metaDataMapModel.setInputTypeConfig("");
	    			} else {
	    				metaDataMapModel.setInputType("单行");
	    				metaDataMapModel.setInputTypeConfig("");
	    			}
	    			
	    			if(metaDataMapModel.getInputType() == null || metaDataMapModel.getInputType().trim().length() == 0){
	    				metaDataMapModel.setInputType("单行");
	    				metaDataMapModel.setInputTypeConfig("");
	    			} 
	    			//UI类型
	    			UIComponentInterface uiComponent = FormUIFactory.getInstance(contextPath).getUIInstance(metaDataMapModel, "");
	    			Map<String, ConfigComponentModel> componentList = FormUIFactory.getInstance(contextPath).getComponentList();
	    			ConfigComponentModel configComponentModel = (ConfigComponentModel) componentList.get(metaDataMapModel.getInputType());
	    			//加载从表单传递过来的查询条件
					if(conditionFieldName.equals(diDatasourceCondition.getFieldName())){
						uiComponent.setValue(likeConditionValue);
					}
	    			if(uiComponent.getValue() == null || uiComponent.getValue().trim().length() == 0 ){
	    				//默认值
						defaultValue = diDatasourceCondition.getDefaultValue();
	    				if(defaultValue != null && defaultValue.trim().length() > 0){
//	    					StringUtils util = new StringUtils(defaultValue);
	    					uiComponent.setValue(rfm.convertMacrosValue(defaultValue));
//	    					defaultValue = rfm.convertMacrosValue(defaultValue);
	    				}
	    			}
    				if (configComponentModel == null) {
        				condition.append("    <td align='left'>").append("<input id='").append(diDatasourceCondition.getFieldName()).append("' type='text' name='")
								 .append(diDatasourceCondition.getFieldName()).append("' value='")
								 .append(defaultValue).append("'>")
								 .append("</td>\n");
        			} else {
						condition.append("    <td align='left'>").append(uiComponent.getModifyHtmlDefine(null))
								 .append("</td>\n");
        			}
					if( j % 2 == 1){
						condition.append("  </tr>\n");
					}
					j++;
				}
			}
			condition.append("  <tr>\n");
			condition.append("    <td colspan=\"4\" align='right'><button class='active1' onclick=\"queryExecute('").append(dataSourceId).append("');return false;\">查询</button>&nbsp;</td>\n");
			condition.append("    </tr>\n");
			condition.append("	</table>\n");
			condition.append("</fieldset>\n");
			//加载自动执行"查询"
			if(likeConditionValue != null && likeConditionValue.trim().length() > 0 && conditionFieldName.trim().length() > 0){
//				condition.append("<script>$(function () { setTimeout(function(){queryExecute('").append(dataSourceId).append("'); },3000)})</script>");
			}
		}
		return condition.toString();
	}
	/**
	 * 获取数据字典的列定义
	 * @param dataSourceId
	 * @param formId
	 * @return
	 */
	private String getDataGirdColumnsJson(String dataSourceId,String formId,String formItemId){
		List<EasyUIDataGirdColumnModel> dataGridColumnModels = new ArrayList<EasyUIDataGirdColumnModel>();
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setDatasourceId(dataSourceId);
		entity.setFormId(formId);
		entity.setFormItemId(formItemId);
		List<DiDatasourceFieldMap> entities = this.find(entity);
		for(int i = 0 ; i < entities.size() ; i++){
			DiDatasourceFieldMap map = entities.get(i);
			if(map != null){
				EasyUIDataGirdColumnModel columnsModel = new EasyUIDataGirdColumnModel();
				columnsModel.setField(map.getName());
				columnsModel.setTitle(map.getDispaly());
				columnsModel.setWidth(map.getWidth());
				columnsModel.setHidden(map.getIshidden() == 1);//是否隐藏，1：隐藏，0:显示
				dataGridColumnModels.add(columnsModel);
			}
		}
		return JsonUtils.toJsonByGoogle(dataGridColumnModels);
	}
	/**
	 * 通过数据源获取分页数据
	 */
	@Override
	public PageJDBC getPageJsonData(String dataSourceId,String rows,String page,String condition) {
		// TODO Auto-generated method stub
		DiDatasource diDataSource =  diDatasourceService.get(dataSourceId);
		DiDatasourceCondition diDatasourceCondition = new DiDatasourceCondition();
		diDatasourceCondition.setDatasourceId(dataSourceId);
		List<DiDatasourceCondition> diDatasourceConditions = diDatasourceConditionService.find(diDatasourceCondition);
		String sql = diDataSource.getExecSql();
		StringBuffer whereCondition = new StringBuffer();
		if(condition != null && condition.trim().length() > 0){
			ObjectMapper objectMapper = new ObjectMapper();
			try {    
		        List<Object> list = objectMapper.readValue(condition, List.class);
		        for(int i = 0 ,j = 0; i < diDatasourceConditions.size() ; i++){
		        	DiDatasourceCondition entity = diDatasourceConditions.get(i);
					if(entity != null){
						for (Object o : list) {    
				            LinkedHashMap map = (LinkedHashMap) o;    
				            if(map != null && map.get(entity.getFieldName()) != null && map.get(entity.getFieldName()).toString().trim().length() > 0){
				            	if(whereCondition != null && whereCondition.toString().trim().length() > 0){
			            			whereCondition.append(" and ");
			            		}
				            	if(entity.getFieldType().toUpperCase().equals(String.valueOf(DataType.STRING)) || 
				            			entity.getFieldType().toUpperCase().equals(String.valueOf(DataType.DATE)) || 
				            					entity.getFieldType().toUpperCase().equals(String.valueOf(DataType.DATETIME))){
				            		if(entity.getCompareType().toLowerCase().equals("like")){
				            			whereCondition.append("temp." + entity.getFieldName()).append(" ").append(entity.getCompareType()).append(" '%").append(map.get(entity.getFieldName())).append("%' ");
				            		} else {
				            			whereCondition.append("temp." + entity.getFieldName()).append(" ").append(entity.getCompareType()).append(" '").append(map.get(entity.getFieldName())).append("' ");
				            		}
				            		System.out.println(whereCondition.toString());
				            	} else if(entity.getFieldType().toUpperCase().equals(String.valueOf(DataType.NUMBER))){
				            		whereCondition.append("temp." + entity.getFieldName()).append(entity.getCompareType()).append(map.get(entity.getFieldName())).append(" ");
				            	} 
				            }
				        }    
					}
		        }
		        if(whereCondition != null && whereCondition.toString().trim().length() > 0){
		        	sql = "select * from (" + sql + ")  temp where " + whereCondition.toString();
		        }
		    } catch (JsonParseException e) {    
		        e.printStackTrace();    
		    } catch (JsonMappingException e) {    
		        e.printStackTrace();    
		    } catch (IOException e) {    
		        e.printStackTrace();    
		    }    

		}
		if(whereCondition.toString().trim().length() > 0){
			return diDatasourceService.getDatasourceResultListForPage(diDataSource.getDataAdapter(),  Integer.parseInt(page),Integer.parseInt(rows), sql);
		} else {
			return new PageJDBC();
		}
	}

	/**
	 * 根据fieldName快速检索
	 */
	public PageJDBC getQuickSearchJsonData(String dataSourceId,String formId,String formItemId,String fieldName,String condition) {
		DiDatasource diDataSource =  diDatasourceService.get(dataSourceId);
		String sql = diDataSource.getExecSql();
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setDatasourceId(dataSourceId);
		entity.setFormId(formId);
		entity.setFormItemId(formItemId);
		entity.setTargetName(fieldName);
		//正常情况会取回一条记录
		List<DiDatasourceFieldMap> entities = this.find(entity);
		if(condition != null && condition.trim().length() > 0 && entities!=null && entities.size() > 0){
			StringBuffer whereCondition = new StringBuffer();
			DiDatasourceFieldMap map = entities.get(0);
			if(map != null){
				if(map.getFilter() == 1){
					whereCondition.append("temp." + map.getName()).append(" like '%").append(condition).append("%' ");
				}else{
					whereCondition.append("temp." + map.getName()).append(" = '").append(condition).append("' ");
				}
			}
			if(whereCondition != null && whereCondition.toString().trim().length() > 0){
				sql = "select * from (" + sql + ")  temp where " + whereCondition.toString();
			}
		}
		return  diDatasourceService.getDatasourceResultListForPage(diDataSource.getDataAdapter(), 1 , 10, sql);
	}
	/**
	 * 回填javascript拼装(子表)
	 * @param dataSourceId
	 * @param formItemId
	 * @return
	 */
	private String getMappingReturnCallBackJavascriptFunctionForSubSheet(String dataSourceId,String formItemId){
		StringBuffer returnJs = new StringBuffer();
		returnJs.append("function callBack(){\n");
        returnJs.append("    var data = $('#dynDictionaryDataGrid').datagrid('getSelected');\n");
		FormItem formItem = formItemService.get(formItemId);
		if(formItem != null){
			Form form = formService.get(formItem.getFormId());
			if(form != null){
				returnJs.append("try{\n");
				returnJs.append(" var row = parent.$('#").append(form.getFormCode()).append("').datagrid('getSelected');\n");
				returnJs.append(" var selectedIndex = parent.$('#").append(form.getFormCode()).append("').datagrid('getRowIndex',row);\n");
				DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
				entity.setDatasourceId(dataSourceId);
				entity.setFormItemId(formItemId);
				List<DiDatasourceFieldMap> entities = this.find(entity);
				if(entities != null && entities.size() > 0){
					for(int i = 0 ; i < entities.size() ; i++){
						DiDatasourceFieldMap map = entities.get(i);
						if(map != null){
							returnJs.append(" var ed_").append(i).append(" = parent.$('#").append(form.getFormCode()).append("').datagrid('getEditor', {index:selectedIndex,field:'").append(map.getTargetName()).append("'});\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'dictionary'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).searchbox('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'textarea'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).textarea('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'numberbox'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).numberbox('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'datetimebox'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).datetimebox('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'datebox'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).datebox('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'combobox'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).combobox('setValue', data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
							returnJs.append(" if(ed_").append(i).append(".type == 'text'){\n");
							returnJs.append("     parent.$(ed_").append(i).append(".target).val(data.").append(map.getName()).append(");\n");
							returnJs.append(" }\n");
						}
					}
				}
				returnJs.append("}catch(e){}\n");
			}
		}
		returnJs.append("   try{\n");
		returnJs.append("        parent.extendDictionarySubsheetCallBack(data);\n");
		returnJs.append("   }catch(e){}\n");
		returnJs.append("}\n");
		System.out.println(returnJs.toString());
		return returnJs.toString();
	}
	/**
	 * 回填javascript拼装（主表）
	 * @param dataSourceId
	 * @param formId
	 * @return
	 */
	private String getMappingReturnCallBackJavascriptFunction(String dataSourceId,String formId,String formItemId){
		StringBuffer returnJs = new StringBuffer();
		returnJs.append("function callBack(){\n");
		returnJs.append("    var data = $('#dynDictionaryDataGrid').datagrid('getSelected');\n");
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setDatasourceId(dataSourceId);
		entity.setFormId(formId);
		entity.setFormItemId(formItemId);
		List<DiDatasourceFieldMap> entities = this.find(entity);
		for(int i = 0 ; i < entities.size() ; i++){
			DiDatasourceFieldMap map = entities.get(i);
			if(map != null){
				returnJs.append( "   try{\n");
				returnJs.append("         parent.document.getElementById('").append(map.getTargetName()).append("').value = data.").append(map.getName()).append(";\n");
				returnJs.append("   }catch(e){}\n");
				map.getTargetName();
			}
		}
		returnJs.append("   try{\n");
		returnJs.append("        parent.extendCallBack(data);\n");
		returnJs.append("   }catch(e){}\n");
		returnJs.append("}\n");
		return returnJs.toString();
	}
	/**
	 * 查询条件javascript function
	 * @param dataSourceId
	 * @return
	 */
	private String getQueryExecuteJavascriptFunction(String dataSourceId){
		StringBuffer queryExecute = new StringBuffer();
		queryExecute.append("function queryExecute(dataSourceId){\n");
		queryExecute.append("   $('#dynDictionaryDataGrid').datagrid('load',getQueryParams('"+dataSourceId+"')); \n");
		queryExecute.append("}\n");

		StringBuffer queryParamsStringBuffer = new StringBuffer();
		queryParamsStringBuffer.append("function getQueryParams(dataSourceId){\n");
		DiDatasourceCondition diDatasourceCondition = new DiDatasourceCondition();
		diDatasourceCondition.setDatasourceId(dataSourceId);
		List<DiDatasourceCondition> diDatasourceConditions = diDatasourceConditionService.find(diDatasourceCondition);//通过dataSourceId获取DiDatasourceCondition的list对象
		queryParamsStringBuffer.append("    var condition = new Array();\n");
        for(int i = 0; i < diDatasourceConditions.size() ; i++){
        	DiDatasourceCondition entity = diDatasourceConditions.get(i);
			if(entity != null){
				queryParamsStringBuffer.append("    condition.push({'").append(entity.getFieldName()).append("':").append("$('#").append(entity.getFieldName()).append("').val()").append("});\n");
				
			}
        }
        //重新加载datagrid的数据
		queryParamsStringBuffer.append("    var data={'condition':JSON.stringify(condition)};\n");
		queryParamsStringBuffer.append("    return data;\n");
		queryParamsStringBuffer.append("}\n");
		return queryExecute.toString()+"\n"+queryParamsStringBuffer.toString();
	}
}

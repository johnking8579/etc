package com.jd.oa.di.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.di.model.DiDatasourceCondition;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
public interface DiDatasourceConditionService extends BaseService<DiDatasourceCondition,String> {
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author yujiahe 部门:综合职能研发部 
 * @date 14-5-21
 *	@email : yujiahe@jd.com
 */
package com.jd.oa.di.controller;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.model.DiTransforLog;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.di.service.DiTransforLogService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/di/diLog")
public class DiLogController {

    @Autowired
  private   DiTransforLogService diTransforLogService;
    @Autowired
    private DiDatasourceService diDatasourceService;
	@Autowired
	private JssService jssService;

    /**
     * 服务接口日志首页
     * @author yujiahe
     * @return
     */
    @RequestMapping(value = "")
    public String index() {
        return "di/diLog/diLog_index";
    }

    /**
     * 服务接口  分页
     * @param request
     * @param diTransforLog
     * @return
     */
    @RequestMapping(value="/diLogPage",method= RequestMethod.POST, produces = "application/json")
    @ResponseBody

    public Object diLogPage(HttpServletRequest request,DiTransforLog diTransforLog){
        PageWrapper<DiTransforLog> pageWrapper=new PageWrapper<DiTransforLog>(request);
        if(StringUtils.isNotEmpty(diTransforLog.getTransFlag())){
            pageWrapper.addSearch("transFlag", diTransforLog.getTransFlag());
        }
        if(StringUtils.isNotEmpty(diTransforLog.getInParameter())){
            pageWrapper.addSearch("inParameter", diTransforLog.getInParameter());
        }
        diTransforLogService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
        return pageWrapper.getResult();
    }

    /**
     * 服务接口查询详情
     * @param id
     * @return
     */
    @RequestMapping(value = "/diLogDetail", method = RequestMethod.GET)
    public ModelAndView interfaceUpdate(String id) {
        ModelAndView mav = new ModelAndView("di/diLog/diLog_view");
        if (StringUtils.isNotEmpty(id)) {
            DiTransforLog diTransforLog = diTransforLogService.get(id);
            if (null != diTransforLog) {
                mav.addObject("diTransforLog", diTransforLog);
            }
        }
        return mav;
    }

    /**
     * 服务接口再次传输
     * @param id
     * @return
     */
    @RequestMapping(value = "/diTransfer", method = RequestMethod.GET)
    @ResponseBody
    Map<String,Object>  diTransfer(String id) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if (StringUtils.isNotEmpty(id)) {
            DiTransforLog diTransforLog = diTransforLogService.get(id);
			Map<String,Object> inParameter;
			try {
				//根据真实数据类型从JSS取回，防止类型转换失败
				File file = jssService.getFile(SystemConstant.BUCKET, id + ".OBJ");
				inParameter = (Map<String,Object>) FileUtil.byte2Object(FileUtil.Base64String2Bytes(FileUtils.readFileToString(file)));
				if(inParameter == null) throw new RuntimeException("从JSS获取接口参数为空，尝试从JSON获取数据.");
			} catch (Exception e) {
				//真实数据取回失败后，用原有JSON数据重试
				inParameter=StringUtils.isNotEmpty(diTransforLog.getInParameter())?DataConvertion.parserToMap(diTransforLog.getInParameter()):new HashMap<String, Object>();
			}
            diDatasourceService.getDatasourceResultList(diTransforLog.getInterfaceId(),0,0,inParameter,"1");
            diTransforLog.setTransFlag("2");
            diTransforLogService.update(diTransforLog);
            resultMap.put("operator", true);
            resultMap.put("message","传输成功");
        }else{
            resultMap.put("operator", true);
            resultMap.put("message","服务ID为空");
        }
        return resultMap;
    }
}

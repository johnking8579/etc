package com.jd.oa.di.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.di.model.DiDatasource;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
public interface DiDatasourceDao extends BaseDao<DiDatasource,String>{

}

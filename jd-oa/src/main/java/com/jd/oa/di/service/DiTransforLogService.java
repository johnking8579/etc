package com.jd.oa.di.service;

import com.jd.oa.common.service.BaseService;
import com.jd.oa.di.model.DiTransforLog;

public interface DiTransforLogService extends BaseService<DiTransforLog, String> {
	
}

/**
 * 
 */
package com.jd.oa.di.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.common.util.StringUtils;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.service.DiCcInterfaceService;

/**
 * @author yujiahe
 * @date 2014-2-20
 * 
 */
@Controller
@RequestMapping("/di/cc")
public class DiCcInterfaceController {
	@Autowired
	private DiCcInterfaceService diCcInterfaceService;

	/**
	 * author yujiahe description 接口首页
	 * 
	 * @return
	 */

	@RequestMapping(value = "/if_index")
	public String index() {
		return "di/cc/if_index";
	}

	/**
	 * author yujiahe description 接口部分分页显示
	 * 
	 * @param request
	 * @return
	 */

	@RequestMapping(value = "/ifPage", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object interfacePage(HttpServletRequest request) {
		String interfaceName = request.getParameter("interfaceName");
		PageWrapper<DiCcInterface> pageWrapper = new PageWrapper<DiCcInterface>(
				request);
		if(StringUtils.isNotEmpty(interfaceName)){
			pageWrapper.addSearch("interfaceName",interfaceName);
		}
		diCcInterfaceService.find(pageWrapper.getPageBean(), "findByMap",
				pageWrapper.getConditionsMap());
		return pageWrapper.getResult();
	}

	/**
	 * author yujiahe description 接口新增视图
	 * 
	 */
	@RequestMapping(value = "/if_add")
	public String datasourceAdd() {
		return "di/cc/if_add";
	}

	/**
	 * author yujiahe description 接口数据提交
	 * 
	 */
	@RequestMapping(value = "/if_addSave", method = RequestMethod.POST)
	@ResponseBody
	public Object interfaceAddSave(DiCcInterface diCcInterface) {

		DiCcInterface diCcInterfaceFQ = new DiCcInterface();
		diCcInterfaceFQ.setInterfaceName(diCcInterface.getInterfaceName());
		int num = diCcInterfaceService.find(diCcInterfaceFQ).size();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//修改
		if (diCcInterface.getId() != null && !diCcInterface.getId().equals("")) {
			if (num > 1
					|| ((num == 1) && (!(diCcInterfaceService.find(
							diCcInterfaceFQ).get(0).getId())
							.equals(diCcInterface.getId())))) {//名称重复情况
				resultMap.put("operator", "success");
				resultMap.put("message", "请更换接口名称，该名称已存在");
			} else {//名称不重复情况
				diCcInterfaceService.update(diCcInterface);
				resultMap.put("operator", "success");
				resultMap.put("message", "接口修改成功");
			}

		} else {//新增
			diCcInterface.setId(null);
			if (diCcInterfaceService.find(diCcInterfaceFQ).size() > 0) {//名称重复情况
				resultMap.put("operator", "success");
				resultMap.put("message", "请更换接口名称，该名称已存在");
			} else {//名称不重复情况
				diCcInterfaceService.insert(diCcInterface);
				resultMap.put("operator", "success");
				resultMap.put("message", "接口新增成功");
			}

		}

		return resultMap;
	}

	/**
	 * author yujiahe description 跳转到接口修改页面 return
	 */
	@RequestMapping(value = "/if_update", method = RequestMethod.GET)
	public ModelAndView interfaceUpdate(String id) {
		ModelAndView mav = new ModelAndView("di/cc/if_add");
		if (null != id && !("").equals(id)) {
			DiCcInterface diCcInterface = diCcInterfaceService.get(id);
			if (null != diCcInterface) {
				mav.addObject("interface", diCcInterface);
			}
		}
		return mav;
	}

	/**
	 * @Description:删除接口支持批量
	 * @author yujiahe
	 */
	@RequestMapping(value = "/if_delete")
	@ResponseBody
	public String interfaceDelete(String ids) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			diCcInterfaceService.mutidelete(ids);// 批量删除

			map.put("operator", true);
			map.put("message", "删除成功");
		} catch (Exception e) {
			map.put("operator", false);
			map.put("message", e.getMessage());
		}
		return JSONObject.fromObject(map).toString();
	}
}

package com.jd.oa.di.service.impl;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.di.dao.DiDatasourceConditionDao;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.service.DiDatasourceConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Service("diDatasourceConditionService")
public class DiDatasouceConditionServiceImpl extends BaseServiceImpl<DiDatasourceCondition,String> implements DiDatasourceConditionService {

	@Autowired
	private DiDatasourceConditionDao diDatasourceConditionDao;

	public DiDatasourceConditionDao getDiDatasourceConditionDao() {
		return diDatasourceConditionDao;
	}

	public void setDiDatasourceConditionDao(DiDatasourceConditionDao diDatasourceConditionDao) {
		this.diDatasourceConditionDao = diDatasourceConditionDao;
	}

	@Override
	public BaseDao<DiDatasourceCondition, String> getDao() {
		return diDatasourceConditionDao;
	}
}

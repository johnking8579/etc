/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-5-23
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.di.service;

import com.jd.common.util.StringUtils;
import com.jd.oa.common.webservice.DataConvertion;
import org.dom4j.Element;

import java.lang.reflect.Method;
import java.util.*;

public class DiInterfaceParameter {

	private String key;
	private String type;
	private Object value;


	public DiInterfaceParameter(Element element) {
		this.key = element.getName();
		this.type = element.attributeValue("type");
		this.value = convertElement(element);
	}

	/**
	 * 类型转化过程
	 * @param element
	 * @return
	 */
	private Object convertElement(Element element){
		String type = element.attributeValue("type");
		if(StringUtils.isEmpty(type)) type="string";
		if(type.equals("int")){
			return parseInt(element);
		}else if(type.equals("string")){
			return parseString(element);
		}else if(type.equals("float")){
			return parseFloat(element);
		}else if(type.equals("double")){
			return parseDouble(element);
		}else if(type.equals("long")){
			return parseLong(element);
		}else if(type.equals("date")){
			return parseDate(element);
		}else if(type.equals("map")){
			return parseMap(element);
		}else if(type.equals("list")){
			return parseList(element);
		}else if(type.equals("pojo")){
			return parsePojo(element);
		}else{
			return element.getTextTrim();
		}
	}

	/**
	 * 解析map类型节点
	 * @param element
	 * @return
	 */
	private Map parseMap(Element element){
		Map<String,Object> map = new LinkedHashMap<String, Object>();
		Iterator iterator = element.elementIterator();
		while (iterator.hasNext()){
			Element e = (Element) iterator.next();
			map.put(e.getName(),convertElement(e));
		}
		return map;
	}

	/**
	 * 解析list类型节点
	 * @param element
	 * @return
	 */
	private List parseList(Element element){
		List<Object> list = new ArrayList<Object>();
		Iterator iterator = element.elementIterator();
		while (iterator.hasNext()){
			Element e = (Element) iterator.next();
			list.add(convertElement(e));
		}
		return list;
	}

	/**
	 * 接卸pojo类型节点
	 * @param element
	 * @return
	 */
	private Object parsePojo(Element element){
		String clazzSting = element.attributeValue("class");
		Object o = null;
		try {
			Class clazz = Class.forName(clazzSting);
			o = clazz.newInstance();
			Method[] methods = clazz.getDeclaredMethods();
			Iterator iterator = element.elementIterator();
			while (iterator.hasNext()){
				Element el = (Element) iterator.next();
				String fieldName = el.getName();
					//遍历方法取出 属性的 get/is方法,可以兼容非JavaBean规范的属性读写方法
					for(Method method:methods){
						if(method.getName().startsWith("set")){
							String methodName=method.getName().replace("set","");
							if(methodName.toLowerCase().equals(fieldName.toLowerCase())){
								method.invoke(o,convertElement(el));
								break;
							}
						}
					}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}

	/**
	 * 解析int类型节点
	 * @param element
	 * @return
	 */
	private int parseInt(Element element){
		if(element.isTextOnly()){
			return DataConvertion.object2Int(element.getTextTrim());
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is int ,but it has children nodes! ") ;
		}
	}

	/**
	 * 解析float类型节点
	 * @param element
	 * @return
	 */
	private float parseFloat(Element element){
		if(element.isTextOnly()){
			return DataConvertion.object2Float(element.getTextTrim());
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is float ,but it has children nodes! ") ;
		}
	}

	/**
	 * 解析float类型节点
	 * @param element
	 * @return
	 */
	private double parseDouble(Element element){
		if(element.isTextOnly()){
			return DataConvertion.object2Double(element.getTextTrim());
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is double ,but it has children nodes! ") ;
		}
	}

	/**
	 * 解析long类型节点
	 * @param element
	 * @return
	 */
	private long parseLong(Element element){
		if(element.isTextOnly()){
			return DataConvertion.object2Long(element.getTextTrim());
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is long ,but it has children nodes! ") ;
		}
	}

	/**
	 * 接卸String类型节点
	 * @param element
	 * @return
	 */
	private String parseString(Element element){
		if(element.isTextOnly()){
			return DataConvertion.object2String(element.getTextTrim());
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is string ,but it has children nodes! ") ;
		}
	}

	/**
	 * 解析日期类型节点
	 * @param element
	 * @return
	 */
	private Date parseDate(Element element){
		if(element.isTextOnly()){
			String dateFormat = element.attributeValue("dateFormat");
			if(StringUtils.isEmpty(dateFormat)) dateFormat = "yyyy-MM-dd HH:mm:ss";
			return DataConvertion.object2Date(element.getTextTrim(), dateFormat);
		}else{
			throw new IllegalArgumentException("the type of node["+element.getName()+"]  is date ,but it has children nodes! ") ;
		}
	}

	public String getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}

	public String getType() {
		return type;
	}
}

package com.jd.oa.di.service.impl;

import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.di.service.HelloService;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.ws.rs.HeaderParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 服务实现类
 * @author wds
 *
 */
@WebService(endpointInterface="com.jd.oa.di.service.HelloService",serviceName="helloWorldService888888888",targetNamespace="http://service.di.oa.jd.com")
public class HelloServiceImpl implements HelloService {
	@Override
	public String sayHello(String param1,String param2) {
//		List<String> list=new ArrayList<String>();
//		list.add("hello1");
//		list.add("hello2");
//		list.add("hello3");
//		return JsonUtils.toJsonByGoogle(list);

		List<Map<String,Object>> lists=new ArrayList<Map<String, Object>>();
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("key1","value1");
		map.put("key2","value2");
		map.put("key3","value3");

		Map<String,Object> map2=new HashMap<String, Object>();
		map2.put("key1","value1");
		map2.put("key2","value2");
		map2.put("key3","value3");

		lists.add(map);
		lists.add(map2);
		// ["hello1","hello2","hello3","hello4"]
		return "hello";
//		return "[\"hello1\",\"hello2\",\"hello3\",\"hello4\"]";
//		return JsonUtils.toJsonByGoogle(lists);
//		return "<table><tbody><t><key1>value1</key1><key2>value2</key2><key3>value3</key3></t></tbody></table>";
	}

}
package com.jd.oa.di.controller;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.model.DiDatasourceFieldMap;
import com.jd.oa.di.service.DatasourceUtils;
import com.jd.oa.di.service.DiCcDbService;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceFieldMapService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Controller
@RequestMapping("/di/datasource")
public class DiDatasourceFieldMapController {

	@Autowired
	private DiDatasourceService diDatasourceService;

	@Autowired
	private DiCcDbService diCcDbService;
	
	@Autowired
	private FormItemService formItemService;

	@Autowired
	private DiCcInterfaceService diCcInterfaceService;
	
	@Autowired
	private DiDatasourceFieldMapService diDatasourceFieldMapService;

	@RequestMapping(value="/ds_mapping",method= RequestMethod.GET)
	public ModelAndView datasourcePage(String dataSourceId,String formId,String fieldId){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("di/datasource/ds_mapping");
		mav.addObject("dataSourceId", dataSourceId);
		mav.addObject("fieldId", fieldId);
		mav.addObject("formId", formId);
		return mav;
	}
	@RequestMapping(value="/mapping_index",method= RequestMethod.GET)
	public ModelAndView dataSourceMapping(String dataSourceId,String formId,String fieldId){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("di/datasource/ds_mapping_index");
		mav.addObject("dataSourceId", dataSourceId);
		mav.addObject("formId", formId);
		mav.addObject("fieldId", fieldId);
		return mav;
	}
	@RequestMapping(value="/mapping_getJsonData",method= RequestMethod.POST)
	@ResponseBody
	public List<DiDatasourceFieldMap> getMappingJsonData(String dataSourceId,String formId,String fieldId){
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setDatasourceId(dataSourceId);
		entity.setFormId(formId);
		entity.setFormItemId(fieldId);
		return diDatasourceFieldMapService.find(entity);
	}
	
	/**
	 * 根据formId获取表单项
	 * @param formId
	 * @return
	 */
	@RequestMapping(value="/mapping_getFormItem",method= RequestMethod.GET)
	@ResponseBody
	public List<FormItem> getFormItem(String formId){
		return formItemService.getFormItemByFormId(formId);
	}

	/**
	 * author wangdongxing
	 * description 数据源添加试图
	 * @return json数据
	 */
	@RequestMapping(value="/mapping_save",method= RequestMethod.POST)
	@ResponseBody
	public String datasourceMappingAdd(String formId,String datas,String dataSourceId,String formItemId){
		return diDatasourceFieldMapService.save(datas, formId,dataSourceId,formItemId);
	}

	/**
	 * author 
	 * description 删除
	 */
	@RequestMapping(value="/mapping_remove",method = RequestMethod.POST)
	@ResponseBody
	public String datasourceAddSave(String id){
		DiDatasourceFieldMap entity = new DiDatasourceFieldMap();
		entity.setId(id);
		entity = diDatasourceFieldMapService.find(entity).get(0);
		diDatasourceFieldMapService.delete(id, true);
		return "success";
	}
	/**
	 * description 表单调用，通过数据源查询数据
	 */
	@RequestMapping(value="/mapping_getBusiness",method = RequestMethod.GET)
	public ModelAndView datasourceGetBusiness(HttpServletRequest request,String dataSourceId,String formId,String formItemId,boolean isSubSheet,String targetName,String likeConditionValue){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("di/datasource/ds_business");
		mav.addObject("dataSourceId", dataSourceId);
		mav.addObject("javascript", diDatasourceFieldMapService.getDataGridDefinition(dataSourceId,formId,formItemId,isSubSheet));
		mav.addObject("formId", formId);
		mav.addObject("queryCondition", diDatasourceFieldMapService.getQueryConditionDefinition(request,dataSourceId,formId,formItemId,targetName,likeConditionValue));
		return mav;
	}
	/**
	 * description 表单调用，通过数据源查询数据
	 */
	@RequestMapping(value="/mapping_getBusinessData",method = RequestMethod.POST)
	@ResponseBody
	public PageJDBC datasourceGetBusinessData(HttpServletRequest request,String dataSourceId){
		String rows = request.getParameter("rows");
		String page = request.getParameter("page");
		String condition = request.getParameter("condition");
		return this.diDatasourceFieldMapService.getPageJsonData(dataSourceId,rows,page,condition);
	}
	/**
	 * 
	 * @param dataSourceId
	 * @param formId
	 * @param fieldId
	 * @return
	 */
	@RequestMapping(value="/mapping_getQuickSearchData",method= RequestMethod.POST)
	@ResponseBody
	public PageJDBC getQuickSearchData(String dataSourceId,String formId,String formItemId,String fieldName,String condition){
		return this.diDatasourceFieldMapService.getQuickSearchJsonData(dataSourceId,formId,formItemId,fieldName,condition);
	}
}

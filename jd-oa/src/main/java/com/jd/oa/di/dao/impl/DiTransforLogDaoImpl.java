package com.jd.oa.di.dao.impl;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiTransforLogDao;
import com.jd.oa.di.model.DiTransforLog;


@Repository("diTransforLogDao")
public class DiTransforLogDaoImpl extends MyBatisDaoImpl<DiTransforLog, String> implements DiTransforLogDao {

}

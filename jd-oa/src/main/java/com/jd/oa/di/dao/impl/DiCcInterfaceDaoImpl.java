/**
 * 
 */
package com.jd.oa.di.dao.impl;

import org.springframework.stereotype.Repository;

import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.di.dao.DiCcInterfaceDao;
import com.jd.oa.di.model.DiCcInterface;


/**
 * @author yujiahe
 * @date 2014-2-20
 *
 */
@Repository("diCcInterfaceDao")
public class DiCcInterfaceDaoImpl extends MyBatisDaoImpl<DiCcInterface,String> implements DiCcInterfaceDao {

}

package com.jd.oa.di.controller;

import com.google.gson.reflect.TypeToken;
import com.jd.common.util.StringUtils;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.di.model.DiCcDb;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcDbService;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Controller
@RequestMapping("/di/datasource")
public class DiDatasourceController {

	@Autowired
	private DiDatasourceService diDatasourceService;

	@Autowired
	private DiCcDbService diCcDbService;

	@Autowired
	private DiCcInterfaceService diCcInterfaceService;

	@RequestMapping(value = "")
	public String index(){
		return "di/datasource/ds_index";
	}

	/**
	 * author wangdongxing
	 * description 数据源的分页展示
	 * param request
	 * @return json数据
	 */
	@RequestMapping(value="/datasourcePage",method= RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object datasourcePage(HttpServletRequest request){
		String dsName = request.getParameter("dsName");
		PageWrapper<DiDatasource> pageWrapper=new PageWrapper<DiDatasource>(request);
		if(StringUtils.isNotEmpty(dsName)){
			pageWrapper.addSearch("name",dsName);
		}
		diDatasourceService.find(pageWrapper.getPageBean(), "findByMap", pageWrapper.getConditionsMap());
		return pageWrapper.getResult();
	}

	/**
	 * author wangdongxing
	 * description 数据源添加试图
	 * @return json数据
	 */
	@RequestMapping(value="/datasource_add")
	public ModelAndView datasourceAdd(String id){
		ModelAndView mav=new ModelAndView();
		mav.setViewName("di/datasource/ds_add");
		if(null != id && !("").equals(id)){
			DiDatasource datasource = diDatasourceService.get(id);
			if(null != datasource){
				mav.addObject("datasource",datasource);
			}
		}
		//取回db数据源连接
		List<DiCcDb> ccDbList=diCcDbService.find(new DiCcDb());
		mav.addObject("ccDbList", ccDbList);
		//取回接口数据源连接
		List<DiCcInterface> ccInterfaceList=diCcInterfaceService.find(new DiCcInterface());
		mav.addObject("ccInterfaceList", ccInterfaceList);
		return mav;
	}

	/**
	 * author wangdongxing
	 * description 添加数据源提交
	 * @return json数据
	 */
	@RequestMapping(value="/datasource_addSave",method = RequestMethod.POST)
	@ResponseBody
	public Object datasourceAddSave(String id,String datasourceName,String dataAdapterType,String dataAdapter,String execSql){
		DiDatasource diDatasource = null;
		if(id != null && !id.equals("")){
			diDatasource = diDatasourceService.get(id);
		}
		if(diDatasource == null) diDatasource=new DiDatasource();
		diDatasource.setName(datasourceName);
		diDatasource.setDataAdapterType(Integer.parseInt(dataAdapterType));
		diDatasource.setDataAdapter(dataAdapter);
		diDatasource.setExecSql(execSql);
		if(diDatasource.getId() != null && !diDatasource.getId().equals("")){
			diDatasourceService.update(diDatasource);
		}else{
			diDatasourceService.insert(diDatasource);
		}
		Map<String,Object> resultMap=new HashMap<String, Object>();
		resultMap.put("operator","success");
		return resultMap;
	}

	/**
	 * author wangdongxing
	 * description 删除数据源
	 * @return json数据
	 */
	@RequestMapping(value="/datasource_delete",method = RequestMethod.POST)
	@ResponseBody
	public Object datasourceDelete(String idstr){
		String[] ids= JsonUtils.fromJsonByGoogle(idstr, new TypeToken<String[]>() {
		});
		Map<String,Object> resultMap=new HashMap<String, Object>();
		try {
			diDatasourceService.deleteDatasource(ids);
			resultMap.put("operator", true);
			resultMap.put("message","删除成功");
		} catch (Exception e) {
			resultMap.put("operator", false);
			resultMap.put("message",e.getMessage());
		}
		return resultMap;
	}

	/**
	 * author wangdongxing
	 * description 调试sql的正确性
	 * @return json数据
	 */
	@RequestMapping(value="/datasource_test",method = RequestMethod.POST)
	@ResponseBody
	public Object datasourceCheckSql(String dataAdapterType,String dataAdapter,String sql){
		Map<String,Object> resultMap=new HashMap<String, Object>();
		List<String> columnList = null;
		try {
			if(dataAdapterType.equals("1")){// database
				columnList=diDatasourceService.verifyDBDatasource(dataAdapter,sql);
			}else if(dataAdapterType.equals("2")){// webService interface
				columnList=diDatasourceService.verifyInterfaceDatasource(dataAdapter);
			}
			if(columnList != null &&  columnList.size() >0){
				resultMap.put("operator", true);
				resultMap.put("message",columnList);
			}else{
				resultMap.put("operator", false);
				resultMap.put("message","连接无效");
			}
		} catch (Exception e) {
			throw  new BusinessException(e.getMessage());
		}
		return resultMap;
	}
	/**
	 * author liub
	 * description ： ui配置读取数据源数据
	 * return
	 */
	@RequestMapping(value="/datasource_get",method=RequestMethod.GET)
	@ResponseBody
	public List<DiDatasource> getList(){
		return this.diDatasourceService.find(null);
	}
	/**
	 * author wangdongxing
	 * description 数据源查询条件的分页展示
	 * param request
	 * @return json数据
	 */
	@RequestMapping(value="/datasource_getColumns",method= RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object getFields(String datasourceId){
		List<String> columnList=new ArrayList<String>();
		DiDatasource datasource=diDatasourceService.get(datasourceId);
		if(datasource !=null){
			try {
				if(datasource.getDataAdapterType() ==1 && datasource.getExecSql() !=null && !datasource.getExecSql().equals("")){
					//数据库类型数据源连接
					columnList = diDatasourceService.verifyDBDatasource(datasource.getDataAdapter(),datasource.getExecSql());
				}else if(datasource.getDataAdapterType() ==2){
					//接口类型的数据源连接
					columnList = diDatasourceService.verifyInterfaceDatasource(datasource.getDataAdapter());
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e.getMessage());
			}
		}
		return  columnList;
	}
	
	@RequestMapping(value="/datasource_getColumnsForMap",method= RequestMethod.GET)
	@ResponseBody
	public List<Map<String,String>> getFieldsForMap(String datasourceId){
		List<String> columnList = (List<String>) this.getFields(datasourceId);
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		for(int i = 0 ; i < columnList.size() ; i++){
			String value = columnList.get(i);
			Map<String,String> map = new HashMap<String,String>();
			map.put("columnId", value);
			list.add(map);
		}
		return  list;
	}

	/**
	 * 获取页面数据
	 * @param datasourceId 动态数据源ID
	 * @param paramsStr  附加的参数条件
	 * @return 返回数据源数据
	 */
	@RequestMapping(value="/datasource_getResultList",method= RequestMethod.POST,produces = "application/json")
	@ResponseBody
	public Object getDatasourceResultList(String datasourceId,String paramsStr){
		Map<String,Object> params=JsonUtils.fromJsonByGoogle(paramsStr,new TypeToken<Map<String,Object>>(){});
		if(datasourceId ==null || "".equals(datasourceId) ) return "No DatasourceId !";
		DiDatasource diDatasource=diDatasourceService.get(datasourceId);
		if(diDatasource == null) return "Invalid DatasourceId !";
		if(diDatasource.getDataAdapterType() == 1){
			return  diDatasourceService.getDatasourceResultList(diDatasource.getDataAdapter(),diDatasource.getExecSql(), "0");
		}else{
			return  diDatasourceService.getDatasourceResultList(diDatasource.getDataAdapter(),1,10000,params, "0");
		}

	}
}

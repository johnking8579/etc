package com.jd.oa.di.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 此处必须添加@WebServer注解，若不添加，则发布的服务当中无任何方法
 * @author wds
 *
 */
@WebService
public interface HelloService {
	@WebMethod(operationName = "sayHello1",action = "http://service.di.oa.jd.com/sayHello1")
	public String sayHello(@WebParam(name="param1") String param1,@WebParam(name="param2") String param2);
}

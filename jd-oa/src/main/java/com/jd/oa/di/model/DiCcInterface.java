package com.jd.oa.di.model;

import java.util.Date;

public class DiCcInterface {
    //ID
    private String id;

    //接口类型：SOAP WS，REST WS，SAF，HTTP
    private String interfaceType;
    
    //接口连接名称
    private String interfaceName;

    //接口连接描述
    private String interfaceDesc;

    //
    private String url;

    
    private String method;

    // 输入参数类型：xml、json
    private String inParameterType;

    //输入参数模板
    private String inParameterTemplate;

    // 输出参数类型：xml、json
    private String outParameterType;

    //输出参数模板
    private String outParameterTemplate;

    
    private int yn;

    
    private String creator;

    
    private Date createTime;

    
    private String modifier;

    
    private Date modifyTime;
//    AUTH_HEADER
    private String authHeader;

    
    public String getId() {
        return id;
    }

    
    


	public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    
    public String getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(String interfaceType) {
        this.interfaceType = interfaceType == null ? null : interfaceType.trim();
    }

   
    public String getInterfaceName() {
        return interfaceName;
    }

    
    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

   
    public String getInterfaceDesc() {
        return interfaceDesc;
    }

    
    public void setInterfaceDesc(String interfaceDesc) {
        this.interfaceDesc = interfaceDesc == null ? null : interfaceDesc.trim();
    }

   
    public String getUrl() {
        return url;
    }

    
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

   
    public String getMethod() {
        return method;
    }

   
    public void setMethod(String method) {
        this.method = method == null ? null : method.trim();
    }

    
    public String getInParameterType() {
        return inParameterType;
    }

   
    public void setInParameterType(String inParameterType) {
        this.inParameterType = inParameterType == null ? null : inParameterType.trim();
    }

    
    public String getInParameterTemplate() {
        return inParameterTemplate;
    }

    
    public void setInParameterTemplate(String inParameterTemplate) {
        this.inParameterTemplate = inParameterTemplate == null ? null : inParameterTemplate.trim();
    }

    
    public String getOutParameterType() {
        return outParameterType;
    }

   
    public void setOutParameterType(String outParameterType) {
        this.outParameterType = outParameterType == null ? null : outParameterType.trim();
    }

    public String getOutParameterTemplate() {
        return outParameterTemplate;
    }

   
    public void setOutParameterTemplate(String outParameterTemplate) {
        this.outParameterTemplate = outParameterTemplate == null ? null : outParameterTemplate.trim();
    }

   
    public int getYn() {
        return yn;
    }

  
    public void setYn(int yn) {
        this.yn = yn;
    }

    
    public String getCreator() {
        return creator;
    }

    
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

   
    public Date getModifyTime() {
        return modifyTime;
    }

   
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
    
    public String getAuthHeader() {
		return authHeader;
	}


	public void setAuthHeader(String authHeader) {
		this.authHeader = authHeader;
	}
}
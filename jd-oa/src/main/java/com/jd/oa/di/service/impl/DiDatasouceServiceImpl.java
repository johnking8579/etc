package com.jd.oa.di.service.impl;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.file.FileUtil;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageJDBC;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.dao.DiDatasourceDao;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.model.DiDatasourceCondition;
import com.jd.oa.di.model.DiTransforLog;
import com.jd.oa.di.service.*;
import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by wangdongxing on 14-2-19.
 * Email : wangdongxing@jd.com
 */
@Service("diDatasourceService")
@CacheProxy
public class DiDatasouceServiceImpl extends BaseServiceImpl<DiDatasource,String> implements DiDatasourceService {

	@Autowired
	private DiDatasourceDao diDatasourceDao;

	@Autowired
	private DiDatasourceConditionService diDatasourceConditionService;

	@Autowired
	private DiCcInterfaceService diCcInterfaceService;
	
	@Autowired
	private DiTransforLogService diTransforLogService;
	@Autowired
	private JssService jssService;
	
	@Override
	public BaseDao<DiDatasource, String> getDao() {
		return diDatasourceDao;
	}

	public DiDatasourceDao getDiDatasourceDao() {
		return diDatasourceDao;
	}

	public void setDiDatasourceDao(DiDatasourceDao diDatasourceDao) {
		this.diDatasourceDao = diDatasourceDao;
	}

	
	/**
	 * 删除数据源实现
	 * @param ids 数据源ID数组
	 */
	@Override
	public void deleteDatasource(String[] ids) {
		for(String id:ids){
			DiDatasourceCondition condition=new DiDatasourceCondition();
			condition.setDatasourceId(id);
			List<DiDatasourceCondition> conditionList = diDatasourceConditionService.find(condition);
			diDatasourceConditionService.delete(conditionList,true);
			diDatasourceDao.delete(id);
		}
	}

	/**
	 * 验证数据库类型的数据源
	 * @param dataAdapterId  数据源适配的连接ID
	 * @param sql  需要验证的sql
	 * @return 返回可用的字段列表
	 */
	@Override
	public List<String> verifyDBDatasource(String dataAdapterId, String sql) {
		return  DatasourceUtils.getDbMap(dataAdapterId,sql);
	}

	/**
	 * 取出map对象key中带有关键字“_LIST”的子map对象，其value是list类型
	 * @param map 父map对象
	 * @return 子map对象，其value实list类型
	 */
	private Map  getListEntry(Map<String,Object> map){
		Map<String,Object> m=new LinkedHashMap<String, Object>();
		if(map == null) return m;
		Iterator iterator=map.entrySet().iterator();
		while (iterator.hasNext()){
			Map.Entry<String,Object> entry=(Map.Entry<String,Object>) iterator.next();
			if(entry.getKey().endsWith("_LIST")){
				m.put("_KEY", entry.getKey().replaceAll("_LIST", ""));
				m.put("_LIST",entry.getValue());
				break;
			}else{
				m=getListEntry((Map<String,Object>) entry.getValue());
			}
		}
		return m;
	}

	/**
	 * 验证接口类型的数据源
	 * @param dataAdapterId  数据源适配的连接ID
	 * @return 返回数据源可以使用的字段列表
	 */
	@Override
	public List<String> verifyInterfaceDatasource(String dataAdapterId) {
		List<String> columnList=new ArrayList<String>();
		DiCcInterface diCcInterface=diCcInterfaceService.get(dataAdapterId);
		//处理<soap:header> 参数
		String soapHeaderTag ="";
		Map<String,Object> soapHeaderMap = null;
		String authHeader=diCcInterface.getAuthHeader();

		if(authHeader != null && !authHeader.equals("") ){
			soapHeaderMap=DataConvertion.parseXml2Map(authHeader);
			if(soapHeaderMap.size() != 0){
				soapHeaderTag=soapHeaderMap.keySet().toArray()[0].toString();
				soapHeaderMap=(Map<String,Object>) soapHeaderMap.get(soapHeaderTag);
			}
		}
		//处理输入参数
		Map<String,Object> inParams=DataConvertion.parseXml2Map(diCcInterface.getInParameterTemplate());
		inParams=(Map<String,Object>) inParams.get("in");

		Object o = DatasourceUtils.getInterfaceResult(dataAdapterId,soapHeaderTag,soapHeaderMap,inParams);
		//接口测试失败
		if(o == null) return  null;
		//输出类型为xml
		Map<String,Object> outParams=DataConvertion.parseXml2Map(diCcInterface.getOutParameterTemplate());
		Map<String,Object> map=getListEntry(outParams);
		map=(Map<String,Object>) map.get("_LIST");
		if(map != null){
			Set<Map.Entry<String,Object>> entrySet=map.entrySet();
			for(Map.Entry<String,Object> entry:entrySet){
				columnList.add(entry.getKey());
			}
		}
		return  columnList;
	}

	/**
	 * 找出关键是key且值是List类型的Map对象的key
	 * @param map  多层的map数据对象
	 * @param key 值是List类型的Map对象的key
	 * @return 找出的map对象
	 */
	private Map<String,Object> getListMap(Map<String,Object> map,String  key){
		Map<String,Object> m=new LinkedHashMap<String, Object>();
		if(map==null) return null;
		Iterator iterator=map.entrySet().iterator();
		while (iterator.hasNext()){
			Map.Entry<String,Object> entry=(Map.Entry<String,Object>) iterator.next();
			String str1 = entry.getKey().replaceAll("_","").toLowerCase();
			String str2 = key.replaceAll("_","").toLowerCase();
			if(str1.equals(str2)){
				m.put("_LIST",entry.getValue());
				break;
			}else{
				if(entry.getValue() instanceof Map){
					m=getListMap((Map<String,Object>) entry.getValue(),key);
				}
			}
		}
		return m;
	}

	/**
	 * 根据需要筛选的字段返回过滤后的新Map对象
	 * @param srcMap 被筛选的Map
	 * @param fields 筛选字段
	 * @return 包含筛选字段的Map
	 */
	private Map<String,Object> filterMapByFields(Map<String,Object> srcMap,Set<String> fields){
		Map<String,Object> map=new LinkedHashMap<String, Object>();
		for(String s:fields){
			String str1 = s.replaceAll("_","").toLowerCase();
			for(String srcKey:srcMap.keySet()){
				 String str2 = srcKey.replaceAll("_","").toLowerCase();
				if(str1.equals(str2)){
					map.put(s,srcMap.get(srcKey));
				}
			}
		}
		return  map;
	}

	/**
	 * 转换接口输入参数的类型
	 * @param inParams
	 * @param inParamsTypeMap
	 */
	private void  convertInParamtetMap(Map<String,Object> inParams,Map<String,String> inParamsTypeMap){
		for(Map.Entry<String,Object> entry:inParams.entrySet()){
			String type = inParamsTypeMap.get(entry.getKey());
			if(type == null){
				inParams.put(entry.getKey(),entry.getValue());
			}else{
				if(type.toLowerCase().equals("int")){
					inParams.put(entry.getKey(),DataConvertion.object2Int(entry.getValue()));
				}else if(type.toLowerCase().equals("string")){
					inParams.put(entry.getKey(),DataConvertion.object2String(entry.getValue()));
				}else if(type.toLowerCase().equals("float")){
					inParams.put(entry.getKey(),DataConvertion.object2Float(entry.getValue()));
				}else if(type.toLowerCase().equals("double")){
					inParams.put(entry.getKey(),DataConvertion.object2Double(entry.getValue()));
				}else if(type.toLowerCase().equals("long")){
					inParams.put(entry.getKey(),DataConvertion.object2Long(entry.getValue()));
				}else if(type.toLowerCase().equals("date")){
					inParams.put(entry.getKey(),DataConvertion.object2Date(entry.getValue(),"yyyy/MM/dd HH:mm:ss"));
				}else if(type.toLowerCase().equals("xmldate")){
					inParams.put(entry.getKey(),DataConvertion.object2XMLGregorianCalendar(entry.getValue(), "yyyy/MM/dd HH:mm:ss"));
				}else{
					inParams.put(entry.getKey(),entry.getValue());
				}
			}
		}
	}

	/**
	 * 获取接口类型数据源返回的数据
	 * @param dataAdapter   数据源适配的连接ID
	 * @param pageNum  页码
	 * @param pageSize  页面大小
	 * @param params 接口需要传入的参数
	 * @return 返回List类型数据，每一项为map类型
	 * @param pageSize  isService 1:服务接口;0:查询数据接口
	 */
	@Override
	@Transactional
	public List<Map<String,Object>>  getDatasourceResultList(String dataAdapter, int pageNum,int pageSize,Map<String,Object> params,String isService) {		
		CallerInfo info = Profiler.registerInfo("wfp_datasourceresultlist",false,true); //方法性能监控
		
		String key = "";		
		List<Map<String,Object>> resultList=new ArrayList<Map<String,Object>>();
		DiCcInterface diCcInterface = diCcInterfaceService.get(dataAdapter);

		String soapHeaderTag ="";
		Map<String,Object> soapHeaderMap = null;
		
		String inParamsStr = "";
		Map<String,Object> inParams=null;
		
		if(diCcInterface != null){
			//记录每个参数的类型
			Map<String,String> inParamsTypeMap = new LinkedHashMap<String, String>();
			try {
				//处理<soap:header> 参数
				String authHeader=diCcInterface.getAuthHeader();
				if(authHeader != null && !authHeader.equals("") ){
					soapHeaderMap=DataConvertion.parseXml2Map(authHeader);
					if(soapHeaderMap.size() != 0){
						soapHeaderTag=soapHeaderMap.keySet().toArray()[0].toString();
						soapHeaderMap=(Map<String,Object>) soapHeaderMap.get(soapHeaderTag);
					}
				}
				//处理webservice输入参数
				Map<String,Object> inParamsTemplate=DataConvertion.parseXml2Map(diCcInterface.getInParameterTemplate());
				inParams=(Map<String,Object>) inParamsTemplate.get("in");
				Map<String,Object> inParamsValueMap = new LinkedHashMap<String, Object>();
				if(inParams != null && inParams.size() != 0){
					for(Map.Entry<String,Object> entry:inParams.entrySet()){
						String inParamItemKey = entry.getKey();
						String[] inParamItemKeyArray = inParamItemKey.split("__");
						if(inParamItemKeyArray.length > 1){
							inParamsTypeMap.put(inParamItemKeyArray[0],inParamItemKeyArray[1]);
						}else{
							inParamsTypeMap.put(inParamItemKeyArray[0],null);
						}
						inParamsValueMap.put(inParamItemKeyArray[0],entry.getValue());
					}
				}
				//给每个接口参数赋值
				if(params != null && params.size() != 0){
					for(Map.Entry<String,Object> entry:params.entrySet()){
						if(inParamsValueMap.containsKey(entry.getKey())){
							inParamsValueMap.put(entry.getKey(),entry.getValue());
						}
					}
				}
				inParams = inParamsValueMap;
				//取回数据
				inParamsStr = inParams!=null?JsonUtils.toJsonByGoogle(inParams).toString():"";
				
			} catch(Exception e){
				e.printStackTrace();
				if ("1".equals(isService)) {
					//服务接口异常处理:截获异常；调用失败后，自动加入消息队列，以便后台进行自动或手动重发。
					DiTransforLog entity = new DiTransforLog();
					entity.setDataAdapterType("2");
					entity.setInterfaceId(dataAdapter);
					entity.setSoapHeaderTag(soapHeaderTag);
					String soapHeaderStr = soapHeaderMap!=null?JsonUtils.toJsonByGoogle(soapHeaderMap).toString():"";
					String inParamterStr = params!=null?JsonUtils.toJsonByGoogle(params).toString():"";
					entity.setSoapHeader(soapHeaderStr);
					entity.setInParameter(inParamterStr);		
					entity.setTransFlag("1");		
					diTransforLogService.insert(entity);	
				}
				Profiler.functionError(info);  //方法可用率监控
				Profiler.registerInfoEnd(info);
				return resultList;
			}	
		
			try {				
				Object o = null;
				if ("1".equals(isService)) {
					o = getInterfaceService(diCcInterface.getId(),soapHeaderTag,soapHeaderMap,inParams,inParamsTypeMap);
				} else {
					o = getInterfaceResult(diCcInterface.getId(),soapHeaderTag,soapHeaderMap,inParamsStr,inParamsTypeMap);
				}			
				
				if(o == null) return null;
				//根据输出模板类型找到真正需要的核心数据的key和需要映射的字段
				Map<String,Object> outParams=DataConvertion.parseXml2Map(diCcInterface.getOutParameterTemplate());
				Map<String,Object> fieldMap=getListEntry(outParams);
				//模板中的字段映射Map
				Map<String,Object> templateMap=(Map<String,Object>) fieldMap.get("_LIST");
	
				// o 的几种类型 List\List-Map\Map\String
				if(o instanceof List){
					List  list=(List) o;
					for(Object son:list){
						if(son instanceof Map){
							resultList.add(filterMapByFields((Map<String, Object>) son, templateMap.keySet()));
						}else{
	
						}
					}
				}else if(o instanceof Map){
					Map<String,Object> interfaceResultMap =(HashMap) o;
//					if(interfaceResultMap == null || interfaceResultMap.size() == 0) return null;
					//根据key找到真正的映射字段所在的该层map
					if(fieldMap.get("_KEY").toString().equals("out")){
						resultList.add(filterMapByFields(interfaceResultMap,templateMap.keySet()));
					}else{
						Map<String,Object> listMap = getListMap(interfaceResultMap, fieldMap.get("_KEY").toString());
						Object obj = listMap.get("_LIST");
						if(obj == null) return null;
						if(obj instanceof ArrayList){
							List<Map<String,Object>> tempList=(List<Map<String,Object>>) obj;
							for(Map<String,Object> map:tempList){
								resultList.add(filterMapByFields(map,templateMap.keySet()));
							}
						}else{
							resultList.add(filterMapByFields((Map<String, Object>) obj,templateMap.keySet()));
						}
					}
				}else if(o instanceof String){
					Map<String,Object> map=new LinkedHashMap<String, Object>();
					if(templateMap.keySet().size()>0){
						for(String s:templateMap.keySet()){
							map.put(s,o);
						}
					}
					resultList.add(map);
				}else{
	
				}

			} catch(Exception e){
				e.printStackTrace();
				if ("1".equals(isService)) {
					//服务接口异常处理:截获异常；调用失败后，自动加入消息队列，以便后台进行自动或手动重发。
					DiTransforLog entity = new DiTransforLog();
					String uuid = IdUtils.uuid2();
					entity.setId(uuid);
					entity.setDataAdapterType("2");
					entity.setInterfaceId(dataAdapter);
					entity.setSoapHeaderTag(soapHeaderTag);
					String soapHeaderStr = soapHeaderMap!=null?JsonUtils.toJsonByGoogle(soapHeaderMap).toString():"";
					entity.setSoapHeader(soapHeaderStr);
					entity.setInParameter(inParamsStr);
					entity.setTransFlag("1");
					//将真实的数据类型保存到JSS上，方便重试的时候，类型匹配失败
					String paramsOBJString = FileUtil.Base64String(FileUtil.Object2Bytes(params));
					jssService.uploadFile(SystemConstant.BUCKET,uuid+".OBJ",paramsOBJString);
					diTransforLogService.insert(entity);
				}
				Profiler.functionError(info);  //方法可用率监控
				Profiler.registerInfoEnd(info);
				return resultList;
			}
			
//			if(diCcInterface.getOutParameterType().equals("1")){
//				//XML格式类型数据
//				if(o instanceof  List){
//					List  list=(List) o;
//					for(Object son:list){
//						resultList.add(filterMapByFields((Map<String, Object>) DataConvertion.Object2Map(son), templateMap.keySet()));
//					}
//				}else{
//					Map<String,Object> interfaceResultMap = null;
//					if(o instanceof  String){
//						interfaceResultMap = DataConvertion.parseXml2Map((String) o);
//					}else if(o instanceof Map){
//						interfaceResultMap =(HashMap) o;
//					}else if (o instanceof JAXBElement){
//						try {
//							//序列化为xml字符串
//							String xml=XmlUtil.marshal(o);
//							interfaceResultMap = DataConvertion.parseXml2Map(xml);
//						} catch (JAXBException e) {
//							e.printStackTrace();
//						}
//					}
//					if(interfaceResultMap == null || interfaceResultMap.size() == 0) return null;
//					//根据key找到真正的映射字段所在的该层map
//					Map<String,Object> listMap = getListMap(interfaceResultMap, fieldMap.get("_KEY").toString());
//					Object obj = listMap.get("_LIST");
//					if(obj == null) return null;
//					if(obj instanceof ArrayList){
//						List<Map<String,Object>> tempList=(List<Map<String,Object>>) obj;
//						for(Map<String,Object> map:tempList){
//							resultList.add(filterMapByFields(map,templateMap.keySet()));
//						}
//					}else{
//						resultList.add(filterMapByFields((Map<String, Object>) obj,templateMap.keySet()));
//					}
//				}
//			}else if(diCcInterface.getOutParameterType().equals("2")){
//				//JSON格式类型
//				String json=(String) o;
//				//数据整理成数组形式统一处理
//				if(!json.startsWith("[")){
//					json="["+json+"]";
//				}
//				try {
//					ObjectMapper mapper = new ObjectMapper();
//					if(json.indexOf("{")>0 && json.indexOf("}")>0){
//							//key-value 形式
//							List<Map<String,Object>>  list = mapper.readValue(json,ArrayList.class);
//							if(fieldMap.get("_KEY").toString().equals("out")){
//								for(Map<String,Object> map:list){
//									resultList.add(filterMapByFields(map,templateMap.keySet()));
//								}
//							}else{
//								Map<String,Object> listMap=getListMap(list.get(0), fieldMap.get("_KEY").toString());
//								Object obj = listMap.get("_LIST");
//								if(obj instanceof ArrayList){
//									List<Map<String,Object>> tempList=(List<Map<String,Object>>) obj;
//									for(Map<String,Object> map:tempList){
//										resultList.add(filterMapByFields(map,templateMap.keySet()));
//									}
//								}else{
//									resultList.add(filterMapByFields((Map<String, Object>) obj,templateMap.keySet()));
//								}
//							}
//					}else{
//						//非key-value形式
//						List<Object> list = mapper.readValue(json,ArrayList.class);
//						for(int i=0;i<list.size();i++){
//							Map<String,Object> 	map=new LinkedHashMap<String, Object>();
//							if(templateMap.keySet().size()>0){
//								for(String s:templateMap.keySet()){
//									map.put(s,list.get(i));
//								}
//							}
//							resultList.add(map);
//						}
//					}
//				} catch (IOException e) {
//					e.printStackTrace();
//					throw new BusinessException(e.getMessage());
//				}
//			}else if (diCcInterface.getOutParameterType().endsWith("3")){
//				//String
//				Map<String,Object> map=new LinkedHashMap<String, Object>();
//				if(templateMap.keySet().size()>0){
//					for(String s:templateMap.keySet()){
//						map.put(s,(String) o);
//					}
//				}
//				resultList.add(map);
//			}
		}
		//分页显示
//		if(resultList.size()==0) return  null;
//		int pageTotal=(int) Math.ceil((double)resultList.size() / (double)pageSize);
//		pageNum = (pageNum>pageTotal)?pageTotal:pageNum;
//		int fromIndex = (pageNum-1) * pageSize;
//		int toIndex=fromIndex + pageSize;
//		toIndex = (toIndex>resultList.size())?resultList.size():toIndex;
		
		if ("1".equals(isService)) {
			//服务接口异常处理:截获异常；调用失败后，自动加入消息队列，以便后台进行自动或手动重发。
			DiTransforLog entity = new DiTransforLog();
			entity.setDataAdapterType("2");
			entity.setInterfaceId(dataAdapter);
			entity.setSoapHeaderTag(soapHeaderTag);
			String soapHeaderStr = soapHeaderMap!=null?JsonUtils.toJsonByGoogle(soapHeaderMap).toString():"";
			entity.setSoapHeader(soapHeaderStr);
			entity.setInParameter(inParamsStr);
			entity.setTransFlag("0");
			diTransforLogService.insert(entity);	
		}
		Profiler.registerInfoEnd(info);
		return  resultList;
	}
	
	/**
	 * 
	 * @desc 远程数据源接口：缓存：为了实现缓存分离出的方法
	 * @author WXJ
	 * @date 2014-3-18 下午05:19:59
	 *
	 * @param interfaceId
	 * @param soapHeaderTag
	 * @param soapHeader
	 * @param inParamterStr
	 * @return
	 */
	@CachePut(key = @CacheKey(template = "JDOA/DiDatasourceInterface/getDatasourceResultList_${p0}_${p3}"), expire = 7200)
	private Object getInterfaceResult(String interfaceId,String soapHeaderTag, Map<String,Object> soapHeader,String inParamterStr,Map<String,String> inParamsTypeMap) {
		Object oo = JsonUtils.fromJsonByGoogle(inParamterStr,new TypeToken<LinkedHashMap<String,Object>>(){});
		Map<String,Object> inParamter = (Map<String,Object>)oo;
		convertInParamtetMap(inParamter,inParamsTypeMap);
		Object o=DatasourceUtils.getInterfaceResult(interfaceId,soapHeaderTag,soapHeader,inParamter);
		return o;
	}
	
	/**
	 * 
	 * @desc 远程服务接口:不能缓存：调用接口服务
	 * @author WXJ
	 * @date 2014-3-20 下午05:19:59
	 *
	 * @param soapHeaderTag
	 * @param soapHeader
	 * @param inParamter
	 * @return
	 */
	private Object getInterfaceService(String interfaceId,String soapHeaderTag, Map<String,Object> soapHeader,Map<String,Object> inParamter,Map<String,String> inParamsTypeMap) {
		convertInParamtetMap(inParamter,inParamsTypeMap);
		return DatasourceUtils.getInterfaceResult(interfaceId,soapHeaderTag,soapHeader,inParamter);
	}

	/**
	 * 分页数据库数据源接口：缓存：获取数据库类型的数据源返回的数据
	 * @param dataAdapter  数据源适配的连接ID
	 * @param pageNum  页码
	 * @param pageSize  页面大小
	 * @param sql 需要执行的sql语句且用于区分数据源是否是数据库连接类型
	 * @return 返回List类型数据，每一项为map类型
	 */
	@Override
	@CachePut(key = @CacheKey(template = "JDOA/DiDatasourceDatabaseForPage/getDatasourceResultList_${p0}_${p1}_${p2}_${p3}"), expire = 7200)
	public PageJDBC getDatasourceResultListForPage(String dataAdapter, int pageNum,int pageSize,String sql) {	
		PageJDBC page = DatasourceUtils.getDbResultForPage(dataAdapter,pageNum,pageSize,sql);
		return  page;
	}		
	
	/**
	 * 
	 * @desc 获取数据库类型数据源返回的数据或执行数据库脚本
	 * @author WXJ
	 * @date 2014-5-14 下午03:39:22
	 *
	 * @param dataAdapter
	 * @param sql
	 * @param isService
	 * @return
	 */
	@Override
	@Transactional
	public List<Map<String,Object>> getDatasourceResultList(String dataAdapter,String sql,String isService) {
		List<Map<String,Object>> o = new ArrayList<Map<String,Object>>();
		if ("1".equals(isService)) {
			try {
				o = getDatasourceService(dataAdapter,sql);
			} catch(Exception e){
				e.printStackTrace();
				//服务接口异常处理:截获异常；调用失败后，自动加入消息队列，以便后台进行自动或手动重发。
				DiTransforLog entity = new DiTransforLog();
				entity.setDataAdapterType("1");
				entity.setInterfaceId(dataAdapter);
				entity.setInParameter(sql);
				entity.setTransFlag("1");
				diTransforLogService.insert(entity);	
				return o;					
			}
			//服务接口异常处理:截获异常；调用失败后，自动加入消息队列，以便后台进行自动或手动重发。
			DiTransforLog entity = new DiTransforLog();
			entity.setDataAdapterType("1");
			entity.setInterfaceId(dataAdapter);
			entity.setInParameter(sql);
			entity.setTransFlag("0");
			diTransforLogService.insert(entity);	
		} else {
			o = getDatasourceResult(dataAdapter,sql);
		}
		return o;	
	}
	
	/**
	 * 数据库数据源接口：缓存：获取数据库类型的数据源返回的数据
	 * @param dataAdapter  数据源适配的连接ID
	 * @param sql 需要执行的sql语句且用于区分数据源是否是数据库连接类型
	 * @return 返回List类型数据，每一项为map类型
	 */	
	@CachePut(key = @CacheKey(template = "JDOA/DiDatasourceDatabase/getDatasourceResultList_${p0}_${p1}"), expire = 7200)
	private List<Map<String,Object>> getDatasourceResult(String dataAdapter,String sql) {
		return DatasourceUtils.getDbResult(dataAdapter,sql);
	}		
	
	/**
	 * 数据库服务接口：不能缓存：获取数据库类型的数据源返回的数据
	 * @param dataAdapter  数据源适配的连接ID
	 * @param sql 需要执行的sql语句且用于区分数据源是否是数据库连接类型
	 * @return 返回List类型数据，每一项为map类型
	 */
	private List<Map<String,Object>> getDatasourceService(String dataAdapter,String sql) {
		return DatasourceUtils.getDbResult(dataAdapter,sql);
	}		
	
}

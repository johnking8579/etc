/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-17
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.service.ws.impl;

import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormAttachment;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormAttachmentService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormSequenceService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.service.dto.ProcessFormTransferDto;
import com.jd.oa.service.ws.ProcessFormTransportationService;
import com.jd.oa.system.service.SysUserService;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.helpers.XMLUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import sun.misc.BASE64Decoder;

import javax.jws.WebService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebService(endpointInterface = "com.jd.oa.service.ws.ProcessFormTransportationService",targetNamespace="http://ws.service.oa.jd.com/")
public class ProcessFormTransportationServiceImpl implements ProcessFormTransportationService {

	private static final Logger logger = Logger.getLogger(ProcessFormTransportationServiceImpl.class);

	private final static String XML_DOC_ROOT = "DataSet";
	private final static String XML_DOC_PROCESSDEFINITION= "ProcessDefinition";
	private final static String XML_DOC_PROCESSDEFINITION_ID= "ProcessDefinitionID";
	private final static String XML_DOC_PROCESSDEFINITION_NAME= "ProcessDefinitionName";
	private final static String XML_DOC_PROCESSDINSTANCE_ID= "ProcessInstanceID";
	private final static String XML_DOC_PROCESSDINSTANCE_STATERERP= "StarterErp";
	private final static String XML_DOC_PROCESSDINSTANCE_SUBJECT = "Subject";
	private final static String XML_DOC_PROCESSDINSTANCE_BUSINESSID = "BusinessID";
	private final static String XML_DOC_MAINFORM = "MainForm";
	private final static String XML_DOC_MAINFORM_ID = "FormID";
	private final static String XML_DOC_MAINFORM_NAME = "FormName";
	private final static String XML_DOC_SUBFORMS= "SubForms";
	private final static String XML_DOC_SUBFORMS_ID= "SubFormID";
	private final static String XML_DOC_SUBFORMS_NAME= "SubFormName";
	private final static String XML_DOC_SUBFORMS_MODEL= "Model";

	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private FormService formService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private ProcessTaskService processTaskService;
	@Autowired
	private OaPafService oaPafService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private FormAttachmentService formAttachmentService;
	@Autowired
	private FormSequenceService formSequenceService;

	@Override
	public String requestFormDefinition(String processDefinitionId) {
		logger.info("外部表单导入接口调用:[processDefinitionId:"+processDefinitionId+"]");
		if(processDefinitionId == null || processDefinitionId.length() == 0){
			throw new BusinessException("不可用的流程定义ID");
		}
		ProcessDefinition processDefinition = processDefService.get(processDefinitionId);
		if(processDefinition == null
				|| processDefinition.getIsOuter().equals("1")
				|| processDefinition.getFormId() == null
				|| processDefinition.getFormId().equals("")
				|| processDefinition.getIsIssue().equals("0")){
			throw new BusinessException("不可用的流程定义");
		}
		ProcessFormTransferDto processFormTransferDto = new ProcessFormTransferDto();
		processFormTransferDto.setProcessDefinition(processDefinition);
		//主表单记录
		Form mainForm = formService.get(processDefinition.getFormId());
		List<FormItem> mainFormItemList = formItemService.selectByFormId(mainForm.getId());
		//所有子表单记录
		List<Form> subForms = mainForm.getSubTables();
		Map<String,List<FormItem>> subFormItemsMap = new HashMap<String, List<FormItem>>();
		for(Form subForm : subForms){
			subFormItemsMap.put(subForm.getId(),formItemService.selectByFormId(subForm.getId()));
		}
		ProcessNode processNode = processNodeService.getFirstNode(processDefinitionId);
		//为dto赋值
		processFormTransferDto.setMainForm(mainForm);
		processFormTransferDto.setMainFormItems(mainFormItemList);
		processFormTransferDto.setSubFormList(subForms);
		processFormTransferDto.setSubFormItemsMap(subFormItemsMap);
		processFormTransferDto.setFirstProessNode(processNode);
		//处理dto为xml数据
		return parse2Xml(processFormTransferDto);
	}

	@Override
	@Transactional (propagation = Propagation.REQUIRED)
	public String transportFormData(String xml) {
		logger.info("外部表单导入:[xml:"+xml+"]");
		//构造返回数据
		Document doc = DOMUtils.createDocument();
		//根结点
		Element root=doc.createElement(XML_DOC_ROOT);

		if(!DataConvertion.isXmlStr(xml.trim())){
			throw new BusinessException("不合法的xml数据");
		}
		Map<String,Object> rootMap = DataConvertion.parseXml2Map(xml.trim());
		if(rootMap == null || rootMap.size() == 0){
			throw new BusinessException("不合法的xml数据");
		}
		Map<String,Object> dataSetMap = (Map<String,Object>) rootMap.get(XML_DOC_ROOT);
		if(dataSetMap == null || dataSetMap.size() ==  0){
			throw new BusinessException(XML_DOC_ROOT+"下没有数据");
		}
		Map<String,Object> processDefinitionMap = (Map<String,Object>) dataSetMap.get(XML_DOC_PROCESSDEFINITION);
		if(processDefinitionMap == null || processDefinitionMap.size() == 0){
			throw new BusinessException(XML_DOC_PROCESSDEFINITION+"下没有数据");
		}
		//解析processDefinition
		String processDefinitionId = (String) processDefinitionMap.get(XML_DOC_PROCESSDEFINITION_ID);
		if(processDefinitionId == null || processDefinitionId.length() == 0){
			throw new BusinessException(XML_DOC_PROCESSDEFINITION_ID+"没有数据");
		}
		//向返回结果中添加<processDefinition>节点
		Element processDefinitionElement=doc.createElement(XML_DOC_PROCESSDEFINITION);

		//取出processDefinition信息
		ProcessDefinition processDefinition = processDefService.get(processDefinitionId);
		ProcessNode processFirstNode = processNodeService.getFirstNode(processDefinitionId);
		String processInstanceId =(String) processDefinitionMap.get(XML_DOC_PROCESSDINSTANCE_ID);
		String subject =(String) processDefinitionMap.get(XML_DOC_PROCESSDINSTANCE_SUBJECT);
		if(subject == null || subject.equals("")){
			throw new BusinessException("field ["+XML_DOC_PROCESSDINSTANCE_SUBJECT+"] should not be null!");
		}
		String starterErp = (String) processDefinitionMap.get(XML_DOC_PROCESSDINSTANCE_STATERERP);
		if(starterErp == null || starterErp.equals("")){
			throw new BusinessException("field ["+XML_DOC_PROCESSDINSTANCE_STATERERP+"] should not be null!");
		}
		String businessId = (String) processDefinitionMap.get(XML_DOC_PROCESSDINSTANCE_BUSINESSID);
		//向返回结果中添加<processDefinitionID>节点
		appendElement(doc, processDefinitionElement,XML_DOC_PROCESSDEFINITION_ID, processDefinition.getId());

		//解析主表信息
		Map<String,Object> mainFormMap = (Map<String,Object>)  dataSetMap.get(XML_DOC_MAINFORM);
		String mainFormId = (String) mainFormMap.get(XML_DOC_MAINFORM_ID);
		String mainFormName = (String) mainFormMap.get(XML_DOC_MAINFORM_NAME);

		List<Map<String,Object>> mainFormDataList=new ArrayList<Map<String, Object>>();
		List<FormItem> formItems = formItemService.selectByFormId(mainFormId);
		for(Map.Entry<String,Object> entry:mainFormMap.entrySet()){
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("fieldName",entry.getKey());
			FormItem item = null;
			for(FormItem formItem : formItems){
				if(formItem.getFieldName().equals(entry.getKey())){
					item = formItem;
					break;
				}
			}
			if(item == null){
				map.put("value",entry.getValue());
			}else{
				if(!isNull(processDefinitionId,processFirstNode.getNodeId(),mainFormId,item.getId())){
					if(entry.getValue() == null
							|| (entry.getValue() instanceof  String &&  ((String) entry.getValue()).length() == 0)){
						throw new BusinessException("field ["+entry.getKey()+"] should not be null!");
					}
				}
				if(item.getInputType().equals("附件")){
					Map<String,Object> thisMap =(Map<String,Object>) entry.getValue();
					Object object = thisMap.get("File");
					if(object == null) continue;
					String fileIds="";
					StringBuilder fileIdsSb = new StringBuilder("");
					
					if(object instanceof Map){
						fileIds = uploadFile((Map<String,Object>)object,null,null);
					}else if(object instanceof List){
						List list = (List) object;
						for(Object o:list){
//							fileIds+=","+uploadFile((Map)o,null,null);
							fileIdsSb.append(","+uploadFile((Map)o,null,null));
						}
						fileIds = fileIdsSb.toString();
						if(fileIds.length()>0) fileIds = fileIds.substring(1);
					}else{
					}
					
					map.put("value",fileIds);
				}else{
					map.put("value",entry.getValue());
				}
			}
			mainFormDataList.add(map);
		}

		String businessObjectId = businessId;
		logger.info("外部表单导入保存主表信息:[mainFormName:"+mainFormName+",datas:,"+DataConvertion.object2Json(mainFormDataList)+",businessId:"+businessId+",mainFormId:"+mainFormId+"]");
		String executeId = applicationService.saveBusinessObject(mainFormName,DataConvertion.object2Json(mainFormDataList),businessId,mainFormId);
		logger.info("外部表单导入保存主表信息结果:["+executeId+"]");
		if(businessId == null || businessId.trim().length() == 0){
			businessObjectId = executeId;
		}
		//向返回结果中添加<BusineesID>节点
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_BUSINESSID,businessObjectId);
		//解析子表信息
		Map<String,Object> subFormsListMap = (Map<String,Object>)  dataSetMap.get(XML_DOC_SUBFORMS);
		if(subFormsListMap != null && subFormsListMap.size() != 0){
			for(Map.Entry<String,Object> entry:subFormsListMap.entrySet()){
				String subRecordIds="";
				Map<String,Object> modelMap = (Map<String,Object>)entry.getValue();
				String subFormId = (String) modelMap.get(XML_DOC_SUBFORMS_ID);
				String subFormName = (String) modelMap.get(XML_DOC_SUBFORMS_NAME);
				List<FormItem> subFormItems = formItemService.selectByFormId(subFormId);
				//先删除-再插入子表记录
				logger.info("外部表单导入删除子表:[subFormName:"+subFormName+",businessObjectId:"+businessObjectId+"]");
				applicationService.deleteBusinessObjectByParentId(subFormName,businessObjectId);
				Object data = modelMap.get(XML_DOC_SUBFORMS_MODEL);
				List<Object> dataList = new ArrayList<Object>();
				if(data instanceof List){
					dataList.addAll((List) data);
				}else{
					dataList.add(data);
				}
				for(Object item : dataList){
					Map<String,Object> itemMap = (Map<String,Object>) item;
					//待上传的附件，需要先保存子表，获取子表ID才能保存附件
					Map<String,Object> filesMap = new HashMap<String, Object>();
					for(Map.Entry<String,Object> entry1 : itemMap.entrySet()){
						FormItem subItem = null;
						for(FormItem formItem : subFormItems){
							if(formItem.getFieldName().equals(entry1.getKey())){
								subItem = formItem;
								break;
							}
						}
						if(subItem == null){

						}else{
							if(!isNull(processDefinitionId,processFirstNode.getNodeId(),subFormId,subItem.getId())){
								if(entry1.getValue() == null
										|| (entry1.getValue() instanceof String && ((String)entry1.getValue()).length() == 0)){
									throw new BusinessException("field ["+entry1.getKey()+"] should not be null!");
								}
							}
							if(subItem.getInputType().equals("附件")){
								Map<String,Object> thisMap =(Map<String,Object>) entry1.getValue();
								Object object = thisMap.get("File");
								String fileIds = "";
								StringBuilder fileIdsSb = new StringBuilder("");
								if(object instanceof Map){
									fileIds = IdUtils.uuid2();
									filesMap.put(fileIds,object);
								}else if(object instanceof List){
									for(Object o:(List) object){
										String id =  IdUtils.uuid2();
//										fileIds += "," +id;
										fileIdsSb.append("," +id);
										filesMap.put(id,o);
									}
									fileIds = fileIdsSb.toString();
								}else{
								}
								if(fileIds.length()>0) fileIds = fileIds.substring(1);
								itemMap.put(entry1.getKey(),fileIds);
							}else{

							}
						}
					}
					itemMap.put("ID","");
					itemMap.put("PARENT_ID", businessObjectId);
					List<Object> list = new ArrayList<Object>();
					list.add(item);
					String datas = DataConvertion.object2Json(list);
					logger.info("外部表单导入保存子表:[subFormName:"+subFormName+",datas:"+datas+",businessObjectId:"+businessObjectId+",mainFormId:"+mainFormId+"]");
					subRecordIds = applicationService.saveBusinessObjectForSub(subFormName, datas, businessObjectId, mainFormId);
					logger.info("外部表单导入保存子表结果:["+subRecordIds+"]");
					//判断是否有附件要上传
					if(filesMap.size()>0){
						for(Map.Entry<String,Object> en:filesMap.entrySet()){
							uploadFile((Map) en.getValue(),en.getKey(),subRecordIds);
						}
					}
				}

			}
		}

		if(processInstanceId == null || processInstanceId.trim().length() == 0){
			//启动流程实例
			processInstanceId = IdUtils.uuid2();
			//生成流水单号
			String followCode = null;
			if(processDefinition.getFollowCodePrefix() != null && !processDefinition.getFollowCodePrefix().equals("")){
				 followCode = formSequenceService.getProcessSequenceCode(processDefinition.getFollowCodePrefix());
			}
			try {
				logger.info("外部表单导入创建实例参数:[processDefKye:"+processDefinition.getPafProcessDefinitionId()+",oaProcessInstanceId:"+processInstanceId+",subject:"+subject+",starterErp:"+starterErp+"]");
				com.jd.oa.common.paf.model.ProcessInstance processInstance = processTaskService.processAppSubmitInstance(
						processDefinition.getPafProcessDefinitionId(), processFirstNode.getNodeId(), mainFormId, businessObjectId,
						mainFormName, subject, "1", processInstanceId, starterErp,followCode);
				logger.info("外部表单导入创建实例结果:[startErp:"+starterErp+",oaProcessInstanceId:"+processInstanceId+",processInstanceId:"+processInstance.getId()+"]");
				//向返回结果中添加<processInstanceID>节点
				appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_ID,processInstanceId);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e);
			}
		}else{
			//驳回、拒绝到申请人的表单，重新提交
			ProcessInstance processInstance = processInstanceService.get(processInstanceId);
			//更新主题
			processInstance.setProcessInstanceName(subject);
			processInstanceService.update(processInstance);
			List<OaTaskInstance> oaTaskInstanceList = oaPafService.getActiveTaskByInstance(starterErp,processInstance.getProcessInstanceId());
			try {
				OaTaskInstance oaTaskInstance = oaTaskInstanceList.get(0);
				oaTaskInstance.setNodeId(processFirstNode.getNodeId());
				oaTaskInstance.setBuinessInstanceId(businessObjectId);
				logger.info("外部表单导入驳回退回状态重新提交参数:[starterErp:"+starterErp+",oaTaskInstance:"+DataConvertion.object2Json(oaTaskInstance)+"]");
				processTaskService.processTaskComplete(starterErp,oaTaskInstance);
				logger.info("外部表单导入驳回退回状态重新提交结果:[成功]");
				appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_ID,processInstanceId);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException(e);
			}
		}
		root.appendChild(processDefinitionElement);
		return XMLUtils.toString(root,4);
	}

	private String parse2Xml(ProcessFormTransferDto dto){
		ProcessDefinition processDefinition = dto.getProcessDefinition();
		Document doc = DOMUtils.createDocument();
		//根结点
		Element root=doc.createElement(XML_DOC_ROOT);
		//创建ProcessDefinition结点
		Element processDefinitionElement=doc.createElement(XML_DOC_PROCESSDEFINITION);
		appendElement(doc, processDefinitionElement,XML_DOC_PROCESSDEFINITION_ID, processDefinition.getId());
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDEFINITION_NAME, processDefinition.getProcessDefinitionName());
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_ID,"");
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_STATERERP, "",true);
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_SUBJECT,"",true);
		appendElement(doc, processDefinitionElement, XML_DOC_PROCESSDINSTANCE_BUSINESSID,"");
		root.appendChild(processDefinitionElement);
		//创建主表form结点
		Element mainFormElement = doc.createElement(XML_DOC_MAINFORM);
		appendElement(doc,mainFormElement,XML_DOC_MAINFORM_ID,dto.getMainForm().getId());
		appendElement(doc,mainFormElement,XML_DOC_MAINFORM_NAME,dto.getMainForm().getFormCode());
		List<FormItem> mainFormItems = dto.getMainFormItems();
		for(FormItem item : mainFormItems){
			if(item.getInputType().equals("附件")){ //附件类型字段
				Element fileElement = appendElement(doc,mainFormElement,item.getFieldName());
				Element fileModelElement = appendElement(doc,fileElement,"File");
				if(!isNull(dto.getProcessDefinition().getId(),dto.getFirstProessNode().getNodeId(),dto.getMainForm().getId(),item.getId())){
					fileElement.setAttribute("NotNull","true");
				}
				appendElement(doc,fileModelElement,"FileName","");
				appendElement(doc,fileModelElement,"FileType","");
				appendElement(doc,fileModelElement,"FileSize","");
				appendElement(doc,fileModelElement,"FileBytes","");
			}else{
				if(!isNull(dto.getProcessDefinition().getId(),dto.getFirstProessNode().getNodeId(),dto.getMainForm().getId(),item.getId())){
					appendElement(doc,mainFormElement,item.getFieldName(),"",true);
				}else{
					appendElement(doc,mainFormElement,item.getFieldName(),"");
				}
			}
		}
		root.appendChild(mainFormElement);
		//创建子表form结点
		List<Form> subFormList = dto.getSubFormList();
		if(subFormList != null && subFormList.size() > 0){
			Element subFormsElement = doc.createElement(XML_DOC_SUBFORMS);
			List<FormItem> subFormItems;
			for(Form form : subFormList){
				Element subFormElement = doc.createElement(form.getFormCode());
				subFormsElement.appendChild(subFormElement);
				appendElement(doc, subFormElement, XML_DOC_SUBFORMS_ID, form.getId());
				appendElement(doc,subFormElement,XML_DOC_SUBFORMS_NAME,form.getFormCode());
				Element modelElement = doc.createElement(XML_DOC_SUBFORMS_MODEL);
				subFormElement.appendChild(modelElement);
				subFormItems = dto.getSubFormItemsMap().get(form.getId());
				for(FormItem item : subFormItems){
					if(item.getInputType().equals("附件")){ //附件类型字段
						Element fileElement = appendElement(doc,modelElement,item.getFieldName());
						if(!isNull(dto.getProcessDefinition().getId(),dto.getFirstProessNode().getNodeId(),form.getId(),item.getId())){
							fileElement.setAttribute("NotNull","true");
						}
						Element fileModelElement = appendElement(doc,fileElement,"File");
						appendElement(doc,fileModelElement,"FileName","");
						appendElement(doc,fileModelElement,"FileType","");
						appendElement(doc,fileModelElement,"FileSize","");
						appendElement(doc,fileModelElement,"FileBytes","");
					}else{
						if(!isNull(dto.getProcessDefinition().getId(),dto.getFirstProessNode().getNodeId(),form.getId(),item.getId())){
							appendElement(doc,modelElement,item.getFieldName(),"",true);
						}else{
							appendElement(doc,modelElement,item.getFieldName(),"");
						}
					}
				}
			}
			root.appendChild(subFormsElement);
		}
		return XMLUtils.toString(root,4);
	}

	private Element appendElement(Document doc,Element parent,String nodeName,String textValue){
		Element element = doc.createElement(nodeName);
		element.setTextContent(textValue);
		parent.appendChild(element);
		return element;
	}
	private Element appendElement(Document doc,Element parent,String nodeName,String textValue,boolean NotNull){
		Element element = appendElement(doc,parent,nodeName,textValue);
		if(NotNull){
			element.setAttribute("NotNull","true");
		}
		return element;
	}

	private Element appendElement(Document doc,Element parent,String nodeName){
		Element element = doc.createElement(nodeName);
		parent.appendChild(element);
		return element;
	}

	private String uploadFile(Map<String,Object> objectMap,String id,String businessId){
		String fileName =(String) objectMap.get("FileName");
		String fileType = (String) objectMap.get("FileType");
		String fileSize = (String) objectMap.get("FileSize");
		String fileBytes = (String) objectMap.get("FileBytes");
		if(fileBytes == null || fileBytes.equals("")) return "";
		byte[] bytes;
		try {
			bytes = new BASE64Decoder().decodeBuffer(fileBytes);
		} catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException("附件解码失败！");
		}
		FormAttachment formAttachment = new FormAttachment();
		formAttachment.setId(id);
		formAttachment.setAttachmentExt(fileType);
		formAttachment.setAttachmentName(fileName);
		formAttachment.setAttachmentSize(Long.parseLong(fileSize));
		formAttachment.setBusinessId(businessId);
		formAttachment.setAttachmentKey(id+fileType);
		return formAttachmentService.uploadFileToJssService(formAttachment,new ByteArrayInputStream(bytes));
	}

	private ProcessNodeFormPrivilege getPrivilegeByFormItem(String processDefinitionId,String processNodeId,String formId,String formItemId){
		ProcessNodeFormPrivilege privilege = new ProcessNodeFormPrivilege();
		privilege.setProcessDefinitionId(processDefinitionId);
		privilege.setFormId(formId);
		privilege.setNodeId(processNodeId);
		privilege.setItemId(formItemId);
		List<ProcessNodeFormPrivilege> privilegeList = processNodeFormPrivilegeService.selectByCondition(privilege);
		if(privilegeList != null && privilegeList.size() >0){
			return privilegeList.get(0);
		}else{
			return null;
		}
	}

	private boolean isNull(String processDefinitionId,String processNodeId,String formId,String formItemId){
		ProcessNodeFormPrivilege privilege = getPrivilegeByFormItem(processDefinitionId,processNodeId,formId,formItemId);
		if(privilege == null) return true;
		if(privilege.getNull() == null) return true;
		if(privilege.getNull().equals("0")) return false;
		return true;
	}


}

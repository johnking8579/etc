package com.jd.oa.service.ws;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.jd.oa.service.dto.MyApply;
import com.jd.oa.service.dto.MyTask;
import com.jd.oa.service.dto.ServiceResponse;

/**
 * 提供流程运行实例过程数据相关接口
 * 
 * @author zhouhuaqi
 * 
 */
@WebService
public interface ProcessService {

	/**
	 * 获取我的申请数据列表
	 * 
	 * @param userName
	 *            用户名，erpid
	 * @return
	 */
	public ServiceResponse<MyApply> getMyApplys(
			@WebParam(name = "userName") String userName);

	/**
	 * 获取我的代办数据列表
	 * 
	 * @param userName
	 *            用户名，erpid
	 * @return
	 */
	public ServiceResponse<MyTask> getMyTasks(
			@WebParam(name = "userName") String userName);
}

package com.jd.oa.agent.service.impl;

import com.jd.activemq.producer.JMSProducer;
import com.jd.oa.agent.service.MqProduceEbsService;
import com.jd.oa.agent.service.MqProduceUimService;
import com.jd.oa.common.utils.PlatformProperties;

import javax.annotation.Resource;
import javax.jms.JMSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("mqProduceUimService")
public class MqProduceUimServiceImpl implements MqProduceUimService {
	
	//@Resource(name = "produceUim")
	private JMSProducer producerUim;
	
	private static final Log logger = LogFactory.getLog(MqProduceEbsService.class);

	@Override
	public String mqProducer(String textMessage) {
		// TODO Auto-generated method stub
		
		String producer = PlatformProperties.getProperty("mq.uim.producer.destination");
		
		try {
			logger.info("send Uim message: " + textMessage);
			producerUim.send(producer, textMessage, "");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
		
		return "success";
	}
	

}
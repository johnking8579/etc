package com.jd.oa.agent.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.jd.oa.agent.UniteToDoInterface;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.design.style.FormStyleInterface;
import com.jd.oa.form.design.style.impl.FormStyle_4_Impl;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.form.util.HtmlModelFactory;
import com.jd.oa.form.util.RepleaseKey;

public class UniteToDoAbst implements UniteToDoInterface{

	private FormTemplateService formTemplateService;
	
	@Override
	public String getExecuteWorklistBindReportFormContext(
			HttpServletRequest request, HttpServletResponse response,
			String processDefinitionKey, String businessObjectId,
			String processNodeId, String processInstanceId,
			String processInstanceStatus) throws Exception {
		if(this.formTemplateService == null){
			this.formTemplateService = SpringContextUtils.getBean(FormTemplateService.class);
		}
		
		
		//从本地获取统一待办表单模板
		String templateContext = "";
		FormStyleInterface style = new FormStyle_4_Impl();
		style.setHtmlName("_FormStyle_4");
//		try {
//			response.getWriter().write(String.valueOf(style.getWeb()));
//		} catch (IOException e) {
//			
//		}
		String templateContextJSONData = "";
		try {
			templateContextJSONData = this.formTemplateService.getTemplateValue(businessObjectId);
		} catch (IOException e) {
			throw new BusinessException("获取统一待办业务数据失败!",e);
		}
		Map<String,String> paramMapTags = new HashMap<String,String>();
		UniteToDoAbst impl = null;
		if(isXml(templateContextJSONData)){
			impl = new UniteToDoRemdyImpl();
		} else {
			impl = new UniteToDoEbsImpl();
		}
		paramMapTags.put("PART1", impl.getPart(templateContextJSONData));
		paramMapTags.put("SUBSHEET", impl.getSubSheet(templateContextJSONData));
		paramMapTags.put("JDOAOPINION", impl.getOpinion());
		paramMapTags.put("JDOA", "");
		paramMapTags.put("FORMTITLE", impl.getFormTitle(templateContextJSONData));
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		return RepleaseKey.replace(HtmlModelFactory.getModel(contextPath,style.getHtmlName()), paramMapTags, "[@", "]");
	}
	public String getFormTitle(String templateContextJSONData){
		return "";
	}
	public String getItemLabelAndFieldConent(String fieldTitle,String value){
		if(value.equals("null") || value == null){
			value = "";
		}
		StringBuffer str = new StringBuffer();
		str.append("<div style='width:30%;float:left;text-align:right'>").append(fieldTitle).append("&nbsp;:&nbsp;</div>");
		str.append("<div style='width:70%;float:right;text-align:left;word-break:break-all;'>").append(value).append("</div>");
		return str.toString();
	}
	@Override
	public  String getBuninessOperationToolbar() {
		return null;
	}
	
	public  String getPart(String templateContextData) {
		return "";
	}
	
	public  String getSubSheet(String templateContextData) {
		return "";
	}
	
	public  String getOpinion() {
		return "";
	}
	
	private boolean isXml(String text)	{
		try {
			new SAXReader().read(new StringReader(text));
			return true;
		} catch (DocumentException e) {
//			log.info(e.getMessage(), e);
			return false;
		}
	}
	
	
}

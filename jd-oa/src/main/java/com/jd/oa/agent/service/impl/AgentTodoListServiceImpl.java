package com.jd.oa.agent.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.jd.oa.agent.dao.AgentTodoListDao;
import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.agent.service.AgentTodoListService;
import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.common.service.BaseServiceImpl;

@Component("agentTodoListService")
public class AgentTodoListServiceImpl extends BaseServiceImpl<AgentTodoList, String> implements AgentTodoListService {

	@Resource
	private AgentTodoListDao agentTodoListDao;
	
	@Override
	public BaseDao<AgentTodoList, String> getDao() {
		return agentTodoListDao;
	}

}

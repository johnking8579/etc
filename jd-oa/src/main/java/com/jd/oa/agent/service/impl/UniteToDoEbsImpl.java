package com.jd.oa.agent.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.internal.StringMap;
import com.jd.oa.agent.UniteToDoInterface;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.utils.StringUtils;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.form.design.style.FormStyleInterface;
import com.jd.oa.form.design.style.impl.FormStyle_4_Impl;
import com.jd.oa.form.execute.RuntimeComputeExpressManager;
import com.jd.oa.form.execute.RuntimeSubSheetFormUIComponent;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.form.util.HtmlModelFactory;
import com.jd.oa.form.util.RepleaseKey;
/**
 * 第三方系统待办
 * @author liubo
 *
 */
public class UniteToDoEbsImpl extends UniteToDoAbst{

	
	/**
	 * 主表信息
	 * @param businessObjectId
	 * @return
	 */
	@Override
	public String getPart(String templateContextJSONData){
		//从 jss服务器获取统一待办业务数据
		StringBuffer str = new StringBuffer();
		Map<String,Object> map = (Map<String, Object>) DataConvertion.json2Map(templateContextJSONData);
//		RequisitionDTO dto = new Gson().fromJson(templateContextJSONData, RequisitionDTO.class);
		if(map != null){
			Map<String,Object> stringMap = (Map<String,Object>)map.get("bussinessData");
			Iterator it = stringMap.entrySet().iterator();
			while(it.hasNext()){
				Entry<String,Object> entry = (Entry)it.next();
				String key = entry.getKey();
					//过滤子表数据
				if(key.toLowerCase().indexOf("businessdetail") == 0){
					continue;
				}
				
				String[] as = entry.getValue().toString().split(":", 2);
				str.append("<div style='width:49%;float:left;line-height:30px;height:40px;display:inline-table;' id='")
					.append(key).append("_div'>")
					.append(getItemLabelAndFieldConent(as[0], as.length > 1 ? as[1] : ""))
					.append("</div>");
			}
		}
		return str.toString();
	}
	@Override
	public String getFormTitle(String templateContextJSONData){
		Map<String,Object> map = (Map<String, Object>) DataConvertion.json2Map(templateContextJSONData);
		if(map != null && map.containsKey("reqName")){
			return map.get("reqName").toString();
		}
		return "";
	}
	/**
	 * 子表信息
	 * @return
	 */
	@Override
	public String getSubSheet(String templateContextJSONData){
		StringBuffer str = new StringBuffer();
		Map<String,Object> map = (Map<String, Object>) DataConvertion.json2Map(templateContextJSONData);
		if(map != null){
			Map<String,Object> stringMap = (Map<String,Object>)map.get("bussinessData");
			Iterator it = stringMap.entrySet().iterator();
			while(it.hasNext()){
				Entry<String,Object> entry = (Entry)it.next();
				String key = entry.getKey();
				if(key.toLowerCase().indexOf("businessdetail") == 0){//解析子表数据
					Map<String,Object> value = (Map<String,Object>)entry.getValue();
//					System.out.println(value);
					if(value == null){
						continue;
					}
					String subSheetTitle = value.get("subReqName").toString();//子表标题
					List<Map<String,String>> subSheetDatas = (ArrayList<Map<String,String>>)value.get("subData");
					
					str.append(getSubSheetBody(key, subSheetTitle, subSheetDatas)).append("\n");
					str.append("<br />");
				}
			}
		}
		return str.toString();
	}
	//定义子表
	/**
	 * 子表 定义
	 * @param formCode
	 * @param formId
	 * @param formName
	 * @param items
	 * @return
	 */
	private String getSubSheetBody(String formCode,String formName,List<Map<String,String>> subSheetDatas){
		StringBuffer sheetBody = new StringBuffer();
		//子表标题
		sheetBody.append("<table width=\"100%\" border=\"0\">\n");
		sheetBody.append("<tr>\n");
		sheetBody.append(" <td colspan=\"3\" align=\"left\">").append(formName).append("</td>\n");
		sheetBody.append("</tr>\n");
		sheetBody.append("</table>\n");
		
		
		sheetBody.append("<!--").append(formName).append(" 子表定义-->\n");
		sheetBody.append("<!-- end ").append(formName).append(" 子表定义-->\n");
		sheetBody.append("<!-- ").append(formName).append(" 子表script定义-->\n");
		sheetBody.append("<script type='text/javascript'>\n");
		//数据值
		sheetBody.append("$(document).ready(function () {\n");
		sheetBody.append("var ").append(formCode).append("_data = ").append(getColumnValueJsonData(subSheetDatas)).append(";\n");
		sheetBody.append("$('#").append(formCode).append("').datagrid({ \n");
		sheetBody.append("data:").append(formCode).append("_data,\n");
		sheetBody.append("singleSelect:true,\n");
		sheetBody.append("nowrap:false,\n");
		sheetBody.append("loadMsg:'',\n");
		//sheetBody.append("frozenColumns:[[\n");
		//sheetBody.append("                {field:'ck',checkbox:true}\n");
		//sheetBody.append("			]],\n");
		sheetBody.append("			fitColumns: false,\n");
		sheetBody.append("    columns: [[");
		//列定义
		if( subSheetDatas != null && subSheetDatas.size() > 0){//只循环一次，获取子表列名称
			StringBuffer sheetBodyColumn = new StringBuffer();
			Map<String,String> columnMaps = subSheetDatas.get(0);
			if(columnMaps != null){
				Iterator it = columnMaps.entrySet().iterator();
				while(it.hasNext()){
					Entry<String,Object> entry = (Entry)it.next();
					String[] as = entry.getValue().toString().split(":", 2);
					sheetBodyColumn.append("\n {field: \"").append(entry.getKey())
						.append("\", title: \"").append(as[0]).append("\",width:120},");
				}
			}
			
			if(sheetBodyColumn.length() > 0){
				sheetBodyColumn.setLength(sheetBodyColumn.length() - 1);
			}
			sheetBody.append(sheetBodyColumn);
		}
		sheetBody.append(" ]]\n");
		sheetBody.append("});\n");
//		sheetBody.append("//easyui表格滚动\n");
//		sheetBody.append("var dv2 = $(\".datagrid-view2\");\n");
//		sheetBody.append("dv2.children(\".datagrid-body\").html(\"<div style='width:1500px;border:solid 0px;height:1px;'></div>\");\n");
		sheetBody.append("});\n");
		sheetBody.append("</script>\n");
		sheetBody.append("<table style=\"height:auto;overflow:scroll;\" id=\"").append(formCode).append("\"></table>\n");
		sheetBody.append("<!-- end").append(formName).append("script定义-->\n");
		return sheetBody.toString();
	}
	/**
	 * 获取列值
	 * @param subSheetDatas
	 * @return
	 */
	private String getColumnValueJsonData(List<Map<String,String>> subSheetDatas){
		List<Map<String,String>> jsonData = new ArrayList<Map<String,String>>();
		//列定义
		if( subSheetDatas != null && subSheetDatas.size() > 0){//只循环一次，获取子表列名称
			for(int i = 0 ; i < subSheetDatas.size() ; i++){
				Map<String,String> columnMaps = subSheetDatas.get(i);
				if(columnMaps != null){
					Map<String,String> data = new HashMap<String,String>();
					Iterator it = columnMaps.entrySet().iterator();
					while(it.hasNext()){
						Entry<String,Object> entry = (Entry)it.next();
						String[] as = entry.getValue().toString().split(":", 2);
						data.put(entry.getKey(), as.length > 1 ? as[1] : "");
					}
					jsonData.add(data);
				}
			}
		}
		return DataConvertion.object2Json(jsonData);
	}
	/**
	 * 历史审核意见
	 * @return
	 */
	@Override
	public String getOpinion(){
		return "";
	}
	/**
	 * 按钮定义
	 * @return
	 */
	@Override
	public String getBuninessOperationToolbar() {
		
		// TODO Auto-generated method stub
		return "";
	}
	
}

package com.jd.oa.agent.service;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 此处必须添加@WebServer注解，若不添加，则发布的服务当中无任何方法
 * @author wds
 *
 */
@WebService
public interface MqProduceRemedyWebService {
	public String mqProducer(@WebParam(name="textMessage") String textMessage);
}

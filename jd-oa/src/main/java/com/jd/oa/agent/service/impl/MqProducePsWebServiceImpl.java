package com.jd.oa.agent.service.impl;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jd.oa.agent.service.MqProducePsWebService;
import com.jd.oa.agent.service.UnifyAgentReceiveService;

/**
 * 服务实现类
 * @author wds
 *
 */
@WebService(endpointInterface="com.jd.oa.agent.service.MqProducePsWebService",
			serviceName="MqProducePsWebService",
			targetNamespace="http://service.agent.oa.jd.com")
public class MqProducePsWebServiceImpl implements MqProducePsWebService {
	
	static Logger log = Logger.getLogger(MqProducePsWebServiceImpl.class);
	
	@Autowired
	private UnifyAgentReceiveService unifyAgentReceiveService;
	
	@Override
	public String mqProducer(String text) {
		try	{
			unifyAgentReceiveService.receive(text);
			return "success";
		} catch(Exception e)	{
			log.error(text + "\n" + e.getMessage(), e);
			return "error";
		}
	}
}
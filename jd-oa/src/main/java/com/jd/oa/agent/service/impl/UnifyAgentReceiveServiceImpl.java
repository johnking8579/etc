package com.jd.oa.agent.service.impl;

import com.google.gson.*;
import com.jd.oa.agent.dao.AgentBizDataDao;
import com.jd.oa.agent.dao.AgentTodoListDao;
import com.jd.oa.agent.model.AgentBizData;
import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.agent.service.*;
import com.jd.oa.api.service.MqProduceJmeService;
import com.jd.oa.app.dao.ProcessTaskHistoryDao;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.process.dao.ProcessDefDao;
import com.jd.oa.process.dao.ProcessDefinitionConfigDao;
import com.jd.oa.process.dao.ProcessInstanceDao;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.system.dao.SysUserDao;
import com.jd.oa.system.model.SysUser;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jd.oa.common.constant.SystemConstant.*;


@Component("unifyAgentReceiveService")
@Transactional
public class UnifyAgentReceiveServiceImpl implements UnifyAgentReceiveService {
	
	private static Logger logger = Logger.getLogger(UnifyAgentReceiveServiceImpl.class);
	private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

	@Resource
	private JssService jssService;
	@Resource
	private ProcessDefinitionConfigDao processDefConfDao;
	@Resource
	private ProcessTaskHistoryDao processTaskHistoryDao;
	@Resource
	private AgentTodoListDao agentTodoListDao;
	@Resource
	private AgentBizDataDao agentBizDataDao;
	@Resource
	private ProcessInstanceDao processInstanceDao;
	@Resource
	private ProcessDefDao processDefDao;
	@Resource
	private SysUserDao userDao;
	@Autowired
	private MqProduceJmeService mqProduceJmeService;
	
	@Override
	public void receive(String text)	{
		logger.info("[统一待办-接受信息:]"+text);
		text = text.trim();
		try {
			Document doc = new SAXReader().read(new StringReader(text));
			if(doc.getRootElement().element("businessData") != null)	{
				receiveRequisition(RequisitionDTO.fromXml(doc), text, true);
			} else if(doc.getRootElement().element("tasks") != null)	{
				receiveTodoList(TodoListDTO.fromXml(doc));
			} else if(doc.getRootElement().element("processStatus") != null)	{
				receiveApprovalResult(ApprovalResultDTO.fromXml(doc));
			} else	{
				throw new UnsupportedOperationException("不识别的xml数据定义:" + text);
			}
		} catch (DocumentException e) {
			JsonObject json = new JsonParser().parse(text).getAsJsonObject();
			if(json.get("bussinessData") != null)	{
				receiveRequisition(gson.fromJson(text, RequisitionDTO.class), text, false);
			} else if(json.get("tasks") != null)	{
				receiveTodoList(gson.fromJson(text, TodoListDTO.class));
			} else if(json.get("processStatus") != null)	{
				receiveApprovalResult(gson.fromJson(text, ApprovalResultDTO.class));
			} else	{
				throw new UnsupportedOperationException("不识别的json数据定义:" + text);
			}
		}
	}
	
	/**
	 * 插入申请任务
	 * @param dto 申请任务pojo
	 * @param text 消息体
	 * @param isXml 是否为xml
	 */
	private void receiveRequisition(RequisitionDTO dto, String text, boolean isXml) {
		SysUser requester = userDao.getByUserName(dto.getReqUserErp(), null);
		if(requester == null)	
			throw new IllegalArgumentException("reqUserErp对应的记录不存在:" + dto.getReqUserErp());
		ProcessDefinition def = processDefDao.findByPafDefId(dto.getProcesskey());
		if(def == null)
			throw new IllegalArgumentException("processkey对应的记录不存在:" + dto.getProcesskey());
		
		ProcessInstance existInstance = processInstanceDao.findByInstanceId(
											join(dto.getProcesskey(), dto.getReqId()));

		if (existInstance == null) {
			String bizId = (isXml ? insertBizDataWithXml(text, def) : insertBizDataWithJson(text, def));
			logger.info("[统一待办-上传消息到JSS]:bizId=" + bizId + ",requester=" + requester.getUserName());
			jssService.uploadFile(BUCKET, bizId, text); //上传消息体至云存储
			insertProcessInstance(dto, bizId, def, requester.getId());
			insertTaskHistory(dto, def.getPafProcessDefinitionId(), requester.getId());
			if(dto.getTasks() != null)	{
				insertTodoList(dto);
			}
		} else {
			if (dto.getTasks() != null && !dto.getTasks().isEmpty()) {
				insertTodoList(dto);
			} else {
				existInstance.setStatus(PROCESS_STATUS_5); 		// 更新为完成
				existInstance.setEndTime(new Date());
				processInstanceDao.update(existInstance);
			}
		}
	}
	
	
	/**
	 * 插入业务数据
	 * @param json
	 * @param def
	 * @return
	 */
	private String insertBizDataWithJson(String json, ProcessDefinition def)	{
		JsonObject j = new JsonParser().parse(json).getAsJsonObject();
		
		List<ProcessDefinitionConfig> confs = processDefConfDao.findConfigItem(def.getId());
		AgentBizData biz = new AgentBizData();
//		biz.setId(id);		//baseDao处理
		biz.setJssBucket(BUCKET);
		for(int i=0; i<confs.size(); i++)	{
			String[] configItem = confs.get(i).getConfigItem().split(":");		//configItem格式为<<中文列1:col1>>
			if(configItem.length == 1)
				throw new BusinessException("ProcessDefinitionConfig.configItem应当使用分号分隔");
			
			JsonElement targetVal = j.get("bussinessData").getAsJsonObject().get(configItem[1]);
			if(targetVal == null)
				throw new IllegalArgumentException("json中未找到配置的业务字段:" + configItem[1]);
			
			try {
				AgentBizData.class.getMethod("setCol" + (i+1), String.class)
					.invoke(biz, targetVal.getAsString());
			} catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		return agentBizDataDao.insert(biz);
	}
	
	/**
	 * 插入业务数据
	 * @param def
	 * @return
	 */
	private String insertBizDataWithXml(String xml, ProcessDefinition def)	{
		Element bussinessData;
		try {
			bussinessData = new SAXReader().read(new StringReader(xml)).getRootElement().element("businessData");
		} catch (DocumentException e) {
			throw new BusinessException(e);
		}
		
		List<ProcessDefinitionConfig> confs = processDefConfDao.findConfigItem(def.getId());
		AgentBizData biz = new AgentBizData();
//		biz.setId(id);		//baseDao处理
		biz.setJssBucket(BUCKET);
		for(int i=0; i<confs.size(); i++)	{
			String[] configItem = confs.get(i).getConfigItem().split(":");		//configItem格式为<<中文列1:col1>>
			if(configItem.length == 1)
				throw new BusinessException("ProcessDefinitionConfig.configItem应当使用分号分隔");
			
			Element targetEle = (Element)bussinessData.selectObject("column[@name='" + configItem[1] + "']");
			if(targetEle == null)
				throw new IllegalArgumentException("json中未找到配置的业务字段:" + configItem[1]);
			
			try {
				AgentBizData.class.getMethod("setCol" + (i+1), String.class)
					.invoke(biz, targetEle.attributeValue("title") + ":" + targetEle.getStringValue());
			} catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		return agentBizDataDao.insert(biz);
	}
	
	/**
	 * 插入流程实例
	 * @param dto
	 * @param bizInstanceId
	 * @param def
	 * @param starterId
	 */
	private void insertProcessInstance(RequisitionDTO dto, String bizInstanceId, 
										ProcessDefinition def, String starterId)	{
		ProcessInstance pi = new ProcessInstance();
//		pi.setId(id);	//basedao处理
		pi.setBusinessInstanceId(bizInstanceId);			// bizdataid
		pi.setProcessDefinitionId(def.getId());		// from process_def
		pi.setProcessDefinitionName(def.getProcessDefinitionName());// 同上
		pi.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));		
		pi.setProcessInstanceName(dto.getReqName());	
		pi.setStarterId(starterId);
//		pi.setPriority(priority);		//不存
		pi.setBeginTime(dto.getReqTime());
//		pi.setEndTime(endTime);			//不存
		pi.setStatus(PROCESS_STATUS_1);		//1审批中, 0草稿
		pi.setLastTaskTime(dto.getReqTime());
//		pi.setYn(0);					//basedao处理
//		pi.setFollowCode(followCode);	//不存
		processInstanceDao.insert(pi);
		logger.info("[统一待办-创建流程实例]:processInstance="+JsonUtils.toJsonByGoogle(pi));
	}
	
	
	/**
	 * 插入流程任务历史
	 * @param dto
	 * @param pafDefId
	 * @param userId
	 */
	private void insertTaskHistory(RequisitionDTO dto, String pafDefId, String userId)	{
		ProcessTaskHistory his = new ProcessTaskHistory();
//		his.setId(id);		//basedao处理
		his.setPafProcessDefinitionId(pafDefId);
		his.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
//		his.setInstanceIscomplete(instanceIscomplete);		//不存
//		his.setNodeId(nodeId);		//不存
//		his.setTaskId(taskId);		//不存
		his.setTaskType(TASK_KUBU_OWNER);
		his.setTaskName("提交申请");
		his.setComment(dto.getComments());
		his.setResult("0");			//0申请
		his.setBeginTime(dto.getReqTime());		// 申请时间
		his.setEndTime(dto.getReqTime());		// 申请时间
//		his.setAssignerId(assignerId);			//不存
		his.setUserId(userId);			//申请人ID
//		his.setYn(yn);	//basedao处理
		processTaskHistoryDao.insert(his);
		logger.info("[统一待办-插入历史记录]:history="+JsonUtils.toJsonByGoogle(his));
	}
	
	
	/**
	 * 插入待办任务
	 * @param dto
	 */
	private void insertTodoList(RequisitionDTO dto)	{
		for (TaskDTO task : dto.getTasks()) {
			String[] candidators = task.getCandidatUsers().split(",");
			for(String candidator : candidators)	{
				AgentTodoList todo = new AgentTodoList();
				todo.setProcessKey(dto.getProcesskey());// json
				todo.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
				todo.setTaskId(task.getTaskId());		// json
				todo.setTaskName(task.getTaskName());	// json
				todo.setCandidateUsers(candidator);// 多个待办 人
				todo.setTaskStatus("0");		// 0待办, 1已办
				todo.setCreator("receiveRequisition接口");
				Date createTime = new Date();
				todo.setCreateTime(createTime);
				agentTodoListDao.insert(todo); 	// 插入统一待办任务
				logger.info("[统一待办-插入待办任务]:" + JsonUtils.toJsonByGoogle(dto));
				//向JME推送信息
				sendMessage2Jme(task, join(dto.getProcesskey(), dto.getReqId()), dto.getProcesskey(), candidator, dto.getComments(), createTime);
				
			}
		}
	}
	
	/**
	 * 通过MQ向JME推送信息
	 * 
	 * @param task
	 * @param processInstanceId
	 * @param processKey
	 * @param candidator
	 * @param comments
	 * @param createTime
	 */
	private void sendMessage2Jme(TaskDTO task,String processInstanceId,String processKey,
			String candidator,String comments,Date createTime){
		ProcessInstance processInstance = processInstanceDao.findByInstanceId(processInstanceId);
		if(null != processInstance){
			//得到流程申请人
			String starterId = processInstance.getStarterId();
			SysUser user = userDao.get(starterId);
			
			//申请单ID
			String reqId = processInstance.getBusinessInstanceId();
			
			//流程名称
			String processName = processInstance.getProcessDefinitionName();
			
			//申请单主题
			String reqName = processInstance.getProcessInstanceName();
			
			//申请时间
			String beginTime = DateUtils.datetimeFormat24(new Timestamp(processInstance.getBeginTime().getTime()));
			
			//处理消息Json串
	    	Map jsonMap = new HashMap();
	    	jsonMap.put("processKey", processKey);                              //流程key
	    	jsonMap.put("processName", processName);                            //流程名称
	    	jsonMap.put("reqId", reqId);                                        //申请单Id
	    	jsonMap.put("taskId", task.getTaskId());                            //待办任务Id
	    	jsonMap.put("taskName", task.getTaskName());                        //待办任务名称
	    	jsonMap.put("assignee", candidator);                                //待办人
	    	jsonMap.put("reqName", reqName);                                    //申请单主题
	    	if(null != user.getUserName() && !user.getUserName().equals("")){
	    		jsonMap.put("reqUserErp", user.getUserName());                  //申请人ERP
	    		jsonMap.put("reqUserName", user.getRealName());                 //申请人姓名
	    	}else{
	    		jsonMap.put("reqUserErp", "");
	    		jsonMap.put("reqUserName", "");
	    	}
	    	jsonMap.put("comments", comments);                                  //申请备注
	    	jsonMap.put("reqTime", beginTime);                                  //申请时间
	    	String startTime = DateUtils.datetimeFormat24(new Timestamp(createTime.getTime()));
	    	jsonMap.put("startTime", startTime);                                //任务到达时间
	    	String textMessage = DataConvertion.object2Json(jsonMap);
	    	//mqProduceJmeService.mqProducer(textMessage);
			logger.info("[统一待办-向JDME发送MQ消息]:" + JsonUtils.toJsonByGoogle(textMessage));
		}
		
	}

	/**
	 * 处理申批结果
	 * @param dto
	 */
	private void receiveApprovalResult(ApprovalResultDTO dto) {
		logger.info("[统一待办-处理审批结果]:" + JsonUtils.toJsonByGoogle(dto));
		ProcessDefinition procDef = processDefDao.findByPafDefId(dto.getProcesskey());
		if(procDef == null)
			throw new IllegalArgumentException("processkey对应的记录不存在:" + dto.getProcesskey());
		SysUser submiter = userDao.getByUserName(dto.getSubmitUserErp(), null);
		if(submiter == null)
			throw new IllegalArgumentException("submitUserErp对应的记录不存在:" + dto.getSubmitUserErp());
		
		ProcessInstance existInstance = processInstanceDao.findByInstanceId(join(dto.getProcesskey(), dto.getReqId()));
		if(existInstance == null)
			throw new IllegalArgumentException("processkey-reqId对应的processInstance不存在: " + join(dto.getProcesskey(), dto.getReqId()));
		if("1".equals(dto.getProcessStatus()))	{		//不为1时不需要改流程实例状态
			existInstance.setStatus(PROCESS_STATUS_5);
			existInstance.setEndTime(new Date());
			if(dto.getSubmitResult() == 4)	{
				existInstance.setStatus(PROCESS_STATUS_4);
				existInstance.setEndTime(new Date());
			}
			processInstanceDao.update(existInstance);
			logger.info("[统一待办-处理审批结果成功，更新流程实例状态]:" + JsonUtils.toJsonByGoogle(existInstance));
		}
		
		//修改待办任务状态, 根据taskId和processInstanceId
		AgentTodoList entity = new AgentTodoList();
    	entity.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
    	entity.setTaskId(dto.getTaskId());
    	List<AgentTodoList> atlList = agentTodoListDao.find(entity);
    	for (AgentTodoList a : atlList) {
    		a.setTaskStatus("1");
    		agentTodoListDao.update(a);
			logger.info("[统一待办-处理审批结果成功，更新待办任务状态]:" + JsonUtils.toJsonByGoogle(a));
    	}
		
		ProcessTaskHistory his = new ProcessTaskHistory();
//		his.setId(IdUtils.uuid2());		//basedao处理
		his.setPafProcessDefinitionId(procDef.getPafProcessDefinitionId());
		his.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
//		his.setInstanceIscomplete(instanceIscomplete);		//不存
//		his.setNodeId(nodeId);		//不存
		his.setTaskId(dto.getTaskId());		//不存
		his.setTaskType(TASK_KUBU_OWNER);	//不存
		
		//查询已存的待办任务,获取第一个任务名
		AgentTodoList param = new AgentTodoList();
		param.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
		param.setTaskId(dto.getTaskId());
		List<AgentTodoList> list = agentTodoListDao.find(param);
		if(!list.isEmpty())	{
			his.setTaskId(list.get(0).getTaskId());	
			his.setTaskName(list.get(0).getTaskName());	
		}
		
		his.setComment(dto.getSubmitComments());
		his.setResult(this.toProcTaskHistoryResult(dto.getSubmitResult()));			
		his.setBeginTime(dto.getSubmitTime());		// 申请时间
		his.setEndTime(dto.getSubmitTime());		// 申请时间
//		his.setAssignerId(assignerId);				//不存
		his.setUserId(submiter.getId());			//申请人或审批人ID
//		his.setYn(yn);	//basedao处理
		processTaskHistoryDao.insert(his);
		logger.info("[统一待办-处理审批结果成功，插入历史库历史记录]:" + JsonUtils.toJsonByGoogle(his));
	}
	
	/**
	 * 把外部系统提交的submitResult转换成processTaskHistory的result值
	 * @param submitResult 1批准, 2拒绝, 3驳回, 4取消, 5归档
	 * @return ("0","提交申请").replace("1","批准").replace("2","拒绝").replace("3","驳回").replace("4","重新申请 ").replace("5","加签审批 ").replace("6","加签 ")
	 */
	private String toProcTaskHistoryResult(int submitResult)	{
		switch(submitResult)	{
			case 1:return "1";
			case 2:return "2";
			case 3:return "3";
			case 4:return "取消";
			default:return "";
		}
	}
	
	/**
	 * 处理待办任务
	 * @param dto
	 */
	private void receiveTodoList(TodoListDTO dto) {
		ProcessInstance existInstance = processInstanceDao.findByInstanceId(join(dto.getProcesskey(), dto.getReqId()));
		if(existInstance == null)	{
			throw new IllegalArgumentException("processkey-reqId对应的processInstance不存在: " + join(dto.getProcesskey(), dto.getReqId()));
		}
		
		for(TaskDTO task : dto.getTasks())	{
			String[] candidators = task.getCandidatUsers().split(",");
			for(String candidator : candidators)	{
				AgentTodoList todo = new AgentTodoList();
				todo.setProcessKey(dto.getProcesskey());// from json
				todo.setProcessInstanceId(join(dto.getProcesskey(), dto.getReqId()));
				todo.setTaskId(task.getTaskId());		// from json
				todo.setTaskName(task.getTaskName());	// from json
				todo.setCandidateUsers(candidator);// 多个待办 人
				todo.setTaskStatus("0");		// 0待办, 1已办
				todo.setCreator("receiveTodoList接口");
				Date createTime = new Date();
				todo.setCreateTime(createTime);
				agentTodoListDao.insert(todo);
				logger.info("[统一待办-插入待办任务]:todo="+JsonUtils.toJsonByGoogle(todo));
				//向JME推送信息
				sendMessage2Jme(task, join(dto.getProcesskey(), dto.getReqId()), dto.getProcesskey(), candidator, "", createTime);
			}
		}
		
	}
	
	/**
	 * processInstanceId的拼接策略
	 * @param processkey
	 * @param reqId
	 * @return
	 */
	private String join(String processkey, String reqId)	{
		return processkey + "-" + reqId;
	}
	
}

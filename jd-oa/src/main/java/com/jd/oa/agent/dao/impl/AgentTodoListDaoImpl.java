package com.jd.oa.agent.dao.impl;

import org.springframework.stereotype.Component;

import com.jd.oa.agent.dao.AgentTodoListDao;
import com.jd.oa.agent.model.AgentTodoList;
import com.jd.oa.common.dao.MyBatisDaoImpl;

@Component("agentTodoListDao")
public class AgentTodoListDaoImpl extends MyBatisDaoImpl<AgentTodoList,String> implements AgentTodoListDao{


}

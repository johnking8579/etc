package com.jd.oa.agent.service.impl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.webservice.DataConvertion;

public class UniteToDoRemdyImpl extends UniteToDoAbst{
	/**
	 * 主表部分数据
	 */
	@Override
	public String getPart(String templateContextJSONData){
		Document doc = this.read(templateContextJSONData);
		return parse(doc);
	}
	/**
	 * 子表数据
	 */
	@Override
	public String getSubSheet(String templateContextData){
		return "";
	}
	@Override
	public String getOpinion(){
		return "";
	}
	
	private Document read(String templateContextData){
		SAXReader saxReader = new SAXReader();
		try {
			return saxReader.read(new StringReader(templateContextData));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public String getFormTitle(String templateContextJSONData){
		Document doc = this.read(templateContextJSONData);
		Element root = doc.getRootElement();
		if(root == null){
			throw new BusinessException("表单数据解析失败");
		}
		StringBuffer str = new StringBuffer();
		if(root.element("reqName") != null){
			Element element = root.element("reqName");
			return element.getText();
		}
		return "";
	}
	/**
	 * 解析主表
	 * @param doc
	 * @return
	 */
	private String parse(Document doc){
		Element root = doc.getRootElement();
		if(root == null){
			throw new BusinessException("表单数据解析失败");
		}
		StringBuffer str = new StringBuffer();
		if(root.element("businessData") != null){
			Element element = root.element("businessData");
			Iterator it = element.elementIterator();
			while(it.hasNext()){
				Element columnElement = (Element)it.next();
				if(columnElement != null){
					String value = columnElement.getText();
					String columnName = columnElement.attributeValue("name");
					String title = columnElement.attributeValue("title");
					str.append("<div style='width:49%;float:left;line-height:30px;height:40px;display:inline-table;border:0px solid red;' id='")
						.append(columnName).append("_div'>");
					str.append(getItemLabelAndFieldConent(title,value));
					str.append("</div>");
				}
			}
		}
		return str.toString();
	}
	/**
	 * <!-- 子表数据-->
	<businessDetails>
		<businessDetail>
		 <column name="c1" title="采购类型1" />
		 <column name="c2" title="采购类型1" />
		 <column name="c3" title="采购类型1" />
		 <datas>
			<data>
				<column name="c1"><![CDATA[外采1]]></column>
				<column name="c2"><![CDATA[外采1]]></column>
				<column name="c3"><![CDATA[外采1]]></column>
			</data>
		 </datas>
		</businessDetail>
	</businessDetails>
	 * @param doc
	 * @return
	 */
	private String parseSubSheet(Document doc){
		Element root = doc.getRootElement();
		if(root == null){
			throw new BusinessException("表单数据解析失败");
		}
		StringBuffer str = new StringBuffer();
		List<Map<String,String>> subSheetDatas = new ArrayList<Map<String,String>>();
		if(root.element("businessDetails") != null){
			Element element = root.element("businessDetails");
			Iterator it = element.elementIterator();
			while(it.hasNext()){
				Element businessDetailElement = (Element)it.next();
				if(businessDetailElement != null){
					if(businessDetailElement.getName().equals("businessDetail")){
						Iterator columnIt = element.elementIterator();
						while(columnIt.hasNext()){
							Element columnElement = (Element)it.next();
							if(columnElement != null){
								String columnName = columnElement.attributeValue("name");
								String title = columnElement.attributeValue("title");
								Map<String,String> map = new HashMap<String,String>();
								map.put(columnName, title);
								subSheetDatas.add(map);
							}
						}
					}
				}
			}
		}
		return "";
	}
}

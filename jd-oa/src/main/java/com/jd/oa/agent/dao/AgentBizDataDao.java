package com.jd.oa.agent.dao;

import com.jd.oa.agent.model.AgentBizData;
import com.jd.oa.common.dao.BaseDao;

public interface AgentBizDataDao extends BaseDao<AgentBizData, String>{


}

package com.jd.oa.agent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.common.utils.SpringContextUtils;
/**
 * oa 审批页面
 * @author liubo
 *
 */
public class OaUniteToDoImpl implements UniteToDoInterface{
	private ApplicationService applicationService;
	@Override
	public String getExecuteWorklistBindReportFormContext(
			HttpServletRequest request,HttpServletResponse response, String processDefinitionKey,
			String businessObjectId, String processNodeId,
			String processInstanceId, String processInstanceStatus) throws Exception {
		if(this.applicationService == null){
			this.applicationService = SpringContextUtils.getBean(ApplicationService.class);
		}
		return this.applicationService.getExecuteWorklistBindReportFormContext(request, processDefinitionKey, businessObjectId, processNodeId, processInstanceId, processInstanceStatus);
	}
	@Override
	public String getBuninessOperationToolbar() {
		// TODO Auto-generated method stub
		if(this.applicationService == null){
			this.applicationService = SpringContextUtils.getBean(ApplicationService.class);
		}
		return this.applicationService.getBuninessOperationToolbar();
	}
	
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-14
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.agent.service;

import com.jd.oa.agent.model.AgentBizData;
import com.jd.oa.common.service.BaseService;

public interface AgentBizDataService extends BaseService<AgentBizData, String> {
}

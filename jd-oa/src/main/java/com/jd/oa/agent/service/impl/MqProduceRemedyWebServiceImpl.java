package com.jd.oa.agent.service.impl;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jd.oa.agent.service.MqProduceRemedyWebService;
import com.jd.oa.agent.service.UnifyAgentReceiveService;


/**
 * 服务实现类
 * @author wds
 *
 */
@WebService(endpointInterface="com.jd.oa.agent.service.MqProduceRemedyWebService",
			serviceName="MqProduceRemedyWebService",
			targetNamespace="http://service.agent.oa.jd.com/")
public class MqProduceRemedyWebServiceImpl implements MqProduceRemedyWebService {
	
	static Logger log = Logger.getLogger(MqProduceRemedyWebServiceImpl.class);
	
	@Autowired
	private UnifyAgentReceiveService unifyAgentReceiveService;
	
	@Override
	public String mqProducer(String text) {
		try	{
			unifyAgentReceiveService.receive(text);
			return "success";
		} catch(Exception e)	{
			log.error(text + "\n" + e.getMessage(), e);
			return "error";
		}
	}
	

}
package com.jd.oa.agent.service;

import java.io.StringReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.jd.oa.common.exception.BusinessException;

/**
 * 申请单. 用来反序列化json. 不序列化businessData
 * {
Processkey:  //流程key,
ReqId:  //申请单ID,
reqUserErp:  //申请人erp,
reqUserName:  //申请人姓名,
reqTime:  //申请时间(yyyy-MM-dd hh:mm:ss),
comments:  //申请备注,
bussinessData:  //业务数据
{c1: ,c2: ,
bussinessDetail1:   //业务数据明细1
[{d1: ,d2: },{d1: ,d2: }],
bussinessDetail2:   //业务数据明细2
[{d1: ,d2: },{d1: ,d2: }]
},
tasks:  
[{taskId:  //待办任务ID,
taskName:  //待办任务名称,
candidatUsers:  //待办人
},
{taskId:  //待办任务ID,
taskName:  //待办任务名称,
candidatUsers:  //待办人
}]
}
 * @copyright Copyright 2014-2020 JD.COM All Right Reserved
 * @see 
 * @author 荆营	部门:综合职能研发部
 * @version 1.0
 * @date 2014年6月27日
 */
public class RequisitionDTO {
	private String processkey;
	private String reqId;
	private String reqName;
	private String reqUserErp;
	private String reqUserName;
	private String comments;
	private List<TaskDTO> tasks;
	private Timestamp reqTime;
	
	/**
	 * 从xml中生成对象
	 * @param xml
	 * @return
	 */
	public static RequisitionDTO fromXml(Document xml)	{
		Element root = xml.getRootElement();
		RequisitionDTO dto = new RequisitionDTO();
//		dto.setBussinessData(bussinessData);	//不处理
		dto.setComments(root.elementText("comments"));
		dto.setProcesskey(root.elementText("processKey"));
		dto.setReqId(root.elementText("reqId"));
		dto.setReqName(root.elementText("reqName"));
		try {
			Date reqTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(root.elementText("reqTime"));
			dto.setReqTime(new Timestamp(reqTime.getTime()));
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
		dto.setReqUserErp(root.elementText("reqUserErp"));
		dto.setReqUserName(root.elementText("reqUserName"));
		
		if(root.element("tasks") != null)	{
			List<Element> tasks = root.element("tasks").elements("task");
			List<TaskDTO> taskDTOs = new ArrayList<TaskDTO>();
			for(Element e : tasks)	{
				TaskDTO task = new TaskDTO();
				task.setCandidatUsers(e.elementText("candidatUsers"));
				task.setTaskId(e.elementText("taskId"));
				task.setTaskName(e.elementText("taskName"));
				taskDTOs.add(task);
			}
			dto.setTasks(taskDTOs);
		}
		return dto;
	}
	
	public String getProcesskey() {
		return processkey;
	}
	public void setProcesskey(String processkey) {
		this.processkey = processkey;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getReqUserErp() {
		return reqUserErp;
	}
	public void setReqUserErp(String reqUserErp) {
		this.reqUserErp = reqUserErp;
	}
	public String getReqUserName() {
		return reqUserName;
	}
	public void setReqUserName(String reqUserName) {
		this.reqUserName = reqUserName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<TaskDTO> getTasks() {
		return tasks;
	}
	public void setTasks(List<TaskDTO> tasks) {
		this.tasks = tasks;
	}
	public Date getReqTime() {
		return reqTime;
	}
	public void setReqTime(Timestamp reqTime) {
		this.reqTime = reqTime;
	}
	public String getReqName() {
		return reqName;
	}
	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	
	private Map<String,Object> bussinessData;
	public Map<String,Object> getBussinessData() {
		return bussinessData;
	}
	public void setBussinessData(Map<String,Object> bussinessData) {
		this.bussinessData = bussinessData;
	}
	
}

package com.jd.oa.app.service;

import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.common.service.BaseService;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-6-16 下午10:19:09
 *
 */
public interface ProcessTaskTimerService extends BaseService<ProcessTaskTimer,String> {

}

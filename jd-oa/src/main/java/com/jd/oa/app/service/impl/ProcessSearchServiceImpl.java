package com.jd.oa.app.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.reflect.TypeToken;
import com.jd.common.web.LoginContext;
import com.jd.oa.app.dao.ProcessSearchDao;
import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.RelationalOperator;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.service.BaseServiceImpl;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.dao.ProcessAddsignerDao;
import com.jd.oa.process.dao.ProcessDefDao;
import com.jd.oa.process.dao.ProcessProxyDao;
import com.jd.oa.process.dao.ProcessSuperviseDao;
import com.jd.oa.process.model.ProcessAddsigner;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessProxy;
import com.jd.oa.process.model.ProcessSupervise;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

/** 
 * @Description: 前台-申请查询Service实现
 * @author guoqingfu
 * @date 2013-10-11 上午11:14:32 
 * @version V1.0 
 */

@Service("processSearchService")
public class ProcessSearchServiceImpl extends BaseServiceImpl<ProcessSearch, String>
		implements ProcessSearchService {

    private static final Logger logger = Logger.getLogger(ProcessSearchServiceImpl.class);

	@Autowired
	private ProcessSearchDao processSearchDao;
	@Autowired
	private OaPafService oaPafService;
	@Autowired
	private ProcessDefDao processDefDao;
	@Autowired
	private ProcessProxyDao processProxyDao;
	@Autowired
	private ProcessAddsignerDao processAddsignerDao;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private UserAuthorizationService userAuthorizationService;
	@Autowired
	private ProcessSuperviseDao processSuperviseDao;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private ProcessTaskHistoryService processTaskHistoryService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private FormService formService;
	@Autowired
	private SysOrganizationService sysOrganizationService;
	@Autowired
	private ProcessDefinitionConfigService processDefinitionConfigService;
	@Autowired
	private FormItemService formItemService;
	
	public ProcessSearchDao getDao() {
		return processSearchDao;
	}
	
	/**
     * @Description: 通过类别ID查询流程定义信息（前台用）
      * @param processTypeId
      * @return List<ProcessDefinition>
      * @author guoqingfu
      * @date 2013-10-11下午12:04:43 
      * @version V1.0
     */
    public List<ProcessSearch> findPrcocessListByTypeId(String processTypeId){
    	try{
    		return processSearchDao.findPrcocessListByTypeId(processTypeId);
    	}catch(Exception e){
    		 logger.error(e);
             throw new BusinessException("通过类别ID查询流程定义信息失败",e);
    	}
    }
    
	@Override
	public List<ProcessSearch> findByType1(String processType1Id) {
		return processSearchDao.findByType1(processType1Id);
	}

    /**
     * 
     * @desc 通过流程实例获得流程定义相关信息
     * @author WXJJ
     * @date 2013-10-14 下午02:51:37
     *
     * @param processInstance
     * @return
     */
	public ProcessDefinition findProcessDefByInstance(String processInstance) {
		// TODO Auto-generated method stub
		return processDefDao.findProcessDefByInstance(processInstance);
	}
	
    /**
     * @Description: 查询我的申请流程详细信息
     * @param erpId 流程发起人
     * @param status 查询多个状态用,分割
     * @return List<ProcessAndTaskInfoDto>
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessAndTaskInfoDto> getMyApplyProTaskInfo(String erpId) {
    	List<ProcessAndTaskInfoDto> result = null;
    	try {
    		//查询当前用户发起的所有活动的流程
    		List<com.jd.oa.process.model.ProcessInstance> listProInstance = this.oaPafService.getMyProcessInstanceOther(erpId);
    		
    		result = new ArrayList<ProcessAndTaskInfoDto>();
    		ProcessAndTaskInfoDto bean = null;
    		if(listProInstance != null) {
    			String instanceId = null;
    			for(ProcessInstance processInstance:listProInstance){
    				bean = new ProcessAndTaskInfoDto();
    				instanceId = processInstance.getProcessInstanceId();
    				bean.setBusinessInstanceId(processInstance.getBusinessInstanceId());//业务实例Id
    				bean.setProcessDefinitionId(processInstance.getProcessDefinitionId());//流程定义Id
    				bean.setProcessInstanceId(instanceId);//流程实例Id
    				bean.setProcessInstanceName(processInstance.getProcessInstanceName());//流程名称流程申请主题
					bean.setFollowCode(processInstance.getFollowCode()); //流水号
    				bean.setBeginTime(DateUtils.datetimeFormat(processInstance.getBeginTime()));//流程申请时间
    				bean.setCurrentStatus(processInstance.getStatus());//流程状态
    				bean.setProcessDifinitionName(processInstance.getProcessDefinitionName());//流程名称
    				bean.setProcessDifinitionKey(processInstance.getPafProcessDefinitionId());//流程对应的Paf key
    				
    				bean.setOaTaskInstanceList(processInstance.getTasks());
    				bean.setPafProcessDefinitionId(processInstance.getPafProcessDefinitionId());
    				
    				//当前节点
    				bean.setCurrentStep(processInstance.getCurrentStep());
    				
    				if(processInstance.getTasks().size() == 1){
    					//当前节点是首节点
    					bean.setFirstNode(true);
    				} else {
    					bean.setFirstNode(false);
    				}
    				
    				result.add(bean);
    			}
    		}
    		
    		//获取草稿状态的流程
    		String userId = this.sysUserService.getUserIdByUserName(erpId, SystemConstant.ORGNAIZATION_TYPE_HR);
    		List<String> status = new ArrayList<String>(1);
    		status.add("0");
    		List<ProcessInstance> draftProcessIns = this.processSearchDao.selectProInsByStaIdAndStatus(userId, status);
    		if(draftProcessIns != null && draftProcessIns.size() > 0){
    			for(ProcessInstance processInstance:draftProcessIns){
    				bean = new ProcessAndTaskInfoDto();
					bean.setProcessDefinitionId(processInstance.getProcessDefinitionId());
    				bean.setBusinessInstanceId(processInstance.getBusinessInstanceId());//业务实例Id
    				bean.setProcessInsPrimayKey(processInstance.getId());//流程实例主键
    				bean.setProcessInstanceName(processInstance.getProcessInstanceName());//流程名称流程申请主题
					bean.setFollowCode(processInstance.getFollowCode()); //流水号
    				bean.setBeginTime(DateUtils.datetimeFormat(processInstance.getBeginTime()));//流程申请时间
    				bean.setCurrentStatus(processInstance.getStatus());//流程状态
    				bean.setProcessDifinitionName(processInstance.getProcessDefinitionName());
    				bean.setPafProcessDefinitionId(processInstance.getPafProcessDefinitionId());
    				
    				//当前节点
    				bean.setCurrentStep(1);
					bean.setFirstNode(false);
    				
    				result.add(bean);
    			}
    		}
    		
    		//按申请时间降序排序
			Collections.sort(result,new Comparator<ProcessAndTaskInfoDto>() {
				@Override
				public int compare(ProcessAndTaskInfoDto o1, ProcessAndTaskInfoDto o2) {
					return o2.getBeginTime().compareTo(o1.getBeginTime());
				}
			});
    	} catch (Exception e) {
    		throw new BusinessException("查询我的申请出现错误(ERPID="+erpId+")！",e);
    	}
    	
    	return result;
    }
    
    /**
     * 
     * @desc 查询指定人员的待办任务（包含加签），代理任务共用此方法
     * @author WXJ
     * @date 2013-11-4 下午12:53:26
     *
     * @param pafKey
     * @param conditions
     * @param erpId
     * @param ownerTask
     * @param addSignerTask
     */
    private void findPrcocessTaskListByUserId(String pafKey, Conditions conditions, String erpId, List<ProcessSearch> ownerTask, List<ProcessSearch> addSignerTask){
    	Conditions cds = new Conditions();
    	try {
			ComUtils.copyProperties(conditions, cds);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	cds.setSize("500");
    	//本人的待办列表 包括本人的和加签过来的
    	List<OaTaskInstance> ownerTaskList = null;
    	if(StringUtils.isNotEmpty(pafKey)){//查询某一流程的待办
//    		ownerTaskList = this.oaPafService.getTask(erpId, pafKey, cds);
    		ownerTaskList = this.oaPafService.getTaskIncludePafAndBiz(erpId, pafKey, cds);
    	} else {//全部的待办
    		//ownerTaskList = this.oaPafService.getTask(erpId, cds);
    		//WXJ 2013-11-20
//    		Map<String, Object> mp = this.oaPafService.getTaskAndAllTotal(erpId, cds);
    		Map<String, Object> mp = this.oaPafService.getTaskAndAllTotalIncludePafAndBiz(erpId, cds);
    		ownerTaskList = (List<OaTaskInstance>) mp.get("taskList");
    	}
    	
    	//根据ERPID获取ID
    	String userId = this.sysUserService.getUserIdByUserName(erpId, "1");
    	
    	//查询本人接收到的加签过来未办理的流程
    	List<ProcessAddsigner> addSignerList = this.processAddsignerDao.findProcessInstanceByAddUserId(userId,"1");
    	
    	//计算分离出非加签任务和加签任务
    	List<ProcessSearch> ownerTaskTemp = new ArrayList<ProcessSearch>();
		List<ProcessSearch> addSignerTaskTemp = new ArrayList<ProcessSearch>();
    	this.handleOwnerTaskInfoNew(userId, ownerTaskList, addSignerList, ownerTaskTemp, addSignerTaskTemp);   
    	//取每个流程定义的前N条待办任务    	
    	this.getProcessSearchTopList(ownerTaskTemp, ownerTask, Integer.valueOf(conditions.getSize()));
    	this.getProcessSearchTopList(addSignerTaskTemp, addSignerTask, Integer.valueOf(conditions.getSize()));
    }
    
    /**
     * @Description: 查询流程待办列表，包括被代理的和加签的
      * @param erpId : bjlxdong
      * @param type
      * @param pafKey
      * @param assingerId
      * @param conditions
      * @param mapProxyTaskList
      * @type 0：全部  , 1：自己的, 2：代理别人的 ,3：接收加签的 4:自己的和接收加签的
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
	public List<ProcessSearch> findPrcocessTaskListByUserId(String erpId,int type, String pafKey,String byProxyUserId,
    		Conditions conditions,Map<String,List<ProcessSearch>>...mapProxyTaskList){
    	try{
    		List<ProcessSearch> result = new ArrayList<ProcessSearch>();
    		
    		List<ProcessSearch> ownerTask = new ArrayList<ProcessSearch>();
    		List<ProcessSearch> addSignerTask = new ArrayList<ProcessSearch>();
    		List<ProcessSearch> proxyTask = new ArrayList<ProcessSearch>();
    		
    		if(type==0 || type==1 || type==3 || type==4){
    			this.findPrcocessTaskListByUserId(pafKey, conditions, erpId, ownerTask, addSignerTask);
    		}
    		
			//代理别人的流程信息 待办
    		if(type==0||type==2){
    			String userPId = this.sysUserService.getUserIdByUserName(erpId, SystemConstant.ORGNAIZATION_TYPE_HR);
    			List<ProcessProxy> listProxy = this.processProxyDao.findListByproxyId(userPId,byProxyUserId);
    			//this.handleProxyTaskInfo(listProxy, pafKey,proxyTask, conditions, mapProxyTaskList);
    			this.handleProxyTaskInfoNew(listProxy, proxyTask, pafKey, conditions, mapProxyTaskList);
    		}
			
			if((type==0||type==1) && ownerTask!=null&&ownerTask.size()>0){
				result.addAll(ownerTask);
			}
			if((type==0||type==2) && proxyTask!=null&&proxyTask.size()>0){
				result.addAll(proxyTask);
			}
			if((type==0||type==3) && addSignerTask!=null&&addSignerTask.size()>0){
				result.addAll(addSignerTask);
			}
			if(type==4){
				result.addAll(ownerTask);
				result.addAll(addSignerTask);
			}
			
    		return result;
    	} catch(Exception e){
    		e.printStackTrace();
    		 logger.error(e);
             throw new RuntimeException("查询待办列表失败！");
    	}
    }
    
    /**
     * @Description: 查询某人审批过的流程
      * @param byProxyUserId 被代理人
      * @param paramHistory :bjlxdong
      * @param conditions
      * @type 0：全部 1：自己的 2：代理别人的 3：接收加签的  4:自己的和接收加签的
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
	public List<ProcessSearch> findPrcocessTaskOverByUserId(int type,String byProxyUserId,
    		ProcessTaskHistory paramHistory,Conditions conditions) {
    	try{
    		List<ProcessSearch> result = new ArrayList<ProcessSearch>();
    		
    		List<ProcessSearch> ownerTask = new ArrayList<ProcessSearch>();
    		List<ProcessSearch> addSignerTask = new ArrayList<ProcessSearch>();
    		List<ProcessSearch> proxyTask = new ArrayList<ProcessSearch>();
    		
    		String userPId = null;
    		String userErpId = paramHistory.getUserId();
    		List<ProcessTaskHistory> tempProcessHis = null;
    		if(userErpId!=null && !"".equals(userErpId)){
    			userPId = this.sysUserService.getUserIdByUserName(paramHistory.getUserId(), SystemConstant.ORGNAIZATION_TYPE_HR);
    			paramHistory.setUserId(userPId);
    			//从数据库表历史节点中查询审批过的流程，包括自己本身的，代理别人的和加签过来的
        		
    			tempProcessHis = this.processTaskHistoryService.selectByCondition(paramHistory);
    		}
    		
			this.convertHistoryToInstance(tempProcessHis,ownerTask,addSignerTask,proxyTask);
			
			if((type==0||type==1) && ownerTask!=null&&ownerTask.size()>0){
				result.addAll(ownerTask);
			}
			if((type==0||type==2) && proxyTask!=null&&proxyTask.size()>0){
				result.addAll(proxyTask);
			}
			if((type==0||type==3) && addSignerTask!=null&&addSignerTask.size()>0){
				//只过滤出自己处理过的加签流程
        		//查询本人接收到的加签过来已办理的流程
        		List<ProcessAddsigner> addSignerList = 
        				this.processAddsignerDao.findProcessInstanceByAddUserId(paramHistory.getUserId(),"2");
        		Map<String,ProcessSearch> mapTemp = null;
        		if(addSignerList != null && addSignerList.size() > 0){
        			mapTemp = new HashMap<String,ProcessSearch>(addSignerList.size());
        			for(ProcessSearch bean:addSignerTask){
        				mapTemp.put(bean.getProcessInstanceId(), bean);
        			}
        			for(ProcessAddsigner bean:addSignerList){
        				result.add(mapTemp.get(bean.getProcessInstanceId()));
        			}
        		}
        			
			}
			
    		return result;
    	} catch(Exception e){
   		 logger.error(e);
         throw new RuntimeException("查询审批过的列表失败！");
    	}
    }
    
	/**
	 * 
	 * @param tempProcessHis
	 * @return
	 */
	private void convertHistoryToInstance(List<ProcessTaskHistory> tempProcessHis,List<ProcessSearch> ownerTask,
			List<ProcessSearch> addSignerTask,List<ProcessSearch> proxyTask){
		ProcessSearch resultBean = null;
		Map<String,String> owerMap = new HashMap<String, String>();
		Map<String,String> proxyMap = new HashMap<String, String>();
		Map<String,String> addSignerMap = new HashMap<String, String>();
		
		if(tempProcessHis != null && tempProcessHis.size() >0){
			for(ProcessTaskHistory history:tempProcessHis){
				resultBean = new ProcessSearch();
				resultBean.setProcessInstanceId(history.getProcessInstanceId());//流程实例ID
				resultBean.setIsComplete(history.getInstanceIscomplete());//流程完成标示 0：未完成 1：已完成
				if(SystemConstant.TASK_KUBU_OWNER.equals(history.getTaskType())){//自己本身的流程
					if(!owerMap.containsKey(history.getProcessInstanceId())){
						owerMap.put(history.getProcessInstanceId(), history.getProcessInstanceId());
						ownerTask.add(resultBean);
					}
				} else if(SystemConstant.TASK_KUBU_PROXY.equals(history.getTaskType())){//代理别人的流程
					if(!proxyMap.containsKey(history.getProcessInstanceId())){
						proxyMap.put(history.getProcessInstanceId(), history.getProcessInstanceId());
						proxyTask.add(resultBean);
					}
				} else {
					if(!addSignerMap.containsKey(history.getProcessInstanceId())){//加签过来的流程
						addSignerMap.put(history.getProcessInstanceId(), history.getProcessInstanceId());
						addSignerTask.add(resultBean);
					}
				}
			}
		}
	}
	
    /**
     * @Description: 查询某人要督办的或督办过的流程列表
      * @param userId :bjlxdong 
      * @param status 1:要督办的  2：督办过的;3:全部
      * @param 流程pafKey
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
    public List<ProcessSearch> findPrcocessSuperviseTaskByUserId(String userErpId,String status,String pafKey,Conditions conditions){
    	try {
    		List<ProcessSearch> result = new ArrayList<ProcessSearch>();
    		//根据ErpId获取UserId
    		String sysUserId = this.sysUserService.getUserIdByUserName(userErpId, SystemConstant.ORGNAIZATION_TYPE_HR);
    		//根据流程PafKey 获取流程定义
    		String paramProDefiID = null;
    		if(StringUtils.isNotEmpty(pafKey)){
    			ProcessDefinition pepDefin = this.processDefService.getByDefinitionId(pafKey);
    			if(pafKey!=null){
    				paramProDefiID = pepDefin.getId();
    			}
    		}
    		
    		List<String> processDefinitionId = null;
    		if(StringUtils.isNotEmpty(paramProDefiID)){
    			processDefinitionId = new ArrayList<String>(1);
    			processDefinitionId.add(paramProDefiID);
    		} else {
    			//获取该用户有权督办的流程定义ID
        		List<SysBussinessAuthExpression> listProcessDefId = this.userAuthorizationService.getResourceByUserId(sysUserId, "4");
    			if(listProcessDefId != null && listProcessDefId.size() > 0){
        			processDefinitionId = new ArrayList<String>();
        			for(SysBussinessAuthExpression bean:listProcessDefId){
        				processDefinitionId.add(bean.getBusinessId());
        			}
        		} else {
        			return result;
        		}
    		}
    		
    		result = this.processSearchDao.findProcessInstanceByDefId(processDefinitionId, 
    				conditions.getCreateStartDate(), conditions.getCreateEndDate());
    		Map<String,ProcessSearch> mapAllTask =new HashMap<String, ProcessSearch>();
    		String processInstanceId = null;
    		
    		if(result!=null&&result.size()>0){
				for(ProcessSearch bean:result){
					processInstanceId = bean.getProcessInstanceId();
					//流程完成标示的设置
					if(SystemConstant.PROCESS_STATUS_1.equals(bean.getStatus())
    						|| SystemConstant.PROCESS_STATUS_2.equals(bean.getStatus())
    						|| SystemConstant.PROCESS_STATUS_3.equals(bean.getStatus())){
						bean.setIsComplete(SystemConstant.PROCESS_COMPLETE_0);//流程还未完成
					} else {
						bean.setIsComplete(SystemConstant.PROCESS_COMPLETE_1);//流程已办结
					}
					mapAllTask.put(processInstanceId, bean);
				}
			}
			
    		//查询已督办过的
    		List<ProcessSupervise> supervisedProcess = this.processSuperviseDao.findProcessInstanceByUserId(sysUserId);
    		List<ProcessSearch> result_supervise = null;
    		if(supervisedProcess != null && supervisedProcess.size() > 0){
    			result_supervise = new ArrayList<ProcessSearch>(supervisedProcess.size());
    			for(ProcessSupervise bean:supervisedProcess){
    				if(mapAllTask.containsKey(bean.getProcessInstanceId())){
    					result_supervise.add(mapAllTask.get(bean.getProcessInstanceId()));
    				}
    			}
    		}
    		
    		if("3".equals(status)){
    			return result;//返回所有的
    		} else if("2".equals(status)){
    			//设置流程的状态
    			if(result_supervise!=null&&result_supervise.size()>0){
    				for(ProcessSearch bean:result_supervise){
    					processInstanceId = bean.getProcessInstanceId();
    					//流程完成标示的设置
    					if(SystemConstant.PROCESS_STATUS_1.equals(bean.getStatus())
        						|| SystemConstant.PROCESS_STATUS_2.equals(bean.getStatus())
        						|| SystemConstant.PROCESS_STATUS_3.equals(bean.getStatus())){
    						bean.setIsComplete(SystemConstant.PROCESS_COMPLETE_0);//流程还未完成
    					} else {
    						bean.setIsComplete(SystemConstant.PROCESS_COMPLETE_1);//流程已办结
    					}
    				}
    			}
    			return result_supervise;//返回督办过的
    		} else {//返回要督办的
    			if(result_supervise == null || result_supervise.size() == 0){
//    				return result;
    			} else {
    				for(ProcessSearch bean:result_supervise) {
    					result.remove(bean);
    				}
    			}
    			//筛选出状态=1,2,3的 即当前流转中的
    			List<ProcessSearch> resultList = new ArrayList<ProcessSearch>();
    			for(ProcessSearch bean:result) {
    				if(SystemConstant.PROCESS_STATUS_1.equals(bean.getStatus())
    						|| SystemConstant.PROCESS_STATUS_2.equals(bean.getStatus())
    						|| SystemConstant.PROCESS_STATUS_3.equals(bean.getStatus())){
    					resultList.add(bean);
    					//result.remove(bean);
    				}
    			}
    			return resultList;
    		}
    	} catch(Exception e) {
    		logger.error(e);
    		e.printStackTrace();
            throw new RuntimeException("查询督办信息失败！");
    	}
    }
    
    /**
     * 返回督办流程个数
     * @param userId
     * @param processDefId
     * @param conditions
     * @return
     */
    public int getProSuperviseTaskCountByUserId(String userErpId,String processDefinId,Conditions conditions) {
    	int count = 0;
    	try {
    		List<ProcessSearch> result = null;
    		//根据ErpId获取UserId
    		String sysUserId = this.sysUserService.getUserIdByUserName(userErpId, SystemConstant.ORGNAIZATION_TYPE_HR);
    		
    		List<String> processDefinitionId = null;
    		//获取该用户有权督办的流程定义ID
    		if(StringUtils.isNotEmpty(processDefinId)) {
    			processDefinitionId = new ArrayList<String>(1);
    			processDefinitionId.add(processDefinId);
    		} else {
    			List<SysBussinessAuthExpression> listProcessDefId = this.userAuthorizationService.getResourceByUserId(sysUserId, "4");
    			if(listProcessDefId != null && listProcessDefId.size() > 0){
        			processDefinitionId = new ArrayList<String>();
        			for(SysBussinessAuthExpression bean:listProcessDefId){
        				processDefinitionId.add(bean.getBusinessId());
        			}
        		} else {
        			return count;
        		}
    		}
    		
    		result = this.processSearchDao.findProcessInstanceByDefId(processDefinitionId, 
    				conditions.getCreateStartDate(), conditions.getCreateEndDate());
    		Map<String,ProcessSearch> mapAllTask =new HashMap<String, ProcessSearch>();
    		String processInstanceId = null;
    		
    		if(result!=null&&result.size()>0){
				for(ProcessSearch bean:result){
					processInstanceId = bean.getProcessInstanceId();
					mapAllTask.put(processInstanceId, bean);
				}
			}
    		
    		//查询已督办过的
    		List<ProcessSupervise> supervisedProcess = this.processSuperviseDao.findProcessInstanceByUserId(sysUserId);
    		List<ProcessSearch> result_supervise = null;
    		if(supervisedProcess != null && supervisedProcess.size() > 0){
    			result_supervise = new ArrayList<ProcessSearch>(supervisedProcess.size());
    			for(ProcessSupervise bean:supervisedProcess){
    				if(mapAllTask.containsKey(bean.getProcessInstanceId())){
    					result_supervise.add(mapAllTask.get(bean.getProcessInstanceId()));
    				}
    			}
    		}
    		
    		if(result != null && result.size() > 0){
    			if(result_supervise == null || result_supervise.size() == 0){
    			} else {
    				for(ProcessSearch bean:result_supervise) {
    					result.remove(bean);
    				}
    			}
    			for(ProcessSearch bean:result){
    				if(SystemConstant.PROCESS_STATUS_1.equals(bean.getStatus())
    						|| SystemConstant.PROCESS_STATUS_2.equals(bean.getStatus())
    						||SystemConstant.PROCESS_STATUS_3.equals(bean.getStatus())){
    						++count;
    				}
    			}
    		}
    		return count;
    	} catch(Exception e) {
    		logger.error(e);
            throw new RuntimeException("查询督办信息失败！");
    	}
    }
    
	@Override
	public List<String> findProcessDefAll() {
		// TODO Auto-generated method stub
		List<ProcessDefinition> pdList = processDefDao.findProcessDefAll();
		List<String> keyList = new ArrayList<String>();
		if (pdList!=null && pdList.size()>0){
			for (ProcessDefinition pd : pdList){
				if (!keyList.contains(pd.getPafProcessDefinitionId()))
					keyList.add(pd.getPafProcessDefinitionId());
			}
			return keyList;
		}else{
			return null;
		}
	}
	
	/**
     * @Description: 处理本身待办或已办流程，拆分出自己的和加签过来的
      * @param userId :bjlxdong 
      * @param status 1:要督办的  2：督办过的;3:全部
      * @param 流程pafKey
      * @return List<ProcessSearch>
      * @author WXJ
      * @date 2013-11-01
      * @version V1.0
     */
	private void handleOwnerTaskInfoNew(String userId,List<OaTaskInstance> ownerTaskList,List<ProcessAddsigner> addSignerList,
			List<ProcessSearch> ownerTask,List<ProcessSearch> addSignerTask){
		Map<String,ProcessAddsigner> mapAddSigner = new HashMap<String,ProcessAddsigner>();
		if(addSignerList != null && addSignerList.size() > 0){
			for(ProcessAddsigner bean:addSignerList){
				mapAddSigner.put(bean.getProcessInstanceId()+bean.getNodeId()+bean.getAddsignUserId(), bean);
			}
		}
		if(ownerTaskList != null && ownerTaskList.size() > 0){
			OaTaskInstance task = null;
			String processInstanceId = null;
			String nodeId = null;
			List<String> processInstanceIds=new ArrayList<String>();
			for(int i=0; i<ownerTaskList.size();i++){
				task = ownerTaskList.get(i);
				//对应的流程实例ID
				processInstanceId = task.getProcessInstanceId();
				processInstanceIds.add(processInstanceId);
			}
			Map<String,ProcessSearch> mapTemp = new HashMap<String, ProcessSearch>();
			if(processInstanceIds.size() > 0){
				List<ProcessSearch> tempList = this.processSearchDao.getProcessInfoByInstanceId(processInstanceIds);
				if(tempList != null && tempList.size() > 0){
					for(ProcessSearch bean:tempList){
						mapTemp.put(bean.getProcessInstanceId(), bean);
					}
				}
			}
			ProcessSearch oldBean = null;
			
			for(int i=0; i<ownerTaskList.size();i++){
				task = ownerTaskList.get(i);
				//对应的流程实例ID
				processInstanceId = task.getProcessInstanceId();
				nodeId = task.getNodeId();

				ProcessSearch ps = new ProcessSearch();
				processInstanceIds.clear();
				processInstanceIds.add(processInstanceId);
				oldBean = mapTemp.get(processInstanceId);
				if(oldBean!=null){
					try {
						ComUtils.copyProperties(oldBean, ps);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					continue;
				}
				if (mapAddSigner.containsKey(processInstanceId+nodeId+userId)){
					task.setTaskType(SystemConstant.TASK_KUBU_ADDSIGNER);
					ps.setCurrentTaskNoteInfo(task);
					ps.setBySignerUserId(mapAddSigner.get(processInstanceId+nodeId+userId).getUserId());//谁加签过来的
					
					addSignerTask.add(ps);
				}else{
					if(task.getProcessDefinitionIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)){
						task.setTaskType(SystemConstant.TASK_KUBU_OWNER);
						ps.setCurrentTaskNoteInfo(task);
						ownerTask.add(ps);
						continue;
					}
					//判断当前节点是否是首节点
					ProcessNode node = this.processNodeService.findAddsignRule(task.getProcessDefinitionId(), nodeId);
					if(node!=null){
						if(!"1".equals(node.getIsFirstNode())){							
							task.setFirstNode(false);
							task.setTaskType(SystemConstant.TASK_KUBU_OWNER);
							ps.setCurrentTaskNoteInfo(task);
							
							ownerTask.add(ps);
						}
					}
				}
			}
		}
	}	
		
	/**
	 * 处理本身待办或已办流程，拆分出自己的和加签过来的
	 * @param ownerTaskList
	 * @param addSignerList
	 * @param ownerTask
	 * @param addSignerTask
	 */
	private void handleOwnerTaskInfo(List<OaTaskInstance> ownerTaskList,List<ProcessAddsigner> addSignerList,
			List<ProcessSearch> ownerTask,List<ProcessSearch> addSignerTask){
		Map<String,ProcessAddsigner> mapAddSigner = new HashMap<String,ProcessAddsigner>();
		if(addSignerList != null && addSignerList.size() > 0){
			for(ProcessAddsigner bean:addSignerList){
				mapAddSigner.put(bean.getProcessInstanceId(), bean);
			}
		}
		if(ownerTaskList != null && ownerTaskList.size() > 0){
			OaTaskInstance task = null;
			List<String> processInstanceIds=new ArrayList<String>();
			Map<String,OaTaskInstance> tempMapTask = new HashMap<String,OaTaskInstance>(ownerTaskList.size());
			String processInstanceId = null;
			String nodeId = null;
			for(int i=0; i<ownerTaskList.size();i++){
				task = ownerTaskList.get(i);
				//对应的流程实例ID
				processInstanceId = task.getProcessInstanceId();
				nodeId = task.getNodeId();
				//判断当前节点是否是首节点
				ProcessNode node = this.processNodeService.findAddsignRule(task.getProcessDefinitionId(), nodeId);
				if(node!=null){
					if("1".equals(node.getIsFirstNode())){
						task.setFirstNode(true);
					} else {
						task.setFirstNode(false);
					}
				}
				tempMapTask.put(processInstanceId, task);
				processInstanceIds.add(processInstanceId);
			}
			
			if(processInstanceIds.size()>0){
				List<ProcessSearch> tempList = this.processSearchDao.getProcessInfoByInstanceId(processInstanceIds);
				if(tempList!=null&&tempList.size()>0){
					OaTaskInstance tempTaskInstance = null;
					for(ProcessSearch bean:tempList){
						processInstanceId = bean.getProcessInstanceId();
						tempTaskInstance = tempMapTask.get(processInstanceId);
	    				if(mapAddSigner.containsKey(processInstanceId)){
	    					//加签过来的流程
	    					tempTaskInstance.setTaskType(SystemConstant.TASK_KUBU_ADDSIGNER);
	    					bean.setCurrentTaskNoteInfo(tempTaskInstance);
	    					bean.setBySignerUserId(mapAddSigner.get(processInstanceId).getUserId());//谁加签过来的
	    					
	    					addSignerTask.add(bean);
	    				} else {
	    					//自己本身的流程
	    					tempTaskInstance.setTaskType(SystemConstant.TASK_KUBU_OWNER);
	    					bean.setCurrentTaskNoteInfo(tempTaskInstance);
	    					ownerTask.add(bean);
	    				}
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @desc 
	 * @author WXJ
	 * @date 2013-11-4 下午12:53:54
	 *
	 * @param listProxy
	 * @param pafKey
	 * @param conditions
	 * @param erpId
	 * @param ownerTask
	 * @param addSignerTask
	 */
	private void handleProxyTaskInfoNew(List<ProcessProxy> listProxy, List<ProcessSearch> proxyTask, String pafKey, Conditions conditions, Map<String, List<ProcessSearch>>... mapProxyTaskList){
		if(listProxy != null && listProxy.size() > 0){
			
			List<ProcessSearch> tempTask = null;
			for(ProcessProxy proxy:listProxy){
				tempTask = new ArrayList<ProcessSearch>();
				
	    		List<ProcessSearch> ownerTask = new ArrayList<ProcessSearch>();
	    		List<ProcessSearch> addSignerTask = new ArrayList<ProcessSearch>();
				
				findPrcocessTaskListByUserId(pafKey, conditions, proxy.getAssignerIdErpId(), ownerTask, addSignerTask);
				proxyTask.addAll(ownerTask);
				proxyTask.addAll(addSignerTask);
				
				tempTask.addAll(ownerTask);
				tempTask.addAll(addSignerTask);
				
				for(ProcessSearch bean:ownerTask){
					bean.setAssignee(proxy.getAssignerIdErpId());
					bean.setIsProxy("1");
				}
				for(ProcessSearch bean:addSignerTask) {
					bean.setAssignee(proxy.getAssignerIdErpId());
					bean.setIsProxy("1");
				}
				
				if(mapProxyTaskList != null && mapProxyTaskList.length > 0){
					//MapKey=被代理人UUID_被代理人的ErpID_被代理人的姓名
					mapProxyTaskList[0].put(proxy.getAssignerId().concat(",").
						concat(proxy.getAssignerIdErpId()).concat(",").concat(proxy.getUserNameAddSinger()), tempTask);
				}
			}
			
		}
	}
	
	/**
	 * 代理别人的流程（查询待办）
	 * @param listProxy
	 * @param proxyTask
	 * @param conditions
	 */
	private void handleProxyTaskInfo(List<ProcessProxy> listProxy,String pafKey,
			List<ProcessSearch> proxyTask, Conditions conditions,Map<String, List<ProcessSearch>>... mapProxyTaskList){
		if(listProxy != null && listProxy.size() > 0){
			List<OaTaskInstance> proxyPerTaskList = null;
			Map<String,OaTaskInstance> tempMapTask = new HashMap<String,OaTaskInstance>();
			String processInstanceId = null;
			List<String> processInstanceIds = null;
			List<ProcessSearch> tempList = null;
			String nodeId = null;
			List<ProcessAddsigner> addSignerList = null;
			Map<String,ProcessAddsigner> mapProcessAddsigner = new HashMap<String,ProcessAddsigner>();
			for(ProcessProxy proxy:listProxy){
				processInstanceIds=new ArrayList<String>(listProxy.size());
				if(StringUtils.isNotEmpty(pafKey)){
					proxyPerTaskList = this.oaPafService.getTask(proxy.getAssignerIdErpId(), pafKey, conditions);
				} else {
					//proxyPerTaskList = this.oaPafService.getTask(proxy.getAssignerIdErpId(), conditions);
					//WXJ 2013-11-20
		    		Map<String, Object> mp = this.oaPafService.getTaskAndAllTotal(proxy.getAssignerIdErpId(), conditions);    		
		    		proxyPerTaskList = (List<OaTaskInstance>) mp.get("taskList");
				}
				//根据代理流程的原办理人查询出还未办理的加签过来的流程
				mapProcessAddsigner.clear();
	    		addSignerList = this.processAddsignerDao.findProcessInstanceByAddUserId(proxy.getAssignerId(),"1");
	    		if(addSignerList != null && addSignerList.size() > 0){
	    			for(ProcessAddsigner bean:addSignerList){
	    				mapProcessAddsigner.put(bean.getProcessInstanceId(), bean);
	    			}
	    		}
	    		
				if(proxyPerTaskList != null && proxyPerTaskList.size() > 0){
					OaTaskInstance task = null;
					for(int i=0; i<proxyPerTaskList.size();i++){
						task = proxyPerTaskList.get(i);
//						task.setTaskType(SystemConstant.TASK_KUBU_PROXY);
	    				//对应的流程实例ID
	    				processInstanceId = task.getProcessInstanceId();
	    				if(mapProcessAddsigner.containsKey(processInstanceId)){
	    					task.setTaskType(SystemConstant.TASK_KUBU_ADDSIGNER);//加签的流程
	    					task.setAddSignerUserId(mapProcessAddsigner.get(processInstanceId).getUserId());
	    				} else {
	    					task.setTaskType(SystemConstant.TASK_KUBU_OWNER);//自己本身的流程
	    				}
	    				nodeId = task.getNodeId();
	    				//判断当前节点是否是首节点
	    				ProcessNode node = this.processNodeService.findAddsignRule(task.getProcessDefinitionId(), nodeId);
	    				if(node!=null){
	    					if("1".equals(node.getIsFirstNode())){
	    						task.setFirstNode(true);
	    					} else {
	    						task.setFirstNode(false);
	    					}
	    				}
	    				
	    				tempMapTask.put(processInstanceId, task);
	    				
	    				processInstanceIds.add(processInstanceId);
	    			}
				}
				
				if(processInstanceIds != null && processInstanceIds.size() > 0){
					tempList = this.processSearchDao.getProcessInfoByInstanceId(processInstanceIds);
					if(tempList!=null&&tempList.size()>0){
						for(ProcessSearch bean:tempList){
							processInstanceId = bean.getProcessInstanceId();
							bean.setCurrentTaskNoteInfo(tempMapTask.get(processInstanceId));

							bean.setBySignerUserId(tempMapTask.get(processInstanceId)!=null?
									tempMapTask.get(processInstanceId).getAddSignerUserId():"");//谁加签过来的
							
							proxyTask.add(bean);
						}
					}
				}
				if(mapProxyTaskList != null && mapProxyTaskList.length > 0){
					//MapKey=被代理人UUID_被代理人的ErpID_被代理人的姓名
					mapProxyTaskList[0].put(proxy.getAssignerId().concat(",").
							concat(proxy.getAssignerIdErpId()).concat(",").concat(proxy.getUserNameAddSinger()), tempList);
				}
			}
		}
	}
	
	/**
     * @Description: 取消流程实例(状态是草稿)
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-23下午07:42:24 
      * @version V1.0
     */
	@Transactional
    public Map<String,Object> cancelApplyForDraft(String processInstanceKey){
    	Map<String,Object> map = new HashMap<String,Object>();
    	try{
    		boolean flag = processSearchDao.cancelApplyForDraft(processInstanceKey);
        	if(flag){
        		map.put("operator", true);
        		map.put("message", "取消申请成功");
        	}else{
	    		map.put("operator", false);
	    		map.put("message", "取消申请失败");
	    	}
        	return map;
    	}catch(Exception e){
    		 logger.error(e);
             throw new BusinessException("取消申请失败",e);
    	}
    }
	
	/**
     * @Description: 取消流程实例(状态是被驳回)
      * @param processInstanceId
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-24上午10:38:14 
      * @version V1.0
     */
	@Transactional
    public Map<String,Object> cancelApplyForRejected(String processInstanceId){
    	Map<String,Object> map = new HashMap<String,Object>();
    	//登录名称
		LoginContext context = LoginContext.getLoginContext();
		String pin = context.getPin();
		try{
			boolean flag = oaPafService.cancelProcessInstance(pin, processInstanceId);
	    	if(flag){
	    		map.put("operator", true);
	    		map.put("message", "取消申请成功");
	    	}else{
	    		map.put("operator", false);
	    		map.put("message", "取消申请失败");
	    	}
	    	return map;
		}catch(Exception e){
			 logger.error(e);
             throw new BusinessException("取消申请失败",e);
		}
    }
    
    /**
     * @Description: 查询流程实例提交后是否被领导审批
      * @param processInstanceId
      * @return boolean : true表示被审批过；false表示没有被领导审批过
      * @author guoqingfu
      * @date 2013-10-24上午11:21:59 
      * @version V1.0
     */
    public boolean findProcessTaskHistory(String processInstanceId){
    	try{
			ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
			processTaskHistory.setProcessInstanceId(processInstanceId);
			if(processInstanceId != null && !"".equals(processInstanceId)){
				List<ProcessTaskHistory> processTaskhistoryList = processTaskHistoryService.selectByCondition(processTaskHistory);
				if (null != processTaskhistoryList
						&& processTaskhistoryList.size() > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("查询流程实例提交后是否已被领导审批失败",e);
        }
    }
    
    /**
     * @Description: 取消流程实例(提交后但领导还没审批)
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-24上午11:42:23 
      * @version V1.0
     */
    @Transactional
    public Map<String,Object> cancelApplyForUnapproved(String processInstanceId){
    	Map<String,Object> map = new HashMap<String,Object>();
    	if(StringUtils.isEmpty(processInstanceId)){
    		map.put("operator", false);
    		map.put("message", "流程实例不能为空");
    		return map;
    	}
    	try{
    		//登录名称
    		LoginContext context = LoginContext.getLoginContext();
			boolean flag = oaPafService.cancelProcessInstance(context.getPin(), processInstanceId);
	    	if(flag){
	    		map.put("operator", true);
	    		map.put("message", "删除成功");
	    	}else{
	    		map.put("operator", false);
	    		map.put("message", "删除失败");
	    	}
	    	return map;
		}catch(Exception e){
			 logger.error(e);
             throw new BusinessException("删除失败",e);
		}
    }
    /**
     * @author wangdongxing
     */
	@Override
	public Map<String, Object> findTodoProcessTaskTotalNum(String loginUser) {
		return findTodoProcessTaskTotalNum(loginUser,null);
	}
	/**
	 * @author wangdongxing
	 */
	@Override
	public Map<String,Object> findTodoProcessTaskTotalNum(
			String loginUser, String processdefinationKey) {
		Conditions conditions=new Conditions();
		
		Map<String,Object> resultMap=null;
		if(processdefinationKey!=null&&!processdefinationKey.equals("")){
			ProcessDefinition processDefinition = processDefService.getByDefinitionId(processdefinationKey);
			if(processDefinition.getIsOuter().equals(SystemConstant.PROCESS_DEFINITION_BIZ)){
				resultMap=oaPafService.getBizTaskAndAllTotal(loginUser,processdefinationKey);
			}else{
				Map<String,Map<String, Object>>  ownerMap=oaPafService.getTaskAndKeyTotalByProcessDef(loginUser, conditions, processdefinationKey);
				if(ownerMap!=null){
					resultMap=ownerMap.get(processdefinationKey);
				}
			}
		}else{
//			resultMap=oaPafService.getTaskAndAllTotal(loginUser, conditions);
			resultMap=oaPafService.getTaskAndAllTotalIncludePafAndBiz(loginUser, conditions);
		}
		if(resultMap!=null){
			int totalNum=0;
			if(resultMap.get("total")!=null){
				totalNum=Integer.parseInt(resultMap.get("total").toString());
			}
			if(resultMap.get("taskList")!=null){
				List<OaTaskInstance> oaTaskInstances=(List<OaTaskInstance>) resultMap.get("taskList");
				if(oaTaskInstances.size()>0){
					List<String> pdfIds=new ArrayList<String>();
					for(OaTaskInstance oti:oaTaskInstances){
						pdfIds.add(oti.getProcessDefinitionId());
					}
					Map<String,List<String>> map=new HashMap<String, List<String>>();
					map.put("pdfIds", pdfIds);
					List<ProcessNode> firstNodeList=processNodeService.find("findFirstNodeList", map);
					if(firstNodeList!=null&&firstNodeList.size()>0){
						Map<String,String> firstNodeMap=new HashMap<String, String>();
						for(ProcessNode node:firstNodeList){
							firstNodeMap.put(node.getProcessDefinitionId(),node.getNodeId());
						}
						for(OaTaskInstance oti:oaTaskInstances){
							if(oti.getProcessDefinitionIsOuter().equals(SystemConstant.PROCESS_DEFINITION_OA)){
								if(firstNodeMap.get(oti.getProcessDefinitionId()).equals(oti.getNodeId())){
									totalNum--;
								}
							}
						}
					}
				}
			}
			resultMap.put("total", totalNum);
		}
		return resultMap;
	}
	/**
	 * @author wangdongxing
	 */
	@Override
	public Map<String, Object> findProxyProcessTaskTotalNum(String loginUser) {
		Map<String, Object>  totalMap=new HashMap<String, Object>();
		String userPId = this.sysUserService.getUserIdByUserName(loginUser, SystemConstant.ORGNAIZATION_TYPE_HR);
		List<ProcessProxy> listProxy = this.processProxyDao.findListByproxyId(userPId,null);
		int totalNum=0;
		for(ProcessProxy processProxy:listProxy){
			Map<String, Object>  map=findTodoProcessTaskTotalNum(processProxy.getAssignerIdErpId());
			if(map!=null&&map.get("total")!=null){
				totalNum+=Integer.parseInt(map.get("total").toString());
			}
		}
		totalMap.put("total", totalNum+"");
		return totalMap;
	}

	@Override
	public Map<String, Object> findForwardProcessTaskTotalNum(String loginUser,String processdefinationId) {
		Map<String,Object> totalMap=new HashMap<String, Object>();
		List<ProcessAddsigner> addSignerList = this.processAddsignerDao.findProcessInstanceByAddUserId(sysUserService.getUserIdByUserName(loginUser, "1"),"1",processdefinationId);
		if(addSignerList!=null){
			totalMap.put("total", addSignerList.size());
		}else{
			totalMap.put("total",0);
		}
		return totalMap;
	}
	
	
	/**
	 * 	
	 * @desc 获取List<ProcessSearch>前N条
	 * @author WXJ
	 * @date 2013-11-6 下午06:37:06
	 *
	 * @param inputList
	 * @param size
	 * @return
	 */
	private void getProcessSearchTopList(List<ProcessSearch> inputList,List<ProcessSearch> outputList,int size){
		//取每个流程定义的前N条待办任务
		Map<String, List<ProcessSearch>> mp = new HashMap<String, List<ProcessSearch>>();
		for (ProcessSearch ps : inputList){
			if (mp.containsKey(ps.getProcessDefinitionId())){
				if (mp.get(ps.getProcessDefinitionId()).size()<size){
					mp.get(ps.getProcessDefinitionId()).add(ps);					
				}
			}else{
				List<ProcessSearch> psList = new ArrayList<ProcessSearch>();
				psList.add(ps);
				mp.put(ps.getProcessDefinitionId(), psList);
			}
		}		

		Set<Map.Entry<String, List<ProcessSearch>>> entryseSet = mp.entrySet();
		for (Map.Entry<String, List<ProcessSearch>> entry : entryseSet) {
			outputList.addAll(entry.getValue());
		}
		
	}
	
	/**
	 * 
	 * @desc 获取List前N条
	 * @author WXJ
	 * @date 2013-11-2 下午05:26:30
	 *
	 * @param inputList
	 * @param size
	 * @return
	 */
	private List getTopList(List inputList,int size){
		List objList = new ArrayList();
		if (inputList!=null){
			if (inputList.size()>size){
				for (int i=0;i<size;i++){
					objList.add(inputList.get(i));
				}
				return objList;
			}else{
				return inputList;
			}
		}else{
			return null;
		}
	}

	
	@Override
	public String findBusinessData(String id,String filters,String status,String startID,String processInstanceName,String beginTimeBegin,String beginTimeEnd) {
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(id);
		Form form = getFormByProcessDefinitionId(processDefinition);
		String businessTableName = form.getFormCode();
		StringBuffer sql = new StringBuffer();

		sql.append(" select");
		// 查询的固定列  i.ID,i.PROCESS_INSTANCE_ID,i.PROCESS_INSTANCE_NAME,i.FOLLOW_CODE,i.STARTER_ID,i.BEGIN_TIME,i.STATUS,u.REAL_NAME,o.ORGANIZATION_NAME,	i.BUSINESS_INSTANCE_ID,d.PAF_PROCESS_DEFINITION_ID,d.PROCESS_DEFINITION_NAME
		sql.append(" i.ID,i.PROCESS_INSTANCE_ID as processInstanceId,i.PROCESS_INSTANCE_NAME as processInstanceName,i.FOLLOW_CODE as followCode,i.STARTER_ID as starterId,i.BEGIN_TIME as beginTime,i.STATUS as status,");
		sql.append(" u.REAL_NAME as realName,o.ORGANIZATION_NAME as organizationName,");
		sql.append("i.BUSINESS_INSTANCE_ID as businessInstanceId , d.PAF_PROCESS_DEFINITION_ID as pafProcessDefinitionId,d.PROCESS_DEFINITION_NAME as processDefinitionName");
		// 查询业务表中的列
		
		List<ProcessDefinitionConfig> listPDC = null;
		ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
		pdc.setProcessDefinitionId(processDefinition.getId());
		pdc.setConfigType("1");

		listPDC = processDefinitionConfigService.find(pdc);
		for (ProcessDefinitionConfig PDC : listPDC) {
			sql.append(", "+businessTableName+"." + PDC.getConfigItem());
		}
		

		sql.append(" from ");
		// 固定系统表
		sql.append(" T_JDOA_PROCESS_INSTANCE i left join T_JDOA_SYS_USER u on u.ID = i.STARTER_ID ,T_JDOA_SYS_ORGANIZATION o,T_JDOA_PROCESS_DEFINITION d,T_JDOA_SYS_POSITION p,");
		// 查询业务表
		sql.append(businessTableName);

		// 固定表之间关联
		sql.append("  WHERE i.YN = 0 "
				+ // 实例未删除
//				" and u.ID = i.STARTER_ID "
//				+ // 申请人关联
				" AND u.STATUS = '0'  "
				+ // 用户未删除
				" AND u.ORGANIZATION_ID = o.ID  "
				+ // 组织机构关联
				" and o.STATUS='0'  "
				+ // 组织机构未删除
				" and i.BUSINESS_INSTANCE_ID = " + businessTableName
				+ ".ID"
				+" and p.POSITION_CODE = u.POSITION_CODE"//和职位表关联
				+ // 实例与业务表关联
				" and i.PROCESS_DEFINITION_ID = d.ID"+
				" and o.ORGANIZATION_TYPE = '"
				+ SystemConstant.ORGNAIZATION_TYPE_HR + "'");// 组织机构
		// 查询条件--流程定义
		sql.append(" and i.PROCESS_DEFINITION_ID = '"
				+ processDefinition.getId() + "'");
		// 查询条件---状态(0:草稿 1:审批中 2:拒绝 3:驳回 4:取消 5:归档)
		if (StringUtils.isNotEmpty(status)) {
			if ("draft".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_0 + "'");
			} else if ("approvaling".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_1 + "'");
			} else if ("overrule".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_2 + "'");
			} else if ("reject".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_3 + "'");
			} else if ("cancel".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_4 + "'");
			} else if ("approved".equalsIgnoreCase(status)) {
				sql.append(" and i.STATUS = '"
						+ SystemConstant.PROCESS_STATUS_5 + "'");
			}
		}
		LoginContext context = LoginContext.getLoginContext();
		String pin = context.getPin();
		String defaultStartID = sysUserService.getUserIdByUserName(pin,
				SystemConstant.ORGNAIZATION_TYPE_HR);
		Boolean isAdministrator = userAuthorizationService
				.isResourceOwnedByUser(
						processDefinition.getId(),
						SysBussinessAuthExpression.BUSSINESS_TYPE_ADMINISTRATOR,
						defaultStartID);
		if (StringUtils.isNotEmpty(startID)) {
			sql.append(" and i.STARTER_ID = '" + startID + "'");
		}
		if (!isAdministrator) {

//			// 申请人
//			if (StringUtils.isNotEmpty(startID)) {
//				sql.append(" and i.STARTER_ID = '" + startID + "'");
//			}

			// 判断下属（权限）
			if (StringUtils.isNotEmpty(pin)) {
				List positionCodeList = sysOrganizationService
						.findUnderlingList(pin);
				if(positionCodeList.size()>0){
					sql.append(" and ( u.POSITION_CODE in ( '");
					for (int i = 0; i < positionCodeList.size(); i++) {
						if (i != positionCodeList.size() - 1) {
							sql.append(positionCodeList.get(i) + "','");
						} else {
							sql.append(positionCodeList.get(i) + "')");
						}
					}
					sql.append(" or i.STARTER_ID = '" + defaultStartID + "')");
				}else{
					sql.append(" and i.STARTER_ID = '" + defaultStartID + "'");
				}
				

				
			}

		}
		// 主题
		if (StringUtils.isNotEmpty(processInstanceName)) {
			sql.append(" and ( i.PROCESS_INSTANCE_NAME like '%"
					+ processInstanceName + "%'");
			sql.append(" or i.FOLLOW_CODE like '%"
					+ processInstanceName + "%' )");
		}
		// 申请时间
//		String beginTimeBegin = request.getParameter("beginTimeBegin");
		if (StringUtils.isNotEmpty(beginTimeBegin)) {
			sql.append(" and  i.BEGIN_TIME >=   '" + beginTimeBegin + "'");
		}
//		String beginTimeEnd = request.getParameter("beginTimeEnd");
		if (StringUtils.isNotEmpty(beginTimeEnd)) {
			sql.append(" and  i.BEGIN_TIME <=   '" + beginTimeEnd + "'");
		}

		// 查询条件--业务表筛选
		List<FormItem> filtersList=JsonUtils.fromJsonByGoogle(filters,new TypeToken<List<FormItem>>(){});

		for (FormItem FI : filtersList) {
			if (StringUtils.isNotEmpty(FI.getComputeExpress())) {
				if (FI.getDataType().equals("DATE")
						|| FI.getDataType().equals("NUMBER")) {
					if (StringUtils.isNotEmpty(FI.getComputeExpress())) {
						sql.append(" and " + businessTableName + "."
								+ FI.getFieldName() + " >= "
								+ FI.getComputeExpress() + "");

					}
					if (StringUtils.isNotEmpty(FI.getComputeExpressDesc())) {
						sql.append(" and " + businessTableName + "."
								+ FI.getFieldName() + " <= "
								+ FI.getComputeExpressDesc() + "");
					}

				} else if (FI.getInputType().equals("列表")
						|| FI.getInputType().equals("单选按纽组")) {
					sql.append(" and " + businessTableName + "."
							+ FI.getFieldName() + " = '"
							+ FI.getComputeExpress() + "'");
				} else {
					sql.append(" and " + businessTableName + "."
							+ FI.getFieldName() + " like '%"
							+ FI.getComputeExpress() + "%'");
				}
			}
		}
		return sql.toString();
	}
	 public Form getFormByProcessDefinitionId(ProcessDefinition processDefinition) throws BusinessException{
			if(processDefinition != null){
				if(processDefinition.getFormId() == null || processDefinition.getFormId().trim().length() == 0){
					throw new BusinessException("指定的流程未绑定表单");
				}
				return formService.get(processDefinition.getFormId());
			} else {
				throw new BusinessException("指定的流程定义KEY不存在");
			}
		}

	
}

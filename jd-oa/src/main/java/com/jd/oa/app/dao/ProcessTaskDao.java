package com.jd.oa.app.dao;

import java.util.List;

import com.jd.oa.app.model.OaProcessInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessProxyTask;
import org.apache.poi.ss.formula.functions.T;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.process.model.ProcessInstance;

public interface ProcessTaskDao extends BaseDao<T, String> {
	public List<ProcessInstance> findProcessInstances(ProcessInstance processInstance);

    /**
     * 根据节点ID 查询流程变量
     * @param nodeId
     * @return
     */
    public List<ProcessNodeFormPrivilege> findVariableByNodeId(String processDefinitionId, String nodeId);
    /**
     * 根据paf返回的流程实例ID 更新任务状态
     * @param oaProcessInstance
     */
    public void updateProcessInstance(OaProcessInstance oaProcessInstance) ;
    /**
     * 审批成功后插入任务历史数据
     * @param processTaskHistory
     * @return
     */
    public void taskHistoryAdd(ProcessTaskHistory processTaskHistory);

    /**
     * 代理人审批成功后插入历史记录
     * @param processProxyTask
     */
    public void processProxyTaskAdd(ProcessProxyTask processProxyTask);
}

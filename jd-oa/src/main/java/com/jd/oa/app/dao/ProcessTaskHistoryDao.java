package com.jd.oa.app.dao;

import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.dao.BaseDao;

import java.util.List;

/**
 * 任务历史Dao接口类
 * User: zhaoming
 * Date: 13-10-22
 * Time: 下午8:47
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessTaskHistoryDao extends BaseDao<ProcessTaskHistory,String> {

    /**
     * 查询流程实例已结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return  已办任务列表
     */
    public List<ProcessTaskHistory> selectEndProcessTaskHistory(ProcessTaskHistory processTaskHistory);

    /**
     * 查询流程实例未结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectProcessTaskHistory(ProcessTaskHistory processTaskHistory);

    /**
     * 根据条件查询任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectByCondition(ProcessTaskHistory processTaskHistory);

    /**
     * 新增任务历史日志
     * @param processTaskHistory 任务历史模型类
     */
    public void insertProcessTask(ProcessTaskHistory processTaskHistory);
    
    /**
     * 查询流程实例未结案的任务历史(一次性读取多个，按照流程实例和创建时间降序排序)
     * @param List<ProcessTaskHistory> 任务历史模型类
     * @return List<ProcessTaskHistory>
     */
    public List<ProcessTaskHistory> selectProcessTaskHistoryAll(List<String> processInstanceIds);
    
    /**
     * 
     * @desc 获取当前流程实例指定Node最新的审批历史记录
     * @author WXJ
     * @date 2014-7-14 下午01:25:45
     *
     * @param processTaskHistory
     * @return processTaskHistory
     */
    public ProcessTaskHistory getRecentHistoryByNode(ProcessTaskHistory processTaskHistory);
}

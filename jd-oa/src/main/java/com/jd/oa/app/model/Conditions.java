package com.jd.oa.app.model;

/**
 * 流程任务查询条件对象
 * User: WXJ
 * Date: 13-10-12
 * Time: 下午6:14
 */
public class Conditions {
	private String processInstanceId;
	private String processDefinitionId;
	private String taskName;
	private String taskDefinitionKey;
	private String assignee;
	/**
	 * 
	 */
	private String businessKey;
	/**
	 * 任务创建起始时间
	 */
	private String createStartDate;
	/**
	 * 任务创建结束时间
	 */
	private String createEndDate;

	private String dueDate;
	private String minDueDate;
	private String maxDueDate;
	
	private String size;
	private String start;
	private String sort;
	private String order;

    private String beginTimeStart;
    private String beginTimeEnd;
    private String endTimeStart;
    private String endTimeEnd;
		
	//Init
	public Conditions(){
		this.setSize("500");
		this.setOrder("desc");
		this.setSort("createTime");
	}
	
	//
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}
	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}
	
	public String getBusinessKey() {
		return businessKey;
	}
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
	public String getCreateStartDate() {
		return createStartDate;
	}
	public void setCreateStartDate(String createStartDate) {
		this.createStartDate = createStartDate;
	}
	public String getCreateEndDate() {
		return createEndDate;
	}
	public void setCreateEndDate(String createEndDate) {
		this.createEndDate = createEndDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getMinDueDate() {
		return minDueDate;
	}
	public void setMinDueDate(String minDueDate) {
		this.minDueDate = minDueDate;
	}
	public String getMaxDueDate() {
		return maxDueDate;
	}
	public void setMaxDueDate(String maxDueDate) {
		this.maxDueDate = maxDueDate;
	}
	
	
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}

    public String getBeginTimeStart() {
        return beginTimeStart;
    }

    public void setBeginTimeStart(String beginTimeStart) {
        this.beginTimeStart = beginTimeStart;
    }

    public String getBeginTimeEnd() {
        return beginTimeEnd;
    }

    public void setBeginTimeEnd(String beginTimeEnd) {
        this.beginTimeEnd = beginTimeEnd;
    }

    public String getEndTimeStart() {
        return endTimeStart;
    }

    public void setEndTimeStart(String endTimeStart) {
        this.endTimeStart = endTimeStart;
    }

    public String getEndTimeEnd() {
        return endTimeEnd;
    }

    public void setEndTimeEnd(String endTimeEnd) {
        this.endTimeEnd = endTimeEnd;
    }
}

package com.jd.oa.app.dao.impl;

import com.jd.oa.app.dao.ProcessTaskHistoryDao;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.dao.MyBatisDaoImpl;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 任务历史Dao实现类
 * User: zhaoming
 * Date: 13-10-22
 * Time: 下午8:56
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ProcessTaskHistoryDaoImpl  extends MyBatisDaoImpl<ProcessTaskHistory, String> implements ProcessTaskHistoryDao {

    /**
     * 查询流程实例已结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectEndProcessTaskHistory(ProcessTaskHistory processTaskHistory){
        return this.getSqlSession().selectList("com.jd.oa.app.model.ProcessTaskHistory.selectEndProcessTaskHistory", processTaskHistory);
    }

    /**
     * 查询流程实例未结案的任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectProcessTaskHistory(ProcessTaskHistory processTaskHistory){
        return this.getSqlSession().selectList("com.jd.oa.app.model.ProcessTaskHistory.selectProcessTaskHistory", processTaskHistory);
    }

    /**
     * 根据条件查询任务历史
     * @param processTaskHistory 任务历史模型类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectByCondition(ProcessTaskHistory processTaskHistory){
        return this.getSqlSession().selectList("com.jd.oa.app.model.ProcessTaskHistory.selectByCondition", processTaskHistory);
    }

    /**
     * 新增任务历史日志
     * @param processTaskHistory 任务历史模型类
     */
    public void insertProcessTask(ProcessTaskHistory processTaskHistory){
        this.getSqlSession().insert("com.jd.oa.app.model.ProcessTaskHistory.insertProcessTask", processTaskHistory);
    }

	@Override
	public List<ProcessTaskHistory> selectProcessTaskHistoryAll(
			List<String> processInstanceIds) {
		return this.getSqlSession().selectList("com.jd.oa.app.model.ProcessTaskHistory.selectProcessTaskHistoryAll", processInstanceIds);
	}

	@Override
	public ProcessTaskHistory getRecentHistoryByNode(ProcessTaskHistory processTaskHistory) {
		// TODO Auto-generated method stub
		List<ProcessTaskHistory> pthList = this.find("getRecentHistoryByNode", processTaskHistory);
		if (pthList!=null && pthList.size()>0)
			return pthList.get(0);
		else
			return null;
	}
}

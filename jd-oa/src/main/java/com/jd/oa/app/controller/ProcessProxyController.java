package com.jd.oa.app.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.service.ProcessTaskService;

@Controller
@RequestMapping(value="/app")
public class ProcessProxyController {

	private static final Logger logger = Logger.getLogger(ProcessProxyController.class);
	
	@Autowired
	private ProcessTaskService processTaskService;
	/**
	 * 代理审批页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/processProxy_index", method=RequestMethod.GET)
	public ModelAndView myTaskProxy() throws Exception{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("app/app_processProxy_index");
		return mv;
	}
	/**
	 * 我的审批列表
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processProxy_list",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processProxyList(HttpServletRequest request){
		Map<String,Map<String,ProcessTaskDto>> map =processTaskService.getProxyTodoProcessTaskList(request,null,null);
		return map;
	}
	/**
	 * 代理审批列表刷新
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processProxy_reload",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processProxyReload(HttpServletRequest request,String pafKey,String byProxyUserId){
		Map<String,Map<String,ProcessTaskDto>> map=new HashMap<String, Map<String,ProcessTaskDto>>();
		if(pafKey==null||pafKey.equals("")||byProxyUserId==null||byProxyUserId.equals("")){
			return map;
		}
		map =processTaskService.getProxyTodoProcessTaskList(request,pafKey,byProxyUserId);
		return map;
	}
}

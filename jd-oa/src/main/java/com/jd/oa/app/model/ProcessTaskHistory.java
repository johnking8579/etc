package com.jd.oa.app.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 任务历史模型类 User: zhaoming Date: 13-10-22 Time: 下午8:48 To change this template
 * use File | Settings | File Templates.
 */

public class ProcessTaskHistory {

	/**
	 * 主键ID
	 */
	private String id;
	/**
	 * 流程实例ID
	 */
	private String processInstanceId;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 节点名称
	 */
	private String taskName;
	/**
	 * 任务ID
	 */
	private String taskId;
	/**
	 * 审批信息
	 */
	private String comment;
	/**
	 * 审批结果
	 */
	private String result;
	/**
	 * 任务开始时间
	 */
	private Date beginTime;

	private String beginTimeStart;
	private String beginTimeEnd;
	/**
	 * 任务结束时间
	 */
	private Date endTime;

	private String endTimeStart;
	private String endTimeEnd;
	/**
	 * 流程是否结案
	 */
	private String instanceIscomplete;
	/**
	 * 流程定义ID
	 */
	private String pafProcessDefinitionId;
	/**
	 * 用户组件ID
	 */
	private String userId;
	/**
	 * 任务处理人姓名
	 */
	private String userName;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 修改人
	 */
	private String modifier;

	/**
	 * 任务类型
	 */
	private String taskType;

	/**
	 * 原处理人
	 */
	private String assignerId;

	/**
	 * 修改时间
	 */
	private Date modifyTime;
	/**
	 * 逻辑删除
	 */
	private int yn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getBeginTimeStart() {
		return beginTimeStart;
	}

	public void setBeginTimeStart(String beginTimeStart) {
		this.beginTimeStart = beginTimeStart;
	}

	public String getBeginTimeEnd() {
		return beginTimeEnd;
	}

	public void setBeginTimeEnd(String beginTimeEnd) {
		this.beginTimeEnd = beginTimeEnd;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getEndTimeStart() {
		return endTimeStart;
	}

	public void setEndTimeStart(String endTimeStart) {
		this.endTimeStart = endTimeStart;
	}

	public String getEndTimeEnd() {
		return endTimeEnd;
	}

	public void setEndTimeEnd(String endTimeEnd) {
		this.endTimeEnd = endTimeEnd;
	}

	public String getInstanceIscomplete() {
		return instanceIscomplete;
	}

	public void setInstanceIscomplete(String instanceIscomplete) {
		this.instanceIscomplete = instanceIscomplete;
	}

	public String getPafProcessDefinitionId() {
		return pafProcessDefinitionId;
	}

	public void setPafProcessDefinitionId(String pafProcessDefinitionId) {
		this.pafProcessDefinitionId = pafProcessDefinitionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public int getYn() {
		return yn;
	}

	public void setYn(int yn) {
		this.yn = yn;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getAssignerId() {
		return assignerId;
	}

	public void setAssignerId(String assignerId) {
		this.assignerId = assignerId;
	}

	// 用于给外部接口返回历史数据
	public String getProcessTaskHistoryForCustomService() {
		java.text.DateFormat format = new java.text.SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");
		String result =	"审批人："+isNull(this.userId)+"("+isNull(this.userName)+")"+" "
				+"审批意见："+ isNull(this.comment)+" "
				+"审批时间："+ format.format(this.endTime) + " ";
		return result;
	}

	private String isNull(String s) {
		if (null == s || s.equals("")) {
			s = " ";
		}
		return s;
	}
}

package com.jd.oa.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.common.web.LoginContext;
import com.jd.oa.app.constant.AppConstant;
import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessSearchService;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.dao.MyBatisDaoImpl;
//import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
//import com.jd.oa.common.dao.sqladpter.SqlAdapter;
//import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
//import com.jd.oa.common.dao.sqladpter.SqlAdapter.RelationalOperator;
//import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.exception.BusinessException;
//import com.jd.oa.common.service.PageForMapService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.PageWrapper;
import com.jd.oa.dict.model.DictData;
import com.jd.oa.dict.service.DictDataService;
import com.jd.oa.form.execute.FormUIUtil;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessType;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessTypeService;
import com.jd.oa.system.model.SysBussinessAuthExpression;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysUserService;
import com.jd.oa.system.service.UserAuthorizationService;

/**
 * @author guoqingfu
 * @version V1.0
 * @Title: ApplySearchController.java
 * @Package com.jd.oa.app.controller
 * @Description: 前台-申请查询controller
 * @date 2013-10-11 上午11:14:32
 */

@Controller
@RequestMapping(value = "/app")
public class ProcessSearchController {
    private static final Logger logger = Logger.getLogger(ProcessSearchController.class);
    @Autowired
    private ProcessSearchService processSearchService;
    @Autowired
    private DictDataService dictDataService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ProcessDefService processDefService;
    @Autowired
    private ProcessTypeService processTypeService;
    @Autowired
    private UserAuthorizationService userAuthorizationService;
    @Autowired
    private ProcessTaskHistoryService processTaskHistoryService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private FormItemService formItemService;
    @Autowired
	private FormService formService;
    @Autowired
    private OaPafService oaPafService;
    @Autowired
    private SysOrganizationService sysOrganizationService;
    @Autowired
    private ProcessDefinitionConfigService processDefinitionConfigService;
    /** 流程查询初始化
     * @param processTypeId
     * @param processTypeName
     * @param taskType
     * @param processDefinitionId
     * @param processDefinitionKey
     * @param addInfo
     * @return
     * @version V2.0
     * @author yujiahe
     * @date 2013-11-02下午02:42:41
     */
    @RequestMapping(value = "/processSearch_index", method = RequestMethod.GET)
    public ModelAndView index(String processTypeId, String processTypeName, String taskType, String processDefinitionId, String processDefinitionKey, String addInfo) {
        
            ModelAndView mav = new ModelAndView("app/app_processSearch_index");
        	mav.addObject("processTypeId", processTypeId);

            mav.addObject("processTypeName", processTypeName);
            mav.addObject("taskType", taskType);
            mav.addObject("processDefinitionId", processDefinitionId);
            mav.addObject("processDefinitionKey", processDefinitionKey);
            mav.addObject("addInfo", addInfo);
            return mav;        
    }

    /**
     * @param processTypeId
     * @return String
     * @Description: tab页切换
     * @author guoqingfu
     * @date 2013-10-11下午02:42:41
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_toggleCondition", method = RequestMethod.GET)
    public ModelAndView toggleCondition(String processTypeId, String flag) {
    	try{
    		ModelAndView mav = new ModelAndView("app/app_processSearch_toggleCondition");

            //申请人
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();
            String sysUserId = new String();
            if (context != null && StringUtils.isNotEmpty(pin)) {
                sysUserId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
            }
            //获取该用户有权申请的流程定义ID
            List<SysBussinessAuthExpression> listProcessDefId = userAuthorizationService.getResourceByUserId(sysUserId, "2");
            Map<String, Object> authMap = new HashMap<String, Object>();
            if (null != listProcessDefId && listProcessDefId.size() > 0) {
                for (SysBussinessAuthExpression authExpression : listProcessDefId) {
                    authMap.put(authExpression.getBusinessId(), authExpression);
                }
            }
            //通过类别ID查询流程信息,用于页面显示流程名称
            List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeId);
            List<ProcessSearch> processDefinitionList = new ArrayList<ProcessSearch>();

            if (null != processDefinitionListInfo && processDefinitionListInfo.size() > 0) {
                for (ProcessSearch processSearch : processDefinitionListInfo) {
                    SysBussinessAuthExpression sysAuthExpression = (SysBussinessAuthExpression) authMap.get(processSearch.getId());
                    if (null != sysAuthExpression) {
                        processDefinitionList.add(processSearch);
                    }
                }
            }

            mav.addObject("processDefinitionList", processDefinitionList);
            mav.addObject("processDefinitionListSize", processDefinitionList.size());
            mav.addObject("processTypeInfoId", processTypeId);
            mav.addObject("flag", flag);
            return mav;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("流程查询tab页切换失败",e);
        }
    }

    /**
     * @param processTypeId
     * @return String
     * @Description: tab页查询条件重载
     * @author guoqingfu
     * @date 2013-10-11下午02:42:41
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_resetCondition", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<ProcessSearch> resetCondition(String processTypeId) {
    	try{
    		//申请人
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();
            String sysUserId = new String();
            if (context != null && StringUtils.isNotEmpty(pin)) {
                sysUserId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
            }
            //获取该用户有权申请的流程定义ID
            List<SysBussinessAuthExpression> listProcessDefId = userAuthorizationService.getResourceByUserId(sysUserId, "2");
            Map<String, Object> authMap = new HashMap<String, Object>();
            if (null != listProcessDefId && listProcessDefId.size() > 0) {
                for (SysBussinessAuthExpression authExpression : listProcessDefId) {
                    authMap.put(authExpression.getBusinessId(), authExpression);
                }
            }
            //通过类别ID查询流程信息,用于页面显示流程名称
            List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeId);
            
            return processDefinitionListInfo;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("流程查询tab页查询条件重载失败",e);
        }
    }

    /**
     * @param model
     * @param processDefinitionKey
     * @return String
     * @Description: 进入流程申请页面
     * @author xulin
     * @date 2013-10-18下午02:42:41
     * @version V1.0
     */
    @RequestMapping(value = "/process_apply", method = RequestMethod.GET)
    public ModelAndView process_apply(String processDefinitionKey, String businessObjectId, String processInstanceStatus, Model model) {
        ModelAndView mav = new ModelAndView("app/app_processApply_iframe");
        StringBuffer url = new StringBuffer("/app/processApp_open_bindReport?processDefinitionKey=");
        url.append(processDefinitionKey);
        if (businessObjectId != null && !"".equals(businessObjectId)) {
            url.append("&businessObjectId=");
            url.append(businessObjectId);
        }
        if (processInstanceStatus != null && !"".equals(processInstanceStatus)) {
            url.append("&processInstanceStatus=");
            url.append(processInstanceStatus);
        }
        model.addAttribute("url", url.toString());
        return mav;
    }

    /**
     * @param pageWrapper
     * @param timeRange
     * @throws ParseException void
     * @Description: 根据查询条件获取申请时间范围
     * @author guoqingfu
     * @date 2013-10-12下午05:59:12
     * @version V1.0
     */
    private void getApplyTimeRange(PageWrapper<ProcessSearch> pageWrapper, String searchName, String timeRange) throws ParseException {
        try{
        	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beginTimeEnd = dateFormat.format(new Date());

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            //一个星期内
            if ("week".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.DATE, -7);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                pageWrapper.addSearch(searchName + "Begin", beginTimeBegin);
                pageWrapper.addSearch(searchName + "End", beginTimeEnd);
            }
            //一个月内
            else if ("month".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.MONTH, -1);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                pageWrapper.addSearch(searchName + "Begin", beginTimeBegin);
                pageWrapper.addSearch(searchName + "End", beginTimeEnd);
            }
            //半年内
            else if ("half".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.MONTH, -6);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                pageWrapper.addSearch(searchName + "Begin", beginTimeBegin);
                pageWrapper.addSearch(searchName + "End", beginTimeEnd);
            }
            //一年内
            else if ("year".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.YEAR, -1);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                pageWrapper.addSearch(searchName + "Begin", beginTimeBegin);
                pageWrapper.addSearch(searchName + "End", beginTimeEnd);
            }
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("根据查询条件获取申请时间范围失败",e);
        }
    }

    /**
     * @param searchName
     * @param timeRange
     * @return Map<String,Object>
     * @Description: 返回时间范围，封装到condition中调用PAF
     * @author guoqingfu
     * @date 2013-10-16下午03:31:00
     * @version V1.0
     */
    private Map<String, String> getTimeRange(String searchName, String timeRange) {
    	try{
    		Map<String, String> map = new HashMap<String, String>();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beginTimeEnd = dateFormat.format(new Date());

            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            //一个星期内
            if ("week".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.DATE, -7);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                map.put(searchName + "Begin", beginTimeBegin);
                map.put(searchName + "End", beginTimeEnd);
            }
            //一个月内
            else if ("month".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.MONTH, -1);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                map.put(searchName + "Begin", beginTimeBegin);
                map.put(searchName + "End", beginTimeEnd);
            }
            //半年内
            else if ("half".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.MONTH, -6);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                map.put(searchName + "Begin", beginTimeBegin);
                map.put(searchName + "End", beginTimeEnd);
            }
            //一年内
            else if ("year".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.YEAR, -1);
                String beginTimeBegin = dateFormat.format(cal.getTime());
                map.put(searchName + "Begin", beginTimeBegin);
                map.put(searchName + "End", beginTimeEnd);
            }
            //超过1个星期
            else if ("overOneWeek".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.DATE, -7);
                String createEndDate = dateFormat.format(cal.getTime());
                map.put("createEndDate", createEndDate);
            }
            //超过3天(过去七天至过去三天范围)
            else if ("overThreeDay".equalsIgnoreCase(timeRange)) {
                //过去三天
                cal.add(Calendar.DATE, -3);
                String createEndDate = dateFormat.format(cal.getTime());
                //过去七天
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, -7);
                String createStartDate = dateFormat.format(calendar.getTime());

                map.put("createStartDate", createStartDate);
                map.put("createEndDate", createEndDate);
            }
            //超过1天(过去三天至过去一天范围)
            else if ("overOneDay".equalsIgnoreCase(timeRange)) {
                //过去一天
                cal.add(Calendar.DATE, -1);
                String createEndDate = dateFormat.format(cal.getTime());
                //过去三天
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, -3);
                String createStartDate = dateFormat.format(calendar.getTime());

                map.put("createStartDate", createStartDate);
                map.put("createEndDate", createEndDate);
            }
            //正常无延误(一天之内)
            else if ("normal".equalsIgnoreCase(timeRange)) {
                cal.add(Calendar.DATE, -1);
                String createStartDate = dateFormat.format(cal.getTime());
                map.put("createStartDate", createStartDate);
                map.put("createEndDate", beginTimeEnd);
            }
            return map;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("返回时间范围，封装到condition中失败",e);
        }
    }


    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 我的申请查询
     * @author guoqingfu
     * @date 2013-10-12下午02:22:24
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForMyApply", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForMyApply(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isNotEmpty(id) && !"null".equals(id)) {
                ProcessDefinition processDefinition = processDefService.getByDefinitionId(id);
                pageWrapper.addSearch("id", processDefinition.getId());
            } else {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }

            //状态(0:草稿 1:审批中 2:拒绝 3:驳回 4:取消 5:归档)
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status)) {
                if ("draft".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_0);
                } else if ("approvaling".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_1);
                } else if ("overrule".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_2);
                } else if ("reject".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_3);
                } else if ("cancel".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_4);
                } else if ("approved".equalsIgnoreCase(status)) {
                    pageWrapper.addSearch("status", SystemConstant.PROCESS_STATUS_5);
                }
            }

            //申请人
            LoginContext context = LoginContext.getLoginContext();
            String pin = null;
            if(context!=null){
            	 pin = context.getPin();
            	 if(StringUtils.isNotEmpty(pin)){
            		 String startID = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                     pageWrapper.addSearch("starterId", startID);
            	 }
            }

            //代理人
            String proxyId = request.getParameter("proxyId");
            if (StringUtils.isNotEmpty(proxyId)) {
                pageWrapper.addSearch("proxyId", proxyId);
                //代理人中的原处理人ID
                if (StringUtils.isNotEmpty(pin)) {
                    String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                    pageWrapper.addSearch("assignerId", assignerId);
                }
            }

            //申请时间
            String beginTime = request.getParameter("beginTime");
            if (StringUtils.isNotEmpty(beginTime)) {
                this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
            }

            //自己申请的没有收到时间、处理时间和被代理人
            //String proxiedId = request.getParameter("proxiedId");

            //主题
            String processInstanceName = request.getParameter("processInstanceName");
            if (StringUtils.isNotEmpty(processInstanceName)) {
                pageWrapper.addSearch("processInstanceName", processInstanceName);
            }

            //机构类型（用于关联查询申请人姓名和部门）
            pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

            processSearchService.find(pageWrapper.getPageBean(), "findPageForMyApply", pageWrapper.getConditionsMap());

            //如果状态是审批中、拒绝、驳回，调用paf的方法，取得当前节点名称
            if ("approvaling".equalsIgnoreCase(status) || "overrule".equalsIgnoreCase(status) || "reject".equalsIgnoreCase(status)) {

                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");

                if (null != resultList && resultList.size() > 0) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    //查询我的申请流程详细信息（所有活动中的流程实例）
                    List<ProcessAndTaskInfoDto> processAndTaskList = processSearchService.getMyApplyProTaskInfo(pin);

                    if (null != processAndTaskList && processAndTaskList.size() > 0) {
                        for (ProcessAndTaskInfoDto patDto : processAndTaskList) {
                            List<OaTaskInstance> oaTaskInstanceList = patDto.getOaTaskInstanceList();
                            if (null != oaTaskInstanceList && oaTaskInstanceList.size() > 0) {
                                //取最后面的一个任务节点，就是当前节点
                                OaTaskInstance oaTaskInstance = oaTaskInstanceList.get(oaTaskInstanceList.size() - 1);
                                if (null != oaTaskInstance) {
                                    oaTaskInstance.setFirstNode(patDto.isFirstNode());
                                    map.put(patDto.getProcessInstanceId(), oaTaskInstance);
                                } else {
                                    map.put(patDto.getProcessInstanceId(), null);
                                }
                            } else {
                                map.put(patDto.getProcessInstanceId(), null);
                            }
                        }
                        
                        //查询流程的历史任务信息 许林 2013-11-19 性能测试
                        List<String> processInstanceIds = new ArrayList<String>();
                        Map<String,List<ProcessTaskHistory>> tempTaskHistory = null;
                        for (ProcessSearch pSearch : resultList) {
                        	processInstanceIds.add(pSearch.getProcessInstanceId());
                        }
                        if(processInstanceIds.size() > 0){
                        	tempTaskHistory = this.processTaskHistoryService.selectProcessTaskHistoryAll(processInstanceIds);
                        }
                        for (ProcessSearch pSearch : resultList) {
                            OaTaskInstance oaTaskInstance = (OaTaskInstance) map.get(pSearch.getProcessInstanceId());
                            if ("approvaling".equalsIgnoreCase(status)) {
                            	//查询是否是刚申请经理还未审批状态
                                List<ProcessTaskHistory> processTaskHistoryList = new ArrayList<ProcessTaskHistory>();
                                if(tempTaskHistory != null){
                                	processTaskHistoryList = tempTaskHistory.get(pSearch.getProcessInstanceId());
                                }
                                if(processTaskHistoryList != null && processTaskHistoryList.size() == 1){
                                	pSearch.setMark(SystemConstant.PROCESS_STATUS_6);
                                }
                            }
                            if (null != oaTaskInstance) {
                                pSearch.setCurrentNoteName(oaTaskInstance.getName());
                                pSearch.setIsFirstNode(oaTaskInstance.isFirstNode());
                                pSearch.setCurrentTaskNoteInfo(oaTaskInstance);
                            }
                        }
                    }
                }
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("我的申请查询失败",e);
        }
        
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 要审批的
     * @author guoqingfu
     * @date 2013-10-16下午04:53:26
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForMyApproval", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForMyApproval(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            
            String pin = null;
            if(context!=null){
            	pin = context.getPin();
            }
            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }

            //收到时间
            String receiveTime = request.getParameter("receiveTime");
            if (StringUtils.isNotEmpty(receiveTime)) {
                Map<String, String> map = getTimeRange("receiveTime", receiveTime);
                conditions.setCreateStartDate(map.get("receiveTimeBegin"));
                conditions.setCreateEndDate(map.get("receiveTimeEnd"));
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                conditions.setMinDueDate(map.get("dueTimeBegin"));
                conditions.setMaxDueDate(map.get("dueTimeEnd"));
            }

            //被代理人
            String proxiedId = request.getParameter("proxiedId");

            //查询流程代办列表，包括被代理的和加签的
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessTaskListByUserId(pin, 1, id, proxiedId, conditions);

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //优先级（2特急申请、1紧急申请、0标准申请），对应数据字典编码(指页面中的状态)
                String priority = request.getParameter("status");
                if (StringUtils.isNotEmpty(priority)) {
                    if ("particularlyUrgentApplication".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_2);
                    } else if ("urgentApplication".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_1);
                    } else if ("standardApplication".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_0);
                    } else {
                        pageWrapper.addSearch("priority", null);
                    }
                }

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }
                //代理人
                String proxyId = request.getParameter("proxyId");
                if (StringUtils.isNotEmpty(proxyId)) {
                    pageWrapper.addSearch("proxyId", proxyId);
                    //代理人中的原处理人ID
                    if (context != null && StringUtils.isNotEmpty(pin)) {
                        String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                        pageWrapper.addSearch("assignerId", assignerId);
                    }
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }
                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForMyApproval", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                if(null != resultList && resultList.size()>0){
    				Map<String,Object> map = new HashMap<String,Object>();
    				for(ProcessSearch pSearch: processSearchList){
    					String piId = pSearch.getProcessInstanceId();
    					OaTaskInstance currentTaskNoteInfo = pSearch.getCurrentTaskNoteInfo();
    					if(null != currentTaskNoteInfo){
    						map.put(piId, pSearch);
    					}else{
    						map.put(piId, null);
    					}
    				}
    				for(ProcessSearch result:resultList){
    					ProcessSearch allBean = (ProcessSearch)map.get(result.getProcessInstanceId());
    					if(null != allBean){
    						if(allBean.getCurrentTaskNoteInfo() != null){
    							result.setCurrentNoteName(allBean.getCurrentTaskNoteInfo().getName());
    							result.setCurrentTaskNoteInfo(allBean.getCurrentTaskNoteInfo());
    						}
    						result.setBySignerUserId(allBean.getBySignerUserId());
    					}
    				}
    			}

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;

            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("要审批的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 审批过的
     * @author guoqingfu
     * @date 2013-10-17上午10:38:54
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForMyApprovalOver", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForMyApprovalOver(HttpServletRequest request) throws Exception {
    	try{
    		 // 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            ProcessTaskHistory paramHistory = new ProcessTaskHistory();

            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = null;
            if(context!=null){
            	pin = context.getPin();
            }
            
            paramHistory.setUserId(pin);

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            paramHistory.setPafProcessDefinitionId(id);

            //收到时间
            String receiveTime = request.getParameter("receiveTime");
            if (StringUtils.isNotEmpty(receiveTime)) {
                Map<String, String> map = getTimeRange("receiveTime", receiveTime);
                paramHistory.setBeginTimeStart(map.get("receiveTimeBegin"));
                paramHistory.setBeginTimeEnd(map.get("receiveTimeEnd"));
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                paramHistory.setEndTimeStart(map.get("dueTimeBegin"));
                paramHistory.setEndTimeEnd(map.get("dueTimeEnd"));
            }

            //状态(haveHandled表示已办；handleOver表示办结),放在数据库中查询
           /* String completed = "0";
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status)) {
                if ("handleOver".equalsIgnoreCase(status)) {
                    completed = "1";
                }
            }
            paramHistory.setInstanceIscomplete(completed);*/
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status)) {
            	  if ("handleOver".equalsIgnoreCase(status)) {
            		  pageWrapper.addSearch("handleOver", "handleOver");
            	  }else{
            		  pageWrapper.addSearch("haveHandled", "haveHandled");
            	  }
            }
           

            //被代理人
            String proxiedId = request.getParameter("proxiedId");

            //查询某人审批过的流程
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessTaskOverByUserId(0, proxiedId, paramHistory, conditions);
            //流程状态
            Map<String, String> mapStatus = null;
            if (processSearchList != null && processSearchList.size() > 0) {
                mapStatus = new HashMap<String, String>(processSearchList.size());
                for (ProcessSearch search : processSearchList) {
                    mapStatus.put(search.getProcessInstanceId(), search.getIsComplete());
                }
            }

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }
                //代理人
                String proxyId = request.getParameter("proxyId");
                if (StringUtils.isNotEmpty(proxyId)) {
                    pageWrapper.addSearch("proxyId", proxyId);
                    //代理人中的原处理人ID
                    if (StringUtils.isNotEmpty(pin)) {
                        String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                        pageWrapper.addSearch("assignerId", assignerId);
                    }
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }

                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForMyApprovalOver", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                if (null != resultList && resultList.size() > 0) {
                    for (ProcessSearch result : resultList) {
                        String instanceId = result.getProcessInstanceId();
                        if (SystemConstant.PROCESS_COMPLETE_0.equals(mapStatus.get(instanceId))) {
                            //流程未完成的话，获取流程当前节点信息
                            List<OaTaskInstance> currentTaskList = this.oaPafService.getActiveTaskByInstance(instanceId);
                            if (currentTaskList != null && currentTaskList.size() > 0) {
                                OaTaskInstance currentTask = currentTaskList.get(0);
                                if (null != currentTask) {
                                    result.setCurrentNoteName(currentTask.getName());
                                }
                            }
                        }
                    }
                }

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("审批过的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 要督办的
     * @author guoqingfu
     * @date 2013-10-18下午12:05:09
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForSupervise", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForSupervise(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();

            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            //状态(overOneWeek超过1星期、overThreeDay超过3天、overOneDay超过1天、normal正常审批无延误)
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status)) {
                Map<String, String> map = getTimeRange("status", status);
                conditions.setCreateStartDate(map.get("createStartDate"));
                conditions.setCreateEndDate(map.get("createEndDate"));
            }

            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                conditions.setMinDueDate(map.get("dueTimeBegin"));
                conditions.setMaxDueDate(map.get("dueTimeEnd"));
            }

            //收到时间、代理人和被代理人不需要处理

            //查询某人要督办的流程列表
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessSuperviseTaskByUserId(pin, "1", id, conditions);

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }
                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForSupervise", pageWrapper.getConditionsMap());

                //获取当前任务节点（要督办的查询出来的结果本身就是未完成的，不需要再进行状态的判断）
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                List<OaTaskInstance> currentTaskList = null;
                OaTaskInstance currentTask = null;
                if(null != resultList && resultList.size()>0){
    				Map<String,Object> map = new HashMap<String,Object>();
    				for(ProcessSearch result:resultList){
    					currentTaskList = this.oaPafService.getActiveTaskByInstance(result.getProcessInstanceId());
    					if(null != currentTaskList && currentTaskList.size() > 0){
    						currentTask = currentTaskList.get(0);
    						result.setCurrentNoteName(currentTask.getName());
    						result.setCurrentTaskNoteInfo(currentTask);
    						result.setBySignerUserId(currentTask.getAddSignerUserId());//加签人
    					}
    				}
    			}

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("要督办的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 督办过的
     * @author guoqingfu
     * @date 2013-10-18下午12:15:51
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForSuperviseOver", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForSuperviseOver(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                conditions.setMinDueDate(map.get("dueTimeBegin"));
                conditions.setMaxDueDate(map.get("dueTimeEnd"));
            }

            //收到时间、代理人和被代理人不需要处理

            //查询某人督办过的流程列表
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessSuperviseTaskByUserId(pin, "2", id, conditions);

            //流程状态(按要求按着审批过的方法先取出流程状态，这时保证每个实例只有一个记录)
            Map<String, String> mapStatus = null;
            if (processSearchList != null && processSearchList.size() > 0) {
                mapStatus = new HashMap<String, String>(processSearchList.size());
                for (ProcessSearch search : processSearchList) {
                    mapStatus.put(search.getProcessInstanceId(), search.getIsComplete());
                }
            }

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //优先级（2特急申请、1紧急申请、0标准申请、全部申请），对应数据字典编码(指页面中的状态)
                String priority = request.getParameter("status");
                if (StringUtils.isNotEmpty(priority)) {
                    if ("particularlyUrgentApplication2".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_2);
                    } else if ("urgentApplication2".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_1);
                    } else if ("standardApplication2".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_0);
                    } else {
                        pageWrapper.addSearch("priority", null);
                    }
                }

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }
                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForSuperviseOver", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                List<OaTaskInstance> currentTaskList = null;
                OaTaskInstance currentTask = null;
                if(null != resultList && resultList.size()>0){
    				for(ProcessSearch result:resultList){
    					if (SystemConstant.PROCESS_COMPLETE_0.equals(mapStatus.get(result.getProcessInstanceId()))) {
    						currentTaskList = this.oaPafService.getActiveTaskByInstance(result.getProcessInstanceId());
    						if(null != currentTaskList && currentTaskList.size() > 0){
    							currentTask = currentTaskList.get(0);
    							result.setCurrentNoteName(currentTask.getName());
    							result.setCurrentTaskNoteInfo(currentTask);
    							result.setBySignerUserId(currentTask.getAddSignerUserId());//加签人
    						}
    					}
    				}
    			}

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("督办过的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 要代别人审批的
     * @author guoqingfu
     * @date 2013-10-17上午11:31:16
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForInsteadApproval", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForInsteadApproval(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            //收到时间
            String receiveTime = request.getParameter("receiveTime");
            if (StringUtils.isNotEmpty(receiveTime)) {
                Map<String, String> map = getTimeRange("receiveTime", receiveTime);
                conditions.setCreateStartDate(map.get("receiveTimeBegin"));
                conditions.setCreateEndDate(map.get("receiveTimeEnd"));
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                conditions.setMinDueDate(map.get("dueTimeBegin"));
                conditions.setMaxDueDate(map.get("dueTimeEnd"));
            }

            //被代理人
            String proxiedId = request.getParameter("proxiedId");

            //查询流程代办列表，代替别人审批的
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessTaskListByUserId(pin, 2, id, proxiedId, conditions);

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //优先级（2特急申请、1紧急申请、0标准申请、全部申请），对应数据字典编码(指页面中的状态)
                String priority = request.getParameter("status");
                if (StringUtils.isNotEmpty(priority)) {
                    if ("particularlyUrgentApplication3".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_2);
                    } else if ("urgentApplication3".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_1);
                    } else if ("standardApplication3".equalsIgnoreCase(priority)) {
                        pageWrapper.addSearch("priority", SystemConstant.PROCESS_PRIORITY_0);
                    } else {
                        pageWrapper.addSearch("priority", null);
                    }
                }

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }

                //带别人审批的不需要有代理人            /*//代理人
                String proxyId = request.getParameter("proxyId");
                if(StringUtils.isNotEmpty(proxyId)){
                    pageWrapper.addSearch("proxyId", proxyId);
                    //代理人中的原处理人ID
                    if (context != null && StringUtils.isNotEmpty(pin)) {
                        String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                        pageWrapper.addSearch("assignerId", assignerId);
                    }
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }
                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForInsteadApproval", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                if(null != resultList && resultList.size()>0){
    				Map<String,Object> map = new HashMap<String,Object>();
    				for(ProcessSearch pSearch: processSearchList){
    					String piId = pSearch.getProcessInstanceId();
    					OaTaskInstance currentTaskNoteInfo = pSearch.getCurrentTaskNoteInfo();
    					if(null != currentTaskNoteInfo){
    						map.put(piId, pSearch);
    					}else{
    						map.put(piId, null);
    					}
    				}
    				for(ProcessSearch result:resultList){
    					ProcessSearch allBean = (ProcessSearch)map.get(result.getProcessInstanceId());
    					if(null != allBean){
    						if(allBean.getCurrentTaskNoteInfo() != null){
    							result.setCurrentNoteName(allBean.getCurrentTaskNoteInfo().getName());
    							result.setCurrentTaskNoteInfo(allBean.getCurrentTaskNoteInfo());
    							result.setAssignee(allBean.getAssignee());
    							result.setIsProxy(allBean.getIsProxy());
    						}
    						result.setBySignerUserId(allBean.getBySignerUserId());
    					}
    				}
    			}

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("要代别人审批的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 代别人审批过的
     * @author guoqingfu
     * @date 2013-10-17上午11:41:17
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForInsteadApprovalOver", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForInsteadApprovalOver(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            ProcessTaskHistory paramHistory = new ProcessTaskHistory();

            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();
            paramHistory.setUserId(pin);

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            paramHistory.setPafProcessDefinitionId(id);

            //收到时间
            String receiveTime = request.getParameter("receiveTime");
            if (StringUtils.isNotEmpty(receiveTime)) {
                Map<String, String> map = getTimeRange("receiveTime", receiveTime);
                paramHistory.setBeginTimeStart(map.get("receiveTimeBegin"));
                paramHistory.setBeginTimeEnd(map.get("receiveTimeEnd"));
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                paramHistory.setEndTimeStart(map.get("dueTimeBegin"));
                paramHistory.setEndTimeEnd(map.get("dueTimeEnd"));
            }

            //状态(haveHandled2表示已办；handleOver2表示办结)
            String completed = "0";
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status)) {
                if ("handleOver2".equalsIgnoreCase(status)) {
                    completed = "1";
                }
            }
            paramHistory.setInstanceIscomplete(completed);

            //被代理人
            String proxiedId = request.getParameter("proxiedId");

            //查询某人审批过的流程
            List<ProcessSearch> processSearchList = processSearchService.findPrcocessTaskOverByUserId(2, proxiedId, paramHistory, conditions);
            //流程状态
            Map<String, String> mapStatus = null;
            if (processSearchList != null && processSearchList.size() > 0) {
                mapStatus = new HashMap<String, String>(processSearchList.size());
                for (ProcessSearch search : processSearchList) {
                    mapStatus.put(search.getProcessInstanceId(), search.getIsComplete());
                }
            }

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }

                //代别人审批过的不需要有代理人
    			/*//代理人
    			String proxyId = request.getParameter("proxyId");
    			if(StringUtils.isNotEmpty(proxyId)){
    				pageWrapper.addSearch("proxyId", proxyId);
    				//代理人中的原处理人ID
    				if (context != null && StringUtils.isNotEmpty(pin)) {
    					String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);       
    					pageWrapper.addSearch("assignerId", assignerId);
    				}
    			}*/

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }

                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForInsteadApprovalOver", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                if (null != resultList && resultList.size() > 0) {
                    for (ProcessSearch result : resultList) {
                        String instanceId = result.getProcessInstanceId();
                        if (SystemConstant.PROCESS_COMPLETE_0.equals(mapStatus.get(instanceId))) {
                            //流程未完成的话，获取流程当前节点信息
                            List<OaTaskInstance> currentTaskList = this.oaPafService.getActiveTaskByInstance(instanceId);
                            if (currentTaskList != null && currentTaskList.size() > 0) {
                                OaTaskInstance currentTask = currentTaskList.get(0);
                                if (null != currentTask) {
                                    result.setCurrentNoteName(currentTask.getName());
                                }
                            }
                        }
                    }
                }

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("代别人审批过的查询失败",e);
        }
    }

    /**
     * @param request
     * @return
     * @throws Exception Object
     * @Description: 别人加签的
     * @author guoqingfu
     * @date 2013-10-17下午01:59:20
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findPageForForward", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForForward(HttpServletRequest request) throws Exception {
    	try{
    		// 创建pageWrapper
            PageWrapper<ProcessSearch> pageWrapper = new PageWrapper<ProcessSearch>(request);
            Conditions conditions = new Conditions();
            ProcessTaskHistory paramHistory = new ProcessTaskHistory();

            List<ProcessSearch> processSearchList = null;
            //登录名称
            LoginContext context = LoginContext.getLoginContext();
            String pin = context.getPin();
            paramHistory.setUserId(pin);

            //paf返回的流程定义ID
            String id = request.getParameter("id");
            if (StringUtils.isEmpty(id)) {
                String processTypeId = request.getParameter("processTypeId");
                pageWrapper.addSearch("processTypeId", processTypeId);
            }
            paramHistory.setPafProcessDefinitionId(id);

            //收到时间
            String receiveTime = request.getParameter("receiveTime");
            if (StringUtils.isNotEmpty(receiveTime)) {
                Map<String, String> map = getTimeRange("receiveTime", receiveTime);
                paramHistory.setBeginTimeStart(map.get("receiveTimeBegin"));
                paramHistory.setBeginTimeEnd(map.get("receiveTimeEnd"));
            }
            //处理时间
            String dueTime = request.getParameter("dueTime");
            if (StringUtils.isNotEmpty(dueTime)) {
                Map<String, String> map = getTimeRange("dueTime", dueTime);
                paramHistory.setEndTimeStart(map.get("dueTimeBegin"));
                paramHistory.setEndTimeEnd(map.get("dueTimeEnd"));
            }

            //被代理人
            String proxiedId = request.getParameter("proxiedId");

            //状态(Untreated表示未处理；handled表示已处理)
            String status = request.getParameter("status");
            if (StringUtils.isNotEmpty(status) && "handled".equalsIgnoreCase(status)) {
                //已处理(包括流程实例已完成的和未完成的)
                processSearchList = processSearchService.findPrcocessTaskOverByUserId(3, proxiedId, paramHistory, conditions);
            } else {
                //未处理（查询流程代办列表，别人加签的）
                processSearchList = processSearchService.findPrcocessTaskListByUserId(pin, 3, id, proxiedId, conditions);
            }
            //流程状态
            Map<String, String> mapStatus = null;
            if (processSearchList != null && processSearchList.size() > 0) {
                mapStatus = new HashMap<String, String>(processSearchList.size());
                for (ProcessSearch search : processSearchList) {
                    mapStatus.put(search.getProcessInstanceId(), search.getIsComplete());
                }
            }

            if (null != processSearchList && processSearchList.size() > 0) {
                pageWrapper.addSearch("processSearchList", processSearchList);

                //申请人
                String starterId = request.getParameter("starterId");
                if (StringUtils.isNotEmpty(starterId)) {
                    pageWrapper.addSearch("starterId", starterId);
                }

                //代理人
                String proxyId = request.getParameter("proxyId");
                if (StringUtils.isNotEmpty(proxyId)) {
                    pageWrapper.addSearch("proxyId", proxyId);
                    //代理人中的原处理人ID
                    if (context != null && StringUtils.isNotEmpty(pin)) {
                        String assignerId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
                        pageWrapper.addSearch("assignerId", assignerId);
                    }
                }

                //申请时间
                String beginTime = request.getParameter("beginTime");
                if (StringUtils.isNotEmpty(beginTime)) {
                    this.getApplyTimeRange(pageWrapper, "beginTime", beginTime);
                }
                //主题
                String processInstanceName = request.getParameter("processInstanceName");
                if (StringUtils.isNotEmpty(processInstanceName)) {
                    pageWrapper.addSearch("processInstanceName", processInstanceName);
                }

                //机构类型（用于关联查询申请人姓名和部门）
                pageWrapper.addSearch("organizationType", SystemConstant.ORGNAIZATION_TYPE_HR);

                processSearchService.find(pageWrapper.getPageBean(), "findPageForForward", pageWrapper.getConditionsMap());

                //获取当前任务节点
                List<ProcessSearch> resultList = (List<ProcessSearch>) pageWrapper.getResult().get("aaData");
                if (null != resultList && resultList.size() > 0) {
                    if (StringUtils.isNotEmpty(status) && "handled".equalsIgnoreCase(status)) {
                        //查询已处理的 需要去paf端获取当前流程的当前任务节点
                        for (ProcessSearch result : resultList) {
                            String instanceId = result.getProcessInstanceId();
                            if (SystemConstant.PROCESS_COMPLETE_0.equals(mapStatus.get(instanceId))) {
                                //流程未完成的话，获取流程当前节点信息
                                List<OaTaskInstance> currentTaskList = this.oaPafService.getActiveTaskByInstance(instanceId);
                                if (currentTaskList != null && currentTaskList.size() > 0) {
                                    OaTaskInstance currentTask = currentTaskList.get(0);
                                    if (null != currentTask) {
                                        result.setCurrentNoteName(currentTask.getName());
                                        result.setCurrentTaskNoteInfo(currentTask);
                                    }
                                } else {
                                	result.setCurrentTaskNoteInfo(new OaTaskInstance());
                                }
                            }
                        }

                    } else {
                        //查询未处理的即代办，当前任务节点已经返回
                        Map<String, Object> map = new HashMap<String, Object>();
                        for(ProcessSearch pSearch: processSearchList){
        					String piId = pSearch.getProcessInstanceId();
        					OaTaskInstance currentTaskNoteInfo = pSearch.getCurrentTaskNoteInfo();
        					if(null != currentTaskNoteInfo){
        						map.put(piId, pSearch);
        					}else{
        						map.put(piId, null);
        					}
        				}
        				for(ProcessSearch result:resultList){
        					ProcessSearch allBean = (ProcessSearch)map.get(result.getProcessInstanceId());
        					if(null != allBean){
        						if(allBean.getCurrentTaskNoteInfo() != null){
        							result.setCurrentNoteName(allBean.getCurrentTaskNoteInfo().getName());
        							result.setCurrentTaskNoteInfo(allBean.getCurrentTaskNoteInfo());
        						}
        						result.setBySignerUserId(allBean.getBySignerUserId());
        					}
        				}
                    }
                }

                String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
                logger.debug(json);
                return json;
            }
            String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
            logger.debug(json);
            return json;
    	}catch(Exception e){
        	logger.error(e);
			throw new BusinessException("别人加签的查询失败",e);
        }
        
    }


    /**
     * @param request
     * @return String
     * @Description: 取消流程实例(状态是草稿)
     * @author guoqingfu
     * @date 2013-10-23下午08:08:27
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_cancelApplyForDraft", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String cancelApplyForDraft(HttpServletRequest request) {
        //草稿状态的流程是没有实例ID的，用的是实例表的主键
        String processInstanceKey = request.getParameter("processInstanceKey");
        Map<String, Object> map = processSearchService.cancelApplyForDraft(processInstanceKey);
        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param request
     * @return String
     * @Description: 取消流程实例(状态是驳回)
     * @author guoqingfu
     * @date 2013-10-24上午10:51:39
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_cancelApplyForRejected", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String cancelApplyForRejected(HttpServletRequest request) {
        String processInstanceId = request.getParameter("processInstanceId");
        boolean cancelProcess = oaPafService.cancelProcessInstance(ComUtils.getLoginNamePin(), processInstanceId);//paf 端关掉流程实例
        Map<String,Object> map = new HashMap<String,Object>();
        if(cancelProcess){
        	map = processSearchService.cancelApplyForRejected(processInstanceId);
        } else {
        	map.put("operator", false);
    		map.put("message", "关闭流程失败!");
        }
        return JSONObject.fromObject(map).toString();
    }

    /**
     * @param request
     * @return boolean: true表示被审批过；false表示没有被领导审批过
     * @Description: 查询流程实例提交后是否被领导审批
     * @author guoqingfu
     * @date 2013-10-24上午11:20:01
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_findProcessTaskHistory", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public boolean findProcessTaskHistory(HttpServletRequest request) {
        String processInstanceId = request.getParameter("processInstanceId");
        return processSearchService.findProcessTaskHistory(processInstanceId);
    }

    /**
     * @param request
     * @return String
     * @Description: 取消流程实例(提交后但领导还没审批)
     * @author guoqingfu
     * @date 2013-10-24上午10:52:45
     * @version V1.0
     */
    @RequestMapping(value = "/processSearch_cancelApplyForUnapproved", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String cancelApplyForUnapproved(HttpServletRequest request) {
        String processInstanceId = request.getParameter("processInstanceId");
        Map<String, Object> map = processSearchService.cancelApplyForUnapproved(processInstanceId);
        return JSONObject.fromObject(map).toString();
    }
    
    
    /** 流程查询初始化
     * @param processTypeId
     * @param processTypeName
     * @param taskType
     * @param processDefinitionId
     * @param processDefinitionKey
     * @param addInfo
     * @return
     * @version V2.0
     * @author yujiahe
     * @date 2013-11-02下午02:42:41
     */
    @RequestMapping(value = "/processSearch_index1", method = RequestMethod.GET)
    public ModelAndView processSearch_index(String processTypeId, String processTypeName, String taskType, String processDefinitionId, String processDefinitionKey, String addInfo) {
        try{
        	//=============================add by yjh start========================================================
            String processTypeIdForQuery = "";//分类 对应dataInfo.dictCode
            String proxiedPersonId = "";//被代理人ID
            String proxiedPerson = ""; //被代理人姓名
            String processStatueForQuery = "";//前台--状态
            if (StringUtils.isEmpty(processTypeId)) { //如果没有传processTypeId,跳过来的话，反查ID
                ProcessType processType = processTypeService.findByProcessDefId(processDefinitionId);
                if(processType!=null){
                    processTypeId = processType.getId();
                }

            }

            if ("owner".equals(taskType)) { //本人的
                processTypeIdForQuery = "tobeapproved"; //要审批的
                processStatueForQuery = "allApplication"; //所有申请
            } else if ("addsigner".equals(taskType)) {//加签的
                processTypeIdForQuery = "othersForwarding";//别人加签的
                processStatueForQuery = "allApplication"; //未处理
            } else if ("proxy".equals(taskType)) {//代理的
                processTypeIdForQuery = "toBeApprovalForOthers";//要待别人审批的
                SysUser sysUser=sysUserService.getByUserName(addInfo);
                proxiedPersonId = sysUser==null?"":sysUser.getId(); //被代理人ID
                proxiedPerson = sysUser==null?"":sysUser.getRealName(); //被代理人姓名
                processStatueForQuery = "allApplication3";//所有申请
            } else if ("supervise".equals(taskType)) {//督办的
                processTypeIdForQuery = "toBeSupervise";//要督办的
                processStatueForQuery = addInfo;
            }
            //========================== add by yjh end =================================================


            ModelAndView mav = new ModelAndView("app/app_processSearch_index1");

//            Map<String, String> map = new HashMap<String, String>();
//           String  parentId= processTypeService.get(processTypeId).getParentId();
//           if("".equals(parentId)||parentId==null){
//               map.put("parentId", processTypeId);
//           } else{
//               map.put("parentId", parentId);
//           }
           

//            List<ProcessType> processTypeList = processTypeService.find("findProcessName", map);
//            int processTypeListSize = processTypeList.size();

            List<DictData> dictDataList = dictDataService.findDictDataList(AppConstant.PROCESSCLASSIFICATION);

            //申请人
            LoginContext context = LoginContext.getLoginContext();
            String pin = null;
            if(context!=null){
            	pin = context.getPin();
            }
            String sysUserId = new String();
            if (StringUtils.isNotEmpty(pin)) {
                sysUserId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
            }
            //获取该用户有权申请的流程定义ID
            List<SysBussinessAuthExpression> listProcessDefId = userAuthorizationService.getResourceByUserId(sysUserId, "2");
            Map<String, Object> authMap = new HashMap<String, Object>();
            if (null != listProcessDefId && listProcessDefId.size() > 0) {
                for (SysBussinessAuthExpression authExpression : listProcessDefId) {
                    authMap.put(authExpression.getBusinessId(), authExpression);
                }
            }

            List<ProcessSearch> processDefinitionList = new ArrayList<ProcessSearch>();
            List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeId);
            if (null != processDefinitionListInfo && processDefinitionListInfo.size() > 0) {
                for (ProcessSearch processSearch : processDefinitionListInfo) {
                    SysBussinessAuthExpression sysAuthExpression = (SysBussinessAuthExpression) authMap.get(processSearch.getId());
                    if (null != sysAuthExpression) {
                        processDefinitionList.add(processSearch);
                    }
                }
            }
            mav.addObject("processDefinitionListAll", processDefinitionListInfo);
            mav.addObject("processDefinitionList", processDefinitionList);
            mav.addObject("processDefinitionListSize", processDefinitionList.size());
            mav.addObject("processTypeInfoId", processTypeId);
//            if (processTypeListSize == 0) {
//                //通过类别ID查询流程信息,用于页面显示流程名称
//                List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeId);
//                if (null != processDefinitionListInfo && processDefinitionListInfo.size() > 0) {
//                    for (ProcessSearch processSearch : processDefinitionListInfo) {
//                        SysBussinessAuthExpression sysAuthExpression = (SysBussinessAuthExpression) authMap.get(processSearch.getId());
//                        if (null != sysAuthExpression) {
//                            processDefinitionList.add(processSearch);
//                        }
//                    }
//                }
//                mav.addObject("processDefinitionListAll", processDefinitionListInfo);
//                mav.addObject("processDefinitionList", processDefinitionList);
//                mav.addObject("processDefinitionListSize", processDefinitionList.size());
//                mav.addObject("processTypeInfoId", processTypeId);
//            } else {
//                List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeList.get(0).getId());
//                if (null != processDefinitionListInfo && processDefinitionListInfo.size() > 0) {
//                    for (ProcessSearch processSearch : processDefinitionListInfo) {
//                        SysBussinessAuthExpression sysAuthExpression = (SysBussinessAuthExpression) authMap.get(processSearch.getId());
//                        if (null != sysAuthExpression) {
//                            processDefinitionList.add(processSearch);
//                        }
//                    }
//                }
//                mav.addObject("processDefinitionListAll", processDefinitionListInfo);
//                mav.addObject("processDefinitionList", processDefinitionList);//流程定义
//                mav.addObject("processDefinitionListSize", processDefinitionList.size());
//                mav.addObject("processTypeInfoId", processTypeList.get(0).getId());
//            }
            mav.addObject("processTypeListSize", 0);
            mav.addObject("dictDataList", dictDataList); //分类
            mav.addObject("processTypeName", processTypeName); //流程类别名称                                                              s
            mav.addObject("processTypeList",null);
            //=============================add by yjh start========================================================
            mav.addObject("pafProcessDefinitionIdForQuery", processDefinitionKey);//对应页面上的 流程名称
            mav.addObject("processTypeIdForQuery", processTypeIdForQuery);//对应页面上的 分类 对应dataInfo.dictCode
            mav.addObject("processStatueForQuery", processStatueForQuery);//前台--状态
            mav.addObject("proxiedPersonIdForQuery", proxiedPersonId);//被代理人ID
            mav.addObject("proxiedPersonForQuery", proxiedPerson);//被代理人 姓名
            //========================== add by yjh end =================================================
            return mav;
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("流程查询初始化失败",e);
        }
    }
    
    /**
     * 
     * @desc 业务数据查询 页面
     * @author WXJ
     * @date 2014-4-16 下午06:53:25
     *
     * @param processTypeId
     * @param processTypeName
     * @param taskType
     * @param processDefinitionId
     * @param processDefinitionKey
     * @param addInfo
     * @return
     */
    @RequestMapping(value = "/businessSearch_index", method = RequestMethod.GET)
    public ModelAndView businessSearch_index(String processTypeId, String processTypeName, String taskType, String processDefinitionId, String processDefinitionKey, String addInfo) {
        try{
        	
            String processTypeIdForQuery = "";//分类 对应dataInfo.dictCode
            String processStatueForQuery = "";//前台--状态
            if (StringUtils.isEmpty(processTypeId)) { //如果没有传processTypeId,跳过来的话，反查ID
                ProcessType processType = processTypeService.findByProcessDefId(processDefinitionId);
                if(processType!=null){
                    processTypeId = processType.getId();
                }

            }

            ModelAndView mav = new ModelAndView("app/app_businessSearch_index");
            
            processTypeIdForQuery = "tobeapproved"; //要审批的
            processStatueForQuery = "allApplication"; //所有申请
            
            //申请人
            LoginContext context = LoginContext.getLoginContext();
            String pin = null;
            if(context!=null){
            	pin = context.getPin();
            }
           
            String sysUserId = "";
            if (StringUtils.isNotEmpty(pin)) {
                sysUserId = sysUserService.getUserIdByUserName(pin, SystemConstant.ORGNAIZATION_TYPE_HR);
            }
            //获取该用户有权申请的流程定义ID
            List<SysBussinessAuthExpression> listProcessDefId = userAuthorizationService.getResourceByUserId(sysUserId, "2");
            Map<String, Object> authMap = new HashMap<String, Object>();
            if (null != listProcessDefId && listProcessDefId.size() > 0) {
                for (SysBussinessAuthExpression authExpression : listProcessDefId) {
                    authMap.put(authExpression.getBusinessId(), authExpression);
                }
            }

            List<ProcessSearch> processDefinitionList = new ArrayList<ProcessSearch>();
            List<ProcessSearch> processDefinitionListInfo = processSearchService.findPrcocessListByTypeId(processTypeId);
            if (null != processDefinitionListInfo && processDefinitionListInfo.size() > 0) {
                for (ProcessSearch processSearch : processDefinitionListInfo) {
                    SysBussinessAuthExpression sysAuthExpression = (SysBussinessAuthExpression) authMap.get(processSearch.getId());
                    if (null != sysAuthExpression) {
                        processDefinitionList.add(processSearch);
                    }
                }
            }
            mav.addObject("processDefinitionListAll", processDefinitionListInfo);
            mav.addObject("processDefinitionList", processDefinitionList);
            mav.addObject("processDefinitionListSize", processDefinitionList.size());
            mav.addObject("processTypeInfoId", processTypeId);
            mav.addObject("processTypeListSize", 0);
            mav.addObject("processTypeName", processTypeName); //流程类别名称                                                              s
            mav.addObject("processTypeList",null);
            //=============================add by yjh start========================================================
            mav.addObject("pafProcessDefinitionIdForQuery", processDefinitionKey);//对应页面上的 流程名称
            mav.addObject("processTypeIdForQuery", processTypeIdForQuery);//对应页面上的 分类 对应dataInfo.dictCode
            mav.addObject("processStatueForQuery", processStatueForQuery);//前台--状态
            //========================== add by yjh end =================================================
            return mav;
        }catch(Exception e){
        	logger.error(e);
			throw new BusinessException("业务数据查询初始化失败",e);
        }
    }
    
    /**
     * 
     * @desc 业务数据查询 分页
     * @author WXJ
     * @date 2014-4-16 下午06:55:30
     *
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/businessSearch_findPageForMyApply", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object findPageForMyApplyByBusiness(HttpServletRequest request) throws Exception {
    	try{
    		String id = request.getParameter("id");//
    		String filters = request.getParameter("filters");//业务数据查询条件List<FormItem>
    		String status = request.getParameter("status");
    		String startID = request.getParameter("applyPersonId");
    		String processInstanceName = request.getParameter("processInstanceName");
    		String beginTimeBegin = request.getParameter("beginTimeBegin");
    		String beginTimeEnd = request.getParameter("beginTimeEnd");
    		//获取流程定义
    		ProcessDefinition processDefinition = processDefService.getByDefinitionId(id);
    		//流程定义表单获取
    		Form form = getFormByProcessDefinitionId(processDefinition);
    		//获取流程业务数据关键字段
    		List<ProcessDefinitionConfig> listPDC = null;
    		ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
    		pdc.setProcessDefinitionId(processDefinition.getId());
    		pdc.setConfigType("1");
    		listPDC = processDefinitionConfigService.find(pdc);
    		
			MyBatisDaoImpl mbdl = new MyBatisDaoImpl();
			 PageWrapper<Map<String ,Object>> pageWrapper = new PageWrapper<Map<String ,Object>>(request);
			 //获取动态查询分页SQL
			 String sql = processSearchService.findBusinessData(id,filters,status,startID, processInstanceName, beginTimeBegin, beginTimeEnd);
	    	 mbdl.find(pageWrapper.getPageBean(), sql,pageWrapper.getConditionsMap(),1);
			//业务数据翻译
			String contextPath = request.getSession().getServletContext().getRealPath("/");
			Object aaData = pageWrapper.getResult().get("aaData");
			FormUIUtil formUIUtil=new FormUIUtil();
			for(Map<String,Object> data :(List<Map<String,Object>>)aaData){
				for (ProcessDefinitionConfig PDC : listPDC) {
					FormItem FI = new FormItem();
					FI.setFieldName(PDC.getConfigItem());
					FI.setFormId(form.getId());
					FormItem Item = formItemService.find(FI).get(0);
					if(data.get(PDC.getConfigItem())!=null&&StringUtils.isNotEmpty(data.get(PDC.getConfigItem()).toString())){
						Map<String,String> businessMap=formUIUtil.buildMasterHashtableOfVerify(contextPath, Item,data.get(PDC.getConfigItem()).toString());	
						data.put(PDC.getConfigItem(), businessMap.get(PDC.getConfigItem()));
					}
				}
			}
			String json = JsonUtils.toJsonByGoogle(pageWrapper.getResult());
			return json;
		} catch (Exception e) {
			logger.error(e);
			throw new BusinessException("我的申请查询失败", e);
		}
        
    }
    /**
     * 根据流程实例processDefinitionKey 返回动态查询的表单项列表（无附件）
     * @author yujiahe
     * @param id
     * @return
     */
    @RequestMapping(value = "/businessSearch_findItemsByPDK")
    @ResponseBody
    public String findItemsByPDK(String id) {
    	ProcessDefinition processDefinition = processDefService.getByDefinitionId(id);
		Form form =getFormByProcessDefinitionId(processDefinition);
		List<FormItem> items = formItemService.selectByFormId(form.getId());
		List<FormItem> noAttachmentItems = new ArrayList<FormItem>();
		for(FormItem FI :items){
			if(!FI.getInputType().equals("附件")){
				noAttachmentItems.add(FI);
			}
		}
        String json = JsonUtils.toJsonByGoogle(noAttachmentItems);
        return json;
    }
    
    @RequestMapping(value = "/findQueryItemsName")
    @ResponseBody
    public String findQueryItemsName(String id) {
    	String itemsName = "";
    	ProcessDefinition processDefinition = processDefService.getByDefinitionId(id);
    	List<ProcessDefinitionConfig> listPDC = null;
		ProcessDefinitionConfig pdc = new ProcessDefinitionConfig();
		pdc.setProcessDefinitionId(processDefinition.getId());
		pdc.setConfigType("1");

		listPDC = processDefinitionConfigService.find(pdc);
		for (ProcessDefinitionConfig PDC : listPDC) {
			itemsName += PDC.getConfigItem()+",";
		}
        return itemsName;
    }
    /**
     * 根据流程实例的processDefinitionKey 和表单项ID 返回相应查询代码块
     * @author yujiahe
     * @param request
     * @param processDefinitionKey
     * @param itemId
     * @return
     */
    @RequestMapping(value = "/businessSearch_getHtmlForQuery")
    @ResponseBody
    public String getHtmlForQuery(HttpServletRequest request,String processDefinitionKey,String itemId) {
    	String json = applicationService.getDynamicQueryConditionsHtml(request, processDefinitionKey,itemId);
        return json;
    }
    
    public Form getFormByProcessDefinitionId(ProcessDefinition processDefinition) throws BusinessException{
		if(processDefinition != null){
			if(processDefinition.getFormId() == null || processDefinition.getFormId().trim().length() == 0){
				throw new BusinessException("指定的流程未绑定表单");
			}
			return formService.get(processDefinition.getFormId());
		} else {
			throw new BusinessException("指定的流程定义KEY不存在");
		}
	}
    
    
    
    
}

package com.jd.oa.app.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.reflect.TypeToken;
import com.jd.common.util.DateFormatUtils;
import com.jd.oa.agent.OaUniteToDoImpl;
import com.jd.oa.agent.UniteToDoInterface;
import com.jd.oa.agent.service.AgentTodoListService;
import com.jd.oa.agent.service.MqProduceEbsService;
import com.jd.oa.agent.service.MqProduceUimService;
import com.jd.oa.agent.service.impl.UniteToDoAbst;
import com.jd.oa.app.dto.ProcessApplyDefaultValueDto;
import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.app.service.OaPafService;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormSequenceService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessAddsigner;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.model.ProcessNodeListener;
import com.jd.oa.process.service.ProcessAddsignerService;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeListenerService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

@Controller
@RequestMapping(value = "/app")
public class ProcessTaskController {
	private static final Logger logger = Logger.getLogger(ProcessTaskController.class);
	
	@Autowired
	private ProcessTaskService processTaskService;
    @Autowired
    private OaPafService oaPafService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private ProcessAddsignerService processAddsignerService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private ProcessInstanceService processInstanceService;
	
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	@Autowired
	private ProcessTaskHistoryService processTaskHistoryService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	
	@Autowired
	private DiDatasourceService diDatasourceService;
	@Autowired
	private DiCcInterfaceService diCcInterfaceService;
	
	@Autowired
	private ProcessNodeListenerService processNodeListenerService ;
	@Autowired
	private FormService formService;
	@Autowired
	private FormSequenceService formSequenceService;
	
	@Autowired
	private MqProduceEbsService mqProduceEbsService;
	
	@Autowired
	private MqProduceUimService mqProduceUimService;
	
	@Autowired
	private AgentTodoListService agentTodoListService;
	
	/**
	 * 我的审批页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/processTask_index", method=RequestMethod.GET)
	public ModelAndView processTaskIndex() throws Exception{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("app/app_processTask_index");
		return mv;
	}
	/**
	 * 我的审批列表
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processTask_list",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processTaskList(HttpServletRequest request,String taskType){
		Map<String,ProcessTaskDto> map=processTaskService.getTodoProcessTaskList(request,taskType);
		return map;
	}

    /**
     * 任务处理页面 - zhaoming
     * @param oaTask 任务实例Id
     * @return 待办任务处理页
     */
    @RequestMapping(value = "/taskHandle_index", method=RequestMethod.GET)
    public ModelAndView taskHandleIndex(String startTime,String oaTask){
        List<OaTaskInstance> oaTaskInstances = JsonUtils.fromJsonByGoogle(oaTask, new TypeToken<List<OaTaskInstance>>() {});
        ModelAndView mv = new ModelAndView("app/app_taskHandle_index");
       if(null != oaTaskInstances && oaTaskInstances.size() > 0) {// modify by liub @cause:添加常规的判断
            mv.addObject("processInstanceId", oaTaskInstances.get(0).getProcessInstanceId());//流程实例ID
            mv.addObject("taskInstanceId", oaTaskInstances.get(0).getId());//任务ID
            mv.addObject("processDefinitionId", oaTaskInstances.get(0).getProcessDefinitionId());
            mv.addObject("processDefinitionKey", oaTaskInstances.get(0).getProcessDefinitionKey());
            mv.addObject("nodeId", oaTaskInstances.get(0).getNodeId());
            mv.addObject("taskType", oaTaskInstances.get(0).getTaskType());
            mv.addObject("businessId",  oaTaskInstances.get(0).getBuinessInstanceId());
            mv.addObject("startTime",startTime);
		   mv.addObject("processDefinitionIsOuter",oaTaskInstances.get(0).getProcessDefinitionIsOuter());
            String processInstanceName = oaTaskInstances.get(0).getProcessInstanceName();
           if(StringUtils.isNotEmpty(processInstanceName)){
               try {
                   processInstanceName = java.net.URLDecoder.decode(processInstanceName,"UTF-8");
               } catch (UnsupportedEncodingException e) {
                   logger.error(e);
                   throw new BusinessException("转码失败!",e);
               }
               mv.addObject("processInstanceName",processInstanceName );
           }else{
               mv.addObject("processInstanceName","");
           }
            String taskname=oaTaskInstances.get(0).getName();
            if(StringUtils.isNotEmpty(taskname)){
                try {
                    taskname = java.net.URLDecoder.decode(taskname,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    logger.error(e);
                    throw new BusinessException("转码失败!",e);
                }
                mv.addObject("taskName",taskname );
            }else{
                mv.addObject("taskName","");
            }
            mv.addObject("addSignerUserId", oaTaskInstances.get(0).getAddSignerUserId());
            mv.addObject("assignerId", oaTaskInstances.get(0).getAssignerId());
            mv.addObject("isProxy", oaTaskInstances.get(0).getIsProxy());

            boolean isHide=false;
            ProcessNode node = new ProcessNode();
            node.setNodeId(oaTaskInstances.get(0).getNodeId());
            node.setProcessDefinitionId( oaTaskInstances.get(0).getProcessDefinitionId());
            List<ProcessNode> list=processNodeService.find(node);
            if(null!=list && list.size() > 0){
               if(StringUtils.isEmpty(list.get(0).getFormTemplateId())){
                   isHide=true;
               }
            }
            mv.addObject("isHide",isHide);

       }

        return mv;
    }
    
    /**
     * 
     * @desc 任务处理页面-Email超链接
     * @author WXJ
     * @date 2013-10-30 下午04:51:19
     * @return
     */
    @RequestMapping(value = "/taskHandle_email", method=RequestMethod.GET)
    public ModelAndView taskHandleEmail(String mailParam){   
//    	mailParam.substring(0, mailParam.length()-1);
    	String[] mailParamArray = mailParam.split(";"); 
    	com.jd.oa.common.utils.StringUtils util = new com.jd.oa.common.utils.StringUtils(mailParamArray[0]);
    	String taskId = util.split("|").get(1);
    	
    	util = new com.jd.oa.common.utils.StringUtils(mailParamArray[1]);
    	String processInstanceId = util.split("|").get(1);
    	
    	util = new com.jd.oa.common.utils.StringUtils(mailParamArray[2]);
    	String taskDefinitionKey = util.split("|").get(1);
    	
    	util = new com.jd.oa.common.utils.StringUtils(mailParamArray[3]);
    	String createTime = util.split("|").get(1);
    	
    	util = new com.jd.oa.common.utils.StringUtils(mailParamArray[4]);
    	String taskName = util.split("|").get(1);
    		
        ModelAndView mv = new ModelAndView("app/app_taskHandle_email");
        mv.addObject("processInstanceId", processInstanceId);
        mv.addObject("taskInstanceId", taskId);
        mv.addObject("nodeId", taskDefinitionKey);
        
        if (createTime==null || "".equals(createTime))
        	createTime = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");

        mv.addObject("startTime",createTime);//2014-05-23T13:48:48CST 类型
        //扩展属性
        Map mp = new HashMap();
        mp.put("processInstanceId", processInstanceId);
        ProcessInstance pi = new ProcessInstance();
        pi.setProcessInstanceId(processInstanceId);
        ProcessInstance processInstance = processInstanceService.selectByProcessInstanceId(pi);        
        ProcessDefinition processDefinition = null;
        if (processInstance!=null)
        	processDefinition = processDefService.getByDefinitionId(processInstance.getPafProcessDefinitionId());
        
        if (processDefinition!=null){
            mv.addObject("processDefinitionId", processDefinition.getId());
            mv.addObject("processDefinitionKey", processDefinition.getPafProcessDefinitionId());
            mv.addObject("businessId",  processInstance.getBusinessInstanceId());        	
        }
        
        ProcessAddsigner processAddsigner = processAddsignerService.getAddSignTask(processInstanceId, taskId, taskDefinitionKey);
        if (processAddsigner!=null) {
            mv.addObject("taskType", "addsigner");
            mv.addObject("addSignerUserId", processAddsigner.getAddsignUserId());
            mv.addObject("assignerId", processAddsigner.getUserId());        	
        }else{
        	mv.addObject("taskType", "owner");
        }
        
        String processInstanceName = processInstance.getProcessInstanceName();
        if(StringUtils.isNotEmpty(processInstanceName)){
            try {
                processInstanceName = java.net.URLDecoder.decode(processInstanceName,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                logger.error(e);
                throw new BusinessException("转码失败!",e);
            }
            mv.addObject("processInstanceName",processInstanceName );
        }else{
            mv.addObject("processInstanceName","");
        }
        
        String taskname="";        
        OaTaskInstance oti = oaPafService.getTaskDetail(taskId);
        if (oti!=null){    
        	taskname=oti.getName();
        }else{
        	taskname=taskName;
        }        	
        
        if(StringUtils.isNotEmpty(taskname)){
            try {
                taskname = java.net.URLDecoder.decode(taskname,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            mv.addObject("taskName",taskname );
        }else{
            mv.addObject("taskName","");
        }
              
        return mv;
    }
    
    /**
     * 
     * @desc 任务处理页面-重新申请
     * @author xulin
     * @date 2013-10-30 下午04:51:19
     * @return
     */
    @RequestMapping(value = "/taskHandle_againApply", method=RequestMethod.GET)
    public ModelAndView taskHandle_againApply(String taskId,String taskName,
    		String taskDefinitionKey,String createTime,String processInstanceId,String mark){    	
    	 ModelAndView mv = new ModelAndView("app/app_taskHandle_againApply");
         mv.addObject("processInstanceId", processInstanceId);
         mv.addObject("taskInstanceId", taskId);
         mv.addObject("nodeId", taskDefinitionKey);
         
         if (createTime==null || "".equals(createTime))
         	createTime = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
         mv.addObject("startTime",createTime);
         
         //扩展属性
         Map mp = new HashMap();
         mp.put("processInstanceId", processInstanceId);
         ProcessInstance pi = new ProcessInstance();
         pi.setProcessInstanceId(processInstanceId);
         ProcessInstance processInstance = processInstanceService.selectByProcessInstanceId(pi);        
         ProcessDefinition processDefinition = null;
         if (processInstance!=null)
         	processDefinition = processDefService.getByDefinitionId(processInstance.getPafProcessDefinitionId());
         
         if (processDefinition!=null){
             mv.addObject("processDefinitionId", processDefinition.getId());
             mv.addObject("processDefinitionKey", processDefinition.getPafProcessDefinitionId());
             mv.addObject("businessId",  processInstance.getBusinessInstanceId());        	
         }
         
         ProcessAddsigner processAddsigner = processAddsignerService.getAddSignTask(processInstanceId, taskId, taskDefinitionKey);
         if (processAddsigner!=null) {
             mv.addObject("taskType", "addsigner");
             mv.addObject("addSignerUserId", processAddsigner.getAddsignUserId());
             mv.addObject("assignerId", processAddsigner.getUserId());        	
         }else{
         	mv.addObject("taskType", "owner");
         }
         
         String processInstanceName = processInstance.getProcessInstanceName();
         if(StringUtils.isNotEmpty(processInstanceName)){
             try {
                 processInstanceName = java.net.URLDecoder.decode(processInstanceName,"UTF-8");
             } catch (UnsupportedEncodingException e) {
                 logger.error(e);
                 throw new BusinessException("转码失败!",e);
             }
             mv.addObject("processInstanceName",processInstanceName );
         }else{
             mv.addObject("processInstanceName","");
         }
         String taskname=taskName;
         if(StringUtils.isNotEmpty(taskname)){
             try {
                 taskname = java.net.URLDecoder.decode(taskname,"UTF-8");
             } catch (UnsupportedEncodingException e) {
                 e.printStackTrace();
             }
             mv.addObject("taskName",taskname );
         }else{
             mv.addObject("taskName","");
         }
         
         //许林
         mv.addObject("mark",mark);
         return mv;
    }
    
    /**
     * 获取按钮组
     * @param processInstanceId 流程实例ID
     * @param taskType 任务类型
     * @param processDefinitionId 流程定义ID
     * @param nodeId 节点ID
     * @param type : 统一待办/原待办
     * @return map
     */
    @RequestMapping(value = "/getButtons", method=RequestMethod.POST,  produces = "application/json")
    @ResponseBody
    public HashMap<String, Object> getButtons(String processInstanceId, String taskType, String processDefinitionId, String nodeId, String taskId,String type){
        return processTaskService.getButtons(processInstanceId, taskType, processDefinitionId, nodeId, taskId,type);
    }

    /**
     * 已办结流程实例历史查看页面
     * @param processInstanceId 流程实例ID
     * @return 已办结流程实例历史查看页面
     */
    @RequestMapping(value = "/endProcessInstance_index", method=RequestMethod.GET)
    public ModelAndView getEndProcessInstanceIndex(String processInstanceId){
        ModelAndView mv = new ModelAndView("app/app_endProcessInstance_index");
        mv.addObject("processInstanceId", processInstanceId);
		ProcessInstance processInstance = new ProcessInstance();
		processInstance.setProcessInstanceId(processInstanceId);
		processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
		ProcessDefinition processDefinition = processDefService.get(processInstance.getProcessDefinitionId());
		mv.addObject("processDefinitionIsOuter", processDefinition.getIsOuter());
		mv.addObject("businessId", processInstance.getBusinessInstanceId());
        return mv;
    }

    /**
     * 获取流程实例运行轨迹
     * @param processInstanceId 流程实例ID
     * @return 流程实例详细信息
     */
    @RequestMapping(value = "/processTrajectory", method=RequestMethod.POST,  produces = "application/json")
    @ResponseBody
    public List<ProcessTaskHistory> getProcessTrajectory(String processInstanceId){
        return processTaskService.getProcessTrajectory(processInstanceId);
    }


	
	/**
	 * 我的审批列表
	 * @param 
	 * @return
	 */
	@RequestMapping(value="/processTask_reload",method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public Object processTaskList(HttpServletRequest request,String processDefinitionId,String processDefinitionName,
			String processDefinitionKey,String tableName,String processColumnsString,String bussinessColumnString,String taskType){
		List<String> bussinessColumns=JsonUtils.fromJsonByGoogle(bussinessColumnString,new TypeToken<List<String>>(){});
		List<String> processColumns=JsonUtils.fromJsonByGoogle(processColumnsString,new TypeToken<List<String>>(){});
		ProcessTaskDto dto=processTaskService.getTodoProcessTaskList(request,processDefinitionId, processDefinitionName, processDefinitionKey, tableName, processColumns,bussinessColumns,taskType);
		return dto;
	}
	/**
	 * 从任务列表中打开电子表单 liub
	 * @param request
	 * @param processDefinitionKey 流程定义ID
	 * @param processInstanceId 流程业务实例ID
	 * @param processNodeId 流程当前任务节点ID
	 * @param type:(unite) 统一待办,(""/null/oa)OA待办
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_open_bindReport", method=RequestMethod.GET)
	public ModelAndView openExecuteWorklistBindReport(HttpServletRequest request,HttpServletResponse response,
			String processDefinitionKey,String businessObjectId,String processInstanceId,
			String processNodeId,String processInstanceStatus,String type) throws Exception{
	    ModelAndView mv = new ModelAndView();
	    if(businessObjectId == null){
	    	businessObjectId = "";
	    }
    	//oa待办
    	UniteToDoInterface uniteToDo = null;
    	if(StringUtils.isNotEmpty(type) && type.toLowerCase().equals("unite")){//统一待办
    		mv.setViewName("app/app_execute_worklist_bindReport_unite");
    		uniteToDo = new UniteToDoAbst();
    	} else {// oa待办
    		mv.setViewName("app/app_execute_worklist_bindReport");
    		uniteToDo = new OaUniteToDoImpl();
    	}
    	mv.addObject("templateHtml", uniteToDo.getExecuteWorklistBindReportFormContext(request,response,processDefinitionKey,businessObjectId, processNodeId,processInstanceId,processInstanceStatus));
    	mv.addObject("toolbar",uniteToDo.getBuninessOperationToolbar());
    	return mv;
	}


    //单独打开电子表单
    @RequestMapping(value = "/processApp_open_bindReport_alone", method=RequestMethod.GET)
    public ModelAndView openExecuteWorklistBindReportAlone(HttpServletRequest request,String processDefinitionKey,String businessObjectId,String processInstanceId,String processNodeId,String processInstanceStatus) throws Exception{
        ModelAndView mv = new ModelAndView();
        if(businessObjectId == null){
            businessObjectId = "";
        }
        mv.setViewName("app/app_execute_worklist_bindReport_alone");
        mv.addObject("templateHtml", applicationService.getExecuteWorklistBindReportFormContext(request,processDefinitionKey,businessObjectId, processNodeId,processInstanceId,processInstanceStatus));
        mv.addObject("toolbar",applicationService.getBuninessOperationToolbar());
        return mv;
    }



	/**
	 * 保存主表数据
	 * @param request
	 * @param boTableName
	 * @param businessObjectId
	 * @param datas
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_save_bindReport", method=RequestMethod.POST)
	@ResponseBody
	public String saveBindReport(HttpServletRequest request,String boTableName,String businessObjectId,String datas,String formId,String processNodeId) throws Exception{
		ProcessNode processNode = new ProcessNode();
		processNode.setNodeId(processNodeId);
		processNode.setIsFirstNode("1");
		if (processNodeService.findCount(processNode)>0){
			businessObjectId = applicationService.saveBusinessObject(boTableName,datas,businessObjectId,formId);
		}else{
			businessObjectId = applicationService.saveBusinessObject(boTableName,datas,businessObjectId,formId,processNodeId);
		}	    
	    return businessObjectId;
	}
	/**
	 * 获取子表数据
	 * @param boTableName 子表名称
	 * @param businessObjectId 主表业务数据ID
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_get_bindReportDataJson", method=RequestMethod.POST,produces = "application/json")
	@ResponseBody
	public Object getBindSubReportDataJson(HttpServletResponse response,String boTableName,String businessObjectId) throws Exception{
	    if(businessObjectId == null || businessObjectId.equals("")) return "[]";
		List<Map<String,Object>> subReportDatas =  applicationService.getBusinessDatas(boTableName,businessObjectId);
	    return subReportDatas;
	}
	/**
	 * 保存草稿
	 * @param boTableName
	 * @param businessObjectId
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_saveDraft", method=RequestMethod.POST)
	@ResponseBody
	public String saveProcessInstanceDraft(String processDefinitionKey,String boTableName,String businessObjectId,String processInstanceName,String taskPriority,String followCode) throws Exception{
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		String currentUser = sysUserService.getUserIdByUserName(ComUtils.getLoginNamePin(), SystemConstant.ORGNAIZATION_TYPE_HR);
		com.jd.oa.process.model.ProcessInstance oaProcessInstance = new com.jd.oa.process.model.ProcessInstance();
		oaProcessInstance.setBusinessInstanceId(businessObjectId);
		List<ProcessInstance> processInstances = processInstanceService.find(oaProcessInstance);
		if(processInstances != null && processInstances.size() > 0){
			if(processInstances.get(0) != null){
				oaProcessInstance = processInstances.get(0);
				if(oaProcessInstance != null){
					oaProcessInstance.setProcessDefinitionId(processDefinition.getId());
				    oaProcessInstance.setProcessInstanceName(processInstanceName);
					oaProcessInstance.setFollowCode(followCode);
				    oaProcessInstance.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
				    oaProcessInstance.setBeginTime(new Date());
				    oaProcessInstance.setStarterId(currentUser);
				    oaProcessInstance.setPriority(taskPriority);//任务优先级
				    oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_0);
				    int result = this.processInstanceService.update(oaProcessInstance);
				    return (String.valueOf(result));
				}
			}
		}
		oaProcessInstance.setProcessDefinitionId(processDefinition.getId());
	    oaProcessInstance.setProcessInstanceName(processInstanceName);
		oaProcessInstance.setFollowCode(followCode);
	    oaProcessInstance.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
	    oaProcessInstance.setBeginTime(new Date());
	    oaProcessInstance.setStarterId(currentUser);
	    oaProcessInstance.setPriority(taskPriority);//任务优先级
	    oaProcessInstance.setStatus(SystemConstant.PROCESS_STATUS_0);
	    String result = this.processInstanceService.insert(oaProcessInstance);
	    return result;
	}
	/**
	 * 保存子表数据
	 * @param request
	 * @param boTableName
	 * @param businessObjectId
	 * @param datas
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_save_bindReport_s", method=RequestMethod.POST)
	@ResponseBody
	public void saveBindSubReport(HttpServletRequest request,String boTableName,String businessObjectId,String datas,String formId,String processNodeId) throws Exception{
		ProcessNode processNode = new ProcessNode();
		processNode.setNodeId(processNodeId);
		processNode.setIsFirstNode("1");
		if (processNodeService.findCount(processNode)>0){
			applicationService.saveBusinessObjectForSub(boTableName,datas,businessObjectId,formId);
		}else{
			applicationService.saveBusinessObjectForSub(boTableName,datas,businessObjectId,formId,processNodeId);
		}	    
	    
	}
	/**
	 * 删除业务子表数据
	 * @param boTableName
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping(value = "/processApp_delete_bindReport_data", method=RequestMethod.POST)
	@ResponseBody
	public Object deleteBindReportData(String boTableName,String id) throws Exception{
	    return applicationService.deleteBindReportData(boTableName,id);
	}

    /**
     * 审批任务
     * @param task
     * @return
     */
    @RequestMapping(value = "/processTask_complete",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object processTaskComplete (OaTaskInstance task,String isQuick) throws BusinessException{
    	Map<String,Object> map = new HashMap<String,Object>() ;
    	//只有批准操作
    	if(isQuick!=null&&isQuick.equals("1")){
    		//判断审批的表单是否有必填项目
    		ProcessNodeFormPrivilege processNodeFormPrivilege=new ProcessNodeFormPrivilege();
    		processNodeFormPrivilege.setProcessDefinitionId(task.getProcessDefinitionId());
    		processNodeFormPrivilege.setNodeId(task.getNodeId());
    		processNodeFormPrivilege.setNull("0");
    		Integer privileges=processNodeFormPrivilegeService.countByCondition(processNodeFormPrivilege);
    		if(privileges!=null&&privileges>0){
    			//存在必填项，要打开详情页面查看
    			map.put("Success",false)  ;
    			map.put("message","notNull");  
    			return map;
    		}
    	}
        try{
            map = processTaskService.processTaskComplete(task);
        } catch (Exception e) {
            logger.error(e);
            throw new BusinessException("审批任务异常!",e);
        }
        return map;

    }

    /**
     *  拒绝
     * @param task
     * @return
     */
    @RequestMapping(value = "/processTask_RejectToPrevious",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object processTaskRejectToPrevious(OaTaskInstance task) {
        Map map = null ;
        try{
            map=processTaskService.processTaskRejectToPrevious(task)    ;
        }catch (Exception e){
            logger.error(e);
            throw new BusinessException("驳回操作异常!",e);
        }
        return map;

    }
    /**
     * 驳回
     * @param task
     * @return
     */
    @RequestMapping(value = "/processTask_RejectToFirst",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object processTaskRejectToFirst(OaTaskInstance task) {
        Map map = null ;
        try{
         map=processTaskService.processTaskRejectToFirst(task)    ;
        }catch (Exception e) {
            logger.error(e);
            throw  new BusinessException("拒绝操作异常!",e);
        }
        return map;

    }
    
    /**
     * 统一待办：审批任务
     * @param task
     * @return
     */
    @RequestMapping(value = "/processTask_complete_unite",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object processTaskCompleteForUnite (OaTaskInstance task,String resultType) throws BusinessException{
    	//处理消息Json串
    	Map jsonMap = new HashMap();
    	jsonMap.put("processkey", task.getProcessDefinitionKey());
        //如果processInstanceId中包括"process2",认为是remedy发送的审批任务
        if(task.getProcessInstanceId().startsWith(task.getProcessDefinitionKey())){
            String processInstanceId = task.getProcessInstanceId();
            if(processInstanceId != null && processInstanceId.trim().length() > 0){
                processInstanceId = processInstanceId.substring((task.getProcessDefinitionKey() + "-").length());
            }
            jsonMap.put("reqId", processInstanceId );
        } else {
            jsonMap.put("reqId", task.getBuinessInstanceId());
        }
    	jsonMap.put("taskId", task.getId());
    	jsonMap.put("submitUserErp", ComUtils.getLoginNamePin());
    	jsonMap.put("submitUserName", ComUtils.getDisplayName());
    	jsonMap.put("submitTime", DateUtils.datetimeFormat(new Date()));
    	jsonMap.put("submitResult", resultType);
    	jsonMap.put("submitComments", task.getComments());
    	String textMessage = DataConvertion.object2Json(jsonMap);

    	//webservice入参
    	Map parameterMp = new HashMap();
    	parameterMp.put("textMessage", textMessage);
    	      
    	//Result
    	Map mp = new HashMap();
    	
        try{
        	ProcessDefinition pd = processDefService.get(task.getProcessDefinitionId());
        	if(pd == null || pd.getSystemId() == null || pd.getSystemId().trim().length() == 0){
        		throw new BusinessException("审批任务异常!","统一待办系统ID错误。");
        	}
        	//PS入参特殊处理
        	if ("PS".equals(pd.getSystemId())) {
    	    	Map psMp = new HashMap();
    	    	psMp.put("approvetext",textMessage);
    	    	parameterMp.put("textMessage", psMp);
        	}
            
        	//若在流程定义中配置统一待办数据源，则直接调用接口，不走MQ
        	if (StringUtils.isNotEmpty(pd.getDataSourceId()) || StringUtils.isNotEmpty(pd.getRejectDataSourceId())){
        		
        		//批准接口
        		if ("1".equals(resultType)) {
	        		if (StringUtils.isNotEmpty(pd.getDataSourceId())) {
	        			DiDatasource datasource = diDatasourceService.get(pd.getDataSourceId());
	                    //如果是接口
	                    if(2==datasource.getDataAdapterType()){
	                    	//判断入参类型是XML或JSON
	                    	DiCcInterface dci = diCcInterfaceService.get(datasource.getDataAdapter());   
	                    	List<Map<String,Object>>  list = null;
	                    	if("1".equals(dci.getInParameterType())){  //入参类型是XML
	                    		list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, jsonMap, "1");
	                    	}else if("2".equals(dci.getInParameterType())){  //入参类型是JSON
	                    		list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, parameterMp, "1");
	                    	}
	                    }
	                    mp.put("Success",true);
	                    mp.put("message","审批成功");
	        		}else{
	        			throw new BusinessException("审批任务异常!","统一待办审批接口配置有误。");
	        		}
        		}

        		//拒绝接口
        		if ("2".equals(resultType)) {
	        		if (StringUtils.isNotEmpty(pd.getRejectDataSourceId())) {
	        			DiDatasource datasource = diDatasourceService.get(pd.getRejectDataSourceId());
	                    //如果是接口
	                    if(2==datasource.getDataAdapterType()){
	                    	//判断入参类型是XML或JSON
	                    	DiCcInterface dci = diCcInterfaceService.get(datasource.getDataAdapter());   
	                    	List<Map<String,Object>>  list = null;
	                    	if("1".equals(dci.getInParameterType())){  //入参类型是XML
	                    		list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, jsonMap, "1");
	                    	}else if("2".equals(dci.getInParameterType())){  //入参类型是JSON
	                    		list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, parameterMp, "1");
	                    	}                        
	                    }
	                    mp.put("Success",true);
	                    mp.put("message","审批成功");
	        		}else{	
	        			throw new BusinessException("审批任务异常!","统一待办审批接口配置有误。");
	        		}
        		}
        	//若未在流程定义中配置统一待办数据源，则走MQ
        	}else{
        		if(pd.getSystemId().trim().toUpperCase().equals("UIM")){
        			mqProduceUimService.mqProducer(textMessage);
            		mp.put("Success",true);
                    mp.put("message","审批成功");
        		} else if(pd.getSystemId().trim().toUpperCase().equals("EBS")){
        			mqProduceEbsService.mqProducer(textMessage);
            		mp.put("Success",true);
                    mp.put("message","审批成功");
        		} else {
        			throw new BusinessException("审批任务异常!","未实现的系统ID");
        		}
        	}       
        	
        	/* 由对方系统调用《2.审批结果接收接口》执行此操作
        	//更新待办任务为“完成”
        	AgentTodoList entity = new AgentTodoList();
        	entity.setProcessInstanceId(task.getProcessInstanceId());
        	entity.setTaskId(task.getId());
        	
        	List<AgentTodoList> atlList = agentTodoListService.find(entity);
        	for (AgentTodoList atl : atlList) {
        		atl.setTaskStatus("1");
        		agentTodoListService.update(atl);
        	}
        	//插入审批历史记录
        	ProcessTaskHistory pth = new ProcessTaskHistory();
        	pth.setProcessInstanceId(task.getProcessInstanceId());
        	pth.setTaskId(task.getId());
        	pth.setTaskName(task.getName());
        	pth.setPafProcessDefinitionId(task.getProcessDefinitionKey());
        	pth.setPafProcessDefinitionId(task.getProcessDefinitionId());        	
        	pth.setBeginTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(task.getStartTime()));
        	pth.setEndTime(new Date());
        	pth.setComment(task.getComments());
        	pth.setTaskType(SystemConstant.TASK_KUBU_OWNER);
        	pth.setUserId(ComUtils.getLoginNamePin());
        	pth.setUserName(ComUtils.getDisplayName());
        	processTaskHistoryService.insert(pth);
        	*/
        	
            mp.put("Success", true);
        } catch (Exception e) {
        	mp.put("Success", false);
            logger.error(e);
            throw new BusinessException("审批任务异常!",e);
        }
        
        return mp;

    }
    
    /**
     * 增加审批意见
     * @return
     */
    @RequestMapping(value = "/processTask_approve_index", method=RequestMethod.GET)
    public ModelAndView processTaskApproveIndex() throws Exception{
        ModelAndView mv = new ModelAndView();
        mv.setViewName("app/app_processTask_approve");
        return mv;
    }
    
    
    /**
     * 加签
     * @param
     * @return
     */
    @RequestMapping(value = "/processTask_forward",method = RequestMethod.POST)
    @ResponseBody
    public String processTask_forward(
    		@RequestParam(value="forwardUserIds[]", required=false) String[] forwardUserIds,
    		@RequestParam(value="processDefinitionId", required=false) String processDefinitionId,
    		@RequestParam(value="processDefinitionKey", required=false) String processDefinitionKey,
    		@RequestParam(value="processInstanceId", required=false) String processInstanceId,
    		@RequestParam(value="taskId", required=false) String taskId,
    		@RequestParam(value="taskName", required=false) String taskName,
    		@RequestParam(value="nodeId", required=false) String nodeId,
    		@RequestParam(value="taskType", required=false) String taskType,
    		@RequestParam(value="startTime", required=false) String startTime,
    		@RequestParam(value="comments", required=false) String comments,
    		@RequestParam(value="processInstanceName", required=false) String processInstanceName,
            @RequestParam(value="businessInstanceId", required=false) String businessInstanceId


    		) {
        if(forwardUserIds !=null &&  forwardUserIds.length > 0){
        	List<String> userList = new ArrayList<String>();
            comments="加签人：";
            StringBuilder commentsSb = new StringBuilder(comments);
        	for(int i=0 ; i < forwardUserIds.length ; i++){
				SysUser forwardUser=sysUserService.get(forwardUserIds[i]);
				if(forwardUser !=null && forwardUser.getUserName()!=null && !forwardUser.getUserName().equals("")){
//	        		comments += forwardUser.getRealName()+"("+forwardUser.getUserName()+") ";//加签人的姓名和Erp在审批意见里面存储显示
	        		commentsSb.append(forwardUser.getRealName()+"("+forwardUser.getUserName()+") ");
                    userList.add(forwardUser.getUserName());
				}
        	}
        	commentsSb.append("，请等待加签人审批！");
//            comments+="，请等待加签人审批！";
        	comments = commentsSb.toString();
        	OaTaskInstance instance = new OaTaskInstance();
        	instance.setProcessDefinitionId(processDefinitionId);
        	instance.setProcessDefinitionKey(processDefinitionKey);
        	instance.setProcessInstanceId(processInstanceId);
        	instance.setId(taskId);
        	instance.setName(taskName);
        	instance.setNodeId(nodeId);
        	instance.setTaskType(taskType);
        	instance.setStartTime(startTime);
        	instance.setComments(comments);
        	instance.setProcessInstanceName(processInstanceName);
            instance.setBuinessInstanceId(businessInstanceId);
        	
			if (processTaskService.addSign(instance, userList)) {				
				return "加签成功，"+comments;
			}
        	
        }
        return "加签失败！";
    }
    /**
     * 已阅
     * @param task
     * @return
     */
    @RequestMapping(value = "/processTask_isRead",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object processTaskIsRead(OaTaskInstance task) {
        Map map=processTaskService.processTaskIsRead(task)    ;
        return map;
    }

    /**
     * 加签之前查询是否已被加签
     * @param processInstanceId
     * @return
     */
    @RequestMapping(value = "/processTask_forwardCount",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Map<String,Object> forwardCount(
            @RequestParam(value="processInstanceId", required=false) String processInstanceId,
            @RequestParam(value="taskId", required=false) String taskId,
            @RequestParam(value="forwardUserIds[]", required=false) String[] forwardUserIds,
            @RequestParam(value="nodeId", required=false) String nodeId ) {
        ProcessAddsigner  addsigner = new  ProcessAddsigner();
        Map<String,Object> map = new HashMap<String,Object> ();
        addsigner.setProcessInstanceId(processInstanceId);
        addsigner.setTaskId(taskId);
        addsigner.setNodeId(nodeId);
        addsigner.setStatus("1");
        List<ProcessAddsigner> list = null;
        boolean Success=true;
        String UserId="";
		String loginUserId=ComUtils.getCurrentLoginUser().getId();
        if(forwardUserIds !=null &&  forwardUserIds.length > 0){
            for(int i=0 ; i < forwardUserIds.length ; i++){
				if(loginUserId.equals(forwardUserIds[i])){
					Success=false;
					map.put("forwardToSelf",1);
					break;
				}
                addsigner.setAddsignUserId(forwardUserIds[i]);
                list = processAddsignerService.find(addsigner);
                if(list.size()>0) {
                    Success=false;
					SysUser sysUser=sysUserService.get(addsigner.getAddsignUserId());
                    UserId=sysUser.getUserName();
					break;
                }
            }
        }
        map.put("Success",Success);
        map.put("UserId",UserId);
    	return map;
    }
    /**
     * 创建流程实例并推进第一个任务
     * @param processDefinitionKey
     * @param nodeId
     * @param businessObjectId
     * @param boTableName
     * @param processInstanceName
     * @param taskPriority
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/processApp_submitInstance", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public com.jd.oa.common.paf.model.ProcessInstance processAppSubmitInstance(String processDefinitionKey,String nodeId,String formId,String businessObjectId,String boTableName,String processInstanceName,String taskPriority,String processInstanceId,String followCode) throws Exception{
    	/*
         * 数据准备
         */
    	String submitUser = ComUtils.getLoginNamePin();
    	ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
        String currentUser = sysUserService.getUserIdByUserName(submitUser, SystemConstant.ORGNAIZATION_TYPE_HR);
        List<String> columns=new ArrayList<String>();
        Map<String,Object> businessObject = formBusinessTableService.getBusinessData(boTableName,columns,businessObjectId).get(0);
        businessObject.put("processInstanceName", processInstanceName);
        businessObject.put("submitUser", submitUser);

        /*
         * 开始事件
         */
        List<ProcessNodeListener> beforeEvents = processNodeListenerService.getBeforeEvents(processDefinition.getId(), nodeId);
        processTaskService.processEvents(beforeEvents, businessObject);
    	/*
    	 * 提交流程实例
    	 */
    	com.jd.oa.common.paf.model.ProcessInstance result = processTaskService.processAppSubmitInstance(processDefinitionKey,nodeId,formId,businessObjectId,boTableName,processInstanceName,taskPriority,processInstanceId,submitUser,followCode);
        businessObject.put("processDefinitionId", processDefinition.getId());
        businessObject.put("processInstanceId", result.getId());
        businessObject.put("starterId", currentUser);
        businessObject.put("status", SystemConstant.PROCESS_STATUS_1);
        /*
         * 结束事件
         */
        List<ProcessNodeListener> endEvents = processNodeListenerService.getEndEvents(processDefinition.getId(), nodeId);
        processTaskService.processEvents(endEvents, businessObject);
	    /*
	     * 1.为定时器推进流程作准备   2.发邮件   3.限制职级自动提交
	     */
        processTaskService.processAppSubmitInstanceNextStep(submitUser, processInstanceName, businessObjectId, result.getId(), processDefinition);
        
		return result;
	}

    
    @RequestMapping(value = "/processTask_taskNum",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object getTaskNum(String taskType,String processdefiniationId) {
        int totalNum=0;
        if(taskType.equals(SystemConstant.TASK_KUBU_OWNER)){
            totalNum=processTaskService.findTodoProcessTaskTotalNum();
        }else if(taskType.equals(SystemConstant.TASK_KUBU_ADDSIGNER)){
            totalNum=processTaskService.findForwardProcessTaskTotalNum();
        }else if(taskType.equals(SystemConstant.TASK_KUBU_PROXY)){
            totalNum=processTaskService.findProxyProcessTaskTotalNum();
        }else if(taskType.equals(SystemConstant.TASK_KUBU_SUPERVISE)){
            totalNum=processTaskService.findSuperviseProcessTaskNum();
        }
        return totalNum;
    }

	/**
	 * 复制一份表单为草稿状态
	 * @param processDefinitionKey 流程定义的key
	 * @param businessObjectId  业务表实例ID
	 * @return
	 */
	@RequestMapping(value = "/processApply_copy",method = RequestMethod.POST,produces = "application/json")
	@ResponseBody
	public Object processApplyCopy(String processDefinitionKey,String businessObjectId) {
		if(processDefinitionKey == null || processDefinitionKey.equals("")) {
			throw new BusinessException("没有传入流程Key:processDefinitionKey");
		}
		ProcessDefinition processDefinition=new ProcessDefinition();
		processDefinition.setPafProcessDefinitionId(processDefinitionKey);
		List<ProcessDefinition> processDefinitions=processDefService.find(processDefinition);
		if(processDefinitions ==null || processDefinitions.size() <1){
			throw new BusinessException("没有对应的流程");
		}
		//取出表单
		Form mainForm=formService.get(processDefinitions.get(0).getFormId());
		if(mainForm == null ){
			throw new BusinessException("没有找到对应的表单");
		}
		
		//取出首结点
		ProcessNode firstProcessNode=processNodeService.getFirstNode(processDefinitions.get(0).getId());
		//复制主表记录
		String tableName=formService.getFormCodeByProDId(processDefinitions.get(0).getId());
		Map<String,Object> mainRecord=formBusinessTableService.getBusinessData(tableName,businessObjectId);
		resetFieldValue(mainRecord,processDefinitions.get(0),firstProcessNode,mainForm);
		List<Map<String,Object>> list=new ArrayList<Map<String, Object>>();
		for(Map.Entry<String,Object> entry:mainRecord.entrySet()){
			Map<String,Object> map=new HashMap<String, Object>();
			map.put("fieldName",entry.getKey());
			map.put("value",entry.getValue());
			list.add(map);
		}
		String newBusinessId = applicationService.saveBusinessObject(tableName,getJsonData(list),null,processDefinitions.get(0).getFormId());
		if(newBusinessId.equals("0")){
			return "复制失败";
		}
		
//		查询子表是否隐藏，如果全部隐藏，则不复制数据
		ProcessNodeFormPrivilege processNodeFormPrivilege = new ProcessNodeFormPrivilege();
		processNodeFormPrivilege.setProcessDefinitionId(processDefinitions.get(0).getId());
		processNodeFormPrivilege.setNodeId(firstProcessNode.getNodeId());
		processNodeFormPrivilege.setHidden("0");//查询未隐藏字段
		List<Form> subFormList =  mainForm.getSubTables();
		List<Form> newSubFormList =  new ArrayList<Form>();
		if(subFormList!=null && subFormList.size()>0){
			for (Form subForm :subFormList) {
				processNodeFormPrivilege.setFormId(subForm.getId());
				Integer privilegeNum=processNodeFormPrivilegeService.countByCondition(processNodeFormPrivilege);
				if(null!=privilegeNum&&privilegeNum>0){//子表有未隐藏字段
					newSubFormList.add(subForm);
				}
			}
			mainForm.setSubTables(newSubFormList);
		}
		
		
		//复制子表记录
		List<Form> subForms=mainForm.getSubTables();
		for(Form form:subForms ){
			List<Map<String,Object>> subReportDatas =  applicationService.getBusinessDatas(form.getFormCode(),businessObjectId);
			for(Map<String,Object> map:subReportDatas){
				map.put("ID","");
				map.put("PARENT_ID", newBusinessId);
				resetFieldValue(map,processDefinitions.get(0),firstProcessNode,form);
			}
			applicationService.saveBusinessObjectForSub(form.getFormCode(),getJsonData(subReportDatas),newBusinessId,mainForm.getId());
		}
		//流程实例表中插入数据
		ProcessInstance processInstance=new ProcessInstance();
		processInstance.setBusinessInstanceId(businessObjectId);
		List<ProcessInstance> processInstanceList = processInstanceService.find(processInstance);
		if(processInstanceList == null || processInstanceList.size() <1){
			throw new BusinessException("无法创建草稿实例！");
		}
		processInstanceList.get(0).setId(null);
		processInstanceList.get(0).setFollowCode(formSequenceService.getProcessSequenceCode(processDefinitions.get(0).getFollowCodePrefix()));
		processInstanceList.get(0).setBusinessInstanceId(newBusinessId);
		processInstanceList.get(0).setProcessInstanceId(null);
		processInstanceList.get(0).setStatus(SystemConstant.PROCESS_STATUS_0);
		processInstanceList.get(0).setLastTaskTime(null);
		processInstanceList.get(0).setEndTime(null);
		processInstanceList.get(0).setBeginTime(new Date());
		processInstanceList.get(0).setCreateTime(new Date());
		try {
			processInstanceService.insert(processInstanceList.get(0));
		} catch (Exception e) {
			e.printStackTrace();
			throw  new BusinessException(e.getMessage());
		}
		return  JsonUtils.toJsonByGoogle("success");
	}

	/**
	 * 将数据转化成JSON
	 * @param o 需要转换的对象
	 * @return  转化后的数据
	 */
	private String getJsonData(Object o){
		String datas;
		try {
			datas = JsonUtils.toJson(o);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
		return datas;
	}

	/**
	 * 按照表单字段的规则，重新给取回的map数据赋值
	 * @param dataMap 需要处理额map
	 * @param processDefinition  流程定义
	 * @param processNode  首结点
	 * @param form  表单
	 */
	private void resetFieldValue(Map<String,Object> dataMap,ProcessDefinition processDefinition,ProcessNode processNode,Form form){
		//取出所有表单项
		List<FormItem> formItemList=formItemService.getFormItemByFormId(form.getId());
		Map<String,FormItem> formItemMap=new HashMap<String, FormItem>();
		for(FormItem item:formItemList){
			formItemMap.put(item.getFieldName(),item);
		}
		//取出所有表单项绑定的规则
		ProcessNodeFormPrivilege processNodeFormPrivilege=new ProcessNodeFormPrivilege();
		processNodeFormPrivilege.setProcessDefinitionId(processDefinition.getId());
		processNodeFormPrivilege.setNodeId(processNode.getNodeId());
		processNodeFormPrivilege.setFormId(form.getId());
		List<ProcessNodeFormPrivilege> processNodeFormPrivilegeList=processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
		Map<String,ProcessNodeFormPrivilege> processNodeFormPrivilegeMap=new HashMap<String, ProcessNodeFormPrivilege>();
		for(ProcessNodeFormPrivilege privilege:processNodeFormPrivilegeList){
			processNodeFormPrivilegeMap.put(privilege.getItemId(),privilege);
		}
		//开始重置数据
		dataMap.put("ID","");
		dataMap.put("YN","0");
		dataMap.put("CREATE_TIME",new Date());
		dataMap.put("MODIFIER","");
		dataMap.put("MODIFY_TIME","");

		for(Map.Entry<String,Object> entry:dataMap.entrySet()){
			//内置变量处理成空值
			FormItem item=formItemMap.get(entry.getKey());
			if(item == null){
				continue;
			}
			if(item.getDefaultValue()!=null && item.getDefaultValue().contains("@")){
				dataMap.put(entry.getKey(),"");
			}
			//隐藏的字段处理成空值
//			ProcessNodeFormPrivilege privilege=processNodeFormPrivilegeMap.get(item.getId());
//			if(privilege !=null && privilege.getHidden()!=null && privilege.getHidden().equals("1")){
//				dataMap.put(entry.getKey(),"");
//			}
			//只读的字段处理成空值
//			if(privilege !=null && privilege.getEdit()!=null && privilege.getEdit().equals("0")){
//				dataMap.put(entry.getKey(),"");
//			}
		}

	}
	
	/**
	 * @author zhengbing 
	 * @desc 获取申请节点主表默认值
	 * @date 2014年7月30日 下午6:26:01
	 * @return Object
	 */
    @RequestMapping(value = "/getMainTableDefaultValue",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Object getMainTableDefaultValue(HttpServletRequest request,String params) {
    	if(null==params||params.trim().length()==0){
    		return null;
    	}
    	String[] paramStr = params.split("\\|");
    	List<ProcessApplyDefaultValueDto> padvs = new ArrayList<ProcessApplyDefaultValueDto>();
    	RuntimeFormManager rfm = new RuntimeFormManager(request.getSession().getServletContext().getRealPath("/"));
    	if(null!=paramStr&&paramStr.length>0){
    		for (int i = 0; i < paramStr.length; i++) {
				String inputstr =  paramStr[i];
				if(null!=inputstr&&inputstr.length()>0){
					String[] input = inputstr.split(":");
					if(null!=input&&input.length==3){
						ProcessApplyDefaultValueDto o = new ProcessApplyDefaultValueDto();
						o.setFieldName(input[0]);
						if("followCode".equals(input[0]) && null!=input[1] && input[1].length()>0 && !input[1].startsWith("@")){//生成单号
							ProcessDefinition processDefinition = processDefService.getByDefinitionId(input[1]);
							FormSequenceService formSequenceService = SpringContextUtils.getBean(FormSequenceService.class);
							o.setDefaultValue(formSequenceService.getProcessSequenceCode(processDefinition.getFollowCodePrefix()));
						}else {
							o.setDefaultValue(rfm.convertMacrosValue(input[1]));
						}
						o.setInputType(input[2]);
						padvs.add(o);
					}
				}
			}
    	}
        return padvs;
    }
    
}

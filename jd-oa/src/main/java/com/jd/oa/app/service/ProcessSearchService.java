package com.jd.oa.app.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.app.dto.ProcessAndTaskInfoDto;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.service.BaseService;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.process.model.ProcessDefinition;

/** 
 * @Description: 前台-申请查询Service
 * @author guoqingfu
 * @date 2013-10-11 上午11:14:32 
 * @version V1.0 
 */
public interface ProcessSearchService extends BaseService<ProcessSearch, String>{
	/**
     * @Description: 通过类别ID查询流程定义信息（前台用）
      * @param processTypeId
      * @return List<ProcessDefinition>
      * @author guoqingfu
      * @date 2013-10-11下午12:04:43 
      * @version V1.0
     */
    public List<ProcessSearch> findPrcocessListByTypeId(String processTypeId);
    
    /**
     * 查询一级流程 -> 所有二级流程  ->所有流程
     * @param processType1Id
     * @return
     */
    public List<ProcessSearch> findByType1(String processType1Id);
    
    /**
     * @Description: 查询我的申请流程详细信息
     * @param erpId
     * @param status 查询多个状态用,分割
     * @return List<ProcessAndTaskInfoDto>
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessAndTaskInfoDto> getMyApplyProTaskInfo(String erpId);
    
    /**
     * 
     * @desc 通过流程实例获得流程定义相关信息
     * @author WXJ
     * @date 2013-10-14 下午02:51:37
     *
     * @param processInstance
     * @return
     */
    public ProcessDefinition findProcessDefByInstance(String processInstance);
    
    /**
     * @Description: 查询流程代办列表，包括被代理的和转发过来的
      * @param erpId
      * @param type
      * @param pafKey
      * @param byProxyUserId
      * @param assingerId
      * @param conditions
      * @param mapProxyTaskList
      * @type 0：全部 1：自己的 2：代理别人的 3：别人转发的
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
    public abstract List<ProcessSearch> findPrcocessTaskListByUserId(String erpId,int type,String pafKey,String byProxyUserId,
    		Conditions conditions,Map<String,List<ProcessSearch>>...mapProxyTaskList);
    
    /**
     * @Description: 查询某人审批过的流程
      * @param paramHistory :bjlxdong
      * @param type
      * @param byProxyUserId 被代理人
      * @param assingerId
      * @param conditions
      * @type 0：全部 1：自己的 2：代理别人的 3：别人转发的  4:自己的和别人转发的
      * @param completed true:流程实例已完成;false:流程实例未完成
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
    public abstract List<ProcessSearch> findPrcocessTaskOverByUserId(int type,String byProxyUserId,
    		ProcessTaskHistory paramHistory,Conditions conditions);
    
    /**
     * @Description: 查询某人的要督办的或督办过的流程列表
      * @param userId
      * @param status 1:要督办的  2：督办过的;3:全部
      * @param 流程pafKey
      * @return List<ProcessSearch>
      * @author xulin
      * @date 2013-10-14下午12:04:43 
      * @version V1.0
     */
    public abstract List<ProcessSearch> findPrcocessSuperviseTaskByUserId(String userErpId,String status,String pafKey,
    		Conditions conditions);
    
    /**
     * 
     * @desc 查询出所有正常状态的已发布流程
     * @author WXJ
     * @date 2013-10-14 下午02:51:37
     *
     * @return
     */
    public List<String> findProcessDefAll();
    /**
     * @Description: 取消流程实例(状态是草稿)
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-23下午07:42:24 
      * @version V1.0
     */
    public Map<String,Object> cancelApplyForDraft(String processInstanceKey);
    /**
     * @Description: 取消流程实例(状态是被驳回)
      * @param processInstanceId
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-24上午10:38:14 
      * @version V1.0
     */
    public Map<String,Object> cancelApplyForRejected(String processInstanceId);
    /**
     * @Description: 查询流程实例提交后是否被领导审批
      * @param processInstanceId
      * @return boolean : true表示被审批过；false表示没有被领导审批过
      * @author guoqingfu
      * @date 2013-10-24上午11:21:59 
      * @version V1.0
     */
    public boolean findProcessTaskHistory(String processInstanceId); 
    /**
     * @Description: 取消流程实例(提交后但领导还没审批)
      * @param processInstanceId
      * @return Map<String,Object>
      * @author guoqingfu
      * @date 2013-10-24上午11:42:23 
      * @version V1.0
     */
    public Map<String,Object> cancelApplyForUnapproved(String processInstanceId);
    
    /**
     * 返回督办流程个数
     * @param userId
     * @param processDefineed 流程定义ID
     * @param conditions
     * @return
     */
    public int getProSuperviseTaskCountByUserId(String userId,String processDefineed,Conditions conditions);
    
    /**
     * 获取指定人的待办任务总数，根据流程定义ID可已分类
     * @param loginUser
     * @param processdefinationId
     */
    public Map<String,Object> findTodoProcessTaskTotalNum(String loginUser);
    public Map<String,Object> findForwardProcessTaskTotalNum(String loginUser,String processdefinationId);
    public Map<String,Object> findProxyProcessTaskTotalNum(String loginUser);
    public Map<String,Object> findTodoProcessTaskTotalNum(String loginUser,String processdefinationKey);
    /**
     * 根据条件查询业务数据表
     * @param processDefinitionKey
     * @param formItemList
     * @return
     */
    public String findBusinessData(String id,String filters,String status,String startID,String processInstanceName,String beginTimeBegin,String beginTimeEnd);
}

package com.jd.oa.app.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.jd.oa.app.dao.ProcessSearchDao;
import com.jd.oa.app.model.ProcessSearch;
import com.jd.oa.common.dao.MyBatisDaoImpl;
import com.jd.oa.process.model.ProcessInstance;

/** 
 * @Description: 前台-申请查询Dao实现
 * @author guoqingfu
 * @date 2013-10-11 上午11:14:32 
 * @version V1.0 
 */

@Repository("processSearchDao")
public class ProcessSearchDaoImpl extends MyBatisDaoImpl<ProcessSearch, String>
		implements ProcessSearchDao {
	
	/**
     * @Description: 通过类别ID查询流程定义信息（前台用）
      * @param processTypeId
      * @return List<ProcessDefinition>
      * @author guoqingfu
      * @date 2013-10-11下午12:04:43 
      * @version V1.0
     */
    public List<ProcessSearch> findPrcocessListByTypeId(String processTypeId){
    	List<ProcessSearch> result = null;
        try {
            result = this.getSqlSession().selectList("com.jd.oa.app.model.ProcessSearch.findPrcocessListByTypeId",processTypeId);
        } catch (DataAccessException e) {
            throw e;
        }
        return result;
    }
    
	@Override
	public List<ProcessSearch> findByType1(String processType1Id) {
		return this.getSqlSession().selectList(ProcessSearch.class.getName() + ".findByType1", processType1Id);
	}
    
    /**
     * @Description: 根据申请人和状态查询流程信息（我的申请）
     * @param starterId
     * @param status 查询多个状态用,分割
     * @return List<ProcessInstance>
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessInstance> selectProInsByStaIdAndStatus(String starterId,List<String> status){
    	List<ProcessInstance> result = null;
    	try {
    		Map<String,Object> parameters = new HashMap<String,Object>();
    		parameters.put("starterId", starterId);
    		parameters.put("status", status);
    		result = this.getSqlSession().selectList(
    			"com.jd.oa.process.model.ProcessInstance.selectProInsByStaIdAndStatus", parameters);
    	} catch (DataAccessException e) {
    		throw e;
    	}
    	return result;
    }
    
    /**
     * @Description: 根据流程实例ID获取流程的详细信息
     * @param starterId
     * @param status 查询多个状态用,分割
     * @return ProcessSearch
	 * @author 许林
	 * @date 2013-10-14上午10:04:43 
	 * @version V1.0
     */
    public List<ProcessSearch> getProcessInfoByInstanceId(String instanceId) {
    	List<ProcessSearch> result = null;
    	try {
    		result = this.getSqlSession().selectList(
    			"com.jd.oa.app.model.ProcessSearch.getProcessInfoByInstanceId", instanceId);
    	} catch (DataAccessException e) {
    		throw e;
    	}
    	return result;
    }
    public List<ProcessSearch> getProcessInfoByInstanceId(List<String> processInstanceIds) {
    	List<ProcessSearch> result = null;
    	try {
    		result = this.getSqlSession().selectList(
    			"com.jd.oa.app.model.ProcessSearch.getProcessInfoByInstanceId", processInstanceIds);
    	} catch (DataAccessException e) {
    		throw e;
    	}
    	return result;
    }
    
	/**
	 * 根据流程定义ID和最后更新时间查询流程实例
	 * @param processDefinitionId
	 * @param createStartDate
	 * @param createEndDate
	 * @return
	 */
	public List<ProcessSearch> findProcessInstanceByDefId(List<String> processDefinitionId,
			String createStartDate,String createEndDate) {
		Map<String,Object> mp = new HashMap<String, Object>();
		mp.put("processDefinitionId", processDefinitionId);
		mp.put("createStartDate", createStartDate);
		mp.put("createEndDate", createEndDate);
		
		return this.getSqlSession().selectList("com.jd.oa.app.model.ProcessSearch.findProcessInstanceByDefId", mp);
	}
	
	/**
     * @Description: 取消流程实例(状态是草稿)
      * @param processInstanceId
      * @return 
      * @author guoqingfu
      * @date 2013-10-23下午07:42:24 
      * @version V1.0
     */
    public boolean cancelApplyForDraft(String processInstanceKey){
    	boolean flag = false;
    	try{
    		flag = this.getSqlSession().update("com.jd.oa.app.model.ProcessSearch.cancelApplyForDraft",processInstanceKey)>0?true:false;
    	}catch(DataAccessException e){
    		throw e;
    	}
    	return flag;
    }

}

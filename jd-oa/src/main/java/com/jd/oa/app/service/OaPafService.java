package com.jd.oa.app.service;

import com.jd.oa.app.model.Conditions;
import com.jd.oa.app.model.OaProcessInstance;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.system.model.SysUser;

import java.util.List;
import java.util.Map;

/**
 * OA工作流平台PAF服务接口
 * User: WXJ
 * Date: 13-10-11
 * Time: 下午5:18
 */
public interface OaPafService {

    /**
     * 
     * @desc 创建流程实例
     * @author WXJ
     * @date 2013-10-11 下午07:58:48
     *
     * @param processDefKey
     * @param businessKey
     * @return ProcessInstance
     */
    public ProcessInstance createProcessInstance(String processDefKey, String businessKey);
    public ProcessInstance createProcessInstance(String processDefKey, String businessKey,String loginUser);

    /**
     * 
     * @desc 推进第一个任务
     * @author WXJ
     * @date 2013-12-18 下午07:58:44
     *
     * @param processInstanceId
     * @param variables
     * @return ProcessInstance
     */
    public ProcessInstance submitHumanProcessInstance(String processInstanceId, Map<String, Object> variables);
    public ProcessInstance submitHumanProcessInstance(String processInstanceId, Map<String, Object> variables,String loginUser);

    /**
     * 
     * @desc 创建流程实例并推进第一个任务
     * @author WXJ
     * @date 2013-10-11 下午07:58:44
     *
     * @param processDefKey
     * @param businessKey
     * @param variables
     * @return ProcessInstance
     */
    public ProcessInstance submitHumanProcessInstance(String processDefKey, String businessKey, Map<String, Object> variables);

    /**
     * 
     * @desc 完成一个任务
     * @author WXJ
     * @date 2013-10-11 下午07:58:36
     *
     * @param taskId
     * @param variables
     * @return Boolean
     */
    public Boolean completeUserTask(String loginUser, String taskId, Map<String, Object> variables);

	/**
	 * 根据流程key获得指定人待办任务List,包括paf任务和统一待办任务
	 * @param loginUser
	 * @param processDefinitionKey
	 * @param conditions
	 * @return
	 */
	public List<OaTaskInstance> getTaskIncludePafAndBiz(String loginUser, String processDefinitionKey, Conditions conditions);

           
    /**
     * 
     * @desc 根据流程key获得指定人待办任务List
     * @author WXJ
     * @date 2013-10-11 下午07:57:16
     *
     * @param loginUser
     * @param processDefinitionKey
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getTask(String loginUser, String processDefinitionKey, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程keyList获取指定人待办任务List
     * @author WXJ
     * @date 2013-10-11 下午07:58:23
     *
     * @param loginUser
     * @param processDefKeyList
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getTask(String loginUser, List<String> processDefKeyList, Conditions conditions);
    
    /**
     * 
     * @desc 获取指定人所有待办任务List
     * @author WXJ
     * @date 2013-10-13 下午07:57:16
     *
     * @param loginUser
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getTask(String loginUser, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程key获取指定人所有已办任务List
     * @author WXJ
     * @date 2013-10-11 下午08:21:28
     *
     * @param loginUser
     * @param completed true:流程实例已完成;false:流程实例未完成;all:所有
     * @param processDefinitionKey
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, String processDefinitionKey, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程keyList获取指定人所有已办任务List
     * @author WXJ
     * @date 2013-10-11 下午08:21:28
     *
     * @param loginUser
     * @param completed true:流程实例已完成;false:流程实例未完成;all:所有
     * @param processDefKeyList
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, List<String> processDefKeyList, Conditions conditions);
    
    /**
     * 
     * @desc 获取指定人所有已办任务List
     * @author WXJ
     * @date 2013-10-14 下午08:21:28
     *
     * @param loginUser
     * @param completed true:流程实例已完成;false:流程实例未完成;all:所有
     * @param processDefKeyList
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getCompleteTask(String loginUser, String completed, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程Instance获取流程跟踪详细信息
     * @author WXJ
     * @date 2013-10-11 下午08:21:51
     *
     * @param processInstId
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public OaProcessInstance getTrackTaskByInstance(String processInstId, Conditions conditions);
    /**
     * @author wangdongxing
     * @desc  多个一个传入loginUser的参数方法，避免出错
     * @param loginUser
     * @param processInstId
     * @param conditions
     * @return
     */
    public OaProcessInstance getTrackTaskByInstance(String loginUser,String processInstId, Conditions conditions);
         
    /**
     * 
     * @desc 根据流程key获取多个流程实例的当前活动任务List
     * @author WXJ
     * @date 2013-10-11 下午08:22:07
     *
     * @param processDefinitionKey
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getActiveTask(String processDefinitionKey, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程keyList获取多个流程实例的当前活动任务List
     * @author WXJ
     * @date 2013-10-15 下午08:22:07
     *
     * @param processDefKeyList
     * @param conditions
     * @return List<OaTaskInstance>
     */
    public List<OaTaskInstance> getActiveTask(List<String> processDefKeyList, Conditions conditions);
    
    /**
     * 
     * @desc 根据流程Instance获取当前活动任务List
     * @author WXJ
     * @date 2013-10-22 下午01:35:42
     *
     * @param processInstId
     * @return
     */
    public List<OaTaskInstance> getActiveTaskByInstance(String processInstId);
    
    public List<OaTaskInstance> getActiveTaskByInstance(String loginUser, String processInstId);
    
//    /**
//     * 
//     * @desc 根据流程Instance获取审批意见List
//     * @author WXJ
//     * @date 2013-10-11 下午08:23:46
//     *
//     * @param processInstId
//     * @return List<OaTaskInstance>
//     */
//    public List<Comment> getCommentListByInstance(String processInstId);
     
    
    /**
     * 
     * @desc 获取任务办理人List
     * @author WXJ
     * @date 2013-10-11 下午08:22:33
     *
     * @param processInstId
     * @param taskId
     * @return List<String>
     */
    //public List<String> getUsers(String processInstId);
    
    /**
     * 
     * @desc 获取任务办理人String
     * @author WXJ
     * @date 2013-12-26 下午08:22:33
     *
     * @param processInstId
     * @param taskId
     * @return List<String>
     */
    public String getUsers(String taskId);

	public List<SysUser> getUsers(String loginUser,String taskId);
    
    /**
     * 
     * @desc 加签
     * @author WXJ
     * @date 2013-10-11 下午08:22:54
     *
     * @param processInstId
     * @param taskId
     * @param userAddedList
     * @return Boolean
     */
    public Boolean addSign(String processInstId, String taskId, List<String> userAddedList);
            
    /**
     * 
     * @desc 驳回至上个人工节点
     * @author WXJ
     * @date 2013-10-11 下午08:23:09
     *
     * @param processInstId
     * @param taskId
     * @param variables
     * @return Boolean
     */
    public Boolean rejectToPrevious(String loginUser, String taskId, Map<String, Object> variables);
    
    /**
     * 
     * @desc 驳回至第一个人工节点
     * @author WXJ
     * @date 2013-10-11 下午08:23:19
     *
     * @param processInstId
     * @param taskId
     * @param variables
     * @return Boolean
     */
    public Boolean rejectToFirst(String loginUser, String taskId, Map<String, Object> variables);
          
    /**
     * 
     * @desc 提交审批意见
     * @author WXJ
     * @date 2013-10-11 下午08:23:31
     *
     * @param processInstId
     * @param taskId
     * @param comment
     * @return Boolean
     */
    public Boolean addTaskComment(String processInstId, String taskId, String comment);
    
    public Boolean addTaskComment(String loginUser, String processInstId, String taskId, String comment);
    
    /**
     * 查询流程实例已结案的任务历史   --赵明
     * @param loginUser 登陆用户
     * @param processDefinitionKey 流程定义KEY
     * @param conditions 查询条件实体类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectEndProcessTaskHistory(String loginUser, String processDefinitionKey, Conditions conditions);

    /**
     * 查询流程实例未结案的任务历史   --赵明
     * @param loginUser 登陆用户
     * @param processDefinitionKey 流程定义KEY
     * @param conditions 查询条件实体类
     * @return 已办任务列表
     */
    public List<ProcessTaskHistory> selectProcessTaskHistory(String loginUser, String processDefinitionKey, Conditions conditions);

    /**
     * 获取我发起的流程实例信息    --赵明
     * @param loginUser 登陆用户
     * @return 流程实例列表
     */
    public List<com.jd.oa.process.model.ProcessInstance> getMyProcessInstance(String loginUser);
    
    /**
     * 获取我发起的流程实例信息   --许林
     * @param loginUser 登陆用户
     * @return 流程实例列表
     */
    public List<com.jd.oa.process.model.ProcessInstance> getMyProcessInstanceOther(String loginUser);
    
    /**
     * 删除流程实例信息     --赵明
     * @param loginUser 登陆用户
     * @param processInstanceId 流程实例ID
     */
    public boolean cancelProcessInstance(String loginUser, String processInstanceId);

    /**
     * 获取流程实例图片
     * @param processInstanceId 流程实例ID
     * @return
     */
    public byte[] processInstanceFlowPictureQuery(String processInstanceId);

    /**
     * 获取任务的名称   --赵明
     * @param loginUser 登陆人
     * @param processInstId 流程实例ID
     * @param taskInstanceId 任务实例ID
     * @return 任务创建时间
     */
    public String getTaskName(String loginUser, String processInstId, String taskInstanceId);
    
    /**
     * 
     * @desc 获取指定人按流程KEY分组的全部待办任务Total和List
     * @author WXJ
     * @date 2013-10-28 下午01:59:07
     * 
     * @param loginUser
     * @param conditions
     * @return Map示例：{[流程key1:{total:val,taskList:val}],[流程key2:{total:val,taskList:val}]}
     */
    public Map<String, Map<String, Object>> getTaskAndKeyTotal(String loginUser, Conditions conditions);
    
    
    public Map<String, Map<String, Object>> getTaskAndKeyTotalByProcessDef(String loginUser, Conditions conditions, String processDef) ;
    
    /**
     * 
     * @desc 获取指定人未分组的全部待办任务Total和List
     * @author WXJ
     * @date 2013-10-28 下午01:59:10
     *
     * @param loginUser
     * @param conditions
     * @return Map示例：{total:val,taskList:val}
     */
    public Map<String, Object> getTaskAndAllTotal(String loginUser, Conditions conditions);

	/**
	 * 获取指定人未分组的全部待办任务Total和List 包括统一待办数据
	 * @param loginUser
	 * @param conditions
	 * @return
	 */
	public Map<String, Object> getTaskAndAllTotalIncludePafAndBiz(String loginUser, Conditions conditions);

	/**
	 *获取指定人所有流程的全部待办任务Total和List 仅包括统一待办数据
	 * @param candidateUser
	 * @return
	 */
	public Map<String,Object> getBizTaskAndAllTotal(String candidateUser);

	/**
	 * 获取指定人指定流程的全部待办任务Total和List 仅包括统一待办数据
	 * @param candidateUser 办理人是谁
	 * @param processDefinitionKey  指定的流程key
	 * @return
	 */
	public Map<String,Object> getBizTaskAndAllTotal(String candidateUser,String processDefinitionKey);


    
    /**
     * 
     * @desc 根据多个流程实例返回当前任务状态
     * @author WXJ
     * @date 2013-11-19 下午03:58:10
     *
     * @param loginUser
     * @param processInstances
     * @return
     */
    public List<OaTaskInstance> getProcessCurrentUserTasks(String loginUser, List<String> processInstances);
    
    /**
     * 
     * @desc 根据taskId获得task详细信息
     * @author WXJ
     * @date 2013-12-24 下午05:53:49
     *
     * @param taskId
     * @return
     */
    public OaTaskInstance getTaskDetail(String taskId);
    
    /**
     * 
     * @desc 根据taskId获得task详细信息
     * @author WXJ
     * @date 2013-12-23 下午05:53:49
     *
     * @param loginUser
     * @param taskId
     * @return
     */
    public OaTaskInstance getTaskDetail(String loginUser, String taskId);
    
    /**
     * 重新分配任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @param origAssignee 当前的任务分配用户
     * @param newAssignee 新的任务分配用户，必须为小写
     * @param assigneeType 用户类型，目前只支持“user”
     * @return 重新分配结果
     */
    public Boolean reassignTask(String loginUser, String taskId, String origAssignee, String newAssignee, String assigneeType);
    
    /**
     * 通过MQ向JME推送信息
     * 
     * @param processInstId
     */
    public void sendMessage2JmeByMQ(String processInstId);
    
}

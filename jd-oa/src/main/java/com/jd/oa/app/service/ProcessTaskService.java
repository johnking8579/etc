package com.jd.oa.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.formula.functions.T;

import com.jd.oa.app.dto.ProcessTaskDto;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.model.ProcessTaskTimer;
import com.jd.oa.common.service.BaseService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessNodeListener;

public interface ProcessTaskService extends BaseService<T, String> {


    /**
     * 取得代理审批的待办任务
     * @return
     */
    public Map<String,Map<String,ProcessTaskDto>> getProxyTodoProcessTaskList(HttpServletRequest request,String pafKey,String byProxyUserId);
    /**
     * 按照时间段获取我的督办任务
     * @param startDay
     * @param endDay
     * @return
     */
    public Map<String,ProcessTaskDto> getSuperviseProcessTaskList(HttpServletRequest request,int startDay,int endDay,String pafKey);
    /**
     * 取得所有流程的待办任务  包括我的审批和转发给我的
     * @return
     */
    public Map<String,ProcessTaskDto> getTodoProcessTaskList(HttpServletRequest request,String taskType);
    /**
     * 有多个条件，减少操作的取流程待办
     * @param processDefinitionId
     * @param processDefinitionName
     * @param processDefinitionKey
     * @param tableName
     * @param bussinessColumns
     * @return
     */
    public ProcessTaskDto getTodoProcessTaskList(HttpServletRequest request,String processDefinitionId,String processDefinitionName,
                                                 String processDefinitionKey,String tableName,List<String> processColumns,List<String> bussinessColumns,String taskType);

    /**
     * 根据实例ID取得业务表数据
     * @param processInstanceId
     * @return
     */
    public Map<String,Object> getBussinessDataByProcessInstance(String processInstanceId,String submitUser,String taskOperateCode);

    /**
     *
     * @desc 转发任务
     * @author WXJ
     * @date 2014-2-7 下午04:43:00
     *
     * @param instance
     * @param userAddedList
     * @return
     */
    public Boolean addSign(OaTaskInstance instance, List<String> userAddedList);

    /**
     * 审批任务 yhb
     * @param task
     * @return
     */
    public  Map<String,Object> processTaskComplete (OaTaskInstance task) throws Exception ;
    public  Map<String,Object> processTaskComplete (String submitUser,OaTaskInstance task) throws Exception ;

    /**
     * 驳回任务至上一个节点
     * @param task
     * @return
     */
    public Map<String,Object> processTaskRejectToPrevious (OaTaskInstance task) throws Exception;
    public Map<String,Object> processTaskRejectToPrevious (String submitUser,OaTaskInstance task) throws Exception;
    /**
     * 驳回任务至第一个第一个节点
     * @param task
     * @return
     */
    public Map<String,Object>  processTaskRejectToFirst(OaTaskInstance task) throws Exception;
    public Map<String,Object>  processTaskRejectToFirst(String submitUser,OaTaskInstance task) throws Exception;

    /**
     * 获取按钮组
     * @param processInstanceId 流程实例ID
     * @param taskType 任务类型
     * @param processDefinitionId 流程定义ID
     * @param nodeId 节点ID
     * @return map
     */
    public HashMap<String, Object> getButtons(String processInstanceId, String taskType, String processDefinitionId, String nodeId, String taskId,String type);

    /**
     * 获取流程实例运行轨迹
     * @param processInstanceId 流程实例ID
     * @return 流程实例详细信息
     */
    public List<ProcessTaskHistory> getProcessTrajectory(String processInstanceId);

    /**
     *
     * @desc 获取OA系统邮件，并解析办理
     * @author WXJ
     * @date 2013-10-17 下午05:16:26
     *
     * @return
     */
    public void parseMailTask() throws Exception ;

    public void dealMailTask(String content, String fromMail) throws Exception;

    /**
     *
     * @desc 获取OA系统短信，并解析办理
     * @author WXJ
     * @date 2013-10-17 下午05:16:26
     *
     * @return
     */
    public Map parseSmsTask(String content, String fromMobile) throws Exception;
    /**
     * 根据流程定义ID查询流程表信息
     * @param nodeID
     * @param processInstanceId
     * @param businessInstanceId
     * @return
     */
    public Map<String,Object> findMapByProcess(String submitUser,String processInstanceName,String nodeID,String processDefinitionId,String businessInstanceId, String result, String comment, OaTaskInstance oti,String processInstanceId)  throws Exception ;

    /**
     *获取转发状态
     * @param oaTaskInstance
     * @return
     */
    public Map<String,Object> processTaskIsRead (OaTaskInstance oaTaskInstance);
    /**
     * 获取我当前的待办任务总数
     */
    public int findTodoProcessTaskTotalNum();
    /**
     * 获取转发给我的审批总数
     * @return
     */
    public int findForwardProcessTaskTotalNum();
    /**
     * 获取我当前的待办任务总数
     */
    public int findProxyProcessTaskTotalNum();
    /**
     * 获取我当前的督办任务总数
     */
    public int findSuperviseProcessTaskNum();


    public String getManagerUser(String submitUser);

    /**
     * 创建流程实例，并推进第一个人工任务
     * @param processDefinitionKey
     * @param nodeId
     * @param formId
     * @param businessObjectId
     * @param boTableName
     * @param processInstanceName
     * @param taskPriority
     * @param processInstanceId
     * @param followCode 流水号
     * @return
     */
    public com.jd.oa.common.paf.model.ProcessInstance processAppSubmitInstance(String processDefinitionKey,String nodeId,String formId,String businessObjectId,String boTableName,String processInstanceName,String taskPriority,String processInstanceId,String submitUser,String followCode) throws Exception;

    public void sendMailToSomeone(String submitUser, String processInstanceId,String processInstanceName,String businessInstanceId,String flag, List<String > mails,List<String> users) ;
    
	public void sendMailToAssignee(String submitUser, String processInstanceId,
			String processInstanceName, String businessInstanceId, String flag,
			boolean sendToStarter, boolean sendToProxy);

    
    /**
     * 
     * @desc 流程定时器执行
     * @author WXJ
     * @date 2014-6-16 下午11:32:42
     *
     * @param taskId
     * @param processInstanceName
     * @param processInstanceId
     * @param businessInstanceId
     * @return
     * 
     */
    public Boolean processTaskTimer(ProcessTaskTimer timer);
	
    String getMailSubject(Map<String, Object> mailVariables, String status);
	
	String getMailContent(Map<String, Object> mailVariables,
			OaTaskInstance task, String processInstanceName, String flag);
	
	Map<String, Object> getMailVariablesByProcess(String submitUser,
			String processInstanceName, String nodeId,
			String processDefinitionId, String businessInstanceId,
			String result, String comment, OaTaskInstance oti,
			String processInstanceId) throws Exception;
	
	public void processEvents(List<ProcessNodeListener> events, Map<String,Object> bussinessObject);
	
	 /**
     * 
     * @author zhengbing 
     * @desc 拆分processAppSubmitInstance方法 1.为定时器推进流程作准备   2.发邮件   3.限制职级自动提交
     * @date 2014年7月17日 下午3:40:37
     * @return void
     */
    public void processAppSubmitInstanceNextStep(String submitUser,String processInstanceName,String businessObjectId,String pafProcessInstanceId,ProcessDefinition processDefinition) throws Exception;
    
}

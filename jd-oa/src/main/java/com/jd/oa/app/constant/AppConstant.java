package com.jd.oa.app.constant;

/**
 * Created with IntelliJ IDEA.
 * User: dbdongjian
 * Date: 13-10-14
 * Time: 下午5:38
 * To change this template use File | Settings | File Templates.
 */
public class AppConstant {
    /**
     * 流程分类
     */
    public static final String PROCESSCLASSIFICATION = "processApply";

    /**
     * 批准
     */
    public static final String APPROVE = "1";

    /**
     * 拒绝
     */
    public static final String REJECT_INFO = "2";

    /**
     * 驳回
     */
    public static final String REBACK_INFO = "3";
}

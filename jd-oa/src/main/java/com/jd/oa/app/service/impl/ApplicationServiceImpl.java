package com.jd.oa.app.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.app.service.ApplicationService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.redis.annotation.CacheKey;
import com.jd.oa.common.redis.annotation.CacheProxy;
import com.jd.oa.common.redis.annotation.CachePut;
import com.jd.oa.common.utils.IOUtils;
import com.jd.oa.common.utils.JavascriptEscape;
import com.jd.oa.form.execute.RuntimeFormManager;
import com.jd.oa.form.execute.RuntimeFormToolbar;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.model.bean.FormBean;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.form.util.RepleaseKey;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.model.ProcessNodeFormPrivilege;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.process.service.ProcessNodeFormPrivilegeService;
import com.jd.oa.process.service.ProcessNodeService;

/**
 * 业务操作service 实现类
 * @author liub
 *
 */
@Service
@CacheProxy
public class ApplicationServiceImpl implements ApplicationService{
	@Autowired
	private FormService formService;
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	@Autowired
    private ProcessNodeFormPrivilegeService processNodeFormPrivilegeService;
	@Autowired
	private ProcessNodeService processNodeService;
	@Autowired
	private FormTemplateService formTemplateService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private ProcessInstanceService processInstanceService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private JssService jssService;
	
	/**
	 * @see
	 */
	@Override
	public String getExecuteWorklistBindReportFormContext(HttpServletRequest request,String processDefinitionKey, String businessObjectId,String processNodeId,String processInstanceId,String processInstanceStatus) throws BusinessException {
		// 申请节点
		if(null != processDefinitionKey && (null==businessObjectId || "".equals(businessObjectId)) && (null==processInstanceId || "".equals(processInstanceId)) && (null==processInstanceStatus||"".equals(processInstanceStatus))){
			String jssKey = processDefinitionKey + SystemConstant.JDOA_APPLY_FORM_JSS_KEY_FIX;
			if(jssService.exist(SystemConstant.BUCKET, jssKey)){//取jss缓存
				return getApplyFormFromJSS(jssKey);
			}else{
				String formContent =  getExecuteWorklistBindReportFormContextForApply(request.getSession().getServletContext().getRealPath("/"), processDefinitionKey);
				try {
					jssService.uploadFile(SystemConstant.BUCKET, jssKey, formContent);//加入JSS缓存
				} catch (Exception e) {
				}
				return formContent;
			}
		}
		ProcessDefinition processDefinition = null;
		ProcessInstance processInstance = new ProcessInstance();
		boolean isEdit = true;
		if(processInstanceStatus != null && processInstanceStatus.trim().length() > 0 &&(processInstanceStatus.equals(SystemConstant.PROCESS_STATUS_0)||processInstanceStatus.equals(SystemConstant.PROCESS_STATUS_7)) ){//草稿显示
			if(processInstanceStatus.equals(SystemConstant.PROCESS_STATUS_7)){//查看他人草稿的状态 默认为只读
				isEdit = false;
			}
			processInstance.setBusinessInstanceId(businessObjectId);//通业务数据ID获取流程实例(processInstance)
			List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
			if(processInstances != null && processInstances.size() > 0){
				if(processInstances.get(0) != null){
					processInstance = processInstances.get(0);
				} else {
					throw new BusinessException("当前流程实例不存，请通知系统管理员!");
				}
			} else {
				throw new BusinessException("当前流程实例不存，请通知系统管理员!");
			}
			processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		} else if( processInstanceStatus != null && processInstanceStatus.trim().length() > 0 && (processInstanceStatus.equals(SystemConstant.PROCESS_STATUS_5) || processInstanceStatus.equals(SystemConstant.PROCESS_STATUS_3))){
			//历史，驳回显示
			processInstance.setProcessInstanceId(processInstanceId);//通流程实例ID获取流程实例(processInstance)
			List<ProcessInstance> processInstances = processInstanceService.find(processInstance);
			if(processInstances != null && processInstances.size() > 0){
				if(processInstances.get(0) != null){
					processInstance = processInstances.get(0);
				} else {
					throw new BusinessException("当前流程实例不存，请通知系统管理员!");
				}
			} else {
				throw new BusinessException("当前流程实例不存，请通知系统管理员!");
			}
			processDefinition = processDefService.get(processInstance.getProcessDefinitionId());
			businessObjectId = processInstance.getBusinessInstanceId();
			isEdit = false;
		} else {
			processInstance.setProcessInstanceId(processInstanceId);
			processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
			processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		}
    	Form form = getFormByProcessDefinitionId(processDefinition);
    	List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(form.getId(),SystemConstant.FORM_TYPE_S);//表单子表
		
		ProcessNode processNode = new ProcessNode();
		processNode.setProcessDefinitionId(processDefinition.getId());
		if(processNodeId == null ){
			processNode.setIsFirstNode("1");
		}else{
			processNode.setNodeId(processNodeId);
		}		
		List<ProcessNode> nodes = processNodeService.find(processNode);
		if(nodes == null || nodes.size() == 0){
			processNode = processNodeService.getFirstNode(processDefinition.getId());
		} else {
			processNode = nodes.get(0);
		}
		if(processNode == null){
			throw new BusinessException("流程节点不存在，请通知系统管理员!");
		}
		List<ProcessNodeFormPrivilege> processNodeFormPrivileges = getProcessNodeFormPrivileges(processDefinition.getId(), processNode.getNodeId(), form.getId());
		FormTemplate formTemplate = formTemplateService.get(processNode.getFormTemplateId());
		if(formTemplate == null){
			throw new BusinessException("流程节点【" + processNode.getNodeId() + "】绑定的表单模板不存");
		}
		List<FormItem> items = formItemService.getFormItemByFormIdAndType(form.getId().trim(),SystemConstant.FORM_TYPE_M);
		String formHtml = "";
		String businessTableName = getBuninessOperationDataTableNameByForm(form);
		Map<String,Object> businessData = formBusinessTableService.getBusinessData(businessTableName,businessObjectId);
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		RuntimeFormManager rfm = new RuntimeFormManager(contextPath,items,processNodeFormPrivileges,businessData,processDefinition, form,subSheets,processNode.getNodeId(),processInstance,businessTableName,businessObjectId);
		rfm.setExtendButtonJavascriptContext(this.getTemplateExtendButtonJavascriptContext(form.getId()));
		return RepleaseKey.replace(getTemplateContext(formTemplate), rfm.getFormPage(contextPath,isEdit,false), "[@", "]");
	}

	/**
	 * @author zhengbing 
	 * @desc 流程申请节点使用
	 * @date 2014年7月30日 上午11:25:08
	 * @return String
	 */
//	@CachePut(key = @CacheKey(template = "JDOA/ApplicationService/getExecuteWorklistBindReportFormContextForApply_${p1}"), expire = 7200)
	public String getExecuteWorklistBindReportFormContextForApply(String contextPath,String processDefinitionKey) throws BusinessException {
		// TODO Auto-generated method stub
		ProcessInstance processInstance = new ProcessInstance();
		processInstance.setProcessInstanceId(null);
		processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
    	Form form = getFormByProcessDefinitionId(processDefinition);
    	List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(form.getId(),SystemConstant.FORM_TYPE_S);//表单子表
		
		ProcessNode processNode = new ProcessNode();
		processNode.setProcessDefinitionId(processDefinition.getId());
		processNode.setIsFirstNode("1");
		List<ProcessNode> nodes = processNodeService.find(processNode);
		if(nodes == null || nodes.size() == 0){
			processNode = processNodeService.getFirstNode(processDefinition.getId());
		} else {
			processNode = nodes.get(0);
		}
		if(processNode == null){
			throw new BusinessException("流程节点不存在，请通知系统管理员!");
		}
		List<ProcessNodeFormPrivilege> processNodeFormPrivileges = getProcessNodeFormPrivileges(processDefinition.getId(), processNode.getNodeId(), form.getId());
		FormTemplate formTemplate = formTemplateService.get(processNode.getFormTemplateId());
		if(formTemplate == null){
			throw new BusinessException("流程节点【" + processNode.getNodeId() + "】绑定的表单模板不存");
		}
		List<FormItem> items = formItemService.getFormItemByFormIdAndType(form.getId().trim(),SystemConstant.FORM_TYPE_M);
		String businessTableName = getBuninessOperationDataTableNameByForm(form);
//		Map<String,Object> businessData = formBusinessTableService.getBusinessData(businessTableName,businessObjectId);
		RuntimeFormManager rfm = new RuntimeFormManager(contextPath,items,processNodeFormPrivileges,null,processDefinition, form,subSheets,processNode.getNodeId(),processInstance,businessTableName,"");
		rfm.setExtendButtonJavascriptContext(this.getTemplateExtendButtonJavascriptContext(form.getId()));
		return RepleaseKey.replace(getTemplateContext(formTemplate), rfm.getFormPage(contextPath,true,true), "[@", "]");
	}
	
	
	/**
     * 获取表单内容
     * @param formTemplate
     * @return
     */
    private String getTemplateContext(FormTemplate formTemplate) throws BusinessException{
    	if(formTemplate != null){
			String templateContext = "";
			try {
				templateContext = formTemplateService.getTemplateValue(formTemplate.getTemplatePath());
			} catch (IOException e) {
				throw new BusinessException("获取表单模板失败!",e);
			}
			if(templateContext.trim().length() > 0){
				templateContext = JavascriptEscape.unescape(templateContext);
				if(templateContext.indexOf("</form>") > 0){
					int subLength = templateContext.substring(templateContext.indexOf("</form>") + "</form>".length()).length();
					StringBuffer sb = new StringBuffer(templateContext);
					sb.setLength(sb.length() - subLength);
					templateContext = sb.toString();
				}
				return templateContext;
			} else {
				return "<span style=\"color:red;font-size:23px;\">当前模版已不存在，请重新选择!</span>";
			}
		} else {
			return "<span style=\"font-size:23px;\">当前表单模板不存在,占击[自动生成]可以根据当前数据源自动构建出用户界面</span>";
		}
    }
    /**
     * 获取javascript 配置内容
     * @param formId
     * @return
     */
    private String getTemplateExtendButtonJavascriptContext(String formId){
    	String javaScriptContext = formTemplateService.getJavascriptValue(formId);
    	return javaScriptContext;
    }
	/**
     * 获取节点表单权限配置
     * @param processDefinitionId
     * @param nodeId
     * @param formId
     * @return
     */
    private List<ProcessNodeFormPrivilege> getProcessNodeFormPrivileges(String processDefinitionId,String nodeId,String formId){
    	ProcessNodeFormPrivilege processNodeFormPrivilege = new ProcessNodeFormPrivilege();
    	processNodeFormPrivilege.setProcessDefinitionId(processDefinitionId);
    	processNodeFormPrivilege.setNodeId(nodeId);
    	//processNodeFormPrivilege.setFormId(formId);
    	return processNodeFormPrivilegeService.selectByCondition(processNodeFormPrivilege);
    }
	/**
	 * @see
	 */
	public Form getFormByProcessDefinitionId(ProcessDefinition processDefinition) throws BusinessException{
		if(processDefinition != null){
			if(processDefinition.getFormId() == null || processDefinition.getFormId().trim().length() == 0){
				throw new BusinessException("指定的流程未绑定表单");
			}
			return formService.get(processDefinition.getFormId());
		} else {
			throw new BusinessException("指定的流程定义KEY不存在");
		}
	}
	/**
	 * @see
	 */
	@Override
	public String getBuninessOperationDataTableNameByForm(Form form) throws BusinessException{
		// TODO Auto-generated method stub
		if(form != null && form.getTableId() != null && form.getTableId().trim().length() > 0){
			FormBusinessTable formBusinessTable = formBusinessTableService.get(form.getTableId());
			if(formBusinessTable != null){
				return formBusinessTable.getTableName();
			}
		}
		return "";
	}
	@Override
	public String getBuninessOperationToolbar(){
		return new RuntimeFormToolbar().toString();
	}
	/**
	 * @see
	 */
	@Override
	public String saveBusinessObject(String boTableName,String datas, String businessObjectId,String formId) {
		// TODO Auto-generated method stub
		List<FormItem> formItems = formItemService.getFormItemByFormIdAndType(formId,SystemConstant.FORM_TYPE_M);
		return formBusinessTableService.saveBusinessData(boTableName,datas, businessObjectId,formItems);
	}
	
	/**
	 * 
	 * @desc 保存主表业务数据
	 * @author WXJ
	 * @date 2014-3-20 上午11:25:21
	 *
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @param formId
	 * @param processNodeId
	 * @return
	 */
	public String saveBusinessObject(String boTableName,String datas,String businessObjectId,String formId,String processNodeId) {
		// TODO Auto-generated method stub
		List<FormItem> formItems = formItemService.getFormItemByFormIdAndType(formId,SystemConstant.FORM_TYPE_M,processNodeId);
		return formBusinessTableService.saveBusinessData(boTableName,datas, businessObjectId,formItems);
	}
	
	/**
	 * @see
	 */
	public String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,String formId){
		List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(formId,SystemConstant.FORM_TYPE_S);//表单子表
		List<FormItem> subSheetFormItems = null;
		for(int i = 0 ; i < subSheets.size() ; i++){
			FormBean formBean = subSheets.get(i);
			if(formBean != null && formBean.getFormCode() != null && formBean.getFormCode().equals(boTableName)){
				subSheetFormItems = formItemService.getFormItemByFormId(formBean.getFormId());
				break;
			}
		}
		return formBusinessTableService.saveBusinessObjectForSub(boTableName,datas, businessObjectId,subSheetFormItems);
	}
	/**
	 * 
	 * @desc 保存子表业务数据
	 * @author WXJ
	 * @date 2014-3-20 下午03:06:28
	 *
	 * @param boTableName
	 * @param datas
	 * @param businessObjectId
	 * @param formId
	 * @param processNodeId
	 * @return
	 */
	public String saveBusinessObjectForSub(String boTableName,String datas,String businessObjectId,String formId,String processNodeId){
		List<FormBean> subSheets = formItemService.getFormAndItemsByFormId(formId,SystemConstant.FORM_TYPE_S);//表单子表
		List<FormItem> subSheetFormItems = null;
		for(int i = 0 ; i < subSheets.size() ; i++){
			FormBean formBean = subSheets.get(i);
			if(formBean != null && formBean.getFormCode() != null && formBean.getFormCode().equals(boTableName)){
				subSheetFormItems = formItemService.getFormItemByFormId(formBean.getFormId());
				break;
			}
		}
		return formBusinessTableService.saveBusinessObjectForSub(boTableName,datas, businessObjectId,subSheetFormItems);
	}
	/**
	 * @see
	 */
	public List<Map<String,Object>> getBusinessDatas(String boTableName,String businessObjectId){
		return formBusinessTableService.getBusinessDatas(boTableName,businessObjectId);
	}
	/**
	 * @see
	 */
	public String deleteBindReportData(String boTableName,String id){
		return formBusinessTableService.deleteBindReportData(boTableName,id);
	}
	/**
	 *
	 * @param boTableName
	 * @param parentId
	 * @return
	 */
	@Override
	public String deleteBusinessObjectByParentId(String boTableName, String parentId) {
		if(boTableName == null || boTableName.trim().length() == 0) return "";
		if(parentId == null || parentId.trim().length() == 0) return "";
		return formBusinessTableService.deleteBusinessObjectByParentId(boTableName,parentId);
	}

	/**
 * 流程实例根据业务数据查询条件动态生成（全部）
 * @param  processDefinitionKey( processxxxxxxx)
 */
	@Override
	public String getDynamicQueryConditionsHtml(HttpServletRequest request,String processDefinitionKey) {
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		Form form = getFormByProcessDefinitionId(processDefinition);
		List<FormItem> items = formItemService.getFormItemByFormIdAndType(form.getId().trim(),SystemConstant.FORM_TYPE_M);
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		RuntimeFormManager rfm = new RuntimeFormManager(contextPath,items,processDefinition);
		// TODO Auto-generated method stub
		return rfm.getDynamicQueryConditions();
	}
	/**
	 * 流程实例根据业务数据查询条件动态生成 根据itemID查询某个
	 * @param  processDefinitionKey( processxxxxxxx)
	 * @param  itemId 
	 */
	@Override
	public String getDynamicQueryConditionsHtml(HttpServletRequest request,String processDefinitionKey,String itemId) {
		ProcessDefinition processDefinition = processDefService.getByDefinitionId(processDefinitionKey);
		List<FormItem> items = new ArrayList();
		items.add(formItemService.get(itemId));
		String contextPath = request.getSession().getServletContext().getRealPath("/");
		RuntimeFormManager rfm = new RuntimeFormManager(contextPath,items,processDefinition);
		// TODO Auto-generated method stub
		return rfm.getDynamicQueryConditions();
	}
	
	/**
	 * @author zhengbing 
	 * @desc 从jss上获取申请节点流程表单缓存
	 * @date 2014年8月1日 下午4:18:33
	 * @return String
	 */
	private String getApplyFormFromJSS(String key) throws BusinessException{
		String result = null;
		InputStream in = null;
		try{
			in = jssService.downloadFile(SystemConstant.BUCKET, key);
			result = IOUtils.toString(in);
		} catch(IOException e){
			throw new BusinessException("获取申请表单缓存失败！",e);
		} finally {
        	try{
        		in.close();
        	} catch(IOException e) {
        		throw new BusinessException("获取申请表单缓存失败！",e);
        	} catch(NullPointerException e){
        		throw new BusinessException("获取申请表单缓存失败！",e);
        	}
        }
		return result;
	}
}

package com.jd.oa.app.model;

import java.util.Date;
import java.util.List;

/**
 * 流程实例Domain（查询流程实例专用）
 * User: WXJ
 * Date: 13-10-15
 * Time: 下午6:14
 */
public class OaProcessInstance {

    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 流程实例创建时间
     */
    private String startTime;
    /**
     * 业务主键ID
     */
    private String businessKey;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 启动活动ID
     */
    private String startActivityId;
    /**
     * 流程发起者
     */
    private String startUserId;
    /**
     * 是否完成
     */
    private String completed;

    private List<OaTaskInstance> tasks;
    /**
     * 任务状态
     */
    private String status;
    /**
     * 流程结束时间
     */
    private String endTime;
    /**
     * 最后更新时间
     */
    private String lastTaskTime;
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getStartActivityId() {
        return startActivityId;
    }

    public void setStartActivityId(String startActivityId) {
        this.startActivityId = startActivityId;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public List<OaTaskInstance> getTasks() {
        return tasks;
    }

    public void setTasks(List<OaTaskInstance> tasks) {
        this.tasks = tasks;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getLastTaskTime() {
        return lastTaskTime;
    }

    public void setLastTaskTime(String lastTaskTime) {
        this.lastTaskTime = lastTaskTime;
    }
}

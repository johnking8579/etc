package com.jd.oa.common.file;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.*;

public class FileUtil {

	/**
	 * 字节数组转Base64字符串
	 * @param bytes
	 * @return
	 */
	public static String Base64String(byte[] bytes){
		if(bytes == null || bytes.length == 0) return null;
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(bytes);
	}

	/**
	 * Base64字符串转字节数组
	 * @param base64Str
	 * @return
	 */
	public static byte[] Base64String2Bytes(String base64Str){
		byte[] bytes = null;
		if(base64Str == null || base64Str.trim().length() == 0) return bytes;
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			bytes = decoder.decodeBuffer(base64Str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}

	/**
	 * 对象转成字节数组
	 * @param o
	 * @return
	 */
	public static byte[] Object2Bytes(Object o){
		if(o == null) return null;
		ByteArrayOutputStream bos =new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream = null;
		try {
			objectOutputStream = new ObjectOutputStream(bos);
			objectOutputStream.writeObject(o);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(objectOutputStream != null) objectOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bos.toByteArray();
	}

	/**
	 * 字节数组转成对象
	 * @param bytes
	 * @return
	 */
	public static Object byte2Object(byte[] bytes){
		Object resultObject = null;
		if(bytes == null || bytes.length == 0) return resultObject;
		ObjectInputStream objectInputStream = null;
		try {
			objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
			resultObject = objectInputStream.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				objectInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return  resultObject;
	}



	/**
	 * 将字节数组压缩
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static byte[] getGZIPBytes(byte[] bytes) throws Exception{
		if(bytes == null || bytes.length == 0) return bytes;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream gos = new GZIPOutputStream(bos);
		gos.write(bytes);
		gos.close();
		return bos.toByteArray();
	}

	/**
	 * 将GZIP压缩后的字节数组，解压为正常字节数组
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static byte[] parseGZIPBytes(byte[] bytes) throws Exception{
		if(bytes == null || bytes.length == 0) return bytes;
		GZIPInputStream gzipInputStream = new GZIPInputStream(new ByteArrayInputStream(bytes));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len=0;
		while ((len = gzipInputStream.read(buffer)) >0){
			bos.write(buffer,0,len);
		}
		gzipInputStream.close();
		return bos.toByteArray();
	}




	/**
	 * 将流转成字节数组
	 * @param inputStream 输入流
	 * @return 字节数组
	 * @throws Exception
	 */
	public static  byte[] getBytes(InputStream inputStream) throws  Exception{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int len;
		byte[] buffer = new byte[1024];
		while (( len = inputStream.read( buffer )) != -1){
			bos.write(buffer,0,len);
		}
		bos.close();
		return bos.toByteArray();
	}

	/**
	 * 多个文件打成zip包
	 * @param zipFilePath
	 * @param files
	 * @return
	 */
	public static boolean zipFiles(String zipFilePath,List<File> files){
		boolean isSuccess=false;
		ZipOutputStream zipOut=null;
		FileInputStream fin=null;
		byte[] buffer=new byte[1024];
		int len=0;
		try {
			zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
			for(int i=0;i<files.size();i++){
				fin=new FileInputStream(files.get(i));
				zipOut.putNextEntry(new ZipEntry(files.get(i).getName()));
				
				while((len = fin.read(buffer))>0) {
					zipOut.write(buffer,0,len);
				}
				zipOut.closeEntry();
				fin.close();
				files.get(i).delete();
			}
			isSuccess=true;
		} catch (Exception e) {
			isSuccess=false;
			e.printStackTrace();
		}finally{
			if(zipOut!=null)
				try {
					zipOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return isSuccess;
	}
	/**
	 * 解压zip文件
	 * @param inputStream
	 * @return
	 */
	public static List<File> tarZipFiles(InputStream inputStream){
		List<File> files=new ArrayList<File>();
		ZipInputStream zin=null;
		ZipEntry zipEntry=null;
		File outFile=null;
		
		FileOutputStream fos=null;
		byte[] buffer=new byte[1024];
		int len=0;
		try {
			zin=new ZipInputStream(inputStream);
			while((zipEntry=zin.getNextEntry())!=null){
				outFile=new File(zipEntry.getName());
				fos=new FileOutputStream(outFile);
				while((len=zin.read(buffer))>0){
					fos.write(buffer,0,len);
				}
				fos.close();
				files.add(outFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(zin!=null) zin.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return files;
	}
	/**
	 * 将文件转化为字节流使用
	 * @param file
	 * @return
	 */
	public static byte[] getFileBytes(File file){
		FileInputStream fin=null;
		byte[] buffer=new byte[1024];
		int len=0;
		ByteArrayOutputStream bos=null;
		try {
			fin=new FileInputStream(file);
			bos=new ByteArrayOutputStream();
			while((len=fin.read(buffer))>0){
				bos.write(buffer,0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(bos!=null) bos.close();
				if(fin!=null) fin.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		if(bos!=null){
			return bos.toByteArray();
		}else{
			return null;
		}
		
	}
	/**
	 * 将文字内容输出到指定文件
	 * @param filePath
	 * @param content
	 * @return
	 */
	public static File createFile(String filePath,String content){
		FileOutputStream fos=null;
		byte[] buffer=new byte[1024];
		int len=0;
		//创建流程图文件
		ByteArrayInputStream jpdlXmlInputStream=null;
		try {
			jpdlXmlInputStream = new ByteArrayInputStream(content.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		try {
			fos=new FileOutputStream(filePath);
			if(jpdlXmlInputStream!=null){
				while((len = jpdlXmlInputStream.read(buffer))>0) {
					fos.write(buffer,0,len);
					fos.flush();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(fos!=null)
				try {
					fos.close();
					jpdlXmlInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return new File(filePath);
	}
	/**
	 * 获取指定（fileName）的扩展名称
	 * @param filename 文件名称
	 * @return 返回文件扩展名
	 */
	public static String getFileExtensionName(String filename){
		 if ((filename != null) && (filename.length() > 0)) {   
	          int dot = filename.lastIndexOf('.');   
	          if ((dot > -1) && (dot < (filename.length() - 1))) {   
	             return filename.substring(dot + 1);   
	          }   
	     }   
	     return filename; 
	}
	/**
	 * 获取不带扩展名的文件名称
	 * @param filename 文件名
	 * @return 返回文件不带扩展名部分
	 */
	public static String getFileNameNoEx(String filename) {   
        if ((filename != null) && (filename.length() > 0)) {   
            int dot = filename.lastIndexOf('.');   
            if ((dot >-1) && (dot < (filename.length()))) {   
                return filename.substring(0, dot);   
            }   
        }   
        return filename;   
    } 
	/**
	 * 根据文件名称，获取文件icon
	 * @param filename 文件名称
	 * @return 返回文件格式图标
	 */
	public static String getFileIcon(String filename){
		String icon = "";
		String exName = getFileExtensionName(filename);
		if(exName.equals("avi")){// avi
			icon = "avi.jpg";
		} else if(exName.equals("zip")){//zip
			return "zip.jpg";
		} else if(exName.equals("rar")){//rar
			return "rar.jpg";
		} else if(exName.equals("exe")){//exe
			return "exe.jpg";
		} else if(exName.equals("xls") || exName.equals("xlsx")){//xls
			return "excel.jpg";
		} else if(exName.equals("doc") || exName.equals("docx")){//doc
			return "word.jpg";
		} else if(exName.equals("ppt") || exName.equals("pptx") ){//ppt
			return "ppt.jpg";
		} else if(exName.equals("txt")){//txt
			return "txt.jpg";
		} else if(exName.equals("chm")){//chm
			return "chm.jpg";
		} else if(exName.equals("pdf")){//pdf
			return "pdf.jpg";
		} else if(exName.equals("jpg")){//jpg
			return "jpg.jpg";
		} else if(exName.equals("bmp")){//bmp
			return "bmp.jpg";
		} else if(exName.equals("gif")){//gif
			return "pic.jpg";
		} else if(exName.equals("html") || exName.equals("htm") || exName.equals("xhtml")){//html
			return "html.jpg";
		} else{
			return "normal.jpg";
		}
		return icon;
	}
	/**
	 *  获取操作系统文件路径分隔符
	 */
	public static String getSep(){
	    return System.getProperties().getProperty("file.separator"); 
	}
	/**
	 * 检查文件路径是否有效
	 * @param path 文件路径
	 */
	public static void checkPath(String path) throws IOException{
		File filePath = new File(path);
		if(!filePath.exists()){
			throw new IOException("无效的文件路径【" + path + "】;");
		}
	}
}

package com.jd.oa.common.paf.model.processinstance;

import java.util.List;

/**
 * 任务信息Domain（查询流程实例专用）
 * User: zhaoming
 * Date: 13-10-11
 * Time: 下午8:02
 * To change this template use File | Settings | File Templates.
 */
public class Task {

    private String taskId;
    private String taskName;
    private String owner;
    private String assignee;
    private String startTime;
    private String completed;
    private String endTime;
    private String duration;
    private List<Comment> comments;
    private String candidateUsers;
    private String uncompletedSubTaskExist;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

	public String getCandidateUsers() {
		return candidateUsers;
	}

	public void setCandidateUsers(String candidateUsers) {
		this.candidateUsers = candidateUsers;
	}

	public String getUncompletedSubTaskExist() {
		return uncompletedSubTaskExist;
	}

	public void setUncompletedSubTaskExist(String uncompletedSubTaskExist) {
		this.uncompletedSubTaskExist = uncompletedSubTaskExist;
	}
    
}

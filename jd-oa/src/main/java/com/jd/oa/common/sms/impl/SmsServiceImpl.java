package com.jd.oa.common.sms.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.task.TaskExecutor;

import com.jd.mobilephonemsg.sender.ws.server.newmessageservice.Message;
import com.jd.mobilephonemsg.sender.ws.server.newmessageservice.MmsResult;
import com.jd.msg.sender.client.proxyClient.SendUmMsg;
import com.jd.oa.common.sms.SmsService;

public class SmsServiceImpl implements SmsService {

	private static final Logger logger = Logger.getLogger(SmsServiceImpl.class);

	/**
	 * 异步执行器
	 */
	private TaskExecutor taskExecutor;
	/**
	 * 发送者编号
	 */
	private String senderNum;

	private SendUmMsg sendUmMsg;

	public boolean sentMessage(String mobile, String msgContent) {
		Message message = new Message();
		message.setMobileNum(mobile);
		message.setMsgContent(msgContent);
		message.setOrderId("");
		message.setSenderNum(senderNum);
		message.setType("");// ""为普通短信，1为促销短信
		logger.info("Send message: [mobile: " + mobile + ", msgContent: "
				+ msgContent + ", senderNum: "+ senderNum + "]");
		MmsResult result = sendUmMsg.sendSMS(message);
		logger.debug("Send message result: [ReceiptNum: "
				+ result.getReceiptNum() + ", ReceiptDesc: "
				+ result.getReceiptDesc()+ "]");
		return result.getReceiptNum().equals("1") ? true : false;
	}
	
	public boolean sentMessageByAsyn(final String mobile, final String msgContent) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				sentMessage(mobile, msgContent);
			}
		});
		return true;
	}

	public boolean massMessage(List<String> mobiles, String msgContent) {
		Message message = new Message();
		message.setMobileNum(listToString(mobiles));
		message.setMsgContent(msgContent);
		message.setOrderId("");
		message.setSenderNum(senderNum);
		message.setType("");// ""为普通短信，1为促销短信
		logger.info("Mass message: [mobiles.size: " + mobiles.size()
				+ ", msgContent: " + msgContent + ", senderNum: "+ senderNum + "]");
		MmsResult result = sendUmMsg.sendSMS(message);
		logger.debug("Mass message result: [ReceiptNum: "
				+ result.getReceiptNum() + ", ReceiptDesc: "
				+ result.getReceiptDesc() + "]");
		return result.getReceiptNum().equals("1") ? true : false;
	}
	
	public boolean massMessageByAsyn(final List<String> mobiles, final String msgContent) {
		taskExecutor.execute(new Runnable() {
			public void run() {
				massMessage(mobiles, msgContent);
			}
		});
		return true;
	}

	/**
	 * 把list转换为一个用逗号分隔的字符串
	 * 
	 * @param list
	 * @return
	 */
	private String listToString(List<String> list) {
		StringBuilder sb = new StringBuilder();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (i < list.size() - 1) {
					sb.append(list.get(i) + ",");
				} else {
					sb.append(list.get(i));
				}
			}
		}
		return sb.toString();
	}

	public String getSenderNum() {
		return senderNum;
	}

	public void setSenderNum(String senderNum) {
		this.senderNum = senderNum;
	}

	public SendUmMsg getSendUmMsg() {
		return sendUmMsg;
	}

	public void setSendUmMsg(SendUmMsg sendUmMsg) {
		this.sendUmMsg = sendUmMsg;
	}

	public TaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	public void setTaskExecutor(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}
}

package com.jd.oa.common.exception.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.jd.oa.common.exception.AlertWindow;
import com.jd.oa.common.exception.BusinessException;

/**
 * 通用异常处理类
 * User: zhaoming
 * Date: 13-8-29
 * Time: 下午22:49
 * To change this template use File | Settings | File Templates.
 */
public class CustomSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

    private static final Logger logger = Logger.getLogger(CustomSimpleMappingExceptionResolver.class);
	@Override
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex) {
		// TODO Auto-generated method stub
		ex.printStackTrace();
        logger.error(ex.getMessage(), ex);
        if (!isAjaxSumbit(request)) {//如果不是异步请求
	        ModelAndView mav = new ModelAndView();
	        mav.addObject("title","提示");
			mav.setViewName("error/global");
		    if (ex instanceof BusinessException) {
		       mav.addObject("message", ex.getMessage());
		    } else if(ex instanceof NullPointerException){
		      mav.addObject("message", ex.getMessage());
		    } else if(ex instanceof RuntimeException){
			  mav.addObject("message", ex.getMessage());
			} else if(ex instanceof IOException){
				  mav.addObject("message", ex.getMessage());
			} else if(ex instanceof Exception){
				  mav.addObject("message", ex.getMessage());
			}
		    return mav;
        } else {
            return super.resolveException(request, response, handler, ex);
        }
	}
    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    	String viewName = determineViewName(ex, request);
        if (viewName != null) {// 格式返回
            if (!isAjaxSumbit(request)) {
                // 如果不是异步请求
                // Apply HTTP status code for error views, if specified.
                // Only apply it if we're processing a top-level request.
                Integer statusCode = determineStatusCode(request, viewName);
                if (statusCode != null) {
                    applyStatusCodeIfPossible(request, response, statusCode);
                }
                return getModelAndView(viewName, ex, request);
            } else {// JSON格式返回
                try {
                    response.setContentType("text/xml;charset=UTF-8");//中文乱码
//                    response.sendError(response.SC_INTERNAL_SERVER_ERROR);
                    PrintWriter writer = response.getWriter();
                    writer.write(ex.getMessage());
                    writer.flush();
                } catch (IOException e) {
                    logger.error(e);
                }
                return null;

            }
        } else {
            return null;
        }
    }

    /**
     * 判断是否ajax提交
     *
     * @param request
     *
     * @return
     */
    public boolean isAjaxSumbit(HttpServletRequest request) {
        return (request.getHeader("accept").indexOf("application/json") > -1 || (request.getHeader("X-Requested-With") != null && request.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1));
    }
}
package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface RemoveCache {
    //    String simpleKey() default "";
    CacheKey[] key();

    String type() default "";

    boolean invokeBefore() default false;

    boolean markAsNull() default false;

    int expire() default 0;

    String expireTime() default "";


}

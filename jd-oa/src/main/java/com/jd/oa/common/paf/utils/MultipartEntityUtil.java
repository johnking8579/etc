package com.jd.oa.common.paf.utils;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 发布流程图
 * User: zhaoming
 * Date: 13-9-24
 * Time: 下午4:47
 * To change this template use File | Settings | File Templates.
 */
public class MultipartEntityUtil {

    public static String httpClientPostFile(File file,String url) throws ClientProtocolException, IOException {

//        FileBody bin = null;
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpPost httppost = new HttpPost(url);
//        if(file != null) {
//            bin = new FileBody(file);
//        }
//
//        StringBody username = new StringBody("13696900475");
//        StringBody password = new StringBody("13696900475");
//        //new StringBody("汉字",Charset.forName("UTF-8")));
//
//        MultipartEntity reqEntity = new MultipartEntity();
////        reqEntity.addPart("username", username);
////        reqEntity.addPart("password", password);
//        reqEntity.addPart("data", bin);
//
//        httppost.setEntity(reqEntity);
//        System.out.println("执行: " + httppost.getRequestLine());
//
//        HttpResponse response = httpclient.execute(httppost);
//        System.out.println("statusCode is " + response.getStatusLine().getStatusCode());
//        HttpEntity resEntity = response.getEntity();
//        System.out.println("----------------------------------------");
//        System.out.println(response.getStatusLine());
//        if (resEntity != null) {
//            System.out.println("返回长度: " + resEntity.getContentLength());
//            System.out.println("返回类型: " + resEntity.getContentType());
//            InputStream in = resEntity.getContent();
//            System.out.println("in is " + in);
////            System.out.println(IoStreamUtil.getStringFromInputStream(in));
//        }
//        if (resEntity != null) {
//            resEntity.consumeContent();
//        }
        return null;
    }

    public static void cxfPostFile(File file,String url){
    	InputStream inputStream = null;
        try {
//            WebClient client = WebClient.create(url);
//            client.type("multipart/form-data");
//            InputStream is = new FileInputStream(file);
//            ContentDisposition cd = new ContentDisposition("attachment;filename=contractCreate.bpmn20.xml");
//            Attachment att = new Attachment("root", is, cd);
//            client.snpost(att);

            List providers = new ArrayList();
            providers.add(new JacksonJsonProvider());
            WebClient sweWebClient = WebClient.create(url, providers);
            sweWebClient.type("multipart/form-data");
            Response response = sweWebClient.post(file);

            System.out.println("状态码: " +  response.getStatus());
            inputStream = (InputStream) response.getEntity();
            StringBuffer buffer = new StringBuffer();
            byte[] bytes = new byte[inputStream.available()];
            while(inputStream.read(bytes) != -1){
                buffer.append(new String(bytes,"utf-8"));
            }
            System.out.println("内容: " +  buffer.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	try{
        		inputStream.close();
        	} catch(IOException e) {
        	}
        }
    }



    public static void main(String[] args) throws ClientProtocolException, IOException {

        File file = new File("c:/temp/contractCreate.bpmn20.xml");
        String url = "http://192.168.224.21:8080/SWPWebConsole/service/processDef/deploy?tenant=test&userName=admin";
        cxfPostFile(file, url);
    }
}

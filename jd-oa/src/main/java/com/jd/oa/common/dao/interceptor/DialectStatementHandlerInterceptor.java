package com.jd.oa.common.dao.interceptor;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;

import com.jd.oa.common.dao.dialect.Dialect;

@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class }) })
public class DialectStatementHandlerInterceptor implements Interceptor {

	private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
	private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
	protected static Logger logger = Logger
			.getLogger(DialectStatementHandlerInterceptor.class);

	private boolean supportMultiDS;

	private String dialect;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.ibatis.plugin.Interceptor#intercept(org.apache.ibatis.plugin
	 * .Invocation)
	 */
	public Object intercept(Invocation invocation) throws Throwable {
		StatementHandler statementHandler = (StatementHandler) invocation
				.getTarget();
		MetaObject metaStatementHandler = MetaObject.forObject(
				statementHandler, DEFAULT_OBJECT_FACTORY,
				DEFAULT_OBJECT_WRAPPER_FACTORY);

		RowBounds rowBounds = (RowBounds) metaStatementHandler
				.getValue("delegate.rowBounds");
		if (rowBounds == null || rowBounds == RowBounds.DEFAULT) {
			return invocation.proceed();
		}

		DefaultParameterHandler defaultParameterHandler = (DefaultParameterHandler) metaStatementHandler
				.getValue("delegate.parameterHandler");

		Configuration configuration = (Configuration) metaStatementHandler
				.getValue("delegate.configuration");

		String dialectType = null;
		try {
			dialectType = dialectType.valueOf(configuration.getVariables()
					.getProperty("dialect"));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		if (dialectType == null) {
			throw new RuntimeException(
					"the value of the dialect property in configuration.xml is not defined : "
							+ configuration.getVariables().getProperty(
									"dialect"));
		}
		Dialect dialect = (Dialect) Class.forName(dialectType).newInstance();

		String originalSql = (String) metaStatementHandler
				.getValue("delegate.boundSql.sql");

		if(defaultParameterHandler.getParameterObject() instanceof Map){
			Map parameterMap = (Map) defaultParameterHandler.getParameterObject();
			if (parameterMap.get("_sorts") != null) {
				if (parameterMap.get("_sorts") instanceof LinkedHashMap) {
					@SuppressWarnings("unchecked")
					LinkedHashMap<String, String> sorts = (LinkedHashMap<String, String>) parameterMap
							.get("_sorts");
					// 使用自定义排序
					originalSql = dialect.getOrderString(originalSql, sorts);
				} else {
					if (logger.isDebugEnabled()) {
						logger.debug("the sort parameter must be 'LinkedHashMap' type");
					}
				}
			}
		}

		// 使用自定义的物理分页方法。若不使用自定义分页（此处注释该方法），则使用mybatis的逻辑分页。物理分页效率高于逻辑分页
		originalSql = dialect.getLimitString(originalSql,
				rowBounds.getOffset(), rowBounds.getLimit());
		// 如果使用自定义的物理分页法，请打开下边的两个注释。
		metaStatementHandler.setValue("delegate.rowBounds.offset",
				RowBounds.NO_ROW_OFFSET);
		metaStatementHandler.setValue("delegate.rowBounds.limit",
				RowBounds.NO_ROW_LIMIT);

		metaStatementHandler.setValue("delegate.boundSql.sql", originalSql);

		if (logger.isDebugEnabled()) {
			BoundSql boundSql = statementHandler.getBoundSql();
			logger.debug("生成分页SQL : " + boundSql.getSql());
		}
		return invocation.proceed();
	}

	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	public void setProperties(Properties properties) {
	}

	public boolean isSupportMultiDS() {
		return supportMultiDS;
	}

	public void setSupportMultiDS(boolean supportMultiDS) {
		this.supportMultiDS = supportMultiDS;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
}

package com.jd.oa.common.webservice;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.ClientImpl;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.service.model.BindingInfo;
import org.apache.cxf.service.model.BindingMessageInfo;
import org.apache.cxf.service.model.BindingOperationInfo;
import org.apache.cxf.service.model.MessagePartInfo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangdongxing on 14-3-9.
 * Email : wangdongxing@jd.com
 */
public class CXFDynamicClient {

	private String wsdlUrl;
	private String namespace;

	private JaxWsDynamicClientFactory jaxWsDynamicClientFactory;
	private Client client;
	private ClientImpl clientImpl;

	public CXFDynamicClient(String wsdlUrl){
		this.wsdlUrl = wsdlUrl;
		this.init();
	}

	public void init(){
		this.jaxWsDynamicClientFactory = JaxWsDynamicClientFactory.newInstance();
		this.client = jaxWsDynamicClientFactory.createClient(this.wsdlUrl);
		this.clientImpl = (ClientImpl) this.client;
		this.namespace = this.clientImpl.getEndpoint().getService().getName().getNamespaceURI();
		this.client.getInInterceptors().add(new LoggingInInterceptor());
		this.client.getOutInterceptors().add(new LoggingOutInterceptor());
	}

	public List<Class<?>> getParamterClass(String method){
		List<Class<?>> paramters=new ArrayList<Class<?>>();
		QName qName = new QName(this.namespace,method);
		BindingInfo bindingInfo = this.clientImpl.getEndpoint().getEndpointInfo().getBinding();
		BindingOperationInfo bindingOperationInfo=bindingInfo.getOperation(qName);
		BindingMessageInfo inputMessageInfo = bindingOperationInfo.getInput();
		List<MessagePartInfo> parts = inputMessageInfo.getMessageParts();
		for(MessagePartInfo part:parts){
			paramters.add(part.getTypeClass());
		}
		return paramters;
	}

	public Object newInstance(Class<?> paramterClass,Map<String,Object> valueMap){
		Object o= null;
		try {
			o = Thread.currentThread().getContextClassLoader().loadClass(paramterClass.getName()).newInstance();
			Method[] methods=paramterClass.getDeclaredMethods();
			Set<Map.Entry<String,Object>> entrySet = valueMap.entrySet();
			for(Map.Entry<String,Object> entry:entrySet){
				for(Method method:methods){
					if(method.getName().startsWith("set")){
						String methodName=method.getName().replace("set","");
						if(methodName.toLowerCase().equals(entry.getKey().toLowerCase())){
							method.invoke(o,entry.getValue());
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  o;
	}

	public void addSoapHeader(String headerTag,Map<String,Object> headerMap){
		List<Header> headersList=(List<Header>) client.getRequestContext().get(Header.HEADER_LIST);
		if(headersList == null) headersList=new ArrayList<Header>();
		Document doc = (Document) DOMUtils.createDocument();
		Element root=doc.createElement(headerTag);
		for(Map.Entry<String,Object> entry:headerMap.entrySet()){
			Element element=doc.createElement(entry.getKey());
			element.setTextContent((String) entry.getValue());
			root.appendChild(element);
		}
		//加入<soap:header>头信息
		Header header=new Header(new QName(this.namespace,headerTag),root);
		headersList.add(header);
		this.client.getRequestContext().put(Header.HEADER_LIST, headersList);
	}

	public void addSoapHeader(String headerTag,Object headerModel){
		List<Header> headersList=(List<Header>) client.getRequestContext().get(Header.HEADER_LIST);
		if(headersList == null) headersList=new ArrayList<Header>();
		//加入<soap:header>头信息
		Header header= null;
		try {
			header = new Header(new QName(this.namespace,headerTag),headerModel,new JAXBDataBinding(headerModel.getClass()));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		headersList.add(header);
		this.client.getRequestContext().put(Header.HEADER_LIST, headersList);
	}

	public Object[] invoke(String method,Object... params){
		try {
			if(params!=null&&params.length > 0){
				return client.invoke(method,params);
			}else{
				return client.invoke(method);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw  new RuntimeException(e);
		}
	}

	public Object[] invoke(RESULT_DATA_TYPE returnType,String method,Object... params){
		Object[] objects  =  this.invoke(method,params);
		if(objects  ==  null ) return  null;
		for(int i = 0;i<objects.length;i++){
			if(objects[i] instanceof  List){
				List list = (List) objects[i];
				for(int j = 0;j<list.size();j++){
					Object o = list.remove(j);
					list.add(j,DataConvertion.convertData(returnType,o));
				}
			}else if(objects[i].getClass().isArray()){
				Object[] os = (Object[]) objects[i];
				for(int j = 0;j<os.length;j++){
					os[j]  = DataConvertion.convertData(returnType,os[j]);
				}
			}else{
				objects[i] = DataConvertion.convertData(returnType,objects[i]);
			}
		}
		return objects;
	}

	/**
	 * 销毁会话
	 */
	public void destroy(){
		if(this.client != null){
			this.client.destroy();
		}
	}

}

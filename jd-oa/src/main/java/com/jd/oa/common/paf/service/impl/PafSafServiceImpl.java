package com.jd.oa.common.paf.service.impl;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.model.processinstance.ProcessInstanceDomain;
import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.swp.proxy.HumanService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PAF工作流平台服务类(SAF方式)
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午5:18
 */

public class PafSafServiceImpl implements PafService {

    private final static Log LOG = LogFactory.getLog(PafSafServiceImpl.class);

    /**
     * PAF平台SAF服务
     */
    private HumanService humanService;
    /**
     * 平台密码
     */
    private String password;
    /**
     * 租户
     */
    private String tenant;

    /**
     * 创建流程实例
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @return result
     */
    public PafResult<ProcessInstance> createProcessInstance(String loginUser, String processDefKey, String businessKey){

        PafResult<ProcessInstance> result = null;
        try {
            String processInstanceId = humanService.createProcessInstance(loginUser, password, processDefKey, businessKey);
            ProcessInstance processInstance =  new ProcessInstance();
            processInstance.setId(processInstanceId);
            result = new PafResult<ProcessInstance>(true, processInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<ProcessInstance>(false);
            result.setErrorCode("createProcessInstance");
            result.setLocalizedMessage("创建流程实例失败");
            result.setErrorStack(e.getMessage());
        }
        return result;

    }
    
    /**
     * 推进第一个人工任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processInstanceId 流程实例
     * @param variables 流程变量
     * @return result
     */
    public PafResult<ProcessInstance> submitHumanProcessInstance(String loginUser, String processInstanceId, Map<String, Object> variables){
        PafResult<ProcessInstance> result = null;
        try {
            humanService.submitHumanProcessInstance(loginUser, password, processInstanceId, variables);
            ProcessInstance processInstance =  new ProcessInstance();
            processInstance.setId(processInstanceId);
            result = new PafResult<ProcessInstance>(true, processInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<ProcessInstance>(false);
            result.setErrorCode("submitHumanProcessInstance");
            result.setLocalizedMessage("推进第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;

    }
    
    /**
     * 创建流程实例并推进第一个人工任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @param variables 流程变量
     * @return result
     */
    public PafResult<ProcessInstance> submitHumanProcessInstance(String loginUser, String processDefKey, String businessKey, Map<String, Object> variables){

//        ObjectMapper mapper = new ObjectMapper();
        PafResult<ProcessInstance> result = null;
        try {
            String processInstanceId = humanService.createProcessInstance(loginUser, password, processDefKey, businessKey);
            humanService.submitHumanProcessInstance(loginUser, password, processInstanceId, variables);
            ProcessInstance processInstance =  new ProcessInstance();
            processInstance.setId(processInstanceId);
            result = new PafResult<ProcessInstance>(true, processInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<ProcessInstance>(false);
            result.setErrorCode("submitHumanProcessInstance");
            result.setLocalizedMessage("创建流程实例并推进第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;

    }

    /**
     * 提交任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 需要完成的任务ID
     * @param variables  流程变量
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeUserTask(String loginUser, String taskId, Map<String, Object> variables){

        PafResult<Boolean> result = null;
        try {
            humanService.completeUserTask(loginUser, password, taskId, variables);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeUserTask");
            result.setLocalizedMessage("提交任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 回退到上一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToPreviousUserTask(String loginUser, String taskId, Map<String, Object> variables){

        PafResult<Boolean> result = null;
        try {
            variables.put("RETREAT","1");
            humanService.completeUserTask(loginUser, password, taskId, variables);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToPreviousUserTask");
            result.setLocalizedMessage("回退到上一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 回退到第一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToFirstUserTask(String loginUser, String taskId, Map<String, Object> variables){

        PafResult<Boolean> result = null;
        try {
            variables.put("RETREAT","-1");
            humanService.completeUserTask(loginUser, password, taskId, variables);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToFirstUserTask");
            result.setLocalizedMessage("回退到第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 加签
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param parentTaskId 任务ID
     * @param users 办理人列表
     * @return 是否正确完成
     */
    public PafResult<Boolean> createSubTask(String loginUser, String parentTaskId, List<String> users){
        PafResult<Boolean> result = null;
        try {
    		List<String> addSignUsers = new ArrayList<String>();
        	if (users!=null) {
        		for (String user : users) {
        			addSignUsers.add(user.toLowerCase());
        		}
        	}
            humanService.createSubTask(loginUser, password, parentTaskId, addSignUsers);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToFirstUserTask");
            result.setLocalizedMessage("回退到第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 查询某一用户的待办任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> taskQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<TaskInstance>> result = null;
        try {
            String taskInstanceJson = humanService.taskQuery(loginUser, password, processDefinitionKey, conditions);
            taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            
            //解决ERP用户名大写的问题
            if (!loginUser.toLowerCase().equals(loginUser)) {
            	taskInstanceJson = humanService.taskQuery(loginUser.toLowerCase(), password, processDefinitionKey, conditions);            	
                taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
                List<TaskInstance> taskInstanceListTemp = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
                });                
                
                if(taskInstanceListTemp != null){
                    taskInstanceList.addAll(taskInstanceListTemp);
                }
            }
            
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("taskQuery");
            result.setLocalizedMessage("获取待办任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    
    /**
     * 查询某一用户的待办任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return  total和pafresult
     */
    public Map<String, Object> taskQueryWithTotal(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
    	Map<String, Object> mp = new HashMap<String, Object>();
        PafResult<List<TaskInstance>> result = null;
        try {
            String taskInstanceJson = humanService.taskQuery(loginUser, password, processDefinitionKey, conditions);       
            //获取total
            PafResult tempResult = JsonUtils.fromJsonByGoogle(taskInstanceJson, PafResult.class);
            int total = tempResult.getTotal();            
            taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            
            //解决ERP用户名大写的问题
            if (!loginUser.toLowerCase().equals(loginUser)) {
            	taskInstanceJson = humanService.taskQuery(loginUser.toLowerCase(), password, processDefinitionKey, conditions);
            	//获取total
                tempResult = JsonUtils.fromJsonByGoogle(taskInstanceJson, PafResult.class);
                int totalTemp = tempResult.getTotal(); 
                taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
                List<TaskInstance> taskInstanceListTemp = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
                });                
                
                if(taskInstanceListTemp != null){
                	total = total + totalTemp;
                    taskInstanceList.addAll(taskInstanceListTemp);
                }
            }
            
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
            mp.put("total", total);
            mp.put("pafResult", result);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("taskQuery");
            result.setLocalizedMessage("获取待办任务失败");
            result.setErrorStack(e.getMessage());
        }
        return mp;
    }
    
    /**
     * 历史任务查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> historyTaskQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<TaskInstance>> result = null;
        try {
            String taskInstanceJson = humanService.historyTaskQuery(loginUser, password, processDefinitionKey, conditions);
            taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("historyTaskQuery");
            result.setLocalizedMessage("获取历史任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 流程实例查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 流程实例列表
     */
    public PafResult<List<ProcessInstance>> processInstQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<ProcessInstance>> result = null;
        try {
            String processInstanceJson = humanService.processInstQuery(loginUser, password, processDefinitionKey, conditions);
            processInstanceJson = StringUtils.substringBetween(processInstanceJson, "{\"data\":", ",\"total\"");
            List<ProcessInstance> processInstanceList = JsonUtils.fromJsonByGoogle(processInstanceJson, new TypeToken<List<ProcessInstance>>() {
            });
            if(processInstanceList == null){
                processInstanceList = new ArrayList<ProcessInstance>();
            }
            result = new PafResult<List<ProcessInstance>>(true, processInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<ProcessInstance>>(false);
            result.setErrorCode("processInstQuery");
            result.setLocalizedMessage("流程实例查询失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 记录审批意见
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId  审批意见所属任务ID
     * @param processInstId  任务所属流程实例ID
     * @param message  审批意见的内容，业务中审批通过与否可以记录在这里
     */
    public PafResult<Boolean> addTaskComment(String loginUser, String taskId, String processInstId, String message){
        PafResult<Boolean> result = null;
        try {
            humanService.addTaskComment(loginUser, password, taskId, processInstId, message);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("addTaskComment");
            result.setLocalizedMessage("记录审批意见失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 重新分配任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @param origAssignee 当前的任务分配用户
     * @param newAssignee 新的任务分配用户，必须为小写
     * @param assigneeType 用户类型，目前只支持“user”
     * @return 重新分配结果
     */
    public PafResult<Boolean> reassignTask(String loginUser, String taskId, String origAssignee, String newAssignee, String assigneeType){
        PafResult<Boolean> result = null;
        try {
            humanService.reassignTask(loginUser.trim(), password, taskId.trim(), origAssignee.trim(), newAssignee.trim(), assigneeType);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("reassignTask");
            result.setLocalizedMessage("重新分配任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 流程实例详细信息查询
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstId 流程实例ID
     * @return 流程实例详细信息
     */
    public PafResult<ProcessInstanceDomain> getProcessInstanceDetail(String loginUser, String processInstId){
        PafResult<ProcessInstanceDomain> result = null;
        try {
            String processInstanceJson = humanService.getProcessInstanceDetail(loginUser, password, processInstId);
            ProcessInstanceDomain processInstance = JsonUtils.fromJsonByGoogle(processInstanceJson, new TypeToken<ProcessInstanceDomain>() {
            });
            if(processInstance == null){
                processInstance = new ProcessInstanceDomain();
            }
            result = new PafResult<ProcessInstanceDomain>(true, processInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<ProcessInstanceDomain>(false);
            result.setErrorCode("getProcessInstanceDetail");
            result.setLocalizedMessage("流程实例详细信息查询");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 取消流程实例
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return 是否成功
     */
    public PafResult<Boolean> cancelProcessInstance(String loginUser, String processInstanceId){
        PafResult<Boolean> result = null;
        try {
            humanService.cancelProcessInstance(loginUser, password, processInstanceId);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<Boolean>(false);
            result.setErrorCode("cancelProcessInstance");
            result.setLocalizedMessage("取消流程实例失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 获取任务详细信息(暂未开放)
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @return 任务详细信息
     */
    public PafResult<TaskInstance> getTaskDetail(String loginUser, String taskId){
        PafResult<TaskInstance> result = null;
        try {
            String taskInstanceJson = humanService.getTaskDetail(loginUser, password, taskId);
            TaskInstance taskInstance = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<TaskInstance>() {
            });
            if(taskInstance == null){
                taskInstance = new TaskInstance();
            }
            result = new PafResult<TaskInstance>(true, taskInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<TaskInstance>(false);
            result.setErrorCode("getTaskDetail");
            result.setLocalizedMessage("任务详细信息");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 获取流程实例图片
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return
     */
    public PafResult<byte[]> processInstanceFlowPictureQuery(String loginUser, String processInstanceId){
        PafResult<byte[]> result = null;
        try {
            byte[] pictureBytes = humanService.processInstanceFlowPictureQuery(processInstanceId, loginUser, password);
            result = new PafResult<byte[]>(true,pictureBytes);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<byte[]>(false);
            result.setErrorCode("processInstanceFlowPictureQuery");
            result.setLocalizedMessage("获取流程实例图片失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }


    /***************************************************************************************************
     *  OA专用
     ***************************************************************************************************/

    /**
     * 查询当前租户下所有的待办任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param conditions 流程变量集合
     * @return 待办任务列表
     */
    public PafResult<List<TaskInstance>> taskQueryAdvanced(String loginUser,  Map<String, Object> conditions){
        PafResult<List<TaskInstance>> result = null;
        try {
//            ObjectMapper mapper = new ObjectMapper();
            String taskInstanceJson = humanService.taskQueryAdvanced(loginUser, password, tenant, conditions);
            taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            
            //解决ERP用户名大写的问题
            if (!loginUser.toLowerCase().equals(loginUser)) {
            	conditions.put("TASK_PARTICIPATOR",loginUser.toLowerCase());
            	taskInstanceJson = humanService.taskQueryAdvanced(loginUser, password, tenant, conditions);
                taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
                List<TaskInstance> taskInstanceListTemp = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
                });
                if(taskInstanceListTemp != null){
                    taskInstanceList.addAll(taskInstanceListTemp);
                }
            }
            
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("taskQueryAdvanced");
            result.setLocalizedMessage("查询当前租户下所有的待办任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    
    
    /**
     * 查询当前租户下所有的待办任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param conditions 流程变量集合
     * @return total和pafresult
     */
    public Map<String,Object> taskQueryAdvancedWithTotal(String loginUser,  Map<String, Object> conditions){
    	Map<String,Object> mp = new HashMap<String, Object>();
        PafResult<List<TaskInstance>> result = null;
        try {
//            ObjectMapper mapper = new ObjectMapper();
            LOG.info("PafSafServiceImpl.taskQueryAdvancedWithTotal:1:loginUser:"+loginUser);
            String taskInstanceJson = humanService.taskQueryAdvanced(loginUser, password, tenant, conditions);            
            //获取total
            PafResult tempResult = JsonUtils.fromJsonByGoogle(taskInstanceJson, PafResult.class);
            int total = tempResult.getTotal();            
            taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            
            //解决ERP用户名大写的问题
            if (!loginUser.toLowerCase().equals(loginUser)) {
            	conditions.put("TASK_PARTICIPATOR",loginUser.toLowerCase());
                LOG.info("PafSafServiceImpl.taskQueryAdvancedWithTotal:2:loginUser.toLowerCase():"+loginUser.toLowerCase());
            	taskInstanceJson = humanService.taskQueryAdvanced(loginUser, password, tenant, conditions);
            	//获取total
                tempResult = JsonUtils.fromJsonByGoogle(taskInstanceJson, PafResult.class);
                int totalTemp = tempResult.getTotal(); 
                taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
                List<TaskInstance> taskInstanceListTemp = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
                });                
                
                if(taskInstanceListTemp != null){
                	total = total + totalTemp;
                    taskInstanceList.addAll(taskInstanceListTemp);
                }
            }
            
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
                        
            mp.put("total", total);
            mp.put("pafResult", result);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("taskQueryAdvanced");
            result.setLocalizedMessage("查询当前租户下所有的待办任务失败");
            result.setErrorStack(e.getMessage());
        }
        return mp;
    }

    
    public PafResult<List<TaskInstance>> getProcessCurrentUserTasks(String loginUser, List<String> processInstances){
//    	Map<String, Object> mp = new HashMap<String, Object>();
        PafResult<List<TaskInstance>> result = null;
        try {
            String taskInstanceJson = humanService.getProcessCurrentUserTasks(loginUser, password, processInstances);
                        
            //taskInstanceJson = StringUtils.substringBetween(taskInstanceJson, "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            //过滤子任务,只保留主任务
            List<TaskInstance> tiList = new ArrayList<TaskInstance>();
            for (TaskInstance ti : taskInstanceList){
            	if (StringUtils.isNotEmpty(ti.getParentTaskId()))
            		continue;
            	else
            		tiList.add(ti);
            }
            result = new PafResult<List<TaskInstance>>(true, tiList);
        } catch (Exception e) {
            LOG.error(e.getMessage(),e);
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("getProcessCurrentUserTasks");
            result.setLocalizedMessage("获取待办任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }


    public void setHumanService(HumanService humanService) {
        this.humanService = humanService;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
   
	
}

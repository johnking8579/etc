package com.jd.oa.common.dao.sqladpter;

/**
 * sql适配器接口
 * 
 * @author zhouhuaqi
 * 
 */
public interface SqlAdapter {

	public enum DatabaseType {
		MYSQL, ORACLE
	}

	/**
	 * sql操作类型
	 * 
	 */
	public enum SqlType {
		DDL_CREATE_TABLE("CREATE TABLE"), DDL_DROP_TABLE("DROP TABLE"), DDL_ADD_COLUMN(
				"ADD COLUMN"), DDL_DROP_COLUMN("DROP COLUMN"), DDL_CHANGE_COLUMN(
				"CHANGE COLUMN"), DML_SELECT("SELECT"), DML_INSERT("INSERT"), DML_UPDATE(
				"UPDATE"), DML_DELETE("DELETE");
		private String value;

		private SqlType(String value) {
			this.value = value;
		}

		public String toString() {
			return this.value;
		}
	}

	/**
	 * 数据库字段数据类型
	 * 
	 */
	public enum DataType {
		STRING, NUMBER, DATE, TIME, DATETIME
	}

	/**
	 * 逻辑运算
	 * 
	 */
	public enum LogicalOperator {
		AND("AND"), OR("OR");
		private String value;

		private LogicalOperator(String value) {
			this.value = value;
		}

		public String toString() {
			return new String(this.value);
		}
	}

	/**
	 * 关系运算
	 * 
	 * <p>
	 * EQ("="), NE("<>"), GT(">"), GE(">="), LT("<"), LE("<=")
	 * </p>
	 * 
	 */
	public enum RelationalOperator {
		EQ("="), NE("<>"), GT(">"), GE(">="), LT("<"), LE("<=");
		private String value;

		private RelationalOperator(String value) {
			this.value = value;
		}

		public String toString() {
			return this.value;
		}
	}

	/**
	 * 排序方式
	 * 
	 */
	public enum SortMethod {
		ASC("ASC"), DESC("DESC");
		private String value;

		private SortMethod(String value) {
			this.value = value;
		}

		public String toString() {
			return this.value;
		}
	}

	/**
	 * 添加待查询的表或视图名称
	 * 
	 * @param tableName
	 * @return
	 */
	public SqlAdapter addTable(String tableName);

	/**
	 * 添加待查询的表或视图名称,并指定别名
	 * 
	 * @param tableName
	 * @param aliasName
	 * @return
	 */
	public SqlAdapter addTable(String tableName, String aliasName);

	/**
	 * dll操作需要添加的列信息
	 * 
	 * @param oldClumnName
	 *            老的列名
	 * @param columnName
	 *            列名
	 * @param dataType
	 *            数据类型
	 * @param dataLength
	 *            数据长度
	 * @param isPK
	 *            是否主键
	 * @param isNull
	 *            是否为NULL
	 * @param defaultValue
	 *            默认值
	 * @param comment
	 *            注释
	 * @return
	 */
	public SqlAdapter addColumn(String oldColumnName, String columnName,
			DataType dataType, String dataLength, boolean isPK, boolean isNull,
			String defaultValue, String comment);

	/**
	 * 添加待查列
	 * 
	 * @param columnName
	 * @return
	 */
	public SqlAdapter addColumn(String columnName);

	/**
	 * 添加待查列,并指定别名
	 * 
	 * @param columnName
	 * @param aliasName
	 * @return
	 */
	public SqlAdapter addColumn(String columnName, String aliasName);

	/**
	 * 添加更新列
	 * 
	 * @param columnName
	 * @param columnValue
	 * @param dataType
	 * @return
	 */
	public SqlAdapter addColumn(String columnName, String columnValue,
			DataType dataType);

	/**
	 * 添加分组字段
	 * 
	 * @param groupField
	 * @return
	 */
	public SqlAdapter addGroupBy(String groupField);

	/**
	 * 添加 AND查询条件
	 * 
	 * @param columnName
	 * @param relationalOperator
	 * @param columnValue
	 * @param dataType
	 * @return
	 */
	public SqlAdapter addAndCondition(String columnName,
			RelationalOperator relationalOperator, String columnValue,
			DataType dataType);

	/**
	 * 添加 OR查询条件
	 * 
	 * @param columnName
	 * @param relationalOperator
	 * @param columnValue
	 * @param dataType
	 * @return
	 */
	public SqlAdapter addOrCondition(String columnName,
			RelationalOperator relationalOperator, String columnValue,
			DataType dataType);

	/**
	 * 添加 AND LIKE 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addAndLikeCondition(String columnName, String columnValue);

	/**
	 * 添加 OR LIKE 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addOrLikeCondition(String columnName, String columnValue);

	/**
	 * 添加 AND IN 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addAndInCondition(String columnName, String columnValue);

	/**
	 * 添加 OR IN 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addOrInCondition(String columnName, String columnValue);

	/**
	 * 添加 AND NOT IN 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addAndNotInCondition(String columnName, String columnValue);

	/**
	 * 添加 OR NOT IN 查询条件
	 * 
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public SqlAdapter addOrNotInCondition(String columnName, String columnValue);

	/**
	 * 添加正向排序字段
	 * 
	 * @param orderField
	 * @return
	 */
	public SqlAdapter addAscOrderBy(String orderField);

	/**
	 * 添加逆向排序字段
	 * 
	 * @param orderField
	 * @return
	 */
	public SqlAdapter addDescOrderBy(String orderField);

	/**
	 * 获取sql
	 * 
	 * @return
	 */
	public String getSql();
	
	/**
	 * 设置sql
	 * 
	 * @param sql
	 * @return
	 */
	public SqlAdapter setSql(String sql);

	/**
	 * 清除待查表和视图
	 * 
	 * @return
	 */
	public SqlAdapter clearTable();

	/**
	 * 清除待查列
	 * 
	 * @return
	 */
	public SqlAdapter clearColumn();

	/**
	 * 清除查询条件
	 * 
	 * @return
	 */
	public SqlAdapter clearCondition();

	/**
	 * 清除排序条件
	 * 
	 * @return
	 */
	public SqlAdapter clearOrder();

	/**
	 * 清除分组列
	 * 
	 * @return
	 */
	public SqlAdapter clearGroup();

	/**
	 * 追加字符串,在后面添加一个空格
	 * 
	 * @param string
	 * @return
	 */
	public SqlAdapter append(String string);

	/**
	 * 删除最后两位字符串
	 * 
	 * @return
	 */
	public SqlAdapter clearEndChar();

	/**
	 * 设置sql操作类型
	 * 
	 * @return
	 */
	public SqlAdapter setSqlType(SqlType sqlType);

	/**
	 * 获得sql操作类型
	 * 
	 * @return
	 */
	public SqlType getSqlType();

	/**
	 * 生成sql语句
	 * 
	 * @return
	 */
	public String toSql();
}

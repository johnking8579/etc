package com.jd.oa.common.utils;

/**
 * 
 * @author zhengbing
 * @desc  生成唯一的16位数字字符串
 * @date 2014年7月3日 下午4:41:47
 */
public class UniqueStringGenerator {
	private UniqueStringGenerator() {
	}

	/**
	 * 
	 * @author zhengbing 
	 * @description 生成唯一的16位数字字符串
	 * @date 2014年7月3日下午4:42:08
	 * @return String
	 */
	public static synchronized String getUniqueString(){

		if (START_GENERATE_COUNT > MAX_GENERATE_COUNT)

			START_GENERATE_COUNT = 101;

		String uniqueNumber = Long.toString(System.currentTimeMillis())
				+ Integer.toString(START_GENERATE_COUNT);

		START_GENERATE_COUNT++;

		return uniqueNumber;

	}

	private static final int MAX_GENERATE_COUNT = 999;

	private static int START_GENERATE_COUNT = 101;
	

	
}

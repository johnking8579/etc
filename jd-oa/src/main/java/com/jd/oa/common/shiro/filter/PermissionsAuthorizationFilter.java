package com.jd.oa.common.shiro.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

public class PermissionsAuthorizationFilter extends AuthorizationFilter {

    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {
    	HttpServletRequest httpServletRequest = (HttpServletRequest) request;
       
    	Subject subject = getSubject(request, response);
        
        String url = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
        
        boolean isPermitted = true;
        
        if(url.indexOf("!")>0){
        	url = url.substring(0,url.indexOf("!"));
        }
        else if(url.indexOf("!")<0 && url.indexOf(".")>0){
        	url = url.substring(0,url.indexOf("."));
        }
        if (!subject.isPermitted(url)){
        	isPermitted = false;
        }
        return isPermitted;
    }
}
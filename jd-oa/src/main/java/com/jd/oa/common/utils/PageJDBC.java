package com.jd.oa.common.utils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PageJDBC implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8750058890634141118L;
	private int pageNo;
	private int pageSize;
	private int total;
	private List<Map<String,Object>> rows;
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<Map<String, Object>> getRows() {
		return rows;
	}
	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}
	
	
}	

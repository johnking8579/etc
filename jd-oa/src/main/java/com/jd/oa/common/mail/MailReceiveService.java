package com.jd.oa.common.mail;

import java.util.Date;
import java.util.List;

import javax.mail.Message;
import javax.mail.Part;
import javax.mail.search.SearchTerm;

/**
 * 邮件接收服务
 * 
 * @author zhouhuaqi
 *
 */
public interface MailReceiveService {
	
	public enum MailFlag{
		ALL,NEW,UNREAD,SEEN,DELETED
	}

	/**
	 * 获取所有邮件
	 * @return
	 */
	public List<Message> getAllMails();
	
	/**
	 * 获取新邮件
	 * @return
	 */
	public List<Message> getNewMails();
	
	/**
	 * 获取未读邮件
	 * @return
	 */
	public List<Message> getUnreadMails();

	/**
	 * 搜索邮件
	 * @param searchTerm 搜索参数
	 * @return
	 */
	public List<Message> searchMail(SearchTerm searchTerm);

	/**
	 * 设置邮件标示
	 * @param messageId 邮件ID
	 * @param flag 
	 * @return
	 */
	public boolean setMailFlag(Message message, MailFlag flag);
	
	/**
	 * 获得收件箱中邮件的数量
	 * @return
	 */
	public int getMessageCount();
	
	/**
	 * 获得收件箱中新邮件的数量
	 * @return
	 */
	public int getNewMessageCount();
	
	/**
	 * 获得收件箱中未读邮件的数量
	 * @return
	 */
	public int getUnreadMessageCount();
	
	/**
	 * 获得已删除邮件的数量
	 * @return
	 */
	public int getDeletedMessageCount();
	
	/**
	 * 获取收件人地址
	 * @param message
	 * @return
	 */
	public String getTOAddress(Message message);
	
	/**
	 * 获取抄送人地址
	 * @param message
	 * @return
	 */
	public String getCCAddress(Message message);
	
	/**
	 * 获取密送人地址
	 * @param message
	 * @return
	 */
	public String getBCCAddress(Message message);
	
	/**
	 * 获取发件人的邮箱地址
	 * @param message
	 * @return
	 */
	public String getFrom(Message message);
	
	/**
	 * 获取邮件主题
	 * @param message
	 * @return
	 */
	public String getSubject(Message message);
	
	/**
	 * 获取邮件发送时间
	 * @param message
	 * @return
	 */
	public Date getSentDate(Message message);
	
	/**
	 * 获取邮件是否需要回复
	 * @param message
	 * @return
	 */
	public boolean getReplySign(Message message);
	
	/**
	 * 邮件是否包含附件
	 * @param part
	 * @return
	 */
	public boolean isContainAttach(Part part);
	
	/**
	 * 获取邮件body内容
	 * @param message
	 * @return
	 */
	public String getContent(Message message);
	
}

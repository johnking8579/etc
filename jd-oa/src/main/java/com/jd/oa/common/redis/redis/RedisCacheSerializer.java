package com.jd.oa.common.redis.redis;

import org.springframework.util.Assert;

import com.jd.oa.common.exception.BusinessException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;


public class RedisCacheSerializer {
    private final Charset charset;

    public RedisCacheSerializer() {
        this(Charset.forName("UTF8"));
    }

    public RedisCacheSerializer(Charset charset) {
        Assert.notNull(charset);
        this.charset = charset;
    }

    public static  byte[] serialize(Object obj) throws IOException{
    	    ByteArrayOutputStream bos = new ByteArrayOutputStream();  
    	    ObjectOutputStream oos = new ObjectOutputStream(bos);  
    	    oos.writeObject(obj);  
    	    byte[] bytes = bos.toByteArray();  
    	    bos.close();  
    	    oos.close();
			return bytes;  
     }
    
     public static Object deSerialize(byte[] buf) throws IOException, ClassNotFoundException{
    	 ByteArrayInputStream bos = null;
    	 Object obj = null;
    	 try{
    		 bos =  new ByteArrayInputStream(buf);
    		 ObjectInputStream ios = new ObjectInputStream(bos);
        	 obj = ios.readObject();
    	 } catch(IOException e){
    		 throw new BusinessException("缓存文件保存失败！",e);
    	 } finally{
    		 try{
    			 if(bos!=null){
    				 bos.close();
    			 }
    		 }catch(IOException e){
    		 }
    	 }
    	 return obj;
     }


    public  String deserialize(byte[] bytes) {
        return (bytes == null ? null : new String(bytes, charset));
    }


    public byte[] serialize1(String string) {
        return (string == null ? null : string.getBytes(charset));
    }
}

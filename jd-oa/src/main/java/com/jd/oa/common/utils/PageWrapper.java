package com.jd.oa.common.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jd.oa.common.dao.Page;

public class PageWrapper<T>{
	private Page<T> pageBean;  //分页组件
	private HttpServletRequest request;
	Map<String,Object> result;  //返回到页面的结果
	private String sEcho;  //datatables 自己标记的东西，暂时无用
	private int sStart; //每页记录的开始位置
	private int sLength; //每页记录的数目长度
	private int isSortingCols; //要排序的字段数目 ,默认为1，暂不支持组合排序
	private Map<String,Object> searchMap=new HashMap<String, Object>(); //搜索map
	
	private String orderBy="";
	private String order="";
	
	private String[] columns=null;
	
	public PageWrapper(HttpServletRequest request) {
		if(null==request) return;
		this.request=request;
		this.result=new HashMap<String, Object>();
		pageBean=new Page<T>();
		
		this.sEcho=request.getParameter("sEcho"); 
		this.sStart = Integer.parseInt(request.getParameter("iDisplayStart")); 
	    this.sLength = Integer.parseInt(request.getParameter("iDisplayLength")); 
	    //得到页面展示的字段
	    int colsNum=Integer.parseInt(request.getParameter("iColumns"));
	    columns=new String[colsNum];
	    for(int i=0;i<colsNum;i++){
	    	columns[i]=request.getParameter("mDataProp_"+i);
	    }
	    
	    //要排序的字段
	    this.isSortingCols=Integer.parseInt(request.getParameter("iSortingCols")); 
	    
	    for(int i=0;i<isSortingCols;i++){
	    	int a=Integer.parseInt(request.getParameter("iSortCol_"+i));
	    	String aa=request.getParameter("sSortDir_"+i);
	    	setOrder(columns[a],aa);
	    }
	    if(sLength==-1){//长度为-1，不支持分页
		    pageBean.setPageNo(1);
		    pageBean.setPageSize(100);
	    }else{
		    //计算下一个页码
		    int page=(int) Math.floor(sStart/sLength)+1;
		    pageBean.setPageNo(page);
		    pageBean.setPageSize(this.sLength);
	    }

	}

	public Page<T> getPageBean() {
		return pageBean;
	}

	public void setPageBean(Page<T> pageBean) {
		this.pageBean = pageBean;
	}

	private void setOrder(String key,String value){
		orderBy+=","+key.trim();
		order+=","+value.trim();
		if(orderBy.startsWith(",")){
			orderBy=orderBy.substring(1);
		}
		if(order.startsWith(",")){
			order=order.substring(1);
		}
	    pageBean.setOrderBy(orderBy);
	    pageBean.setOrder(order);
	}
	public Map<String, Object> getResult() {
		result.put("sEcho",sEcho);//原值返回
		result.put("iTotalRecords",pageBean.getTotalCount()==-1?0:pageBean.getTotalCount()); //过滤前总记录数
		result.put("iTotalDisplayRecords",pageBean.getTotalCount()==-1?0:pageBean.getTotalCount());//过滤后总记录数
		result.put("current_page",pageBean.getPageNo());
		result.put("aaData", pageBean.getResult()); //返回的列表数据
		return result;
	}


	public Map<String, Object> getResultMap() {
		result.put("sEcho",sEcho);//原值返回
		result.put("iTotalRecords",pageBean.getTotalCount()==-1?0:pageBean.getTotalCount()); //过滤前总记录数
		result.put("iTotalDisplayRecords",pageBean.getTotalCount()==-1?0:pageBean.getTotalCount());//过滤后总记录数
		result.put("current_page",pageBean.getPageNo());
		return result;
	}
	
	public String getsEcho() {
		return sEcho;
	}

	public int getsStart() {
		return sStart;
	}

	public int getsLength() {
		return sLength;
	}

	public int getIsSortingCols() {
		return isSortingCols;
	}


	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String[] getColumns() {
		return columns;
	}
	//返回 搜索和排序条件组合
	public Map<String,Object>  getConditionsMap(){
		return searchMap;
	}
	//添加搜索条件
	public void addSearch(String key,Object value){
		this.searchMap.put(key, value);
	}
	//添加排序条件
	public void addSort(String key,String value){
		setOrder(key,value);
	}
	//添加一些额外的数据到页面
	public void addResult(String key,Object value){
		this.result.put(key, value);
	}
}

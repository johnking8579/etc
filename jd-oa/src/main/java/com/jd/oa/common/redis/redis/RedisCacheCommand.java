package com.jd.oa.common.redis.redis;

import java.util.Set;

/**
 * @author: dbcuizhen
 * @time: 13-8-15  下午2:43
 * @version: 1.0
 * @since: 1.0
 */
public interface RedisCacheCommand {
    void close();

    void evict(Object key);

    void flushAll();

    Object get(Object key);

    Set<String> keys(String pattern);

    void put(Object key, Object value, int seconds);

    void put(Object key, Object value);

    boolean isKeyInCache(Object key);

    int getTimeToIdleSeconds(Object key);
    int getTimeToLiveSeconds(Object key);

}

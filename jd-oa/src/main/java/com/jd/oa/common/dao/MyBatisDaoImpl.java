package com.jd.oa.common.dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.utils.ComUtils;
import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.common.utils.ReflectionUtils;
import com.jd.oa.common.utils.SpringContextUtils;

/**
 * 封装mybatis原生API的DAO泛型基类.
 * 
 * @param <T>
 *            DAO操作的对象类型
 * @param <PK>
 *            主键类型
 * 
 * @author zhouhq
 */
@SuppressWarnings("unchecked")
public class MyBatisDaoImpl<T, PK extends Serializable> extends
		SqlSessionDaoSupport implements BaseDao<T, PK> {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	protected SqlSessionFactory sessionFactory;

	/**
	 * Entity的类型
	 */
	protected Class<T> entityClass;

	/**
	 * Entity的主键类型
	 */
	protected Class<PK> pkClass;

	public String sqlMapNamespace = null;

	public static final String POSTFIX_INSERT = "insert";

	public static final String POSTFIX_UPDATE = "update";

	public static final String POSTFIX_DELETE_BY_ID = "deleteById";

	public static final String POSTFIX_DELETE = "delete";

	public static final String POSTFIX_GET = "get";

	public static final String POSTFIX_SELECT = "find";

	public static final String POSTFIX_SELECT_COUNT = "findCount";

	public static final String POSTFIX_SELECTPAGE = "findByPage";

	public static final String POSTFIX_SELECTPAGE_COUNT = "findByPageCount";

	/**
	 * 用于Dao层子类使用的构造函数. 通过子类的泛型定义取得对象类型Class. eg. public class UserDao extends
	 * MyBatisDaoImpl<User, Long>
	 */
	public MyBatisDaoImpl() {
		this.entityClass = ReflectionUtils.getClassGenricType(getClass());
		this.pkClass = ReflectionUtils.getClassGenricType(getClass(), 1);
		this.sqlMapNamespace = entityClass.getName();
	}

	@Resource(name = "sqlSessionFactory")
	public void setSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}

	public String getSqlMapNamespace() {
		return sqlMapNamespace;
	}

	public PK insert(T entity) {
		try {
			if (ReflectionUtils.getFieldValue(entity, "id") == null) {
				ReflectionUtils.setFieldValue(entity, "id", IdUtils.uuid2());
			}
			Method setCreatorMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setCreator");
			if (setCreatorMethod != null)
				setCreatorMethod.invoke(entity, ComUtils.getLoginName());
			Method seCreateTimeMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setCreateTime");
			if (seCreateTimeMethod != null)
				seCreateTimeMethod.invoke(entity, new Date());
			Method setModifierMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setModifier");
			if (setModifierMethod != null)
				setModifierMethod.invoke(entity, ComUtils.getLoginName());
			Method seModifyTimeMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setModifyTime");
			if (seModifyTimeMethod != null)
				seModifyTimeMethod.invoke(entity, new Date());

			getSqlSession().insert(sqlMapNamespace + "." + POSTFIX_INSERT,
					entity);
			return pkClass.getConstructor(String.class)
					.newInstance(
							String.valueOf(ReflectionUtils.getFieldValue(
									entity, "id")));
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			logger.error(e.getMessage(), e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	public List<PK> insert(List<T> entitys) {
		List<PK> pkList = new ArrayList<PK>();
		for (T e : entitys)
			pkList.add(null == e ? null : insert(e));
		return pkList;
	}

	public Object insert(final String ql, final Object... values) {
		int num = 0;
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				num += getSqlSession().insert(sqlMapNamespace + "." + ql,
						values[i]);
			}
		} else {
			num += getSqlSession().insert(sqlMapNamespace + "." + ql);
		}
		return num;
	}

	public int delete(PK id) {
		return delete(get(id), false);

	}
	
	public int delete(PK id, boolean isPhysicalDelete) {
		return delete(get(id), isPhysicalDelete);
	}

	public int delete(T entity, boolean... isPhysicalDelete) {
		try {
			if(isPhysicalDelete.length == 1 && isPhysicalDelete[0])
				return getSqlSession().delete(sqlMapNamespace + "." + POSTFIX_DELETE, entity);
			else{
				Method setYnMethod = ReflectionUtils.getAccessibleMethodByName(
						entity, "setYn");
				if (setYnMethod != null)
					setYnMethod.invoke(entity, 1);
	
				return update(entity);
			}
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			logger.error(e.getMessage(), e);
		}
		return 0;
		// return getSqlSession().delete(sqlMapNamespace + "." + POSTFIX_DELETE,
		// entity);
	}

	public int delete(List<T> entitys, boolean... isPhysicalDelete) {
		int rowsEffected = 0;
		for (T e : entitys)
			rowsEffected += null == e ? 0 : delete(e, isPhysicalDelete);
		return rowsEffected;
	}

	public int delete(final String ql, final Object... values) {
		int num = 0;
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				num += getSqlSession().delete(sqlMapNamespace + "." + ql,
						values[i]);
			}
		} else {
			num += getSqlSession().delete(sqlMapNamespace + "." + ql);
		}
		return num;
	}

	public int update(T entity) {
		try {
			Method setModifierMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setModifier");
			if (setModifierMethod != null)
				setModifierMethod.invoke(entity, ComUtils.getLoginName());
			Method seModifyTimeMethod = ReflectionUtils
					.getAccessibleMethodByName(entity, "setModifyTime");
			if (seModifyTimeMethod != null)
				seModifyTimeMethod.invoke(entity, new Date());

			getSqlSession().update(sqlMapNamespace + "." + POSTFIX_UPDATE,
					entity);
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			logger.error(e.getMessage(), e);
		}
		return 1;
	}

	public int update(List<T> entity) {
		int rowsEffected = 0;
		for (T e : entity)
			rowsEffected += null == e ? 0 : update(e);
		return rowsEffected;
	}

	public int update(final String ql, final Object... values) {
		int num = 0;
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				num += getSqlSession().update(sqlMapNamespace + "." + ql,
						values[i]);
			}
		} else {
			num += getSqlSession().update(sqlMapNamespace + "." + ql);
		}
		return num;
	}

	public T get(final PK id) {
		return (T) getSqlSession().selectOne(
				sqlMapNamespace + "." + POSTFIX_GET, id);
	}

	public Object get(final String ql, final Object... values) {
		if (values != null) {
			Object result = null;
			for (int i = 0; i < values.length; i++) {
				result = getSqlSession().selectOne(sqlMapNamespace + "." + ql,
						values[i]);
			}
			return result;
		} else {
			return getSqlSession().selectOne(sqlMapNamespace + "." + ql);
		}
	}

	public List<T> find(final T entity) {
		return getSqlSession().selectList(
				sqlMapNamespace + "." + POSTFIX_SELECT, entity);
	}

	public final long findCount(final T entity) {
		return getSqlSession().selectList(
				sqlMapNamespace + "." + POSTFIX_SELECT, entity).size();
	}

	public List<T> find(final String ql, final Object... values) {
		if (values != null) {
			List<T> result = null;
			for (int i = 0; i < values.length; i++) {
				result = getSqlSession().selectList(sqlMapNamespace + "." + ql,
						values[i]);
			}
			return result;
		} else {
			return getSqlSession().selectList(sqlMapNamespace + "." + ql);
		}
	}

	// mybtis执行复杂sql语句，并传入多个参数
	public List<T> find(final String ql, final Map<String, Object> map) {
		return getSqlSession().selectList(sqlMapNamespace + "." + ql, map);
	}

	public long findCount(final String ql, final Object... values) {
		Long result = null;
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				result = (Long) getSqlSession().selectOne(
						sqlMapNamespace + "." + ql, values[i]);
			}
		} else {
			result = (Long) getSqlSession().selectOne(
					sqlMapNamespace + "." + ql);
		}
		return result != null ? result.longValue() : 0;
	}

	public Page<T> find(final Page<T> page, final T entity) {
		RowBounds rowBounds = new RowBounds((page.getPageNo() - 1)
				* page.getPageSize(), page.getPageSize());
		page.setResult((List<T>) getSqlSession().selectList(
				sqlMapNamespace + "." + POSTFIX_SELECTPAGE, entity, rowBounds));
		page.setTotalCount(findCount(POSTFIX_SELECTPAGE_COUNT, entity));
		return page;
	}

	public Page<T> find(final Page<T> page, final String ql, final Object value) {
		RowBounds rowBounds = new RowBounds((page.getPageNo() - 1)
				* page.getPageSize(), page.getPageSize());
		if (value instanceof Map) {
			if (page.isOrderBySetted()) {
				LinkedHashMap<String, String> sorts = new LinkedHashMap<String, String>();
				for (int i = 0; i < page.getOrder().split(",").length; i++) {
					sorts.put(ComUtils.getDBColumnNameFromJavaName(page
							.getOrderBy().split(",")[i]), page.getOrder()
							.split(",")[i]);
				}
				((Map) value).put("_sorts", sorts);
			}
		}
		page.setResult((List<T>) getSqlSession().selectList(
				sqlMapNamespace + "." + (ql == null ? "findByMap" : ql), value,
				rowBounds));
		page.setTotalCount(findCount((ql == null ? "findByMap" : ql) + "Count",
				value));
		return page;
	}
/**
 * 用sql语句直接查询分页
 * yujiahe
 */
	public Page<T> find(final Page<T> page, final String sql, final Object value,final int operateType) {
		
		
		//排序相关
		StringBuffer sortSql = new StringBuffer();
		if (value instanceof Map) {
			if (page.isOrderBySetted()) {
				sortSql.append(" order by ");
				String[]orderBys = page.getOrder().split(",");
				if(orderBys.length > 0){
					for (int i = 0; i < page.getOrder().split(",").length; i++) {
						sortSql.append(" "+page.getOrderBy().split(",")[i]+" ");
						sortSql.append(" "+page.getOrder().split(",")[i]+" ");
						if(i<page.getOrder().split(",").length-1){
							sortSql.append(",");
						}
					}
				}else{
					
				}
			}
		}
		//查询范围
		StringBuffer limitSql = new StringBuffer();
		limitSql.append(" limit "+(page.getPageNo() - 1)* page.getPageSize()+","+page.getPageSize()+" ");
		
		JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringContextUtils.getBean("jdbcTemplate");
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(sql);
		sqlQuery.append(sortSql);
		sqlQuery.append(limitSql);
		List rows = jdbcTemplate.queryForList(sqlQuery.toString());
		page.setResult(rows);
		page.setTotalCount(jdbcTemplate.queryForList(sql.toString()).size());
		return page;
	}
	public Page<Object> find(final Page<Object> page, final String ql,
			final Object... values) {
		RowBounds rowBounds = new RowBounds((page.getPageNo() - 1)
				* page.getPageSize(), page.getPageSize());
		page.setResult((List<Object>) getSqlSession().selectList(
				sqlMapNamespace + "." + ql, values, rowBounds));
		page.setTotalCount(findCount(ql + "Count", values));
		return page;
	}

	public Object sql(SqlAdapter sqlAdapter) {
		switch (sqlAdapter.getSqlType()) {
		case DDL_CREATE_TABLE:
			return getSqlSession().update(sqlMapNamespace + ".ddl", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DDL_DROP_TABLE:
			return getSqlSession().update(sqlMapNamespace + ".ddl", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DDL_ADD_COLUMN:
			return getSqlSession().update(sqlMapNamespace + ".ddl", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DDL_DROP_COLUMN:
			return getSqlSession().update(sqlMapNamespace + ".ddl", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DDL_CHANGE_COLUMN:
			return getSqlSession().update(sqlMapNamespace + ".ddl", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DML_SELECT:
			return getSqlSession().selectList(sqlMapNamespace + ".dmlSelect", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DML_INSERT:	
//			String id = IdUtils.uuid2();
			String id = String.valueOf(System.nanoTime());
			sqlAdapter.addColumn("ID", id, DataType.STRING);
			sqlAdapter.addColumn("CREATOR", ComUtils.getLoginName(), DataType.STRING);	
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String fmtDate = formatter.format(new Date());		
			sqlAdapter.addColumn("CREATE_TIME", fmtDate, DataType.DATETIME);
			getSqlSession().insert(sqlMapNamespace + ".dmlInsert", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
			return id;
		case DML_UPDATE:
			sqlAdapter.addColumn("MODIFIER", ComUtils.getLoginName(), DataType.STRING);
			SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sqlAdapter.addColumn("MODIFY_TIME", formatter1.format(new Date()), DataType.DATETIME);
			return getSqlSession().update(sqlMapNamespace + ".dmlUpdate", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		case DML_DELETE:
			return getSqlSession().delete(sqlMapNamespace + ".dmlDelete", sqlAdapter.getSql()!=null?sqlAdapter.getSql():sqlAdapter.toSql());
		default:
			return null;
		}
	}
}

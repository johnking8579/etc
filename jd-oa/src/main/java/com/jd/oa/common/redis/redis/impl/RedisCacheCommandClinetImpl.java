package com.jd.oa.common.redis.redis.impl;


import com.jd.oa.common.redis.redis.RedisCacheCommand;
import com.jd.oa.common.redis.redis.RedisCacheSerializer;
import com.jd.redis.JedisOperation;
import com.jd.redis.support.RedisCommands;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class RedisCacheCommandClinetImpl implements RedisCacheCommand {

    private static final Logger logger = Logger.getLogger(RedisCacheCommandClinetImpl.class);
    private RedisCommands redisCommands;

    public RedisCacheCommandClinetImpl(RedisCommands redisCommands) {
        this.redisCommands = redisCommands;
    }

    public void close() {
        redisCommands.close();
    }

    public void evict(Object key) {
        if (key == null) {
            return;
        }
        if (!checkCacheInit()) {
            return;
        }
        try {
            final byte[] keyArray = RedisCacheSerializer.serialize(key);
            redisCommands.nativeExecute(new JedisOperation() {
                public void call(Jedis jedis) {
                    jedis.del(keyArray);
                }
            });
        } catch (Exception e) {
            logger.error("调用缓存服务器evict操作出错:", e);
        }


    }

    private boolean checkCacheInit() {
        if (redisCommands == null) {
            logger.error("缓存服务器没有初始化");
            return false;
        }
        return true;
    }

    public void flushAll() {
        redisCommands.nativeExecute(new JedisOperation() {
            public void call(Jedis jedis) {
                jedis.flushAll();
            }
        });

    }

    public Object get(Object key) {
        if (key == null) {
            return null;
        }
        if (!checkCacheInit()) {
            return null;
        }
        byte[] buf;
        Object returnValue = null;
        try {
            buf = redisCommands.get(RedisCacheSerializer.serialize(key));
            if (buf == null) {
                logger.warn("没有查找到key:[" + key + "]");
                return buf;
            }
            returnValue = RedisCacheSerializer.deSerialize(buf);
        } catch (Exception e) {
            logger.error("调用缓存服务器get操作出错:", e);
        }
        return returnValue;

    }

    public Set<String> keys(final String pattern) {
        final Set<String> keys = new HashSet<String>();
        redisCommands.nativeExecute(new JedisOperation() {
            public void call(Jedis jedis) {
                System.out.println(jedis.dbSize());
                keys.addAll(jedis.keys(pattern));
            }
        });
        return keys;
    }

    public void put(Object key, Object value, int seconds) {
        if (key == null) {
            return;
        }
        if (!checkCacheInit()) {
            return;
        }
        if (value == null) {
            return;
        }
        try {
            redisCommands.setex(RedisCacheSerializer.serialize(key), seconds,
                    RedisCacheSerializer.serialize(value));
        } catch (IOException e) {
            logger.error("调用缓存服务器put操作出错:", e);
        } catch (IllegalStateException e) {
            logger.error("put key error", e);
        }

    }

    public void put(Object key, Object value) {
        if (key == null) {
            return;
        }

        if (value == null) {
            return;
        }

        if (!checkCacheInit()) {
            return;
        }

        try {
            //默认十二小时有效期
            redisCommands.setex(RedisCacheSerializer.serialize(key),
                    43200, RedisCacheSerializer.serialize(value));
        } catch (Exception e) {
            logger.error("调用缓存服务器put操作出错:", e);
        }

    }

    public boolean isKeyInCache(Object key) {
        try {
            return redisCommands.exists(RedisCacheSerializer.serialize(key));
        } catch (IOException e) {
            //TODO:
        }
        return false;
    }

    public int getTimeToIdleSeconds(Object key) {
        try {
            return Integer.parseInt(redisCommands.objectIdletime(RedisCacheSerializer.serialize(key)) + "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.valueOf(0);
    }

    public int getTimeToLiveSeconds(Object key) {
        return 0;
    }

    public Long getTimeToIdleSeconds() {
        //TODO
        try {
            return redisCommands.objectIdletime(RedisCacheSerializer.serialize("*"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Long.valueOf(0);
    }

    public int getTimeToLiveSeconds() {
        return 0;
    }
}

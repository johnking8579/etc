package com.jd.oa.common.redis;

import com.jd.oa.common.redis.annotation.*;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;


public interface CacheInterceptor extends MethodInterceptor {
    Object doCache(Cache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;

    Object doListCache(ListedCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;

    Object doSimpleCache(SimpleCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;

    void removeCache(RemoveCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;

    Object writeCache(WriteCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;

    Object writeListCache(WriteListCache annotation, Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable;
}

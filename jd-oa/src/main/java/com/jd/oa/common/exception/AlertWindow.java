package com.jd.oa.common.exception;

import org.w3c.dom.html.HTMLLabelElement;

public class AlertWindow {
	public static String getWarningWindow(String title){
		return getWarningWindow(title,"");
	}
	
	/**
	 */
	public static String getWarningWindow(String title,String msg){
		StringBuffer html = new StringBuffer();
		html.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
		html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
		html.append("<head>\n");
		html.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
		html.append("<title>BPM Message Alert</title>\n");
		html.append("<style type=\"text/css\" media=\"all\">@import url(\"/themes/alert.css\");</style>\n");
		html.append("</head>\n");
		html.append("<body>\n");
		html.append("<div style=\"height:60px;\"></div>\n");
		html.append("<div id=\"messageBoxFrame\">\n");
		html.append("<b class=\"xtop\"><b class=\"xb1\"></b><b class=\"xb2\"></b><b class=\"xb3\"></b><b class=\"xb4\"></b></b>\n");
		html.append("<div class=\"xboxcontent\">\n");
		html.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		html.append("<tr>\n");
		html.append("<td width=\"26%\" height=\"120\" align=\"center\"><img src=\"#springUrl('')/static/common/img/alert.png\" width=\"160\" height=\"170\" /></td>\n");
		html.append("<td width=\"74%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n");
		html.append("<tr>\n");
		html.append("<td height=\"20\" valign=\"bottom\"><h2 style=\"color:#a0560d; font-family:'΢ɭ҅ºا; font-weight:normal; margin:0;\">" + title + "</h2></td>\n");
		html.append("</tr>\n");
		html.append("<tr>\n");
		html.append("<td height=\"20\" valign=\"top\"><p  style=\"color:#a0560d; font-family:'΢ɭ҅ºا; font-size:16px; margin:0; padding-right:10px;\">" + msg + "</p></td>\n");
		html.append("</tr>\n");
		html.append("</table>\n");
		html.append("</td>\n");
		html.append("</tr>\n");
		html.append("</table>\n");
		html.append("</div>\n");
		html.append("<b><b class=\"xb4\"></b><b class=\"xb3\"></b><b class=\"xb2\"></b><b class=\"xb1\"></b></b>\n");
		html.append("</div>\n");
		html.append("</body>\n");
		html.append("</html>\n");
		return html.toString();
	}
	/**
	 */
	public static String getInfoWindow(String msg){
		return getWarningWindow(msg);
	}
}

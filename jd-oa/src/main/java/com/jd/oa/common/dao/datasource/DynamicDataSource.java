package com.jd.oa.common.dao.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.jd.oa.common.dao.interceptor.DialectStatementHandlerInterceptor;
import com.jd.oa.common.utils.SpringContextUtils;

/**
 * 动态数据源
 * 
 * @author zhouhq
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 是否支持多数据源
	 */
	private boolean supportMultiDataSource;

	@Override
	protected Object determineCurrentLookupKey() {
		// 在进行DAO操作前，通过上下文环境变量，获得数据源的类型
		return DataSourceHandle.getDataSourceType();
	}

	public Connection getConnection() throws SQLException {
		logger.info("DataSourceHandle.getDataSourceType()==="
				+ DataSourceHandle.getDataSourceType());
		Connection con = determineTargetDataSource().getConnection();
		String databaseProductName = con.getMetaData().getDatabaseProductName();
		if (supportMultiDataSource) {
			DialectStatementHandlerInterceptor dialectStatementHandlerInterceptor = (DialectStatementHandlerInterceptor) SpringContextUtils
					.getBean("dialectStatementHandlerInterceptor");
			if ("MySQL".equals(databaseProductName)) {
				dialectStatementHandlerInterceptor
						.setDialect("com.jd.oa.common.dao.dialect.impl.MySQLDialect");
			}

			if (databaseProductName.startsWith("Microsoft SQL Server")) {
				dialectStatementHandlerInterceptor
						.setDialect("com.jd.oa.common.dao.dialect.impl.SQLServerDialect");
			}

			if ("Oracle".equals(databaseProductName)) {
				dialectStatementHandlerInterceptor
						.setDialect("com.jd.oa.common.dao.dialect.impl.OracleDialect");
			}
		}

		// 获得当前连接的数据源
		BasicDataSource dataSource;
		String catalog = "";
		// 如果没有指定数据源，则用默认数据源
		if (DataSourceHandle.getDataSourceType() == null) {
			dataSource = (BasicDataSource) this.determineTargetDataSource();
		} else {
			dataSource = (BasicDataSource) SpringContextUtils
					.getBean(DataSourceHandle.getDataSourceType());
		}

		logger.info("["
				+ (con.getCatalog() != null ? con.getCatalog() : catalog)
				+ "数据源连接池:]\t空闲连接：" + dataSource.getNumIdle() + "\t活动连接："
				+ dataSource.getNumActive() + "\t总连接数："
				+ (dataSource.getNumIdle() + dataSource.getNumActive()));
		DataSourceHandle.clearDataSourceType();
		return con;
	}

	public boolean isSupportMultiDataSource() {
		return supportMultiDataSource;
	}

	public void setSupportMultiDataSource(boolean supportMultiDataSource) {
		this.supportMultiDataSource = supportMultiDataSource;
	}
}

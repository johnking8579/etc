/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-3-21
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.common.webservice;

import com.jd.common.json.JsonUtil;
import com.jd.common.xml.XmlUtil;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.utils.JsonUtils;
import com.jd.oa.di.service.DiInterfaceParameter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataConvertion {

	/**
	 * 数据类型转换
	 * @param returnType
	 * @param object
	 * @return
	 */
	public static  Object convertData(RESULT_DATA_TYPE returnType,Object object){
		Object result  =  null;
		switch (returnType){
			case MAP:
				result  =  Object2Map(object);
				break;
			case JSON:
				try {
					result  =  JsonUtil.marshal(object);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case STRING:
				try {
					result  =  JsonUtil.marshal(object);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			default:
				result = object;
				break;
		}
		return result;
	}

	/**
	 * Map转Pojo对象
	 * @param clazz
	 * @param valueMap
	 * @return
	 */
	public static Object map2Pojo(Class<?> clazz,Map<String,Object> valueMap){
		Object o= null;
		try {
			o = Thread.currentThread().getContextClassLoader().loadClass(clazz.getName()).newInstance();
			Method[] methods=clazz.getDeclaredMethods();
			Set<Map.Entry<String,Object>> entrySet = valueMap.entrySet();
			for(Map.Entry<String,Object> entry:entrySet){
				for(Method method:methods){
					if(method.getName().startsWith("set")){
						String methodName=method.getName().replace("set","");
						if(methodName.toLowerCase().equals(entry.getKey().toLowerCase())){
							method.invoke(o,entry.getValue());
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  o;
	}

	/**
	 * 简单对象转map
	 * @param o
	 * @return
	 */
	private static Map<String,Object> Pojo2Map(Object o){
		Map<String,Object> returnMap = new HashMap<String, Object>();
		Class<?> clazz = o.getClass();
		Field[] fields = clazz.getDeclaredFields();
		Method[] methods  =  clazz.getDeclaredMethods();
		for(Field field:fields){
			try {
				//遍历方法取出 属性的 get/is方法,可以兼容非JavaBean规范的属性读写方法
				for(Method method:methods){
					String methodName;
					if(method.getName().startsWith("get")){
						methodName = method.getName().substring(3);
					}else if(method.getName().startsWith("is")){
						methodName = method.getName().substring(2);
					}else{
						continue;
					}
					if(methodName.toLowerCase().equals(field.getName().toLowerCase())){
						Object out = method.invoke(o, null);
						returnMap.put(field.getName(),Object2Map(out));
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnMap;
	}
	/**
	 * 对象实例转成Map类型实例
	 * @param o 对象
	 * @return map实例
	 */
	public static Object Object2Map(Object o){
		if( o== null) return null;
		if(o instanceof List){
			List list = (List) o;
			for(int j = 0;j<list.size();j++){
				Object os = list.remove(j);
				list.add(j,Object2Map(os));
			}
			return o;
		}else if(o.getClass().isArray()){
			Object[] os = (Object[]) o;
			Map[] maps=new HashMap[2];
			for(int j = 0;j<os.length;j++){
				maps[j]=(Map) Object2Map(os[j]);
			}
			return maps;
		}else if(o instanceof  Map){
			Map<String,Object> map  =  (Map<String,Object>) o;
			for(Map.Entry<String,Object> entry:map.entrySet()){
				map.put(entry.getKey(),Object2Map(entry.getValue()));
			}
			return map;
		}else if(o instanceof String){
			String str = (String) o;
			if(isXmlStr(str)){
				return parseXml2Map(str);
			}else if(isJsonStr(str)){
				return json2Map(str);
			}else{
				return str;
			}
		}else if(o instanceof JAXBElement){
			try {
				String xml = XmlUtil.marshal(o);
				Map<String,Object> map = parseXml2Map(xml);
				return map;
			} catch (JAXBException e) {
				e.printStackTrace();
				return o;
			}
		}else if (o instanceof XMLGregorianCalendar){
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(object2Date(o,null));
		}else if(isPojoObject(o)){
			return Pojo2Map(o);
		}else{
			return o;
		}
	}

	/**
	 * 判断是不是简单对象
	 * @param o
	 * @return
	 */
	public static  boolean isPojoObject(Object o){
		if(o.getClass().isArray()) return false;
		if(o instanceof String) return false;
		if(o instanceof Integer) return false;
		if(o instanceof Double) return false;
		if(o instanceof Float) return false;
		if(o instanceof Date) return false;
		if(o instanceof JAXBElement) return  false;
		if(o instanceof BigDecimal) return  false;
		if(o instanceof XMLGregorianCalendar) return false;
		return  o.getClass().getDeclaredFields().length > 0;
	}

	/**
	 *XML Element元素转Map
	 * @param element
	 * @param resultMap
	 */
	public  static void  parseElement2Map(org.dom4j.Element element,Map<String,Object> resultMap){
		//叶子节点
		if(element.isTextOnly()){
			if(element.getTextTrim()==null || element.getTextTrim().equals("")){
				resultMap.put(element.getName(),null);
			}else{
				resultMap.put(element.getName(),element.getTextTrim());
			}
			return;
		}
		Map<String,Object> newMap=new LinkedHashMap<String, Object>();
		Iterator iterator=element.elementIterator();
		while(iterator.hasNext()){
			org.dom4j.Element son=(org.dom4j.Element) iterator.next();
			parseElement2Map(son, newMap);
		}
		//重复的节点组织成以节点名称为key,value是List类型的map对象
		if(resultMap.get(element.getName()) !=null){
			Object temp=resultMap.get(element.getName());
			List<Object> list=new ArrayList<Object>();
			if(temp instanceof  ArrayList){
				list=(ArrayList) temp;
			}else{
				list.add(temp);
			}
			list.add(newMap);
			resultMap.put(element.getName(),list);
		}else{
			resultMap.put(element.getName(),newMap);
		}
	}

	/**
	 * 将xml数据组织成map对象
	 * @param xml 数据
	 * @return map对象
	 */
	public static Map<String,Object> parseXml2Map(String xml){
		Map<String,Object> resutlMap=new LinkedHashMap<String, Object>();
		try {
			org.dom4j.Document document= DocumentHelper.parseText(xml);
			org.dom4j.Element root=document.getRootElement();
			//递归实现组织map数据
			parseElement2Map(root,resutlMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resutlMap;
	}

	/**
	 * 将xml数据组织成List<DiInterfaceParameter>对象
	 * @param xml 数据
	 * @return List<DiInterfaceParameter>对象
	 */
	public static List<DiInterfaceParameter> parseXml2DiInterfaceParameterList(String xml){
		List<DiInterfaceParameter> parametersList = new ArrayList<DiInterfaceParameter>();
		try {
			Document document = DocumentHelper.parseText(xml);
			Element rootElement = document.getRootElement();
			Iterator iterator = rootElement.elementIterator();
			while (iterator.hasNext()){
				Element element = (Element) iterator.next();
				parametersList.add(new DiInterfaceParameter(element));
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return parametersList;
	}


	/**
	 * JsonArray类型转成map
	 * @param jsonArray
	 * @return
	 */
	public static Object json2Map(JSONArray jsonArray){
		List<Object> list = new ArrayList<Object>();
		Iterator iterator = jsonArray.iterator();
		while (iterator.hasNext()){
			Object object = iterator.next();
			if(object instanceof JSONObject){
				list.add(json2Map((JSONObject) object));
			}else{
				list.add(object);
			}
		}
		return list;
	}

	/**
	 * JsonObject 类型转成map
	 * @param jsonObject
	 * @return
	 */
	public static Object json2Map(JSONObject jsonObject){
		if(jsonObject.isArray()){
			return  json2Map(JSONArray.fromObject(jsonObject));
		}else{
			Map<String,Object> map = new LinkedHashMap<String, Object>();
			Set<Map.Entry<String,Object>> set = jsonObject.entrySet();
			for(Map.Entry<String,Object> entry:set){
				if(entry.getValue() instanceof  JSONArray){
					map.put(entry.getKey(),json2Map((JSONArray) entry.getValue()));
				}else if(entry.getValue() instanceof JSONObject){
					map.put(entry.getKey(),json2Map((JSONObject) entry.getValue()));
				}else{
					map.put(entry.getKey(),entry.getValue());
				}
			}
			return map;
		}
	}

	/**
	 * json串转成map
	 * @param json
	 * @return
	 */
	public static  Object json2Map(String json){
		if(json == null || json.length()==0) return null;
		if(json.startsWith("[") && json.endsWith("]")){
			//json数组
			return  json2Map(JSONArray.fromObject(json));
		}else if(json.startsWith("{") && json.endsWith("}")){
			//单个json对象
			return json2Map(JSONObject.fromObject(json));
		}else{
			//普通字串
			return json;
		}
	}

	public static boolean isJsonStr(String json){
		return (json.startsWith("[") && json.startsWith("]"))
				|| (json.startsWith("{") && json.endsWith("}"));
	}

	public static boolean isXmlStr(String xml){
		return  xml.trim().startsWith("<?xml");
	}

	public static String object2Json(Object o){
		String datas;
		try {
			datas = JsonUtils.toJson(o);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
		return datas;
	}

	public static int object2Int(Object o){
		if(o == null) return  0;
		int result = 0;
		try {
			result = Integer.parseInt(o.toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static float object2Float(Object o){
		if(o == null) return  0;
		float result = 0;
		try {
			result = Float.parseFloat(o.toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static double object2Double(Object o){
		if(o == null) return  0;
		double result = 0;
		try {
			result = Double.parseDouble(o.toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static long object2Long(Object o){
		if(o == null) return  0;
		long result = 0;
		try {
			result = Long.parseLong(o.toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String object2String(Object o){
		if(o == null) return null;
		if(o instanceof String){
			return (String) o;
		}else if(o instanceof Date){
			return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format((Date) o);
		}else if (o instanceof java.sql.Date){
			return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(( java.sql.Date) o);
		}else{
			return o.toString();
		}
	}

	public static Date object2Date(Object o,String format){
		if(o == null) return null;
		Date s1 = null;
		if(o instanceof XMLGregorianCalendar){
			s1 =  ((XMLGregorianCalendar) o).toGregorianCalendar().getTime();
		}else if(o instanceof String){
			String date = o.toString();
			if(date == null || date.length() == 0) return  null;
			DateFormat df = new SimpleDateFormat(format);
			try {
				s1 = df.parse(date);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{

		}
		return s1;
	}

	public static XMLGregorianCalendar object2XMLGregorianCalendar(Object o,String format){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(object2Date(o,format));
		XMLGregorianCalendar gc = null;
		try {
			gc = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return gc;
	}

    /**
     * json to map
     * @param s
     * @return
     */
    public static Map parserToMap(String s){
        Map map=new HashMap();
        JSONObject json=JSONObject.fromObject(s);
        Iterator keys=json.keys();
        while(keys.hasNext()){
            String key=(String) keys.next();
            String value=json.get(key).toString();
            if(value.startsWith("{")&&value.endsWith("}")){
                map.put(key, parserToMap(value));
            }else{
                map.put(key, value);
            }
        }
        return map;
    }

}

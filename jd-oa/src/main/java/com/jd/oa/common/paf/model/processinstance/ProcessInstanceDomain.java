package com.jd.oa.common.paf.model.processinstance;

import java.util.List;

/**
 * 流程实例Domain（查询流程实例专用）
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午6:14
 */
public class ProcessInstanceDomain {

    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 流程实例创建时间
     */
    private String startTime;
    /**
     * 业务主键ID
     */
    private String businessKey;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 启动活动ID
     */
    private String startActivityId;
    /**
     * 流程发起者
     */
    private String startUserId;
    /**
     * 是否完成
     */
    private String completed;

    private List<Task> tasks;

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getStartActivityId() {
        return startActivityId;
    }

    public void setStartActivityId(String startActivityId) {
        this.startActivityId = startActivityId;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-16
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.common.webservice;

public enum RESULT_DATA_TYPE {
	MAP,JSON,STRING
}

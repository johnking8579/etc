/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-4-15
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.common.webservice;

import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

public class CXFRestfulClient {

	private WebClient webClient;

	public CXFRestfulClient(String url){
		this.webClient = WebClient.create(url);
	}

	/**
	 * Get方式请求
	 * @param path
	 * @param queryParmas
	 * @return
	 */
	public Object Get(RESULT_DATA_TYPE returnType,String path,Map<String,Object> queryParmas){
		webClient.path(path);
		if(queryParmas != null && queryParmas.size() > 0){
			for(Map.Entry<String,Object> entry : queryParmas.entrySet()){
				webClient.query(entry.getKey(),entry.getValue());
			}
		}
		webClient.type(MediaType.TEXT_XML);
		Response response = webClient.get();
		return  DataConvertion.convertData(returnType,response.getEntity().toString());
	}

	/**
	 * Post方式请求
	 * @param path
	 * @param queryParmas
	 * @return
	 */
	public Object Post(RESULT_DATA_TYPE returnType,String path,Map<String,Object> queryParmas){
		webClient.path(path);
		webClient.type(MediaType.APPLICATION_FORM_URLENCODED);
		StringBuilder stringBuilder = new StringBuilder();
		if(queryParmas != null && queryParmas.size()>0){
			for(Map.Entry<String,Object> entry : queryParmas.entrySet()){
				stringBuilder.append("&"+entry.getKey()+"="+entry.getValue());
			}
		}
		String postData = "";
		if(stringBuilder.length() > 0){
			postData = stringBuilder.substring(1);
		}
		Response response = webClient.post(postData);
		return  DataConvertion.convertData(returnType,response.getEntity().toString());
	}
}

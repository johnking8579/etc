package com.jd.oa.common.redis.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface SimpleCache {
    public String key();
    String type() default "";
    int expire();
    String expireTime() default "";
//    public boolean layerd() default true;
    boolean cacheNull() default false;
}

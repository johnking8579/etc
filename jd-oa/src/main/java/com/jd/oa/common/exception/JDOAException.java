package com.jd.oa.common.exception;
/**
 * 异常基本类
 * @author liub
 *
 */
public class JDOAException extends Exception{
	
	private static final long serialVersionUID = 6223554758134037936L;
	
	public JDOAException(){
		super();
	}
	public JDOAException(String msg){
		super(msg);
	}
	public JDOAException(String s, Exception ex) {
	  super(s, ex);
	} 
	public JDOAException(Throwable cause) {
        super(cause);
    }
}

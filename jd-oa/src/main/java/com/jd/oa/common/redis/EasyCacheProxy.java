package com.jd.oa.common.redis;

import com.jd.oa.common.redis.annotation.CacheProxy;
import net.sf.cglib.proxy.Enhancer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory;
import org.springframework.beans.factory.support.CglibSubclassingInstantiationStrategy;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.support.SimpleInstantiationStrategy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class EasyCacheProxy extends SimpleInstantiationStrategy implements BeanFactoryPostProcessor {
    static final String DEFAULT_INTERCEPTOR = "default";
    Logger logger = LoggerFactory.getLogger(EasyCacheProxy.class);
    private Boolean cacheTurnOn = Boolean.TRUE;
    private CglibSubclassingInstantiationStrategy defaultInstantiationStrategy = new CglibSubclassingInstantiationStrategy();
    private Map<String, CacheInterceptor> interceptors = new HashMap<String, CacheInterceptor>();

    public EasyCacheProxy(CacheInterceptor interceptor) {
        interceptors.put(DEFAULT_INTERCEPTOR, interceptor);
    }

    public Boolean getCacheTurnOn() {
        return cacheTurnOn;
    }

    public void setCacheTurnOn(Boolean cacheTurnOn) {
        this.cacheTurnOn = cacheTurnOn;
    }

    public Map<String, CacheInterceptor> getInterceptors() {
        return interceptors;
    }

    public void setInterceptors(Map<String, CacheInterceptor> interceptors) {
        this.interceptors = interceptors;
    }

    @Override
    public Object instantiate(RootBeanDefinition beanDefinition,
                              String beanName, BeanFactory owner) {
        if (proxyThis(beanDefinition, beanName)) {
            return createProxy(beanDefinition.getBeanClass());
        } else {
            return this.defaultInstantiationStrategy.instantiate(beanDefinition, beanName, owner);
        }
    }

    @Override
    public Object instantiate(RootBeanDefinition beanDefinition,
                              String beanName, BeanFactory owner, Constructor constructor, Object[] args) {
        if (proxyThis(beanDefinition, beanName)) {
            return createProxy(beanDefinition.getBeanClass());
        } else {
            return this.defaultInstantiationStrategy.instantiate(beanDefinition, beanName, owner, constructor, args);
        }
    }

    protected Object createProxy(final Class<?> beanClass) {
        Enhancer enhancer = new Enhancer();
        CacheProxy annotation = beanClass.getAnnotation(CacheProxy.class);
        String cacheType = annotation.type();
        if (cacheType.equals("")) {
            cacheType = DEFAULT_INTERCEPTOR;
        }
        CacheInterceptor interceptor = interceptors.get(cacheType);
        if (interceptor == null) {
            throw new UnsupportedOperationException("cache type " + cacheType + " not configured");
        }
        enhancer.setCallback(interceptor);
        enhancer.setSuperclass(beanClass);
        return enhancer.create();
    }

    protected boolean proxyThis(RootBeanDefinition beanDefinition, String beanName) {
        Class<?> beanClass = beanDefinition.getBeanClass();
        Annotation annotation = beanClass.getAnnotation(CacheProxy.class);
        if (annotation != null && !beanDefinition.getMethodOverrides().isEmpty())
            throw new UnsupportedOperationException("Method Injection not supported!");
        return annotation != null && cacheTurnOn;
    }

    public void postProcessBeanFactory(
            ConfigurableListableBeanFactory beanFactory) throws BeansException {
        if (beanFactory instanceof AbstractAutowireCapableBeanFactory) {
            ((AbstractAutowireCapableBeanFactory) beanFactory).setInstantiationStrategy(this);
        }
    }
}

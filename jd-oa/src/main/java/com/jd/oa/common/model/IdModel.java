package com.jd.oa.common.model;

import java.io.Serializable;
/**
 * 统一定义id的model基类.
 * 
 * 基类统一定义id的属性名称、数据类型、列名映射及生成策略.
 * 子类可重载getId()函数重定义id的列名映射和生成策略.
 * 
 * @author zhouhq
 * 
 */
public abstract class IdModel implements Serializable, Cloneable{

	private static final long serialVersionUID = 2112579240392519299L;
	
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public IdModel clone()throws CloneNotSupportedException {
		return (IdModel)super.clone();
	}
}

package com.jd.oa.common.shiro;

import java.io.Serializable;

public class ShiroUser implements Serializable {

	private static final long serialVersionUID = -3728956741946214496L;
	
	/**
	 * 登录名
	 */
	private String loginName;
	
	/**
	 * 显示名
	 */
	private String displayName;
	
	/**
	 * 组织机构名称
	 */
	private String organzationName;

	public ShiroUser(String loginName, String displayName, String organzationName) {
		super();
		this.loginName = loginName;
		this.displayName = displayName;
		this.organzationName = organzationName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getOrganzationName() {
		return organzationName;
	}

	public void setOrganzationName(String organzationName) {
		this.organzationName = organzationName;
	}
}

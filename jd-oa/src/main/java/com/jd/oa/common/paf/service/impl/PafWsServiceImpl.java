package com.jd.oa.common.paf.service.impl;

import com.google.gson.reflect.TypeToken;
import com.jd.oa.common.paf.service.PafClientService;
import com.jd.oa.common.paf.service.PafService;
import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.ProcessInstance;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.model.processinstance.ProcessInstanceDomain;
import com.jd.oa.common.utils.JsonUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PAF工作流平台服务类(WS方式)
 * User: zhaoming
 * Date: 13-7-18
 * Time: 下午5:18
 */

public class PafWsServiceImpl implements PafService {

    private final static Log LOG = LogFactory.getLog(PafWsServiceImpl.class);

    /**
     * PAF平台WS服务
     */
    private PafClientService pafClientService;
    /**
     * 平台密码
     */
    private String password;

    /**
     * 创建流程实例
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @return result
     */
    public PafResult<ProcessInstance> createProcessInstance(String loginUser, String processDefKey, String businessKey){

        PafResult<ProcessInstance> result = null;
        try {
            String processInstanceId = pafClientService.getClient().invoke("createProcessInstance", loginUser, password, processDefKey, businessKey)[0].toString();
            ProcessInstance processInstance =  new ProcessInstance();
            processInstance.setId(processInstanceId);
            result = new PafResult<ProcessInstance>(true, processInstance);
        } catch (Exception e) {
        	e.printStackTrace();
            LOG.error(e.getMessage());
            result = new PafResult<ProcessInstance>(false);
            result.setErrorCode("createProcessInstance");
            result.setLocalizedMessage("创建流程实例失败");
            result.setErrorStack(e.getMessage());
        }
        return result;

    }

    /**
     * 创建流程实例并推进第一个人工任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefKey 流程定义Key
     * @param businessKey 业务ID
     * @param variables 流程变量
     * @return result
     */
    public PafResult<ProcessInstance> submitHumanProcessInstance(String loginUser, String processDefKey, String businessKey, Map<String, Object> variables){

        PafResult<ProcessInstance> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object[] res = pafClientService.getClient().invoke("createProcessInstance", loginUser, password, processDefKey, businessKey);
            String processInstanceId = res[0].toString();
            pafClientService.getClient().invoke("submitHumanProcessInstanceWithJson", loginUser, password, processInstanceId, mapper.writeValueAsString(variables));
            ProcessInstance processInstance =  new ProcessInstance();
            processInstance.setId(processInstanceId);
            result = new PafResult<ProcessInstance>(true, processInstance);
        } catch (Exception e) {
        	e.printStackTrace();
            LOG.error(e.getMessage());
            result = new PafResult<ProcessInstance>(false);
            result.setErrorCode("submitHumanProcessInstance");
            result.setLocalizedMessage("创建流程实例并推进第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;

    }

    /**
     * 提交任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 需要完成的任务ID
     * @param variables  流程变量
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeUserTask(String loginUser, String taskId, Map<String, Object> variables){

        PafResult<Boolean> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object[] res = pafClientService.getClient().invoke("completeUserTaskWithJson", loginUser, password, taskId, mapper.writeValueAsString(variables));
            result = new PafResult<Boolean>(true,Boolean.parseBoolean(res[0].toString()));
        } catch (Exception e) {
        	e.printStackTrace();
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeUserTask");
            result.setLocalizedMessage("提交任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 回退到上一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToPreviousUserTask(String loginUser, String taskId){

        PafResult<Boolean> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> variables = new HashMap<String, Object>();
            variables.put("RETREAT","1");
            Object[] res = pafClientService.getClient().invoke("completeUserTaskWithJson",loginUser, password, taskId, mapper.writeValueAsString(variables));
            result = new PafResult<Boolean>(true,Boolean.parseBoolean(res[0].toString()));
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToPreviousUserTask");
            result.setLocalizedMessage("回退到上一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 回退到第一个人工任务
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param taskId 任务实例ID
     * @return 是否正确完成
     */
    public PafResult<Boolean> completeToFirstUserTask(String loginUser, String taskId){

        PafResult<Boolean> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> variables = new HashMap<String, Object>();
            variables.put("RETREAT", "-1");
            Object[] res = pafClientService.getClient().invoke("completeUserTaskWithJson",loginUser, password, taskId, mapper.writeValueAsString(variables));
            result = new PafResult<Boolean>(true,Boolean.parseBoolean(res[0].toString()));
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToFirstUserTask");
            result.setLocalizedMessage("回退到第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 加签
     * @param loginUser  当前登录用户的ID或用户名，与流程定义时使用一致
     * @param parentTaskId 任务ID
     * @param users 办理人列表
     * @return 是否正确完成
     */
    public PafResult<Boolean> createSubTask(String loginUser, String parentTaskId, List<String> users){
        PafResult<Boolean> result = null;
        try {
            pafClientService.getClient().invoke("createSubTask",loginUser, password, parentTaskId, users);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("completeToFirstUserTask");
            result.setLocalizedMessage("回退到第一个人工任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }



    /**
     * 查询某一用户的待办任务
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> taskQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<TaskInstance>> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object[] res = pafClientService.getClient().invoke("taskQueryWithJson", loginUser, password, processDefinitionKey, mapper.writeValueAsString(conditions));
            String taskInstanceJson = StringUtils.substringBetween(res[0].toString(), "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
        } catch (Exception e) {
        	e.printStackTrace();
            LOG.error(e.getMessage());
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("taskQuery");
            result.setLocalizedMessage("获取任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 历史任务查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 任务列表
     */
    public PafResult<List<TaskInstance>> historyTaskQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<TaskInstance>> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object[] res =  pafClientService.getClient().invoke("historyTaskQueryJson",loginUser, password, processDefinitionKey, mapper.writeValueAsString(conditions));
            String taskInstanceJson = StringUtils.substringBetween(res[0].toString(), "{\"data\":", ",\"total\"");
            List<TaskInstance> taskInstanceList = JsonUtils.fromJsonByGoogle(taskInstanceJson, new TypeToken<List<TaskInstance>>() {
            });
            if(taskInstanceList == null){
                taskInstanceList = new ArrayList<TaskInstance>();
            }
            result = new PafResult<List<TaskInstance>>(true, taskInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<List<TaskInstance>>(false);
            result.setErrorCode("historyTaskQuery");
            result.setLocalizedMessage("获取历史任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 流程实例查询
     * @param loginUser 当前登录用户的ID或用户名，与流程定义时使用一致
     * @param processDefinitionKey 流程定义Key
     * @param conditions 支持的流程相关属性的查询：processInstanceId, processDefinitionId, businessKey, createStartDate(任务创建起始时间), createEndDate(任务创建结束时间)，taskName, taskDefinitionKey，dueDate， minDueDate， maxDueDate， size, start, sort, order
     * @return 流程实例列表
     */
    public PafResult<List<ProcessInstance>> processInstQuery(String loginUser, String processDefinitionKey,  Map<String, Object> conditions){
        PafResult<List<ProcessInstance>> result = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            Object[] res = pafClientService.getClient().invoke("processInstQueryJson", loginUser, password, processDefinitionKey, mapper.writeValueAsString(conditions));
            String processInstanceJson = StringUtils.substringBetween(res[0].toString(), "{\"data\":", ",\"total\"");
            List<ProcessInstance> processInstanceList = JsonUtils.fromJsonByGoogle(processInstanceJson, new TypeToken<List<ProcessInstance>>() {
            });
            if(processInstanceList == null){
                processInstanceList = new ArrayList<ProcessInstance>();
            }
            result = new PafResult<List<ProcessInstance>>(true, processInstanceList);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<List<ProcessInstance>>(false);
            result.setErrorCode("processInstQuery");
            result.setLocalizedMessage("流程实例查询失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 记录审批意见
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId  审批意见所属任务ID
     * @param processInstId  任务所属流程实例ID
     * @param message  审批意见的内容，业务中审批通过与否可以记录在这里
     */
    public PafResult<Boolean> addTaskComment(String loginUser, String taskId, String processInstId, String message){
        PafResult<Boolean> result = null;
        try {
            pafClientService.getClient().invoke("addTaskComment", loginUser, password, taskId, processInstId, message);
            result = new PafResult<Boolean>(true,true);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("addTaskComment");
            result.setLocalizedMessage("记录审批意见失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 重新分配任务
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @param origAssignee 当前的任务分配用户
     * @param newAssignee 新的任务分配用户，必须为小写
     * @param assigneeType 用户类型，目前只支持“user”
     * @return 重新分配结果
     */
    public PafResult<Boolean> reassignTask(String loginUser, String taskId, String origAssignee, String newAssignee, String assigneeType){
        PafResult<Boolean> result = null;
        try {
            Object[] res = pafClientService.getClient().invoke("reassignTask", loginUser, password, taskId, origAssignee, newAssignee, assigneeType);
            result = new PafResult<Boolean>(true,Boolean.parseBoolean(res[0].toString()));
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("reassignTask");
            result.setLocalizedMessage("重新分配任务失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 流程实例详细信息查询
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstId 流程实例ID
     * @return 流程实例详细信息
     */
    public PafResult<ProcessInstanceDomain> getProcessInstanceDetail(String loginUser, String processInstId){
        PafResult<ProcessInstanceDomain> result = null;
        try {
            Object[] res = pafClientService.getClient().invoke("getProcessInstanceDetail",loginUser, password, processInstId);
            ProcessInstanceDomain processInstance = JsonUtils.fromJsonByGoogle(res[0].toString(), new TypeToken<ProcessInstanceDomain>() {
            });
            if(processInstance == null){
                processInstance = new ProcessInstanceDomain();
            }
            result = new PafResult<ProcessInstanceDomain>(true, processInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<ProcessInstanceDomain>(false);
            result.setErrorCode("getProcessInstanceDetail");
            result.setLocalizedMessage("流程实例详细信息查询");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 取消流程实例
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return 是否成功
     */
    public PafResult<Boolean> cancelProcessInstance(String loginUser, String processInstanceId){
        PafResult<Boolean> result = null;
        try {
            Object[] res = pafClientService.getClient().invoke("cancelProcessInstance",loginUser, password, processInstanceId);
            result = new PafResult<Boolean>(true,Boolean.parseBoolean(res[0].toString()));
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<Boolean>(false);
            result.setErrorCode("cancelProcessInstance");
            result.setLocalizedMessage("取消流程实例失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }

    /**
     * 获取任务详细信息(暂未开放)
     * @param loginUser 当前登录用户的ID或用户名
     * @param taskId 任务ID
     * @return 任务详细信息
     */
    public PafResult<TaskInstance> getTaskDetail(String loginUser, String taskId){
        PafResult<TaskInstance> result = null;
//        try {
//            Object[] res = pafClientService.getClient().invoke("getTaskDetail", loginUser, password, taskId);
//            TaskInstance taskInstance = JsonUtil.fromJsonByGoogle(res[0].toString(), new TypeToken<TaskInstance>() {
//            });
//            if(taskInstance == null){
//                taskInstance = new TaskInstance();
//            }
//            result = new PafResult<TaskInstance>(true, taskInstance);
//        } catch (Exception e) {
//            LOG.error(e.getMessage());
//            result = new PafResult<TaskInstance>(false);
//            result.setErrorCode("getTaskDetail");
//            result.setLocalizedMessage("任务详细信息");
//            result.setErrorStack(e.getMessage());
//        }
        return result;
    }

    /**
     * 获取流程实例图片
     * @param loginUser 当前登录用户的ID或用户名
     * @param processInstanceId 流程实例ID
     * @return
     */
    public PafResult<byte[]> processInstanceFlowPictureQuery(String loginUser, String processInstanceId){
        PafResult<byte[]> result = null;
        try {
            Object[] res = pafClientService.getClient().invoke("processInstanceFlowPictureQuery",processInstanceId, loginUser, password);
            result = new PafResult<byte[]>(true,(byte[])res[0]);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            result = new PafResult<byte[]>(false);
            result.setErrorCode("processInstanceFlowPictureQuery");
            result.setLocalizedMessage("获取流程实例图片失败");
            result.setErrorStack(e.getMessage());
        }
        return result;
    }


    /***************************************************************************************************
     *  OA专用
     ***************************************************************************************************/

    /**
     * 查询当前租户下所有的待办任务 (注：OA系统不再支持WS方式调用)
     * @param loginUser 当前登录用户的ID或用户名
     * @param conditions 流程变量集合
     * @return 待办任务列表
     */
    public PafResult<List<TaskInstance>> taskQueryAdvanced(String loginUser, Map<String, Object> conditions) {
        return null;
    }

    public void setPafClientService(PafClientService pafClientService) {
        this.pafClientService = pafClientService;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	@Override
	public Map<String, Object> taskQueryWithTotal(String loginUser,
			String processDefinitionKey, Map<String, Object> conditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> taskQueryAdvancedWithTotal(String loginUser,
			Map<String, Object> conditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PafResult<List<TaskInstance>> getProcessCurrentUserTasks(String loginUser, 
			List<String> processInstances) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PafResult<Boolean> completeToPreviousUserTask(String loginUser,
			String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PafResult<Boolean> completeToFirstUserTask(String loginUser,
			String taskId, Map<String, Object> variables) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PafResult<ProcessInstance> submitHumanProcessInstance(
			String loginUser, String processInstanceId,
			Map<String, Object> variables) {
		// TODO Auto-generated method stub
		return null;
	}


}

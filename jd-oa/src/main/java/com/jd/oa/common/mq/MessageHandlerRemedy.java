package com.jd.oa.common.mq;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jd.activemq.consumer.TextMessageListener;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.service.ProcessDefService;

public class MessageHandlerRemedy extends TextMessageListener {
	private static final Log logger = LogFactory.getLog(MessageHandlerRemedy.class);
	@Autowired
	private DiDatasourceService diDatasourceService; 
	@Autowired
	private ProcessDefService processDefService; 
	
	@Override
	protected void onMessage(String textMessage) throws Exception {
		logger.info("received Remedy message: " + textMessage);
		//调用Remedy审批接口
		
		//从textMessage获取processKey
		String processKey = "";
		Map<String,Object> mp = (Map<String, Object>) DataConvertion.json2Map(textMessage);
		if (mp.get("processKey")!=null) {
			processKey = mp.get("processKey").toString();		
		
			ProcessDefinition entity = new ProcessDefinition();
			entity.setPafProcessDefinitionId(processKey);
			
			ProcessDefinition pd = null;
			List<ProcessDefinition> pdList = processDefService.find(entity);
			if (pdList!=null & pdList.size()>0) {
				pd = pdList.get(0);
				
				DiDatasource datasource = diDatasourceService.get(pd.getDataSourceId());
				
				Map<String,Object> bussinessObject = new HashMap<String,Object>();
				bussinessObject.put("textMessage", textMessage);
				
		        //如果是接口
		        if(2==datasource.getDataAdapterType()){
		            List<Map<String,Object>>  list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, bussinessObject, "1");        
		        }
			}
		}
		
	}
}

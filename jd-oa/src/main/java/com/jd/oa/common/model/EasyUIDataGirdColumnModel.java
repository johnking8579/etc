package com.jd.oa.common.model;

public class EasyUIDataGirdColumnModel {
//	{field:'formId',title:'formId',width:200,hidden:true},
	private String field;
	private String title;
	private int width;
	private boolean hidden;
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
}

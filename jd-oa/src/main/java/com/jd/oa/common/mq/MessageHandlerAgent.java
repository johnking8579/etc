package com.jd.oa.common.mq;

import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jd.activemq.consumer.TextMessageListener;
import com.jd.oa.agent.service.UnifyAgentReceiveService;
import com.jd.oa.common.utils.SpringContextUtils;

public class MessageHandlerAgent extends TextMessageListener {
	private static final Log logger = LogFactory.getLog(MessageHandlerAgent.class);
	private UnifyAgentReceiveService unifyAgentReceiveService;

	@Override
	protected void onMessage(String textMessage) throws Exception {
		logger.info("received Agent message: " + textMessage);
		if(this.unifyAgentReceiveService == null){
			this.unifyAgentReceiveService = SpringContextUtils.getBean(UnifyAgentReceiveService.class);
		}
		this.unifyAgentReceiveService.receive(textMessage);
	}
}

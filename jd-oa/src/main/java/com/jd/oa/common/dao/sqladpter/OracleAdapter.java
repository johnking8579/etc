package com.jd.oa.common.dao.sqladpter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * oracle适配器
 * 
 * @author zhouhuaqi
 * 
 */
public class OracleAdapter extends BaseSqlAdapter {

	private static Logger logger = LoggerFactory.getLogger(OracleAdapter.class);

	/**
	 * sql类型
	 */
	private SqlType sqlType;

	public OracleAdapter() {
	}

	public OracleAdapter(SqlType sqlType) {
		this.sqlType = sqlType;
	}

	public OracleAdapter(SqlType sqlType, String sql) {
		this.sqlType = sqlType;
		super.sql = sql;
	}

	/**
	 * dll操作需要添加的列信息
	 * 
	 * @param oldClumnName
	 *            老的列名
	 * @param columnName
	 *            列名
	 * @param dataType
	 *            数据类型
	 * @param dataLength
	 *            数据长度
	 * @param isPK
	 *            是否主键
	 * @param isNull
	 *            是否为NULL
	 * @param defaultValue
	 *            默认值
	 * @param comment
	 *            注释
	 * @return
	 */
	public BaseSqlAdapter addColumn(String oldColumnName, String columnName,
			DataType dataType, String dataLength, boolean isPK, boolean isNull,
			String defaultValue, String comment) {
		StringBuffer sql = new StringBuffer();
		int length;
		sql.append((oldColumnName != null ? "`" + oldColumnName + "` " : "")
				+ "`" + columnName + "` ");
		switch (dataType) {
		case STRING:
			length = !dataLength.contains(",") ? Float.valueOf(dataLength)
					.intValue() : 0;
			if (length == 1) {
				sql.append("CAHR(1)");
			} else if (length > 2000) {
				sql.append("TEXT");
			} else {
				sql.append("VARCHAR(" + length + ")");
			}
			break;
		case NUMBER:
			if(dataLength!=null){
				if(!dataLength.contains(",")){
					length = Integer.valueOf(dataLength).intValue();
					if(length == 1)
						sql.append("TINYINT");
					else if(length >1 && length <= 10)
						sql.append("INT");
					else if(length > 10)
						sql.append("BIGINT");
				}
				else{
					sql.append("DECIMAL"
							+ ("(" + dataLength.split(",")[0] + ","
									+ dataLength.split(",")[1] + ")"));
				}
			}
			else{
				sql.append("INT");
			}
			break;
		default:
			sql.append(dataType.name());
			break;
		}
		sql.append((isNull ? " NULL" : " NOT NULL"));
		if (defaultValue != null && !"".equals(defaultValue)) {
			if (dataType.equals(DataType.STRING))
				sql.append(" DEFAULT '" + defaultValue + "'");
			else
				sql.append(" DEFAULT " + defaultValue);
		}
		if (comment != null && !"".equals(comment)) {
			sql.append(" COMMENT '" + comment + "'");
		}
		if (isPK)
			columnMap.put("PK_" + columnName, sql.toString());
		else
			columnMap.put(columnName, sql.toString());
		return this;
	}

	/**
	 * 目前仅支持 Oracle, MySQL ; 日期查询生成 其他数据库方式请根据需要自行扩展
	 */
	protected String getDateTimeQuery(String columnName, RelationalOperator ro,
			String columnValue, DataType dataType) {
		switch (dataType) {
		case DATE:
			return "to_char(".concat(getColumnSql(columnName))
					.concat(",'yyyy-mm-dd')").concat(ro.toString()).concat("'")
					.concat(columnValue).concat("'");
		case TIME:
			return "to_char(".concat(getColumnSql(columnName))
					.concat(",'hh24:mi:ss')").concat(ro.toString()).concat("'")
					.concat(columnValue).concat("'");
		case DATETIME:
			return "to_char(".concat(getColumnSql(columnName))
					.concat(",'yyyy-mm-dd hh24:mi:ss')").concat(ro.toString())
					.concat("'").concat(columnValue).concat("'");
		default:
			break;
		}

		return getColumnSql(columnName).concat(" ").concat(ro.toString())
				.concat(" \"").concat(columnValue).concat("\"");
	}

	@Override
	public SqlType getSqlType() {
		return sqlType;
	}

	/**
	 * main()
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * 创建数据表
		 */
		SqlAdapter adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_CREATE_TABLE)
				.addTable("T_BO_TEST")
				.addColumn(null, "ID", DataType.STRING, "32", Boolean.FALSE,
						Boolean.FALSE, null, "")
				.addColumn(null, "NAME", DataType.STRING, "50", Boolean.FALSE,
						Boolean.TRUE, null, "")
				.addColumn(null, "DESC", DataType.STRING, "1000",
						Boolean.FALSE, Boolean.TRUE, null, "");
		logger.info(adapter.toSql());

		/**
		 * 增加字段
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_ADD_COLUMN)
				.addTable("T_BO_TEST")
				.addColumn(null, "BIRTHDAY", DataType.DATE, null,
						Boolean.FALSE, Boolean.TRUE, null, "出生日期");
		logger.info(adapter.toSql());

		/**
		 * 修改字段
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DDL_CHANGE_COLUMN)
				.addTable("T_BO_TEST")
				.addColumn("NAME", "NAME", DataType.STRING, "150",
						Boolean.FALSE, Boolean.TRUE, null, "");
		logger.info(adapter.toSql());

		/**
		 * 查询
		 */
		adapter = new MySqlAdapter();
		adapter.setSqlType(SqlType.DML_SELECT)
				.addTable("base_user")
				.addTable("base_group")
				.addColumn("base_user.user_name")
				.addColumn("base_group.user_age")
				.addAndCondition("id", RelationalOperator.EQ, "xxxx",
						DataType.STRING)
				.addAndInCondition("base_group.user_age", "'x','x','x','x','x'")
				.addOrLikeCondition("xxx", "yyyy")
				.addGroupBy("base_user.user_name")
				.addAscOrderBy("base_user.create_time")
				.addDescOrderBy("base_user.id");
		logger.info(adapter.toSql());
	}
}

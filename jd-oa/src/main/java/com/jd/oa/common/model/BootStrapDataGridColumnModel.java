package com.jd.oa.common.model;

/**
 * bootstrap 表格列定义model
 * @author liub
 *
 */
public class BootStrapDataGridColumnModel {
	public String mDataProp;
	public String sTitle;
	public String sClass;
	public String sWidth;
	public String getmDataProp() {
		return mDataProp;
	}
	public void setmDataProp(String mDataProp) {
		this.mDataProp = mDataProp;
	}
	public String getsTitle() {
		return sTitle;
	}
	public void setsTitle(String sTitle) {
		this.sTitle = sTitle;
	}
	public String getsClass() {
		return sClass;
	}
	public void setsClass(String sClass) {
		this.sClass = sClass;
	}
	public String getsWidth() {
		return sWidth;
	}
	public void setsWidth(String sWidth) {
		this.sWidth = sWidth;
	}
	
	
}

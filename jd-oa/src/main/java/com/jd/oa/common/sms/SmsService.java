package com.jd.oa.common.sms;

import java.util.List;

public interface SmsService {

	/**
	 * 发送短信
	 * 
	 * @param mobile
	 *            电话号码。手机号码。电信、移动、联通号码均可。
	 * @param msgContent
	 *            短信内容。内容字符长度不大于329。大于329短信接口不会切分。注意：短信内容不要带“[京东]”
	 *            字样。“[京东]”短信内容尾，接口会自动追加至短信内容。若不按此规则，则会在接到的短信中出现两次“[京东]” 字样
	 * 
	 * @return
	 */
	public boolean sentMessage(String mobile, String msgContent);
	
	/**
	 * 异步发送短信
	 * 
	 * @param mobile
	 *            电话号码。手机号码。电信、移动、联通号码均可。
	 * @param msgContent
	 *            短信内容。内容字符长度不大于329。大于329短信接口不会切分。注意：短信内容不要带“[京东]”
	 *            字样。“[京东]”短信内容尾，接口会自动追加至短信内容。若不按此规则，则会在接到的短信中出现两次“[京东]” 字样
	 * 
	 * @return
	 */
	public boolean sentMessageByAsyn(final String mobile, final String msgContent);

	/**
	 * 群发短信
	 * 
	 * @param mobiles
	 *            电话号码。手机号码。电信、移动、联通号码均可。一次性可以输入至多5000个手机号。
	 * @param msgContent
	 *            短信内容。内容字符长度不大于329。大于329短信接口不会切分。注意：短信内容不要带“[京东]”
	 *            字样。“[京东]”短信内容尾，接口会自动追加至短信内容。若不按此规则，则会在接到的短信中出现两次“[京东]” 字样
	 * @return
	 */
	public boolean massMessage(List<String> mobiles, String msgContent);
	
	/**
	 * 异步群发短信
	 * 
	 * @param mobiles
	 *            电话号码。手机号码。电信、移动、联通号码均可。一次性可以输入至多5000个手机号。
	 * @param msgContent
	 *            短信内容。内容字符长度不大于329。大于329短信接口不会切分。注意：短信内容不要带“[京东]”
	 *            字样。“[京东]”短信内容尾，接口会自动追加至短信内容。若不按此规则，则会在接到的短信中出现两次“[京东]” 字样
	 * @return
	 */
	public boolean massMessageByAsyn(final List<String> mobiles, final String msgContent);
}

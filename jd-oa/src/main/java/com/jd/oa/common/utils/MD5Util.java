package com.jd.oa.common.utils;

import java.security.MessageDigest;

public class MD5Util {

	public static final  char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

	/**
	 * MD5加密方法
	 * @param s
	 * @return
	 */
	public final static String MD5(String s) {
	        try {
	            byte[] btInput = s.getBytes();
	            // 获得MD5摘要算法的 MessageDigest 对象
	            MessageDigest mdInst = MessageDigest.getInstance("MD5");
	            // 使用指定的字节更新摘要
	            mdInst.update(btInput);
	            // 获得密文
	            byte[] md = mdInst.digest();
	            // 把密文转换成十六进制的字符串形式
				return byte2HexStr(md);
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	}

	/**
	 * byte数组转16进制字符串
	 * @param data
	 * @return
	 */
	public static String byte2HexStr(byte[] data){
		int j = data.length;
		char str[] = new char[j * 2];
		int k = 0;
		for (int i = 0; i < j; i++) {
			byte byte0 = data[i];
			str[k++] = hexDigits[byte0 >>> 4 & 0xf];
			str[k++] = hexDigits[byte0 & 0xf];
		}
		return new String(str);
	}

	/**
	 * 16进制字符串转成byte数组
	 * @param hexStr
	 * @return
	 */
	public static byte[] hex2Bytes(String hexStr){
		char[] array = hexStr.toCharArray();
		byte[] buffer = new byte[array.length / 2];
		int k = 0;
		for(int i=0;i<array.length-1;i+=2){
			int pos1 = parse(array[i]);
			int pos2 = parse(array[i+1]);
			buffer[k++] = (byte) (pos1 << 4 | pos2);
		}
		return buffer;
	}

	public static int parse(char c){
		for(int i =0;i<hexDigits.length;i++){
			if(hexDigits[i] == c){
				return i;
			}
		}
		return -1;
	}
	 public static void main(String[] args) {
	        System.out.println(MD5Util.MD5("_JDOA_wangdongxing"));
	        System.out.println(MD5Util.MD5("加密"));
	 }
}	

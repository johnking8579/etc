package com.jd.oa.common.redis.annotation;


public @interface  CacheKeyEl {
    boolean simple() default false;
    String el();
}

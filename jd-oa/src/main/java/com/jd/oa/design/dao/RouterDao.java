package com.jd.oa.design.dao;

import com.jd.oa.common.dao.BaseDao;
import com.jd.oa.form.model.FormItem;

import java.util.List;
import com.jd.oa.form.model.Form;
/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-13
 * Time: 上午10:52
 * 路由规则Dao
 */
public interface RouterDao extends BaseDao<FormItem,Long> {
    /**
     * 获取表单相关的主表和子表信息
     * @param form
     * @return
     */
    public List<Form> findTableByFormId(Form form);
    /**
     * 获取表单字段名称
     * @param formItem
     * @return
     */
    public List<FormItem> getItemNameById(FormItem formItem);

    /**
     * 根据ID查询 formItem信息
     * @param formItem
     * @return
     */
    public List<FormItem> findFormItemByFormId (FormItem formItem);
    
    
    public List<FormItem> findAllFormItemsByFormId(FormItem formItem) ;

    /**
     * 获取对应表单下的配置变量个数
     * @param form
     * @return
     */
    public Integer getCountVariable (Form form);
}

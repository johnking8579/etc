package com.jd.oa.api.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.jd.oa.api.service.PASProcessTaskHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.api.dto.ProcessTaskHistoryDto;
import com.jd.oa.app.dao.ProcessTaskHistoryDao;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.common.utils.DateUtils;

@Service("pasProcessTaskHistoryService")
public class PASProcessTaskHistoryServiceImpl implements PASProcessTaskHistoryService {
	
	@Autowired
    private ProcessTaskHistoryDao processTaskHistoryDao;

	@Override
	public List<ProcessTaskHistoryDto> getProcessTaskHistory(
			String processInstanceId) {
		List<ProcessTaskHistoryDto> processTaskHistoryDtos = new ArrayList<ProcessTaskHistoryDto>();
		ProcessTaskHistory processTaskHistory = new ProcessTaskHistory();
		processTaskHistory.setProcessInstanceId(processInstanceId);
		List<ProcessTaskHistory> list = processTaskHistoryDao.selectByCondition(processTaskHistory);
		ProcessTaskHistoryDto processTaskHistoryDto;
		if(null != list && !list.isEmpty()){
			for(ProcessTaskHistory proTasHis:list){
				processTaskHistoryDto = new ProcessTaskHistoryDto();
				processTaskHistoryDto.setTaskName(proTasHis.getTaskName());
				processTaskHistoryDto.setSubmitUserName(proTasHis.getUserName());
				processTaskHistoryDto.setSubmitComments(proTasHis.getComment());
				processTaskHistoryDto.setSubmitResult(proTasHis.getResult());
				String beginTime = DateUtils.datetimeFormat24(new Timestamp(proTasHis.getBeginTime().getTime()));
				processTaskHistoryDto.setStartTime(beginTime);
				String endTime = DateUtils.datetimeFormat24(new Timestamp(proTasHis.getEndTime().getTime()));
				processTaskHistoryDto.setEndTime(endTime);
				processTaskHistoryDtos.add(processTaskHistoryDto);
			}
		}
		return processTaskHistoryDtos;
	}

}

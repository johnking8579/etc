/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author yujiahe 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : yujiahe@jd.com
 */
package com.jd.oa.api.service.impl;

import com.jd.oa.agent.service.MqProduceEbsService;
import com.jd.oa.api.service.PASProcessTaskApprovalService;
import com.jd.oa.app.model.OaTaskInstance;
import com.jd.oa.app.model.ProcessTaskHistory;
import com.jd.oa.app.service.ProcessTaskHistoryService;
import com.jd.oa.app.service.ProcessTaskService;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.webservice.DataConvertion;
import com.jd.oa.di.model.DiCcInterface;
import com.jd.oa.di.model.DiDatasource;
import com.jd.oa.di.service.DiCcInterfaceService;
import com.jd.oa.di.service.DiDatasourceService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessInstanceService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("pasProcessTaskApprovalServiceImpl")
public class PASProcessTaskApprovalServiceImpl implements PASProcessTaskApprovalService {
    @Autowired
    private ProcessTaskHistoryService processTaskHistoryService;
    @Autowired
    private ProcessInstanceService processInstanceService;
    @Autowired
    private ProcessDefService processDefService;
    @Autowired
    private ProcessTaskService processTaskService;
    @Autowired
    private DiDatasourceService diDatasourceService;
    @Autowired
    private DiCcInterfaceService diCcInterfaceService;
    @Autowired
    private MqProduceEbsService mqProduceEbsService;

    private static final Logger logger = Logger.getLogger(PASProcessTaskApprovalServiceImpl.class);

    /**
     * @param processInstanceId //申请单ID
     * @param id                //待办任务ID
     * @param submitUserErp     //审批人erp
     * @param submitUserName    //审批人姓名
     * @param startTime         //审批时间(yyyy-MM-dd hh:mm:ss)
     * @param submitResult      //审批结果(1:批准 2:拒绝 3:驳回)
     * @param submitComments    //审批意见
     * @return
     */
    @Override
    public Object doProcessApproval(String processInstanceId, String id, String submitUserErp, String submitUserName, String startTime, String submitResult, String submitComments, String taskName, String nodeId, String taskType) {

        Map<String, Object> resultMap = new HashMap<String, Object>();//返回结果
        OaTaskInstance task = new OaTaskInstance();//task dto

        // 查询ProcessInstance
        ProcessInstance processInstanceForQuery = new ProcessInstance();
        processInstanceForQuery.setProcessInstanceId(processInstanceId);
        ProcessInstance PI = processInstanceService.selectByProcessInstanceId(processInstanceForQuery);

        if (null != PI) {
            ProcessDefinition PD = processDefService.get(PI.getProcessDefinitionId());
            if (null != PD) {
                //组装数据到DTO START
                task.setBuinessInstanceId(PI.getBusinessInstanceId());
                task.setComments(submitComments);
                task.setId(id);
                task.setName(taskName);
                task.setNodeId(nodeId);
                task.setProcessDefinitionId(PI.getProcessDefinitionId());
                task.setProcessDefinitionKey(PD.getPafProcessDefinitionId());
                task.setProcessInstanceId(processInstanceId);
                task.setProcessInstanceName(PI.getProcessInstanceName());
                task.setStartTime(startTime);
                task.setTaskType(taskType);
                //组装数据到DTO END
                if (PD.getIsOuter().equals("2")) { //统一代办
                    task.setIsProxy("1");
                    return this.processTaskCompleteForUnite(task, submitResult, submitUserErp, submitUserName);
                } else { //内部
                    return this.processTaskCompleteForNormal(task, submitResult, submitUserErp);
                }
            } else {
                resultMap.put("result", "1");
                resultMap.put("message", "无该流程定义");
            }
        } else {
            resultMap.put("result", "1");
            resultMap.put("message", "无该流程实例记录");
        }
        return resultMap;
    }

    /**
     * 普通代办审批函数
     *
     * @param task
     * @param submitResult
     * @param submitUserErp
     * @return
     */
    private Object processTaskCompleteForNormal(OaTaskInstance task, String submitResult, String submitUserErp) {
        Map resultMap = new HashMap();
        Map map = new HashMap();
        if (submitResult.equals("1")) { //批准
            try {
                map = processTaskService.processTaskComplete(submitUserErp, task);
            } catch (Exception e) {
                e.printStackTrace();
                resultMap.put("message", "普通审批【批准】失败：" + map.get("message"));
            }
        } else if (submitResult.equals("2")) { //拒绝
            try {
                map = processTaskService.processTaskRejectToPrevious(submitUserErp, task);
            } catch (Exception e) {
                e.printStackTrace();
                resultMap.put("message", "普通审批【拒绝】失败：" + map.get("message"));
            }
        } else if (submitResult.equals("3")) { //驳回
            try {
                map = processTaskService.processTaskRejectToFirst(submitUserErp, task);

            } catch (Exception e) {
                e.printStackTrace();
                resultMap.put("message", "普通审批【驳回】失败：" + map.get("message"));
            }
        }
        if (null != map.get("Success") && map.get("Success").equals(true)) {
            resultMap.put("result", "0");
        } else {
            resultMap.put("result", "1");
        }
        resultMap.put("message", map.get("message"));
        return resultMap;
    }

    /**
     * 统一代办审批
     *
     * @param task
     * @param resultType
     * @param submitUserErp
     * @param submitUserName
     * @return
     */
    private Object processTaskCompleteForUnite(OaTaskInstance task, String resultType, String submitUserErp, String submitUserName) {
        //处理消息Json串
        Map jsonMap = new HashMap();
        jsonMap.put("processkey", task.getProcessDefinitionKey());
        //如果processInstanceId中包括"process2",认为是remedy发送的审批任务
        if (task.getProcessInstanceId().startsWith(task.getProcessDefinitionKey())) {
            String processInstanceId = task.getProcessInstanceId();
            if (processInstanceId != null && processInstanceId.trim().length() > 0) {
                processInstanceId = processInstanceId.substring((task.getProcessDefinitionKey() + "-").length());
            }
            jsonMap.put("reqId", processInstanceId);
        } else {
            jsonMap.put("reqId", task.getBuinessInstanceId());
        }
        jsonMap.put("taskId", task.getId());
        jsonMap.put("submitUserErp", submitUserErp);
        jsonMap.put("submitUserName", submitUserName);
        jsonMap.put("submitTime", task.getStartTime());
        jsonMap.put("submitResult", resultType);
        jsonMap.put("submitComments", task.getComments());
        String textMessage = DataConvertion.object2Json(jsonMap);
        //webservice入参
        Map parameterMp = new HashMap();
        parameterMp.put("textMessage", textMessage);
        //Result
        Map mp = new HashMap();
        try {
            ProcessDefinition pd = processDefService.get(task.getProcessDefinitionId());
            //PS入参特殊处理
            if ("PS".equals(pd.getSystemId())) {
                Map psMp = new HashMap();
                psMp.put("approvetext", textMessage);
                parameterMp.put("textMessage", psMp);
            }
            //若在流程定义中配置统一待办数据源，则直接调用接口，不走MQ
            if (StringUtils.isNotEmpty(pd.getDataSourceId()) || StringUtils.isNotEmpty(pd.getRejectDataSourceId())) {
                //批准接口
                if ("1".equals(resultType)) {
                    if (StringUtils.isNotEmpty(pd.getDataSourceId())) {
                        DiDatasource datasource = diDatasourceService.get(pd.getDataSourceId());
                        //如果是接口
                        if (2 == datasource.getDataAdapterType()) {
                            //判断入参类型是XML或JSON
                            DiCcInterface dci = diCcInterfaceService.get(datasource.getDataAdapter());
                            List<Map<String, Object>> list = null;
                            if ("1".equals(dci.getInParameterType())) {  //入参类型是XML
                                list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, jsonMap, "1");
                            } else if ("2".equals(dci.getInParameterType())) {  //入参类型是JSON
                                list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, parameterMp, "1");
                            }
                        }
                        mp.put("result", "0");
                        mp.put("message", "审批成功");
                    } else {
                        throw new BusinessException("审批任务异常!", "统一待办审批接口配置有误。");
                    }
                }
                //拒绝接口
                if ("2".equals(resultType)) {
                    if (StringUtils.isNotEmpty(pd.getRejectDataSourceId())) {
                        DiDatasource datasource = diDatasourceService.get(pd.getRejectDataSourceId());
                        //如果是接口
                        if (2 == datasource.getDataAdapterType()) {
                            //判断入参类型是XML或JSON
                            DiCcInterface dci = diCcInterfaceService.get(datasource.getDataAdapter());
                            List<Map<String, Object>> list = null;
                            if ("1".equals(dci.getInParameterType())) {  //入参类型是XML
                                list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, jsonMap, "1");
                            } else if ("2".equals(dci.getInParameterType())) {  //入参类型是JSON
                                list = diDatasourceService.getDatasourceResultList(datasource.getDataAdapter(), 1, 1000000, parameterMp, "1");
                            }
                        }
                        mp.put("result", "0");
                        mp.put("message", "审批成功");
                    } else {
                        throw new BusinessException("审批任务异常!", "统一待办审批接口配置有误。");
                    }
                }
                //若未在流程定义中配置统一待办数据源，则走MQ
            } else {
                mqProduceEbsService.mqProducer(textMessage);
                mp.put("result", "0");
                mp.put("message", "审批成功");
            }
            /* 由对方系统调用《2.审批结果接收接口》执行此操作
            //更新待办任务为“完成”
        	AgentTodoList entity = new AgentTodoList();
        	entity.setProcessInstanceId(task.getProcessInstanceId());
        	entity.setTaskId(task.getId());

        	List<AgentTodoList> atlList = agentTodoListService.find(entity);
        	for (AgentTodoList atl : atlList) {
        		atl.setTaskStatus("1");
        		agentTodoListService.update(atl);
        	}
        	//插入审批历史记录
        	ProcessTaskHistory pth = new ProcessTaskHistory();
        	pth.setProcessInstanceId(task.getProcessInstanceId());
        	pth.setTaskId(task.getId());
        	pth.setTaskName(task.getName());
        	pth.setPafProcessDefinitionId(task.getProcessDefinitionKey());
        	pth.setPafProcessDefinitionId(task.getProcessDefinitionId());
        	pth.setBeginTime(DateUtils.convertFormatDateString_yyyy_MM_dd_HH_mm_ss(task.getStartTime()));
        	pth.setEndTime(new Date());
        	pth.setComment(task.getComments());
        	pth.setTaskType(SystemConstant.TASK_KUBU_OWNER);
        	pth.setUserId(ComUtils.getLoginNamePin());
        	pth.setUserName(ComUtils.getDisplayName());
        	processTaskHistoryService.insert(pth);
        	*/
            mp.put("result", "0");
        } catch (Exception e) {
            mp.put("result", "1");
            mp.put("message", "统一代办审批异常" + e);
            logger.error(e);
            throw new BusinessException("审批任务异常!", e);
        }
        return mp;
    }
}



/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.service;

import com.jd.oa.api.dto.ProcessTodoTaskDto;
import com.jd.oa.app.model.Conditions;
import com.jd.oa.system.model.SysUser;

import java.util.List;
import java.util.Map;

public interface PASProcessTaskListService {

	/**
	 * 根据ERP获取待办任务
	 * @return 用户待办任务列表
	 */

	public List<ProcessTodoTaskDto> getTodoTaskList(String userErp);

	/**
	 * 按照条件查询待办任务
	 * @return
	 */
	public List<ProcessTodoTaskDto> getTodoTaskList(String userErp,Map<String,String> queryMap);
}

/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.auth;


import com.jd.oa.common.webservice.DataConvertion;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public class PASAuthInfo {

	public static final String SYSTEM_ID="systemId";
	public static final String ERP="erp";
	public static final String TOKEN="token";
	public static final String REUQEST_TIME="requestTime";

	private String systemId;
	private String erp;
	private String token;
	private String requestTime;

	public PASAuthInfo(HttpServletRequest request){
		this.systemId = request.getParameter(SYSTEM_ID);
		this.erp = request.getParameter(ERP);
		this.token = request.getParameter(TOKEN);
		this.requestTime = DataConvertion.object2String(new Date());
	}

	public String getSystemId() {
		return systemId;
	}

	public String getErp() {
		return erp;
	}

	public String getToken() {
		return token;
	}

	public String getRequestTime() {
		return requestTime;
	}
}

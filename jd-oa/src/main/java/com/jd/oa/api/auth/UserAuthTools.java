/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.auth;

import com.jd.oa.common.utils.MD5Util;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;
import org.apache.commons.lang.StringUtils;

public class UserAuthTools {

	private final static String CHECK_TOKEN="_JDOA_";
	private static SysUserService sysUserService = null;

	public static SysUser auth(String systemId,String userErp,String token){
		if(StringUtils.isEmpty(systemId)||StringUtils.isEmpty(userErp) || StringUtils.isEmpty(token)){
			throw new RuntimeException("用户ERP或TOKEN不能为空!");
		}
		if(!token.equals(MD5Util.MD5(CHECK_TOKEN +systemId+"_"+userErp))){
			throw new RuntimeException("用户ERP验证不合法!");
		}
		if(sysUserService == null){
			sysUserService = SpringContextUtils.getBean("sysUserService");
		}
		SysUser sysUser = sysUserService.getByUserName(userErp);
		if(sysUser == null){
			throw new RuntimeException("用户["+userErp+"]不存在!");
		}
		return sysUser;
	}
}

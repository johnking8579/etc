package com.jd.oa.api.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.jd.activemq.producer.JMSProducer;
import com.jd.oa.api.service.MqProduceJmeService;
import com.jd.oa.common.utils.PlatformProperties;

import javax.annotation.Resource;
import javax.jms.JMSException;

@Component("mqProduceJmeService")
public class MqProduceJmeServiceImpl implements MqProduceJmeService {
	@Resource(name = "produceJme")
	private JMSProducer produceJme;
	private static final Log logger = LogFactory.getLog(MqProduceJmeServiceImpl.class);

	@Override
	public String mqProducer(String textMessage) {
		
		String producer = PlatformProperties.getProperty("mq.jme.producer.destination");
		
		try {
			logger.info("send Jme message: " + textMessage);
			produceJme.send(producer, textMessage, "");
		} catch (JMSException e) {
			e.printStackTrace();
			return "error";
		}
		
		return "success";
	}

}

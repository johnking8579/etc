package com.jd.oa.api.dto;

/**
 * @author zhengbing
 * @desc  
 * @date 2014年7月19日 下午3:02:54
 */
public class ProcessInstanceDetailSonDto {
	
	private String businessColumnId; //关键业务字段id,
	private String businessColumnName; //关键业务字段name,
	private String businessColumnValue; //关键业务字段value},
	
	public String getBusinessColumnId() {
		return businessColumnId;
	}
	public void setBusinessColumnId(String businessColumnId) {
		this.businessColumnId = businessColumnId;
	}
	public String getBusinessColumnName() {
		return businessColumnName;
	}
	public void setBusinessColumnName(String businessColumnName) {
		this.businessColumnName = businessColumnName;
	}
	public String getBusinessColumnValue() {
		return businessColumnValue;
	}
	public void setBusinessColumnValue(String businessColumnValue) {
		this.businessColumnValue = businessColumnValue;
	}
	
	
}

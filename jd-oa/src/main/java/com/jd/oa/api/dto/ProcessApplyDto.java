/**
 * @copyright Copyright 2014-XXXX JD.COM All Right Reserved
 * @author wangdongxing 部门:综合职能研发部 
 * @date 14-7-19
 *	@email : wangdongxing@jd.com
 */
package com.jd.oa.api.dto;

public class ProcessApplyDto {

	/**
	 * 流程定义Code
	 */
	private String processCode;
	/**
	 * 流程定义Key
	 */
	private String processKey;
	/**
	 * 流程定义名称
	 */
	private String processName;
	/**
	 * 申请单实例ID
	 */
	private String reqId;
	/**
	 * 申请单主题
	 */
	private String reqName;
	/**
	 * 申请单创建时间
	 */
	private String reqTime;
	/**
	 * 申请单状态Code
	 */
	private String statusCode;
	/**
	 * 申请单状态文本
	 */
	private String statusText;
	/**
	 * 申请单当前审批任务ID
	 */
	private String currentTaskId;
	/**
	 * 申请单当前审批任务名称
	 */
	private String currentTaskName;
	/**
	 * 申请单当前审批人ERP,多个审批人用逗号分隔
	 */
	private String currentTaskAssigneerErp;
	/**
	 * 申请单当前审批人名字，多个审批人用逗号分隔
	 */
	private String currentTaskAssigneerName;


	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getProcessKey() {
		return processKey;
	}

	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getReqId() {
		return reqId;
	}

	public void setReqId(String reqId) {
		this.reqId = reqId;
	}

	public String getReqName() {
		return reqName;
	}

	public void setReqName(String reqName) {
		this.reqName = reqName;
	}

	public String getReqTime() {
		return reqTime;
	}

	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public String getCurrentTaskId() {
		return currentTaskId;
	}

	public void setCurrentTaskId(String currentTaskId) {
		this.currentTaskId = currentTaskId;
	}

	public String getCurrentTaskName() {
		return currentTaskName;
	}

	public void setCurrentTaskName(String currentTaskName) {
		this.currentTaskName = currentTaskName;
	}

	public String getCurrentTaskAssigneerErp() {
		return currentTaskAssigneerErp;
	}

	public void setCurrentTaskAssigneerErp(String currentTaskAssigneerErp) {
		this.currentTaskAssigneerErp = currentTaskAssigneerErp;
	}

	public String getCurrentTaskAssigneerName() {
		return currentTaskAssigneerName;
	}

	public void setCurrentTaskAssigneerName(String currentTaskAssigneerName) {
		this.currentTaskAssigneerName = currentTaskAssigneerName;
	}
}

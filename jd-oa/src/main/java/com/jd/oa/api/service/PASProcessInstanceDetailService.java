package com.jd.oa.api.service;


import javax.servlet.http.HttpServletRequest;

import com.jd.oa.api.dto.ProcessInstanceDetailDto;

public interface PASProcessInstanceDetailService {
	 /**
	  * 
	  * @author zhengbing 
	  * @desc 根据processInstanceId 查询申请信息详情
	  * @date 2014年7月19日 下午4:46:49
	  * @return ProcessInstanceDetailDto
	  */
    public ProcessInstanceDetailDto findProcessInstanceDetail(HttpServletRequest request,String taskType);
}

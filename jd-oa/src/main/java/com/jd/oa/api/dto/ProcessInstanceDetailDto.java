package com.jd.oa.api.dto;

import java.util.List;

/**
 * @author zhengbing
 * @desc  
 * @date 2014年7月19日 下午2;59;53
 */
public class ProcessInstanceDetailDto {
	
	private String processkey; //流程key,
	private String processName; //流程名称,PROCESS_DEFINITION_NAME
	private String reqId; //申请单ID,     BUSINESS_INSTANCE_ID
	private String reqName; //申请单主题,PROCESS_INSTANCE_NAME
	private String reqUserErp; //申请人erp,CREATOR
	private String reqUserName; //申请人姓名, 
	private String comments; //申请备注,
	private String reqTime; //申请时间(yyyy-MM-dd hh;mm;ss),BEGIN_TIME
	private List<ProcessInstanceDetailSonDto> bussinessData;
	
	
	public String getProcesskey() {
		return processkey;
	}
	public void setProcesskey(String processkey) {
		this.processkey = processkey;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getReqName() {
		return reqName;
	}
	public void setReqName(String reqName) {
		this.reqName = reqName;
	}
	public String getReqUserErp() {
		return reqUserErp;
	}
	public void setReqUserErp(String reqUserErp) {
		this.reqUserErp = reqUserErp;
	}
	public String getReqUserName() {
		return reqUserName;
	}
	public void setReqUserName(String reqUserName) {
		this.reqUserName = reqUserName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public List<ProcessInstanceDetailSonDto> getBussinessData() {
		return bussinessData;
	}
	public void setBussinessData(List<ProcessInstanceDetailSonDto> bussinessData) {
		this.bussinessData = bussinessData;
	}
	
	
}

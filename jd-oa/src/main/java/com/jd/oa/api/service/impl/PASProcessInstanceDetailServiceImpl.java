package com.jd.oa.api.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jd.oa.agent.service.AgentBizDataService;
import com.jd.oa.api.dto.ProcessInstanceDetailDto;
import com.jd.oa.api.dto.ProcessInstanceDetailSonDto;
import com.jd.oa.api.service.PASProcessInstanceDetailService;
import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.utils.DateUtils;
import com.jd.oa.form.execute.FormUIUtil;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessInstanceService;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.SysUserService;

/**
 * 查看流程详情 实现类
 * @author zhengbing
 *
 */
@Service("pasProcessInstanceDetailService")
public class PASProcessInstanceDetailServiceImpl implements PASProcessInstanceDetailService{
	private final static Log log = LogFactory.getLog(PASProcessInstanceDetailServiceImpl.class);


    @Autowired
    private ProcessInstanceService processInstanceService;
    @Autowired
    private SysUserService sysUserService;
	@Autowired
	private AgentBizDataService agentBizDataService;
	@Autowired
	private ProcessDefService processDefService;
	@Autowired
	private ProcessDefinitionConfigService processDefinitionConfigService;
	@Autowired
	private FormItemService formItemService;
	@Autowired
	private FormService formService;
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	
	
	 /**
	  * 根据processInstanceId 查询申请信息详情
	  */
    @Override
    public ProcessInstanceDetailDto findProcessInstanceDetail(HttpServletRequest request,String processInstanceId) {
        //返回的数据结构
    	ProcessInstanceDetailDto processInstanceDetailDto = new ProcessInstanceDetailDto();
    	
    	ProcessInstance processInstance = new ProcessInstance();
        processInstance.setProcessInstanceId(processInstanceId);
		processInstance = processInstanceService.selectByProcessInstanceId(processInstance);
		if(null==processInstance) return null;
		ProcessDefinition processDefinition = processDefService.get(processInstance.getProcessDefinitionId());
		if(null==processDefinition) return null;
		
		processInstanceDetailDto.setProcesskey(processInstance.getPafProcessDefinitionId());
		processInstanceDetailDto.setProcessName(processInstance.getProcessDefinitionName());
		processInstanceDetailDto.setReqId(processInstance.getProcessInstanceId());
		processInstanceDetailDto.setReqName(processInstance.getProcessInstanceName());
		SysUser sysUser = sysUserService.get(processInstance.getStarterId());
		String userName = "";
		String erp = "";
		if(null!=sysUser){
			userName = sysUser.getRealName();
			erp = sysUser.getUserName();
		}
		processInstanceDetailDto.setReqUserName(userName);
		processInstanceDetailDto.setReqUserErp(erp);
		processInstanceDetailDto.setComments("");
		
		String ReqTime = DateUtils.datetimeFormat24(new Timestamp(processInstance.getBeginTime().getTime()));
		processInstanceDetailDto.setReqTime(ReqTime);
		
		String processDefId = processInstance.getProcessDefinitionId();
		String businessInstanceId = processInstance.getBusinessInstanceId();
		String isOuter = processDefinition.getIsOuter();
		
		List<ProcessInstanceDetailSonDto> pidss = null;
		if(SystemConstant.PROCESS_DEFINITION_BIZ.equals(isOuter)) {
			pidss = this.handlerMapToGetBusinessDataForBizTask(processDefId, businessInstanceId);
		}else if(SystemConstant.PROCESS_DEFINITION_OA.equals(isOuter)) {
			pidss = this.handlerMapToGetBusinessData(request,  processDefId, businessInstanceId);
		}
		processInstanceDetailDto.setBussinessData(pidss);
        return processInstanceDetailDto;
    }
    
    /**
     * 
     * @author zhengbing 
     * @desc 得到业务数据并且组织数据返回 统一待办主表关键的业务数据
     * @date 2014年7月19日 下午4:54:49
     * @return List<ProcessInstanceDetailSonDto>
     */
	private List<ProcessInstanceDetailSonDto> handlerMapToGetBusinessDataForBizTask(String processDefId, String businessInstanceId){
		List<String> bussinessColumns=new ArrayList<String>();
        List<ProcessInstanceDetailSonDto> bussinessData = new ArrayList<ProcessInstanceDetailSonDto>();


        //取得列表配置的显示项
        ProcessDefinitionConfig config=new ProcessDefinitionConfig();
        config.setProcessDefinitionId(processDefId);
        config.setConfigType("1");
        List<ProcessDefinitionConfig> configs = processDefinitionConfigService.find(config);
        
        for(ProcessDefinitionConfig c:configs){
			String configItem = c.getConfigItem();
			String[] items = configItem.split(":");
        	ProcessInstanceDetailSonDto dto = new ProcessInstanceDetailSonDto();
            dto.setBusinessColumnId(items[1]);
            dto.setBusinessColumnName(items[0]);
            bussinessData.add(dto);
            bussinessColumns.add(items[1]);
        }

        //填充业务数据
		Map<String,Object> data = (Map<String,Object>) agentBizDataService.get(businessInstanceId);
		for(int i=0;i<bussinessColumns.size();i++){
			String dataIndex = data.get("COL"+(i+1))+"";
			if(null==dataIndex||dataIndex.length()==0) continue;
			for (ProcessInstanceDetailSonDto o : bussinessData) {
				if(null != o.getBusinessColumnId() && o.getBusinessColumnId().equals(bussinessColumns.get(i))){
					if(dataIndex.contains(":")){
						String value = dataIndex.substring(dataIndex.indexOf(":")+1);
						if(value!=null && value.trim().length()>0 && !value.equals("null")){
							o.setBusinessColumnValue(value);
						}
						break;
					}
				}
			}
		}
      return  bussinessData;
	}

    /**
     * 
     * @author zhengbing 
     * @desc 得到业务数据并且组织数据返回oa主表关键数据
     * @date 2014年7月19日 下午4:55:01
     * @return List<ProcessInstanceDetailSonDto>
     */
    private List<ProcessInstanceDetailSonDto> handlerMapToGetBusinessData(HttpServletRequest request,String processDefId, String businessInstanceId){
        //取项目上下文路径
        String contextPath = request.getSession().getServletContext().getRealPath("/");
        List<String> bussinessColumns=new ArrayList<String>();
        List<ProcessInstanceDetailSonDto> bussinessData = new ArrayList<ProcessInstanceDetailSonDto>();

        ProcessDefinition pdf=processDefService.get(processDefId);
        //取得列表配置的显示项
        ProcessDefinitionConfig config=new ProcessDefinitionConfig();
        config.setProcessDefinitionId(processDefId);
        config.setConfigType("1");
        List<ProcessDefinitionConfig> configs=processDefinitionConfigService.find(config);
        List<FormItem> businessFormItems=new ArrayList<FormItem>();  //对字段进行转义操作
        
        for(ProcessDefinitionConfig c:configs){
            FormItem formItem=new FormItem();
            formItem.setFormId(pdf.getFormId());
            formItem.setFieldName(c.getConfigItem());
            List<FormItem> formItems=formItemService.find(formItem);
            if(null!=formItems&&formItems.size()>0){
            	 ProcessInstanceDetailSonDto dto = new ProcessInstanceDetailSonDto();
                 dto.setBusinessColumnId(formItems.get(0).getFieldName());
                 dto.setBusinessColumnName(formItems.get(0).getFieldChineseName());
                 bussinessData.add(dto);
                 bussinessColumns.add(c.getConfigItem());
                 businessFormItems.add(formItems.get(0));
            }
        }
        //取得Table名字
        String tableName = formService.getFormCodeByProDId(processDefId);
        
        FormUIUtil formUIUtil=new FormUIUtil();
        //取业务数据

        //填充业务数据
        List<Map<String,Object>> datas= null;
        if(bussinessColumns.size()!=0){
            datas=formBusinessTableService.getBusinessData(tableName,bussinessColumns,businessInstanceId);
        }
        Map<String,Object> data = null;
        if(null!=datas&&datas.size()>0){
        	data = datas.get(0);
        }
        for(FormItem item:businessFormItems){
            if(data.get(item.getFieldName())!=null){
            	Map<String,String> businessMap=formUIUtil.buildMasterHashtableOfVerify(contextPath, item,data.get(item.getFieldName()).toString());
            	for (ProcessInstanceDetailSonDto o  : bussinessData) {
        			if(o.getBusinessColumnId().equals(item.getFieldName())){
        				o.setBusinessColumnValue(businessMap.get(item.getFieldName()));
        				break;
        			}
        		}
            }
        }
      return  bussinessData;
    }
}

function refresh(){
	$("input,select,textarea").not("input[name$='hk'],select[name$='_length']").jqBootstrapValidation();
}
function validate(selfEls){
	var els;
	if(selfEls!=null&&selfEls.length>0){
		els=selfEls;
	}else{
		els=$("input:visible,select,textarea").not("input[name$='hk'],select[name$='_length']");
	}
	els.trigger("change.validation",{submitting: true});
	var bool=false;
	els.each(function(){
		bool=$(this).jqBootstrapValidation("hasErrors");
		if(bool){
			return false;
		}
	});
	return !bool;
}
//refresh();
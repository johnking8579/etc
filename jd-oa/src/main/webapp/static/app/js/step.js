var Step_Tool_dc = function (ClassName, totalCount, callFun) {
    this.ClassName = ClassName,
        this.callFun = callFun,
        this.Steps = new Array(),
        this.totalCount = totalCount;
    this.stepAllHtml = "";

};
Step_Tool_dc.prototype = {
    /**
     * 绘制到目标位置
     */
    createStepArray: function (currStep, stepListJson) {
        this.currStep = currStep;
        for (var i = 0; i < stepListJson.length; i++) {
            var Step_Obj = new Step(this.currStep, stepListJson[i].StepNum, stepListJson[i].StepText, stepListJson[i].StepMark, stepListJson[i].StepStatus, this.totalCount);
            Step_Obj.createStepHtml();
            this.Steps.push(Step_Obj);
        }
    },
    drawStep: function (currStep, stepListJson) {
        this.clear();
        this.createStepArray(currStep, stepListJson);
        if (this.Steps.length > 0) {
            this.stepAllHtml += "<ul>";
            var preStepNum = 0;
            for (var i = 0; i < this.Steps.length; i++) {
                if (this.Steps[i].StepNum - preStepNum > 1) {
                    if (this.Steps[i].currStep < this.Steps[i].StepNum) {
                        this.stepAllHtml += '<li class="dashedgray"><span></span></li>';
                    } else {
                        this.stepAllHtml += '<li class="dashedred"><span></span></li>';
                    }
                }
                this.stepAllHtml += this.Steps[i].htmlCode;
                preStepNum = this.Steps[i].StepNum;

            }
            this.stepAllHtml += "</ul>";
            jQuery("." + this.ClassName).html(this.stepAllHtml);
            this.createEvent();
        } else {
            jQuery("." + this.ClassName).html("没有任何步骤");
        }
    },
    createEvent: function () {
        var self = this;
        jQuery("." + this.ClassName + " ul li a").click(function () {

            var num = jQuery(this).attr("data-value");
            var text = jQuery(this).attr("data-text");
            var result = {value: num, text: text, taskId: self.ClassName};
            eval(self.callFun + "(result)");
        });
    }, clear: function () {
        this.Steps = new Array();
        jQuery("." + this.ClassName).html("");
        this.stepAllHtml = "";
    }

};
var Step = function (currStep, StepNum, StepText, StepMark, StepStatus, totalCount) {
    this.currStep = currStep;
    this.StepNum = StepNum;
    this.StepText = StepText;
    this.totalCount = totalCount;
    this.StepMark = StepMark;
    this.StepStatus = StepStatus;
    this.htmlCode = "";
};
Step.prototype = {
    createStepHtml: function () {
        var stepHtml = "";
        var stepMarkHtml = "";
        stepHtml = stepHtml + "\<a href=\"#\"    data-value=\"" + this.StepNum + "\" data-text=\"" + this.StepText + "\" \>" + this.StepText + "\</a\>";
        if (this.StepMark != "") {
            if (this.StepStatus == "0") {
                stepMarkHtml = "\<div class=\"prompt\"\>" + this.StepMark + "\<s\>\<\/s\>\<\/div\>";

            } else {
                stepMarkHtml = "\<div class=\"prompt01\"\>" + this.StepMark + "\<s\>\<\/s\>\<\/div\>";
            }
        }
        if (this.currStep > this.totalCount) {
            this.currStep = this.totalCount;
        } else if (this.currStep <= 0) {
            this.currStep = 1;
        }

        if (this.currStep > this.StepNum && this.StepNum == 1) {
            classSype = "firstFinshStep";
        } else if (this.currStep == this.StepNum && this.StepNum == 1) {
            classSype = "firstFinshStep_curr1";
        }
        else if (this.currStep == this.StepNum && this.currStep != this.totalCount) {//当前步骤,下一个未进行,并且不是最后一个
            classSype = "coressStep";
        } else if (this.currStep == this.StepNum && this.StepNum == this.totalCount) {//当前步骤 并且是最后一步
            classSype = "finshlast";
        } else if (this.currStep < this.StepNum && this.StepNum == this.totalCount) {//未进行步骤,并且是最后一个
            classSype = "last";
        } else if (this.currStep < this.StepNum) {//未进行的步骤
            classSype = "loadStep";
        } else if (this.currStep > this.StepNum) {//已进行的步骤
            classSype = "finshStep";
        }
        stepHtml = "\<li class=\"" + classSype + "\"\>" + stepHtml + stepMarkHtml + "\</li\>";

        this.htmlCode = stepHtml;
    }
};
/*
	Jtab
	option()
	init()
	autoRun() ���˲���auto:trueʱ���á�	
*/
(function($) {
    $.fn.Jtab = function(option, callback) {
		if(!this.length)return;
        if (typeof option == "function") {
            callback = option;
            option = {};
        }
        var settings = $.extend({
            type: "static",
            auto: false,
            event: "click",
            currClass: "curr",
            source: "data-tag",
			hookKey:"data-widget",
			hookItemVal: "tab-item",
            hookContentVal: "tab-content",
            stay: 5000,
            delay: 100,
			threshold:null,
            mainTimer: null,
            subTimer: null,
            index: 0,
			compatible:false
        }, option || {});
        var items = $(this).find("*["+settings.hookKey+"="+settings.hookItemVal+"]"),
            contens = $(this).find("*["+settings.hookKey+"="+settings.hookContentVal+"]"),
			isUrl = settings.source.toLowerCase().match(/http:\/\/|\d|\.aspx|\.ascx|\.asp|\.php|\.html\.htm|.shtml|.js/g);
			
        if (items.length != contens.length) {
            return false;
        }

        var init = function(index, tag) {
            settings.subTimer = setTimeout(function() {
				items.removeClass(settings.currClass);
				if(settings.compatible){
					// contens.eq(settings.index).hide();
                    contens.hide();
				}
                if (tag) {
                    settings.index++;
					//settings.threshold=settings.threshold?settings.threshold:items.length;
                    if (settings.index == items.length) {
                        settings.index = 0;
                    }
                } else {
                    settings.index = index;
                }
                settings.type = (items.eq(settings.index).attr(settings.source) != null) ? "dynamic" : "static";
                rander();
            }, settings.delay);
        };
        var autoRun = function() {
            settings.mainTimer = setInterval(function() {
                init(settings.index, true);
            }, settings.stay);
        };
        var rander = function() {
            items.eq(settings.index).addClass(settings.currClass);
				if(settings.compatible){
					contens.eq(settings.index).show();
				}
            switch (settings.type) {
				default:
				case "static":
					var source = "";
					break;
				case "dynamic":
					var source = (!isUrl) ? items.eq(settings.index).attr(settings.source) : settings.source;
					items.eq(settings.index).removeAttr(settings.source);
					break;
            }
            if (callback) {
                callback(source, contens.eq(settings.index), settings.index);
            }
        };
		//
        items.each(function(i) {
            $(this).bind(settings.event, function() {
                clearTimeout(settings.subTimer);
                clearInterval(settings.mainTimer);
                init(i, false);
            }).bind("mouseleave", function() {
                if (settings.auto) {
                    autoRun();
                } else {
                    return;
                }
            });
        });
        if (settings.type == "dynamic") {
            init(settings.index, false);
        }
        if (settings.auto) {
            autoRun();
        }
    };
})(jQuery);
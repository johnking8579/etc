// 禁止文本选中
$(document).bind("selectstart",function(){
    return false;
});
$(".box-warp.draggable .mt").bind("mousedown",function(e){
    if($(e.target).hasClass("box-ctrl")){return}
    var $m1 = $(this).parent();
    var $m2 = $(".dragging-content");
    var drggingContent = $m1.html();
    $m1.addClass("dragged-leftholder");
    $m2.html(drggingContent);
    // $m2.css({
    //     width:$m1.width(),
    //     height:$m1.height(),
    //     left:$m1.offset().left,
    //     top:$m1.offset().top
    // }).addClass("dragged-out").show();
    $(this).bind("mousemove",function(e){
        // console.log(e.pageX);
    });
    function leaveOrRelease(){
        $m1.removeClass("dragged-leftholder");
        $(this).unbind("mousemove");
    }
    $m1.one("mouseup",leaveOrRelease);
    $m1.one("mouseleave",leaveOrRelease);
});

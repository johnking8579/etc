<%@ page contentType="text/html; charset=utf-8"%>
<meta http-equiv="Content-Type" content="text/html charset=utf-8">

<script type="text/javascript" src="<%=request.getContextPath()%>/static/common/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/static/common/js/json2.js"></script> 

<script type="text/javascript">
<!--
var tid = "<%=request.getParameter("id")%>";
var task = workflow.getFigure(tid);
jq(function(){
	  jq('#task-properties-layout').mouseleave(function() {
        saveUserTaskProperties(); 
      });	
      jq('#name').blur(function() {
          saveUserTaskProperties(); 
       });
      jq('#isFirst').blur(function() {
          saveUserTaskProperties(); 
       });
	populateTaskProperites();
	//switchTaskCandidatesList(jq('#performerType').combobox('getValue'));	
	
	//WXJ
    // localAssignee = \\${init} 申请节点
    // localAssignee = \\${assignee} 会签节点
	localAssignee = getAssignee($('#id').val());
	if (localAssignee!=null && localAssignee!=""&&localAssignee!="\\${assignee}"){
		$("#isFirst").attr("checked",true);
	}else{
		$("#isFirst").attr("checked",false);
	}
    //是否是会签节点
    var percent = getCounterSignPercent($('#id').val());
    if(parseFloat(percent) > 0){
        $("#isCounterSign").attr("checked",true);
        $("#isCounterSign").val("1");
        $("#completionConditionTr").show();
        $("#completionCondition").val(percent);
    }
});
function switchTaskCandidatesList(performerType){
	if(performerType == 'candidateUsers'){
		task_candidate_panel.panel("refresh","candidateUsersConfig.jsp");
	}else if(performerType == 'candidateGroups'){
		task_candidate_panel.panel("refresh","candidateGroupsConfig.jsp");
	}
}
function listenerActionBt(value,rowData,rowIndex){
	var id = rowData.id;
	var e = '<img onclick="editListener(\''+id+'\')" src="./image/edit.gif" title="'+"编辑"+'" style="cursor:hand;"/>';   
    var d = '<img onclick="deleteListener(\''+id+'\')" src="./image/delete.gif" title="'+"删除"+'" style="cursor:hand;"/>';
	//return e+'&nbsp;'+d;
	return d;
}
function editListener(id){
	_listener_win.window('open');
	_listener_win.window('refresh','taskListenerConfig.html');
}
function deleteListener(id){
	task.deleteListener(id);
	loadTaskListeners();
}
function formActionBt(value,rowData,rowIndex){
	var id = rowData.id;
	//var e = '<img onclick="editForm('+id+')" src="./image/edit.gif" title="'+"编辑"+'" style="cursor:hand;"/>';   
    var d = '<img onclick="deleteForm('+id+')" src="./image/delete.gif" title="'+"删除"+'" style="cursor:hand;"/>';
	//return e+'&nbsp;'+d;
	return d;
}
function editForm(id){
	
}
function deleteForm(id){
	
}


function saveUserTaskProperties(type,data){
	task.taskId=jq('#id').val();
	task.taskName=jq('#name').val();
	task.setContent(jq('#name').val());
		
    if(type=="users"){
    task.candidateUsers=data;
    
    }
    if(type=="mail"){
    if(data.type=="create"){
     task.mail_c_subject=data.subject;
     task.mail_c_content=data.content;
     task.mail_c_event=data.type;
    }
    if(data.type=="complete"){
     task.mail_e_subject=data.subject;
     task.mail_e_content=data.content;
     task.mail_e_event=data.type;
    }
    
    }
   
}
function populateTaskProperites(){
	jq('#id').val(task.taskId);
	jq('#name').val(task.taskName);
	jq('#performerType').combobox('setValue',task.performerType);
	jq('#expression').val(task.expression);
	//jq('#formKey').val(task.formKey);
	jq('#assignee').val(task.assignee);
		
	if(!is_Empty(task.candidateUsers)){
	  jq('#candidateUsers').val(task.candidateUsers);
	}
	if(!is_Empty(task.candidateUsers)){
	  jq('#candidateGroups').val(task.candidateGroups);
	}
	
	
     jq('#multiInstance :checked').val(task.multiInstance_sequential);
	 jq('#loop_cardinality').val(task.loop_cardinality);
	jq('#collection').val(task.collection);
	jq('#element_variable').val(task.element_variable);
	jq('#completion_condition').val(task.completion_condition);
	jq('#multiInstance :checked').val(task.multiInstance_sequential);
	loadTaskListeners();
	
}
function loadTaskListeners(){
	var listeners = task.listeners;
	var listener_grid_rows=[];
	for(var i=0;i<listeners.getSize();i++){
		var listener = listeners.get(i);
		var service_class="";
		if(!is_Empty(listener.serviceExpression)){
		service_class=listener.serviceExpression;
		}
		if(!is_Empty(listener.serviceClass)){
		service_class=listener.serviceClass;
		}
		var nlistener = {
					id:listener.getId(),
					listenerImplimentation:service_class,
					type:listener.serviceType,
					event:listener.event,
					fields:listener.getFieldsString(),
					action:''
				};
		listener_grid_rows[i]=nlistener;
	};
	//alert(listener_grid_rows);
	var listener_grid_data={
			total:listeners.getSize(),
			rows:listener_grid_rows
	};
	//jq('#task-listeners-list').datagrid('loadData',listener_grid_data);
}

function isCounterSignNode(){
    return  $("#isCounterSign").val();
}
function changeIsCounterSignVal(){
    var percent = $("#completionCondition").val();
    var condition = "\\${nrOfCompletedInstances / nrOfInstances >= "+percent+"}";
    task.completion_condition= condition;
}
function saveIsCounterSignNode(ctx){
    if($("#isFirst").is(":checked")){
        alert("申请节点不能同时为会签节点！");
        $("#isCounterSign").attr("checked",false);
        return;
    }

    if($("#isCounterSign").is(":checked")){
        $("#isCounterSign").val("1");
        $("#completionConditionTr").show();
    }else{
        $("#isCounterSign").val("0");
        $("#completionConditionTr").hide();
    }
}
//WXJ
function saveIsFirstNode(ctx){
	if($("#isFirst").is(":checked")){	
    	//$("#assignee").val("\$\{init\}");
    	$("#isFirst").val("1");
    }else{
    	//$("#assignee").val("");
    	$("#isFirst").val("0");
    } 
   
	var ao=[];
	ao.push({ "name": "isFirstNode", "value":$("#isFirst").val()});
	ao.push({ "name": "processId", "value":$("#processDefinitionId").val() });
	ao.push({ "name": "nodeId", "value":$("#id").val() });
	
	
	jQuery.ajax( {
		async: false ,
	    url:"<%=request.getContextPath()%>/process/processNode_addSignUpdate", 
	    data: ao, 
	    success: function(resp) {
	            if(parseInt(resp)>=1) {
	            	bol=true;
	            	//console.log("..."+$("#isFirst").is(":checked"));
	            	if($("#isFirst").is(":checked")){
	                	$("#assignee").val("\$\{init\}");
	                	 setAssignee($('#id').val(),$('#assignee').val());
	                	 
	                	 setTaskAsync($('#id').val(),"false"); 
	                }else{
	                	$("#assignee").val("");
	                	 setAssignee($('#id').val(),$('#assignee').val());
	                	 
	                	 setTaskAsync($('#id').val(),"true"); 
	                } 
	            }             	              
	            	
	            else if(parseInt(resp)==-1){
	            	Dialog.alert("提示结果","只允许有一个申请节点！");
	            	$("#isFirst").attr("checked",false);
	            	bol = false;
	            }
	    }
	});
	
	
	
	//保存流程定义文件
	saveProcessTask(ctx);
	
	return bol;
}

//保存流程定义文件
function saveProcessTask(ctx){
	//设置Initiator
	setInitiator("start","init");
    $.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id : $("#processDefinitionId").val(),
            processDefinitionFile : getProcess(),
            allUserTask : JSON.stringify(getAllUserTask())
        },
        url:"<%=request.getContextPath()%>/process/saveProcessDefinitionToJss",
        success:function (data) {
        	if (addProcessDef(ctx)){
        		
        	}else{
        		
        	}  
        },
        error: function (data) {
            
        }
    });
}


//WXJ
//节点配置
function node(ctx){
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			//节点办理人设置
			document.getElementById("candidateUserFrame").contentWindow.candidateUserSave();
			
			//节点加签规则设置
			document.getElementById("addSignFrame").contentWindow.addSignSave();
						
			//节点表单项权限设置
			document.getElementById("formItemAuthFrame").contentWindow.subForm();
						
			//消息提醒设置
			document.getElementById("emailFrame").contentWindow.emailSave();
			
			//事件设置
			document.getElementById("listenerFrame").contentWindow.listenerSave();
			
			//timer设置
			document.getElementById("timerFrame").contentWindow.timerDelaySave();
			
			//设置流程定义
	        saveUserTaskProperties(); 
			
	      //保存流程定义文件
	        saveProcessTask(ctx);
		}			     							
	}];
	var json_data = JSON.stringify(getAllUserTask()); 
	$("#allUserTask").val(json_data);
	var param="?formId="+$("#formId").val()+"&processId="+$("#processDefinitionId").val()+"&nodeId="+$('#id').val()+"&allUserTask=0";
	Dialog.openRemote("节点配置","<%=request.getContextPath()%>/process/processDef_nodeTab"+encodeURI(encodeURI(param)), 1000, 620,buttons);
}

//-->
</script>

<input type="hidden" id="allUserTask" name="allUserTask" value=""/>
<div id="task-properties-layout" class="easyui-layout" fit="true">
	<div id="task-properties-panel" region="center" border="true">
		<div id="task-properties-accordion" class="easyui-accordion" fit="true" border="false">
				<table id="general-properties">
					<tr>
						<td>Id:</td>
					</tr>
					<tr>
						<td><input type="text" id="id" name="id" size="10" value="" readonly="true"/></td>
					</tr>
					<tr>
						<td>标签:</td>
					</tr>
					<tr>
						<td><input type="text" id="name" name="name" size="10" value=""/></td>
					</tr>
					<tr>
						<td><input type="checkbox" id="isFirst" name="isFirst" value="0" onclick="return saveIsFirstNode('<%=request.getContextPath()%>');"/>&nbsp;&nbsp;是否申请节点</td>
					</tr>
                    <tr>
                        <td><input type="checkbox" id="isCounterSign" name="isCounterSign" value="0" onclick="return saveIsCounterSignNode('<%=request.getContextPath()%>');"/>&nbsp;&nbsp;是否会签节点</td>
                    </tr>
                    <tr id="completionConditionTr" style="display: none">
                        <td>会签通过比例:
                            <select onchange="changeIsCounterSignVal()" id="completionCondition" name="completionCondition" style="width: 100px">
                                <option value="0.1">0.1</option>
                                <option value="0.2">0.2</option>
                                <option value="0.3">0.3</option>
                                <option value="0.4">0.4</option>
                                <option value="0.5">0.5</option>
                                <option value="0.6">0.6</option>
                                <option value="0.7">0.7</option>
                                <option value="0.8">0.8</option>
                                <option value="0.9">0.9</option>
                                <option value="1.0">1.0</option>
                            </select>
                        </td>
                    </tr>
					<tr>
						<td><input type="hidden" id="assignee" name="assignee" value=""/></td>
					</tr>
					<tr>
						<td>属性配置: &nbsp;&nbsp; <a href="#" title="点击设置" onclick="node('<%=request.getContextPath()%>');">点击设置</a></td>
					</tr>
				</table>
			</div>

	</div>
</div>
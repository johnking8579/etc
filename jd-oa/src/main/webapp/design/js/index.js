var processDefinitionId="";
var processDefinitionName="";
var processDefinitionVariables="";
var _process_def_provided_listeners="";
var is_open_properties_panel = false;

var workflow;
jq(function(){
	jq('#process-definition-tab').tabs({
		fit:true,
		onSelect:function(title){
			if(title=='Diagram'){
				
			}else if(title=='XML'){
				jq('#descriptorarea').val(workflow.toXML());
			}
		}
	});
jq('#loadxml').dialog();
jq('#loadxml').dialog('close');
	try{
		_task_obj = jq('#task');
		_designer = jq('#designer');
		_properties_panel_obj = _designer.layout('panel','east');
		_properties_panel_obj.panel({
			onOpen:function(){
				is_open_properties_panel = true;
			},
			onClose:function(){
				is_open_properties_panel = false;
			}
		});
		_process_panel_obj = _designer.layout('panel','center');
		_task_context_menu = jq('#task-context-menu').menu({});
		//_designer.layout('collapse','east');
		_properties_panel_obj.panel('refresh','processProperties.jsp');
		jq('.easyui-linkbutton').draggable({
					proxy:function(source){
						var n = jq('<div class="draggable-model-proxy"></div>');
						n.html(jq(source).html()).appendTo('body');
						return n;
					},
					deltaX:0,
					deltaY:0,
					revert:true,
					cursor:'auto',
					onStartDrag:function(){
						jq(this).draggable('options').cursor='not-allowed';
					},
					onStopDrag:function(){
						jq(this).draggable('options').cursor='auto';
					}	
		});
		//向中间图层拖拽的相应函数
		jq('#paintarea').droppable({
					accept:'.easyui-linkbutton',
					onDragEnter:function(e,source){
						
						jq(source).draggable('options').cursor='auto';
					},
					onDragLeave:function(e,source){
						jq(source).draggable('options').cursor='not-allowed';
					},
					onDrop:function(e,source){
						//jq(this).append(source)
						//jq(this).removeClass('over');
						
						var wfModel = jq(source).attr('wfModel');
						var shape = jq(source).attr('shape');
						if(wfModel){
							var x=jq(source).draggable('proxy').offset().left;
							var y=jq(source).draggable('proxy').offset().top;
							var xOffset    = workflow.getAbsoluteX();
		                    var yOffset    = workflow.getAbsoluteY();
		                    var scrollLeft = workflow.getScrollLeft();
		                    var scrollTop  = workflow.getScrollTop();
		                  //alert(xOffset+"|"+yOffset+"|"+scrollLeft+"|"+scrollTop);
		                   
		                    addModel(wfModel,x-xOffset+scrollLeft,y-yOffset+scrollTop,shape);
						}
					}
				});
		//jq('#paintarea').bind('contextmenu',function(e){
			//alert(e.target.tagName);
		//});
	
	}catch(e){
		alert(e.message);
	};
	
});
function addModel(name,x,y,icon){
	var model = null;
	if(icon!=null&&icon!=undefined)
		model = eval("new draw2d."+name+"('"+icon+"')");
	else
		model = eval("new draw2d."+name+"()");
	model.generateId(name);
	workflow.addModel(model,x,y);
}

function openEventProperties(id){
	if(!is_open_properties_panel)
		_designer.layout('expand','east');
	_properties_panel_obj.panel('refresh','eventProperties.jsp?id='+id);
}
function openTaskProperties(id,type){
	if(!is_open_properties_panel)
		_designer.layout('expand','east');
	if(type=="draw2d.UserTask"){
		_properties_panel_obj.panel('refresh','taskProperties.jsp?id='+id);
	}
	
	if(type=="draw2d.ServiceTask"){
       _properties_panel_obj.panel('refresh','serviceProperties.jsp?id='+id);
	}
    if(type=="draw2d.ScriptTask"){
       _properties_panel_obj.panel('refresh','ScriptProperties.jsp?id='+id);
	}
	 if(type=="draw2d.ReceiveTask"){
       _properties_panel_obj.panel('refresh','ReceiveTaskProperties.jsp?id='+id);
	}
	 if(type=="draw2d.MailTask"){
       _properties_panel_obj.panel('refresh','MailTaskProperties.jsp?id='+id);
	}
	 if(type=="draw2d.TimerCatchEvent"){
       _properties_panel_obj.panel('refresh','TimerCatchEventProperties.jsp?id='+id);
	}
		
}
function openGatewayProperties(id){
	if(!is_open_properties_panel)
		_designer.layout('expand','east');
	_properties_panel_obj.panel('refresh','gatewayProperties.jsp?id='+id);
}
function openProcessProperties(id){
	if(!is_open_properties_panel)
		_designer.layout('expand','east');
	_properties_panel_obj.panel('refresh','processProperties.jsp?id='+id);
}
function openFlowProperties(id){
	if(!is_open_properties_panel)
		_designer.layout('expand','east');
	_properties_panel_obj.panel('refresh','flowProperties.jsp?id='+id);
}
function deleteModel(id){
	alert("deleteModel");
	var task = workflow.getFigure(id);
	workflow.removeFigure(task);
}
function redo(){
	workflow.getCommandStack().redo();
}
function undo(){
	workflow.getCommandStack().undo();
}
function importFile(){
	jq('#loadxml').dialog('open');
	jq('#loadxmlTxt').val("");
	
}
//function transformation(){
//	//console.info(workflow.figures);
//	var data = jq.parseXML( jq('#loadxmlTxt').val() );
//	parseProcessDescriptor(data);
//	
//	
//}

function transformation(data){
	//console.info(workflow.figures);
	var result = jq.parseXML(data);
	parseProcessDescriptor(result);
	
	
}

function exportProcessDef(obj){
	//obj.href="${ctx}/wf/procdef/procdef!exportProcessDef.action?procdefId="+processDefinitionId+"&processName="+processDefinitionName;
}



function openProcessDef(){
	jq.ajax({
		url:"test.xml",
		dataType:'xml',
		error:function(){
			alert("<s:text name='label.common.error'></s:text>","System Error","error");
			return "";
		},
		success:parseProcessDescriptor	
	}); 
}

function createCanvas(disabled){
	try{
		//initCanvas();
		workflow  = new draw2d.MyCanvas("paintarea");
		workflow.scrollArea=document.getElementById("designer-area");
		if(disabled)
			workflow.setDisabled();
		if(typeof processDefinitionId != "undefined" && processDefinitionId != null &&  processDefinitionId != "null" && processDefinitionId != "" && processDefinitionId != "NULL"){
			openProcessDef();
		}else{
			var id = "process"+Sequence.create();
			//var id = workflow.getId();
			workflow.process.category='http://www.activiti.org/test';
			workflow.process.id=id;
			workflow.process.name=id;
			// Add the start,end,connector to the canvas
			
		} 
	}catch(e){
		alert(e.message);
	}
}


function test(a){
	alert(a);
}

function getUserTasks(){
	var data=jq.parseXML(workflow.toXML());
	var xml=jq(data);
	var userTasks = xml.find('userTask');
	var result=new Array();
	userTasks.each(function(i) {
		 var task=new Object();
		 task.name = jq(this).attr('name');
		 task.id = jq(this).attr('id');
		 task.nodeId = jq(this).attr('id');
         var hasCountSignNode = jq(this).find('multiInstanceLoopCharacteristics').attr('activiti:collection');
         if(typeof hasCountSignNode != 'undefined' && hasCountSignNode != null){
            task.isCountSignNode = '1';
         }
         result.push(task);
	});
	return result;
}

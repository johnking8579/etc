draw2d.ScriptTask=function(){
	draw2d.Task.call(this);
	this.setTitle("脚本任务");
	this.setIcon();
};
draw2d.ScriptTask.prototype=new draw2d.Task();
draw2d.ScriptTask.prototype.type="draw2d.ScriptTask";
draw2d.ScriptTask.newInstance=function(ScriptTaskXMLNode){
	var task = new draw2d.ScriptTask();
	task.id=ScriptTaskXMLNode.attr('id');
	task.taskId=ScriptTaskXMLNode.attr('id');
	task.taskName=ScriptTaskXMLNode.attr('name');
	task.setContent(ScriptTaskXMLNode.attr('name'));
	return task;
};
draw2d.ScriptTask.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.ScriptTask.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.ScriptTask.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="script-task-icon";
};
draw2d.ScriptTask.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<scriptTask id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
    if(!is_Empty(this.asynchronous)){
    	xml=xml+'activiti:async="'+this.asynchronous+'" ';
    }
	

	xml=xml+'scriptFormat="groovy" ';
	xml=xml+'>\n';
	return xml;
};
draw2d.ScriptTask.prototype.getEndElementXML=function(){
	var xml = '</scriptTask>\n';
	return xml;
};



draw2d.ScriptTask.prototype.getScriptContentXML=function(){
	var xml='';
    if(is_Empty(this.script_content)){
		return xml;
	}
    xml=xml+'<script>'+this.script_content+'</script>';
	return xml;
};
draw2d.ScriptTask.prototype.toXML=function(){
	var xml=this.getStartElementXML();
	xml=xml+this.getScriptContentXML();
	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.ScriptTask.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};
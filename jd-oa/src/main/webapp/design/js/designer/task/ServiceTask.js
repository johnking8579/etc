draw2d.ServiceTask=function(){
	draw2d.Task.call(this);
	this.setTitle("服务任务");
	this.setIcon();
};
draw2d.ServiceTask.prototype=new draw2d.Task();
draw2d.ServiceTask.prototype.type="draw2d.ServiceTask";
draw2d.ServiceTask.newInstance=function(serviceTaskXMLNode){
	var task = new draw2d.ServiceTask();
	task.id=serviceTaskXMLNode.attr('id');
	task.taskId=serviceTaskXMLNode.attr('id');
	task.taskName=serviceTaskXMLNode.attr('name');
	task.setContent(serviceTaskXMLNode.attr('name'));
	return task;
};
draw2d.ServiceTask.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.ServiceTask.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.ServiceTask.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="service-task-icon";
};
draw2d.ServiceTask.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<serviceTask id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
    if(!is_Empty(this.asynchronous)){
    	xml=xml+'activiti:async="'+this.asynchronous+'" ';
    }
	
	
	return xml;
};
draw2d.ServiceTask.prototype.getEndElementXML=function(){
	var xml = '</serviceTask>\n';
	return xml;
};

draw2d.ServiceTask.prototype.getExtensionElementsXML=function(){
	if(this.listeners.getSize()==0)return '';
	var xml = '<extensionElements>\n';
	xml=xml+this.getListenersXML();
	xml=xml+'</extensionElements>\n';
	return xml;
};
draw2d.ServiceTask.prototype.getListenersXML=function(){
	var xml = '';
	for(var i=0;i<this.listeners.getSize();i++){
		var listener = this.listeners.get(i);
		xml=xml+listener.toXML();
	}
	return xml;
};

draw2d.ServiceTask.prototype.getMainConfigXML=function(){
	var xml = '';
    if(this.service_type=='Java_class'){
       xml=xml+'activiti:class="'+this.service_class+'" '
	}if(this.service_type=='Expression'){
      xml=xml+'activiti:expression="'+this.service_class+'" '
	}
	if(!is_Empty(this.result_variable)){
     xml=xml+'activiti:resultVariableName="'+this.result_variable+'" '

	}
	return xml;
};


draw2d.ServiceTask.prototype.getServiceStartEndXML=function(){
	return '>\n';
};
draw2d.ServiceTask.prototype.toXML=function(){
	
	var xml="";
	xml=xml+this.getStartElementXML();
	xml=xml+this.getMainConfigXML();
    //加上'>\n'
    xml=xml+this.getServiceStartEndXML();


	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.ServiceTask.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};
draw2d.MailTask=function(){
	draw2d.Task.call(this);
	this.setTitle("邮件");
	this.setIcon();
};
draw2d.MailTask.prototype=new draw2d.Task();
draw2d.MailTask.prototype.type="draw2d.MailTask";
draw2d.MailTask.newInstance=function(MailTaskXMLNode){
	var task = new draw2d.MailTask();
	task.id=MailTaskXMLNode.attr('id');
	task.taskId=MailTaskXMLNode.attr('id');
	task.taskName=MailTaskXMLNode.attr('name');
	task.setContent(MailTaskXMLNode.attr('name'));
	return task;
};
draw2d.MailTask.prototype.setWorkflow=function(_5019){
	draw2d.Task.prototype.setWorkflow.call(this,_5019);
};
draw2d.MailTask.prototype.getContextMenu=function(){
	var menu = draw2d.Task.prototype.getContextMenu.call(this);
  return menu;
};
draw2d.MailTask.prototype.setIcon = function(){
	var icon=draw2d.Task.prototype.setIcon.call(this);
	icon.className="mail-task-icon";
};
draw2d.MailTask.prototype.getStartElementXML=function(){
	var xml="";
	var name = this.taskId;
	var taskName = trim(this.taskName);
	if(taskName != null && taskName != "")
		name = taskName;
	if(!is_Empty(this.taskId)){
		xml=xml+'<serviceTask id="'+this.taskId+"";
	}else{
		return xml;
	}
     if(!is_Empty(name)){
    	 xml=xml+'" name="'+name+'" ';  
	}
//    if(!is_Empty(this.asynchronous)){
//    	xml=xml+'activiti:async="'+this.asynchronous+'" ';
//    }
    xml=xml+'activiti:async="'+true+'" '; 

	xml=xml+'activiti:type="mail" ';
	xml=xml+'>\n';
	return xml;
};
draw2d.MailTask.prototype.getEndElementXML=function(){
	var xml = '</serviceTask>\n';
	return xml;
};

draw2d.MailTask.prototype.getListenersXML=function(){
	var xml = '';
	for(var i=0;i<this.listeners.getSize();i++){
		var listener = this.listeners.get(i);
		xml=xml+listener.toXML();
	}
	return xml;
};


draw2d.MailTask.prototype.getMainconfigXML=function(){
	var xml='';
    if(!is_Empty(this.mail_to)){
    	xml=xml+'<extensionElements>';
		xml=xml+'<activiti:field name="to"><activiti:string>'+this.mail_to+'</activiti:string></activiti:field>';
	}else{
		return xml;
	}
	if(!is_Empty(this.mail_from)){
		xml=xml+'<activiti:field name="from"><activiti:string>'+this.mail_from+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_subject)){
		xml=xml+'<activiti:field name="subject"><activiti:string>'+this.mail_subject+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_cc)){
		xml=xml+'<activiti:field name="cc"><activiti:string>'+this.mail_cc+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_bcc)){
		xml=xml+'<activiti:field name="bcc"><activiti:string>'+this.mail_bcc+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_charset)){
		xml=xml+'<activiti:field name="charset"><activiti:string>'+this.mail_charset+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_htmlText)){
		xml=xml+'<activiti:field name="html"><activiti:string>'+this.mail_htmlText+'</activiti:string></activiti:field>';
	}
	if(!is_Empty(this.mail_NhtmlText)){
		xml=xml+'<activiti:field name="text"><activiti:string>'+this.mail_NhtmlText+'</activiti:string></activiti:field>';
	}
	xml=xml+'</extensionElements>';
	return xml;
};
draw2d.MailTask.prototype.toXML=function(){
	var xml=this.getStartElementXML();
	xml=xml+this.getMainconfigXML();
	xml=xml+this.getEndElementXML();
	return xml;
};
draw2d.MailTask.prototype.toBpmnDI=function(){
	var w=this.getWidth();
	var h=this.getHeight();
	var x=this.getAbsoluteX();
	var y=this.getAbsoluteY();
	var xml='<bpmndi:BPMNShape bpmnElement="'+this.taskId+'" id="BPMNShape_'+this.taskId+'">\n';
	xml=xml+'<omgdc:Bounds height="'+h+'" width="'+w+'" x="'+x+'" y="'+y+'"/>\n';
	xml=xml+'</bpmndi:BPMNShape>\n';
	return xml;
};
<%@ page contentType="text/html; charset=utf-8"%>
<meta http-equiv="Content-Type" content="text/html charset=utf-8">
<script type="text/javascript">
<!--
var tid = "<%=request.getParameter("id")%>";

var task = workflow.getFigure(tid);
jq(function(){
		 jq('#task-properties-panel').mouseleave(function() {
          saveTaskProperties(); 
       });
	populateTaskProperites();
	//switchTaskCandidatesList(jq('#performerType').combobox('getValue'));
});


function saveTaskProperties(){
	//alert(tid);
	task.taskId=jq('#id').val();
	task.taskName=jq('#name').val();
	task.setContent(jq('#name').val());
	
	//task.formKey=jq('#formKey').val();
 
	if(jq('#asynchronous:checkbox').attr('checked')=="checked"){
		task.asynchronous="true";
	}else{
        task.asynchronous="false";
	}
	if(jq('#Expression:radio').attr('checked')=="checked"){
		task.service_type="Expression";
	}else{
        task.service_type="Java_class";
	}
	task.service_class=jq('#service_class').val();
	task.result_variable=jq('#result_variable').val();
}

function populateTaskProperites(){
	jq('#id').val(task.taskId);
	jq('#name').val(task.taskName);
	jq('#performerType').combobox('setValue',task.performerType);
	jq('#expression').val(task.expression);
	
	
	if(task.asynchronous=="true"){
		jq('#asynchronous:checkbox').attr('checked',true);
	}else{
        jq('#asynchronous:checkbox').attr('checked',false);
	}
	if(task.service_type=="Expression"){
		jq('#Expression:radio').attr('checked',true);
	}else{
       jq('#Java_class:radio').attr('checked',true);
	}
	jq('#service_class').val(task.service_class);
	jq('#result_variable').val(task.result_variable);
	
	loadTaskListeners();
}
function loadTaskListeners(){
	var listeners = task.listeners;
	var listener_grid_rows=[];
	//alert(listeners.getSize());
	for(var i=0;i<listeners.getSize();i++){
		var listener = listeners.get(i);
		var nlistener = {
					id:listener.getId(),
					listenerImplimentation:listener.getServiceImplementation(),
					type:listener.serviceType,
					event:listener.event,
					fields:listener.getFieldsString(),
					action:''
				};
		listener_grid_rows[i]=nlistener;
	};
	//alert(listener_grid_rows);
	var listener_grid_data={
			total:listeners.getSize(),
			rows:listener_grid_rows
	};
	jq('#task-listeners-list').datagrid('loadData',listener_grid_data);
}
//-->
</script>
<div id="task-properties-layout" class="easyui-layout" fit="true">
	<div id="task-properties-panel" region="center" border="true">
		<div id="task-properties-accordion" class="easyui-accordion" fit="true" border="false">
				<table id="general-properties">
					<tr>
						<td align="right">Id:</td>
						<td><input type="text" id="id" name="id" size="10" value="" readonly="true"/></td>
					</tr>
					<tr>
						<td align="right">标签:</td>
						<td><input type="text" id="name" name="name" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">异步:</td>
						<td><input type="checkbox" id="asynchronous"  name="asynchronous" checked="checked" /></td>
					</tr>
					
			
					<tr >
					<input  id="Java_class" type="radio" name="Sequential" value="Java_class" />Java class	
					<input  id="Expression" type="radio" name="Sequential" value="Expression"  checked="checked" />Expression	
					</tr>
					<tr>
						<td align="right">Service class:</td>
						<td><input type="text" id="service_class" name="id" size="100" value=""/></td>
					</tr>
					<tr>
						<td align="right">Result variable:</td>
						<td><input type="text" id="result_variable" name="id" size="100" value=""/></td>
					</tr>
				</table>
	
			</div>
	</div>
</div>
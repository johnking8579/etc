<%@ page contentType="text/html; charset=utf-8"%>
<meta http-equiv="Content-Type" content="text/html charset=utf-8">
<script type="text/javascript">
<!--
var tid = "<%=request.getParameter("id")%>";

var task = workflow.getFigure(tid);
jq(function(){
			 jq('#task-properties-layout').mouseleave(function() {
          saveTaskProperties(); 
       });
	populateTaskProperites();
	//switchTaskCandidatesList(jq('#performerType').combobox('getValue'));
});


function saveTaskProperties(){
	//alert(tid);
	task.taskId=jq('#id').val();
	task.taskName=jq('#name').val();
	task.setContent(jq('#name').val());
	

	if(jq('#asynchronous:checkbox').attr('checked')=='checked'){
		task.asynchronous="true";
	}else{
        task.asynchronous="false";
	}
}

function populateTaskProperites(){
	jq('#id').val(task.taskId);
	jq('#name').val(task.taskName);


	if(task.asynchronous=="true"){
		jq('#asynchronous:checkbox').attr('checked',true);
	}else{
       jq('#asynchronous:checkbox').attr('checked',false);
	}

	
	loadTaskListeners();
}
function loadTaskListeners(){
	var listeners = task.listeners;
	var listener_grid_rows=[];
	//alert(listeners.getSize());
	for(var i=0;i<listeners.getSize();i++){
		var listener = listeners.get(i);
		var nlistener = {
					id:listener.getId(),
					listenerImplimentation:listener.getServiceImplementation(),
					type:listener.serviceType,
					event:listener.event,
					fields:listener.getFieldsString(),
					action:''
				};
		listener_grid_rows[i]=nlistener;
	};
	//alert(listener_grid_rows);
	var listener_grid_data={
			total:listeners.getSize(),
			rows:listener_grid_rows
	};
	jq('#task-listeners-list').datagrid('loadData',listener_grid_data);
}
//-->
</script>
<div id="task-properties-layout" class="easyui-layout" fit="true">
	<div id="task-properties-panel" region="center" border="true">
		<div id="task-properties-accordion" class="easyui-accordion" fit="true" border="false">
				<table id="general-properties">
					<tr>
						<td align="right">Id:</td>
						<td><input type="text" id="id" name="id" size="10" value="" readonly="true"/></td>
					</tr>
					<tr>
						<td align="right">标签:</td>
						<td><input type="text" id="name" name="name" size="10" value=""/></td>
					</tr>

					<tr>
						<td align="right">异步:</td>
						<td><input name="asynchronous" id="asynchronous" type="checkbox" checked="checked" /></td>
					</tr>
					
				</table>


			</div>
	</div>
</div>
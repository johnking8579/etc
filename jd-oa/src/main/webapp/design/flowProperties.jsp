<%@ page contentType="text/html; charset=utf-8"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/static/common/js/jquery.js"></script>
<script type="text/javascript">
<!--
var fid = "<%=request.getParameter("id")%>";
var line = workflow.getLine(fid);
jq(function(){
	 jq('#line-properties-panel').mouseleave(function() {
         saveLineProperties(); 
      });
	 jq('#condition').blur(function() {
         saveLineProperties(); 
      });
	 jq('#name').blur(function() {
		 saveLineProperties(); 
      });
	 
	populateLineProperites();
});
function listenerActionBt(value,rowData,rowIndex){
	var id = rowData.id;
	var e = '<img onclick="editListener(\''+id+'\')" src="${ctx}/image/edit.gif" title="'+"ä¿®æ¹"+'" style="cursor:hand;"/>';   
    var d = '<img onclick="deleteListener(\''+id+'\')" src="${ctx}/image/delete.gif" title="'+"å é¤"+'" style="cursor:hand;"/>';
	return e+'&nbsp;'+d;
}
function editListener(id){
	_listener_win.window('open');
	_listener_win.window('refresh','flowListenerConfig.html');
}
function deleteListener(id){
	line.deleteListener(id);
	loadLineListeners();
}
function saveLineProperties(){
	line.lineId=jq('#id').val();
	line.lineName=jq('#name').val();
	line.setLabel(jq('#name').val());
	line.condition=jq('#condition').val();
}
function populateLineProperites(){
	jq('#id').val(line.lineId);
	jq('#name').val(line.lineName);
	jq('#condition').val(line.condition);
}
function loadLineListeners(){
	var listeners = line.listeners;
	var listener_grid_rows=[];
	//alert(listeners.getSize());
	for(var i=0;i<listeners.getSize();i++){
		var listener = listeners.get(i);
		var nlistener = {
					id:listener.getId(),
					listenerImplimentation:listener.getServiceImplementation(),
					type:listener.serviceType,
					event:listener.event,
					fields:listener.getFieldsString(),
					action:''
				};
		listener_grid_rows[i]=nlistener;
	};
	//alert(listener_grid_rows);
	var listener_grid_data={
			total:listeners.getSize(),
			rows:listener_grid_rows
	};
	jq('#line-listeners-list').datagrid('loadData',listener_grid_data);
}

//流向表达式
function exp(formId){
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			var exp=saveTable();
            if(exp!="") {
                   exp ='$'+'{'+exp+'}';
            }
			$("#condition").val(exp);
			//设置流程定义
			saveLineProperties(); 
		}			     							
	}];
	//var formId=$("#formId", parent.document);
	var formId=$("#formId").val();
    var processDefinitionId=$("#processDefinitionId").val();
	var exp=$("#condition").val();
	var param="?formId="+formId+"&exp="+encodeURI(encodeURI(exp)+"&processDefinitionId="+processDefinitionId);

	//	var formId=$("#formId",parent.document).val();
		if(formId==""||formId=="null"){
			Dialog.alert("提示信息", "获取表单信息失败！", "","");
		}else{
			Dialog.openRemote("流向表达式","<%=application.getContextPath()%>/design/router_index"+param, 800, 400,buttons);
		}
}

//-->
</script>
<div id="line-properties-layout" class="easyui-layout" fit="true">
	<div id="line-properties-panel" region="center" border="true">
		<div class="easyui-accordion" fit="true" border="false">
				<table id="general-properties">
					<tr>
						<td>Id:</td>
					</tr>
					<tr>
						<td><input type="text" id="id" name="id" size="10" value="" readonly="true"/></td>
					</tr>
					<tr>
						<td>Name:</td>
					</tr>
					<tr>
						<td><input type="text" id="name" name="name" size="10" value=""/></td>
					</tr>
					<tr>
						<td>Condition: &nbsp;&nbsp; <a href="#" title="点击设置" onclick="exp();">点击设置</a></td>
					</tr>
					<tr>
						<td><textarea id="condition" name="condition" cols="30" rows="5"></textarea></td>
					</tr>
				</table>
		</div>
	</div>
</div>
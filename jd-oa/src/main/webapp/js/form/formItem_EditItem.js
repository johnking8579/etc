/**
 * Created with IntelliJ IDEA.
 * Desctription:表单表单项配置IFRAME JS
 * User: yujiahe
 * Date: 13-9-17
 * Time: 上午11:03
 * To change this template use File | Settings | File Templates.
 */


var nEditing = null;//是否正在编辑
var fNum = "";//同名表单项名称个数
var cNum = "";//同名表单项中文名称个数
var createType = "";//高级|普通
var isUpdateMark = true;//是否可更新
var validateMsg = "";//校验信息
var isStdNew = false;

$(function () {
    createType = $("#createType").val();
    isUpdateMark = $("#isUpdateMark").val();

    if (createType == "advancedModule") {
        $("#addSonForm").hide();
    } else if (createType == "copyModule") {
        createType = "standardModule";
    }
//    else if (createType == "standardModule" && isUpdateMark == false ){
//
//    }
    setFormPage("formId", "mainFormItemList");
    /**
     * 新增子表按钮触发事件
     * @pram 传入parentId 为其主表的ID
     */
    $('#addSonForm').click(function () {
        var parentId = $("input#formId").val();
        var buttons = [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function () {
                }
            },
            {
                "label": "保存",
                "class": "btn-success",
                "callback": function () {
                    if (!validate2($("#formForm"))) {
                        return false;
                    } else {
                        return addForm();
                    }

                }
            }
        ];
        Dialog.openRemote('新增子表', 'form_add?parentId=' + parentId, 450, 350, buttons);
    });
    $("#itemDataType").live('change', function (e) {
        disableLength();
        defaultSet();
        $("#inputType").val("");
    });

});
/**
 * 表单项名称，查询同名表单项个数
 */
$("#fieldName").live('blur', (function (e) {

    var fieldName = $("#fieldName").val();
    var formId = $("#hiddenFormId").val();
    jQuery.ajax({
        async: false,
        type: "POST",
        url: springUrl + "/form/formItem_getItemNumOfForm",
        data: {
            formId: formId,
            fieldName: fieldName
        },
        success: function (num) {
            fNum = num;
        }
    });
}
    ));

/**
 * 表单项中文名称，查询同名表单项个数
 */

$("#fieldChineseName").live('blur', (function (e) {

    var fieldChineseName = $("#fieldChineseName").val();
    var formId = $("#hiddenFormId").val();
    jQuery.ajax({
        async: false,
        type: "POST",
        url: springUrl + "/form/formItem_getItemNumOfForm",
        data: {
            formId: formId,
            fieldChineseName: fieldChineseName
        },
        success: function (num) {
            cNum = num;
        }
    });
}
    ));
/**
 * 长度默认值（字符类型：50，数字类型：10）
 */
function defaultSet() {
    var itemDataType = $("#itemDataType").val();
    if (itemDataType == 'STRING') {
        $("#itemLength").val("50");
    } else if (itemDataType == 'NUMBER') {
        $("#itemLength").val("10");

    } else {
        $("#itemLength").val("");
    }
}
/**
 * 时间或者日期，长度不可输入
 */
function disableLength() {
    var itemDataType = $("#itemDataType").val();
    if (itemDataType == 'STRING' || itemDataType == 'NUMBER') {
        $("#itemLength").removeAttr("disabled");
    } else {
        $("#itemLength").attr({"disabled": "disabled"});
        $("#itemLength").val("");
    }
}
/**
 * 分页配置及显示（主表、子表）
 * @param FId       对应相应主表或者子表的formId
 * @param tableId   对应页面上table的ID
 */

function setFormPage(FId, tableId) {
    var dynamicBtn = [];
    if (createType == "standardModule") {
        dynamicBtn.push({
            "sExtends": "text",
            "sButtonText": "新增表单项",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                addRow(eval('table' + FId), FId, tableId);
            }
        });
        dynamicBtn.push({
            "sExtends": "text",
            "sButtonText": "删除表单项",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                var ids = Table.getSelectedRowsIDs(eval('table' + FId));//复选框选中项目的ID，逗号隔开
                if (ids.length == 0) {
                    Dialog.alert("消息提示", "没有选中项", "确定");
                } else if (ids == undefined || ids == '') {
                    Table.render(eval('table' + FId));
                } else {
                    delFormItem(ids, FId);
                }
            }
        })
    }

    var options = Table.options;
    options = {
        "callback_fn": function () {
        },
        scrollY: "100%",
        isPaging: false,
        useCheckbox: true,
        pageUrl: "formItem_mainFormPage",
        defaultSort: [
            [7, "asc"]
        ],
        sendData: function (sSource, aoData, fnCallback) {
            var formId = $("input#" + FId).val();
            aoData.push({
                "name": "formId",
                "value": formId
            }, {
                "name": "computeExpressDesc",
                "value": ''
            });
            jQuery.ajax({
                type: "POST",
                url: "formItem_mainFormPage",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    nEditing = null;
                    fnCallback(resp);
                }
            });
        },
        // 分页列设置
        columns: [

            { "mData": "fieldName", "sTitle": "<span class='mustInput'>*</span>表单项名称", "bSortable": false, "sClass": "my_class", "sWidth": "100px"},
            { "mData": "fieldChineseName", "sTitle": "<span class='mustInput'>*</span>表单项显示名称", "bSortable": false, "sClass": "my_class", "sWidth": "120px"},
            { "mData": "dataType", "sTitle": "<span class='mustInput'>*</span>数据库类型", "bSortable": false, "sClass": "my_class", "sWidth": "80px", fnRender: function (obj) {
                var dataTypeDisplay;
                switch (obj.aData.dataType) {
                    case "STRING" :
                        dataTypeDisplay = "文本";
                        break;
                    case "NUMBER" :
                        dataTypeDisplay = "数值";
                        break;
                    case "DATE" :
                        dataTypeDisplay = "日期";
                        break;
                    case "TIME" :
                        dataTypeDisplay = "时间";
                        break;
                    case "DATETIME" :
                        dataTypeDisplay = "日期时间";
                        break;
                    default:
                        dataTypeDisplay = "文本";
                        break;
                }
                return dataTypeDisplay;
            }},
            { "mData": "length", "sTitle": "<span class='mustInput'>*</span>长度", "bSortable": false, "sClass": "my_class", "sWidth": "40px"},
            {"mData": "inputType", "sTitle": "UI类型", "bSortable": false, "sWidth": "90px"},
            { "mData": "defaultValue", "sTitle": "默认值", "bSortable": false, "sClass": "my_class", "sWidth": "60px"},
            { "mData": "sortNo", "sTitle": "<span class='mustInput'>*</span>显示顺序", "bSortable": true, "sClass": "my_class", "sWidth": "90px"},

            {"mData": "id", "sTitle": "操作", "bSortable": false, "sWidth": "80px", "mRender": function (data, type, full) {
                return " <a href='#' title='修改' class='edit_item' onclick='return false;'>修改</a> " +
                    " <a href='#' title='取消' class='cancel_item'></a> " +
                    " <a href='#' title='删除' class='del_item' onclick='deleteRow(\"" + FId + "\",$(this));return false'>删除</a>";
            } }

        ],
        // 分页按钮设置

        btns: dynamicBtn
    };
//动态生成table名称 由table+FId 拼接
    eval('table' + FId + '=Table.dataTable(tableId, options);');
    eval('bindEditByTable(tableId,table' + FId + ')');
}

/**
 * 删除表单项
 * @param formItemIds  删除表单项ID
 * @parm FId      分页标示，用于定位页面分页
 */
function delFormItem(formItemIds, FId) {
    Dialog.del('formItem_delete?ids=' + formItemIds, '', function (result) {
        Table.render(eval('table' + FId));
    });

}
/**
 * 新增表单项，
 * @param formId 表单Id
 * @param FId    分页标示，用于定位页面分页
 */
function addFormItem(formId, FId) {
    $.ajax({
        type: "GET",
        cache: "false",
        url: "formItem_addSave",
        dataType: "json",
        data: "formId=" + formId,
        success: function (data) {
            Table.render(eval('table' + FId));
        }
    });

}
/**
 * 新增一行记录
 * @param oTable
 * @param FId
 * @param tableId
 */
function addRow(oTable, FId, tableId) {
    var formId = $("input#" + FId).val();
    var arr = [];
    arr.push({"id": "", "fieldName": "", "fieldChineseName": "", "defaultValue": "", "dataType": "", "length": "", "sortNo": "", "inputType": "", "inputTypeConfig": "", "htmlInner": "", "formId": formId});
    var oSettings = oTable.fnSettings();
    oSettings.oFeatures.bServerSide = false;
    var aiNew = oTable.fnAddData(arr);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    editRow(oTable, nRow, tableId, 'new');
    oSettings.oFeatures.bServerSide = true;

}
/**
 * 删除一行记录
 * @param FId
 * @param el
 */
function deleteRow(FId, el) {
    var oTable = eval('table' + FId);
    var nRow = el.parents('tr')[0];
    var aData = oTable.fnGetData(nRow);
    if (aData.id == null || aData.id == "") { //未保存到数据库不走后台删除
        var oSettings = oTable.fnSettings();
        oSettings.oFeatures.bServerSide = false;
        oTable.fnDeleteRow(nRow);
        oSettings.oFeatures.bServerSide = true;
    } else {//走后台删除
        var ids = new Array();
        ids.push(aData.id);
        delFormItem(ids, FId);
    }
    nEditing = null;
}
function bindEditByTable(tableId, table) {

    //绑定编辑按钮按钮
    $('#' + tableId + ' a.edit_item').live('click', function (e) {

        e.preventDefault();
        var nRow = $(this).parents('tr')[0];
        if (nEditing == nRow && this.innerHTML == "保存") {
            /* This row is being edited and should be saved */
            saveRow(table, nRow, tableId);
        } else {
            editRow(table, nRow, tableId);
        }
    });
    $('#' + tableId + ' a.cancel_item').live('click', function (e) {
        e.preventDefault();
        restoreRow(table, nEditing);
    });
}
/**
 * 编辑分页
 * @param oTable
 * @param nRow
 * @param tableId
 * @param flag
 */
function editRow(oTable, nRow, tableId, isNew) {
    if (nEditing == null) {
    } else {
        var currentData = oTable.fnGetData(nEditing);
        if (currentData.id == null || currentData.id == "") {
            var oSettings = oTable.fnSettings();
            oSettings.oFeatures.bServerSide = false;
            oTable.fnDeleteRow(nEditing);
            oSettings.oFeatures.bServerSide = true;
        } else {
            restoreRow(oTable, nEditing);
        }
    }
    nEditing = nRow;
    //aData.dataType
    var aData = oTable.fnGetData(nEditing);
    $("#hiddenFormId").val(aData.formId);
    var dtype = aData.dataType;
    var s = '';
    var i = '';
    var d = '';
    var t = '';
    var dt = '';
    if ("文本" == dtype) {
        s = ' selected="selected"';
    } else if ("数值" == dtype) {
        i = ' selected="selected"';
    } else if ("日期" == dtype) {
        d = ' selected="selected"';
    } else if ("时间" == dtype) {
        t = ' selected="selected"';
    } else if ("日期时间" == dtype) {
        dt = ' selected="selected"';
    }
    var jqTds = $('>td', nEditing);
    if ((isNew || isUpdateMark == 'true')&&createType == "standardModule") {
        if ($.trim(aData.length) == '') {
            aData.length = "50";
        }
        jqTds[1].innerHTML = '<input name="j1" id="fieldName" style="width:' + ((jqTds[1].clientWidth - 16 - 18) * 100 / (jqTds[1].clientWidth - 16)) + '%" value="' + aData.fieldName + '"  type="text" data-rule="required;nochineseandspecial;lettersFirst " /> ';

        jqTds[3].innerHTML =
            '<select id="itemDataType"  name="j3"  style="width:' + ((jqTds[3].clientWidth - 16 - 18) * 100 / (jqTds[3].clientWidth - 16)) + '%" data-rule="required;">'
                + '<option value="STRING"' + s + '>文本' + '</option>'
                + '<option value="NUMBER"' + i + '>数值' + '</option>'
                + '<option value="DATE"' + d + '>日期' + '</option>'
                + '<option value="TIME"' + t + '>时间' + '</option>'
                + '<option value="DATETIME"' + dt + '>日期时间' + '</option>'
                + '</select>';


    }
    jqTds[2].innerHTML = '<input name="j2" id="fieldChineseName" style="width:' + ((jqTds[2].clientWidth - 16 - 18) * 100 / (jqTds[2].clientWidth - 16)) + '%"  value="' + aData.fieldChineseName + '" type="text" data-rule="required;"    />';
//    if (isNew || isUpdateMark == 'true') {
    if (createType == "standardModule"){
        jqTds[4].innerHTML = '<input name="j4" id="itemLength"  style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" value="' + aData.length + '" type="text"   />';
}
//    } else if (createType == "standardModule" && isUpdateMark == "false" && (aData.length.indexOf(",") < 0)) {//暂时只支持  int 不支持浮点
//        jqTds[4].innerHTML = '<input name="j4" id="itemLength"  style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" value="' + aData.length + '" type="text"  data-rule="integer;range[' + aData.length + '~]" />';
//    }

    jqTds[6].innerHTML = '<input  name="j6"  id="fieldDefaultValue" style="width:' + ((jqTds[6].clientWidth - 16 - 18) * 100 / (jqTds[6].clientWidth - 16)) + '%"  value="' + aData.defaultValue + '" type="text"  />';
    jqTds[7].innerHTML = '<input name="j7" id = "sortNo" style="width:' + ((jqTds[7].clientWidth - 16 - 18) * 100 / (jqTds[7].clientWidth - 16)) + '%"  value="' + aData.sortNo + '" type="text"  data-rule="required;float" />';
    var formId = document.getElementById('formId').value;
    var inputTypeDisplay = (aData.inputType == null ? " " : aData.inputType);
    jqTds[5].innerHTML = "<span id='UI' style='cursor:pointer,width:" + ((jqTds[5].clientWidth - 16 - 18) * 100 / (jqTds[5].clientWidth - 16)) + "%' onclick=\"window.parent.editInputType('" + aData.id + "','" + aData.fieldName + "','" + aData.fieldChineseName + "','" + aData.inputType + "','" + formId + "');\">"
        + '<input disabled id="inputType" style="width:60%" value="' + inputTypeDisplay + '" type="text" />' + "<img src='" + springUrl + "/static/common/img/edit.png' alt='组件' />" + "</span>";
    $('.edit_item', jqTds[8]).text('保存');
    if (isNew != "new") {
        $('.cancel_item', jqTds[8]).text('取消');
    }
    $("#inputTypeConfig").val(aData.inputTypeConfig);
    $("#htmlInner").val(aData.htmlInner);
    bindValidate($('#' + tableId));// 绑定验证
    if (isNew || isUpdateMark == 'true') {
        disableLength();
    } else if (createType == "standardModule" && isUpdateMark == "false") {
        if (dtype == "文本" || dtype == "数值") {
            $("#itemLength").removeAttr("disabled");
        } else {
            $("#itemLength").attr({"disabled": "disabled"});
        }
    }
}


/**
 * 保存行
 * @param oTable
 * @param nRow
 * @param tableId
 */
function saveRow(oTable, nRow, tableId) {
    var jqInputs = $('input,select', nRow);
    arr = [];
    //收集数据
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var num = jqInputs.length;
    arr.push({"name": "id", "value": aData.id});
    arr.push({"name": "formId", "value": aData.formId});
    if (num == 8) {//全部修改
        arr.push({"name": "fieldName", "value": jqInputs[1].value});
        arr.push({"name": "fieldChineseName", "value": jqInputs[2].value});
        arr.push({"name": "dataType", "value": jqInputs[3].value});
        arr.push({"name": "length", "value": jqInputs[4].value});
        arr.push({"name": "sortNo", "value": jqInputs[7].value});
        arr.push({"name": "inputType", "value": $("#inputType").val()});
        arr.push({"name": "inputTypeConfig", "value": $("#inputTypeConfig").val()});
        arr.push({"name": "htmlInner", "value": $("#htmlInner").val()});
        arr.push({"name": "defaultValue", "value": jqInputs[6].value});
    } else if (num == 6) {//普通模式 不可修改 情况下
        arr.push({"name": "fieldChineseName", "value": jqInputs[1].value});
        arr.push({"name": "length", "value": jqInputs[2].value});
        arr.push({"name": "defaultValue", "value": jqInputs[4].value});
        arr.push({"name": "sortNo", "value": jqInputs[5].value});
        arr.push({"name": "inputType", "value": $("#inputType").val()});
        arr.push({"name": "inputTypeConfig", "value": $("#inputTypeConfig").val()});
        arr.push({"name": "htmlInner", "value": $("#htmlInner").val()});
    } else {//高级模式
        arr.push({"name": "fieldChineseName", "value": jqInputs[1].value});
        arr.push({"name": "defaultValue", "value": jqInputs[3].value});
        arr.push({"name": "sortNo", "value": jqInputs[4].value});
        arr.push({"name": "inputType", "value": $("#inputType").val()});
        arr.push({"name": "inputTypeConfig", "value": $("#inputTypeConfig").val()});
        arr.push({"name": "htmlInner", "value": $("#htmlInner").val()});
    }
    if ((num == 8 ) && (((fNum > 1) && (aData.fieldName == $("#fieldName").val())) || ((fNum > 0) && (aData.fieldName != $("#fieldName").val())))) {
        Dialog.alert("系统提示", "已存在过该表单项名称(后台内置表单项），请重置");
        nEditing = nRow;//校验不成功则继续保存
    } else if ((((cNum > 1) && (aData.fieldChineseName == $("#fieldChineseName").val())) || ((cNum > 0) && (aData.fieldChineseName != $("#fieldChineseName").val())))) {
        Dialog.alert("系统提示", "已存在该表单项显示名称，请重置");
    }
    else {
        if (!validate2($('#' + tableId))) {
            nEditing = nRow;//校验不成功则继续保存
        } else if (!validateDefaultValue(num == 8 ? jqInputs[1].value : aData.fieldName,
            num == 8 ? jqInputs[3].value : aData.dataType,
            num > 5 ? $("#itemLength").val() : aData.length,
            $("#fieldDefaultValue").val(),aData.length)  ) {
            validateDefaultValueMsg();
        }
        else {
            jQuery.ajax({
                async: false,
                type: "POST",
                url: springUrl + "/form/formItem_edit",
                data: arr,

                success: function (resp) {
                    isStdNew = false;
                    if (parseInt(resp) >= 1)
                        bol = true;
                }
            });
            oTable.fnDraw();
            nEditing = null;//校验成功
        }
    }

}
/**
 * 点击取消时触发，分页行数据恢复
 * @param oTable
 * @param nRow
 */
function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    switch (aData.dataType) {
        case "文本" :
            aData.dataType = "STRING";
            break;
        case "数值" :
            aData.dataType = "NUMBER";
            break;
        case "日期" :
            aData.dataType = "DATE";
            break;
        case "时间" :
            aData.dataType = "TIME";
            break;
        case "日期时间" :
            aData.dataType = "DATETIME";
            break;

    }
    oTable.fnUpdate(aData.fieldName, nRow, 1, false);
    oTable.fnUpdate(aData.fieldChineseName, nRow, 2, false);
    oTable.fnUpdate(aData.defaultValue, nRow, 6, false);
    oTable.fnUpdate(aData.dataType, nRow, 3, false);
    oTable.fnUpdate(aData.length, nRow, 4, false);
    oTable.fnUpdate(aData.sortNo, nRow, 7, false);
    oTable.fnUpdate(aData.inputType, nRow, 5, false);
    $('.edit_item', jqTds[8]).text('修改');
    $('.cancel_item', jqTds[8]).text('');

}

/**
 * 根据数据类型校验默认值格式
 * @param fieldName
 * @param dataType
 * @param lengthValue
 * @param defaultValue
 * @return {*}
 */
function validateDefaultValue(fieldName, dataType, lengthValue, defaultValue,oldLengthValue) {
    var reguValue = "";
    var reguLength = "";
    var reguInteger = "";
    var flagValue = false;
    var flagLength = false;
    var flagName = false;
    var flagInteger = false;
    switch (dataType) {
        case "STRING" :
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            //不输入非法字符且长度小于3000
            reguValue = /^[^\!\@\#\$\%\^\&\*]{0,3000}|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "文本格式默认值不合法，应小于等于3000字符，且无非法字符";
            }

            //不输入非法字符且长度值小于3000
            if (!isNaN(lengthValue) && lengthValue <= 3000 && lengthValue >= 1) {
                if(createType == "standardModule" && isUpdateMark == "false" && (oldLengthValue >lengthValue) ){
                    validateMsg = "长度不合法，应该大于原本长度 "+oldLengthValue +" ，否则会影响原有业务数据。";
                }else{
                    flagLength = true;
                }

            }else{
                validateMsg = "长度不合法，应为小于等于3000的正整数";
            }

//            if (!flagLength) {
//                validateMsg = "长度不合法，应为小于等于3000的正整数";
//            }
            return  (flagValue && flagLength && flagName);
            break;
        case "NUMBER" :
            validateMsg = "";
//           检验fieldName是否为内置字段
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
//            默认值校验
            reguValue = /^(-?\d+)(\.\d+)?$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "默认值不合法，应为整数或小数";
            }
//            检验长度(只有在标准模式下可以修改长度，否则无需校验）
            if (createType == "standardModule"){
                if (!isNaN(lengthValue) && (lengthValue > 100 || lengthValue < 1)) {
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                } else if (!isNaN(lengthValue) && lengthValue <= 100 && lengthValue >= 1) {
                    reguInteger = /^[1-9][0-9]*$/;
                    flagInteger = reguInteger.test(lengthValue);
                    if (flagInteger) {
                        if(isUpdateMark == "false"  &&oldLengthValue.length>0 &&oldLengthValue!=null){
                            if(( reguInteger.test(oldLengthValue))&& (oldLengthValue >lengthValue) ){
                                validateMsg =  "长度不合法，应该大于原本长度 "+oldLengthValue +" ，否则会影响原有业务数据。";
                            }else{
                                validateMsg =  "长度不合法，原有长度为 "+oldLengthValue +" ，会影响原有业务数据。";
                            }
                        }else{
                            flagLength = true;
                        }

                    }
                    else {
                        validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                    }
                } else if (isNaN(lengthValue)) {
                    reguInteger = /^[1-9][0-9]*$/;
                    reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                    flagLength = reguLength.test(lengthValue);
                    if (!flagLength) {
                        validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                    }
                    else {
                        var flag1 = false;
                        var flag2 = false;
                        reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                        flag1 = reguInteger.test(oldLengthValue);
                        flag2 = reguLength.test(oldLengthValue);
                        if ((lengthValue.split(",")[0] <= 100) && (lengthValue.split(",")[0] >= 1) && (parseInt(lengthValue.split(",")[0]) > parseInt(lengthValue.split(",")[1]))) {
                            if(isUpdateMark == "false"){//已有业务数据
                                if (oldLengthValue.length>0 &&oldLengthValue!=null
                                    &&(flag1 ||
                                    (flag2&& (parseInt(lengthValue.split(",")[0]) >= parseInt(oldLengthValue.split(",")[0]))
                                        && (parseInt(lengthValue.split(",")[1]) >= parseInt(oldLengthValue.split(",")[1]))) )) {
                                    flagLength = true;
                                } else {
                                    flagLength = false;
                                    validateMsg = "长度值不合法，更改的decimal格式（m,n)中m应不小于原有 "+oldLengthValue.split(",")[0]+"n不小于"+oldLengthValue.split(",")[1];
                                }
                            }else{//无业务数据可以随意修改
                                flagLength = true;
                            }
                        } else {
                            flagLength = false;
                            validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                        }
                    }
                }
            }else{
                flagLength = true;
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "DATE" :  //2004-04-30
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$|^\@.*|^$/
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "日期格式默认值不合法，格式应为2004-04-30，或@day";
            }
            return (flagValue && flagName);
            break;
        case "TIME" : //23:59
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /^(([1-9]{1})|([0-1][0-9])|([1-2][0-3])):([0-5][0-9])$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "时间格式默认值格式不合法，格式应为 23:59,或@date";
            }
            return (flagValue && flagName);
            break;
        case "DATETIME" :  //00-00-00 00:00:00 | 0000-00-00 00:00:00 | 09-05-22 08:16:00 | 1970-00-00 00:00:00 | 20090522081600
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /(\d{2}|\d{4})(?:\-)?([0]{1}\d{1}|[1]{1}[0-2]{1})(?:\-)?([0-2]{1}\d{1}|[3]{1}[0-1]{1})(?:\s)?([0-1]{1}\d{1}|[2]{1}[0-3]{1})(?::)?([0-5]{1}\d{1})(?::)?([0-5]{1}\d{1})|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "系统提示", "日期时间格式默认值格式不合法，格式应为1970-00-00 00:00:00，或@date";
            }
            return (flagValue && flagName);
            break;
        case "文本" :
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            //不输入非法字符且长度小于3000
            reguValue = /^[^\!\@\#\$\%\^\&\*]{0,3000}|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "文本格式默认值不合法，应小于等于3000字符，且无非法字符";
            }

            //不输入非法字符且长度值小于3000
            if (!isNaN(lengthValue) && lengthValue <= 3000 && lengthValue >= 1) {
                if(createType == "standardModule" && isUpdateMark == "false" && (oldLengthValue >lengthValue) ){
                    validateMsg = "长度不合法，应该大于原本长度 "+oldLengthValue +" ，否则会影响原有业务数据。";
                }else{
                    flagLength = true;
                }

            }else{
                validateMsg = "长度不合法，应为小于等于3000的正整数";
            }

//            if (!flagLength) {
//                validateMsg = "长度不合法，应为小于等于3000的正整数";
//            }
            return  (flagValue && flagLength && flagName);
            break;
        case "数值" :
            validateMsg = "";
//           检验fieldName是否为内置字段
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
//            默认值校验
            reguValue = /^(-?\d+)(\.\d+)?$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "默认值不合法，应为整数或小数";
            }
//            检验长度(只有在标准模式下可以修改长度，否则无需校验）
            if (createType == "standardModule"){
            if (!isNaN(lengthValue) && (lengthValue > 100 || lengthValue < 1)) {
                validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
            } else if (!isNaN(lengthValue) && lengthValue <= 100 && lengthValue >= 1) {
                reguInteger = /^[1-9][0-9]*$/;
                flagInteger = reguInteger.test(lengthValue);
                if (flagInteger) {
                    if(isUpdateMark == "false"  &&oldLengthValue.length>0 &&oldLengthValue!=null){
                        if(( reguInteger.test(oldLengthValue))&& (oldLengthValue >lengthValue) ){
                            validateMsg =  "长度不合法，应该大于原本长度 "+oldLengthValue +" ，否则会影响原有业务数据。";
                        }else{
                            validateMsg =  "长度不合法，原有长度为 "+oldLengthValue +" ，会影响原有业务数据。";
                        }
                    }else{
                        flagLength = true;
                    }

                }
                else {
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
            } else if (isNaN(lengthValue)) {
                reguInteger = /^[1-9][0-9]*$/;
                reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                flagLength = reguLength.test(lengthValue);
                if (!flagLength) {
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
                else {
                    var flag1 = false;
                    var flag2 = false;
                    reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                    flag1 = reguInteger.test(oldLengthValue);
                     flag2 = reguLength.test(oldLengthValue);
                    if ((lengthValue.split(",")[0] <= 100) && (lengthValue.split(",")[0] >= 1) && (parseInt(lengthValue.split(",")[0]) > parseInt(lengthValue.split(",")[1]))) {
                        if(isUpdateMark == "false"){//已有业务数据
                            if (oldLengthValue.length>0 &&oldLengthValue!=null
                                &&(flag1 ||
                                (flag2&& (parseInt(lengthValue.split(",")[0]) >= parseInt(oldLengthValue.split(",")[0]))
                                    && (parseInt(lengthValue.split(",")[1]) >= parseInt(oldLengthValue.split(",")[1]))) )) {
                                flagLength = true;
                            } else {
                                flagLength = false;
                                validateMsg = "长度值不合法，更改的decimal格式（m,n)中m应不小于原有 "+oldLengthValue.split(",")[0]+"n不小于"+oldLengthValue.split(",")[1];
                            }
                        }else{//无业务数据可以随意修改
                            flagLength = true;
                        }
                    } else {
                        flagLength = false;
                        validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                    }
                }
            }
            }else{
                flagLength = true;
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "日期" :  //2004-04-30
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$|^\@.*|^$/
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "日期格式默认值不合法，格式应为2004-04-30，或@day";
            }
            return (flagValue && flagName);
            break;
        case "时间" : //23:59
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /^(([1-9]{1})|([0-1][0-9])|([1-2][0-3])):([0-5][0-9])$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "时间格式默认值格式不合法，格式应为 23:59,或@date";
            }
            return (flagValue && flagName);
            break;
        case "日期时间" :  //00-00-00 00:00:00 | 0000-00-00 00:00:00 | 09-05-22 08:16:00 | 1970-00-00 00:00:00 | 20090522081600
            validateMsg = "";
            if (fieldName.toLowerCase() == "id" || fieldName.toLowerCase() == "yn" || fieldName.toLowerCase() == "create_time" || fieldName.toLowerCase() == "creator"
                || fieldName.toLowerCase() == "modify_time" || fieldName.toLowerCase() == "modifier" || fieldName.toLowerCase() == "parent_id") {
                validateMsg = "表单项名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else {
                flagName = true;
            }
            reguValue = /(\d{2}|\d{4})(?:\-)?([0]{1}\d{1}|[1]{1}[0-2]{1})(?:\-)?([0-2]{1}\d{1}|[3]{1}[0-1]{1})(?:\s)?([0-1]{1}\d{1}|[2]{1}[0-3]{1})(?::)?([0-5]{1}\d{1})(?::)?([0-5]{1}\d{1})|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if (!flagValue) {
                validateMsg = "系统提示", "日期时间格式默认值格式不合法，格式应为1970-00-00 00:00:00，或@date";
            }
            return (flagValue && flagName);
            break;
    }
}
/**
 * 根据数据类型校验默认值格式提示信息
 * @return {*}
 */
function validateDefaultValueMsg() {
    Dialog.alert("系统提示", validateMsg);
}


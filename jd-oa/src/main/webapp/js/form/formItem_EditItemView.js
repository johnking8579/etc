/**
 * Created with IntelliJ IDEA.
 * Desctription:表单字段配置IFRAME JS
 * User: yujiahe
 * Date: 13-9-17
 * Time: 上午11:03
 * To change this template use File | Settings | File Templates.
 */

//是否正在编辑
//var nEditing = null;

var selectedColums = [];
var selectedSubViewColums = [];
var selectedSubColums = [];

$(function(){
	$('tr.parent').click(function(){   // 获取所谓的父行
		$(this).toggleClass("selected")   // 添加/删除高亮
		.siblings('.child_'+this.id).toggle();  // 隐藏/显示所谓的子行
	});
	
	selectedColums=configColumns;
	selectedSubViewColums=configSubViewColumns;
	selectedSubColums=configSubColumns;
	printSelectedColumns(selectedColums,selectedSubViewColums);
	
	$("input[type='checkbox']",$('#mainFormItemList')).click(function(){
    	var status = $(this).parent().next().text();
    	var formType = $(this).attr("formType");
    	var formCode = $(this).attr("formCode");
    	if($(this).attr("checked")=="checked"){
    		if(formType == 'M'){
    			selectedColums.push(status);
    		}else{
    			selectedSubViewColums.push(status);
    			selectedSubColums.push(formCode+":"+status);
    		}
    		
    	}else{
    		if(formType == 'M'){
    			var i = $.inArray(status,selectedColums);
                if(i>=0)
                    selectedColums.splice(i,1);
    		}else{
    			var j = $.inArray(status,selectedSubViewColums);
    			if(j>=0){
    				selectedSubViewColums.splice(j,1);
    				selectedSubColums.splice(j,1);
    			}	
    		}
    	}
    	printSelectedColumns(selectedColums,selectedSubViewColums);
    });
    
    $("input[type='checkbox']",$('#mainFormItemList')).each(function(){
    	var status = $(this).parent().next().text();
    	if($.inArray(status,configColumns)>=0 || $.inArray(status,selectedSubViewColums)>=0){
    		 $(this).attr("checked","checked");
    	}
    });
})

function printSelectedColumns(arr,subarr){
    if(arr.length<=0){
        $("#selectedColums").html("请选择要显示的列：");
        return;
    }
    var str = [];
    str.push("显示顺序（按先后顺序）：");
    $.each(arr,function(index,value){
        str.push('【');
        str.push(value);
        str.push('】');
    });
    $("#selectedColums").html(str.join(''));
    
    if(subarr.length<=0){
        $("#selectedSubColums").html("");
        return;
    } 
    var subStr = [];
    subStr.push("子表字段：");
    $.each(subarr,function(index,value){
    	subStr.push('【');
    	subStr.push(value);
    	subStr.push('】');
    });
    $("#selectedSubColums").html(subStr.join(''));
}


//$(function () {
//    setFormPage("formId", "mainFormItemList"); //主表分页显示
//});
/**
 * 分页配置及显示（主表、子表）
 * @param FId       对应相应主表或者子表的formId
 * @param tableId   对应页面上table的ID
 */

/*function setFormPage(FId, tableId) {
    var options = Table.options;
    options = {
        "remember_status":false,
        "callback_fn": function () {

            $("input[name='all_chk']",$('#mainForm')).css("display","none");

            selectedColums=configColumns;
            printSelectedColumns(selectedColums);

            $("tbody input[type='checkbox']",$('#mainFormItemList')).click(function(){
                var status = $(this).parent().next().text();
                if($(this).attr("checked")=="checked"){
                    selectedColums.push(status);
                }else{
                    var i = $.inArray(status,selectedColums);
                    if(i>=0)
                        selectedColums.splice(i,1);

                }
                printSelectedColumns(selectedColums);

            });

            $("tbody input[type='checkbox']",$('#mainFormItemList')).each(function(){
                var status = $(this).parent().next().text();
                if($.inArray(status,configColumns)>=0){
                    $(this).attr("checked","checked");
                }
            });

        },
        isPaging: false,
        useCheckbox: true,
        pageUrl: "formItem_mainFormPage",
        defaultSort: [ [3, "asc"]],
        sendData: function (sSource, aoData, fnCallback) {
            var formId = $("input#" + FId).val();
            aoData.push({
                "name": "formId",
                "value": formId
            },{
                "name": "computeExpressDesc",
                "value": ''
            });
            jQuery.ajax({
                type: "POST",
                url: springUrl +"/form/formItem_mainFormPage",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    nEditing=null;
                    fnCallback(resp);
                }
            });
        },
        // 分页列设置
        columns: [
            { "mData": "fieldName", "sTitle": "字段名称", "bSortable": false, "sClass": "my_class","sWidth": "60px"},
            { "mData": "fieldChineseName", "sTitle": "字段显示名称", "bSortable": false, "sClass": "my_class","sWidth": "100px"},
            { "mData": "sortNo", "sTitle": "显示顺序", "bSortable": true, "sClass": "my_class", "sWidth": "90px","bVisible":false}
        ],
        
        // 分页按钮设置
        btns: []
    };
    //动态生成table名称 由table+FId 拼接
    eval('table' + FId + '=Table.dataTable(tableId, options);');

}*/


//根据条件选中相应的元素
/*function findCheckboxByName(name){
    var item = null;
    $("tbody input[type='checkbox']",$('#mainFormItemList')).each(function(){
        var status = $(this).parent().next().text();
        if(status == name){
            item = $(this);
        }

    });

    return  item;
}

function printSelectedColumns(arr){
    if(arr.length<=0){
        $("#selectedColums").html("请选择要显示的列：");
        return;
    }
    var str = [];
    str.push("显示顺序（按先后顺序）：");
    $.each(arr,function(index,value){
        str.push('【');
        str.push(value);
        str.push('】');
    });
    $("#selectedColums").html(str.join(''));
}*/




/**
 * 数据字典-数据类别新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

function addHiddenParent(){
    $("#isShowAddTable").hide();
}
function addShowParent(){
    $("#isShowAddTable").show();
}
/**
 * 初始化操作
 */
$(function() {
	bindValidate($('#businessTableForm'));
    if($("#tableId").val()!=""){
        $("#tableTypeAddBak").attr("checked",true);
        $("#tableTypeAdd").attr("checked",false);
        $("#tableTypeAddBak").attr("disabled",true);
        $("#tableTypeAdd").attr("disabled",true);
        $("#parentId").val($("#tableId").val());
        $("#parentId").attr("disabled",true);
    }
    else{
        if($("#tableTypeAdd").attr("checked")=="checked"){
            $("#isShowAddTable").hide();
        }
        else{
            $("#isShowAddTable").show();
        }
    }
});

/**
 * Created with IntelliJ IDEA.
 * Desctription:
 * User: yujiahe
 * Date: 13-9-15
 * Time: 上午11:21
 * To change this template use File | Settings | File Templates.
 */
/**
 * 保存变更所有人
 */
function updateFormOwner(){

        var editFormId = $('#editFormId').val();
        //新增数据项
        jQuery.ajax({
            type: "POST",
            cache: false,
            dataType: 'json',
            data: {
                ids: $('#ids').val(),
                owner: $("#updateOwner").val()
            },
            url: springUrl + "/form/formOwner_updateSave",
            success: function (data) {
                if (data.operator == true) {
                    Dialog.alert("失败", data.message);
                }else{
                    Dialog.hide();
                    Table.render(formTable);
                    Dialog.alert("系统提示", data.message);
                }
            }
//           , error: function (data) {
//                if (data.responseText == null) {
//                    Dialog.alert("失败", "变更所有人失败");
//                } else {
//                    Dialog.alert("失败", data.responseText);
//                }
//            }
        });

}

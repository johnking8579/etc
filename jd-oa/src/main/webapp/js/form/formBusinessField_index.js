/**
 * 数据字典-数据项列表页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var editFlag = 0;
var table;
var options = Table.options;
var nEditing = null;
var validateMsg = "";
options = {
    pageUrl: springUrl + "/form/formBusinessField_findPage",
    useCheckbox: true,
    isPaging: false,
    scrollY: "100%",
    defaultSort: [
        [8, "asc"]
    ],
    sendData: function (sSource, aoData, fnCallback) {
        var tableId = $("#idInfo").val();
        if ($("#idInfo").val() == null) tableId = "";
        aoData.push({"name": "tableId", "value": tableId});
        jQuery.ajax({
            type: "POST",
            url: sSource,
            dataType: "json",
            data: aoData,
            success: function (json) {
                fnCallback(json);
            }
        });
    },
    columns: [
        { "mData": "fieldName", "sTitle": "<font color=red>*</font>字段名称", "bSortable": false, "bVisible": true, "sWidth": 80},
        { "mData": "fieldChineseName", "sTitle": "<font color=red>*</font>字段显示名称", "bSortable": false, "sClass": "my_class", "sWidth": 80},
        { "mData": "dataType", "sTitle": "<font color=red>*</font>数据库类型", "bSortable": false, "sClass": "my_class", "sWidth": 80, fnRender: function (obj) {
            var dataTypeDisplay;
            switch (obj.aData.dataType) {
                case "STRING" :
                    dataTypeDisplay = "文本";
                    break;
                case "NUMBER" :
                    dataTypeDisplay = "数值";
                    break;
                case "DATE" :
                    dataTypeDisplay = "日期";
                    break;
                case "TIME" :
                    dataTypeDisplay = "时间";
                    break;
                case "DATETIME" :
                    dataTypeDisplay = "日期时间";
                    break;
                default:
                    dataTypeDisplay = "";
                    break;
            }
            return dataTypeDisplay;
        }},
        { "mData": "length", "sTitle": "长度", "bSortable": false, "sClass": "my_class", "sWidth": 60},
        { "mData": "isNull", "sTitle": "<font color=red>*</font>是否为空", "bSortable": false, "sClass": "my_class", "sWidth": 60, fnRender: function (obj) {
            var isNullDisplay;
            switch (obj.aData.isNull) {
                case '0' :
                    isNullDisplay = "是";
                    break;
                case '1' :
                    isNullDisplay = "否";
                    break;
                default:
                    isNullDisplay = "";
                    break;
            }
            return isNullDisplay;
        }},
        { "mData": "inputType", "sTitle": "UI类型", "bSortable": false, "sClass": "my_class", "sWidth": 100 },
        { "mData": "defaultValue", "sTitle": "默认值", "bSortable": false, "sClass": "my_class", "sWidth": 60},
        { "mData": "sortNo", "sTitle": "<font color=red>*</font>显示顺序", "bSortable": false, "sClass": "my_class", "sWidth": 60},
        {"mData": "id", "sTitle": "操作", "bSortable": false, "sWidth": 55, "mRender": function (data, type, full) {
            if (data == "init") {
                return "";
            }
            else {
//                if ($("#flag").val() == 'true') {
//                    return "";
//                }
//                else {
                    return "<a href='#'  class='edit' onclick='return false;'>修改</a>  " +
                        " <a href='#' title='取消' class='cancel_item'></a> " +
                        "<a href='#' onclick='deleteTableField(\"" + data + "\",$(this));'/>删除</a>";
//                }
            }
        }}
    ],
    btns: [
        {
            "sExtends": "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success hiddenBtn",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                addRow(table);
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "删除",
            "sButtonClass": "btn btn-success hiddenBtn",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                batchDeleteTableField();
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "生成业务表",
            "sButtonClass": "btn btn-success hiddenBtn",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                createBusinessTable();
            }
        },
        {
            "sExtends": "text",
            "sButtonText": "返回",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function (nButton, oConfig, oFlash) {
                goBackToTable();
            }
        }
    ]
};


function createBusinessTable() {
    var businessTableId = $("#idInfo").val();
    Dialog.confirm("确认", "您确定生成业务表?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: springUrl + "/form/formBusinessField_createBusinessTable",
                data: {
                    businessTableId: businessTableId
                },
                success: function (data) {
                    if (data.operator == false)
                        Dialog.alert("失败！", data.message);
                    else {
                        Dialog.alert("成功！", data.message);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        }
    });
}


function editInputType(id, name, title, inputType) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
                Dialog.hide();
            }
        },
        {
            "label": "确定",
            "class": "btn-success",
            "callback": function () {
                var configInputTypeConfig = document.getElementById('formuicomponentIframe').contentWindow.returnInputTypeConfig();
                $("#inputTypeConfig").val(configInputTypeConfig.inputTypeConfig);
                $("#inputType").val(configInputTypeConfig.inputType);
            }
        }
    ];
    var dialogTitle = '配置表单控件';
    if (title != '') {
        dialogTitle = title + '【' + name + '】';
    }
    Dialog.openRemote(dialogTitle, 'filed_item_ui_design_index?fieldId=' + id + "&inputType=" + inputType + "&isFormDesign=false", 800, 400, buttons);
}

$(function () {
    bindValidate($('#businessFieldForm'));

    table = Table.dataTable("formBusinessField", options);
    //是否正在编辑

    bindEditByTable(table);
    //绑定修改按钮
    $('#formBusinessField a.edit').live('click', function (e) {
        var nRow = $(this).parents('tr')[0];
        if (nEditing !== null && nEditing != nRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRow(table, nEditing);
            editRow(table, nRow);
            nEditing = nRow;
        }
        else if (nEditing == nRow && this.innerHTML == "保存") {
            /* This row is being edited and should be saved */
            saveRow(table, nEditing);
        }
        else {
            /* No row currently being edited */
            editRow(table, nRow);
            nEditing = nRow;
        }
    });
//    if ($("#flag").val() == 'true') {
//        $(".hiddenBtn").hide();
//    }
});


//修改
function editRow(oTable, nRow) {
    if (nEditing == null) {
    } else {
        var currentData = oTable.fnGetData(nEditing);
        if (currentData.id == null || currentData.id == "init") {
            var oSettings = oTable.fnSettings();
            oSettings.oFeatures.bServerSide = false;
            oTable.fnDeleteRow(nEditing);
            oSettings.oFeatures.bServerSide = true;
        } else {
            restoreRow(oTable, nEditing);
        }
    }
    nEditing = nRow;
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nEditing);
    var dtype = aData.dataType;
    var s = '';
    var i = '';
    var d = '';
    var t = '';
    var dt = '';
    if ("文本" == dtype) {
        s = ' selected="selected"';
    } else if ("数值" == dtype) {
        i = ' selected="selected"';
    } else if ("日期" == dtype) {
        d = ' selected="selected"';
    } else if ("时间" == dtype) {
        t = ' selected="selected"';
    } else if ("日期时间" == dtype) {
        dt = ' selected="selected"';
    }
    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        data: {
            formBusinessFieldId: aData.id
        },
        url: springUrl + "/form/formBusinessField_isUsedByForm",
        success: function (data) {
            if (data.operator == false) {//没有被表单引用
                editFlag = 1;
                var chineseNameDisplay = (aData.fieldChineseName == null ? " " : aData.fieldChineseName);
                jqTds[2].innerHTML = '<input name="jq2" style="width:' + ((jqTds[2].clientWidth - 16 - 18) * 100 / (jqTds[2].clientWidth - 16)) + '%" value="' + chineseNameDisplay + '" type="text" required data-rule="required;length[~200]"/>';
                var inputTypeDisplay = (aData.inputType == null ? " " : aData.inputType);
                jqTds[6].innerHTML = "<span style='cursor:pointer,width:" + ((jqTds[6].clientWidth - 16 - 18) * 100 / (jqTds[6].clientWidth - 16)) + "%' onclick=\"editInputType('" + aData.id + "','" + aData.fieldName + "','" + aData.fieldChineseName + "','" + aData.inputType + "');\">" + '<input disabled id="inputType" style="width:70px" value="' + inputTypeDisplay + '" type="text" />' + "<img src='" + springUrl + "/static/common/img/edit.png' alt='组件' />" + "</span>";
                var lengthDisplay = (aData.length == null ? " " : aData.length);

                if((aData.length.indexOf(",") < 0)){//长度为整数 则必须输入比它大的整数
                    jqTds[4].innerHTML = '<input id="editLength" name="jq4" style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" value="' + lengthDisplay + '" type="text" data-rule="integer;range[' + aData.length + '~]" />';
                }else{
                    //浮点数暂时不支持修改
                }
//                jqTds[4].innerHTML = '<input id="editLength" name="jq4" style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" value="' + lengthDisplay + '" type="text" />';

                var defaultValueDisplay = (aData.defaultValue == null ? " " : aData.defaultValue);
                jqTds[7].innerHTML = '<input name="jq5" style="width:' + ((jqTds[7].clientWidth - 16 - 18) * 100 / (jqTds[7].clientWidth - 16)) + '%" value="' + defaultValueDisplay + '" type="text" />';
                var sortNoDisplay = (aData.sortNo == null ? " " : aData.sortNo);
                jqTds[8].innerHTML = '<input  name="jq7" style="width:' + ((jqTds[8].clientWidth - 16 - 18) * 100 / (jqTds[8].clientWidth - 16)) + '%" value="' + sortNoDisplay + '" type="text" required data-rule="required; integer; range[0~]"/>';
                if (dtype == "文本" || dtype == "数值") {
                    $("#editLength").removeAttr("disabled");
                } else {
                    $("#editLength").attr({"disabled": "disabled"});
                }
                if (aData.id == "init") {
                    jqTds[9].innerHTML = "<a href='#'  class='edit'>保存</a>  " + "<a href='#' onclick='deleteTableField(\"" + data + "\",$(this));'/>删除</a>";
                }
                else {
                    $('.edit', jqTds[9]).text('保存');
                    $('.cancel_item', jqTds[9]).text('取消');
                }
            }
            else {//已经被表单引用
                editFlag = 0;
                var nameDisplay = (aData.fieldName == null ? " " : aData.fieldName);
                jqTds[1].innerHTML = '<input name="jq1" style="width:' + ((jqTds[1].clientWidth - 16 - 18) * 100 / (jqTds[1].clientWidth - 16)) + '%" value="' + nameDisplay + '" type="text" required data-rule="required;nochineseandspecial;lettersFirst;length[~32]"/>';
                var chineseNameDisplay = (aData.fieldChineseName == null ? " " : aData.fieldChineseName);
                jqTds[2].innerHTML = '<input name="jq2" style="width:' + ((jqTds[2].clientWidth - 16 - 18) * 100 / (jqTds[2].clientWidth - 16)) + '%" value="' + chineseNameDisplay + '" type="text" required data-rule="required;length[~200]"/>';

                jqTds[3].innerHTML = '<select id="selectType" name="jq3" style="width:' + ((jqTds[3].clientWidth - 16 ) * 100 / (jqTds[3].clientWidth - 16)) + '%" onchange="disLength()" data-rule="required">' + '<option value="STRING"' + s + '>文本' + '</option>' +
                    '<option value="NUMBER"' + i + '>数值' + '</option>' + '<option value="DATE"' + d + '>日期' + '</option>' + '<option value="TIME"' + t + '>时间' + '</option>' +
                    '<option value="DATETIME"' + dt + '>日期时间' + '</option></select>';

                var lengthDisplay = (aData.length == null ? " " : aData.length);
                jqTds[4].innerHTML = '<input id="editLength" name="jq4" style="width:' + ((jqTds[4].clientWidth - 16 - 18) * 100 / (jqTds[4].clientWidth - 16)) + '%" value="' + lengthDisplay + '" type="text" />';
                var defaultValueDisplay = (aData.defaultValue == null ? " " : aData.defaultValue);
                jqTds[7].innerHTML = '<input name="jq5" style="width:' + ((jqTds[7].clientWidth - 16 - 18) * 100 / (jqTds[7].clientWidth - 16)) + '%" value="' + defaultValueDisplay + '" type="text" />';
                var nullFlag = aData.isNull;
                var yFlag = '';
                var nFlag = '';
                if (nullFlag == '是') {
                    yFlag = ' selected="selected"';
                }
                else if (nullFlag == '否') {
                    nFlag = ' selected="selected"';
                }
                jqTds[5].innerHTML = '<select  name="jq6" style="width:' + ((jqTds[5].clientWidth - 16 - 18) * 100 / (jqTds[5].clientWidth - 16)) + '%" data-rule="required">' + '<option value="0"' + yFlag + '>是' + '</option><option value="1"' + nFlag + '>否' + '</option></select>';
                var inputTypeDisplay = (aData.inputType == null ? " " : aData.inputType);
                jqTds[6].innerHTML = "<span style='cursor:pointer,width:" + ((jqTds[6].clientWidth - 16 - 18) * 100 / (jqTds[6].clientWidth - 16)) + "%' onclick=\"editInputType('" + aData.id + "','" + aData.fieldName + "','" + aData.fieldChineseName + "','" + aData.inputType + "');\">" + '<input disabled id="inputType" style="width:70px" value="' + inputTypeDisplay + '" type="text" />' + "<img src='" + springUrl + "/static/common/img/edit.png' alt='组件' />" + "</span>";
                var sortNoDisplay = (aData.sortNo == null ? " " : aData.sortNo);
                jqTds[8].innerHTML = '<input  name="jq7" style="width:' + ((jqTds[8].clientWidth - 16 - 18) * 100 / (jqTds[8].clientWidth - 16)) + '%" value="' + sortNoDisplay + '" type="text" required data-rule="required; integer; range[0~]"/>';
                if (aData.id == "init") {
                    jqTds[9].innerHTML = "<a href='#'  class='edit'>保存</a>  " + "<a href='#' onclick='deleteTableField(\"" + data + "\",$(this));'/>删除</a>";
                }
                else {
                    $('.edit', jqTds[9]).text('保存');
                }
                $("#inputTypeConfig").val(aData.inputTypeConfig);
                setDefalutValue();
            }
        }
    });
}

//保存
function saveRow(oTable, nRow) {
    if (!validate2($('#businessFieldForm'))) {
        nEditing = nRow;
        return false;
    }
    var jqElements = $('input,select', nRow);
    arr = [];
    //收集数据
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    if (aData.id == "init") {
        if (!validateDefaultValue(jqElements[1].value,jqElements[3].value,jqElements[4].value, jqElements[7].value)) {
            validateDefaultValueMsg();
        }
        else{
            jQuery.ajax({
                type: "POST",
                cache: false,
                dataType: 'json',
                data: {
                    fieldName: jqElements[1].value,
                    fieldChineseName: jqElements[2].value,
                    tableId: $('#idInfo').val()
                },
                url: springUrl + "/form/formBusinessField_isExistField",
                success: function (data) {
                    if (data.operator == true) {
                        Dialog.alert("失败", data.message);
                        nEditing = nRow;
                        return false;
                    }
                    else {
                        jQuery.ajax({
                            type: "POST",
                            cache: false,
                            async: false,
                            url: springUrl + "/form/formBusinessField_addSave",
                            data: {
                                fieldName: jqElements[1].value,
                                fieldChineseName: jqElements[2].value,
                                dataType: jqElements[3].value,
                                length: jqElements[4].value,
                                defaultValue: jqElements[7].value,
                                isNull: jqElements[5].value,
                                sortNo: jqElements[8].value,
                                inputTypeConfig: $("#inputTypeConfig").val(),
                                inputType: $("#inputType").val(),
                                tableId: $("#idInfo").val()
                            },
                            success: function (resp) {
                                nEditing = null;
                                oTable.fnDraw();
                                if (parseInt(resp) >= 1)
                                    bol = true;
                            }
                        });
                    }
                }
            });
        }
    }
    else {
        if (editFlag == 0) {   //全部可修改状态
            if (!validateDefaultValue(jqElements[1].value,jqElements[3].value,jqElements[4].value, jqElements[7].value)) {
                validateDefaultValueMsg();
            }
            else{
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    dataType: 'json',
                    data: {
                        id: aData.id,
                        fieldName: jqElements[1].value,
                        fieldChineseName: jqElements[2].value,
                        tableId: $('#idInfo').val()
                    },
                    url: springUrl + "/form/formBusinessField_isExistField",
                    success: function (data) {
                        if (data.operator == true) {
                            Dialog.alert("失败", data.message);
                            nEditing = nRow;
                            return false;
                        }
                        else {
                            //后台修改
                            jQuery.ajax({
                                type: "POST",
                                cache: false,
                                async: false,
                                url: springUrl + "/form/formBusinessField_updateSave",
                                data: {
                                    id: aData.id,
                                    fieldName: jqElements[1].value,
                                    fieldChineseName: jqElements[2].value,
                                    dataType: jqElements[3].value,
                                    length: jqElements[4].value,
                                    defaultValue: jqElements[7].value,
                                    isNull: jqElements[5].value,
                                    sortNo: jqElements[8].value,
                                    inputTypeConfig: $("#inputTypeConfig").val(),
                                    inputType: $("#inputType").val()
                                },
                                success: function (resp) {
                                    nEditing = null;
                                    oTable.fnDraw();
                                    if (parseInt(resp) >= 1)
                                        bol = true;
                                }
                            });
                        }
                    }
                });
            }
        }
        else {     //部分可修改状态
            if (!validateDefaultValue(aData.fieldName,aData.dataType,aData.length, jqElements[2].value)) {
                validateDefaultValueMsg();
            }
            else{
                //后台修改
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    async: false,
                    url: springUrl + "/form/formBusinessField_updateSave",
                    data: {
                        id: aData.id,
                        inputTypeConfig: $("#inputTypeConfig").val(),
                        inputType: $("#inputType").val(),
                        defaultValue: jqElements[2].value,
                        sortNo: jqElements[3].value
                    },
                    success: function (resp) {
                        nEditing = null;
                        oTable.fnDraw();
                        if (parseInt(resp) >= 1)
                            bol = true;
                    }
                });
            }
        }
    }
}

//
function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var dataTypeDisplay;
    switch (aData.dataType) {
        case "文本" :
            dataTypeDisplay = "STRING";
            break;
        case "数值" :
            dataTypeDisplay = "NUMBER";
            break;
        case "日期" :
            dataTypeDisplay = "DATE";
            break;
        case "时间" :
            dataTypeDisplay = "TIME";
            break;
        case "日期时间" :
            dataTypeDisplay = "DATETIME";
            break;
        default:
            dataTypeDisplay = "";
            break;
    }
    var isNullDisplay;
    switch (aData.isNull) {
        case "是" :
            isNullDisplay = "0";
            break;
        case "否" :
            isNullDisplay = "1";
            break;
        default:
            isNullDisplay = "";
            break;
    }
    oTable.fnUpdate(aData.fieldName, nRow, 1, false);
    oTable.fnUpdate(aData.fieldChineseName, nRow, 2, false);
    oTable.fnUpdate(dataTypeDisplay, nRow, 3, false);
    oTable.fnUpdate(aData.length, nRow, 4, false);
    oTable.fnUpdate(aData.defaultValue, nRow, 7, false);
    oTable.fnUpdate(isNullDisplay, nRow, 5, false);
    oTable.fnUpdate(aData.inputType, nRow, 6, false);
    oTable.fnUpdate(aData.sortNo, nRow, 8, false);
    $('.edit', jqTds[9]).text('修改');
    $('.cancel_item', jqTds[9]).text('');
}


function addRow(oTable) {
    editFlag = 0;
    var arr = [];
    arr.push({"id": "init", "fieldName": "", "fieldChineseName": "", "dataType": "", "length": "", "isNull": "", "inputType": "", "defaultValue": "", "sortNo": ""});
    var oSettings = oTable.fnSettings();
    oSettings.oFeatures.bServerSide = false;
    var aiNew = oTable.fnAddData(arr);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    editRow(oTable, nRow);
    oSettings.oFeatures.bServerSide = true;
}

function bindEditByTable(oTable) {

    //绑定编辑按钮按钮
    $('edit').live('click', function () {
        var nRow = $(this).parents('tr')[0];
        if (nEditing == nRow && this.innerHTML == "保存") {
            /* This row is being edited and should be saved */
            saveRow(table, nRow);
            nEditing = null;
        } else {
            editRow(table, nRow);
        }
    });

    $(' a.cancel_item').live('click', function (e) {
        e.preventDefault();
        restoreRow(table, nEditing);
    });
}


function deleteTableField(id, el) {
    var nRow = el.parents('tr')[0];
    var aData = table.fnGetData(nRow);
    if (aData.id == "init") { //未保存到数据库不走后台删除
        var oSettings = table.fnSettings();
        oSettings.oFeatures.bServerSide = false;
        table.fnDeleteRow(nRow);
        oSettings.oFeatures.bServerSide = true;
        nEditing = null;
    } else {//走后台删除
        Dialog.confirm("确认", "确定删除数据表字段?", "是", "否", function (result) {
            if (result) {
                jQuery.ajax({
                    type: "post",
                    dataType: "json",
                    url: springUrl + "/form/formBusinessField_delete",
                    data: {
                        ids: id
                    },
                    success: function (data) {
                        if (data.operator == false)
                            Dialog.alert("失败！", data.message);
                        else {
                            Dialog.alert("成功！", data.message, "确定", function call() {
                                window.location.href = springUrl + "/form/formBusinessField_index?id=" + $("#idInfo").val();
                            });
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
            }
        });
    }
}

function batchDeleteTableField() {
    var idsInfo = Table.getSelectedRowsIDs(table);
    var idsInfoLength = idsInfo.length;
    var ids = new Array();
    if (idsInfo[idsInfoLength - 1] == "init") {
        for (var i = 0; i < idsInfoLength - 1; i++) {
            ids.push(idsInfo[i]);
        }
    }
    else {
        ids = idsInfo;
    }
    var length = ids.length;
    ids = ids.toString();
    if (idsInfoLength == 0) {
        Dialog.alert("消息提示", "没有选中项", "确定");
    } else {
        Dialog.confirm("确认", "确定批量删除?", "是", "否", function (result) {
            if (result) {
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    url: springUrl + "/form/formBusinessField_delete",
                    data: {
                        ids: ids,
                        length: length
                    },
                    success: function (data) {
                        if (data.operator == false)
                            Dialog.alert("失败！", data.message);
                        else {
                            Dialog.alert("成功！", data.message, "确定", function call() {
                                window.location.href = springUrl + "/form/formBusinessField_index?id=" + $("#idInfo").val();
                            });
                        }
                    }
                });
            }
        });
    }
}

function goBackToTable() {
    if ($("#tableType").val() == "m") {
        window.location.href = springUrl + "/form/formBusinessTable_index?tableId=" + $("#tableId").val() + "&tableTypeFlag=" + $("#tableType").val();
    }
    else {
        window.location.href = springUrl + "/form/formBusinessTable_index?tableId=" + $("#tableId").val() + "&tableTypeFlag=" + $("#tableType").val();
    }
}

function disLength() {
    var itemDataType = $("#selectType").val();
    if (itemDataType == 'NUMBER') {
        $("#editLength").val("10");
        $("#editLength").removeAttr("disabled");
    }
    else if (itemDataType == 'STRING') {
        $("#editLength").val("50");
        $("#editLength").removeAttr("disabled");
    }
    else {
        $("#editLength").attr({"disabled": "disabled"});
        $("#editLength").val("");
    }
}
function setDefalutValue(){
    var itemDataType = $("#selectType").val();
    if (itemDataType == 'NUMBER') {
        $("#editLength").removeAttr("disabled");
    }
    else if (itemDataType == 'STRING') {
        $("#editLength").removeAttr("disabled");
    }
    else {
        $("#editLength").attr({"disabled": "disabled"});
        $("#editLength").val("");
    }
}
/**
 * 根据数据类型校验默认值格式
 * @param fieldName
 * @param dataType
 * @param lengthValue
 * @param defaultValue
 * @return {*}
 */
function validateDefaultValue(fieldName,dataType, lengthValue,defaultValue) {
    var reguValue = "" ;
    var reguLength = "";
    var reguInteger = "";
    var flagValue = false;
    var flagLength = false;
    var flagName = false;
    var flagInteger = false;
    switch (dataType) {
        case "STRING" :
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            //不输入非法字符且长度小于3000
            reguValue = /^[^\!\@\#\$\%\^\&\*]{0,3000}|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "文本格式默认值不合法，应小于等于3000字符，且无非法字符";
            }
            //不输入非法字符且长度值小于3000
            if(!isNaN(lengthValue) && lengthValue <= 3000 && lengthValue >=1){
                flagLength = true;
            }
            if(!flagLength){
                validateMsg = "长度不合法，应为小于等于3000的正整数";
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "NUMBER" :
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^(-?\d+)(\.\d+)?$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "默认值不合法，应为整数或小数";
            }
            if(!isNaN(lengthValue) && (lengthValue > 100 || lengthValue <1)){
                validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
            }
            else if(!isNaN(lengthValue) && lengthValue <= 100 && lengthValue >=1){
                reguInteger = /^[1-9][0-9]*$/;
                flagInteger = reguInteger.test(lengthValue);
                if(flagInteger){
                    flagLength = true;
                }
                else{
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
            }
            else if(isNaN(lengthValue)){
                reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                flagLength = reguLength.test(lengthValue);
                if(!flagLength){
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
                else{
                    if((lengthValue.split(",")[0] <= 100) && (lengthValue.split(",")[0] >= 1) && (parseInt(lengthValue.split(",")[0] )> parseInt(lengthValue.split(",")[1]))){
                        flagLength = true;
                    }
                    else{
                        flagLength = false;
                        validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                    }
                }
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "DATE" :  //2004-04-30
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$|^\@.*|^$/
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "日期格式默认值不合法，格式应为2004-04-30，或@day";
            }
            return (flagValue && flagName);
            break;
        case "TIME" : //23:59
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^(([1-9]{1})|([0-1][0-9])|([1-2][0-3])):([0-5][0-9])$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "时间格式默认值格式不合法，格式应为 23:59,或@date";
            }
            return (flagValue && flagName);
            break;
        case "DATETIME" :  //00-00-00 00:00:00 | 0000-00-00 00:00:00 | 09-05-22 08:16:00 | 1970-00-00 00:00:00 | 20090522081600
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /(\d{2}|\d{4})(?:\-)?([0]{1}\d{1}|[1]{1}[0-2]{1})(?:\-)?([0-2]{1}\d{1}|[3]{1}[0-1]{1})(?:\s)?([0-1]{1}\d{1}|[2]{1}[0-3]{1})(?::)?([0-5]{1}\d{1})(?::)?([0-5]{1}\d{1})|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "系统提示", "日期时间格式默认值格式不合法，格式应为1970-00-00 00:00:00，或@date";
            }
            return (flagValue && flagName);
            break;
        case "文本" :
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            //不输入非法字符且长度小于3000
            reguValue = /^[^\!\@\#\$\%\^\&\*]{0,3000}|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "文本格式默认值不合法，应小于等于3000字符，且无非法字符";
            }
            //不输入非法字符且长度值小于3000
            if(!isNaN(lengthValue) && lengthValue <= 3000 && lengthValue >=1){
                flagLength = true;
            }
            if(!flagLength){
                validateMsg = "长度不合法，应为小于等于3000的正整数";
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "数值" :
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^(-?\d+)(\.\d+)?$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "默认值不合法，应为整数或小数";
            }
            if(!isNaN(lengthValue) && (lengthValue > 100 || lengthValue <1)){
                validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
            }
            else if(!isNaN(lengthValue) && lengthValue <= 100 && lengthValue >=1){
                reguInteger = /^[1-9][0-9]*$/;
                flagInteger = reguInteger.test(lengthValue);
                if(flagInteger){
                    flagLength = true;
                }
                else{
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
            }
            else if(isNaN(lengthValue)){
                reguLength = /^[1-9][0-9]*(\,[1-9][0-9]*)$/;
                flagLength = reguLength.test(lengthValue);
                if(!flagLength){
                    validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                }
                else{
                    if((lengthValue.split(",")[0] <= 100) && (lengthValue.split(",")[0] >= 1) && (parseInt(lengthValue.split(",")[0] )> parseInt(lengthValue.split(",")[1]))){
                        flagLength = true;
                    }
                    else{
                        flagLength = false;
                        validateMsg = "长度值不合法，应为小于100正整数或者（m,n）形式,m、n均为正整数小于等于100，m大于n";
                    }
                }
            }
            return  (flagValue && flagLength && flagName);
            break;
        case "日期" :  //2004-04-30
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$|^\@.*|^$/
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "日期格式默认值不合法，格式应为2004-04-30，或@day";
            }
            return (flagValue && flagName);
            break;
        case "时间" : //23:59
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /^(([1-9]{1})|([0-1][0-9])|([1-2][0-3])):([0-5][0-9])$|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "时间格式默认值格式不合法，格式应为 23:59,或@date";
            }
            return (flagValue && flagName);
            break;
        case "日期时间" :  //00-00-00 00:00:00 | 0000-00-00 00:00:00 | 09-05-22 08:16:00 | 1970-00-00 00:00:00 | 20090522081600
            validateMsg = "";
            if(fieldName.toLowerCase()=="id"||fieldName.toLowerCase()=="yn"|| fieldName.toLowerCase()=="create_time"|| fieldName.toLowerCase()=="creator"
                || fieldName.toLowerCase()=="modify_time"||fieldName.toLowerCase()=="modifier"||fieldName.toLowerCase()=="parent_id") {
                validateMsg = "字段名称不允许出现数据库默认字段（id,yn,create_time,creator,modify_time,modifyier,parent_id）";
            }
            else{
                flagName = true;
            }
            reguValue = /(\d{2}|\d{4})(?:\-)?([0]{1}\d{1}|[1]{1}[0-2]{1})(?:\-)?([0-2]{1}\d{1}|[3]{1}[0-1]{1})(?:\s)?([0-1]{1}\d{1}|[2]{1}[0-3]{1})(?::)?([0-5]{1}\d{1})(?::)?([0-5]{1}\d{1})|^\@.*|^$/;
            flagValue = reguValue.test(defaultValue);
            if(!flagValue){
                validateMsg = "系统提示", "日期时间格式默认值格式不合法，格式应为1970-00-00 00:00:00，或@date";
            }
            return (flagValue && flagName);
            break;
    }
}
/**
 * 根据数据类型校验默认值格式提示信息
 * @return {*}
 */
function validateDefaultValueMsg(){
    Dialog.alert("系统提示",validateMsg);
}


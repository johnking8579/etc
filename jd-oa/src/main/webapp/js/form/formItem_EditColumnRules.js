/**
 * Created with IntelliJ IDEA.
 * Desctription: 表单列规则配置IFRAME
 * User: yujiahe
 * Date: 13-9-17
 * Time: 上午11:04
 * To change this template use File | Settings | File Templates.
 */
var formId = "";
var nEditing = null;//编辑分页全局变量
$(function () {
    formId = $("#formId").val();
    setColumnRulesTable(formId);

    //选中子表单，子表单字段联动
    $("#sonFormCR").live('change', function (e) {
        sonItem();
    })
});
/**
 * 根据选中的子表单，查询出表单项列表，且为数据类型
 */
function sonItem() {
    var sonFormId = $("#sonFormCR").val();
    $.ajax({
        type: "GET",
        cache: "false",
        url: "formItem_getFormItemByFormId",
        dataType: "json",
        data: "formId=" + sonFormId,
        success: function (json) {
            var numItemHtml = "";
            for (var i = 0; i < json.length; i++) {
                if (json[i].dataType == "NUMBER") { //如果类型为数值类型则显示
                    numItemHtml += "<option value=\"" + json[i].id + "\">" + json[i].fieldChineseName + "</option> ";
                }
            }
            $("#sonFormItem").html(numItemHtml);
        }
    });
}
/**
 * 列规则分页设置函数
 * @param formId
 */
function setColumnRulesTable(formId) {
    var options = Table.options;
    options = {
        isPaging: false,
        useCheckbox: true,
        pageUrl: "formItem_columnRulesPage",
        defaultSort: [],
        sendData: function (sSource, aoData, fnCallback) {
            aoData.push({
                "name": "formId",
                "value": formId
            });
            jQuery.ajax({
                type: "POST",
                url: "formItem_columnRulesPage",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);
                }
            });
        },
        // 分页列设置
        columns: [
            { "mDataProp": "computeExpressDesc", "sTitle": "子表", "bSortable": true, "bVisible": true, "sWidth": 300},
            { "mDataProp": "computeExpress", "sTitle": "字段", "bSortable": true, "bVisible": true, "sWidth": 300},
            { "mDataProp": "fieldChineseName", "sTitle": "规则", "bSortable": true, "bVisible": true, "sWidth": 80, "mRender": function (data, type, full) {
                return "合计为：";
            }},
            { "mDataProp": "fieldChineseName", "sTitle": "主表字段", "bSortable": true, "bVisible": true, "sWidth": 300, "mRender": function (data, type, full) {
                return data;
            }},
            {"mData": "id", "sTitle": "操作", "bSortable": false, "sWidth": 35, "mRender": function (data, type, full) {
                return "<a href='#' title='修改' class='edit_item'>修改</a><a href='#' title='删除' class='del_item' onclick='deleteColumnRow($(this))'>删除</a>";
            }}
        ],
        // 分页按钮设置
        btns: [
        /**
         * 新增表单项
         */
            {
                "sExtends": "text",
                "sButtonText": "新增",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    if(nEditing==null){
                        addRow(table)
                    }
                   ;
                }
            },
        /**
         * 删除表单项（批量）
         */
            {
                "sExtends": "text",
                "sButtonText": "删除",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    var ids = Table.getSelectedRowsIDs(table);//复选框选中项目的ID，逗号隔开
                    if (ids.length == 0) {
                        Dialog.alert("消息提示", "没有选中项", "确定");
                    } else {
                        delFormColumnRules(ids)
                    }
                }
            }
        ]
    };
    table = Table.dataTable("columnRulesTable", options);
    bindEditByTable(); //绑定可编辑
}

/**
 * 分页删除一行
 * @param el
 */
function deleteColumnRow(el) {
    var oTable = table;
    var nRow = el.parents('tr')[0];
    var aData = oTable.fnGetData(nRow);
    if (aData.id == null || aData.id == "") { //未保存到数据库不走后台删除
        var oSettings = oTable.fnSettings();
        oSettings.oFeatures.bServerSide = false;
        oTable.fnDeleteRow(nRow);
        oSettings.oFeatures.bServerSide = true;
    } else {//走后台删除
        var ids = new Array();
        ids.push(aData.id);
        delFormColumnRules(ids);
    }
}
//添加一行记录
function addRow(oTable) {
    $.ajax({
        type: "GET",
        cache: "false",
        url: "formItem_getFormItemByFormId",
        dataType: "json",
        data: {
            formId: formId,
            computeExpressDesc: "isNull"
        },
        success: function (json) {
            var mainFormItemHtml = "<select id='mainFormId'  style='width:150px'>";
            var flag = false;
            for (var i = 0; i < json.length; i++) {
                if (json[i].dataType == "NUMBER") {
                    mainFormItemHtml += "<option value=\"" + json[i].id + "\" selected>" + json[i].fieldChineseName + "</option> ";
                    flag = true;
                }
            }
            mainFormItemHtml += "</select>";
            if (flag) {
                var arr = [];
                arr.push({"computeExpressDesc": "", "computeExpress": "", "fieldChineseName": "", "id": ""});
                var oSettings = oTable.fnSettings();
                oSettings.oFeatures.bServerSide = false;
                var aiNew = oTable.fnAddData(arr);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                oSettings.oFeatures.bServerSide = true;
                var jqTds = $('>td', nRow);
//                var aData = oTable.fnGetData(nRow);
                jqTds[4].innerHTML = mainFormItemHtml;
            } else {
//                Dialog.alert("提示信息", "已经没有字段可以配置", "", "");
                $("#alert").removeClass("alert-info").addClass("alert-error").text("已经没有字段可以配置");
            }
        }
    });
}
/**
 * 删除列规则（批量）
 * @param formItemIds
 */
function delFormColumnRules(formItemIds) {
    Dialog.del('formItem_columnRolesEdit?oldItemId=' + formItemIds, '', function (result) {
        Table.render(table)
        $("#alert").removeClass("alert-info alert-error").addClass("alert-success").text("列规则删除成功");
    });
}


function bindEditByTable() {
    //绑定编辑按钮按钮
    $('#columnRulesTable  a.edit_item').live('click', function (e) {
        e.preventDefault();
        //return;
        var nRow = $(this).parents('tr')[0];
        if (nEditing !== null && nEditing != nRow) {
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRow(table, nEditing);
            editRow(table, nRow);
            nEditing = nRow;
        }
        else if (nEditing == nRow && this.innerHTML == "保存") {
            /* This row is being edited and should be saved */
            saveRow(table, nEditing);
            nEditing = null;
        }
        else {
            /* No row currently being edited */
            editRow(table, nRow);
            nEditing = nRow;
        }
    });

}
//修改
function editRow(oTable, nRow) {

    var aData = oTable.fnGetData(nRow);
    nEditing = nRow;
    var jqTds = $('>td', nRow);
    var sonFormHtml = "<select id='sonFormCR' style='width:150px' >";
    //子表单
    $.ajax({
        type: "post",
        cache: "false",
        url: "formItem_getSonFormByParentId",
        dataType: "json",
        data: {
            formId: formId
        },
        success: function (json) {
            for (var i = 0; i < json.length; i++) {
                if (aData.computeExpressDesc == json[i].formName) {
                    sonFormHtml += "<option value=\"" + json[i].id + "\" selected>" + json[i].formName + "</option> "
                } else {
                    sonFormHtml += "<option value=\"" + json[i].id + "\">" + json[i].formName + "</option> "
                }

            }
            sonFormHtml += "</select>";
            jqTds[1].innerHTML = sonFormHtml;
            sonItem();
        }
    });

    //子表单字段
    jqTds[2].innerHTML = '<select id="sonFormItem"  style="width:150px" ><option>请选择</option></select>';

    //点击编辑后“编辑”变“保存”
    $('.edit_item', jqTds[5]).text('保存');
}

//保存

function saveRow(oTable, nRow) {
    var jqInputs = $('input', nRow);
    arr = [];
    //收集数据
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var computeExpressDesc = $("#sonFormCR").val();
    var computeExpress = $("#sonFormItem").val();
    var itemId;
    var alerttext = "";
    if (aData.id == '') {
        alerttext = "列规则新增成功";
        itemId = $("#mainFormId").val();
    } else {
        alerttext = "列规则修改成功";
        itemId = aData.id;
    }
    //后台修改
    jQuery.ajax({
        async: false,
        type: "POST",
        url: springUrl + "/form/formItem_cptExpAdd",
        data: {
            id: itemId,
            computeExpressDesc: computeExpressDesc,
            computeExpress: computeExpress,
            oldItemId: ''//aData.id

        },
        before: function () {
        },
        success: function (resp) {
            if (parseInt(resp) >= 1)
                bol = true;
            $("#alert").removeClass("alert-info alert-error").addClass("alert-success").text(alerttext);
        }
    });
    oTable.fnDraw();
}

function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(aData.computeExpressDesc, nRow, 1, false);
    oTable.fnUpdate(aData.computeExpress, nRow, 2, false);
    oTable.fnUpdate(aData.fieldChineseName, nRow, 4, false);
    $('.edit_item', jqTds[4]).text('修改');
}





function addField(){
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            fieldName:$('#fieldName').val(),
            fieldChineseName:$('#fieldChineseName').val()
        },
        /*beforeSend:function(){//提交前校验
            $("#fieldChineseName").trigger("change.validation",{submitting: true});
            $("#fieldName").trigger("change.validation",{submitting: true});
            if($("#fieldChineseName").jqBootstrapValidation("hasErrors") || $("#fieldName").jqBootstrapValidation("hasErrors")){
                return false;
            }
        },*/
        url: springUrl+"/form/formBusinessField_isExistField",
        success:function (data) {
            if(data.operator == true){
                Dialog.alert("失败",data.message);
                return false;
            }
            else{
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        fieldName:$('#fieldName').val(),
                        fieldChineseName:$('#fieldChineseName').val(),
                        tableId:$('#tableId').val(),
                        inputType:$('#inputType').val(),
                        dataType:$('#dataType').val(),
                        length:$('#length').val(),
                        lengthDecimal:$('#lengthDecimal').val(),
                        defaultValue:$('#defaultValue').val(),
                        sortNo:$('#sortNo').val()
                    },
                    url: springUrl+"/form/formBusinessField_addSave",
                    success:function (data) {
                        window.location.href = springUrl+"/form/formBusinessField_index?id="+data.tableId;
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据表失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }
                    }
                });
            }
        },
        error: function (data) {
            if(data.responseText == null){
                Dialog.alert("失败","新建数据表失败");
            }else{
                Dialog.alert("失败",data.responseText);
            }
        }
    });
}

function cancelAddField(){
    $("#fieldLoad").html("");
}


//初始化页面
$(function() {
    /*$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();*/
    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#dataType').click(function() {
        //预留数据类型弹出框
    });
    $('#addField').click(function() {
        addField();
    });
    $('#cancelAddField').click(function() {
        cancelAddField();
    });
});

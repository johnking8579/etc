// 页面初始化加载分页
$(document).ready(
	loadTelmplateSelect
);


/**
 * 加载表单类型select下拉框
 */
function loadTelmplateSelect() {
	 $('#exe').click(function () {
	    	loadData();
	    });
	    $('#divClear').click(function () {
	    	clearData();
	    });
    jQuery.ajax({
        type: "get",
        cache: true,
        dataType: 'text',
        url: springUrl + "/form/formTemplate_getTelmplateSelect",
        success: function (data) {
            $(data).appendTo($("#templatePath"));
        },
        error: function (data) {
            Dialog.alert("失败!", "加载表单模板失败");
        }
    });
}

/**
 * 加载表单类型select下拉框
 */
function loadData() {
	var exeTimes = $("#exeTimes").val();
	var templatePath;
	if($("#pathType").attr("checked")){
		templatePath = $("#templatePath_text").val();
	}else{
		templatePath = $("#templatePath").find("option:selected").val();
	}
    jQuery.ajax({
        type: "get",
        cache: false,
        dataType: 'text',
        url: springUrl + "/form/formTemplate_testJssExecute?templatePath="+templatePath+"&exeTimes="+exeTimes,
        success: function (data) {
            $("#exeResult").val(data);
        },
        error: function (data) {
            Dialog.alert("失败!", "执行失败！");
        }
    });
}
function clearData() {
   $("#exeResult").val('');
}

//关闭灰色 jQuery 遮罩
function changeType(){
	if($("#pathType").attr("checked")){
		$('#templatePath_text').show();
	    $("#templatePath").hide();
	}else{
		$('#templatePath_text').hide();
	    $("#templatePath").show();
	}
}




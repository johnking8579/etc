var lastIndex = undefined;
$(function(){
    $('#dataSourceMappingGrid').datagrid({
        url:'/di/datasource/mapping_getJsonData?formId=' + formId + '&dataSourceId=' + dataSourceId + '&fieldId=' + fieldId,
        rownumbers : true,
        height : 290,
        singleSelect : true,
        columns:[[
	        {field:'id',title:'',width:200,checkbox:true},
	        {field:'formId',title:'formId',width:200,hidden:true},
	        {field:'formItemId',title:'formItemId',width:200,hidden:true},
	        {field:'name',title:'源字段名称',width:140, editor:{
	        	type:'combobox',
	        	options:{
	        		valueField:'columnId',
	        		textField:'columnId',
	        		method : 'GET',
		        	url:'/di/datasource/datasource_getColumnsForMap?datasourceId=' + dataSourceId
	        	}
	        }},
	        {field:'targetName',title:'目标字段名称',width:140, editor:{
	        	type:'combobox',
	        	options:{
	        		valueField:'fieldName',
	        		textField:'fieldName',
	        		method : 'GET',
		        	url:'/di/datasource/mapping_getFormItem?formId=' + formId,
		        	onSelect : function(record){
		        		$('#dataSourceMappingGrid').datagrid('updateRow',{
		        			index: editIndex,
		        			row: {
		        				fieldType: record.fieldChineseName
		        			}
		        		});
		        	}
	        	}
	        }},
	        {field:'dispaly',title:'列标题',width:140,editor:'text'},
	        {field:'width',title:'列宽度',width:80,align:'left',editor:'numberbox'},
	        {field:'filter',title:'是否模糊过滤',width:80,align:'center',editor:{type:'checkbox',options: { on:'1',  off:'0'} }},
	        {field:'ishidden',title:'是否隐藏',width:80,align:'center',editor:{type:'checkbox',options: { on:'1',  off:'0'} }},
	        {field:'datasourceId',title:'datasourceId',width:200,hidden:true}
        ]],
        onClickRow:function(rowIndex){
        	if (lastIndex != rowIndex){
        		$('#dataSourceMappingGrid').datagrid('endEdit', lastIndex);
        	}
        	$('#dataSourceMappingGrid').datagrid('beginEdit', rowIndex);
        		lastIndex = rowIndex;
        	}
    });
});
//add insert grid
function append(){
	$('#dataSourceMappingGrid').datagrid('endEdit', lastIndex);
	$('#dataSourceMappingGrid').datagrid('appendRow',{});
	editIndex = $('#dataSourceMappingGrid').datagrid('getRows').length - 1;
	$('#dataSourceMappingGrid').datagrid('selectRow', lastIndex).datagrid('beginEdit', lastIndex);
}
function save(){
	alert(fieldId);
	var datagrid = $('#dataSourceMappingGrid');
    if (datagrid.length == 0) {
        return;
    }
    endEdit(datagrid);
    var rows = datagrid.datagrid('getChanges');
    if (rows.length > 0) {
        $.ajax({
            type: "POST",
            url: "/di/datasource/mapping_save",
            async: false,
            data: "&datas=" + JSON.stringify(rows) + '&formId=' + formId + '&dataSourceId=' +dataSourceId + '&formItemId=' + fieldId ,
            success: function (businessObjectId) {
                $('#dataSourceMappingGrid').datagrid('acceptChanges');
                alert('保存成功');
                datagrid.reload();
            },
            error: function (msg) {
                alert('保存失败!');
                return false;
            }
        });
    }
}
function removeData(){
	var row = $('#dataSourceMappingGrid').datagrid('getSelected');
    if (row) {
        var index = $('#dataSourceMappingGrid').datagrid('getRowIndex', row);
        $.ajax({
            type: "POST",
            url: "/di/datasource/mapping_remove",
            async: false,
            data: "id=" + row.id,
            success: function () {
            	$('#dataSourceMappingGrid').datagrid('deleteRow', index);
            	 alert('数据删除成功!');
            },
            error: function (msg) {
                alert('数据删除失败!');
                return false;
            }
        });
    }
	return false;
}
function endEdit(objDatagrid) {
    var rows = objDatagrid.datagrid('getRows');
    for (var i = 0; i < rows.length; i++) {
        objDatagrid.datagrid('endEdit', i);
    }
}


$(function(){
    $("#dataAdapter").live("change",function(){
        var dataAdapterType=$("#dataAdapter").find("option:selected").parent().attr("group");
        if(dataAdapterType =="1"){
               $("#execSql").parent().parent().show();
        }else{
            $("#execSql").parent().parent().hide();
        }
    });
    $("#dataAdapter").trigger("change");
});
function addDatasource(modal){
    var id=$("#id").val();
    var datasourceName=$("#datasourceName").val();
    var dataAdapter=$("#dataAdapter").val();
    var dataAdapterType=$("#dataAdapter").find("option:selected").parent().attr("group");
    var execSql="";
    if(dataAdapterType =="1"){
        execSql=$("#execSql").val();
        if(!validateSql(execSql)){
            return false;
        }
    }
    var data={
            "id":id,
            "datasourceName":datasourceName,
            "dataAdapter":dataAdapter,
            "dataAdapterType":dataAdapterType,
            "execSql":execSql
    };
    $.ajax({
        url:springUrl+"/di/datasource/datasource_addSave",
        async:true,
        type:"post",
        data:data,
        dataType : 'json',
        cache:false,
        complete:function(){},
        success: function(result){
            if( result.operator == "success"){
                Dialog.alert("消息提示","数据源操作成功！");
                Table.render(datasourceTable);
                Dialog.hideModal(modal);
            }
        },
        error : function(data) {
            Dialog.alert("请求异常",data.responseText);
        }
    });
}
function validateSql(sql){
    sql=sql.trim();
    var startStr=sql.substring(0,7).toLocaleLowerCase();
    if(startStr!= "select "){
        alert("SQL语句必须以\"select\"开头!");
        return false;
    }
    return true;
}
function testDatasource(){
    var dataAdapterType=$("#dataAdapter").find("option:selected").parent().attr("group");
    var dataAdapter=$("#dataAdapter").val();
    if(dataAdapter == null || dataAdapter==""){
        alert("请选择数据源!");
        return false;
    }
    var sql="";
    if(dataAdapterType == "1"){
        sql=$("#execSql").val();
        if(!validateSql(sql)){
            return false;
        }
    }
    var data={
        "dataAdapterType":dataAdapterType,
        "dataAdapter":dataAdapter,
        "sql":sql
    };
    $.ajax({
        url:springUrl+"/di/datasource/datasource_test",
        type:"POST",
        data:data,
        dataType : 'json',
        cache:false,
        complete:function(){},
        success: function(result){
            if(result.operator == true){
                var column="";
                $.each(result.message,function(i,n){
                    column+="【"+n+"】";
                });
                Dialog.alert("测试成功","返回字段："+column);
            }else{
                Dialog.alert("消息提示","连接无效！");
            }
        },
        error : function(data) {
            Dialog.alert("请求异常",data.responseText);
        }
    });
}
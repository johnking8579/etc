var diLogTable;
$(function() {
    var diLogTable_options = {
        pageUrl : springUrl + "/di/diLog/diLogPage",
        useCheckbox : true,
//        defaultSort:[[5,"desc"]],
        isPaging : true,
        bAutoWidth : true,
        scrollY : "550px",
        callback_fn : function() {
        },
        sendData : function(sSource, aoData, fnCallback) {
            var inParameter = $("input#inParameter").val();
            var transFlag = $("select#transFlag").val();
            aoData.push({
                "name": "transFlag",
                "value": transFlag
            },{
                "name":"inParameter",
                "value":inParameter
            });
            jQuery.ajax({
                type : "POST",
                url : sSource,
                dataType : "json",
                data : aoData,
                success : function(resp) {
                    ifTableData = resp;
                    fnCallback(resp);
                },
                error : function(msg) {
                    Dialog.alert("操作失败", msg.responseText);
                }
            });
        },
        columns : [
            {
                "sTitle" : "传输结果","sClass" : "my_class","sWidth" :100,"bSortable": true,"fnRender" : function(obj) {
                var transFlag = obj.aData.transFlag;
                if (transFlag ==0) {
                    return " <span class='label label-success'>成功</span>";
                } else if(transFlag ==1)  {
                    return " <span class='label label-important'>失败</span>";
                }else{
                    return " <span class='label label-warning'>追加成功</span>";
                }
            }
            }
//            ,{"mDataProp" : "interfaceId","sTitle" : "接口ID", "sClass" : "my_class","bSortable": true, "sWidth" : "100"},
            ,{"mDataProp" : "interfaceName","sTitle" : "接口名称", "sClass" : "my_class","bSortable": true, "sWidth" : "200"},
            { "sTitle" : "入参","sClass" : "my_class","sWidth" :"300","bSortable": true, "fnRender" : function(obj) {
                var inParameter = obj.aData.inParameter;
                return "<pre width='50'>"+inParameter+"</pre>";

//              return "test";
            }
            },
            { "sTitle" : "服务类型","sClass" : "my_class","sWidth" :"100","bSortable": true, "fnRender" : function(obj) {
                    var dataAdapterType = obj.aData.dataAdapterType;
                    if (dataAdapterType == 1) {
                        return "DB数据源类型";
                    } else  {
                        return "接口类型";
                    }
                }
            },

            {"mDataProp" : "creator", "sTitle" : "操作人", "sClass" : "my_class","bSortable": true, "sWidth" :100},
            { "mData": "createTime", "sTitle": "操作时间",  "bSortable": true,"sWidth":100,"mRender": function (data) {
                return new Date(data).format('yyyy-MM-dd hh:mm:ss ');
            }},
            { "bSortable": false, "sWidth": 100, "sTitle": "操作", "fnRender": function (obj) {
                var id = obj.aData.id;
                var transFlag = obj.aData.transFlag;
                var html = "<a href='#' class='btn' title='查看详情' onclick='viewDetail(\""
                    + id
                    + "\");'><img src='"
                    + springUrl
                    + "/static/common/img/edit.png' alt='查看详情'></a>&nbsp;";
                if (transFlag == 1) {//失败的情况提供重新传输功能
                    html += "<a onclick='transfer(\""
                        + id
                        + "\");' title='再次传输' class='btn' href='#'><img alt='再次传输' src='"
                        + springUrl
                        + "/static/common/img/addsign.png'></a>&nbsp;";
                }


                    return html;
                }
            } ],
        btns : [ ]
    };
    diLogTable = Table.dataTable("diLog_table", diLogTable_options);
    $('#search').click(function () {
        Table.render(diLogTable);
    });
});
function viewDetail(id){
    Dialog.openRemote("查看服务日志详情",springUrl+"/di/diLog/diLogDetail?id="+id, 1000,600,
        [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function() {}
            }
        ]);
}
function transfer(id){
    var adata = [];
    adata.push({
        "name": "id",
        "value": id
    });
    jQuery.ajax({
        type : "get",
        url :  springUrl + "/di/diLog/diTransfer",
        dataType : "json",
        data :adata,
        success : function(resp) {
            Dialog.alert("操作成功", resp.message);
            Table.render(diLogTable);
        },
        error : function(msg) {
            Dialog.alert("操作失败", msg.responseText);
        }
    });

}
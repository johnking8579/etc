var ifTable;
var ifTableData;

$(function() {
	var ifTable_options = {
		pageUrl : springUrl + "/di/cc/ifPage",
		useCheckbox : true,
		defaultSort : [],
		isPaging : true,
		bAutoWidth : true,
		scrollY : "550px",
		callback_fn : function() {
		},
		sendData : function(sSource, aoData, fnCallback) {
            var interfaceName=$("#interfaceNameSearchText").val();
            interfaceName=interfaceName.replace("_","\\_");
            aoData.push(
                { "name": "interfaceName", "value":interfaceName}
            );
			jQuery.ajax({
				type : "POST",
				url : sSource,
				dataType : "json",
				data : aoData,
				success : function(resp) {
					ifTableData = resp;
					fnCallback(resp);
				},
				error : function(msg) {
					Dialog.alert("操作失败", msg.responseText);
				}
			});
		},
		columns : [
				{
					"mDataProp" : "interfaceName",
					"sTitle" : "接口名称",
					"sClass" : "my_class",
					"sWidth" : "130"
				},
				{
					"sTitle" : "接口类型",
					"sClass" : "my_class",
					"sWidth" : "60",
					"fnRender" : function(obj) {
						var interfaceType = obj.aData.interfaceType;
						if (interfaceType == 1) {
							return "SOAP WS";
							// 接口类型：1:SOAP WS，2:REST WS，3:SAF，4:HTTP
						} else if (interfaceType == 2) {
							return "REST WS";
						} else if (interfaceType == 3) {
							return "SAF";
						} else if (interfaceType == 4) {
							return "HTTP";
						} else if (interfaceType == 5) {
							return "内部类";
						} else  {
							return "others";
						}
					}
				},
				{
					"mDataProp" : "url",
					"sTitle" : "URL",
					"sClass" : "my_class",
					"sWidth" : "200"
				},
				{
					"mDataProp" : "interfaceDesc",
					"sTitle" : "接口描述",
					"sClass" : "my_class",
					"sWidth" : "200"
				},
				{
					"bSortable" : false,
					"sWidth" : "70",
					"sTitle" : "操作",
					"fnRender" : function(obj) {
						var id = obj.aData.id;
						var html = "<a href='#' class='btn' title='修改' onclick='edit(\""
								+ id
								+ "\");'><img src='"
								+ springUrl
								+ "/static/common/img/edit.png' alt='修改'></a>&nbsp;"
								+ "<a onclick='del(\""
								+ id
								+ "\");' title='删除' class='btn' href='#'><img alt='删除' src='"
								+ springUrl
								+ "/static/common/img/del.png'></a>&nbsp;";

						return html;
					}
				} ],
		btns : [
				{
					"sExtends" : "text",
					"sButtonText" : "添加",
					"sToolTip" : "",
					"fnClick" : function(nButton, oConfig, oFlash) {
						Dialog
								.openRemote(
										"添加接口",
										springUrl + "/di/cc/if_add",
										600,
										600,
										[
												{
													"label" : "取消",
													"class" : "btn-success",
													"callback" : function() {
													}
												},
												{
													"label" : "保存",
													"class" : "btn-success",
													"callback" : function(
															modal, e) {
														var validateEl = $('#interfaceAddForm');
														if (!validate2(validateEl)) {
															return false;
														}
														addInterface(modal);
														return false;
													}
												} ]);
					}
				}, {
					"sExtends" : "text",
					"sButtonText" : "删除",
					"sToolTip" : "",
					"fnClick" : function(nButton, oConfig, oFlash) {
						var ids = Table.getSelectedRowsIDs(ifTable);
						if (ids.length == 0) {
							Dialog.alert("消息提示", "没有选中项", "确定");
						} else {
							del(ids);
						}
					}
				} ]
	};
	ifTable = Table.dataTable("interface_table", ifTable_options);
    $("#search").click(function(){
        Table.render(ifTable);
    });
});
// 删除操作
function del(ids){
	Dialog.del(springUrl + "/di/cc/if_delete?ids="+ids, '', function(result) {
		Table.render(ifTable);
		Dialog.alert("操作结果", result.message);
	});
}

//修改操作
function edit(id){
    Dialog.openRemote("修改接口连接",springUrl+"/di/cc/if_update?id="+id, 600,600,
        [
            {
                "label": "取消",
                "class": "btn-success",
                "callback": function() {}
            },
            {
                "label": "保存",
                "class": "btn-success",
                "callback": function(modal,e) {
                    var validateEl=$('#interfaceAddForm');
                    if(!validate2(validateEl)){
                        return false;
                    }
                    addInterface(modal);
                    return false;
                }
            }
        ]);
}
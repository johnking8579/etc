var ccdbTable;
var ccdbTableData;

$(function(){
	
    var	ccdbTable_options={
        pageUrl:springUrl+"/di/cc/db/db_list",
        useCheckbox:true,
        defaultSort:[],
        isPaging:true,
        bAutoWidth:true,
        scrollY:"500px",
        callback_fn:function(){
        },
        sendData:function ( sSource, aoData, fnCallback ) {
            jQuery.ajax( {
                type: "POST",
                url:sSource,
                dataType: "json",
                data: aoData,
                success: function(resp) {
                	ccdbTableData=resp;
                    fnCallback(resp);
                },
                error:function(msg){
                    Dialog.alert("操作失败",msg.responseText);
                }
            });
        },
        columns: [
            
            { "sTitle":"类型","sClass": "my_class","sWidth":"50","fnRender":function(obj){
            	var type = obj.aData.dbType;
            	if(type === '1')
            		return "<img src='"+springUrl+"/static/common/img/mysql.jpg' />";
            	else if(type === '2')
            		return "<img src='"+springUrl+"/static/common/img/oracle.jpg' />";
            	else
            		return "unknow";
            }},
            { "mDataProp": "dbName","sTitle":"名称","sClass": "my_class","sWidth":"70"},
            {"sTitle":"操作","bSortable":false,
            	"fnRender":function(obj){
				var id = obj.aData.id;
				return "<a href='#' class='btn' title='修改' onclick='diCcDbUpdate(\""+id+"\");'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>"
							+ "&nbsp;&nbsp;<a href='#' class='btn' title='删除' onclick='diCcDbDelete(\""+id+"\",true);'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'></a>";
            	}
            }
        ],
        btns:[
            {
                "sExtends":    "text",
                "sButtonText": "新增",
                "sToolTip": "",
                "fnClick": function ( nButton, oConfig, oFlash ) {
                    Dialog.openRemote("新增数据库连接",springUrl+"/di/cc/db/db_add", 550, 690,
                        [
                            {
                                "label": "取消",
                                "class": "btn-success",
                                "callback": function() {}
                            },
                            {
                                "label": "保存",
                                "class": "btn-success",
                                "callback": function(modal,e) {
                                	var bol = diCcDbAddAjax();
                                	if(bol != '-1'){
			     						Dialog.alert("提示信息","新增成功！","确定");
			     						Table.render(ccdbTable);
			     						return true;
			     					}else{
			     						Dialog.alert("提示信息","新增失败！","确定");
			     						return false;
			     					}
                                }
                            }
                        ]);
                }
            }
         ]
    };
    ccdbTable = Table.dataTable("ccdb_table",ccdbTable_options);
});


function diCcDbAddAjax(){
	
	var dbType= $("input[name=ccdbType]:checked").val();
	var dbName = $.trim($("#ccdbName").val());
	var dbDriver =$.trim( $("#ccdbDriver").val());
	var dbUrl =$.trim( $("#ccdbUrl").val());
	var dbUsername =$.trim( $("#ccdbUsername").val());
	var dbPassword =$.trim( $("#ccdbPassword").val());
	var dbDesc =$.trim( $("#ccdbDesc").val());
	var dbConfigProperties = $.trim( $("#ccdbConfigProperties").val()); 
	
	
	
	var ao=[];
	ao.push(
		{ "name": "dbType", "value":dbType }
		,{ "name": "dbName", "value":dbName}
		,{ "name": "driver", "value":dbDriver}
		,{ "name": "url", "value":dbUrl}
		,{ "name": "username", "value":dbUsername}
		,{ "name": "password", "value":dbPassword}
		,{ "name": "configProperties", "value":dbConfigProperties}
		,{"name":"dbDesc","value":dbDesc});
	
	var bol= "-1";
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:springUrl+"/di/cc/db/db_addAjax", 
        data: ao, 
        success: function(resp) {

        	 bol = resp;
        },
		error: function(msg){

		}
	});
	
	
	return bol;
}



function diCcDbDelete(id){
	Dialog.confirm("删除警告！","确定要删除选中数据吗？","确定","取消",function(result){
		if(result){
			var ao=[];
			ao.push({ "name": "id", "value":id});
			
			jQuery.ajax( {
				async: false ,
		        type: "POST", 
		        url:springUrl+"/di/cc/db/db_deleteAjax", 
		        data: ao, 
		        success: function(resp) {
		        	if(resp == "1"){
		        		Dialog.alert("提示信息","删除成功！","确定");
		        		Table.render(ccdbTable);
		        	}else{
		        		Dialog.alert("提示信息","删除失败！","确定");
		        	}
		  
		        }
			});
		}
			
	});
}


//打开修改数据库连接窗口
function diCcDbUpdate(id){
	Dialog.openRemote("更新数据库连接",springUrl+"/di/cc/db/db_update?id="+id, 550, 690,
            [
                {
                    "label": "取消",
                    "class": "btn-success",
                    "callback": function() {}
                },
                {
                    "label": "更新",
                    "class": "btn-success",
                    "callback": function() {
                     	var bol = diCcDbUpdateAjax(id);
                    	if(bol != '-1'){
     						Dialog.alert("提示信息","更新成功！","确定");
     						Table.render(ccdbTable);
     						return true;
     					}else{
     						Dialog.alert("提示信息","更新失败！","确定");
     						return false;
     					}
                    }
                }
            ]);
}

function diCcDbUpdateAjax(id){
	var dbType= $("input[name=ccdbType]:checked").val();
	var dbName = $.trim($("#ccdbName").val());
	var dbDriver =$.trim( $("#ccdbDriver").val());
	var dbUrl =$.trim( $("#ccdbUrl").val());
	var dbUsername =$.trim( $("#ccdbUsername").val());
	var dbPassword =$.trim( $("#ccdbPassword").val());
	var dbDesc =$.trim( $("#ccdbDesc").val());
	var dbConfigProperties = $.trim( $("#ccdbConfigProperties").val()); 
	
	
	
	var ao=[];
	ao.push(
		{ "name": "dbType", "value":dbType }
		,{ "name": "id", "value":id}
		,{ "name": "dbName", "value":dbName}
		,{ "name": "driver", "value":dbDriver}
		,{ "name": "url", "value":dbUrl}
		,{ "name": "username", "value":dbUsername}
		,{ "name": "password", "value":dbPassword}
		,{ "name": "configProperties", "value":dbConfigProperties}
		,{"name":"dbDesc","value":dbDesc});
	
	var bol= "-1";
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:springUrl+"/di/cc/db/db_updateAjax", 
        data: ao, 
        success: function(resp) {

        	 bol = resp;
        },
		error: function(msg){

		}
	});
	
	console.log(bol)
	return bol;
}

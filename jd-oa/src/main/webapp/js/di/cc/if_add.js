function addInterface(modal){
	var id=$("#id").val();
    var interfaceType=$("#interfaceType").val();
    var interfaceName=$("#interfaceName").val();
    var url=$("#url").val();
    var interfaceDesc=$("#interfaceDesc").val();
    var method=$("#method").val();
    var inParameterType=$("#inParameterType").val();
    var inParameterTemplate=$("#inParameterTemplate").val();
    var outParameterType=$("#outParameterType").val();
    var outParameterTemplate=$("#outParameterTemplate").val();//authHeader
    var authHeader=$("#authHeader").val();
    var data={
    		"id":id,
            "interfaceType":interfaceType,
            "interfaceName":interfaceName,
            "url":url,
            "interfaceDesc":interfaceDesc,
            "method":method,
            "inParameterType":inParameterType,
            "inParameterTemplate":inParameterTemplate,
            "outParameterType":outParameterType,
            "outParameterTemplate":outParameterTemplate,
            "authHeader":authHeader
    };
    $.ajax({
        url:springUrl+"/di/cc/if_addSave",
        async:true,
        type:"post",
        data:data,
        dataType : 'json',
        cache:false,
        complete:function(){},
        success: function(result){
            if( result.operator == "success"){
                Dialog.alert("消息提示",result.message);
                Table.render(ifTable);
                Dialog.hideModal(modal);
            }
        },
        error : function(data) {
            Dialog.alert("请求异常",data.responseText);
        }
    });
}
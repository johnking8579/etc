/**
 * 数据字典-数据类别新增页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var dictTypeId = $('#id').val();
/**
 * 添加数据类别-确定按钮响应函数
 */
function saveDictType(){
	var validateEl=$('#dictTypeForm');
	if(!validate2(validateEl)){
		return false;
	}
    //判断当前当前类别CODE是否存在
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
        	 dicttypeCode:$('#dicttypeCode').val(),
             parentId:$('#parentName').val(),
             dicttypeName:$('#dicttypeName').val()
        },
        url:springUrl + "/dict/dictType_isExistDictTypeCode",
        /*beforeSend:function(){//提交前校验
            $("#dicttypeName").trigger("change.validation",{submitting: true});
            $("#dicttypeCode").trigger("change.validation",{submitting: true});
            if($("#dicttypeCode").jqBootstrapValidation("hasErrors") || $("#dicttypeName").jqBootstrapValidation("hasErrors")){
                return false;
            }
        },*/
        success:function (data) {
            if(data.operator == false){
                Dialog.alert("失败",data.message);
            }else{
                //新增数据列别
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        dicttypeName:$('#dicttypeName').val(),
                        dicttypeCode:$('#dicttypeCode').val(),
                        parentId:$('#parentName').val(),
                        dicttypeDesc:$('#dicttypeDesc').val()
                    },
                    url: springUrl+"/dict/dictType_addSave",
                    success:function (data) {
                        $("#box-right").load(springUrl+"/dict/dictType_view?dicttypeCode="+$('#dicttypeCode').val());
                        if($("#parentName").val() == ""){
                            refreshNode("null");
                        }
                        else{
                            refreshNode($("#parentName").val());
                        }
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据类别失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }

                    }
                });
            }
        },
        error: function (data) {
            Dialog.alert("失败",data.responseText);
        }
    });
}

/**
 * 添加数据类别-取消按钮响应函数
 */
function cancelDictType(){
    $("#box-right").html("");
    refreshNode($("#parentName").val());
}

/**
 * 初始化操作
 */
$(function() {
     /*$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();*/
    //绑定保存按钮事件
    $('#addDictType').click(function() {
        saveDictType();
    });
    //绑定取消按钮事件
    $('#cancelDictType').click(function() {
        cancelDictType();
    });

    bindValidate($('#dictTypeForm'));
    
   /* $("#dicttypeName").jqBootstrapValidation();
    $("#dicttypeCode").jqBootstrapValidation();*/
});

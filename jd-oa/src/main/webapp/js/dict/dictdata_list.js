/**
 * 数据字典-数据项列表页面JS
 * User: dongjian
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var typeId = $("#dictTypeId").val();
var table;
var options=Table.options;
options={
		pageUrl: springUrl+"/dict/dictData_findDictDataPage",
		useCheckbox:true,
		//defaultSort:[[1,"asc"],[2,"desc"]],
		sendData:function ( sSource, aoData, fnCallback ) {
			var dictTypeId=$("input#dictTypeId").val();
			var dictDataName=$("input#dictDataName").val();
			if(dictTypeId==null) dictTypeId="";
			if(dictDataName==null) dictDataName="";
			aoData.push( { "name": "dictTypeId", "value":dictTypeId } ,{"name":"dictDataName","value":dictDataName} );
			jQuery.ajax( {
	                    type: "POST", 
	                    url:sSource, 
	                    dataType: "json",
	                    data: aoData, 
	                    success: function(json) {
	                            fnCallback(json);
	                    },
                        error: function (data) {
                            Dialog.alert("失败","数据字典数据查询失败");
                        }
		    });
		},
    columns: [
        { "mData": "id","sTitle":"id","bSortable":false,"bVisible":false},
        { "mData": "dictCode","sTitle":"数据编码","bSortable":false,"bVisible":true},
        { "mData": "dictName","sTitle":"数据名称","bSortable":false,"sClass": "my_class",
            fnRender: function (obj)
            {
                var html ="<a href='#' onclick='detailData(\""+ obj.aData.id+"\");'>"+ obj.aData.dictName +"</a>";
                return html;
            }
        },
        { "mData": "parentName","sTitle":"父编号","bSortable":false,"sClass": "my_class"},
        { "mData": "modifier","sTitle":"操作人","bSortable":false,"sClass": "my_class"},
        { "mData": "modifyTime","sTitle":"最后更新" ,"bSortable":false,"mRender":function(data){
            return data==null?"":new Date(data).format("yyyy-MM-dd");
        }},
        { "mData": "sortNo","sTitle":"显示顺序","bSortable":false,"bVisible":true},
        {"mData": "id","sTitle":"操作","bSortable":false,"mRender":function(data, type, full){
            return "<a href='#' title='修改' class='btn'><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'/ onclick='updateDictData(\""+data+"\");'></a>&nbsp;"+
            "<a href='#' title='删除' class='btn'><img src='"+springUrl+"/static/common/img/del.png' alt='删除' onclick='deleteDictData(\""+data+"\");'/></a>";
        }}
    ],
    btns:[
        {
            "sExtends":    "text",
            "sButtonText": "新增",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                Dialog.openRemote('数据项新增',springUrl+"/dict/dictData_add?dictTypeId="+typeId,500,400,[
                    {
                        "label": "取消",
                        "class": "btn-success",
                        "callback": function() {}
                    },
                    {
                        "label": "保存",
                        "class": "btn-success",
                        "callback": function() {
                            if(!validate2($('#dictDataForm'))){
	     						return false;
	     					}
                            addDictData();
                        }
                    }
                ]);
            }
        },
        {
            "sExtends":    "text",
            "sButtonText": "删除",
            "sButtonClass": "btn btn-success",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                batchDeleteDictData();
            }
        }
    ]
};

//初始化页面
$(function() {
    table=Table.dataTable("dictData",options);
    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#searchDictData').click(function() {
        Table.render(table);
    });
  
});

/**
 * 数据字典-数据项修改函数
 */
function updateDictData(id){
    Dialog.openRemote('数据项修改',springUrl+"/dict/dictData_update?dictDataId="+id,500,420,[
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function() {}
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function() {
            	if(!validate2($('#dictDataForm'))){
						return false;
					}
                updateDictDataInfo();
            }
        }
    ]);
}

/**
 * 数据字典-数据项删除函数
 */
function deleteDictData(id){
    Dialog.confirm("确认","确定删除数据字典数据项?","是","否",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                url: springUrl+"/dict/dictData_delete",
                data:{
                    ids:id
                },
                success:function (data) {
                	if (data.operator == false)
                        Dialog.alert("失败！",data.message);
                    else{
                        Dialog.alert("成功！",data.message);
                        $("#box-right").load(springUrl+"/dict/dictData_list?dictTypeId="+typeId);
                    }
                },
                error: function (data) {
                    Dialog.alert("失败","删除数据字典项失败");
                }
            });
        }
    });
}

/**
 * 数据字典-数据项查看详情函数
 */
function detailData(id){
   /* $("#box-right").load(springUrl+"/dictData/dictData_viewDictData?dictDataId="+id);*/
    Dialog.openRemote('数据项查看',springUrl+"/dict/dictData_view?dictDataId="+id,500,450,[
        {
            "label": "关闭",
            "class": "btn-success",
            "callback": function() {}
        }]);
}

function batchDeleteDictData()
{
	 var ids=Table.getSelectedRowsIDs(table);
     var length = ids.length;
     ids = ids.toString();
     if(length==0){
         Dialog.alert("消息提示","没有选中项","确定");
     }else{
    	 Dialog.confirm("确认","确定批量删除字典数据项?","是","否",function(result){
    	    if(result){
    	    	 jQuery.ajax({
    	             type:"POST",
    	             cache:false,
    	             url: springUrl+"/dict/dictData_delete",
    	             data:{
    	                 ids:ids,
    	                 length:length
    	             },
    	             success:function (data) {
    	            	 if (data.operator == false)
                             Dialog.alert("失败！",data.message);
                         else{
                             Dialog.alert("成功！",data.message);
                             $("#box-right").load(springUrl+"/dict/dictData_list?dictTypeId="+typeId);
                         }
    	             },
                     error: function (data) {
                         Dialog.alert("失败","数据字典数据批量删除失败");
                     }
    	         });   
    	    }
    	  });
     }
}
/**
 * 添加数据项-确定按钮响应函数
 */
function addDictData(){
	 var sortNo = $('#sortNo').val();
     var dictTypeId = $('#dictTypeId').val();
     var dictCode = $("#dictDataCode").val();
     var dictName = $('#dictName').val();
     var parentId = $('#parentName').val();
    //判断当前数据项CODE是否存在
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            dictCode:$('#dictDataCode').val(),
            dictTypeId:dictTypeId
        },
        url:springUrl + "/dict/dictData_isExistDictDataCode",
        success:function (data) {
            if(data == true){
                Dialog.alert("失败","数据编码不能重复");
                return false;
            }else{
                //新增数据项
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        sortNo:sortNo,
                        dictTypeId:dictTypeId,
                        dictCode:dictCode,
                        dictName:dictName,
                        parentId:parentId
                    },
                    url: springUrl+"/dict/dictData_addSave",
                    success:function (data) {
                        Dialog.hide();
                        $("#box-right").load(springUrl+"/dict/dictData_list?dictTypeId="+$("#dictTypeId").val());
                    },
                    error: function (data) {
                        if(data.responseText == null){
                            Dialog.alert("失败","新建数据项失败");
                        }else{
                            Dialog.alert("失败",data.responseText);
                        }

                    }
                });
            }
        },
        error: function (data) {
            Dialog.alert("失败",data.responseText);
        }
    });
}

/**
 * 修改数据项-提交按钮响应函数
 */
function updateDictDataInfo(){
    var sortNo = $('#sortNo').val();
    var dictName = $('#dictName').val();
    var parentId = $('#parentName').val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        url: springUrl+"/dict/dictData_updateSave",
        data:{
            id:$('#id').val(),
            sortNo:sortNo,
            dictName:dictName,
            parentId:parentId
        },
        success:function (data) {
            Dialog.hide();
            $("#box-right").load(springUrl+"/dict/dictData_list?dictTypeId="+ $("#dictTypeId").val());
        },
        error: function (data) {
            Dialog.alert("失败","数据字典数据更新失败");
        }
    });
}
/**
 * Created with IntelliJ IDEA.
 * User: yaohaibin
 * Date: 13-9-12
 * Time: 下午1:38
 * 路由规则-条件表达式js
 */
//表单表List
var formList;
/**
 * 初始化页面
 */
$(document).ready(function(){
    //运算符初始化
    $("#operator").val('');
    var decode=decodeURI($("#exp").val()) ;
    $("#exp").val(decode) ;
    //加载一级下拉框
    loadSelectData();
    //确定按钮绑定事件
    $('#saveButton').click(function() {
        if(!validate2($("#routerForm"))) {
             return false;
        }
        saveResults();
    });
    //select
    $('#selectTable').change(function() {
      loadSelectField();
    });
    //保存表单
    $('#saveTable').click(function(){
         saveTable();
    });
    intTable();
    bindValidate($('#routerForm'));
});
/*
*保存选择的路由规则
*/
function saveResults(){
    var table=$("#valueTable tbody");
    var tr=$("<tr></tr>");
    var td0=$("<td style='display:none'></td>");
    var td1=$("<td></td>");
    var td2=$("<td></td>");
    var td3=$("<td></td>");
    var formId=$("#selectTable").val();
    var formName=$("#selectTable").find("option:selected").text();
    var itemName= $("#selectField").find("option:selected").text();     //字段名称
    var itemId=$("#selectField").val();
//    if(itemId==""){
//        Dialog.alert("提示信息","请选择字段信息");
//        return false;
//    }
    var opertor=$("#operator").val();
//    if(opertor=="") {
//          Dialog.alert("提示信息","请选择运算符");
//          return false;
//    }
    var inputValue =$("#resultsValue").val();
//    if(inputValue==""){
//        Dialog.alert("提示信息","请填写值");
//        return false;
//    }
    var tdtext= formName+"_"+itemName+" "+opertor +" "+'"'+inputValue+'"';
    var hiddenText=formId+"_"+itemId+" "+opertor +" "+'"'+inputValue+'"';
    td1.append(tdtext);
    td1.addClass("span2");
    td0.append(hiddenText);
    tr.append(td0) ;
    tr.append(td1);
    var select = $("<select id=\"sel\"></select>");
    var opt = $("<option value=\"0\">or</option>"+"<option value=\"1\">and</option>");
    select.append(opt);
    select.addClass("span2") ;
    td2.append(select);
    td2.addClass("span2");
    var btn=  $( "<a href='#' class='btn' title='删除' onclick='deleteRow(this);'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>");

    td3.append(btn);
    td3.addClass("span2");
    tr.append(td1) ;
    tr.append(td2) ;
    tr.append(td3) ;
    table.append(tr);
}
//保存选择的运算符
function operatorSave(value){
    $("#operator").val(value);
}
/**
 *  加载一级菜单
 */
function loadSelectData(){
    var formId=$("#formId").val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        async:false,
        data:{
            id : formId
        },
        url:springUrl + "/design/router_findForms",
        success:function (data) {
            formList=data;
	        for(var i=0;i<data.length;i++){
	            $("<option value='"+data[i].id+"'>"+data[i].formName+"</option>").appendTo($("#selectTable"));
	        }
        },
        error: function (data) {
            Dialog.alert("失败","加载表信息失败");
        }
    });
}
/**
 * 加载二级菜单
 */
function loadSelectField(){
  var code= $("#selectTable").val();
  var processDefinitionId= $("#processDefinitionId").val();
   $("#selectField").empty();
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id : code   ,
            formId: processDefinitionId
        },
        url:springUrl + "/design/router_findFormItems",
        success:function (data) {
            if(data.length==0){
                $("<option value=' '>请选择</option>").appendTo($("#selectField"));
            }else{
                for(var i=0;i<data.length;i++){
                    $("<option value='"+data[i].fieldName+"'>"+data[i].fieldChineseName+"</option>").appendTo($("#selectField"));
                }
            }
        },
        error: function (data) {
            Dialog.alert("失败","加载字段信息失败");
        }
    });
}
/**
 * 根据行Id删除行
 * @param rowId
 */
function deleteRow(obj){
    Dialog.confirm("删除提示","确定删除选中的数据?","确定","取消",function(result){
        if(result){
            var tr=obj.parentNode.parentNode;
            var tbody=tr.parentNode;
            tbody.removeChild(tr);
        }
    });
}
/**
 * 保存表单需要的数据
 */
function saveTable(){
   var valueId="";     //不能使用全局变量
//    valueName="";
    //获取tboday
   var tboday=$("#valueTable tbody");
    if(tboday.find("tr").length!=0){
        //获取tr
        tboday.find("tr").each(function(i){
             tboday.find("tr:eq(" + i + ")").find("td").each(function(j){
                 //第二个td 为名称 不使用 使用第一个隐藏的td内容
                  if(j==1||j==3) {

                  }else{
                      if(j==0) {
                          valueId+="E"+$(this).text();
                      }else{
                         if(i!=tboday.find("tr").length-1)
                         {   //拼字符串 最后3个空格和第一个空格不能省略  格式为：" "+value+"  "形式
                             valueId+=" "+ $(tboday.find("tr:eq(" + i + ")").find("td select")).find("option:selected").text()+" ";

                         }
                      }

                  }
             });
        });
    }else{
    }
    return valueId;
}
function intTable(){
	var exp=$.trim($("#exp").val());
	    if(exp==""){
		
            }else{
                    exp=exp.substring(2,exp.length-1) ;
                    var table=$("#valueTable tbody");
                    //行数据
                    var expression =exp.split(" or ");
                //    var idExpression=valueId.split(" or ");
                //    value= value.replace(/;/g," ");
                for(var i=0;i<expression.length;i++){
                    if(expression [i].indexOf(" and ")!=-1){
                        var trData=expression [i].split(" and ");
                        //            var trIdData=idExpression[i].split(" and ");
                        for(var j=0;j<trData.length;j++){
                            var tr=$("<tr></tr>");
                            var td0=$("<td style='display:none'></td>");
                            var td1=$("<td></td>");
                            var td2=$("<td></td>");
                            var td3=$("<td></td>");
                            var opt;
                            if(j==trData.length-1){
                                opt= $("<option value=\"0\" selected>or</option>"+"<option value=\"1\">and</option>");
                            }else{
                                opt= $("<option value=\"0\">or</option>"+"<option value=\"1\" selected>and</option>");
                            }
                            var itemId=trData [j];
                            var item=getFormItemInfo(itemId);
                            td1.append(item.itemName);
                            td1.addClass("span2");
                            td0.append(item.itemCode);
                            tr.append(td0) ;
                            tr.append(td1);
                            var select = $('<select id="sel"></select>');
                            select.append(opt);
                            select.addClass("span2") ;
                            td2.append(select);
                            td2.addClass("span2");
                            var btn=  $( "<a href='#' class='btn' title='删除' onclick='deleteRow(this);'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>");
                            td3.append(btn);
                            td3.addClass("span2");
                            tr.append(td2) ;
                            tr.append(td3) ;
                            table.append(tr);
                        }
                    }else{
                        var tr=$("<tr></tr>");
                        var td0=$("<td style='display:none'></td>");
                        var td1=$("<td></td>");
                        var td2=$("<td></td>");
                        var td3=$("<td></td>");
                        var opt;
                        opt= $("<option value=\"0\" selected>or</option>"+"<option value=\"1\">and</option>");
                        var itemId=expression [i];
                        var item=getFormItemInfo(itemId);

        //	            var itemId=idExpression[i];
                        td1.append(item.itemName);
                        td1.addClass("span2");
                        td0.append(item.itemCode);
                        tr.append(td0) ;
                        tr.append(td1);
                        var select = $('<select id="sel"></select>');
                        select.append(opt);
                        select.addClass("span2") ;
                        td2.append(select);
                        td2.addClass("span2");
                        var btn=  $( "<a href='#' class='btn' title='删除' onclick='deleteRow(this);'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>");
                        td3.append(btn);
                        td3.addClass("span2");
                        tr.append(td2) ;
                        tr.append(td3) ;
                        table.append(tr);
	                 }
	       }
	}
}
/**
 * 根据表达式 拆分code
 * @param code
 */
function getFormItemInfo(code){
   var item= new Object();
   var itemCode="";
   var itemName="";
   var opter="";
   var value ="";
   var formId="";
    code=code.substring(1,code.length);
    var arryList;
   if(code.indexOf("!=")!=-1){
       arryList=  code.split("!=")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter="!=";
       formId=code.split("!=")[0].split("_")[0];
       value=code.split("!=")[1];
   }else if(code.indexOf(">=")!=-1){
       arryList=  code.split(">=")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter=">=";
	   value=code.split(">=")[1];
       formId=code.split(">=")[0].split("_")[0];
   }else if(code.indexOf(">")!=-1){
       arryList=  code.split(">")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter=">";
	   value=code.split(">")[1];
       formId=code.split(">")[0].split("_")[0];
   }else if(code.indexOf("<=")!=-1){
       arryList=  code.split("<=")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter="<=";
	   value=code.split("<=")[1];
       formId =code.split("<=")[0].split("_")[0];
   }else if(code.indexOf("<")!=-1){
       arryList=  code.split("<")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter="<";
	   value=code.split("<")[1];
       formId= code.split("<")[0].split("_")[0];
   }else if(code.indexOf("==")!=-1){
       arryList=  code.split("==")[0].split("_");
       for(var num=1;num<arryList.length;num++){
           if(num==arryList.length-1){
               itemCode+=arryList[num];
           }else{
               itemCode+=arryList[num]+"_";
           }
       }
	   opter="==";
	   value=code.split("==")[1];
       formId =code.split("==")[0].split("_")[0];
   }
    var formName="";
   for(var i=0;i<formList.length;i++)  {
        var form=formList[i];
       if(form.id==formId){
           formName= form.formName ;
       }
   }
   itemCode=itemCode.replace(/(^\s*)|(\s*$)/g,'');
   item.itemCode=formId+"_"+itemCode+" "+opter+" "+value;
   jQuery.ajax({
       type:"POST",
       cache:false,
       dataType : 'json',
   	   async:false,
       data:{
           formId : formId,
           fieldName:itemCode
       },
       url:springUrl + "/design/router_getFormItemName",
       success:function (data) {
    	   itemName = data.itemName;
       },
       error: function (data) {
           Dialog.alert("失败","获取表达式名称失败");
       }
   });
   	item.itemName=formName+"_"+itemName+" "+opter+" "+value;
   	return item;
}
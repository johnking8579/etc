$(function(){
	reloadTaskNum();
	getProcessTaskAll();
});
function reloadTaskNum(){
	parent.getMyTaskNum("proxy","",false);
	var parentAllNum=parent.$(".proxyNum:eq(0)").text();
	if(parentAllNum!=null&&parentAllNum!=0){
		$(".allNum").text("("+parentAllNum+")");
		$(".allNum").parent().show();
	}else{
		$(".allNum").parent().parent().parent().remove();
	}
}
function show(obj,proxyName){
	$(".curr1").removeClass("curr1");
	obj.addClass("curr1");
	if(proxyName=="all"){
		$("#proxyContainer").find("div[id^='pane']").each(function(i){
			$(this).show();
		});
	}else{
		$("#proxyContainer").find("div[id^='pane']").not("#pane"+proxyName).each(function(){
			$(this).hide();
		});
		$("#pane"+proxyName).show();
	}
	parent.autoTabHeight($("#proxyContainer").height()+40,'proxy');
}
function more(paneId,processDefinitionId){
	var portlet=$("#pane"+paneId).find("#pdfId"+processDefinitionId);
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var byProxyUserId=portlet.find("tr:eq(0)").attr("byProxyerId");
	var url=springUrl+"/app/processSearch_index?"
		+"taskType=proxy&"
		+"processDefinitionId="+processDefinitionId+"&"
		+"processDefinitionKey="+processDefinitionKey+"&"
		+"addInfo="+byProxyUserId;
	parent.parent.$("#mainFrame").attr("src",url);
}
function reload(paneId,processDefinitionId){
	var portlet=$("#pane"+paneId).find("#pdfId"+processDefinitionId);
	
	//当前刷新流程的值
	var dataNum=portlet.find(".dataNum").text();
	//全部总数
	var allNum=$.trim($(".allNum").text()).replace("(","").replace(")","");
	//当前tab也上标记的数目
	var keyNum=$.trim($(".key"+paneId).find(".num").text()).replace("(","").replace(")","");
	
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var byProxyUserId=portlet.find("tr:eq(0)").attr("byProxyerId");
	var data={
		"pafKey":processDefinitionKey,
		"byProxyUserId":byProxyUserId
	};
	jQuery.ajax( {
        type: "POST",
        async:true,
        url:springUrl+'/app/processProxy_reload',
        dataType: "json",
        data:data,
        success: function(result) {
			for(var key in result){
				var proxyKeys=key.split(",");
				displayProcessTask(proxyKeys,result[key][processDefinitionId],portlet.parent(),"reload",processDefinitionId);
				var total=parseInt(allNum)-parseInt(keyNum)+parseInt(proxyKeys[3]);
				$(".allNum").text("("+total+")");
				parent.$(".proxyNum").text(total);
	        	$(".key"+paneId).find(".num").text("("+proxyKeys[3]+")");
			}
        }
	});
}
function getProcessTaskAll(){
	jQuery.ajax( {
		type: "POST",
		async:true,
		url:springUrl+'/app/processProxy_list',
		dataType: "json",
		success: function(result) {
			var hasData=false;
			for(var key in result){
				hasData=true;
				displayProxyTask(key,result[key]);
			}
			if(!hasData){
				$("div#noDataDiv").show();
			}
			parent.autoTabHeight($("#proxyContainer").height()+40,'proxy');
		}
	});
}
function displayProxyTask(proxyName,proxyTask){
	var newPortletDisplay=$("#proxyContainer").find(".proxyDiv:eq(0)").clone();
	var proxyKeys=proxyName.split(",");
	newPortletDisplay.find(".mline:eq(0)").html("<span class='mlinetxt'>代理<span class='ftx-04'> "+proxyKeys[2]+" </span>的审批</span>");
	$("div#proxyContainer").append(newPortletDisplay);
	$("#tab1").find(".clearfix:eq(0)").append('<li class="key'+proxyKeys[1]+' hide" data-widget="tab-item1" onclick="show($(this),\''+proxyKeys[1]+'\');">'+proxyKeys[2]+'<span class="num">('+proxyKeys[3]+')</span></li>');
	newPortletDisplay.attr("id","pane"+proxyKeys[1]);
	var hasData=false;
	for(var key in proxyTask){
		hasData=true;
		displayProcessTask(proxyKeys,proxyTask[key],newPortletDisplay,"add");
	}
	if(hasData==true){
		$('.key'+proxyKeys[1]).removeClass("hide");
		newPortletDisplay.removeClass("hide");
		$("div#noDataDiv").hide();
	}else{
		$('.key'+proxyKeys[1]).remove();
		newPortletDisplay.remove();
		$("div#noDataDiv").show();
	}
}
function displayProcessTask(proxyKeys,processTask,obj,addOrRealod,lastPefId){
	var newDisplay;
	if(addOrRealod=="reload"){
		if(processTask==null){
			newDisplay=obj.find("#pdfId"+lastPefId);
			newDisplay.remove();
			parent.autoTabHeight($("#proxyContainer").height()+40,'proxy');
			return;
		}
		newDisplay=obj.find("#pdfId"+processTask.processDefinitionId);
		//无记录删掉流程显示
		if(processTask.totalNum==0){
			newDisplay.remove();
			return;
		}
		newDisplay.find("tbody").html("");
	}else if(addOrRealod=="add"){
		//无记录不显示
		if(processTask.totalNum==0) return;
		newDisplay=obj.find(".taskPane:eq(0)").clone();
		obj.append(newDisplay);
	}
	newDisplay.attr("id","pdfId"+processTask.processDefinitionId);
	var dataListSize=0;
	if(processTask.dataList!=null){
		dataListSize=processTask.dataList.length;
	}
	var titleHtml='<div class="fl"><span class="ftx-05"> '+processTask.processDefinitionName+'- </span>有<span class="ftx-04 dataNum"> '+processTask.totalNum+'  </span>笔等待您审批</div>';
	if(processTask.totalNum>5){
		titleHtml+='<div class="fr"><a href="#" onclick="more(\''+proxyKeys[1]+'\',\''+processTask.processDefinitionId+'\');">更多<s class="icon-more"></s></a></div>';
	}
	newDisplay.find(".grid-ex:eq(0)").html(titleHtml);
	var th="<tr processDefinitionName='"+processTask.processDefinitionName+"'" +
	" processDefinitionId='"+processTask.processDefinitionId+"'"+
	" processDefinitionKey='"+processTask.processDefinitionKey+"'"+
	" tableName='"+processTask.tableName+"'"+
	" byProxyerId='"+proxyKeys[1]+"'"+
	" tableName='"+processTask.tableName+"'"+
	" bussinessColumns='"+JSON.stringify(processTask.bussinessColumns)+"'>";
	th+="<td class='status'>&nbsp;</td>";
	$.each(processTask.processColumns,function(i,c){
		if(i==0){
			th+="<th align='left'>"+c+"</th>";
		}else{
			th+="<th>"+c+"</th>";
		}
	});
	th+="<th>操作</th>";
	th+="</tr>";
	newDisplay.find("thead").html(th);
	if(dataListSize!=0){
		$.each(processTask.dataList,function(i,d){
			var tr="<tr class='todo' taskType='"+d.taskType+"'" +
			" processInstanceId='"+d.processInstanceId+"'"+
			" businessId='"+d.businessId+"'"+
			" taskId='"+d.taskId+"'"+
			" taskName='"+d.taskName+"'"+
			" nodeId='"+d.nodeId+"'"+
			" bySignerUserId='"+d.bySignerUserId+"'"+
			" realName='"+d.realName+"'"+
                "startTime='"+d.startTime+"'" +
                " status='"+d.status+"'"+
			" organizationName='"+d.organizationName+"'"+
            "processInstanceName='" +d.processInstanceName+"'"+
                "followCode='" +d.followCode+"'"+
                "processDefinitionIsOuter='" +d.processDefinitionIsOuter+"'"+
			">";
			var reasonTitle="";
			if(d.taskType=="addsigner"){
				reasonTitle+='加签：';
			}
			reasonTitle+=concatProcessInsanceName(d.followCode,d.processInstanceName);
			if(d.isFirstNode==true){
				return true;
			}
			if(d.status==2){ //拒绝的任务显示
				reasonTitle+='<span class="ftx-04">[拒绝]</span>';
			}else if(d.status==3){ //驳回的任务是我的申请，不应在此地显示
				return true;
			}		
			tr+="<td class='status'>&nbsp;</td>";
			tr+="<td width='25%' align='left' style='padding:0 0 0 10px'><a href='#' onclick=\"openTaskHandleIndex($(this))\">"+reasonTitle+"</a></td>";
			tr+="<td width='70'>"+d.applyName+"</td>";
			tr+="<td width='140'>"+d.startTime+"</td>";
			$.each(processTask.bussinessColumns,function(i,c){
				if(d.data!=null){
					if(d.data[c]==null){
						tr+="<td>&nbsp;</td>";
					}else{
						tr+="<td>"+d.data[c]+"</td>";
					}
				}else{
					tr+="<td>&nbsp;</td>";
				}
			});
            if(d.processDefinitionIsOuter == "0" && d.taskType=="owner")  {
                tr+="<td width='200'><a  href='#' onclick=\"approve($(this))\"><i class='icon-edit'></i>批准</a>&nbsp;"+
                "<a href='#' onclick=\"reback($(this))\"><i class='icon-edit'></i>拒绝</a>&nbsp;" +
                "<a href='#' onclick=\"reject($(this))\"><i class='icon-edit'></i>驳回</a>" +
                "<a href='#' onclick=\"detail($(this))\"><i class='icon-edit'></i>流程状态</a>" +
                '</td>';
            }else{
                tr+="<td width='200'><a  href='#' onclick=\"approveForUnite($(this))\"><i class='icon-edit'></i>批准</a>&nbsp;"+
                    "<a href='#' onclick=\"rebackForUnite($(this))\"><i class='icon-edit'></i>拒绝</a>&nbsp;" +
                    '</td>';
            }
			tr+="</tr>";
			newDisplay.find("tbody").append(tr);
		});
	}else{
		newDisplay.find("tbody").append("暂无数据");
	}
	newDisplay.removeClass("hide");
	parent.autoTabHeight($("#proxyContainer").height()+40,'proxy');
}
//节点图回调函数
function detail(obj) {
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote(processInstanceName + '|流程图跟踪', '/app/processApply_detail?instanceId=' + processInstanceId, 1000, 520);
}
/**
 * 弹出任务处理页面
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
//function openTaskHandleIndex(businessId,taskType,processName,processInstanceId,taskInstanceId,processDefinitionId,nodeId){
function openTaskHandleIndex(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskType=obj.parent().parent().attr("taskType");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionKey=obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processName =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionName");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskName =obj.parent().parent().attr("taskName");
    var startTime= obj.parent().parent().attr("startTime");
    var byProxyerId=obj.parent().parent().parent().parent().find("tr:eq(0)").attr("byProxyerId");
    var processDefinitionIsOuter = obj.parent().parent().attr("processDefinitionIsOuter");
    var arrObj = [];
    var obj = {};
    obj.nodeId = nodeId;
    obj.processInstanceId = processInstanceId;
    obj.buinessInstanceId = businessId;
    obj.taskType = taskType;
    obj.id = taskId;
    obj.name=encodeURI(encodeURI(taskName));
    startTime=startTime.replace(" ","T");
    obj.assignerId=byProxyerId;   //代理人
    obj.isProxy=1; //是否代理
    obj.processDefinitionKey = processDefinitionKey;
    obj.processDefinitionId=processDefinitionId;
    obj.processDefinitionIsOuter=processDefinitionIsOuter;
    arrObj.push(obj);
//    Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?businessId="+businessId+"&taskType="+taskType+"&processInstanceId="+processInstanceId+"&taskInstanceId="+taskInstanceId+"&processDefinitionId="+processDefinitionId+"&nodeId="+nodeId,"750","450");
    parent.parent.Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?oaTask="+JSON.stringify(arrObj)+"&startTime="+startTime,1000,520);
}


/**
 * 批准
 * @param processInstanceId
 * @param taskId
 * @param nodeId
 */
function approve(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var byProxyerId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("byProxyerId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var comment  = parent.parent.$("#approveComment").val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            id :taskId,
            processDefinitionId: processDefinitionId,
            nodeId:nodeId,
            taskType:taskType,
            buinessInstanceId:businessId,
            processInstanceId:processInstanceId,
            name:   taskName,
            startTime:   startTime,
            processDefinitionKey:processDefinitionKey,
            assignerId:byProxyerId, //代理人
            isProxy:1, //是否代理
            comments:comment,
            isQuick:"1"
        },
        url:springUrl + "/app/processTask_complete",
        success:function (data) {
            Dialog.hide();
            if(data.Success){
                reload(byProxyerId,processDefinitionId) ;
                parent.parent.Dialog.alert("提示信息",data.message);
            }else{
            	if(data.message=="notNull"){
            		openTaskHandleIndex(obj);
            	}else{
            		parent.parent.Dialog.alert("提示信息",data.message);
            	}
            }
        },
        error: function (data) {
        	parent.parent.Dialog.alert("提示信息","审批失败");
        }
    });
}

//驳回操作
function reject(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var byProxyerId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("byProxyerId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    parent.parent.Dialog.openRemote("驳回意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        assignerId:byProxyerId,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToFirst",
                    success:function (data) {
                        if(data.Success){
                            reload(byProxyerId,processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }else{
                        	parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                    	parent.parent.Dialog.alert("提示信息","驳回失败");
                    }
                });
                return false;
            }
        }
    ]);
}
//拒绝操作
function reback(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var byProxyerId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("byProxyerId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    parent.parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async:true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        taskType:taskType,
                        buinessInstanceId:businessId,
                        processInstanceId:processInstanceId,
                        name:   taskName,
                        startTime:   startTime,
                        processDefinitionKey:processDefinitionKey,
                        assignerId:byProxyerId,
                        comments:comment
                    },
                    url:springUrl + "/app/processTask_RejectToPrevious",
                    success:function (data) {
                        if(data.Success){
                            reload(byProxyerId,processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }else{
                        	parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                    	parent.parent.Dialog.alert("提示信息","拒绝失败");
                    }
                });
                return false;
            }
        }
    ]);
}
function downloadfile(id){
	 var url = springUrl + "/app/process_downloadfile?id=" + id;
	 window.location.href = encodeURI(url);
}

function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}

/**
 * 批准 for 统一待办任务
 */
function approveForUnite(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    var comment  = parent.parent.$("#approveComment").val();

    jQuery.ajax({
        type:"POST",
        cache:false,
        async : true,
        dataType : 'json',
        data:{
            id :taskId,
            processDefinitionId: processDefinitionId,
            nodeId:nodeId,
            buinessInstanceId: businessId,
            taskType:taskType,
            processDefinitionKey:processDefinitionKey,
            startTime: startTime,
            name: taskName,
            assignerId:bySignerUserId,
            isProxy:'1',
            addSignerUserId: '',
            processInstanceId: processInstanceId,
            processInstanceName: processInstanceName,
            comments:comment,
            resultType:"1"
        },
        url:springUrl + "/app/processTask_complete_unite",
        success:function (data) {
            if(data.Success){
                reload(processDefinitionId) ;
                parent.parent.Dialog.alert("提示信息",data.message);
            }else{
                if(data.message=="notNull"){
                    openTaskHandleIndex(obj);
                }else{
                    parent.parent.Dialog.alert("提示信息",data.message);
                }
            }
        },
        error: function (data) {
            parent.parent.Dialog.alert("提示信息","审批失败");
        }
    });
}

//拒绝操作 for 统一待办
function rebackForUnite(obj){
    var nodeId=obj.parent().parent().attr("nodeId");
    var businessId = obj.parent().parent().attr("businessId");
    var taskId = obj.parent().parent().attr("taskId");
    var processDefinitionId= obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionId");
    var taskType=obj.parent().parent().attr("taskType");
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var taskName =obj.parent().parent().attr("taskName");
    var bySignerUserId= obj.parent().parent().attr("bySignerUserId");
    var startTime= obj.parent().parent().attr("startTime");
    var processDefinitionKey =obj.parent().parent().parent().parent().find("tr:eq(0)").attr("processDefinitionKey");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote("拒绝意见",springUrl + "/app/processTask_approve_index",550,300,[
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function() {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function(modal) {
                var comment  = parent.parent.$("#approveComment").val();
                if(!parent.parent.checkValue()){
                    return false;
                }
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    async : true,
                    dataType : 'json',
                    data:{
                        id :taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId:nodeId,
                        buinessInstanceId: businessId,
                        taskType:taskType,
                        processDefinitionKey:processDefinitionKey,
                        startTime: startTime,
                        name: taskName,
                        assignerId:bySignerUserId,
                        isProxy:'1',
                        addSignerUserId: '',
                        processInstanceId: processInstanceId,
                        processInstanceName: processInstanceName,
                        comments:comment,
                        resultType:"2"
                    },
                    url:springUrl + "/app/processTask_complete_unite",
                    success:function (data) {
                        if(data.Success){
                            reload(processDefinitionId) ;
                            parent.parent.Dialog.hideModal(modal);
                            parent.parent.Dialog.alert("提示信息",data.message);
                        } else{
                            parent.parent.Dialog.alert("提示信息",data.message);
                        }
                    },
                    error: function (data) {
                        parent.parent.Dialog.alert("提示信息","审批失败");
                    }
                });
                return false;
            }
        }
    ]);
}
var personFlag = '';
var hiddenTrFlag = 1;
var table;
var dataTableQueryAsync = true;
var customColumns = [];

/**
 * 分页配置
 * @type {{pageUrl: string, scrollY: string, sendData: Function, columns: Array, btns: Array}}
 */


/**
 * 流程跟踪图
 * @param processInastanceId
 * @param processInatanceName
 */
function processTrackDetail(processInastanceId) {
    parent.Dialog.openRemote('流程图跟踪', '/app/processApply_detail?instanceId=' + processInastanceId, 1000, 520);
}

/**
 * 重新申请 驳回或拒绝到第一个节点时调用
 * @param taskId
 * @param taskName
 * @param processInstanceId
 * @param taskDefinitionKey
 */
function openTaskHandle(taskId, taskName, processInstanceId, taskDefinitionKey) {
    parent.Dialog.openRemote("重新申请", springUrl + "/app/taskHandle_againApply?taskId=" + taskId
        + "&taskName=" + encodeURI(encodeURI(taskName)) + "&processInstanceId=" + processInstanceId
        + "&taskDefinitionKey=" + taskDefinitionKey + "&mark=1", "1000", "620"
    );
}

/**
 * 批准
 * @param processInstanceId
 * @param taskId
 * @param nodeId
 */
var objectRowData = [];
function approve(processInstanceId) {
    var obj = objectRowData[processInstanceId];
    var nodeId = obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId = obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType = obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName = obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId = obj.aData.bySignerUserId;
    var startTime = obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = obj.aData.processInstanceName;
    var followCode = obj.aData.followCode;
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理

    jQuery.ajax({
        type: "POST",
        cache: false,
        dataType: 'json',
        data: {
            id: taskId,
            processDefinitionId: processDefinitionId,
            nodeId: nodeId,
            taskType: taskType,
            buinessInstanceId: businessId,
            processInstanceId: processInstanceId,
            name: taskName,
            addSignerUserId: bySignerUserId,
            startTime: startTime,
            processDefinitionKey: processDefinitionKey,
            processInstanceName: processInstanceName,
            assignerId: assignerId,   //代理人
            isProxy: isProxy,
            comments: "",
            isQuick: "1"
        },
        url: springUrl + "/app/processTask_complete",
        success: function (data) {
            if (data.Success) {
                refershTable();
                parent.Dialog.alert("提示信息", data.message);
            } else {
                if (data.message == "notNull") {
                    var obj = objectRowData[processInstanceId];
                    openTaskHandleIndex_daiban(processInstanceId, processInstanceName, startTime.replace(" ", "T"));
                } else {
                    parent.Dialog.alert("提示信息", data.message);
                }
            }
        },
        error: function (data) {
            parent.Dialog.alert("提示信息", "审批失败");
        }
    });
}

/**
 * 驳回操作
 * @param obj
 */
function reject(processInstanceId) {
    var obj = objectRowData[processInstanceId];
    var nodeId = obj.aData.currentTaskNoteInfo.nodeId;
    var businessId = obj.aData.businessInstanceId;
    var taskId = obj.aData.currentTaskNoteInfo.id;
    var processDefinitionId = obj.aData.currentTaskNoteInfo.processDefinitionId;
    var taskType = obj.aData.currentTaskNoteInfo.taskType;
    var processInstanceId = obj.aData.processInstanceId;
    var taskName = obj.aData.currentTaskNoteInfo.name;
    var bySignerUserId = obj.aData.bySignerUserId;
    var startTime = obj.aData.currentTaskNoteInfo.startTime;
    var processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
    var processInstanceName = obj.aData.processInstanceName;
    var assignerId = obj.aData.assignee;  //代理人
    var isProxy = obj.aData.isProxy;  //是否代理

    parent.Dialog.openRemote("驳回意见", springUrl + "/app/processTask_approve_index", 550, 300, [
        {
            'label': '返回',
            'class': 'inactive',
            'callback': function () {
            }
        } ,
        {
            'label': '确定',
            'class': 'active1',
            'callback': function (modal) {
                var comment = parent.$("#approveComment").val();
                if (!parent.checkValue()) {
                    return false;
                }
                jQuery.ajax({
                    type: "POST",
                    cache: false,
                    async: true,
                    dataType: 'json',
                    data: {
                        id: taskId,
                        processDefinitionId: processDefinitionId,
                        nodeId: nodeId,
                        taskType: taskType,
                        buinessInstanceId: businessId,
                        processInstanceId: processInstanceId,
                        name: taskName,
                        addSignerUserId: bySignerUserId,
                        startTime: startTime,
                        processDefinitionKey: processDefinitionKey,
                        processInstanceName: processInstanceName,
                        assignerId: assignerId,   //代理人
                        isProxy: isProxy,
                        comments: comment
                    },
                    url: springUrl + "/app/processTask_RejectToFirst",
                    success: function (data) {
                        if (data.Success) {
                            parent.Dialog.alert("提示信息", data.message);
                            refershTable();
                            parent.Dialog.hideModal(modal);
                        } else {
                            parent.Dialog.alert("提示信息", data.message);
                        }
                    },
                    error: function (data) {
                        parent.Dialog.alert("提示信息", "驳回失败!");
                    }
                });
                return false;
            }
        }
    ]);
}


/**
 * 申请人，代理人，被代理人点击弹出框 选择人员
 */
function remoteOrgUserInfo() {
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
//                cancel();
            }
        },
        {
            "label": "确定",
            "class": "active1",
            "callback": function () {
                var selectRecords = Table.getSelectedRows(parent.orgUserDataTable);
                if (selectRecords.length > 1) {
                    parent.Dialog.alert("提示信息", "每次只能选择一个人员");
                    return false;
                } else {
                    if (selectRecords.length == 0) {
                        setUserInfo('', '');
                    }
                    else {
                        setUserInfo(selectRecords[0].id, selectRecords[0].realName);
                    }
                }

            }
        }
    ];
    parent.Dialog.openRemote('选择人员', '/system/sysAddress_getOrgUser', 800, 500, buttons);
}


function setUserInfo(userId, userName) {
    $("#" + personFlag + "PersonId").val(userId);
    $("#" + personFlag + "Person").val(userName);
}


function processStatusChange() {
    return;
    $("#processStatue").empty();
    if ($("#processTypeId").val() != "") {
        jQuery.ajax({
            type: "POST",
            url: springUrl + "/dict/dictData_findDictDataListByPid",
            data: {
                pid: $("#processTypeId").val().split("-")[0]
            },
            success: function (data) {
                for (var i = 0; i < data.length; i++) {

                    $("#processStatue").append("<option value=" + data[i].dictCode + ">" + data[i].dictName + "</option>");

                }
            },
            error: function (data) {
                parent.Dialog.alert("失败", "流程状态值联动失败");
            }
        });
    }
}
/*
 * 获取动态过滤条件
 */
function getFilters() {
    var filters = [];
    $("#dynamicSearchBox > div").each(function () {
        var key = $(this).attr("id");
        var option = $("#itemCode option[value='" + key.substring(3) + "']");
        var obj = {};
        obj.fieldName = option.attr("fieldName");
        obj.dataType = option.attr("dataType");
        obj.inputType = option.attr("inputType");

        if (obj.inputType == '日期') {
            obj.computeExpress = $(this).find("input:eq(2)").val();
            obj.computeExpressDesc = $(this).find("input:eq(5)").val();
        } else if (obj.inputType == '列表') {
            obj.computeExpress = $(this).find("option:selected").val();
        } else if (obj.inputType == '单选按纽组') {
            obj.computeExpress = $(this).find("input:checked").val();
        }
        else {
            obj.computeExpress = $(this).find("input:eq(0)").val();
            obj.computeExpressDesc = $(this).find("input:eq(1)").val();
        }

        filters.push(obj);
    });

    return filters;
}


/**
 * 页面初始化加载
 */

$(function () {

    /**
     * 数据字典-数据项查询绑定事件
     */
    $('#processSearchFun').click(function () {
        var textSelect = $("#processTypeId").find("option:selected").text();
        var valSelect = $("#processTypeId").find("option:selected").val();
        switch (valSelect) {
            case 'DICTDATA_0000106_01-applyYourself' :  //自己申请的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApply";
                break;
            case 'DICTDATA_0000106_02-tobeapproved' :  //要审批的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApproval";
                break;
            case 'DICTDATA_0000106_03-approvalOver' :  //审批过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApprovalOver";
                break;
            case 'DICTDATA_0000106_06-toBeApprovalForOthers' :  //要代别人审批的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApproval";
                break;
            case 'DICTDATA_0000106_07-ApprovalOverForOthers' :  //代别人审批过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForInsteadApprovalOver";
                break;
            case 'DICTDATA_0000106_08-othersForwarding' :  //别人加签的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForForward";
                break;
            case 'DICTDATA_0000106_04-toBeSupervise' :  //要督办的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSupervise";
                break;
            case 'DICTDATA_0000106_05-superviseOver' :  //督办过的
                var oSettings = table.fnSettings();
                oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForSuperviseOver";
                break;
            default:
                break;
        }
        Table.render(table);
    });
    //选择员工
    $('#applyPersonDiv').click(function () {
        personFlag = 'apply';
        remoteOrgUserInfo();
    });
    $('#proxyPerson').click(function () {
        personFlag = 'proxy';
        remoteOrgUserInfo();
    });
    $('#proxiedPerson').click(function () {
        personFlag = 'proxied';
        remoteOrgUserInfo();
    });


    /*
     * 业务数据查询
     */
    $("#beginTimeBegin").datebox({
        formatter: formatD
    });
    $("#beginTimeEnd").datebox({
        formatter: formatD
    });

    $('body').delegate("#processInstanceName", "change", function () {

        $("#itemCode").html('');
        $("#processSearchBox").html('');

        if ($(this).val() == null || $(this).val() == "") return;
        $("#itemCode").append("<option value=''>请稍后...</option>");
        $("#dynamicSearchBox").html('');

        window.filter = {};
        jQuery.ajax({
            type: "GET",
            url: "/app/businessSearch_findItemsByPDK",
            dataType: "json",
            data: [
                { "name": "id", "value": $(this).val() }
            ],
            success: function (data) {
                $("#itemCode").html('');
                if (data && data.length > 0) {
                    $("#itemCode").append("<option value=''>--请选择--</option>");
                    for (var i = 0; i < data.length; i++) {
                        $("#itemCode").append("<option fieldName='" + data[i].fieldName + "' dataType='" + data[i].dataType + "' inputType='" + data[i].inputType + "' value='" + data[i].id + "'>" + data[i].fieldChineseName + "</option>");
                    }
                }

            },
            error: function (data) {
            }
        });


        jQuery.ajax({
            type: "GET",
            url: "/app/findQueryItemsName",
            data: [
                { "name": "id", "value": $(this).val() }
            ],
            success: function (data) {

                if (data != null && data != "") {
                    customColumns = data.split(",");
                    customColumns.pop()
                }

            },
            error: function (data) {
            }
        });

    });

    $("body").delegate("#itemCode", "change", function () {
        var item = $("#itemCode option:selected");//$(this);
        if ($(this).val() == null || $(this).val() == "") return;

        //没有选择过则添加
        var key = 'key' + $(this).val();
        if (!window.filter[key] || window.filter[key] == 0) {


            var d = [];
            d.push({ "name": "processDefinitionKey", "value": $("#processInstanceName option:selected").val() });
            d.push({ "name": "itemId", "value": $("#itemCode option:selected").val() });
            jQuery.ajax({
                type: "GET",
                url: "/app/businessSearch_getHtmlForQuery",
                data: d,
                beforeSend: function () {
                    $("#filter_loader").css("display", "");
                },
                success: function (data) {

                    if (data == null || data == '') return;
                    window.filter[key] = 1;

                    data = data.replace(/\<textarea/, "<input");
                    data = data.replace(/\>.*\<\/textarea\>/, "type='text' />");

                    item.css("display", "none");
                    var fragment = $("<div id='" + key + "'  style='float:left;margin-left:15px;'></div>");
                    fragment.append(item.html() + "&nbsp;");
                    fragment.append(data);
                    fragment.append('<div class="deleteFilter" style="background: url(&quot;/static/common/img/filter.png&quot;) no-repeat scroll -37px -62px transparent; cursor: pointer; display: inline-block; width: 7px; height: 7px; margin-left: 10px; padding-bottom: 5px;"></div>');
                    $("#dynamicSearchBox").append(fragment);

                    $("input[type=text]", "#" + key).val('');
                    $("input", "#" + key).attr("onkeypress", "");
                    $("img", "#" + key).css("display", "none");

                    $("input:not(:checked)", "#" + key).attr("onclick", "");


                    $("#filter_loader").css("display", "none");

                    var option = $("#itemCode option[value='" + key.substring(3) + "']");

                    if (option.attr("dataType") == "DATE") {
                        $('#' + option.attr("fieldName") + "_BeginDate").datebox({
                            formatter: formatD
                        });

                        $('#' + option.attr("fieldName") + "_EndDate").datebox({
                            formatter: formatD
                        });
                    }
                    changeHeight();
                },
                error: function (data) {
                }
            });

        } else if (window.filter[key] == 1) { //选择过就删除
            window.filter[key] == 0;
            item.css("display", "");
            $("#dynamicSearchBox").remove("div#" + key);
        }


    });

    //删除过滤条件
    $("#dynamicSearchBox").delegate(".deleteFilter", "click", function () {

        var key = $(this).parent().attr("id");
        $("#dynamicSearchBox").find("div#" + key).remove();
        window.filter[key] = 0;
        $("#itemCode option[value='" + key.substring(3) + "']").css("display", "");
    });


    $("#reSearch").click(reDrawTable);

});
/**
 * 复制一份表单
 * @param processDefinitionKey
 * @param businessObjectId
 * @param modal
 */
function copyForm(processDefinitionKey, businessObjectId, modal) {
    var data = {
        "processDefinitionKey": processDefinitionKey,
        "businessObjectId": businessObjectId
    };
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/app/processApply_copy",
        dataType: "json",
        data: data,
        success: function (result) {
            if (result == "success") {
                parent.Dialog.hideModal(modal);
                parent.Dialog.alert("复制成功", "复制表单成功，请到申请&待办页面查看使用！");
            }
        },
        error: function (msg) {
            parent.Dialog.alert("操作失败", msg.responseText);
        }
    });
}
/**
 * 弹出任务处理页面 申请时使用
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
function openTaskHandleIndex(businessId, processName, pafKey, status) {
    var url = springUrl + "/app/process_apply?processDefinitionKey=" + businessId;
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
            }
        },
        {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(pafKey, businessId, modal);
                return false;
            }
        }
//        ,
//        {
//            "label": "保存草稿",
//            "id": "saveDraftButton",
//            "class": "active1",
//            "callback": function (modal) {
//                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                frame.saveDraft(modal);
//                return false;
//            }
//        },
//        {
//            "label": "提交",
//            "id": "submitProcessButton",
//            "class": "active1",
//            "callback": function (modal) {
//                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                parent.Dialog.confirm("系统消息", "提交后将无法修改，确定提交申请？", "确定", "取消", function (result) {//提交前确认
//                    if (result) {
//                        frame.submitProcess(modal);
//                        return false;
//                    }
//                });
//                return false;
//            }
//        }
    ];
    parent.Dialog.openRemote(processName, url, 1000, 620, buttons);
}

/**
 * 草稿状态重新打开
 * @param processDefinitionKey 流程定义Key
 * @param processDefinitionName 流程定义名称
 * @param businessObjectId 业务实例ID
 */
function draftInfoProcess(processInstanceStatus, processDefinitionKey, businessObjectId, processDefinitionName) {
    var url = springUrl + "/app/process_apply?processDefinitionKey=" + processDefinitionKey
        + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=7" ;//默认为只读，因为是查看他人的草稿状态
    var buttons = [
        {
            "label": "取消",
            "class": "btn-cancel",
            "callback": function () {
            }
        },
        {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(processDefinitionKey, businessObjectId, modal);
                return false;
            }
        }
//        ,
//        {
//            "label": "保存草稿",
//            "class": "active1",
//            "callback": function (modal) {
//                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                frame.saveDraft(modal);
//                return false;
//            }
//        },
//        {
//            "label": "提交",
//            "class": "active1",
//            "callback": function (modal) {
//                var frame = parent.document.getElementById('bindReportFrame').contentWindow;
//                parent.Dialog.confirm("系统消息", "提交后将无法修改，确定提交申请？", "确定", "取消", function (result) {//提交前确认
//                    if (result) {
//                        frame.submitProcess(modal);
//                        return false;
//                    }
//                });
//                return false;
//            }
//        }
    ];
    parent.Dialog.openRemote(processDefinitionName, url, 1000, 620, buttons);
}

/**
 * 更多条件--精简条件
 * @param thisLi
 */
function toggleCondition(thisLi) {
    if (hiddenTrFlag != 0) {
        $(".hiddenTr").show();
        thisLi.removeClass("close11").html("精简筛选条件<b></b>");
        hiddenTrFlag = 0;
    }
    else {
        $(".hiddenTr").hide();
        thisLi.addClass("close11").html("更多筛选条件<b></b>");
        hiddenTrFlag = 1;
    }
}
/**
 * 取消申请
 * @param processInstanceKey
 */
function cancelApply(processInstanceKey, status) {
    var url = springUrl + "/app/processSearch_cancelApplyForRejected";
    if (status == '6') {
        url = springUrl + "/app/processSearch_cancelApplyForUnapproved";
    }
    parent.Dialog.confirm("确认", "确定取消申请?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                url: url,
                data: {
                    processInstanceId: processInstanceKey
                },
                success: function (data) {
                    if (data.operator == false) {
                        parent.Dialog.alert("失败！", data.message);
                        return false;
                    }
                    else {
                        parent.Dialog.alert("成功！", data.message);
                        Table.render(table);
                    }
                }
            });
        }
    });
}

/**
 * 草稿取消申请
 * @param processInstanceKey
 * @param processInstanceId
 */
function cancelApplyForDraft(processInstanceKey, processInstanceId) {
    parent.Dialog.confirm("确认", "确定取消申请?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                url: springUrl + "/app/processSearch_cancelApplyForDraft",
                data: {
                    processInstanceId: processInstanceId,
                    processInstanceKey: processInstanceKey
                },
                success: function (data) {
                    if (data.operator == false) {
                        parent.Dialog.alert("失败！", data.message);
                        return false;
                    }
                    else {
                        parent.Dialog.alert("成功！", data.message);
                        Table.render(table);
                    }
                },
                error: function (data) {
                    parent.Dialog.alert("失败", "取消申请失败");
                }
            });
        }
    });
}

function loadCondition(divId, id, thisEle) {
    $("li").removeClass("curr");
    thisEle.addClass("curr");
    $("#" + divId).load(springUrl + "/app/processSearch_toggleCondition?processTypeId=" + id + "&flag=" + hiddenTrFlag);
    resetSelect(id);
}

function resetSelect(id) {
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/app/processSearch_resetCondition",
        data: {
            processTypeId: id
        },
        success: function (data) {
            $("#processTypeInfoId").val(id);
            $("#processInstanceName").empty();
            $("#processInstanceName").append("<option value=''>请选择...</option>");
            for (var i = 0; i < data.length; i++) {
                $("#processInstanceName").append("<option value=" + data[i].pafProcessDefinitionId + ">" + data[i].processDefinitionName + "</option>");
            }
        },
        error: function (data) {
            parent.Dialog.alert("失败", "重置流程状态查询条件失败");
        }
    });
}

/**
 * 分类查找
 */
function getColumnsByType(title) {
    var obj = $("option[fieldName=" + title + "]", "#itemCode");
    var inputType = obj.attr("inputtype");
    if (inputType == "日期") {
        return {"mData": title, "sTitle": obj.text(), "bSortable": false, "bVisible": true, "mRender": function (data) {
            return new Date(data).formatDate("yyyy-MM-dd");
        }
        };
    } else {
        return {"mData": title, "sTitle": obj.text(), "bSortable": false, "bVisible": true};
    }

}

function dynamicOptions() {
    var dynamicColumns = [];
    dynamicColumns.push({ "mData": "processInstanceName", "sTitle": "主题", "bSortable": false, "bVisible": true, fnRender: function (obj) {
        var status = obj.aData.status;
        var textSelect = "DICTDATA_0000106_01-applyYourself";

        var html = getShiyouHtml(status, textSelect, obj);
        return html;
    }
    });
    dynamicColumns.push({ "mData": "status", "sTitle": "状态", "bSortable": false, "bVisible": true, fnRender: function (obj) {
        var status = obj.aData.status;

        if (status == null || status == '') return '';
        var html = $("#processStatue").find("option").eq(parseInt(status) + 1).text();
        return html;
    }
    });
    dynamicColumns.push({ "mData": "realName", "sTitle": "申请人", "bSortable": false, "bVisible": true });

var Columnslength = customColumns.length + 4;
    for (var i = 0; i < customColumns.length; i++) {
        dynamicColumns.push(getColumnsByType(customColumns[i]));
    }

    dynamicColumns.push({ "mData": "beginTime", "sTitle": "申请时间", "bSortable": false, "bVisible": true, "mRender": function (data) {
        return new Date(data).formatDate("yyyy-MM-dd hh:mm:ss");
    }});

    var options = {
        pageUrl: "/app/businessSearch_findPageForMyApply",
        scrollY: "100%",
        defaultSort:[[Columnslength,"desc"]],
        sendData: function (sSource, aoData, fnCallback) {

            var id = $("#processInstanceName").find("option:selected").val();
            var processInstanceName = $("#processInstanceNameZT").val();
            var status = $("#processStatue").find("option:selected").val();
            var applyPersonId = $("#applyPersonId").val();

            aoData.push({ "name": "id", "value": id });
            aoData.push({ "name": "status", "value": status });
            aoData.push({ "name": "processInstanceName", "value": processInstanceName });
            aoData.push({ "name": "beginTimeBegin", "value": $("#beginTimeBegin").datebox('getValue') });
            aoData.push({ "name": "beginTimeEnd", "value": $("#beginTimeEnd").datebox('getValue') });
            aoData.push({ "name": "applyPersonId", "value": applyPersonId });
            var filters = getFilters();


            aoData.push({ "name": "filters", "value": JSON.stringify(filters) });

            jQuery.ajax({
                type: "POST",
                url: sSource,
                async: dataTableQueryAsync,
                dataType: "json",
                data: aoData,
                success: function (json) {
                    fnCallback(json);
                    dataTableQueryAsync = true;
                },
                error: function (data) {
                    var sAjaxSource;
                    if (table != null) {
                        sAjaxSource = table.fnSettings().sAjaxSource;
                    }
                    if (sAjaxSource == null || sAjaxSource == "/app") {
                    } else {
                        parent.Dialog.alert("失败", "查询流程任务列表失败");
                    }
                    dataTableQueryAsync = true;
                    changeHeight();
                }
            });
        },
        columns: dynamicColumns,
        btns: [],
        callback_fn: function () {
            changeHeight();
        }
    };

    return options;
}


function reDrawTable() {
    var processInstanceName = $("#processInstanceName").find("option:selected").val();
    $("#processSearchBox").html('');
    $("#processSearchBox").html('<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered display"   id="processSearchTable" width="100%"></table>');

    //判断Nuber字段是否为数字
    var filters = getFilters();
    for (var i = 0; i < filters.length; i++) {
        var dt = filters[i];
        if (dt.dataType == 'NUMBER') {
            if (dt.computeExpress != null && dt.computeExpress != '' && isNaN(parseFloat(dt.computeExpress))) {
                alert("【" + $("#itemCode option[fieldName='" + dt.fieldName + "']").text() + "】字段只能填写数字！");
                return;
            }
            if (dt.computeExpressDesc != null && dt.computeExpressDesc != '' && isNaN(parseFloat(dt.computeExpressDesc))) {
                alert("【" + $("#itemCode option[fieldName='" + dt.fieldName + "']").text() + "】字段只能填写数字！");
                return;
            }
        }
    }

    table = Table.dataTable("processSearchTable", dynamicOptions());

}

/**
 * 查看流程实例历史详情
 * @param processInstanceId
 */
function detailProcessInstance(processInstanceId, pafKey, businessId) {
    var buttons = [
        {
            "label": "返回",
            "class": "active1",
            "callback": function () {
            }
        }
    ];
    if (pafKey != null && pafKey != "") {
        buttons[1] = {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(pafKey, businessId, modal);
                return false;
            }
        }
    }


    parent.Dialog.openRemote("查看流程实例历史详情", springUrl + "/app/endProcessInstance_index?processInstanceId=" + processInstanceId, 1000, 620, buttons);
}

/**
 * 主题点击弹出页面 代办情况下
 * @param status
 * @param valueSelect
 * @param obj
 */
var arrayParam = [];

/**
 * 草稿取消状态
 */
function draftCancel(processInstanceStatus, processDefinitionKey, businessObjectId, processDefinitionName){
    var url = springUrl + "/app/process_apply?processDefinitionKey=" + processDefinitionKey
        + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=7" ;
    var buttons = [
        {
            "label": "取消",
            "class": "btn-cancel",
            "callback": function () {
            }
        }]
    parent.Dialog.openRemote(processDefinitionName, url, 1000, 620, buttons);
}
function getShiyouHtml(status, valueSelect, obj) {
    var html = "";
    switch (valueSelect) {
        case 'DICTDATA_0000106_01-applyYourself' ://自己申请的
            if (status == '0') {
                //processInstanceStatus, processDefinitionKey, businessObjectId, processDefinitionName
                html = "<a href='#' onclick='draftInfoProcess(\"" + status + "\",\"" + obj.aData.pafProcessDefinitionId + "\",\""
                    + obj.aData.businessInstanceId + "\",\"" + obj.aData.processDefinitionName + "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
            }else if (typeof(obj.aData.processInstanceId) == "undefined"&&status==4){
                html = "<a href='#' onclick='draftCancel(\""+status+"\",\""+obj.aData.pafProcessDefinitionId + "\",\""
                    +obj.aData.businessInstanceId+"\",\""+obj.aData.processDefinitionName+ "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";

            } else {
                html = "<a href='#' onclick='detailProcessInstance(\"" + obj.aData.processInstanceId + "\",\"" + obj.aData.pafProcessDefinitionId + "\",\"" +
                    obj.aData.businessInstanceId + "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
            }
            break;
        case 'DICTDATA_0000106_02-tobeapproved' ://要审批的
        case 'DICTDATA_0000106_06-toBeApprovalForOthers' ://要代别人审批的
            var arrObj = [];
            var object = {};
            object.nodeId = typeof(obj.aData.currentTaskNoteInfo.nodeId) != 'undefined' ? obj.aData.currentTaskNoteInfo.nodeId : "";
            object.processInstanceId = obj.aData.processInstanceId;
            object.buinessInstanceId = obj.aData.businessInstanceId;
            object.taskType = obj.aData.currentTaskNoteInfo.taskType;
            object.id = obj.aData.currentTaskNoteInfo.id;
            object.name = encodeURI(encodeURI(obj.aData.currentTaskNoteInfo.name));
            var startTime = typeof(obj.aData.currentTaskNoteInfo.startTime) != 'undefined' ?
                obj.aData.currentTaskNoteInfo.startTime.replace(" ", "T") : "";
            ;
            object.addSignerUserId = obj.aData.bySignerUserId;   //加签人
            object.processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
            object.processDefinitionId = obj.aData.currentTaskNoteInfo.processDefinitionId;
            object.processInstanceName = encodeURI(encodeURI(obj.aData.processInstanceName)); //主题名称
            object.assignerId = obj.aData.assignee;  //代理人
            object.isProxy = obj.aData.isProxy;  //是否代理

            arrObj.push(object);
            var processName = typeof(obj.aData.currentTaskNoteInfo.processDefinitionName) != 'undefined' ?
                obj.aData.currentTaskNoteInfo.processDefinitionName : obj.aData.processInstanceName;
            var parmObj = JSON.stringify(arrObj);
            arrayParam[object.processInstanceId] = parmObj;
            html = '<a href="#" onclick="openTaskHandleIndex_daiban(\'' + object.processInstanceId + '\',\'' + processName + '\',\'' + startTime + '\')">' + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + '</a>';
            break;
        case 'DICTDATA_0000106_08-othersForwarding' ://别人加签的
            var arrObj = [];
            var object = {};
            object.nodeId = typeof(obj.aData.currentTaskNoteInfo.nodeId) != 'undefined' ? obj.aData.currentTaskNoteInfo.nodeId : "";
            object.processInstanceId = obj.aData.processInstanceId;
            object.buinessInstanceId = obj.aData.businessInstanceId;
            object.taskType = obj.aData.currentTaskNoteInfo.taskType;
            object.id = obj.aData.currentTaskNoteInfo.id;
            object.name = encodeURI(encodeURI(obj.aData.currentTaskNoteInfo.name));
            var startTime = typeof(obj.aData.currentTaskNoteInfo.startTime) != 'undefined' ?
                obj.aData.currentTaskNoteInfo.startTime.replace(" ", "T") : "";
            ;
            object.addSignerUserId = obj.aData.bySignerUserId;   //加签人
            object.processDefinitionKey = obj.aData.currentTaskNoteInfo.processDefinitionKey;
            object.processDefinitionId = obj.aData.currentTaskNoteInfo.processDefinitionId;
            object.processInstanceName = encodeURI(encodeURI(obj.aData.processInstanceName)); //主题名称
            arrObj.push(object);
            var processName = typeof(obj.aData.currentTaskNoteInfo.processDefinitionName) != 'undefined' ?
                obj.aData.currentTaskNoteInfo.processDefinitionName : obj.aData.processInstanceName;
            var parmObj = JSON.stringify(arrObj);
            arrayParam[object.processInstanceId] = parmObj;
            html = '<a href="#" onclick="openTaskHandleIndex_daiban(\'' + object.processInstanceId + '\',\'' + processName + '\',\'' + startTime + '\')">' + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + '</a>';
            break;
        case 'DICTDATA_0000106_03-approvalOver' : //审批过的 
        case 'DICTDATA_0000106_05-superviseOver' : //督办过的
        case 'DICTDATA_0000106_07-ApprovalOverForOthers'://代别人审批过的
        case 'DICTDATA_0000106_04-toBeSupervise' ://要督办的 
            html = "<a href='#' onclick='detailProcessInstance(\"" + obj.aData.processInstanceId + "\");'>" + concatProcessInsanceName(obj.aData.followCode,obj.aData.processInstanceName) + "</a>";
            break;
        default:
            break;
    }
    return html;
}

/**
 * 弹出任务处理页面 流程代办时使用
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
function openTaskHandleIndex_daiban(processInstanceId, processName, startTime) {
    parent.Dialog.openRemote(processName, springUrl + "/app/taskHandle_index?oaTask=" + arrayParam[processInstanceId] + "&startTime=" + startTime, "1000", "520");
}

/**
 * 刷新table
 */
function refershTable() {
    var oSettings = table.fnSettings();
    if (oSettings.sAjaxSource == '/app') {
        oSettings.sAjaxSource = springUrl + "/app/processSearch_findPageForMyApply";
    }
    Table.render(table);
}


function formatD(date) {
    var date = new Date(date);
    return date.formatDate('yyyy-MM-dd');
}


Date.prototype.formatDate = function (format) {
    var date = this;
    if (!format) {
        format = "yyyy-MM-dd";
    }
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = "0" + month;
    }
    var year = date.getFullYear();
    format = format.replace("MM", month.toString());
    if (format.indexOf("yyyy") > -1) {
        format = format.replace("yyyy", year.toString());
    } else if (format.indexOf("yy") > -1) {
        format = format.replace("yy", year.toString().substr(2, 2));
    }
    var day = date.getDate();
    if (day < 10) {
        day = "0" + day;
    }
    format = format.replace("dd", day.toString());
    var hours = date.getHours();
    if (format.indexOf("t") > -1) {
        if (hours > 11) {
            format = format.replace("t", "pm");
        } else {
            format = format.replace("t", "am");
        }
    }
    if (format.indexOf("HH") > -1) {
        format = format.replace("HH", hours.toString());
    }
    if (format.indexOf("hh") > -1) {
        if (hours > 12) {
            hours - 12;
        }
        if (hours == 0) {
            hours = 12;
        }
        format = format.replace("hh", hours.toString());
    }
    var minutes = date.getMinutes();
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (format.indexOf("mm") > -1) {
        format = format.replace("mm", minutes.toString());
    }
    var seconds = date.getSeconds();
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    if (format.indexOf("ss") > -1) {
        format = format.replace("ss", seconds.toString());
    }
    return format;

}


function radioClick(event) {
    var e = window.event || event;
    var element = e.target || e.srcElement;


    $("input:radio", $(element).parent().parent()).next().removeClass('label').removeClass('label-warning');
    $(element).next().addClass('label').addClass('label-warning');


    return true;

}

function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}
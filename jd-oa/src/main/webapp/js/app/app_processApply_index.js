/**
 * Created with IntelliJ IDEA.
 * Desctription:我的申请页面 JS
 * User: yujiahe
 * Date: 13-10-24
 * Time: 上午11:03
 * To change this template use File | Settings | File Templates.
 */
var rejectNum = 0;//驳回个数
var allNum = 0
/**
 * 我的申请 全部 数量设置 tab页 以及 上级页面的头部
 */
function setAllNum() {
    if (allNum > 0) {
        $("#allNum").text("（" + allNum + "）");
        $("#showAll").show();
        parent.setNum("myApply",allNum);
        parent.$(".myApplyNum").text(allNum);
    } else {

        $("#showAll").hide();
        parent.$(".myApplyNum").text(allNum);
        parent.setNum("myApply",allNum);
        $("#allNum").text("（" + allNum + "）");
//        parent.$("myApplyTip").hide();
    }
}
$(function () {
    //所有步骤的数据
    $("#showAll").hide();
    $("#rejectChk").hide();
    if (check != '' && typeof(check) != 'undefined') { //如果有我的申请数据
//       转换为json对象
        var listProcessTask = eval('(' + check + ')');
        allNum = listProcessTask.length;
        setAllNum();
        for (var i = 0; i < listProcessTask.length; i++) {
            var oaTaskInstanceList = listProcessTask[i].oaTaskInstanceList;
            var processInstanceName = listProcessTask[i].processInstanceName;//实例名称
            var followCode = listProcessTask[i].followCode;//实例单号
            var processBeginTime = listProcessTask[i].beginTime;//实例起始时间
            var processInstanceId = listProcessTask[i].processInstanceId;//流程实例ID
            var processInsPrimayKey = listProcessTask[i].processInsPrimayKey;//流程实例主键

            var processDefinitionKey = listProcessTask[i].pafProcessDefinitionId;//流程对应的paf key
            var processDefinitionName = listProcessTask[i].processDifinitionName;//流程名称
            var businessObjectId = listProcessTask[i].businessInstanceId;//流程对应的业务ID

            //驳回部分加载
            var currentStatus = listProcessTask[i].currentStatus;//流程实例的当前状态
            var isFirstNode = listProcessTask[i].firstNode;//当前节点是否是首节点

            var rejectTabHtml = "";//[草稿]/[驳回]/[拒绝]
            var cancelBtn = "";//取消申请
            var applyAgain = "";//重新申请
            
            var candidateUsersHtml="";
//            if(oaTaskInstanceList != null && oaTaskInstanceList.length>0){
//              var  oaTaskInstanceListLength = oaTaskInstanceList.length;  
//              var  taskId = oaTaskInstanceList[oaTaskInstanceListLength-1].id;
//                 candidateUsersHtml=
//                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' style='color:#7ABD54' class='findCandidateUsers' processInstanceId = '" + processInstanceId +"'  title='test' taskId='" + taskId + "'>查看审批人</a>";
//            }
            
            
            if (currentStatus == "0"
                || currentStatus == "3"
                || (currentStatus == '2' && isFirstNode == true)
                || currentStatus == '6') {
                if (currentStatus == "0") {
                    rejectTabHtml = "&nbsp;&nbsp;<span class='ftx-04'>[草稿]</span>";
                }
                if (currentStatus == "3") {
                    rejectTabHtml = "&nbsp;&nbsp;<span class='ftx-04'>[驳回]</span>";
                }
                if (currentStatus == "2") {
                    rejectTabHtml = "&nbsp;&nbsp;<span class='ftx-04'>[拒绝]</span>";
                }
                //驳回/草稿/拒绝且是首节点的场合 可以取消申请
                if (currentStatus == "0") {
                    //草稿状态时 依据流程主键删除流程
                    cancelBtn = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' class='cancelApplyCss' currentStatus='" + currentStatus 
                    	+ "' processInsPrimayKey='" + processInsPrimayKey + "'>取消申请</a>";
                } else {
                    //其它状态时依据流程实例ID删除流程
                	cancelBtn = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' class='cancelApplyCss' currentStatus='" + currentStatus 
                		+ "' processInsPrimayKey='" + processInstanceId + "'>取消申请</a>";
                }
                //重新发起申请
                if (currentStatus != '6') {
                    if (oaTaskInstanceList.length > 0) {
                        applyAgain = "&nbsp;&nbsp<input type='submit' class='active1 ml20' value='重新申请' onClick='openTaskHandle(\""
                        	+ processInstanceName + "\",\""
                        	+ oaTaskInstanceList[0].id + "\",\"" 
                            + oaTaskInstanceList[0].name + "\",\"" 
                            + listProcessTask[i].processInstanceId + "\",\"" + oaTaskInstanceList[0].taskDefinitionKey + "\")'>";
                    } else {
                        //草稿状态的场合
                        applyAgain = "&nbsp;&nbsp<input type='submit' class='active1 ml20' value='继续申请' onClick='reApplyForDraft(\""
                            + processDefinitionKey + "\",\"" + processInstanceName + "\",\"" + businessObjectId + "\",\"" + currentStatus + "\")'>";
                    }
                }
            }
            if (currentStatus == '3') { //驳回状态的情况
                ++rejectNum; //驳回实例的计数
                candidateUsersHtml="";
                var rejectedHtml = "<p class='text-info' style='cursor:pointer' id='p_reject_" + processInstanceId + "'>" +
                "<a herf='javascript:void(0);' class='detaiDraftCss' style='cursor:pointer' processInstanceId='" + processInstanceId + "' " +
                    "processDefinitionKey='"+ processDefinitionKey +"'  businessObjectId='"+ businessObjectId +"'>"+concatProcessInsanceName(followCode,processInstanceName) + "</a> " +"  -   （" + processBeginTime + "）" + rejectTabHtml + cancelBtn + applyAgain + "</p>" + //显示流程名称及时间
                    "<input type='hidden' id='processTask" + i + "Id' value='" + processInstanceId + "'/> " +  //在隐藏域中存入流程实例ID
                    "<input type='hidden' id='processTask" + i + "Name'  value='" + processInstanceName + "'/> " +  //在隐藏域中存入实例名称
                    "<input type='hidden' id='processTask" + i + "Time'  value='" + processBeginTime + "'/> " +  //在隐藏域中存入实例起始时间
                    "<input type='hidden' id='processTask" + i + "currentStatus'  value='" + currentStatus + "'/> " +  //在隐藏域中存入状态
                    " <div id='div_reject_" + processInstanceId + "' class='step_context processTask" + i + "'></div><hr style='margin:0'/>"; //流程图的位置
                $("#rejected").append(rejectedHtml);
            }
//            全部实例加载过程
            var processTaskHtml = "";
            if (currentStatus != "0") { //非草稿状态
                processTaskHtml = "<p id='p_all_" + processInstanceId + "' class='text-info'>" +
                    " <a herf='javascript:void(0);' class='detaiDraftCss' style='cursor:pointer' processInstanceId='" + processInstanceId + "' " +
                    "processDefinitionKey='"+ processDefinitionKey +"'  businessObjectId='"+ businessObjectId +"'>" + concatProcessInsanceName(followCode,processInstanceName) + "</a>    "
                    + "   -   （" + processBeginTime + "）"+ rejectTabHtml +candidateUsersHtml + cancelBtn + applyAgain + "</p>" + //显示流程名称及时间
                    "<input type='hidden' id='processTask" + i + "Id' value='" + processInstanceId + "'/> " +  //在隐藏域中存入流程实例ID
                    "<input type='hidden' id='processTask" + i + "Name'  value='" + processInstanceName + "'/> " +  //在隐藏域中存入实例名称
                    "<input type='hidden' id='processTask" + i + "Time'  value='" + processBeginTime + "'/> " +  //在隐藏域中存入实例起始时间
                    "<input type='hidden' id='processTask" + i + "currentStatus'  value='" + currentStatus + "'/> " +  //在隐藏域中存入状态
                    "<div id='div_all_" + processInstanceId + "' class='step_context processTask" + i + "'></div><hr style='margin:0'/>"; //流程图的位置
            } else {  //草稿状态
                processTaskHtml = "<p id='p_all_" + processInsPrimayKey + "' class='text-info'>" +
                    " <a herf='javascript:void(0)' class='detailFDraftCss' style='cursor:pointer' processDefinitionKey='"+ processDefinitionKey +"' " +
                    		"processInstanceName='"+ processInstanceName +"' businessObjectId='"+ businessObjectId +"' currentStatus='"+ currentStatus +"'>" + concatProcessInsanceName(followCode,processInstanceName) + "</a>"
                    + "   -   （" + processBeginTime + "）" + rejectTabHtml + cancelBtn + applyAgain + "</p>" + //显示流程名称及时间
                    "<input type='hidden' id='processTask" + i + "Id' value='" + processInsPrimayKey + "'/> " +  //在隐藏域中存入流程实例ID
                    "<input type='hidden' id='processTask" + i + "Name'  value='" + processInstanceName + "'/> " +  //在隐藏域中存入实例名称
                    "<input type='hidden' id='processTask" + i + "Time'  value='" + processBeginTime + "'/> " +  //在隐藏域中存入实例起始时间
                    "<input type='hidden' id='processTask" + i + "currentStatus'  value='" + currentStatus + "'/> " +  //在隐藏域中存入状态
                    "<div id='div_all_" + processInsPrimayKey + "' class='step_context processTask" + i + "'></div><hr style='margin:0'/>"; //流程图的位置
            }
            $("#processTask").append(processTaskHtml);
            var currentStep = listProcessTask[i].currentStep;
            var listStep = listProcessTask[i].listStep;
            var listLength = listProcessTask[i].listStep.length - 1;

            var StepTool = new Step_Tool_dc("processTask" + i, listStep[listLength].StepNum, "detail");
            StepTool.drawStep(currentStep, listStep);
            
            if(currentStatus == '1' || currentStatus == '6'){
	        	if(oaTaskInstanceList != null && oaTaskInstanceList.length>0){
		            var  taskId = oaTaskInstanceList[oaTaskInstanceList.length-1].id;
		            findCandidateUsers(taskId,processInstanceId,currentStatus);  
		        }
            }
            if(currentStatus == '2'){
	        	if(oaTaskInstanceList != null && oaTaskInstanceList.length>0){
		            var  taskId = oaTaskInstanceList[oaTaskInstanceList.length-1].id;
		            findCandidateUsers(taskId,processInstanceId,currentStatus);  
		        }
            }
            
        }
//        显示驳回数目
        if (rejectNum > 0) {
            $("#rejectChk").show();
            $("#rejectNum").text("（" + rejectNum + "）");
//            parent.$(".rebackNum").text(rejectNum);
         //app_info.vm上数量设置
            parent.setNum("reject",rejectNum);
        } else {//如果无驳回，则不显示驳回TAB
            $("#rejectChk").hide();

        }
        ;
    } else { //无实例返回则显示
        $("#showAll").hide();
        $("#processTask").append("您目前暂无申请.");
        $("#rejectChk").hide();
       $("#myApplyTab").remove();
    }
//      页面加载时默认显示全部，隐藏被驳回
    $("#rejected").hide();
    $("#processTask").show();
//        如果点击驳回，则隐藏全部，显示驳回
    $("#rejectChk").bind("click", function () {
    	$("#rejectChk").addClass("curr1");
    	$("#showAll").removeClass("curr1");
        $("#rejected").show();
        $("#processTask").hide();
        parent.autoTabHeight($("#tab1").height()+50,'apply');
    });
//        如点击全部，则隐藏驳回，显示全部
    $("#showAll").bind("click", function () {
    	$("#rejectChk").removeClass("curr1");
    	$("#showAll").addClass("curr1");
        $("#rejected").hide();
        $("#processTask").show();
        parent.autoTabHeight($("#tab1").height()+50,'apply');
    });
});

//节点图回调函数
function detail(result) {
    var instanceId = $("#" + result.taskId + "Id").val();
    var instanceName = $("#" + result.taskId + "Name").val();
    var instanceTime = $("#" + result.taskId + "Time").val();
    var currentStatus = $("#" + result.taskId + "currentStatus").val();
    if (currentStatus == "0") {
        parent.parent.Dialog.alert("系统提示", "很抱歉，草稿状态下，无流程实例，无法返回流程实例图。");
    } else {
        parent.parent.Dialog.openRemote(instanceName + '|流程图跟踪', '/app/processApply_detail?instanceId=' + instanceId, 1000,520);
    }

}
$(function(){
	$(".cancelApplyCss").click(function(){
		var currentStatus = $(this).attr('currentStatus');
		var processInstanceId = $(this).attr('processInsPrimayKey');
		cancelApply(processInstanceId, currentStatus);
	});

	$(".findCandidateUsers").click(function(){
		var processInstanceId = $(this).attr('processInstanceId');
		var taskId = $(this).attr('taskId');
		findCandidateUsers(taskId,processInstanceId);
		 $(this).css('display','none');
	});

	$(".detaiDraftCss").click(function(){
		var processInstanceId = $(this).attr('processInstanceId');
        var processDefinitionKey = $(this).attr('processDefinitionKey');
        var businessObjectId = $(this).attr('businessObjectId');
		detailProcessInstance(processInstanceId,processDefinitionKey,businessObjectId);
	});
	$(".detailFDraftCss").click(function(){
        var currentStatus = $(this).attr('currentStatus');
        var processDefinitionKey = $(this).attr('processDefinitionKey');
        var processInstanceName = $(this).attr('processInstanceName');
        var businessObjectId = $(this).attr('businessObjectId');
//        var bool = confirm("编辑表单是否弹出新窗口页面?");
        var bool = false;
        if(bool){
            var url = springUrl + "/app/processApp_open_bindReport_alone?processDefinitionKey=" + processDefinitionKey
                + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=" + currentStatus;
            window.open(url);
        }else{
            openTaskHandleIndex(processDefinitionKey, processInstanceName, businessObjectId, currentStatus);
        }
	});
});
//草稿状态的的继续申请按钮事件
function reApplyForDraft(processDefinitionKey,processInstanceName,businessObjectId,currentStatus){
//    var bool = confirm("编辑表单是否弹出新窗口页面?");
    var bool = false;
    if(bool){
        var url = springUrl + "/app/processApp_open_bindReport_alone?processDefinitionKey=" + processDefinitionKey
            + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=" + currentStatus;
        window.open(url);
    }else{
        openTaskHandleIndex(processDefinitionKey, processInstanceName, businessObjectId, currentStatus);
    }
}

/**
 * 取消申请事件
 * @param obj
 */
function cancelApply(processInstanceId, status) {
    parent.parent.Dialog.confirm("确认", "确定取消申请?", "是", "否", function (result) {
        if (result) {
            if (status == "0") {
                jQuery.ajax({
                    type: "POST",
                    url: springUrl + "/app/processSearch_cancelApplyForDraft",
                    data: {
                        processInstanceKey: processInstanceId
                    },
                    success: function (data) {
                        if (data.operator == false) {
                            parent.parent.Dialog.alert("失败！", data.message);
                            return false;
                        }
                        else {
                            parent.parent.Dialog.alert("成功！", data.message);
                            //更新全部统计数字
                            --allNum;
                            setAllNum();
                            $("#div_all_" + processInstanceId).next().remove();
                            $("#div_all_" + processInstanceId).remove();
                            $("#p_all_" + processInstanceId).remove();
                        }
                        parent.autoTabHeight($("#tab1").height()+50,'apply');
                    }
                });
            }
            if (status == "3" || status == "2" || status == "6") {
            	var url = springUrl + "/app/processSearch_cancelApplyForRejected";
            	if(status == '6'){
            		url = springUrl + "/app/processSearch_cancelApplyForUnapproved";
            	}
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        processInstanceId: processInstanceId
                    },
                    success: function (data) {
                        if (data.operator == false) {
                            parent.parent.Dialog.alert("失败！", data.message);
                            return false;
                        }
                        else {
                            parent.parent.Dialog.alert("成功！", data.message);
                            //更新全部统计数字
                            --allNum;
                            setAllNum();
                            //更新驳回统计数字
                            if (status == '3') {
                                --rejectNum;
                                if (rejectNum > 0) {
                                    $("#rejectNum").text(rejectNum);
                                } else {//如果无驳回，则不显示驳回TAB
                                    $("#rejectChk").hide();
                                    $("#processTask").show();
                                }
                            }
                            $("#div_reject_" + processInstanceId).remove();
                            $("#div_all_" + processInstanceId).remove();
                            $("#p_reject_" + processInstanceId).remove();
                            $("#p_all_" + processInstanceId).remove();
                        }
                        parent.autoTabHeight($("#tab1").height()+50,'apply');
                    }
                });
            }
        }
    });

}

/**
 * 复制一份表单
 * @param processDefinitionKey
 * @param businessObjectId
 * @param modal
 */
function copyForm(processDefinitionKey,businessObjectId,modal){
    var data={
        "processDefinitionKey":processDefinitionKey,
        "businessObjectId":businessObjectId
    };
    jQuery.ajax( {
        type: "POST",
        url:springUrl+"/app/processApply_copy",
        dataType: "json",
        data: data,
        success: function(result) {
            if(result=="success"){
                parent.parent.Dialog.hideModal(modal);
                parent.parent.Dialog.alert("复制成功","复制表单成功，请到申请&待办页面查看使用！");
                parent.load('apply', springUrl + '/app/processApply_index', true);
            }
        },
        error:function(msg){
            parent.parent.Dialog.alert("操作失败",msg.responseText);
        }
    });
}

/**
 * 重新申请 事件
 * @param taskId
 * @param taskName
 * @param processInstanceId
 * @param taskDefinitionKey
 */
function openTaskHandle(processInstanceName,taskId, taskName, processInstanceId, taskDefinitionKey) {

    parent.parent.Dialog.openRemote("重新申请【" + processInstanceName + "】", springUrl
    		+ "/app/taskHandle_againApply?taskId=" + taskId + "&taskName=" + encodeURI(encodeURI(taskName))
    		+ "&processInstanceId=" + processInstanceId + "&taskDefinitionKey=" + taskDefinitionKey + "&mark=1", 1000,520
    );
}

/**
 * 查看申请详情 事件
 * @param processInstanceId
 */
function detailProcessInstance(processInstanceId,processDefinitionKey,businessObjectId) {
    var buttons = [
        {
            "label": "返回",
            "class": "active1",
            "callback": function () {
            }
        },
        {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(processDefinitionKey,businessObjectId,modal);
                return false;
            }
        }
    ];
    parent.parent.Dialog.openRemote("查看流程实例历史详情", springUrl + "/app/endProcessInstance_index?processInstanceId=" + processInstanceId,  1000,620, buttons);
}
/**
 * 弹出任务处理页面（草稿状态）
 * @param processDefinitionKey 流程定义Key
 * @param processDefinitionName 流程定义名称
 * @param businessObjectId 业务实例ID
 */
function openTaskHandleIndex(processDefinitionKey, processDefinitionName, businessObjectId, processInstanceStatus) {

    var url = springUrl + "/app/process_apply?processDefinitionKey=" + processDefinitionKey
        + "&businessObjectId=" + businessObjectId + "&processInstanceStatus=" + processInstanceStatus;
    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
            }
        },
        {
            "label": "复制表单",
            "class": "active1",
            "callback": function (modal) {
                copyForm(processDefinitionKey,businessObjectId,modal);
                return false;
            }
        },
        {
            "label": "保存草稿",
            "class": "active1",
            "callback": function (modal) {
                var frame = parent.parent.document.getElementById('bindReportFrame').contentWindow;
                var check = frame.saveDraft(modal);
//                if(check){
//                	parent.load('apply', springUrl + '/app/processApply_index', true);
//                }
                return false;
            }
        },
        {
            "label": "提交",
            "class": "active1",
            "callback": function (modal) {
                var frame = parent.parent.document.getElementById('bindReportFrame').contentWindow;
                /*frame.submitProcess();
                parent.load('apply', springUrl + '/app/processApply_index', true);
                return false;*/
                var check = frame.submitProcess(modal);
                return false;
            }
        }
    ];
    parent.parent.Dialog.openRemote("继续申请【" + processDefinitionName + "】", url,  1000,620, buttons);
}
/*
 * 显示当前代办人
 */
function findCandidateUsers(taskId,processInstanceId,currentStatus){
    var url = springUrl + "/app/processApply_getCandidateUsers";
               
                jQuery.ajax({
                    type: "get",
                    url: url,
                    data: {
                        taskId: taskId
                    },
                    success: function (data) {
                    	if(currentStatus == '1' || currentStatus == '6'){
                    		$("#div_all_"+processInstanceId+" .coressStep a") .append("<br><span style='color:#FF8A00'  >["+data.candidateUsers+"]</span>");
                    	} else if(currentStatus == '2') {
                    		$("#div_all_"+processInstanceId+" .finshStep a") .append("<br><span style='color:#FF8A00'  >["+data.candidateUsers+"]</span>");
                    	}
                    }
                });

}

function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}
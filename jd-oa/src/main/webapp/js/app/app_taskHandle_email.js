/**
 * 任务处理页面JS
 * User: zhaoming
 * Date: 13-10-15
 * Time: 下午5:43
 * To change this template use File | Settings | File Templates.
 */
var mark;
$(function(){

    var processInstanceId = $("#processInstanceId").val();
    var taskInstanceId = $("#taskInstanceId").val();
    var processDefinitionId = $("#processDefinitionId").val();
    var nodeId = $("#nodeId").val();
    var businessId  =    $("#businessId").val();
    var processDefinitionKey =  $("#processDefinitionKey ").val();
    var taskType=  $("#taskType").val();
    mark = $("#mark").val();
    
//    表单加载
//    $('#formContent').load('');
//    按钮组
    getButtons(processInstanceId, taskType,processDefinitionId,nodeId,taskInstanceId);
//    获取流程实例运行轨迹
    getProcessTrajectory(processInstanceId);

//    按钮组事件绑定 - 提交
    $('#submit').click(function() {
    	var flag = document.getElementById('taskInfoFrame').contentWindow.saveBusinessData();
    	if(typeof(flag) != 'false'){
    		approveInfo("submit");
    	}
    });
//    按钮组事件绑定 - 草稿
    $('#draft').click(function() {
        document.getElementById('taskInfoFrame').contentWindow.saveDraft();
        var frame = document.getElementById('mainFrame').contentWindow.load;
        /*if(typeof(frame) != 'undefined'){
        	document.getElementById('mainFrame').contentWindow.load('apply', springUrl+'/app/processApply_index',true);
        } else {
        	window.frames['mainFrame'].refershTable();
        }*/
    });
//    按钮组事件绑定 - 批准
    $('#approve').click(function() {
    	var frame = document.getElementById('taskInfoFrame').contentWindow;
    	var flag = frame.saveBusinessData();
    	if(flag ){
    		Dialog.confirm("系统消息", "确定批准？", "确定", "取消", function (result) {
            	if(result){
            		approveInfo("approve");
            	}
            });    	
    	}
    });
//    按钮组事件绑定 - 拒绝
    $('#reject').click(function() {
        Dialog.confirm("系统消息", "确定拒绝？", "确定", "取消", function (result) {
        	if(result){
                rebackInfo();
        	}
        }); 
    });
//    按钮组事件绑定 - 驳回
    $('#reback').click(function() {
        Dialog.confirm("系统消息", "确定驳回？", "确定", "取消", function (result) {
        	if(result){
                rejectInfo();
        	}
        }); 
    });
//    按钮组事件绑定 - 加签
    $('#forward').click(function() {
    	forward();
    });
//    按钮组事件绑定 - 确认
    $('#confirm_yes').click(function() {
        approveInfo("confirm_yes");
    });
    $('#confirm_no').click(function() {
        approveInfo("confirm_no");
    });
    
    document.getElementById('taskInfoFrame').src = springUrl+"/app/processApp_open_bindReport?processDefinitionKey="+processDefinitionKey+"&businessObjectId="+businessId+"&processInstanceId=" + processInstanceId + "&processNodeId="+nodeId+ "&processInstanceStatus=";
    document.getElementById('taskInfoFrame').onload = iFrameHeight;
    
    if($("#isHide").val()=="true") {
        $("#approveDiv").hide();
    }else{
        //加签任务 默认校验是否执行审批
        if(taskType=="addsigner"){
            setTimeout('isRead()',500);
        }
    }
//    $("#taskInfoFrame").attr("scrolling","yes");
//    $("#taskInfoFrame").wrap("<div id='SF_wrap'></div>");
//    $("#taskInfoFrame").atrr("width",$("#taskInfoFrame").width());
//    $("#SF_wrap").css("width",$("#taskInfoFrame").widht()-60).css("overflow","hidden");

});
/**
 * 按钮状态切换
 * @param flag
 */
function buttonStatus(flag){
	if(flag){
	       $("#approve").attr("disabled", true);
	       $("#reject").attr("disabled", true);
	       $("#reback").attr("disabled", true);
	       $("#forward").attr("disabled", true);
	       $("#confirm_yes").attr("disabled", true);
	       $("#confirm_no").attr("disabled", true);
	   }else{
	       $("#approve").removeAttr("disabled");
	       $("#reject").removeAttr("disabled");
	       $("#reback").removeAttr("disabled");
	       $("#forward").removeAttr("disabled");
	       $("#confirm_yes").removeAttr("disabled");
	       $("#confirm_no").removeAttr("disabled");
	   }
}
//加签操作
function forward(){
	var processDefinitionId = $("#processDefinitionId").val();
	var processInstanceId = $("#processInstanceId").val();
	var taskInstanceId = $("#taskInstanceId").val();
	var taskName = $("#taskName").val();
	var nodeId = $("#nodeId").val();
	var startTime = $("#startTime").val();
	var processDefinitionKey = $("#processDefinitionKey").val();
	var processInstanceName = $("#processInstanceName").val();
    var businessInstanceId = $("#businessId").val();

    var buttons = [
        {
            "label": "取消",
            "class": "inactive",
            "callback": function () {
            }
        },
        {
            "label": "确定",
            "class": "active1",
            "callback": function () {
               var selectRecords = Table.getAllSelectedRowIds("orgUserDataTable");
               if(selectRecords.length<=0)   {
                   Dialog.alert("提示信息","至少选择一个加签人！");
                   return false;
               }else{            	  
            	    var d={
            	    		"forwardUserIds": selectRecords,
            				"processDefinitionId": processDefinitionId,
            				"processDefinitionKey": processDefinitionKey,
            				"processInstanceId": processInstanceId,
            	    		"taskId": taskInstanceId,
                			"nodeId": nodeId,
                			"taskName": taskName,
                			"taskType": "addsigner",
                            "comments": "加签任务",
                            "startTime": startTime,
                            "processInstanceName": processInstanceName,
                        "businessInstanceId":businessInstanceId
            	    };
                   var res=false;
                   jQuery.ajax({
                       async:false,
                       type:"POST",
                       url : springUrl + "/app/processTask_forwardCount",
                       data: d,
                       success: function (result) {
                          if(result.Success){
                              jQuery.ajax({
                                  async:false,
                                  type:"POST",
                                  url : springUrl + "/app/processTask_forward",
                                  data: d,
                                  success: function (result) {
                                      Dialog.confirm("系统消息", result, "确定", '', function (result) {
                                          if(result){
                                              closeWindow();
                                          }
                                      });
                                      res=true;
                                  } ,
                                   error: function (data) {
                                       Dialog.alert("提示信息","加签失败");
                                   }
                              });
                          }else{
                              Dialog.alert("提示信息","选择的用户已存在相同的加签任务，且尚未审批，不能多次加签。");
                          }
                       } ,
                       error: function (data) {
                           Dialog.alert("提示信息","加签失败");
                       }
                   });
                   return res;
               }
	                         


            }
        }
    ];
//	 currentProcessInstanceId = processInstanceId;
    Dialog.openRemote('选择用户', springUrl + '/system/sysAddress_getOrgUser?processInstanceId='+processInstanceId+'&isMultiSelect=1',900, 500, buttons);
}

/**
 * 判断是否为已阅权限
 */
function isRead(){
    jQuery.ajax({
        type: "POST",
        url: springUrl + "/app/processTask_isRead",
        dataType: "json",
        async:false,
        data: {
            processInstanceId:  $("#processDefinitionId").val(),
            nodeId:  $("#nodeId").val()
        },
        success: function(data) {
            //加签规则为阅读的 在每次加载页面上自动执行审批
             if(data.isRead=="2"){
                 jQuery.ajax({
                     type:"POST",
                     cache:false,
                     async : false,
                     dataType : 'json',
                     data:{
                         id :$("#taskInstanceId").val(),
                         processDefinitionId: $("#processDefinitionId").val(),
                         nodeId:$("#nodeId").val(),
                         buinessInstanceId: $("#businessId").val(),
                         taskType:$("#taskType").val(),
                         processDefinitionKey:$("#processDefinitionKey").val(),
                         startTime:  $("#startTime").val(),
                         name:  $("#taskName").val(),
                         assignerId:   $("#assignerId").val(),
                         isProxy:$("#isProxy").val(),
                         addSignerUserId:  $("#addSignerUserId").val(),
                         processInstanceId:  $("#processInstanceId").val(),
                         processInstanceName: $("#processInstanceName").val(),
                         comments:"已阅"
                     },
                     url:springUrl + "/app/processTask_complete",
                     success:function (data) {
                    	
                     },
                     error: function (data) {
                    	 Dialog.alert("失败","加签任务审批失败");
                     }
                 });
             }
        },
        error: function (data) {
            Dialog.alert("失败","获取加签任务配置失败");
        }
    });
}
/**
 * 获取按钮组
 * @param processDefinitionId 流程定义ID
 * @param nodeId 节点ID
 */
function getButtons(processInstanceId, taskType, processDefinitionId, nodeId, taskId){
    jQuery.ajax( {
        type: "POST",
        url: springUrl + "/app/getButtons",
        dataType: "json",
        async:false,
        data: {
            processInstanceId: processInstanceId,
            taskType: taskType,
            processDefinitionId: processDefinitionId,
            nodeId: nodeId,
            taskId: taskId
        },
        success: function(data) {
            var buttons = '<div style="text-align: left;" class="controls">';
            
            if(data.isFinishedFlag == true){   
            	buttons = buttons + "<span style='font-weight:bold;font-size:16px;color:#FF0000;'>此申请您已经进行过审批。</span>　";
            	$("#comments").hide();
            }else{
	            if(data.submitButtonFlag == true){
	                buttons = buttons + '<button id="submit"  class="active1">　提  交　</button>　';
	            	//隐藏审批意见
	            	$("#comments").hide();
	            }
	            if(data.draftButtonFlag == true){
	                buttons = buttons + '<button id="draft"  class="active1">保存草稿</button>　';
	            	//隐藏审批意见
	            	$("#comments").hide();
	            }
	            if(data.approveButtonFlag == true){
	                buttons = buttons + '<button id="approve"  class="active1">　批  准　</button>　';
	            }
	            if(data.rejectButtonFlag == true){
	                buttons = buttons + '<button id="reject" title="退回到上一个审批人（或申请人）"  class="active1">　拒  绝　</button>　';
	            }
	            if(data.rebackButtonFlag == true){
	                buttons = buttons + '<button id="reback" title="退回到申请人"  class="active1">　驳  回　</button>　';
	            }
	            if(data.forwardButtonFlag == true){
	                buttons = buttons + '<button id="forward"  class="inactive">　加  签　</button>　';
	            }
	            if(data.confirmButtonFlag == true){
	                buttons = buttons + '<button id="confirm_yes"  class="active1">　同意　</button>　';
	                buttons = buttons + '<button id="confirm_no"  class="active1">　不同意　</button>　';
	            }
            	
            }
            buttons = buttons + '</div>';
            $("#buttons").append(buttons);
        },
        error: function (data) {
            Dialog.alert("失败","获取按钮组失败");
        }
    });
}

/**
 * 获取流程实例运行轨迹
 * @param processInstanceId 流程实例ID
 */
function getProcessTrajectory(processInstanceId){
    jQuery.ajax( {
        type: "POST",
        url: springUrl + "/app/processTrajectory",
        dataType: "json",
        data: {
            processInstanceId: processInstanceId
        },
        success: function(data) {
            var html = "";
            for(var i = 0; i<data.length; i++){
                html = html + "<tr>";
                html = html + "<td width='100px'>" + isEmpty(data[i].taskName) + "</td>";
                html = html + "<td width='70px'>" + isEmpty(data[i].userId) + "</td>";
                html = html + "<td width='60px'>" + isEmpty(data[i].result).replace("0","提交申请").replace("1","批准").replace("2","拒绝").replace("3","驳回").replace("4","重新申请 ").replace("5","加签审批").replace("6","加签 ") + "</td>";
                html = html + "<td style = 'text-align: left' width = '300px'>" + isEmpty(data[i].comment) + "</td>";
                html = html + "<td width='140px'>" + dateHandle(data[i].beginTime) + "</td>";
                html = html + "<td width='140px'>" + dateHandle(data[i].endTime) + "</td>";
                html = html + "</tr>";
            }
            $("#processTrajectory").append(html);
        },
        error: function (data) {
            Dialog.alert("失败","获取任务处理历史失败");
        }
    });
}

function isEmpty(obj){
    if(obj == null || obj=="" || obj=="null"){
        return "";
    }else{
        return obj;
    }
}

function dateHandle(obj){
    if(obj == null || obj=="" || obj=="null"){
        return "";
    }else{
        return new Date(obj).format("yyyy-MM-dd hh:mm:ss");
    }
}

Date.prototype.format = function(format)
{
    var o = {
        "M+" : this.getMonth()+1, //month
        "d+" : this.getDate(),    //day
        "h+" : this.getHours(),   //hour
        "m+" : this.getMinutes(), //minute
        "s+" : this.getSeconds(), //second
        "q+" : Math.floor((this.getMonth()+3)/3), //quarter
        "S" : this.getMilliseconds() //millisecond
    }
    if(/(y+)/.test(format)){
        format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o){
        if(new RegExp("("+ k +")").test(format)){
            format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
        }
    }
    return format;
}

function iFrameHeight() {
	var ifm= document.getElementById("taskInfoFrame");   
	var subWeb = document.frames ? document.frames["taskInfoFrame"].document : ifm.contentDocument;   
	if(ifm != null && subWeb != null) {
	   ifm.height = subWeb.body.scrollHeight;
    }
	$("#showbox").removeClass("hide");
    try{ //解决IE8显示不全的问题
        $("#showbox").parent().parent().css("overflow-y","auto");
    }catch (e){
    }
}
/**
 * 审批
 * @param processInstanceId
 * @param taskId
 * @param nodeId
 */
function approveInfo(type){
    buttonStatus(true);
    var comment= $("#folderDesc").val();
    if("confirm_yes"==type)
    	comment = "同意（"+comment+"）";
    if("confirm_no"==type)
    	comment = "不同意（"+comment+"）";
    if(comment.length>200){
        Dialog.alert("提示信息","输入意见太多,只能支持200个字符!");
        $("#folderDesc").focus();
    }else{
        jQuery.ajax({
            type:"POST",
            cache:false,
            async : true,
            dataType : 'json',
            data:{
                id :$("#taskInstanceId").val(),
                processDefinitionId: $("#processDefinitionId").val(),
                nodeId:$("#nodeId").val(),
                buinessInstanceId: $("#businessId").val(),
                taskType:$("#taskType").val(),
                processDefinitionKey:$("#processDefinitionKey").val(),
                startTime:  $("#startTime").val(),
                name: $("#taskName").val(),
                assignerId:   $("#assignerId").val(),
                isProxy:$("#isProxy").val(),
                addSignerUserId:  $("#addSignerUserId").val(),
                processInstanceId: $("#processInstanceId").val(),
                processInstanceName: $("#processInstanceName").val(),
                comments:comment
            },
            url:springUrl + "/app/processTask_complete",
            success:function (data) {
                buttonStatus(false);
                if(data.Success)  {
                    if("submit"!=type){
                       
                    }else{
                    	var mainFrame=document.getElementById('mainFrame').contentWindow;
                    	if(mainFrame != null ){
                    		var frame = document.getElementById('mainFrame').contentWindow.load;
                    		if(typeof(frame) != 'undefined'){
                    			document.getElementById('mainFrame').contentWindow.load('apply', springUrl+'/app/processApply_index',true);
                    		} else {
                    			window.frames['processSearchFrame'].refershTable();
                    		}
                    	}
                    }
                    if(typeof(mark)!='undefined' && mark == '1'){
                    	Dialog.hide();
                    }
                    Dialog.alert("提示信息",data.message);
                }else{
                    Dialog.alert("提示信息",data.message);
                }

            },
            error: function (data) {
                buttonStatus(false);
                 Dialog.alert("提示信息","操作失败");
            }
        });
    }
}

/**
 * 驳回
 * @param taskId
 * @param processInstanceId
 */
function rejectInfo(){
    var comment  = $("#folderDesc").val();
    if(comment==""){
        Dialog.alert("提示信息","请填写审批意见!");
        $("#folderDesc").focus();
    }else{
        if(comment.length>200){
            Dialog.alert("提示信息","输入意见太多,只能支持200个字符!");
            $("#folderDesc").focus();
        }else{
            buttonStatus(true);
            jQuery.ajax({
            type:"POST",
            async : true,
            cache:false,
            dataType : 'json',
            data:{
                id :$("#taskInstanceId").val(),
                processDefinitionId: $("#processDefinitionId").val(),
                nodeId:$("#nodeId").val(),
                buinessInstanceId: $("#businessId").val(),
                taskType:$("#taskType").val(),
                processDefinitionKey:$("#processDefinitionKey").val(),
                startTime:  $("#startTime").val(),
                name:  $("#taskName").val(),
                assignerId:   $("#assignerId").val(),
                isProxy:$("#isProxy").val(),
                addSignerUserId:  $("#addSignerUserId").val(),
                processInstanceId: $("#processInstanceId").val(),
                processInstanceName: $("#processInstanceName").val(),
                comments:comment
            },
            url:springUrl + "/app/processTask_RejectToFirst",
            success:function (data) {
                buttonStatus(false);
                if(data.Success)  {
                   

                    
                    Dialog.alert("提示信息",data.message);
                }else{
                    Dialog.alert("提示信息",data.message);
                }
            },
            error: function (data) {
                buttonStatus(false);
                Dialog.alert("提示信息","驳回失败");
            }
          });
        }
    }
}
/**
 *拒绝
 * @param taskId
 * @param processInstanceId
 */
function rebackInfo(){
    var comment  = $("#folderDesc").val();
    if(comment==""){
        Dialog.alert("提示信息","请填写审批意见!");
        $("#folderDesc").focus();
    }else{
        if(comment.length>200){
            Dialog.alert("提示信息","输入意见太多,只能支持200个字符!");
            $("#folderDesc").focus();
        }else{
            buttonStatus(true);
            jQuery.ajax({
                type:"POST",
                cache:false,
                async : true,
                dataType : 'json',
                data:{
                    id :$("#taskInstanceId").val(),
                    processDefinitionId: $("#processDefinitionId").val(),
                    nodeId:$("#nodeId").val(),
                    buinessInstanceId: $("#businessId").val(),
                    taskType:$("#taskType").val(),
                    processDefinitionKey:$("#processDefinitionKey").val(),
                    startTime:  $("#startTime").val(),
                    name:  $("#taskName").val(),
                    assignerId:   $("#assignerId").val(),
                    isProxy:$("#isProxy").val(),
                    addSignerUserId:  $("#addSignerUserId").val(),
                    processInstanceId: $("#processInstanceId").val(),
                    processInstanceName: $("#processInstanceName").val(),
                    comments:comment
                },
                url:springUrl + "/app/processTask_RejectToPrevious",
                success:function (data) {
                    buttonStatus(false);
                    if(data.Success)  {
                       
                        
                        Dialog.alert("提示信息",data.message);
                    }else{
                        Dialog.alert("提示信息",data.message);
                    }

                },
                error: function (data) {
                    buttonStatus(false);
                    Dialog.alert("提示信息","拒绝失败");
                }
            });
        }
    }
}

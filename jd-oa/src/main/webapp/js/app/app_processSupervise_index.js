$(function(){
	reloadTaskNum();
	displaySuperviseTask(7);
	displaySuperviseTask(3);
	displaySuperviseTask(1);
	displaySuperviseTask(0);
});
function reloadTaskNum(){
	parent.getMyTaskNum("supervise","",false);
	var parentAllNum=parent.$(".superviseNum:eq(0)").text();
	if(parentAllNum!=null&&parentAllNum!=0){
		$(".allNum").text("("+parentAllNum+")");
		$(".allNum").parent().show();
	}else{
		$(".allNum").parent().parent().parent().remove();
	}
	$("#supervise").click(function(){
		addSupervise();
	});
}
function addSupervise(processInstanceId,nodeId,taskId,dayKey,processDefinitionId){
	//!!!!还要修改
//	var processInstanceId = 'mydo-f1cdfd1e-353e-11e3-9c74-001f29cc9390';
//	var nodeId = 'UserTask1381743644918';
//	var taskId = 'kkkkkkkk';
//	var superviseUserId='kkkkkkkk';
	var params = [];
	params.push({ "name": "processInstanceId", "value":processInstanceId}) ;
	params.push({ "name": "nodeId", "value":nodeId}) ;
	params.push({ "name": "taskId", "value":taskId}) ;
//	params.push({ "name": "superviseUserId", "value":superviseUserId}) ;
	
	jQuery.ajax( {
		async: true ,
        url:springUrl + "/app/processSupervise_add", 
        //dataType: "json",
        data: params, 
        success: function(resp) {
        	if(resp!=null && parseInt(resp) >0){
        		parent.parent.Dialog.alert("操作结果","操作成功！");
        		reload(dayKey,processDefinitionId);
        	}
        	else if(resp!=null && parseInt(resp) == 0)
        		parent.parent.Dialog.alert("操作结果","操作失败：没有获取到督办人！");
        	else
        		parent.parent.Dialog.alert("操作结果","操作失败！");
         }
	});
	

}

//根据nodeid获取办理人
function getUsersByNodeId(){
	//流程定义的所有权限表达式
   var expressionIds ;
	var data={
			"processInstanceId": "mydo-f1cdfd1e-353e-11e3-9c74-001f29cc9390",
			"nodeId": "UserTask1381743644918"
	};

	
	jQuery.ajax({
		async:false,
		url : springUrl+"/app/getUsersByNodeId",
		data : data,
		success : function(result) {
			expressionIds = result;
		}
	});
	
	users.push(getUsersByAuthId(expressionIds));

	
	
}
function more(dayKey,processDefinitionId){
	var portlet=$("#pane"+dayKey).find("#pdfId"+processDefinitionId);
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var timeline="";
	if(dayKey=="7"){
		timeline="overOneWeek";
	}else if(dayKey=="3"){
		timeline="overThreeDay";
	}else if(dayKey=="1"){
		timeline="overOneDay";
	}else if(dayKey=="0"){
		timeline="normal";
	}
	var url=springUrl+"/app/processSearch_index?"
		+"taskType=supervise&"
		+"processDefinitionId="+processDefinitionId+"&"
		+"processDefinitionKey="+processDefinitionKey+"&"
		+"addInfo="+timeline;
	parent.parent.$("#mainFrame").attr("src",url);
}
//add by wdx
function reload(dayKey,processDefinitionId){
	var portlet=$("#pane"+dayKey).find("#pdfId"+processDefinitionId);
	//当前刷新流程的值
	var dataNum=$.trim(portlet.find(".dataNum").text());
	//全部总数
	var allNum=$.trim($(".allNum").text()).replace("(","").replace(")","");
	//当前tab也上标记的数目
	var dayKeyNum=$.trim($(".key"+dayKey).find(".num").text()).replace("(","").replace(")","");
	var processDefinitionKey=portlet.find("tr:eq(0)").attr("processDefinitionKey");
	var startDay=0;
	var endDay=0;
	if(dayKey=="7"){
		startDay=-1;
		endDay=7;
	}else if(dayKey=="3"){
		startDay=7;
		endDay=3;
	}else if(dayKey=="1"){
		startDay=3;
		endDay=1;
	}else if(dayKey=="0"){
		startDay=1;
		endDay=0;
	}
	var data={
		"startDay":startDay,
		"endDay":endDay,
		"pafKey":processDefinitionKey
	};
	jQuery.ajax( {
        type: "POST",
        async:true,
        url:springUrl+'/app/processSupervise_reload',
        dataType: "json",
        data:data,
        success: function(result) {
        	var hasData=false;
			for(var key in result){
				hasData=true;
				displayProcessTask(result[key],portlet.parent(),"reload",dayKey);
	        	var total=parseInt(allNum)-parseInt(dataNum)+parseInt(result[key].totalNum);
	        	var dayKeyTaskNum=parseInt(dayKeyNum)-parseInt(dataNum)+parseInt(result[key].totalNum);
	        	$(".allNum").text("("+total+")");
	        	$(".key"+dayKey).find(".num").text("("+dayKeyTaskNum+")");
	        	parent.$(".superviseNum").text(total);
			}
			if(hasData==false){
				portlet.parent().remove();
	        	var total=parseInt(allNum)-parseInt(dataNum);
	        	var dayKeyTaskNum=parseInt(dayKeyNum)-parseInt(dataNum);
	        	$(".allNum").text("("+total+")");
	        	$(".key"+dayKey).find(".num").text("("+dayKeyTaskNum+")");
	        	parent.$(".superviseNum").text(total);
	        	parent.autoTabHeight($("#superviseProxy").height()+40,'supervise');
			}
        }
	});
}
function displayProcessTask(processTask,obj,addOrRealod,dayKey){
	var newDisplay;
	if(addOrRealod=="reload"){
		newDisplay=obj.find("#pdfId"+processTask.processDefinitionId);
		//无记录删掉流程显示
		if(processTask.totalNum==0){
			newDisplay.remove();
			return;
		}
		newDisplay.find("tbody").html("");
	}else if(addOrRealod=="add"){
		//无记录不显示
		if(processTask.totalNum==0) return;
		newDisplay=obj.find(".taskPane:eq(0)").clone();
		obj.append(newDisplay);
	}
	newDisplay.attr("id","pdfId"+processTask.processDefinitionId);
	var dataListSize=0;
	if(processTask.dataList!=null){
		dataListSize=processTask.dataList.length;
	}
	var titleHtml='<div class="fl"><span class="ftx-05"> '+processTask.processDefinitionName+'- </span>有<span class="ftx-04 dataNum"> '+dataListSize+'  </span>笔等待您督办</div>';
	if(processTask.totalNum>5){
		titleHtml+='<div class="fr"><a href="#" onclick="more(\''+dayKey+'\',\''+processTask.processDefinitionId+'\');">更多<s class="icon-more"></s></a></div>';
	}
	newDisplay.find(".grid-ex:eq(0)").html(titleHtml);
	var th="<tr processDefinitionName='"+processTask.processDefinitionName+"'" +
	" processDefinitionId='"+processTask.processDefinitionId+"'"+
	" processDefinitionKey='"+processTask.processDefinitionKey+"'"+
	" tableName='"+processTask.tableName+"'"+
	" bussinessColumns='"+JSON.stringify(processTask.bussinessColumns)+"'>";
	th+="<td class='status'>&nbsp;</td>";
	$.each(processTask.processColumns,function(i,c){
		if(i==0){
			th+="<th align='left'>"+c+"</th>";
		}else{
			th+="<th>"+c+"</th>";
		}
	});
	th+="<th>操作</th>";
	th+="</tr>";
	newDisplay.find("thead").html(th);	
	if(dataListSize!=0){
		$.each(processTask.dataList,function(i,d){
			var tr="<tr class='todo' taskType='"+d.taskType+"'" +
			" processInstanceId='"+d.processInstanceId+"'"+
			" taskId='"+d.taskId+"'"+
			" taskName='"+d.taskName+"'"+
			" nodeId='"+d.nodeId+"'"+
            " processInstanceName='" +d.processInstanceName+"'"+
                "followCode='" +d.followCode+"'"+
			">";
			tr+="<td class='status'>&nbsp;</td>";
			tr+="<td width='25%' align='left' style='padding:0 0 0 10px'><a href='#' onclick=\"detailProcessInstance('"+d.processInstanceId+"')\">"+concatProcessInsanceName(d.followCode,d.processInstanceName)+"</a></td>";
			tr+="<td width='70'>"+d.applyName+"</td>";
			tr+="<td width='140'>"+d.startTime+"</td>";
			$.each(processTask.bussinessColumns,function(i,c){
				if(d.data!=null){
					if(d.data[c]==null){
						tr+="<td>&nbsp;</td>";
					}else{
						tr+="<td>"+d.data[c]+"</td>";
					}
				}else{
					tr+="<td>&nbsp;</td>";
				}
			});
			tr+="<td width='200'><a href='#' onclick=\"javascript:addSupervise('"+d.processInstanceId+"','"+d.nodeId+"','"+d.taskId+"','"+dayKey+"','"+processTask.processDefinitionId+"');\" ><i class='icon-edit'></i>督办</a>&nbsp;" +
            "<a href='#' onclick=\"detail($(this))\"><i class='icon-edit'></i>流程状态</a>" +
			"</td>";
			tr+="</tr>";
			newDisplay.find("tbody").append(tr);
		});
		newDisplay.find("tbody").find("tr:gt(4)").hide();
	}else{
		newDisplay.find("tbody").append("暂无数据");
	}
	newDisplay.removeClass("hide");
	parent.autoTabHeight($("#superviseProxy").height()+40,'supervise');
}
//节点图回调函数
function detail(obj) {
    var processInstanceId = obj.parent().parent().attr("processInstanceId");
    var processInstanceName =obj.parent().parent().attr("processInstanceName");
    parent.parent.Dialog.openRemote(processInstanceName + '|流程图跟踪', '/app/processApply_detail?instanceId=' + processInstanceId, 1000, 520);
}

var allNoData=true;
function loadProcessTask(startDay,endDay,divEl,tabKey){
	var data={
		"startDay":startDay,
		"endDay":endDay
	};
	jQuery.ajax( {
		type: "POST",
		async:true,
		url:springUrl+'/app/processSupervise_list',
		dataType: "json",
		data:data,
		success: function(result) {
			var hasData=false;
			var totalNum=0;
			for(var key in result){
				hasData=true;
				displayProcessTask(result[key],divEl,"add",tabKey);
				totalNum+=result[key].dataList.length;
			}
			if(hasData==true){
				allNoData=false;
				$(".key"+tabKey).removeClass("hide");
				divEl.removeClass("hide");
				$("#noDataDiv").hide();
			}else{
				$(".key"+tabKey).remove();
				divEl.remove();
				if(allNoData==true){
					$("#noDataDiv").show();
				}
			}
			$(".key"+tabKey).find(".num").text("("+totalNum+")");
			show($(".curr1"),"all");
		}
	});
}
function displaySuperviseTask(key){
	var timeStr="";
	var tabName="";
	var startDay;
	var endDay;
	if(key=="7"){
		timeStr="<span class='mlinetxt'>待审批超过<span class='ftx-04'> 一 </span>星期</span>";
		tabName="待审批超过1星期";
		startDay=-1;
		endDay=7;
	}else if(key=="3"){
		timeStr="<span class='mlinetxt'>待审批超过<span class='ftx-04'> 3 </span>天</span>";
		tabName="待审批超过3天";
		startDay=7;
		endDay=3;
	}else if(key=="1"){
		timeStr="<span class='mlinetxt'>待审批超过<span class='ftx-04'> 1 </span>天</span>";
		tabName="待审批超过1天";
		startDay=3;
		endDay=1;
	}else if(key=="0"){
		timeStr="<span class='mlinetxt'>正常审批无延误</span>";
		tabName="正常审批无延误";
		startDay=1;
		endDay=0;
	}
	var newPortletDisplay=$("#superviseProxy").find(".proxyDiv:eq(0)").clone();
	newPortletDisplay.find(".mline:eq(0)").html(timeStr);
	$("div#superviseProxy").append(newPortletDisplay);
	$("#tab1").find(".clearfix:eq(0)").append('<li class="key'+key+' hide" data-widget="tab-item1" onclick="show($(this),\''+key+'\');">'+tabName+'<span class="num"></span>'+'</li>');
	newPortletDisplay.attr("id","pane"+key);
	loadProcessTask(startDay,endDay,newPortletDisplay,key);
}
function show(obj,proxyName){
	$(".curr1").removeClass("curr1");
	obj.addClass("curr1");
	if(proxyName=="all"){
		$("#superviseProxy").find("div[id^='pane']").each(function(i){
			$(this).show();
		});
	}else{
		$("#superviseProxy").find("div[id^='pane']").not("#pane"+proxyName).each(function(){
			$(this).hide();
		});
		$("#pane"+proxyName).show();
	}
	parent.autoTabHeight($("#superviseProxy").height()+40,'supervise');
}
/**
* 查看申请详情 事件
* @param processInstanceId
*/
function detailProcessInstance(processInstanceId) {
	var buttons = [
		{
			"label": "返回",
			"class": "active1",
			"callback": function () {
			}
		}
	];
	parent.parent.Dialog.openRemote("查看流程实例历史详情", springUrl + "/app/endProcessInstance_index?processInstanceId=" + processInstanceId, 1000, 620, buttons);
}
function downloadfile(id){
	 var url = springUrl + "/app/process_downloadfile?id=" + id;
	 window.location.href = encodeURI(url);
}
function concatProcessInsanceName(followCode,processInstanceName){
    if(followCode != null && followCode != ''){
        return '['+followCode+']' + processInstanceName;
    }
    return processInstanceName;
}
$(function(){
	getMyTaskNum("owner","");
	getMyTaskNum("addsigner","");
	getMyTaskNum("proxy","");
	getMyTaskNum("supervise","");
});
function setNum(id,num){
	if(num!=0){
		if(id!='addsigner'){
			$(".tip").removeClass("hide");
		}
		$("."+id+"Num").each(function(i){
			$(this).text(num);
			$(this).parent().removeClass("hide");
		});
	}else{
		$("."+id+"Num").each(function(i){
			$(this).text(num);
		});
		$(".p"+id).addClass("hide");
		if($(".myApplyNum:eq(0)").text()=="0"
			&& $(".ownerNum:eq(0)").text()=="0"
			&& $(".proxyNum:eq(0)").text()=="0"
			&& $(".superviseNum:eq(0)").text()=="0"){
			$(".tip").addClass("hide");
		}
	}
	autoTabHeight();
}
function setLeftTotalNum(num){
	var all=parent.$(".all").text();
	var all_num=parseInt(all)+num;
	if(all_num!=0){
		parent.$(".all").show();
		parent.$(".all").text(all_num);
	}else{
		parent.$(".all").hide();
	}
}
function getMyTaskNum(taskType,pdfId,isAsync){
		if(isAsync==null) isAsync=true;
		var data={
			"processDefinitionId":pdfId,
			"taskType":taskType
		};
		jQuery.ajax( {
	        type: "POST",
	        async:isAsync,
	        url:springUrl+'/app/processTask_taskNum',
	        dataType: "json",
	        data:data,
	        success: function(result) {
	        	setNum(taskType,result);
	        }
		});
}
/**
 * 添加代理人JS
 * User: yaohaibin
 * Date: 13-9-17
 * Time: 下午14:18
 */
/**
 *
 * 页面初始化
 */
var userTable;
var options = Table.options;
options = {
    defaultSort:[['1','desc'],['2','desc']],
    pageUrl:springUrl+"/process/processProxy_findList",
    isPaging:false,
    useCheckbox:false,
    sendData:function ( sSource, aoData, fnCallback ) {
        jQuery.ajax( {
            type: "POST",
            url:sSource,
            dataType: "json",
            data: aoData,
            success: function(data) {
                if(data.aaData.length>0){
                    $("#tableDiv").show();
                    $("#cancelBt").show();
                }
                fnCallback(data);
            },
            error: function (data) {
                Dialog.alert("失败","文件列表查询失败");
            }
        });
    },
    columns: [
        {"mData": "proxyId","sTitle":"代理人" ,"bSortable":false},
        {"mData": "proxyBeginTime","sTitle":"起始时间" ,"mRender":function(data){
            return new Date(data).format("yyyy-MM-dd");
        }},
        {"mData": "proxyEndTime","sTitle":"终止时间" ,"mRender":function(data){
            return new Date(data).format("yyyy-MM-dd");
        }},
        {"mData": "id","sTitle":"操作","bSortable":false,"sWidth":200,"mRender":function(data){
            return  "<a href='#' class='btn' title='删除' onclick='deleteRow(\""+data+"\");'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>";
        }}
    ],
    btns:[
        {
            "sExtends":    "text",
            "sButtonText": "添加",
            "sButtonClass": "btn-success active1",
            "sToolTip": "",
            "fnClick": function ( nButton, oConfig, oFlash ) {
                addProxy();
            }
        }
    ]
};
$(document).ready(function() {
    userTable = Table.dataTable("user_table",options);
    //时间控件
    $('#starDatetimepicker').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('#starDatetimepicker') .datetimepicker('setStartDate', new Date($("#today").val()).format("yyyy-MM-dd"));
    //时间空间
    $('#endDatetimepicker').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('#endDatetimepicker') .datetimepicker('setStartDate', new Date($("#today").val()).format("yyyy-MM-dd"));
    $('#starDatetimepicker').datetimepicker().on('changeDate', function(ev){
        var startTime=$("#proxyBeginTime").val();
        if(startTime!=""){
            if( $('#proxyEndTime').val()!="") {
                if(!compareDateTime(startTime,$('#proxyEndTime').val())){
                    $('#proxyEndTime').val("") ;
                    $('#proxyEndTimeTest').val("") ;
                }
            }

            $('#endDatetimepicker') .datetimepicker('setStartDate',null);
            $('#endDatetimepicker') .datetimepicker('setStartDate',startTime);
        }
        $('input[name="proxyBeginTimeTest"]').trigger("validate");
    });
    $('#endDatetimepicker').datetimepicker().on('changeDate', function(ev){
        $('input[name="proxyEndTimeTest"]').trigger("validate");
    });
    $("#selectUser").click(function(){
//        Dialog.openRemote('选择用户',springUrl+'/auth/expression_findUserIndex',800,600);
        remoteOrgUserInfo();
    });
    $("#saveUser").click(function(){
        if(!validate2($('#formGroup'))){
            return false;
        }
       saveUserInfo();
    });
    $("#cancelBt").click(function(){
        $("#formGroup").hide() ;
    });
    bindValidate($('#formGroup'));
    if($("#count").val()=="0"){
        $("#formGroup").show() ;
        $("#tableDiv").hide();
    }else{
        $("#formGroup").hide() ;
        $("#tableDiv").show();
    }
});
/**
 w
 * @param userId
 * @param userName
 */
function setUserName(userId,userName){
    $("#user_id").val(userId);
    $("#user_name").val(userName);
}
/**
 * 保存办理人
 */
function saveUserInfo(){
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            proxyId : $("#user_id").val(),
            beginTime:$("#proxyBeginTime").val(),
            endTime: $("#proxyEndTime").val()
        },
        url:springUrl + "/process/processProxy_getCount",
        success:function (data) {
            if(data==0){
                jQuery.ajax({
                    type:"POST",
                    cache:false,
                    dataType : 'json',
                    data:{
                        proxyId : $("#user_id").val(),
                        beginTime:$("#proxyBeginTime").val(),
                        endTime: $("#proxyEndTime").val()
                    },
                    url:springUrl + "/process/processProxy_addSave",
                    success:function (data) {
                        if(data.success){
                            Dialog.alert("提示信息",data.message)
                            Table.render(userTable);
                        }else{
                            Dialog.alert("提示信息",data.message)
                        }
                    },
                    error: function (data) {
                        Dialog.alert("失败","保存代理人失败");
                    }
                })
            }else{
                Dialog.alert("失败","添加失败!此时间段有相同的代理人");
            }
        },
        error: function (data) {
            Dialog.alert("失败","保存代理人失败");
        }
    }) ;
}
/**
 *拼装table
 * @param data
 */
function createTable(data){
    var table=$("#valueTable tbody");
    for(var i=0;i<data.length;i++){
        var item=data[i];
        var tr=$("<tr style='display:none'></tr>");
        var td0=$("<td></td>");
        var td1=$("<td></td>");
        var td2=$("<td></td>");
        var td3=$("<td></td>");
        var td4=$("<td></td>");
        td0.append(item.id);
        td1.append(item.proxyId);
        td2.append(item.proxyBeginTime);
        td3.append(item.proxyEndTime);
        var btn=  $("<a href='#' class='btn' title='删除' onclick='deleteRow(this,'"+item.id+");'><img src='"+springUrl+"/static/common/img/del.png' alt='删除'/></a>");
        td4.append(btn);
        tr.append(td0) ;
        tr.append(td1) ;
        tr.append(td2) ;
        tr.append(td3) ;
        tr.append(td4) ;
        table.append(tr);
    }
}
/**
 * 删除代理人信息
 * @param id
 */
function  deleteRow(id){
    Dialog.confirm("删除提示","确定删除选中的数据?","确定","取消",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                dataType:'json',
                url:springUrl + "/process/processProxy_delete",
                data:{
                    id:id
                },
                success:function (data) {
                    Dialog.alert("成功","代理人删除成功！");
                    Table.render(userTable);
                },
                error : function(data){
                    Dialog.alert("失败","代理人删除失败！");
                }
            });
        }
    });
}

function remoteOrgUserInfo() {
    var buttons = [
        {
            "label": "取消",
            "class": "",
            "callback": function () {
//                cancel();
            }
        },
        {
            "label": "确定",
            "class": "btn-success active1",
            "callback": function () {
                var selectRecords = Table.getSelectedRows(orgUserDataTable);
                if(selectRecords.length>1)   {
                    Dialog.alert("提示信息","每次只能选择一个代理人");
                    return false;
                }else{
                    if(selectRecords.length==0){
                        Dialog.alert("提示信息","请选择代理人");
                        return false;
                    }else{
                        $("#user_id").val(selectRecords[0].id);
                        $("#user_name").val(selectRecords[0].realName);
                        $('input[name="user_id"]').trigger("validate");
                    }
                }

            }
        }
    ];
    Dialog.openRemote('选择用户', springUrl + '/system/sysAddress_getOrgUser',820, 500, buttons);
}
/**
 * 对比日期大小
 * @param a
 * @param b
 * @return {boolean}
 */
function compareDateTime(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes >= lktimes) {
        return false;
    }
    else{
        return true;
    }
}

function addProxy(){
    $("#user_id").val("");
    $("#proxyBeginTimeTest").val("");
    $("#proxyBeginTime").val("");
    $("#user_name").val("");
    $("#proxyEndTimeTest").val("");
    $("#proxyEndTime").val("");
    $(".msg-box").hide();
    $("#cancelBt").show();
    $("#formGroup").show();
}
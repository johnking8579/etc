
$(function(){
	
	
	$("input[name=processTypeOptions]").click(function(){
		if($(this).val()=='1'){
			$("#insert_processParent").css("display","none");
			$("#insert_processTypeNameLabel").text("一级名称 :");
			$('#insert_processTypeNameParent').attr("data-rule","");
		}else if($(this).val()=='2'){
			$("#insert_processParent").css("display","");
			$("#insert_processTypeNameLabel").text("二级名称 :");
			$('#insert_processTypeNameParent').attr("data-rule","required");
		}
	});;
	
	//是否外部url checkbox
	$("#is_outer").click(function(){
		if($(this).prop('checked')){
			$("#link_urlLabel").css("display","") ;
		}else{
			$("#link_urlLabel").css("display","none") ;
		}
	});
	
	//根据是否有id初始化数据
	initial();
	
	//验证
	//$('#processTypeAddForm').validator();
	bindValidate($('#processTypeAddForm'));
	
});

function initial(){
	//如果不为空，则执行修改操作，否则执行新增
	if(processTypeId!=null && processTypeId!=""){
			//根据id获取流程分类
			var ao=[];
			ao.push(
				{ "name": "processTypeId", "value":processTypeId}) ;
			
			jQuery.ajax( {
				async: false ,
		        type: "POST", 
		        url:"processTypeById", 
		        //dataType: "json",
		        data: ao, 
		        success: function(resp) {
		        	var processType = eval("("+resp+")");
//		        	console.log(processType);
//		        	console.log('success:'+processType.processTypeName+processType.processTypeCode);
		        	$("#insert_processTypeName").val(processType.processTypeName);
		        	$("#insert_sort_no").val(processType.sortNo);
		        	
		        	if(processType.parentId!=null && processType.parentId!=""){
		        		$("input[name=processTypeOptions]").eq(1).click();
		        		$("#insert_processTypeNameParent").val(processType.parentId);
		        		
		        	}
		        	
		        	if(processType.isOuter != null && processType.isOuter=='1'){
		        		$("#is_outer").attr("checked","checked") ;
		        		$("#link_url").val(processType.linkUrl) ;
		        		$("#link_urlLabel").css("display","") ;
		        	}
		        	
		        	$("input[name=processTypeOptions]").attr("disabled","true");
		        }
			});
	
	}
}
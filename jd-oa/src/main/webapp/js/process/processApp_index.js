/**
 * 流程-DEMO测试页JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

function getTask(){
    jQuery.ajax( {
        type: "POST",
        url: springUrl + "/process/processApp_getTask",
        dataType: "json",
        data: {
            processDefinitionKey:$("#processDefinitionKey").val()
        },
        success: function(data) {
            JSON.stingify(data);
            var html = "";
            for(var i = 0; i<data.length; i++){
                html = html + "<tr>";
                html = html + "<td>" + data[i].id + "</td>";
                html = html + "<td>" + data[i].taskDefinitionKey + "</td>";
                html = html + "<td><a href='#' onclick=\"openTaskHandleIndex('任务处理页面','" + data[i].processInstanceId + "','" + data[i].id + "')\">" + data[i].name + "</a></td>";
                html = html + "<td>" + data[i].processInstanceId + "</td>";
                html = html + "</tr>";
            }
            $("tbody").append(html);
        },
        error: function (data) {
            Dialog.alert("失败","待办任务列表查询失败");
        }
    });
}

/**
 * 创建流程实例
 */
function createProcessInstance(){
	Dialog.confirm("创建流程实例","确定创建流程实例?","确定","取消",function(result){
        if(result){
        	openWorkflowBindReport($("#processDefinitionKey").val());
        }
    });
}

/**
 * 创建流程实例并推进第一个任务
 */
function createSubmitProcessInstance(){
    Dialog.confirm("创建流程实例","确定创建流程实例并推进第一个任务?","确定","取消",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                dataType:'json',
                url:springUrl + "/app/processApp_submitInstance",
                data:{
                    processDefinitionKey:$("#processDefinitionKey").val()
                },
                success:function (data) {
                    Dialog.alert("成功","流程实例ID : " + data.id);
                    
                },
                error:function(data){
                    Dialog.alert("失败",data.responseText);
                }
            });
        }
    });
}

function openWorkflowBindReport(processTaskInstanceId){
	var processDefinitionKey = document.getElementById('processDefinitionKey').value;
	window.open(springUrl + "/app/processApp_open_bindReport?processDefinitionKey=" + processDefinitionKey + "&processTaskInstanceId=" + processTaskInstanceId,processTaskInstanceId,"location=no,menubar=yes,toolbar=no,status=yes,directories=no,scrollbars=yes,resizable=yes,width=" + window.screen.width + ",height=" + window.screen.height);
}

/**
 * 推进流程
 * @param id 文件ID
 */
function completeTask(id){
    Dialog.confirm("推进流程","确定推进流程?","确定","取消",function(result){
        if(result){
            jQuery.ajax({
                type:"POST",
                cache:false,
                dataType:'json',
                url:springUrl + "/process/processApp_completeTask",
                data:{
                    taskInstanceId:id
                },
                success:function (data) {
                    Dialog.alert("成功","推进成功");
                    Table.render(fileTable);
                },
                error : function(data){
                    Dialog.alert("失败",data.responseText);
                }
            });
        }
    });
}

/**
 * 初始化页面
 */
$(document).ready(function() {
    $('#create').click(function() {
        createProcessInstance();
    });
    $('#createSubmit').click(function() {
        createSubmitProcessInstance();
    });
        
    $('#search').click(function() {
        getTask();
    });
});

function openMyTaskBindDyForm(id,taskDefinitionKey,name,processInstanceId){
//	window.open(sprintUrl + "/app/app_execute_worklist_bindReport_open?",id,"location=no,menubar=yes,toolbar=no,status=yes,directories=no,scrollbars=yes,resizable=yes,width=" + window.screen.width + ",height=" + window.screen.height);
	return false;
}

/**
 * 弹出任务处理页面
 * @param processName 流程名称
 * @param processInstanceId 流程实例ID
 * @param taskInstanceId 任务实例ID
 */
function openTaskHandleIndex(processName,processInstanceId,taskInstanceId){
    Dialog.openRemote(processName,springUrl + "/app/taskHandle_index?processInstanceId="+processInstanceId+"&taskInstanceId="+taskInstanceId,"750","450");
}
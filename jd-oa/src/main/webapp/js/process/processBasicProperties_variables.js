var processBasicProperties_variables ;
var processBasicProperties_options ;
$( function(){
	
	 $("input[type='radio'][name='isOuter']").click(function(){
		 if($(this).val() == 0) {$("#controlOfDatasource").css("display","none");$("#variableNo").css("display","");}
		 else{ $("#controlOfDatasource").css("display","inline-block");$("#variableNo").css("display","none");}
	 });
	
	
	$("#addVariable").click( function(){
		var variableName = $("#variableName").val() ;
		var variableNo = $("#variableNo").val() ;
		var processId = getUrlParam(window.location.search+'','processId') ;
		var isOuter = $("input[type='radio'][name='isOuter']:checked").val();
		var aoData = [] ;
		aoData.push({ "name": "processId",   "value": processId   });
		aoData.push({ "name": "variables_name",   "value": variableName   });
		if(isOuter == '0'){
			
			aoData.push({ "name": "variables_no",   "value": variableNo   });
		}
			
		else if(isOuter == '1'){
			var dataAdapterType=$("#dataAdapter").find("option:selected").attr("data-type");//类型 1:DB数据源类型 2:接口类型
			var dataAdapterKey=$("#dataAdapter").find("option:selected").text();
		    var dataAdapter=$("#dataAdapter").val();
			aoData.push({ "name": "variables_no",   "value":   "dataSource#"+(dataAdapterType-1)+"#" + dataAdapter +"#" + dataAdapterKey});
		}
			
		
		 jQuery.ajax({
             url: springUrl + "/process/variable_insert",
             data: aoData,
             type:"POST",
             success: function (resp) {
            	 if(parseInt(resp)!=0){
                	 Dialog.alert("操作结果","新增变量成功！") ;
                	 Table.render(processBasicProperties_variables) ; 
            	 }else{
            		 Dialog.alert("操作结果","新增变量失败！") ;
            	 }
            	 

             },
             error:function(msg){
             }
         });
	});
	
	
	processBasicProperties_options  = {
        "callback_fn": function () {
        },
        isPaging: false,
        useCheckbox: false,
        pageUrl: springUrl +"/process/variable_list",
        defaultSort: [ ],
        sendData: function (sSource, aoData, fnCallback) {
        	var processId = getUrlParam(window.location.search+'','processId') ;
        	aoData.push({"name":"processId","value":processId}) ;
        	
            jQuery.ajax({
                type: "POST",
                url: sSource,
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp); //不能删除，否则数据无法展示出来
                },
                error:function(msg){
                	console.log("shibai");
                }
            });
        },
        // 分页列设置
        columns: [
            { "mDataProp": "configId", "sTitle": "变量", "bVisible":false, "bSortable": false, "sClass": "my_class","sWidth": "60px"},
            { "mDataProp": "name", "sTitle": "变量", "bSortable": true, "sClass": "my_class","sWidth": "60px"},
            { "sTitle": "变量值", "bSortable": false, "sClass": "my_class", "sWidth": "90px","fnRender":function(obj){
            	var id = obj.aData.id ; 
            	var valNo = obj.aData.valNo;
            	if(valNo != null && valNo.indexOf("dataSource#") > -1)
            		{
            			var arr = valNo.split("#");
            			
            			return arr[3] ? '[数据源]'+arr[3] : '' ;
            		}
            	return "<a onclick=variableCandidateUser('"+ id +"') title='设置变量值' href='#'>"+valNo+"</a>&nbsp;";
            }},
            { "mDataProp": "expression", "sTitle": "条件表达式", "bSortable": false, "sClass": "my_class", "sWidth": "90px"},
            { "sTitle": "操作", "bSortable": false, "sClass": "my_class", "sWidth": "90px","fnRender":function(obj){
            	var id = obj.aData.id ; 
            	var valNo = obj.aData.valNo;
            	if(valNo != null && valNo.indexOf("dataSource#") > -1){
            		
            		return "<a onclick=variableDelete('"+id+"') title='删除' class='btn' href='#'>删除</a>";
            	}
            	return "<a onclick=expOpen('"+ id +"') title='配置' class='btn' href='#'>配置表达式</a>&nbsp;&nbsp;"
            	+"<a onclick=variableDelete('"+id+"') title='删除' class='btn' href='#'>删除</a>";
            }}
        ],
        // 分页按钮设置
        btns: []
    };
});


//流程变量（办理人）
function variableCandidateUser(id){
	window.parent.variableCandidateUser(id);
}

function loadSelectData(){
		if(processBasicProperties_variables != null){
			Table.render(processBasicProperties_variables) ; 
		}else{
			processBasicProperties_variables=Table.dataTable("processBasicProperties_variables", processBasicProperties_options) ;
		}

}




//流向表达式
function expOpen( id  ){
	
	
	var base = window.parent.document.getElementById("baseFrame").contentWindow.document ;
	
	var formName = $("#formIdSelect",base).text() ;
	
	
	var formId = $("#formId",base).val() ;

	if(formId==undefined || formId=="") {Dialog("提示","清先在【基本属性】标签页选择表单！");return}
	var processDefinitionId = getUrlParam(window.location.search+'','processId') ;
	
	window.parent.expOpen(processDefinitionId,formId,id) ;


}



function variableDelete(id){
	
	var aoData = [] ; 
	aoData.push({ "name": "id",   "value": id   }) ;
	
	 jQuery.ajax({
         url: springUrl + "/process/variable_delete",
         data: aoData,
         type:"POST",
         success: function (resp) {
        	 if(parseInt(resp)!=0){
        		 Dialog.alert("操作结果","删除成功！") ;
        		 Table.render(processBasicProperties_variables) ; 
        	 }else{
        		 Dialog.alert("操作结果","删除失败！") ;
        		 Table.render(processBasicProperties_variables) ; 
        	 }
        	 
         },
         error:function(msg){
//         	console.log("shibai") ;
         }
     });
}






function getUrlParam(wlocation,name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = wlocation.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	} 

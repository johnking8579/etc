/**
 * 文档-文件管理添加文件JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

$(function() {
    $('#processNodeFormPrivilege').click(function() {
        loadprocessNodeFormPrivilegePage("process:1:1","111","2222");
    });

});

function loadprocessNodeFormPrivilegePage(processDefinitionId, nodeId, formId){
    Dialog.openRemote("表单个性化",springUrl+"/processNodeFormPrivilege/index?processDefinitionId="+processDefinitionId+"&nodeId="+nodeId+"&formId=0001","800","450",[
        {
            'label': '取消',
            'class': 'btn-success',
            'callback': function() {
            }
        },{
            'label': '确定',
            'class': 'btn-success',
            'callback': function() {
                subForm();
            }
        }]
    );
}

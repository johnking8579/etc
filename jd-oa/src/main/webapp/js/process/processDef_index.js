var processDef_table;
var processDef_tableData;
//流程类别的数据缓存
var processTypeData;
//组织类型的数据缓存
var organizationTypeData;

var variable_id ;
$(function(){ 
	//加载页面数据
	getProcessDefType("processType");
	$("select[name='processType']").attr("onchange","getChildrenProcessType('processType','childProcessType')");
	$("select[name='isIsSue']").attr("onchange","reset();");
	getOrganizationType("orgType");
	//加载表格
	var	processDef_options={
			pageUrl:springUrl+"/process/processDef_page",
			useCheckbox:true,
			defaultSort:[],
			isPaging:true,
			bAutoWidth:true,
			scrollY:"500px",
			callback_fn:function(){
			},
			sendData:function ( sSource, aoData, fnCallback ) {
				var processType=$("select[name='processType']").val();
				var childProcessType=$("select[name='childProcessType']").val();
				var processName=$("input[name='processName']").val();
				processName=processName.replace("_","\\_");
				var isIsSue=$("select[name='isIsSue']").val();
				var isOuter=$("select[name='isOuter']").val();
				var owner=$("input[name='owner']").val();
				if(owner!=null){
					owner=owner.replace("_","\\_");
				}
				var orgType=$("select[name='orgType']").val();
				aoData.push( 
						{ "name": "processTypeId", "value":processType}
						,{ "name": "childProcessTypeId", "value":childProcessType}
						,{ "name": "processDefinitionName", "value":processName}
						,{ "name": "isIssue", "value":isIsSue}
						,{ "name": "isOuter", "value":isOuter}
						,{ "name": "owner", "value":owner}
						,{ "name": "organizationType", "value":orgType}
						,{ "name": "isTemplate", "value":"0"}
						);   
				jQuery.ajax( {
		                    type: "POST", 
		                    url:sSource, 
		                    dataType: "json",
		                    data: aoData, 
		                    success: function(resp) {
		                    	processDef_tableData=resp;
		                            fnCallback(resp);
		                    },
		                    error:function(msg){
		                    	Dialog.alert("操作失败",msg.responseText);
		                    }
			    });
			},
			columns: [
	            { "mDataProp": "pafProcessDefinitionId","sTitle":"流程key","sClass": "my_class","sWidth":"70"},
	            { "mDataProp": "processDefinitionName","sTitle":"流程名称","sClass": "my_class","sWidth":"200"},
	            { "mDataProp": "processTypeId","sTitle":"流程类别","sClass": "my_class","sWidth":"70"},
//	            { "mDataProp": "sortNo","sTitle":"显示顺序","sClass": "my_class","sWidth":"70"},
	            { "mDataProp": "owner","sTitle":"所有人","sClass": "my_class","sWidth":"100"},
	            { "mDataProp": "organizationType","sTitle":"组织架构","sClass": "my_class","sWidth":"70","fnRender":function(obj){
	            	return (obj.aData.organizationType!=null&&obj.aData.organizationType=="1")?"EHR":"其他";
	            }},
	            { "mDataProp": "isIssue","sTitle":"发布状态","sClass": "my_class","sWidth":"70","fnRender":function(obj){
	            	return (obj.aData.isIssue!=null&&obj.aData.isIssue=="1")?"已发布":"未发布";
	            }},
	            { "mDataProp": "isOuter","sTitle":"流程标识","sClass": "my_class","sWidth":"70",bSortable:false,"fnRender":function(obj){
	            	if(obj.aData.isOuter!=null){
	            		if(obj.aData.isOuter=="0"){
	            			return "内部流程";
	            		}else if(obj.aData.isOuter=="1"){
	            			return "外部流程";
	            		}else if(obj.aData.isOuter=="2"){
	            			return "集成流程";
	            		}else{
	            			return "";
	            		}
	            	}
	            	
	            }},
	            { "bSortable":false,"sWidth":"300","sTitle":"操作","fnRender":function(obj){
					var id = obj.aData.id;
					var formId = obj.aData.formId;
					var processDefinitionName = obj.aData.processDefinitionName;
					var pafProcessDefinitionId = obj.aData.pafProcessDefinitionId;
					
					if (obj.aData.isOuter=="外部流程"){
						var html="<a href='#' class='btn' title='修改' onclick=\"edit(\'"+id+"\');\"><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"
						+"<a onclick=\"#\" class='btn disabled' href='#'><img alt='配置' src='"+springUrl+"/static/common/img/set.png'></a>&nbsp;"
						+"<a onclick=\"assgin(\'"+id+"\');\" title='授权申请人' class='btn' href='#'><img alt='授权申请人' src='"+springUrl+"/static/common/img/enjoy.png'></a>&nbsp;"
						+"<a onclick=\"#\" class='btn disabled' href='#'><img alt='授权加签人' src='"+springUrl+"/static/common/img/addsign.png'></a>&nbsp;"
						+"<a onclick=\"#\" class='btn disabled' href='#'><img alt='流程定义' src='"+springUrl+"/static/common/img/design.png'></a>&nbsp;"
						+"<a onclick=\"#\" class='btn disabled' href='#'><img alt='发布' src='"+springUrl+"/static/common/img/issue.png'></a>&nbsp;"
						+"<a onclick=\"del(\'"+id+"\');\" title='删除' class='btn' href='#'><img alt='删除' src='"+springUrl+"/static/common/img/del.png'></a>&nbsp;";
			
						return html;
						
					}else if(obj.aData.isOuter=="集成流程"){
						var html="<a href='#' class='btn' title='修改' onclick=\"edit(\'"+id+"\');\"><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"
						+"<a onclick=\"settingBusinessConfig(\'"+id+"\');\" title='配置' class='btn' href='#'><img alt='配置' src='"+springUrl+"/static/common/img/set.png'></a>&nbsp;"
						+"<a onclick=\"del(\'"+id+"\');\" title='删除' class='btn' href='#'><img alt='删除' src='"+springUrl+"/static/common/img/del.png'></a>&nbsp;";
			
						return html;
						
					}else{
						var html="<a href='#' class='btn' title='修改' onclick=\"edit(\'"+id+"\');\"><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"
						+"<a onclick=\"setting(\'"+id+"\');\" title='配置' class='btn' href='#'><img alt='配置' src='"+springUrl+"/static/common/img/set.png'></a>&nbsp;"
						+"<a onclick=\"assgin(\'"+id+"\');\" title='授权申请人' class='btn' href='#'><img alt='授权申请人' src='"+springUrl+"/static/common/img/enjoy.png'></a>&nbsp;"
						+"<a onclick=\"assignAddSigner(\'"+id+"\');\" title='授权加签人' class='btn' href='#'><img alt='授权加签人' src='"+springUrl+"/static/common/img/addsign.png'></a>&nbsp;"
						+"<a onclick=\"design(\'"+id+"\',\'"+formId+"\',\'"+processDefinitionName+"\',\'"+pafProcessDefinitionId+"\');\" title='流程定义' class='btn' href='#'><img alt='流程定义' src='"+springUrl+"/static/common/img/design.png'></a>&nbsp;"
						+"<a onclick=\"start(\'"+id+"\');\" title='发布' class='btn' href='#'><img alt='发布' src='"+springUrl+"/static/common/img/issue.png'></a>&nbsp;"
						+"<a onclick=\"view(\'"+pafProcessDefinitionId+"\');\" title='预览JSS缓存' class='btn' href='#'><img alt='预览JSS缓存' src='"+springUrl+"/static/common/img/view_obj.gif'></a>&nbsp;"
						+"<a onclick=\"del(\'"+id+"\');\" title='删除' class='btn' href='#'><img alt='删除' src='"+springUrl+"/static/common/img/del.png'></a>&nbsp;";
			
						return html;
					}
			}}
	        ],
			btns:[
					{
					    "sExtends":    "text",
					    "sButtonText": "新增",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							Dialog.openRemote("添加流程定义",springUrl+"/process/processDef_add", 550, 425,
							[
					 			{
					 				"label": "取消",
					 				"class": "btn-success",
					 				"callback": function() {}
					 			},                                                       			                                                                 
					 			{
					 				"label": "保存",
					 				"class": "btn-success",
					 				"callback": function(modal,e) {
					 					var validateEl=$('#validateForm');
					 					if(!validate2(validateEl)){
					 						return false;
					 					}
					 					 var obj= addProcessDef();
                                         if (obj.bool) {
                                        	 Table.render(processDef_table);
                                        	 if (obj.isOuter=="0"){
                                        		 setting(obj.processId);
                                        	 }else{
                                        		 Dialog.alert("操作结果", "保存流程定义成功!");                                        		 
                                        	 }      
                                         } else {
                                             Dialog.alert("操作结果", obj.message);
                                             return false;
                                         }
					 				}
					 			}                                                       			                                                                 
							]);
					    }
					},
					{
					    "sExtends":    "text",
					    "sButtonText": "另存为模板",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							 var resIds=Table.getSelectedRowsIDs(processDef_table);
							 if(resIds==null||resIds.length==0){
								 Dialog.alert("消息提示","没有选中项","确定");
								 return false;
							 }
							 var data={
									"idstr":JSON.stringify(resIds)
							 };
							 Dialog.post(springUrl+"/process/processDef_saveToTemplete",data,function(result){
									Table.render(processDef_table);
									Dialog.alert("操作结果",result.message);
							 });
					    }
					},	
					{
						"sExtends":    "text",
						"sButtonText": "变更所有人",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							 var resIds=Table.getSelectedRowsIDs(processDef_table);
							 if(resIds==null||resIds.length==0){
								 Dialog.alert("消息提示","没有选中项","确定");
								 return false;
							 }
							 var buttons = [
							                   {
							                       "label": "取消",
							                       "class": "btn-success",
							                       "callback": function () {
							                       }
							                   },
							                   {
							                       "label": "保存",
							                       "class": "btn-success",
							                       "callback": function () {
							                           var selectRecords = $("#updateOwner").val();
							                           if(selectRecords==null||selectRecords==""){
							                               Dialog.alert("提示信息","请选择变更人");
							                               return false;
							                           }else{
							                        	    var data={
							                        				"resIds":JSON.stringify(resIds),
							                        				"ownerId": selectRecords
							                        	    };
							                        		Dialog.post(springUrl+"/process/processDef_changeOwner",data,function(result){
							                        				if(result.operator){
							                        					Table.render(processDef_table);
							                        				}
							                        				Dialog.alert("提示信息",result.message);
							                        		});
							                           }

							                       }
							                   }
							               ];
							 Dialog.openRemote('变更所有人', springUrl+'/form/formOwner_update', 500, 200, buttons);
							 //Dialog.openRemote('选择用户', springUrl + '/system/sysAddress_getOrgUser',650, 600, buttons);
						}
					},	
					{
						"sExtends":    "text",
						"sButtonText": "批量授权",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							 var resIds=Table.getSelectedRowsIDs(processDef_table);
							 if(resIds==null||resIds.length==0){
								 Dialog.alert("消息提示","没有选中项","确定");
								 return false;
							 }
							expressionId="";
							Dialog.openRemote('授权用户',springUrl+'/auth/authExpression_select',1000,500,
									[
										{
										    'label': '取消',
										    'class': 'btn-success',
										    'callback': function() {}
										 },
										 {
											 'label': '确定',
											 'class': 'btn-success',
											 'callback': function() {
												 var authIds=selectOk();
												 if(!assginAll(resIds,authIds,"process")){
													 return false;
												 }
												 Dialog.alert("操作结果","授权成功");									 
											 }
										 }
										    
									]);
						}
					},	
					{
						"sExtends":    "text",
						"sButtonText": "导出",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							var ids=Table.getSelectedRowsIDs(processDef_table);
							if(ids.length==0){
								Dialog.alert("消息提示","没有选中项","确定");
							}else if(ids.length>1){
								Dialog.alert("消息提示","暂不支持批量导出","确定");
							}else{
								processDefExport(ids);
							}
						}
					},
					{
						"sExtends":    "text",
						"sButtonText": "导入",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
//							$("#file").trigger("click");
							 upfileAction('n1');
						}
					},{
						"sExtends":    "text",
						"sButtonText": "缓存至JSS",
						"sToolTip": "",
						"fnClick": function ( nButton, oConfig, oFlash ) {
							var ids=Table.getSelectedRowsIDs(processDef_table);
							if(ids.length==0){
								Dialog.alert("消息提示","没有选中项","确定");
							}else{
								jssUpload();
							}
						}
					},
					{
							    "sExtends":    "text",
							    "sButtonText": "删除",
								"sToolTip": "",
								"fnClick": function ( nButton, oConfig, oFlash ) {
									var ids=Table.getSelectedRowsIDs(processDef_table);
									if(ids.length==0){
										Dialog.alert("消息提示","没有选中项","确定");
									}else{
										delAll(ids);
									}
							    }
					}
			]
	};
	processDef_table=Table.dataTable("processDefTable",processDef_options);
	bindValidate($("#searchform"));
	$('#searchBtn').click(function() {
		if(validate2($("#searchform"))){
			Table.render(processDef_table); 
		}
	} );
	
});
var wind;
function upfileAction(fieldName) {
//    wind = new FlexWindow(fieldName + "&flag=imp");
//    wind.open();
	//springUrl+'/app/process_FlexUpFile?fieldName=' + fieldName + '&flag=imp
	Dialog.openSimple('上传附件',"<iframe frameborder='0' style='overflow:hidden' src='" + springUrl+ "/app/process_FlexUpFile?fieldName=" + fieldName + "&flag=imp' width='590' height='320'></iframe>",615,320);
}
//获取流程类别数据
function getProcessDefType(objName){
	if(processTypeData==null){
		jQuery.ajax({
			type : "post",
			async:false,
			url : springUrl + "/process/processNameAjax_view",
			dataType : "json",
			success : function(result) {
				processTypeData=result;
			},
            error:function(msg){
            	Dialog.alert("操作失败",msg.responseText);
            }
		});
	}
	var options = "";
	$.each(processTypeData, function(i, type) {
		options += "<option value='" + type.id + "'>"
				+ type.processTypeName + "</option>";
	});
	$("select[name='"+objName+"']").append(options);
}
//加载流程类别子类
function getChildrenProcessType(parentName,childName) {
	var id = $("select[name='"+parentName+"']").val();
	if(id==null||id==""){
		$("select[name='"+childName+"']").find("option").remove();
		var options = "<option value=''>不限</option>";
		$("select[name='"+childName+"']").html(options);
//		$("select[name='"+childName+"']").hide();
		return;
	}else{
		$("select[name='"+childName+"']").show();
	}
	var data = {
			"parentId" : id
	};
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl + "/process/processChildNameAjax_view",
		data : data,
		dataType : "json",
		success : function(result) {
			var options = "<option value=''>不限</option>";
			$.each(result, function(i, type) {
				options += "<option value='" + type.id + "'>"
				+ type.processTypeName + "</option>";
			});
			$("select[name='"+childName+"']").html(options);
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
}
//加载组织类型数据
function getOrganizationType(orgTypeName) {
	if(organizationTypeData==null){
		var data = {
				"dictTypeCode" : "organizationType"
		};
		jQuery.ajax({
			type : "post",
			async:false,
			url : springUrl + "/dict/dictData_findDictDataList",
			data : data,
			dataType : "json",
			success : function(result) {
				organizationTypeData=result;
			},
            error:function(msg){
            	Dialog.alert("操作失败",msg.responseText);
            }
		});
	}
	var options = "";
	$.each(organizationTypeData, function(i, org) {
		options += "<option value='" + org.dictCode + "'>"
				+ org.dictName + "</option>";
	});
	$("select[name='"+orgTypeName+"']").append(options);
}
//流程导出操作
function processDefExport(ids){
	var url = springUrl + "/process/processDef_export?ids=" + JSON.stringify(ids);
	window.location.href = encodeURI(url);
}
/**
 * 上传成功后，进行导入操作
 * @param filePath
 * @param fileName
 * @param tmp
 */
function returnUploadSucess(filePath,fileName,tmp){
	var flag = window.confirm("确定要执行导入吗?");
	if(flag){
		Dialog.hide();
		var data={
				"fileName":fileName
		};
		Dialog.post(springUrl+"/process/processDef_import",data,function(result){
			if(result.operator==true){
				Dialog.alert('提示消息','导入成功!');
				Table.render(processDef_table);
			}
		});
	}
}
//流程导入操作
function processUpload(){
	var url=springUrl + "/process/processDef_import";
	var filepath=$("#file").val();
	var fileArr=filepath.split("\\");
	var fileTArr=fileArr[fileArr.length-1].toLowerCase().split(".");
	var filetype=fileTArr[fileTArr.length-1];
	if(filetype!="zip"){
		Dialog.alert("操作信息","上传文件类型必须是zip格式！");
		return false;
	}
	$("#processUploadFile").attr("action",url);
	$("#processUploadFile").submit();
}
//修改操作
function edit(id){
	Dialog.openRemote("修改流程定义",springUrl+"/process/processDef_update?id="+id, 550,400,
			[
	 			{
	 				"label": "取消",
	 				"class": "btn-success",
	 				"callback": function() {}
	 			},                                                       			                                                                 
	 			{
	 				"label": "保存",
	 				"class": "btn-success",
	 				"callback": function() {
	 					var validateEl=$('#validateForm');
	 					if(!validate2(validateEl)){
	 						return false;
	 					}
	 					 var obj= addProcessDef();
                         if (obj.bool) {
                             Table.render(processDef_table);
                             if (obj.isOuter=="0"){
                                 setting(id);
                        	 }else{
                        		 Dialog.alert("操作结果", "保存流程定义成功!");                                        		 
                        	 }     
                         } else {
                             Dialog.alert("操作结果", obj.message);
                             return false;
                         }
	 				}
	 			}                                                       			                                                                 
			]);
}
//配置操作
//如果页面上定义了该变量，会直接读变量expressionId，没有变量会调用getExpressionId方法
var expressionId;
function getExpressionId(){
	return expressionId;
}
function setting(id){
	Dialog.openRemote("配置流程属性",springUrl+"/process/processDef_setting?id="+id, 1000, 550,
			[
	 			{
	 				"label": "取消",
	 				"class": "btn-success",
	 				"callback": function() {}
	 			},                                                       			                                                                 
	 			{
	 				"label": "保存",
	 				"class": "btn-success",
	 				"callback": function() {
	 					var iframe1 = document.getElementById("baseFrame").contentWindow;
	 					if(!iframe1.collectAllDatas()){
	 						Dialog.alert("操作结果","操作失败！");
	 						return false;
	 					}
	 					//不点击该tab就不会加载页面，因而不会有selectOk函数
	 					if(typeof selectOk == 'function'){
	 	 					var authIds=selectOk();
							var resIds=new Array();
							resIds.push(id);
							if(!assginAll(resIds,authIds,"supervise")){
								Dialog.alert("操作结果","操作失败！");
								return false;
							}
	 					}
	 					
	 					
	 					var iframe2 = document.getElementById("administratorFrame").contentWindow;
	 					if(iframe2 && iframe2.saveAdministrator){
	 						iframe2.saveAdministrator();
	 					}
	 					
	 					Table.render(processDef_table);
	 					Dialog.alert("操作结果","操作成功！");
	 				}
	 			}                                                       			                                                                 
			]);
}

//统一待办配置业务数据表关键字段
function settingBusinessConfig(id){
	Dialog.openRemote("配置业务数据关键字段",springUrl+"/process/processDef_settingBusinessConfig?id="+id, 550, 400,
			[
			 	{
			 		"label": "取消",
			 		"class": "btn-success",
			 		"callback": function() {}
			 	},
			 	{
			 		"label": "保存",
			 		"class": "btn-success",
			 		"callback": function() {
			 			var validateEl=$('#validateForm');
	 					if(!validate2(validateEl)){
	 						return false;
	 					}
	 					validateConfigItemSize();
	 					var isConfigItemSize=$('#isConfigItemSize').html();
	 					if(isConfigItemSize!=''){
	 						return false;
	 					}
	 					var obj = addSettingBusinessConfig();
	 					if (obj.bool) {
                            Table.render(processDef_table);
                            Dialog.alert("操作结果", "操作成功！"); 
                        } else {
                            Dialog.alert("操作结果", obj.message);
                            return false;
                        }
			 		}
			 		
			 	}
			]);
}

//授权
function assgin(id){
	var data={
			"id":id,
			"businessType":"process"
	};
	Dialog.post(springUrl+"/process/processDef_findAuthIds",data,function(result){
		expressionId=JSON.stringify(result);
	});
	Dialog.openRemote('授权用户',springUrl+'/auth/authExpression_select?selectedIds='+expressionId,1000,550,
			[
				{
				    'label': '取消',
				    'class': 'btn-success',
				    'callback': function() {}
				 },
				 {
					 'label': '确定',
					 'class': 'btn-success',
					 'callback': function() {
						 var authIds=selectOk();
						 var resIds=new Array();
						 resIds.push(id);
						 if(!assginAll(resIds,authIds,"process")){
							 return false;
						 }
						 Dialog.alert("操作结果","授权成功");
						 Table.render(processDef_table);
					 }
				 }
				    
			]);
}


//授权加签人
function assignAddSigner(id){
	var data={
			"id":id,
			"businessType":"addsign"
	};
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl+"/process/processDef_findAuthIds",
		data : data,
		success : function(result) {
			expressionId=JSON.stringify(result);
//			console.log(expressionIds);
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
	
	Dialog.openRemote('授权加签人',springUrl+'/auth/authExpression_select?selectedIds='+expressionId,1000,550,
			[
				{
				    'label': '取消',
				    'class': 'btn-success',
				    'callback': function() {}
				 },
				 {
					 'label': '确定',
					 'class': 'btn-success',
					 'callback': function() {
						 var authIds=selectOk();
						 var resIds=new Array();
						 resIds.push(id);
						 if(!assginAll(resIds,authIds,"addsign")){
							 return false;
						 }
						 Dialog.alert("操作结果","授权加签人成功");
						 Table.render(processDef_table);
					 }
				 }
				    
			]);
}

//批量授权
function assginAll(resIds,authIds,businessType){
	var bool=false;
    var data={
			"resIds":JSON.stringify(resIds),
			"authIds":JSON.stringify(authIds),
			"businessType":businessType
    };
	Dialog.post(springUrl+"/process/processDef_assgin",data,function(result){
			bool=result.operator;						
	});
	return bool;
}
//发布操作
function start(id){
	var ids = [{"id":id}];
    var data={
        "ids":JSON.stringify(ids)
    };
    Dialog.post(springUrl+"/process/processDef_sue",data,function(result){
        Table.render(processDef_table);
        Dialog.alert("操作结果",result.message);
    });
}
function startAll(ids){
    var idList = [];
    for(var i=0; i<ids.length; i++){
        idList.push({"id":ids[0]});
    }
    alert(JSON.stringify(idList));
    return;
	var data={
			"ids":JSON.stringify(idList)
		};
	Dialog.post(springUrl+"/process/processDef_sue",data,function(result){
		Table.render(processDef_table);
		Dialog.alert("操作结果",result.message);
	});
}
//删除操作
function del(id){
	var ids=new Array();
	ids.push(id);
	delAll(ids);
}
//批量删除操作
function delAll(ids){
	var data={
			"idstr":JSON.stringify(ids)
	};
	Dialog.del(springUrl+"/process/processDef_delete",data,function(result){
		Table.render(processDef_table);
		Dialog.alert("操作结果",result.message);
	});
}

//流程定义
function design(id,formId,processDefinitionName,pafProcessDefinitionId){
	var url = springUrl+"/design/tab.jsp?processDefinitionId="+id+"&formId="+formId+
			"&processDefinitionName="+encodeURI(encodeURI(processDefinitionName))+
			"&pafProcessDefinitionId="+pafProcessDefinitionId;
	window.open(url,id);
	//Dialog.openRemote("流程定义",springUrl+"/design/tab.jsp?id="+id+"&formId="+formId, 1100, 550,"");
}

//从processBaiscProperties_index.js移过来
function formIdSelectClick(){
//	console.log('a is clicked..');
	
	//查找表单
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			var bol = false; //process_insert(id);
//			console.log(formSearchTable);
			var data = Table.getSelectedRows(formSearchTable);
//			console.log(data);

			if(data.length>1){
				Dialog.alert("提示信息","只能选择单个表单！");return false;}
			if(data.length<1){
				Dialog.alert("提示信息","请选择一个表单 ! ");return false;}
			
			var baseFrame = document.getElementById("baseFrame").contentWindow.document;
			
			var formName = data[0].formName;
			var formId = data[0].id;
			var processId = getUrlParam(document.getElementById("baseFrame").contentWindow.location.search+'','processId');
			
			var data = {
					"processId" : processId,
					"formId": formId
			};
			
			jQuery.ajax({
				type : "get",
				async:false,
				url : springUrl + "/process/formChanged",
				data : data,
				success : function(result) {
//					console.log(result);
					$("#formIdSelect",baseFrame).text(formName);
					$("#formId",baseFrame).val(formId);
					
					//直接开始设置哪些列
					formColumnSelectClick();
				},
                error:function(msg){
                	Dialog.alert("操作失败",msg.responseText);
                }
			});
			
			return true;
			}			     							
	}];
	
	Dialog.openRemote('选择表单',springUrl+'/form/formSearch_index',600,500,buttons);
	
	//return false;
}

//配置表单显示列
function formColumnSelectClick(){
	var baseFrame = document.getElementById("baseFrame").contentWindow.document;
	var formId = $("#formId",baseFrame).val();
	var processId = getUrlParam(document.getElementById("baseFrame").contentWindow.location.search+'','processId');
	
//	console.log('---------------');
//	console.log(processId);
//	console.log(formId);
//	console.log('---------------');
	
	if(formId==null || formId==''){
		Dialog.alert("提示信息","请先选择表单！");
		return;
	}
	
	//查找表单
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){
			$("input[type='checkbox']",$('#mainFormItemList')).die();
			 selectedColums = [];
			 selectedSubViewColums = [];
			 selectedSubColums = [];
		}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			if(selectedColums.length<=0){
				Dialog.alert("提示信息","至少选择一个列 ! ");
				return false;
			}
			var data = {
					"processId" : processId,
					"formId": formId,
					"items": selectedColums,
					"subItems": selectedSubColums
			};
			jQuery.ajax({
				type : "get",
				async:false,
				url : springUrl + "/process/configColumns",
				data : data,
				dataType : "json",
				success : function(result) {
//					console.log(result);
				},
                error:function(msg){
                	Dialog.alert("操作失败",msg.responseText);
                }
			});
			$("input[type='checkbox']",$('#mainFormItemList')).die();
			 selectedColums = [];
			 selectedSubViewColums = [];
			 selectedSubColums = [];
			return true;
			}			     							
	}];
	
	Dialog.openRemote('配置表单显示列',springUrl+'/form/formItem_EditItemView?formId='+formId+'&processId='+processId,600,500,buttons);
	
}
//配置子表单显示列
function formSubColumnSelectClick(){
	var baseFrame = document.getElementById("baseFrame").contentWindow.document;
	var formId = $("#formId",baseFrame).val();
	var processId = getUrlParam(document.getElementById("baseFrame").contentWindow.location.search+'','processId');
	
	if(formId==null || formId==''){
		Dialog.alert("提示信息","请先选择表单！");
		return;
	}
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){
			$("input[type='checkbox']",$('#mainFormItemList')).die();
			 selectedColums = [];
			 selectedSubViewColums = [];
			 selectedSubColums = [];
		}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			if(selectedColums.length<=0){
				Dialog.alert("提示信息","至少选择一个列 ! ");
				return false;
			}
			var data = {
					"processId" : processId,
					"formId": formId,
					"items": selectedColums,
					"subItems": selectedSubColums
			};
			jQuery.ajax({
				type : "get",
				async:false,
				url : springUrl + "/process/configColumns",
				data : data,
				dataType : "json",
				success : function(result) {
//					console.log(result);
				},
                error:function(msg){
                	Dialog.alert("操作失败",msg.responseText);
                }
			});
			$("input[type='checkbox']",$('#mainFormItemList')).die();
			 selectedColums = [];
			 selectedSubViewColums = [];
			 selectedSubColums = [];
			 return true;
		}
	}];
	Dialog.openRemote('配置子表单显示列',springUrl+'/form/formItem_EditItemViewSub?formId='+formId+'&processId='+processId,600,500,buttons);
}

function formIdClear(){
	var baseFrame = document.getElementById("baseFrame").contentWindow.document;

	
	var formId = $("#formId",baseFrame).val();
	var processId = getUrlParam(document.getElementById("baseFrame").contentWindow.location.search+'','processId');
	
	if(formId== null || formId=='') return;
	
	var data = {
			"processId" : processId,
			"formId": formId
	};
	jQuery.ajax({
		type : "get",
		async:false,
		url : springUrl + "/process/formIdClear",
		data : data,
		dataType : "json",
		success : function(result) {
//			console.log(result);
			$("#formId",baseFrame).val('');
			$("#formIdSelect",baseFrame).text( ' 请选择表单 ');
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
	 
     

}


//打开变量表达式
function expOpen(processDefinitionId , formId ,id ){
	variable_id = "" ;
	variable_id = id ;
	var buttons = [{
		"label": "取消",
		"class": "btn-success",
		"callback": function(){}
	},
	{
		"label": "确定",
		"class": "btn-success",
		"callback": function(){
			var exp=saveTable();
            if(exp!="") {
//                   exp ='$'+'{'+exp+'}';
//                   console.log(exp) ;
                   var ao = [] ;
                   ao.push({"name":"id","value":id}) ;
                   ao.push({"name":"expression","value":exp}) ;
                   jQuery.ajax({
                       asyn: false ,
                       url: springUrl + "/process/variable_update" ,
                       data: ao,
                       success: function (resp) {
                           //fnCallback(resp); //不能删除，否则数据无法展示出来
//                    	   console.log( resp ) ;
                    	   var processBasicProperties_variables = document.getElementById("variablesFrame").contentWindow.processBasicProperties_variables ;
                    		Table.render(processBasicProperties_variables) ; 
                       },
                       error:function(msg){
//                       	console.log("shibai");
                       }
                   });
                   
            }
            
//			$("#condition").val(exp);
//			//设置流程定义
//			saveLineProperties(); 
		}			     							
	}];
	//var formId=$("#formId", parent.document);
	
//	var base = window.parent.document.getElementById("baseFrame").contentWindow.document;
//	var base = document.getElementById("baseFrame").contentWindow.document;
//	
//	var formName = $("#formIdSelect",base).text() ;
//	var formId = $("#formId",base).val() ;

//	var processDefinitionId = getUrlParam(window.location.search+'','processId');

//	var formId=$("#formId").val();
//    var processDefinitionId=$("#processDefinitionId").val();
	
//	var exp=$("#condition").val();
//	var param="?formId="+formId+"&exp="+encodeURI(encodeURI(exp)+"&processDefinitionId="+processDefinitionId);
	var param="?formId="+formId+"&processId="+processDefinitionId;

	
	//	var formId=$("#formId",parent.document).val();
		if(formId=="" || formId=="null"){
			Dialog.alert("提示信息", "获取表单信息失败！", "","");
		}else{
			Dialog.openRemote("流向表达式",springUrl + "/design/routerForVariable_index"+param, 800, 500,buttons);
		}
}


//流程变量（办理人）
function variableCandidateUser(id){
	var data={
			"id":id,
			"businessType":"variable"
	};
	jQuery.ajax({
		type : "post",
		async:false,
		url : springUrl+"/process/processDef_findAuthIds",
		data : data,
		success : function(result) {
			expressionId=JSON.stringify(result);
//			console.log(expressionIds);
		},
        error:function(msg){
        	Dialog.alert("操作失败",msg.responseText);
        }
	});
	
	Dialog.openRemote('流程变量（办理人）',springUrl+'/auth/authExpression_select?selectedIds='+expressionId,1000,500,
			[
				{
				    'label': '取消',
				    'class': 'btn-success',
				    'callback': function() {}
				 },
				 {
					 'label': '确定',
					 'class': 'btn-success',
					 'callback': function() {
						 var authIds=selectOk();
						 var resIds=new Array();
						 resIds.push(id);
						 if(!assginAll(resIds,authIds,"variable")){
							 return false;
						 }
						 selectOk=null;
						 Dialog.alert("操作结果","流程变量（办理人）设置成功");						 
					 }
				 }
				    
			]);
}

function getUrlParam(wlocation,name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = wlocation.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	} 

function jssUpload(){
	var datas = Table.getSelectedRows(processDef_table);
	var ids = "";
	var inner = 0;
	for (var i = 0; i < datas.length; i++) {
		if(datas[i].isOuter=='内部流程'){
			inner ++;
			ids+=datas[i].pafProcessDefinitionId+",";
		}
	}
	if(inner==0){
		Dialog.alert("错误信息","您选择的"+datas.length+"个流程中，没有内部流程！");
		return false;
	}
	Dialog.confirm("系统消息", "您共选择"+datas.length+"个流程，其中内部流程"+inner+"个，确定对"+inner+"个内部流程进行缓存?", "确定", "取消", function (result) {
    	if(result){
    		jQuery.ajax({
    			type : "post",
    			async:false,
    			dataType:"text",
    			url : springUrl+"/process/uploadWholeApplyForm2JSS",
    			data : 'ids='+ids,
    			success : function(result) {
    				Dialog.alert("操作结果",result);
    			},
    	        error:function(msg){
    	        	Dialog.alert("操作失败",msg.responseText);
    	        }
    		});
    	}
    }); 
	
}
//预览整个申请节点jss缓存表单
function view(processDefinitionKey){
	var url = springUrl + "/process/openApplyFormFromJss?processDefinitionKey="+processDefinitionKey ;
	var buttons = [
           {
           "label": "取消",
           "class": "inactive",
           "callback": function () {
           }
       }
   ];
	window.open(url);
//Dialog.openRemote("",url,1000,570,buttons);
	

}



var baseUrl = "#springUrl('')";
var resourceId = "${resourceId}";
var resourceCode = "${resourceCode}";
var resourceName = "${resourceName}";
				
//节点加签规则设置
function addSignSave(){
	
	//console.log("nodeId : "+ getUrlParam("nodeId"));
	//console.log("processId : "+ getUrlParam("processId"));
	//获取加签结果
	//console.log($("input[name='processNodeRule']:checked",$(document.getElementById("addSignFrame")).document).val());
	//获取加签规则
	var addsignRule = $("input[name='processNodeRule']:checked").val();
	
	//是否同步到所有节点
	var allUpdate = $("input[name='processNodeRuleForAll']:checked").val();
	
	//console.log("rule: "+addsignRule+", allUpdate: "+allUpdate);
	
	//添加发送Ajax数据
	var ao=[];
	
	ao.push({ "name": "nodeId", "value":getUrlParam("nodeId") });
	ao.push({ "name": "processId", "value":getUrlParam("processId") });
	if(addsignRule!=undefined && addsignRule!=null && addsignRule!='')
		ao.push({ "name": "addsignRule", "value":addsignRule });
	
	if(allUpdate!=undefined && allUpdate!=null && allUpdate!='')
		ao.push({ "name": "allUpdate", "value":allUpdate });
	
	//console.log("ao: "+ ao);
	
	//return false;
	
	var bol = false; //process_insert(id);
	
	jQuery.ajax( {
		async: false ,
        type: "POST", 
        url:"processNode_addSignUpdate", 
        data: ao, 
        success: function(resp) {
                if(parseInt(resp)>=1)
                	
                
                	bol=true;
        }
    });
	
	if(bol){
		Dialog.alert("提示信息","保存成功！");
		//Table.render(processNodeTable);
		return true;
	}else{
		Dialog.alert("提示信息","保存失败！");
		return false;
	}
}



//获取url的参数
function getUrlParam(name){

	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

	var r = window.location.search.substr(1).match(reg);  //匹配目标参数

	if (r!=null) return unescape(r[2]); return null; //返回参数值

	} 

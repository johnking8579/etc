/**
 * 流程-表单个性化配置页JS
 * User: zhaoming
 * Date: 13-9-13
 * Time: 上午10:13
 * To change this template use File | Settings | File Templates.
 */

$(function(){
});

function selectCheckbox(obj){
    var selectFlag = obj.id;


    var arr =  selectFlag.split("_");
    if(arr.length == 2){
        var type = arr[0];
        if(type == "null"){
            $("#isHidden_master").attr("checked",false);
            $("#isEdit_master").attr("checked",false);
            $("#isNull_master").attr("checked",false);
        }else if(type == "isHidden"){
            $("#null_master").attr("checked",false);
            $("#isEdit_master").attr("checked",false);
            $("#isNull_master").attr("checked",false);
        }else if(type == "isEdit"){
            $("#null_master").attr("checked",false);
            $("#isHidden_master").attr("checked",false);
            $("#isNull_master").attr("checked",false);
        }else if(type == "isNull"){
            $("#null_master").attr("checked",false);
            $("#isHidden_master").attr("checked",false);
            $("#isEdit_master").attr("checked",false);
        }
    }else{
        var type = arr[0];
        var suffix = arr[1];
        var count = arr[2];
        if(type == "null"){
            $("#isHidden_sub_"+count).attr("checked",false);
            $("#isEdit_sub_"+count).attr("checked",false);
            $("#isNull_sub_"+count).attr("checked",false);
        }else if(type == "isHidden"){
            $("#null_sub_"+count).attr("checked",false);
            $("#isEdit_sub_"+count).attr("checked",false);
            $("#isNull_sub_"+count).attr("checked",false);
        }else if(type == "isEdit"){
            $("#null_sub_"+count).attr("checked",false);
            $("#isHidden_sub_"+count).attr("checked",false);
            $("#isNull_sub_"+count).attr("checked",false);
        }else if(type == "isNull"){
            $("#null_sub_"+count).attr("checked",false);
            $("#isHidden_sub_"+count).attr("checked",false);
            $("#isEdit_sub_"+count).attr("checked",false);
        }
    }

    if($("#"+selectFlag).attr("checked")=="checked"){
        $("input[selectFlag='"+selectFlag+"']").attr("checked",true);
    }else{
        $("input[selectFlag='"+selectFlag+"']").attr("checked",false);
    }
}

function subForm(){
    var processNodeFormPrivilegeList = [];
    $.each($("input[flag='true']"), function(i,val){
        var processNodeFormPrivilege = {};
        processNodeFormPrivilege.id = $("#id_"+val.value).val();
        processNodeFormPrivilege.formId = $("#formId_"+val.value).val();
        processNodeFormPrivilege.itemId = $("#fieldName_"+val.value).val();
        processNodeFormPrivilege.isEdit = isCheckedContrary("isEdit_"+val.value);
        processNodeFormPrivilege.isNull = isCheckedContrary("isNull_"+val.value);
        processNodeFormPrivilege.isHidden = isChecked("isHidden_"+val.value);
        if (isChecked("isHidden_"+val.value)=="1") processNodeFormPrivilege.isEdit = "0";
        processNodeFormPrivilege.isVariable = isChecked("isVariable_"+val.value);
        processNodeFormPrivilegeList.push(processNodeFormPrivilege);
    });
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            processDefinitionId : $("#processDefinitionId").val(),
            nodeId : $("#nodeId").val(),
            processNodeFormPrivileges : JSON.stringify(processNodeFormPrivilegeList),
            formTemplateId : $("#formTemplateId").val(),
            printTemplateId : $("#printTemplateId").val()
        },
        url:springUrl + "/process/processNode_FormPrivilegeSave",
        success:function (data) {
            //Dialog.alert("成功","保存成功");
        	return true;
        },
        error: function (data) {
            Dialog.alert("失败","保存失败");
            return false;
        }
    });
}

function isChecked(obj){
    var flag = 0;
    if($("#"+obj).is(":checked")){
        flag = 1;
    }
    return flag;
}

function isCheckedContrary(obj){
    var flag = 1;
    if($("#"+obj).is(":checked")){
        flag = 0;
    }
    return flag;
}

/**
 * 预览
 * @returns {Boolean}
 */
function reviewTemplate(){
    subForm();
	setTimeout(function(){
		var date = new Date();
		window.open(springUrl + '/process/processNode_FormPrivilegeReview?processDefinitionId=' + $("#processDefinitionId").val() + '&nodeId=' + $("#nodeId").val() + "&formTemplateId=" + $("#formTemplateId").val() + '&formId= '+ document.getElementById('formId').value,'newwindow' + date.getTime(),'height=800px,width=800px,top=0,left=0,toolbar=no,menubar=no,scrollbars=yes, resizable=no,location=no, status=no') ;
	},1000);
	return false;
}




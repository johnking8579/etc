
var processNodeDynamicButton_table;
var processNodeDynamicButton_tableData;

$(function(){ 
	//加载表格
	var	processNodeDynamicButton_options={
			"callback_fn":function(){
			},
			isPaging: false,
			useCheckbox: false,
			defaultSort: [ ],
			
			sendData: function (sSource, aoData, fnCallback) {
				var processDefinitionId=$("input[id='processDefinitionId']").val();
				var nodeId=$("input[id='nodeId']").val();
				var formId=$("input[id='formId']").val();
	            aoData.push({
	                "name": "processDefinitionId",
	                "value": processDefinitionId
	            },{
	                "name": "nodeId",
	                "value": nodeId
	            });
	            jQuery.ajax({
	                type: "POST",
	                url: springUrl +"/process/processNode_dynamicButtonPage",
	                dataType: "json",
	                data: aoData,
	                success: function (resp) {
	                	processNodeDynamicButton_tableData=resp;
                        fnCallback(resp);
	                }
	            });
	        },
	        columns: [
	  			    { "mDataProp": "buttonName","sTitle":"按钮名称","bSortable": false,"sClass": "my_class","sWidth":"150"},
	  			    { "mDataProp": "buttonExegesis","sTitle":"按钮提示","bSortable": false,"sClass": "my_class","sWidth":"100"},
	  			    { "mDataProp": "form.formName","sTitle":"表单","bSortable": false,"sClass": "my_class","sWidth":"200"},
	  			    { "bSortable":false,"sTitle":"操作","fnRender":function(obj){
	  			    	var formId=$("input[id='formId']").val();
	  			    	var id = obj.aData.id;
	  			    	var html="<a href='#' class='btn' title='修改' onclick=\"edit(\'"+id+"\',\'"+formId+"\');\"><img src='"+springUrl+"/static/common/img/edit.png' alt='修改'></a>&nbsp;"
	  			    		+"<a onclick=\"del(\'"+id+"\');\" title='删除' class='btn' href='#'><img alt='删除' src='"+springUrl+"/static/common/img/del.png'></a>&nbsp;";
	  			    	return html;
	  			    }
	  			    }
	  		],
			btns:[
			  {
			    "sExtends":    "text",
			    "sButtonText": "新增",
				"sToolTip": "",
				"fnClick": function ( nButton, oConfig, oFlash ) {
					var processDefinitionId=$("input[id='processDefinitionId']").val();
					var nodeId=$("input[id='nodeId']").val();
					var formId=$("input[id='formId']").val();
					Dialog.openRemote("添加按钮",springUrl+"/process/extendButton_add?formId="+formId+"&processId="+processDefinitionId+"&nodeId="+nodeId, 550, 425,
					[
			 			{
			 				"label": "取消",
			 				"class": "btn-success",
			 				"callback": function() {}
			 			},                                                       			                                                                 
			 			{
			 				"label": "保存",
			 				"class": "btn-success",
			 				"callback": function(modal,e) {
			 					var validateEl=$('#validateForm');
			 					if(!validate2(validateEl)){
			 						return false;
			 					}
			 					 var obj= addDynamicButton();
                                 if (obj.bool) {
                                	 Dialog.alert("操作结果", obj.message);
                                	 Dialog.hideModal(modal);
                                	 loadSelectData();
                                	 //Table.render(processNodeDynamicButton_table);     
                                 } else {
                                     Dialog.alert("操作结果", obj.message);
                                    // Dialog.hideModal(modal);
                                     return false;
                                 }
			 				}
			 			}                                                       			                                                                 
					]);
			    }
			},
			{
				"sExtends":    "text",
				"sButtonText": "生成标准按钮",
				"sToolTip": "",
				"fnClick": function ( nButton, oConfig, oFlash ) {
					var obj= createStandardButtons();
					if (obj.bool) {
                   	 Dialog.alert("操作结果", obj.message);
                   	 loadSelectData();    
                    } else {
                        Dialog.alert("操作结果", obj.message);
                        return false;
                    }
				}
			}
			]
	};
	
	processNodeDynamicButton_table=Table.dataTable("processNodeDynamicButtonTable", processNodeDynamicButton_options) ;
	
});

function loadSelectData(){
	if(processNodeDynamicButton_table != null){
		Table.render(processNodeDynamicButton_table) ; 
	}else{
		processNodeDynamicButton_table=Table.dataTable("processNodeDynamicButtonTable", processNodeDynamicButton_options) ;
	}

}

//删除操作
function del(id){
	var data={
			"id":id
	};
	Dialog.del(springUrl+"/process/extendButton_delete",data,function(result){
		loadSelectData();
		Dialog.alert("操作结果",result.message);
	});
}

//修改操作
function edit(id,formId){
	Dialog.openRemote("修改按钮",springUrl+"/process/extendButton_update?id="+id+"&formId="+formId, 550,400,
			[
	 			{
	 				"label": "取消",
	 				"class": "btn-success",
	 				"callback": function() {}
	 			},                                                       			                                                                 
	 			{
	 				"label": "保存",
	 				"class": "btn-success",
	 				"callback": function(modal,e) {
	 					var validateEl=$('#validateForm');
	 					if(!validate2(validateEl)){
	 						return false;
	 					}
	 					 var obj= addDynamicButton();
                         if (obj.bool) {
                        	 loadSelectData()
                        	 Dialog.alert("操作结果", obj.message);
                        	 Dialog.hideModal(modal);    
                         } else {
                             Dialog.alert("操作结果", obj.message);
                             return false;
                         }
	 				}
	 			}                                                       			                                                                 
			]);
}

//自动生成子表标准按钮
function createStandardButtons(){
	var obj={};
	var processDefinitionId=$("input[id='processDefinitionId']").val();
	var nodeId=$("input[id='nodeId']").val();
	var formId=$("input[id='formId']").val();
	var data = {
			"processId" : processDefinitionId,
			"nodeId" : nodeId,
			"formId" : formId
	};
	Dialog.post(springUrl + "/process/extendButton_addStandardButtons", data, function(
			result) {
        obj.bool=result.operator;
        obj.message=result.message;
	});
	return obj;
}

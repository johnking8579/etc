/**
 * 文档-文件管理使用的tree js
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

var folderTree;
//根节点 folders 传入参数
var Nodes ={
	    "id":"null",
	    "name":"京东文档",
	    "isParent":true,
	    "iconSkin":"diy1"
	};
var set= {
	async : {
		enable : true,
		url :springUrl+"/doc/wffolder_treeLoad",
		autoParam : ["id"]
	},
	check: {
		enable: false,
		chkStyle: "radio"
	},
	callback : {
		 beforeClick : beforeClick
	},
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false,
		expandSpeed: "fast"
	}
};
function beforeClick(treeId, treeNode) {
    return true;
}
/**
 * 刷新父节点
 * @param pId 父节点ID
 */
function refreshNode(pId){
        var treeNode = folderTree.getNodeByTId("tree_1");
        folderTree.reAsyncChildNodes(treeNode, "refresh");
        var nodes = folderTree.transformToArray(folderTree.getNodes());
        var tId = "";
        for(var i=0; i<nodes.length; i++){
            if(nodes[i].id == pId){
                tId = nodes[i].tId;
                break;
            }
        }
        if(tId != ""){
            var treeNode = folderTree.getNodeByTId(tId);
            folderTree.reAsyncChildNodes(treeNode, "refresh");
        }
}
/**
 * Tree 双击响应
 */
function zTreeOnDblClick(event, treeId, treeNode){
	//增加和修改页面使用参数
	if(treeNode.id!='null'){
		$("#folderId").val(treeNode.id);
		$("#folderName").val(treeNode.name);
		$("#selectFolder").text(treeNode.name);
        Dialog.hide();
	}

}
/**
 * 获取树上选择的叶子节点
 */
function getSelectedNodes(){
	var nodes = folderTree.getSelectedNodes();
	return nodes;
}
/**
 * 单击事件
 * @param event
 * @param treeId
 * @param treeNode
 */
function zTreeOnClick(event, treeId, treeNode){
	if(treeNode.id!="null"){
		$("#file_name").val(treeNode.name);
	}
	folderTree.expandNode(treeNode);
}
/**
 * 初始化操作
 */
$(function() {
	jQuery.fn.zTree.init($("#folderTree"), set, Nodes);
    folderTree = jQuery.fn.zTree.getZTreeObj("folderTree");
});

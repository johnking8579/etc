/**
 * 文档-文件管理修改文件JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
/**
 * 初始化操作
 */
var editor;
$(function() {
//    绑定文件夹树加载按钮事件
    $('#selectFloder').click(function() {
        loadFolderTree();
    });
    //修改文件
    $('#updateFile').click(function() {
        if(!validate2($('#fileForm'))){
            return false;
        }
         updateFile();
    });
    //返回
    $('#returnFileIndex').click(function() {
        returnFileIndex();
    });
    //下载按钮绑定事件
    $("#downLoadFile").click(function(){
        downLoadFile();
    });
    KindEditor.ready(function(K) {
        editor = K.create('textarea[name="fileContent"]', {
            filterMode : false,
            resizeType : 0,
            items :[
                'source', '|', 'undo', 'redo', '|', 'preview', 'print','code', 'cut', 'copy', 'paste',
                'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|',
                'table', 'hr', 'emoticons', 'pagebreak',
                'anchor', 'link', 'unlink', '|', 'about'
            ]
        });
    });
    bindValidate($('#fileForm'));
});
//创建新文件页面-保存按钮。
function updateFile(){
    var fileName = "";
    var fileSuffix = "";
    var fileType = $('input:radio[name="fileType"]:checked').val();
    if(fileType == 0){
        if($('#file').val().lastIndexOf("\\") != -1){
            fileName = $('#file').val().substring($('#file').val().lastIndexOf("\\")+1,$('#file').val().lastIndexOf("."));
            fileSuffix = $('#file').val().substring($('#file').val().lastIndexOf(".")+1);
        }else{
            fileName = $('#file').val().substring(0, $('#file').val().lastIndexOf("."));
            fileSuffix = $('#file').val().substring($('#file').val().lastIndexOf(".")+1);
        }
    }else{
        fileName = $('#fileName').val();
        fileSuffix = 'html';
        if(editor.count('html') == 0||editor.isEmpty()) {
            Dialog.alert("提示信息","请输入文档内容")  ;
            return false;
        }
    }

    //不更新文件
    if(fileName==""&&fileType == 0){
        fileName = $('#fileOldName').val().substring($('#fileOldName').val().lastIndexOf("\\")+1,$('#fileOldName').val().lastIndexOf("."));
        fileSuffix = $('#fileOldName').val().substring($('#fileOldName').val().lastIndexOf(".")+1);
    }
        //获取同一桶下相同文件的数量
        jQuery.ajax({
            type:"POST",
            cache:false,
            dataType : 'json',
            data:{
                id : $('#id').val(),
                folderId : $('#folderId').val(),
                fileName: fileName,
                fileSuffix:fileSuffix
            },
            url:springUrl + "/doc/wffile_getFileCount",
            success:function (data) {
                if(data==0){
                    $("#fileContent").val(editor.html());
                    $("#fileForm").attr("action",springUrl+"/doc/wffile_updateSave");
                    $("#fileForm").submit();
                }else{
                    Dialog.alert("失败","该文件夹下已存在该文件");
                }
            },
            error: function (data) {
                Dialog.alert("失败","修改文件失败 ！");
            }
        });
}
/**
 * 创建新文件页面-取消按钮
 */
function returnFileIndex(){
    window.location.href = springUrl+"/doc/wffile_index";
}

/**
 * 加载文件夹树
 */
function loadFolderTree(){
    Dialog.openRemote("文件夹树",springUrl+"/doc/wffolder_treeIndex","305","400",[
        {
            'label': '取消',
            'class': 'btn-success',
            'callback': function() {

            }
        },
        {
            'label': '确定',
            'class': 'btn-success',
            'callback': function() {
                var folder=getSelectedNodes();
                if(folder[0].id=="null"){
                    $("#folderId").val("");
                    $("#folderName").val("");
                    $("#selectFolder").text("请选择");
                    return false;
                }else{
                    $("#folderId").val(folder[0].id);
                    $("#folderName").val(folder[0].name);
                    $("#selectFolder").text(folder[0].name);
                }
            }
        }]
    );
}

/**
 * 下载响应
 */
function downLoadFile(){
    $("#downForm").attr("method","post");
    $("#downForm").attr("action",springUrl+"/doc/wffile_download");
    $("#downForm").submit();
}
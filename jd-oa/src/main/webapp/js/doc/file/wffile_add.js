/**
 * 文档-文件管理添加文件JS
 * User: yaohaibin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
/**
 * 初始化操作
 */
var editor;
$(function() {
    $("#fileName").attr("disabled",true);
    //绑定文件夹树加载按钮事件
    $('#selectFloder').click(function() {
        loadFolderTree();
    });
    //保存文件
    $('#saveFile').click(function() {
        if(!validate2($('#fileForm'))){
            return false;
        }
        saveFile();
    });
    //返回
    $('#returnFileIndex').click(function() {
        returnFileIndex();
    });
    //授权
    $('#permissions').click(function() {
    	authorization();
    });

    //文件类型-文件上传
    $('#uploadRadio').click(function() {
        $("#uploadDiv").show();
        $("#fileNameDiv").hide();
        $("#editorDiv").hide();
        $("#fileName").attr("disabled",true);
        $("#file").attr("disabled",false);

    });
    //文件类型-富文本编辑器
    $('#editorRadio').click(function() {
        $("#fileNameDiv").show();
        $("#editorDiv").show();
        $("#file").attr("disabled",true);
        $("#fileName").attr("disabled",false);
        $("#uploadDiv").hide();
    });

    KindEditor.ready(function(K) {
        editor = K.create('textarea[name="fileContent"]', {
            resizeType : 0  ,
            items :[
                'source', '|', 'undo', 'redo', '|', 'preview', 'print','code', 'cut', 'copy', 'paste',
                'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|',
                'table', 'hr', 'emoticons', 'pagebreak',
                'anchor', 'link', 'unlink', '|', 'about'
            ]
        });
    });
    bindValidate($('#fileForm'));

});
//创建新文件页面-保存按钮。
function saveFile(){
    var fileName = "";
    var fileSuffix = "";
    var fileType = $('input:radio[name="fileType"]:checked').val();
    if(fileType == 0){
        if ($('#file').val() != "") {
            if ($('#file').val().lastIndexOf("\\") != -1) {
                fileName = $('#file').val().substring($('#file').val().lastIndexOf("\\") + 1, $('#file').val().lastIndexOf("."));
                fileSuffix = $('#file').val().substring($('#file').val().lastIndexOf(".")+1);
            } else {
                fileName = $('#file').val().substring(0, $('#file').val().lastIndexOf("."));
                fileSuffix = $('#file').val().substring($('#file').val().lastIndexOf(".")+1);
            }
         }
    }else{
        fileName = $('#fileName').val();
        fileSuffix = 'html';
        if(editor.count('html') == 0||editor.isEmpty()) {
            Dialog.alert("提示信息","请输入文档内容")  ;
            return false;
        }
    }
    //获取同一桶下相同文件的数量
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType : 'json',
        data:{
            folderId : $('#folderId').val(),
            fileName: fileName,
            fileSuffix:fileSuffix
        },
        url:springUrl + "/doc/wffile_getFileCount",
        success:function (data) {
            if(data==0){
                $("#fileContent").val(editor.html());
                $("#fileForm").attr("action",springUrl+"/doc/wffile_addSave");
                document.getElementById("fileForm").submit();
            }else{
                Dialog.alert("失败","该文件夹下已存在该文件");
            }
        },
        error: function (data) {
            Dialog.alert("失败","获取相同文件数量失败！");
        }
    });
}
/**
 * 创建新文件页面-取消按钮
 */
function returnFileIndex(){
    window.location.href = springUrl+"/doc/wffile_index";
}
/**
 * 授权用户
 */
function authorization(){
Dialog.openRemote('授权用户',springUrl+'/auth/authExpression_select?selectedIds=""',1000,500,
[
  {
     'label': '取消',
     'class': 'btn-success',
     'callback': function() {
    	
     	}
     },
 {	
      'label': '确定',
      'class': 'btn-success',
      'callback': function() {
    	  //返回的选择对象
     	 var tableObj=selectOk();
     	 if(tableObj.length<=0){
              $("#authExpressionId").val("");
              $("#permissionExpressionNames").val("") ;
     	  }else{
     		 var arrObj = [];
     		 for(var i=0; i<tableObj.length; i++){
     	        var obj = {};
     	        obj.authExpressionId = tableObj[i];
     	        arrObj.push(obj);
     	    }
     		getExpressName(tableObj);       		
     		$("#authExpressionId").val(JSON.stringify(arrObj));
     	  }
      }
 }]);                 
}
/**
 * 获取表达式的名称
 */
function getExpressName(tableObj){
	var expressName="";
	  jQuery.ajax({
	        type:"POST",
	        cache:false,
	        dataType:'json',
	        url:springUrl+"/doc/wffile_getSysBussinessName?ids="+tableObj,
	        data:{
	        },
	        success:function (data) {
	     		$("#permissionExpressionNames").val(data.bussinessName);  
	        },
	        error : function(data){
	        	Dialog.alert("提示信息","获取字段名称失败!");
	        }
	    });
}
/**
 * 加载文件夹树
 */
function loadFolderTree(){
	Dialog.openRemote("文件夹树",springUrl+"/doc/wffolder_treeIndex","305","400",
			[
        	 {
        		 'label': '取消',
        		 'class': 'btn-success',
        		 'callback': function() {
           	
            	}
        	},
        	{	
               'label': '确定',
               'class': 'btn-success',
               'callback': function() {
              	 var folder=getSelectedNodes();
              	 if(folder[0].id=="null"){
              		$("#folderId").val("");
            		$("#folderName").val("");
            		$("#selectFolder").text("请选择");
            		return false;
              	 }else{
              		$("#folderId").val(folder[0].id);
            		$("#folderName").val(folder[0].name);
                    $('input[name="folderId"]').trigger("validate");
            		$("#selectFolder").text(folder[0].name);
              	 }
               }
            }]);
}
function  validatorFile(){
    $('input[name="file"]').trigger("validate");
}
/*Ztree*/

var orgTree;
//根节点 folders 传入参数
var zNodes ={
	    "id":"single",
	    "name":"组织机构",
	    "isParent":true,
	    "iconSkin":"diy1"
	};
var setting = {
	async : {
		enable : true,
		url :springUrl+"/auth/expression_orgTreeLoad",
		autoParam : ["id"]
	},
	check: {
		enable: false
	},
	 callback : {
      beforeClick : beforeClick
  },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false,
		expandSpeed: "fast"
	}
};
/**
* 初始化操作
*/
$(function() {
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
});

function beforeClick(treeId, treeNode) {
    if (treeNode.id != "single" && treeNode.id != "multiple") {
        $("#orgid").val(treeNode.props.id);
        $("#realName").val("");
        if(treeNode.props.id!=""){
        	var oSettings = table.fnSettings();
            oSettings.sAjaxSource = springUrl + "/system/userRolePage";
        	Table.render(table);
        }//如果选择某节点，则触发分页刷新查询
        return true;
    } else {
        alert("请选择下级机构！");
    }
}

/**
 * 获取当前节点下的所有叶子节点
 * @param treeNode
 * @param result
 * @returns
 */
function getAllChildrenNodes(treeNode,result){  
    if (treeNode.isParent) {  
      var childrenNodes = treeNode.children;  
      if (childrenNodes) {  
          for (var i = 0; i < childrenNodes.length; i++) {  
              result += ',' + childrenNodes[i].id;  
              result = getAllChildrenNodes(childrenNodes[i], result);  
          }  
      }  
  }  
  return result;
}  

var table;
$(document).ready(function() {
    //top.autoHeight($(document).height());
    //加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
    var options=Table.options;
    options={
        pageUrl:"/",
        useCheckbox:false,
        scrollY:600,
        defaultSort:[],
        sendData:function ( sSource, aoData, fnCallback ) {
            var selectgroups=$("select#selectgroups").val();//获取所选用户组ID
            var selectroleType=$("select#selectroleType").val();//获取所选角色ID
            var roleId;
            if(selectgroups!='') {roleId=selectgroups;}
            if(selectroleType!='') {roleId=selectroleType; } //用户组或者角色ID 统一为roleId处理 同为数据库表role.id
            var realName=$("input#realName").val(); //获取查询用户姓名
            var orgId = $("#orgid").val();//获取当前组织编码
            aoData.push( { "name": "realName", "value":realName }, { "name": "roleId", "value":roleId },{ "name": "orgId", "value":orgId } );
            jQuery.ajax( {
                type: "POST",
                url:sSource,
                dataType: "json",
                data: aoData,
                success: function(resp) {
                    fnCallback(resp);
                }
            });
        },
        columns: [
            { "mData": "realName","sTitle":"用户姓名","sWidth":100 },
            { "mData": "organizationFullName","sTitle":"部门","bSortable":true ,"sWidth":200},
            {"mData": "id","sTitle":"操作","bSortable":false,"sWidth":200,"mRender":function(data, type, full){  //
                var id="'"+data+"'";
                var viewHtml = '<a href="#" class="btn" title="查看详情" onclick="Dialog.openRemote(\'查看详情\',\'sysUser_view?userId='+data+'\');"><img src="'+springUrl+'/static/common/img/edit.png" alt="查看详情"/></a>&nbsp;&nbsp;';
                var editHtml='<a href="#" class="btn"  title="授权" onclick="javascript:update('+id+')" ><img src="'+springUrl+'/static/common/img/enjoy.png" alt="授权"/></a>&nbsp;&nbsp;';
                var viewParentHtml='<a href="#" class="btn"  title="查看上级" onclick="Dialog.openRemote(\'查看上级\',\'sysUserRole_showParent?userId='+data+'\');" ><img src="'+springUrl+'/static/common/img/set.png" alt="查看上级"/></a>&nbsp;&nbsp;';
                return viewHtml+editHtml+viewParentHtml;
            }}
        ],
        btns:[]
    };
    table=Table.dataTable("sysUserRoleTable",options);
    $('#search').click(function() { //查询触发事件
    	var oSettings = table.fnSettings();
        oSettings.sAjaxSource = springUrl + "/system/userRolePage";
        Table.render(table);
        $('#selectgroups').removeAttr("disabled");
        $('#selectroleType').removeAttr("disabled");
    });
//    $('#realName').keyup(function() {
//    	var oSettings = table.fnSettings();
//        oSettings.sAjaxSource = springUrl + "/system/userRolePage";
//
//    	Table.render(table);
//    	$('#selectgroups').removeAttr("disabled");
//        $('#selectroleType').removeAttr("disabled");
//    });
    
    $('#selectroleType').change(function() { //点击用户角色清空用户组，不支持与用户组级联查询 ，可与用户姓名级联查询
        $('#selectgroups').val("");
        $('#selectgroups').attr("disabled","disabled");
    } );

    $('#selectgroups').change(function() { //点击用户组清空用户角色，不支持与用户角色级联查询 ，可与用户姓名级联查询
        $('#selectroleType').val("");
        $('#selectroleType').attr("disabled","disabled");
    } );
    //页面初始化加载角色类型及用户组类型
    $.ajax({
        type: "POST",
        cache:"false",
        url: "sysRole_findSysRoleList",
        dataType: "json",
        success: function(json){
            var ary = new Array();
            var role = '<option value="">不限</option>';
            var groups='<option value="">不限</option>';
            if(json != null){
                for(var i=0;i<json.length;i++){
                    if(json[i].roleType=='0') { //roleType=0，则为角色
                        role +='<option value="'+json[i].id+'">'+json[i].roleName+'</option>';
                    }
                    if(json[i].roleType=='1') {//roleType=1，则为用户组
                        groups +='<option value="'+json[i].id+'">'+json[i].roleName+'</option>';
                    }
                }
            }
            $("#selectroleType").html(role);
            $("#selectgroups").html(groups);
        }
    });
});
function hello(){
    var datas=Table.getSelectedRows(table);
    alert(datas[0].name);
}
/**
 * 单个用户角色分配
 * @param id
 */
function update(id){
    Dialog.openRemote('单个角色授权',springUrl+"/system/sysUserRole_update?Id="+id,800,430);
}

/**
 * 批量用户角色分配
 */
function updateRoles(){
    document.location.href = springUrl+"/system/sysUserRoles_update";
}
/**
 * 刷新用户信息列表
 */
function refreshUserTable(){
	Table.render(table);
}

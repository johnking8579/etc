/**
 * 资源-修改页面JS
 * User: xulin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */
/**
 * 修改资源-确定按钮响应函数
 */
function updateResource(){
	if(validate2($('#inputForm'))){
		var params = $("#inputForm").serialize();
        jQuery.ajax({
            type:"POST",
            cache:false,
            data:params,
			dataType: "json",
            url:springUrl + "/system/sysResource_updateSave",
            success:function(data) {
            	Dialog.alert("确定","资源更新成功!");
            	refreshNode($("#parentId").val());
            	var id = $("#id").val();
            	$("#box-right").load(springUrl+"/system/sysResource_view?id="+id);
            },
			error : function(data) {
				 Dialog.alert("失败","程序异常结束!");
			}
        });
	}
};

/**
 * 修改资源-取消按钮响应函数
 */
function cancelResource(){
    $("#box-right").hide();
    refreshNode($("#parentId").val());
};

function selectChange(obj){
	var index = obj.selectedIndex;
	var value = obj.options[index].value;
	$("#displayStatus").attr("value",value);
}

/**
 * 初始化操作
 */
$(function() {
	
    //绑定保存按钮事件
    $('#updateResource').click(function() {
    	updateResource();
    });
    //绑定取消按钮事件
    $('#cancelResource').click(function() {
        cancelResource();
    });

    $('#displayStatusSelect').attr('value', displayStatus);
    
    //验证
	bindValidate($('#inputForm'));
});

var orgTree;
//根节点 folders 传入参数
var zNodes = {
	    "id" : "single",
	    "name" : "组织机构",
	    "isParent" : true,
	    "iconSkin" : "diy1"
	};
var check = {
	enable : true	
};
if(!isMulti){
	check.chkStyle = "radio";
	check.radioType = "all";
}
var setting = {
	async : {
		enable : true,
		url : baseUrl + "/system/sysAddress_getTreeNodeDataJson",
		autoParam : ["id"]
	},
	check: check,
	 callback : {
        //onClick : treeNodeClick
    },
	data : {
		simpleData : {
			enable : true,
			idKey : 'id',
			idPKey : 'pId',
			rootPid : null
		}
	},
	view: {
		dblClickExpand: false,
		showLine : true,
		selectedMulti : false,
		expandSpeed : "fast"
	}
};
var zTreeHeight = getHeight();
jQuery("#orgTree").height(zTreeHeight - 80);
/**
 * 初始化操作
 */
$(function() {
	//加载组织机构树
	jQuery.fn.zTree.init($("#orgTree"), setting, zNodes);
	orgTree = jQuery.fn.zTree.getZTreeObj("orgTree");
	
	//隐藏dataTable 数据条目信息
	jQuery(".dataTables_info").parent().hide();
	
	document.getElementById('loading').style.display = 'none';
	document.getElementById('mainPanel').style.display = '';
});	

function getHeight(){
	  return jQuery(".modal-body").height();
}
function callBack(){
	var selectRecords = orgTree.getCheckedNodes(); 
	  if(selectRecords.length <= 0){
		  Dialog.alert("提示", "请选择用户数据记录!");
	  }else{
		  setSelectOrgUserData(selectRecords);
	  }
}
function cancel(){
	Dialog.hide();
}
function defaultCallBackFunction(selectRecords){
	 Dialog.alert("提示", "请定义【确定】按钮的回调函数!");
	 return;
}

﻿var table;
$(document).ready(function () {
    var options = Table.options;
    options = {
        pageUrl: "sysOperation_list",
        sendData: function (sSource, aoData, fnCallback) {
            var operationName = $("input#operationName").val();
            var operationCode = $("input#operationCode").val();
            aoData.push({ "name": "operationName", "value": operationName },{ "name": "operationCode", "value": operationCode });

            jQuery.ajax({
                type: "POST",
                url: "sysOperation_list",
                dataType: "json",
                data: aoData,
                success: function (resp) {
                    fnCallback(resp);
                }
            });
        },

        columns: [
            {"mData": "operationCode", "sTitle": "操作编码", "bSortable": false },
            {"mData": "operationName", "sTitle": "操作名称" , "bSortable": false},
            {"mData": "description", "sTitle": "描述" , "bSortable": false},
            {"mData": "id", "sTitle": "操作", "mRender": function (data, type, full) {
                var render = '<a href="#" class="btn" title="编辑" onclick="editOperation(\'' + data + '\')" ><img src="' + springUrl + '/static/common/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;'
                    + '<a href="#" class="btn " title="删除" onclick="deleteOperation(\'' + data + '\')"><img src="' + springUrl + '/static/common/img/del.png" alt="删除"/></a>';
                return render;
            }}
        ],
        btns: [
            {
                "sExtends": "text",
                "sButtonText": "新增",
                "sButtonClass": "btn btn-success",
                "sToolTip": "",
                "fnClick": function (nButton, oConfig, oFlash) {
                    var buttons = [
                        {
                            "label": "取消",
                            "class": "btn-success",
                            "callback": function () {
                            }
                        },
                        {
                            "label": "保存",
                            "class": "btn-success",
                            "callback": function () {
                                if (!validate2($("#addOperation"))) {
                                    return false;
                                } else {
                                    return save();
                                }

                            }
                        }
                    ];
                    Dialog.openRemote('新增', 'sysOperation_add', 600, 400, buttons);
                }
            }
        ]
    };
    table = Table.dataTable("sysoperation", options);
});
function editOperation(id) {
    var buttons = [
        {
            "label": "取消",
            "class": "btn-success",
            "callback": function () {
            }
        },
        {
            "label": "保存",
            "class": "btn-success",
            "callback": function () {
                if (!validate2($("#updateOperation"))) {
                    return false;
                } else {
                    return modify();
                }
            }
        }
    ];
    Dialog.openRemote('编辑操作', 'sysOperation_update?id=' + id, 550, 400, buttons);
}

function save() {
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: "sysOperation_save",
        data: {
            operationCode: $('#operationCode').val(),
            operationName: $('#operationName').val(),
            description: $('#description').val()
        },
        success: function (msg) {
            Table.render(table);
            Dialog.hide();
        },
        error: function (msg) {

        }
    });
}
function modify() {
    jQuery.ajax({
        type: "POST",
        cache: false,
        url: "sysOperation_save",
        data: {
            id: $('#id').val(),
            operationCode: $('#operationCode').val(),
            operationName: $('#operationName').val(),
            description: $('#description').val()
        },
        success: function (msg) {
            Table.render(table);
            Dialog.hide();
        },
        error: function (msg) {

        }
    });
}

function deleteOperation(id) {
    Dialog.confirm("提示", "确定要删除该条记录吗?", "是", "否", function (result) {
        if (result) {
            jQuery.ajax({
                type: "POST",
                cache: false,
                url: "sysOperation_delete.json",
                data: {
                    id: id
                },
                success: function (msg) {
                    Table.render(table);
                },
                error: function (msg) {

                }
            });
        }
    });
}
function hello() {
    var datas = Table.getSelectedRows(table);
    alert(datas[0].name);
}

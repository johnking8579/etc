/**
 * Created with IntelliJ IDEA.
 * User: yujiahe
 * Date: 13-9-2
 * Time: 下午3:19
 * To change this template use File | Settings | File Templates.
 */
var flag='0';  //0:系统角色 1：用户组

function cancel(){
    document.location.href = springUrl+"/system/sysUserRole_index";
}
$(document).ready(function() {
    var demo1 = $('[name="duallistbox_demo1[]"]').bootstrapDualListbox();
    $("#updateGroups").hide();
});

function updateUserRole(){
    $("#updateUserRole").show();
    $("#updateGroups").hide();
    flag='0';
}
function updateGroups(){
    $("#updateUserRole").hide();
    $("#updateGroups").show();
    flag='1';
}
function saveUserRoles(){
    var userIds=$('[name="duallistbox_demo1[]"]').val();
    var roleId;
    if(flag='0'){
        roleId=$("#sysRoleList").val();
    }else{
        roleId=$("#sysGroupList").val()
    }

    $.ajax({
        type: "GET",
        cache:"false",
        url: "sysUserRoles_updateSave",
        dataType: "json",
        data:"roleId="+roleId+"&userIds="+userIds,
        success: function(json){
            if(json != null){
                document.location.href = springUrl+"sysUserRole_index";

            }
        }});
}

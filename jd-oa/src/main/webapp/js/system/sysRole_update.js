$(document).ready(function(){
	var editRoleTypeValue=$("#editRoleTypeValue").val();
	$("#editRoleType").val(editRoleTypeValue);
    bindValidate($("#editRole"));// 绑定验证
});


function save(){
	jQuery.ajax({
        type:"POST",
        cache:false,
        data:{
			id:$('#editRoleId').val(),
            roleType:$('#editRoleType').val(),
            roleName:$('#editRoleName').val()
        },
        url:springUrl+"/system/sysRole_updateSave.json",
        success:function (data) {
        	refreshUserTable();
        }
    });
}
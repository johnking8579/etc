var setting = {
	async: {
		enable: true,
		url: getUrl
	},
	check: {
		enable: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	view: {
		expandSpeed: ""
	},
	callback: {
		onClick: zTreeOnClick
	}
};

var zNodes = treeNode;

function getUrl(treeId, treeNode) {
	var param = "nodeId=" + treeNode.id + '&level=' +treeNode.level;
	return baseUrl + "/system/sysPrivilege_getTreeNodesJson?" + param;
}
function zTreeOnClick(treeId, treeNode) {
	//var privilegeTable = Table.dataTable("sysPrivilegeContext");
	Table.render(privilegeTable); 
}

function saveAjax(data){
	jQuery.ajax({
        type : "POST",
        cache : false,
        url : baseUrl + "/system/sysPrivilege_save.json",
        data : data,
        success:function (msg) {
        	
        },
        error : function(msg){
        	
        }
    });
}
function deleteAjax(data){
	
}
function cancel(){
	Dialog.hide();
}
var privilegeTable;
var treeObj;
$(document).ready(function(){
	treeObj = $.fn.zTree.init($("#privilegeTree"), setting, zNodes);
	var options = Table.options;
	options = {
			pageUrl : "getOperationDataJson",
			sendData : function ( sSource, aoData, fnCallback ) {
				var sNodes = treeObj.getSelectedNodes();
				if (sNodes.length > 0) {
					var node = sNodes[0];
					if(!node.isParent){
						aoData.push( { "name": "resourceId", "value" : node.props.id });
						jQuery.getJSON(sSource, aoData, function (json) { 
							fnCallback(json);
						});
					}
				}
			},   
			columns: [
	            {"mData" : "operationCode","sTitle":"操作编码","bSortable" : true },
	            {"mData" : "operationName","sTitle":"操作名称" },
	            {"mData" : "code","sTitle":"权限编码" },
	            {"mData" : "name","sTitle":"权限名称" },
	            {"mData" : "url","sTitle":"URL","bVisible": false },
	            {"mData" : "resourceId","sTitle":"资源ID", "bVisible": false},
				{"mData" : "id","sTitle":"操作","mRender":function(data, type, full){
					 var render = '<a href="#" class="btn" title="编辑" onclick="dataTablesReader(\'' + data + '\')"><img src="'+springUrl+'/static/common/img/edit.png" alt="修改"/></a>&nbsp;&nbsp;'
						+'<a href="#" class="btn" title="删除" onclick="deleteOperation(\'' + data + '\')"><img src="'+springUrl+'/static/common/img/del.png" alt="修改"/></a>';
					 return render;
				}},
	        ],
	        btns:[{
			    "sExtends":    "text",
			    "sButtonText": "新增",
				"sToolTip": "",
				"fnClick": function ( nButton, oConfig, oFlash ) {
					var sNodes = treeObj.getSelectedNodes();
					if (sNodes.length > 0) {
						var node = sNodes[0];
						if(!node.isParent){
							var buttons = [{
						     				"label": "取消",
						     				"class": "btn-success",
						     				"callback": cancel
							     		},
							     		{
						     				"label": "保存",
						     				"class": "btn-success",
						     				"callback": function(){
						     					if (!validate2($("#addPrivilege"))) {
						     						return false;
						     					} else {
						     						save_insert();
						     					}
						     				}
							     		}];
							Dialog.openRemote('选择功能操作',encodeURI('sysPrivilege_add?resourceId=' + node.props.id + "&resourceCode=" + node.props.code + "&resourceName=" + node.name),600,500,buttons);
						} else {
							alert('当前选中的【' + node.name + '】非叶子节点！');
						}
					} else {
						alert('请选择一个节点');
					}
			    }
	        }]
	};
	privilegeTable = Table.dataTable("sysPrivilegeContext",options);
});
function dataTablesReader(id){
	var sNodes = treeObj.getSelectedNodes();
	var resourceCode,resourceName;
	if (sNodes.length > 0) {
		var node = sNodes[0];
		resourceCode = node.props.code;
		resourceName = node.name;
	}
	var buttons = [{
			"label": "取消",
			"class": "btn-success",
			"callback": cancel
		},
		{
			"label": "保存",
			"class": "btn-success",
			"callback": function(){
				if (!validate2($("#updatePrivilege"))) {
					return false;
				} else {
					save_update();
				}
			}
		}];
	Dialog.openRemote('编辑','sysPrivilege_edit?id=' + id + "&resourceCode=" + resourceCode + "&resourceName=" + resourceName,600,520,buttons);
}
function deleteOperation(id){
	Dialog.confirm("提示","确定要删除该条记录吗?","是","否",function(result){
		if(result){
			jQuery.ajax({
		        type : "POST",
		        cache : false,
		        url : baseUrl + "/system/sysPrivilege_remove",
		        data : {
		            id : id
		        },
		        success:function (msg) {
		        	//var privilegeTable = $('#sysPrivilegeContext').dataTable();
		        	Table.render(privilegeTable); 
		        },
		        error : function(msg){
		        	
		        }
		    });
		}
	});
}

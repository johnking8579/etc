/**
 * @Description: 流程实例当前审批人的修改页面JS
 * @author:yujiahe
 *
 */

/**
 * 保存修改  流程实例当前审批人的修改
 * @param type   M ：主任务  S：子任务
 * @param taskId
 * @param origAssignee
 * @param $this
 */
function editSave(type, taskId, origAssignee, $this) {
    var newAssignee = $("#" + taskId).val();//新的任务分配用户，必须为小写
    var data = {
        "taskId": taskId,
        "origAssignee": origAssignee,
        "newAssignee": newAssignee
    };
    $.ajax({
        url: springUrl + "/system/processMaintain_editSave",
        async: true,
        type: "get",
        data: data,
        dataType: 'json',
        cache: false,
        complete: function () {
        },
        success: function (result) {
            if (result.operator == "success") {
                Dialog.alert("请求结果", result.message);
                if (type == 'M') {//主任务则累加办理人
                   var oldAssignee  = origAssignee.indexOf(",")>0?origAssignee.split(",")[0]:origAssignee;//保留第一个审批人，用新的替换后面部分
                    $this.parent().prev().prev().text(oldAssignee+"," + newAssignee);
                } else {//子任务则替换办理人
                    $this.parent().prev().prev().text(newAssignee);
                }
                $("#" + taskId).val("");//清空
                Table.render(PMTable);
            }
        },
        error: function (data) {
            Dialog.alert("请求异常", result.message);
        }
    });
}
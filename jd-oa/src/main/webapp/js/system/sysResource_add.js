/**
 * 资源-新增页面JS
 * User: xulin
 * Date: 13-8-30
 * Time: 上午10:30
 * To change this template use File | Settings | File Templates.
 */

/**
 * 添加资源-确定按钮响应函数
 */
function saveResource(){
	if(validate2($('#inputForm'))){
		var params = $("#inputForm").serialize();
        jQuery.ajax({
            type:"POST",
            cache:false,
            data:params,
			dataType: "json",
            url:springUrl + "/system/sysResource_addSave",
            success:function(data) {
            	 if(!data.check){
                     Dialog.alert("失败","已存在相同的资源!");
                 } else {
                	 Dialog.alert("确定","资源创建成功!");
                	 refreshNode($("#parentId").val());
                	 $("#box-right").load(springUrl+"/system/sysResource_view?id="+data.id);
                 }
            },error : function(data) {
				 Dialog.alert("失败","程序异常结束!");
			}
        });
	}
}

/**
 * 添加资源-取消按钮响应函数
 */
function cancelResource(){
    $("#box-right").hide();
    refreshNode($("#parentId").val());
}

function selectChange(obj){
	var index = obj.selectedIndex;
	var value = obj.options[index].value;
	$("#displayStatus").attr("value",value);
}

/**
 * 初始化操作
 */
$(function() {
    //绑定保存按钮事件
    $('#saveResource').click(function() {
    	saveResource();
    });
    //绑定取消按钮事件
    $('#cancelResource').click(function() {
        cancelResource();
    });
    
    //验证
	bindValidate($('#inputForm'));
});

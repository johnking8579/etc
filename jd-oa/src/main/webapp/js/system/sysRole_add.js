$(document).ready(function(){//页面初始化设置

    bindValidate($("#addRole"));// 绑定验证

});

/**
 * Cancel按钮事件
 */
function cancel(){
}
function submit(){//新增动作
    var bol = false;
    var newRoleType=$('#newRoleType').val();
    var newRoleName=$('#newRoleName').val();
    jQuery.ajax({
        type:"POST",
        cache:false,
        dataType: 'json',
        async:false,
        data:{
            roleType:newRoleType,
            roleName:newRoleName
        },
        url:springUrl+"/system/sysRole_addSave.json",
        success:function (data) {
            bol = true;
            Table.render(table);
        }
    });

    return bol;
}

function addRoles(){ //新增保存按钮触发动作
    var roleName=$('#newRoleName').val();
    $.ajax({ //后台查询是否已有该名称角色
        type: "POST",
        cache:"false",
        url: "sysRole_checkRoleNameUnique",
        dataType: "json",
        async:false,
        data: "roleName="+roleName,
        success: function(data){
            if(data>0){//如果系统存在该名称的用户拒绝提交，前台显示
                Dialog.alert("系统提示","该角色已存在，请重置","")
            } else{ //如果不存在则提交
                 return submit();
            }
        }
    });
}




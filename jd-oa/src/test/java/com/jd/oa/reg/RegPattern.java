package com.jd.oa.reg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new RegPattern().run();
	}
	public void run(){
		String express = "num1=num2+num3+num4";
		String regEx = "=|\\+|\\-"; //表示a或F  
		
		String temp = "";   
		int index = -1;   
         
		Pattern pattern = Pattern.compile(regEx);   
        Matcher matcher = null;   
        while (pattern.matcher(express).find()) {   
             matcher = pattern.matcher(express);   
             if (matcher.find()) {   
                  temp = matcher.group();   
                  index = express.indexOf(temp);   
                  System.out.println(express.substring(0, index));
                  express = express.substring(index + temp.length());
             }   
        }   
        System.out.println(express);
	}
	//num1   ＝   num2   ＋   num3   ＋   num4
}

package com.jd.oa.form.service.impl;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.DataType;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.RelationalOperator;
import com.jd.oa.common.dao.sqladpter.SqlAdapter.SqlType;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.form.service.FormBusinessTableService;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormBusinessTableServiceImplTest extends SpringContextTestCase {

	@Autowired
	private FormBusinessTableService formBusinessTableService;
	
	@Test
	public void testGetBusinessData() {
		SqlAdapter adpater = new MySqlAdapter();
		/*adpater.setSqlType(SqlType.DML_SELECT);
		adpater.addTable("T_JDOA_7069081501503224331");
		adpater.addAndCondition("ID", RelationalOperator.EQ, "1234567890-", DataType.STRING);*/
		adpater.setSqlType(SqlType.DML_SELECT);
		adpater.setSql("SELECT `c1`, `r1`, `l1` FROM `T_JDOA_4610336534205535793` AS TEMP_TABLE_ALIAS_0 WHERE `YN` = '0' AND `ID` = 'ea3069db25cc4ff3b0b0a10f5c932166'");
		Object object = formBusinessTableService.sql(adpater);
		System.out.println(object);
	}

}

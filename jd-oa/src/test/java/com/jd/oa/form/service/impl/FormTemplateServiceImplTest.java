package com.jd.oa.form.service.impl;

import java.io.IOException;
import java.io.InputStream;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.exception.BusinessException;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class FormTemplateServiceImplTest extends SpringContextTestCase{

	    
	    @Autowired
		private JssService jssService;
		
	    
	    public void testGetTemplateValue() throws IOException{
			InputStream in = null;
			try{
//				8bb65e1cca314fecbbcce44bdedd692e.htm   8bb65e1cca314fecbbcce44bdedd692e.htm
				in = jssService.downloadFile(SystemConstant.BUCKET, "1020afeca8e045448b5ecd3519f7bebc.htm");
//				String result = IOUtils.toString(in);
			} catch(Exception e){
				e.printStackTrace();
				throw new BusinessException("获取流程表单模板失败！",e);
			} finally {
	        	try{
	        		in.close();
	        	} catch(IOException e) {
	        	} catch(NullPointerException e){
	        		e.printStackTrace();
	        		throw new BusinessException("获取流程表单模板失败！",e);
	        	}
	        }
			
			
		}
	    @Test
	    public void test()throws IOException{
	    	StringBuffer sb = new StringBuffer();
	    	for (int i = 0; i < 50; i++) {
	    		long begin = System.currentTimeMillis();
	    		testGetTemplateValue();
	    		long end = System.currentTimeMillis();
	    		sb.append("获取流程表单模板时间"+i+"：       "+(end-begin)+" 毫秒******************************\n");
			}
	    	System.out.println("\n\n\n\n");
	    	System.out.println(sb.toString());
	    }
}

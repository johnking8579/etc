package com.jd.oa.service.ws.impl;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;

import com.jd.oa.custom.ProcessService;
import com.jd.oa.di.service.HelloService;

public class ProcessServiceTest {
//	@Test
	public void testFindProcessTaskHistoryByBusinessId() throws Exception {

		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		Client client = dcf
				.createClient("http://oa.jd.com/services/ws/processService?wsdl");
		Object[] res = client.invoke("findProcessTaskHistoryByBusinessId",
				"5dd8eb2461c24d4faed8cbdc7bd846ba");
		System.out.println(res[0] + "++++++++++++++++");

	}

	// @Test
	public void testhelloWorld() throws Exception {

		ClientProxyFactoryBean factory = new ClientProxyFactoryBean();

		factory.setServiceClass(HelloService.class);

		factory.setAddress("http://oa.jd.com/services/ws/helloService?wsdl");

		// factory.getServiceFactory().setDataBinding(new AegisDatabinding());

		HelloService client = (HelloService) factory.create();

		System.out.println(client.sayHello("2", "2"));

		System.exit(0);

	}
	@Test
	public void updatePurchaseDetail() throws Exception {

		JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
		String parm="<purchaseDetails>"+
						"<purchaseDetail sn=\"01\">"+
						 	"<id>ererere</id>"+
						    "<comments>yujiahe</comments>"+
						    "<productStatus>4</productStatus>"+
						    "<inputNumCount>5</inputNumCount>"+
						"</purchaseDetail>"+
					"</purchaseDetails>";
		Client client = dcf
				.createClient("http://oa.jd.com/services/ws/processService?wsdl");
		Object[] res = client.invoke("updatePurchaseDetail",
				parm);
		System.out.println(res[0] + "++++++++++++++++");

	}

}

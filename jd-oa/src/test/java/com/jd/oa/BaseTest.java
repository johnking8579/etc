package com.jd.oa;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class BaseTest extends AbstractTransactionalJUnit4SpringContextTests{

}

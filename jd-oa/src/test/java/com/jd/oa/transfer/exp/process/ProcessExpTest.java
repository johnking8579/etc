package com.jd.oa.transfer.exp.process;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.process.xpackage.ExtendedAttributeDocument.ExtendedAttribute;
import org.apache.xmlbeans.process.xpackage.ExtendedAttributesDocument;
import org.apache.xmlbeans.process.xpackage.FormBusinessFieldsDocument;
import org.apache.xmlbeans.process.xpackage.FormBusinessTableDocument;
import org.apache.xmlbeans.process.xpackage.FormDocument;
import org.apache.xmlbeans.process.xpackage.FormItemsDocument;
import org.apache.xmlbeans.process.xpackage.FormTemplateDocument;
import org.apache.xmlbeans.process.xpackage.NodeDocument.Node;
import org.apache.xmlbeans.process.xpackage.NodesDocument;
import org.apache.xmlbeans.process.xpackage.PackageDocument;
import org.apache.xmlbeans.process.xpackage.PackageHeaderDocument;
import org.apache.xmlbeans.process.xpackage.PackageHeaderDocument.PackageHeader;
import org.apache.xmlbeans.process.xpackage.PrintTemplateDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDocument;
import org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigsDocument;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.form.model.Form;
import com.jd.oa.form.model.FormBusinessField;
import com.jd.oa.form.model.FormBusinessTable;
import com.jd.oa.form.model.FormItem;
import com.jd.oa.form.model.FormTemplate;
import com.jd.oa.form.service.FormBusinessFieldService;
import com.jd.oa.form.service.FormBusinessTableService;
import com.jd.oa.form.service.FormItemService;
import com.jd.oa.form.service.FormService;
import com.jd.oa.form.service.FormTemplateService;
import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.model.ProcessDefinitionConfig;
import com.jd.oa.process.model.ProcessNode;
import com.jd.oa.process.service.ProcessDefService;
import com.jd.oa.process.service.ProcessDefinitionConfigService;
import com.jd.oa.process.service.ProcessNodeService;
import com.jd.oa.transfer.exp.ExportContext;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessExpTest extends SpringContextTestCase{
	@Autowired
	private FormTemplateService formTemplateService;
	
	@Autowired
	private ProcessDefService processDefService;
	
	@Autowired
	private ProcessNodeService processNodeService;
	
	@Autowired
	private FormService formService;
	
	@Autowired
	private FormItemService formItemService;
	
	@Autowired
	private FormBusinessTableService formBusinessTableService;
	
	@Autowired
	private FormBusinessFieldService formBusinessFieldService;
	
	@Autowired
	private ProcessDefinitionConfigService processDefinitionConfigService;
	
	@Test
	public void test(){
		ProcessExpTest dt = new ProcessExpTest();
		// Create an instance of a Datetime type based on the received XML's
		// schema
		PackageDocument doc = dt.parseXml("D:/dev/jdworkspace/OA/workflow_init.xml");
		// Prints the element values from the XML
		/**
		 * test template name = EBS运维流程-主数据流程
		 */
		dt.createDocument(doc, "D:/dev/jdworkspace/OA/OAworkflow_test.xml", "392dff8ccf004b2a80c251dec4e01b12");
	}
	public void createDocument(PackageDocument doc, String file, String processDefId) {
		// Retrieve the <package> element and add a new <package> element.
		if(processDefService == null){
			processDefService = SpringContextUtils.getBean(ProcessDefService.class);
		}
		if(processNodeService == null){
			processNodeService = SpringContextUtils.getBean(ProcessNodeService.class);
		}
		if(formTemplateService == null){
			formTemplateService = SpringContextUtils.getBean(FormTemplateService.class);
		}
		if(formService == null){
			formService = SpringContextUtils.getBean(FormService.class);
		}
		if(formItemService == null){
			formItemService = SpringContextUtils.getBean(FormItemService.class);
		}
		if(formBusinessTableService == null){
			formBusinessTableService = SpringContextUtils.getBean(FormBusinessTableService.class);
		}
		if(formBusinessFieldService == null){
			formBusinessFieldService = SpringContextUtils.getBean(FormBusinessFieldService.class);
		}
		if(processDefinitionConfigService == null){
			processDefinitionConfigService = SpringContextUtils.getBean(ProcessDefinitionConfigService.class);
		}
		// process start
		PackageDocument.Package packageElement = doc.getPackage();
		ProcessDefinition processDefinition = processDefService.get(processDefId);
		packageElement.setId(processDefinition.getId());
		packageElement.setName(processDefinition.getProcessDefinitionName());
		// insert packageHeader attribute
		PackageHeader packageHeaderAttribute = packageElement.addNewPackageHeader();
		packageHeaderAttribute.setBPDLModel("eForm(View) Model");
		packageHeaderAttribute.setBPDLVersion(ExportContext.getVersion());
		packageHeaderAttribute.setBPDLVendor(ExportContext.getVendor());
		packageHeaderAttribute.setCreateDate(new Date().toString());
		packageHeaderAttribute.setUpdataDate(new Date().toString());
		packageHeaderAttribute.setDescription("");
		packageHeaderAttribute.setProcessTypeId(processDefinition.getProcessTypeId());
		packageHeaderAttribute.setPafProcessDefinitionId(processDefinition.getPafProcessDefinitionId());
		packageHeaderAttribute.setProcessDefinitionName(processDefinition.getProcessDefinitionName());
		packageHeaderAttribute.setOrganizationType(processDefinition.getOrganizationType());
		packageHeaderAttribute.setFilePath(processDefinition.getFilePath());
		packageHeaderAttribute.setIsIssue(processDefinition.getIsIssue());
		packageHeaderAttribute.setFormId(processDefinition.getFormId());
		packageHeaderAttribute.setAlertMode(processDefinition.getAlertMode());
		packageHeaderAttribute.setIsComment(processDefinition.getIsComment());
		packageHeaderAttribute.setIsUploadAttach(processDefinition.getIsUploadAttach());
		packageHeaderAttribute.setIsRelateDoc(processDefinition.getIsRelateDoc());
		packageHeaderAttribute.setIsRelateProcessInstance(processDefinition.getIsRelateProcessInstance());
		packageHeaderAttribute.setIsTemplate(processDefinition.getIsTemplate());
		packageHeaderAttribute.setIsManagerInspect(processDefinition.getIsManagerInspect());
		packageHeaderAttribute.setSortNo(processDefinition.getSortNo());
		packageHeaderAttribute.setYn(processDefinition.getYn());
		packageHeaderAttribute.setOwner(processDefinition.getOwner());
		// process end
		//node start
		createDocumentNode(packageElement,processDefId);// create node schema
		createDocumentForm(packageElement,processDefinition.getFormId());// create form schema
		createDocumentFormProcessDefinitionConfig(packageElement,processDefId);//create processDefinitionConfig schema
		//node end
		// //insert ExtendedAttributes attribute
		ExtendedAttributesDocument.ExtendedAttributes extendedAttributesElement = packageElement.addNewExtendedAttributes();
		ExtendedAttribute extendedAttribute = extendedAttributesElement.addNewExtendedAttribute();
		extendedAttribute.setName("");
		extendedAttribute.setValue("");
		
		/** process end * */
		
		boolean isXmlValid = validateXml(doc,file);
		if (isXmlValid) {
			File f = new File(file);
			File parent = new File(f.getParent());
			if (parent != null && !parent.exists()) {
				parent.mkdirs();
			}
			try {
				// Writing the XML Instance to a file.
				doc.save(f, ExportContext.getDefaultXmlOptions());
				System.out.println("导出workflow process模型[ " + f.getPath() + "] --成功!");
			} catch (IOException e) {
				System.out.println("导出workflow process模型[ " + f.getPath() + "] --失败!");
				e.printStackTrace(System.err);
			}
		}else{
			System.out.println("导出workflow process模型校验完整性失败:" + file);
		}
	}
	
	/**
	 * 创建流程配置（processDefinitionConfig）schema
	 * @param packageElement
	 * @param processDefId
	 */
	private void createDocumentFormProcessDefinitionConfig(PackageDocument.Package packageElement,String processDefId){
		List<ProcessDefinitionConfig> processDefinitionConfigs = getProcessDefinitionConfigs(processDefId);
		if(processDefinitionConfigs != null && processDefinitionConfigs.size() > 0){
			ProcessDefinitionConfigsDocument.ProcessDefinitionConfigs processDefinitionConfigsElement = packageElement.addNewProcessDefinitionConfigs();
			for(int i = 0 ; i < processDefinitionConfigs.size() ;i++){
				ProcessDefinitionConfig processDefinitionConfig = processDefinitionConfigs.get(i);
				if(processDefinitionConfig != null){
					org.apache.xmlbeans.process.xpackage.ProcessDefinitionConfigDocument.ProcessDefinitionConfig definitionConfigAttribute = processDefinitionConfigsElement.addNewProcessDefinitionConfig();
					definitionConfigAttribute.setProcessDefinitionId(processDefinitionConfig.getProcessDefinitionId());
					definitionConfigAttribute.setConfigType(processDefinitionConfig.getConfigType());
					definitionConfigAttribute.setConfigItem(processDefinitionConfig.getConfigItem());
					definitionConfigAttribute.setSortNo(processDefinitionConfig.getSortNo());
					definitionConfigAttribute.setYn(processDefinitionConfig.getYn());
				}
			}
		}
	}
	/**
	 * create form schema
	 * @param packageElement
	 * @param formId
	 */
	private void createDocumentForm(PackageDocument.Package packageElement,String formId){
		Form form = getForm(formId);
		if(form != null){
			FormDocument.Form formElement = packageElement.addNewForm();
			formElement.setId(form.getId());
			formElement.setFormCode(form.getFormCode());
			formElement.setFormName(form.getFormName());
			formElement.setFormType(form.getFormType());
			formElement.setParentId(form.getParentId());
			formElement.setTableId(form.getTableId());
			formElement.setFormDesc(form.getFormDesc());
			formElement.setYn(form.getYn());
			formElement.setOwner(form.getOwner());
			formElement.setCreateType(form.getCreateType());
			createDocumentFormItem(formElement,formId);
			createDocumentFormBusinessTable(formElement,formId);
			createDocumentFormBusinessField(formElement,formId);
		}
		
	}
	/**
	 * 
	 * @param formElement
	 * @param formId
	 */
	private void createDocumentFormItem(FormDocument.Form formElement,String formId ){
		List<FormItem> formItems=getFormItem(formId);
		
		if(formItems != null && formItems.size() > 0){
			
			FormItemsDocument.FormItems formItemsElement = formElement.addNewFormItems();
			for(int i = 0 ; i < formItems.size() ;i++){
				FormItem formItem = formItems.get(i);
				if(formItem != null){
					org.apache.xmlbeans.process.xpackage.FormItemDocument.FormItem formItemAttribute = formItemsElement.addNewFormItem();
					formItemAttribute.setId(formItem.getId());
					formItemAttribute.setFormId(formItem.getFormId());
					formItemAttribute.setFieldName(formItem.getFieldName());
					formItemAttribute.setFieldChineseName(formItem.getFieldChineseName());
					formItemAttribute.setTableType(formItem.getTableType());
					formItemAttribute.setFieldId(formItem.getFieldId());
					formItemAttribute.setDataType(formItem.getDataType());
					formItemAttribute.setLength(formItem.getLength());
					formItemAttribute.setDefaultValue(formItem.getDefaultValue());
					formItemAttribute.setInputType(formItem.getInputType());
					formItemAttribute.setInputTypeConfig(formItem.getInputTypeConfig());
					formItemAttribute.setDictTypeCode(formItem.getDictTypeCode());
					formItemAttribute.setSortNo(formItem.getSortNo());
					formItemAttribute.setValidateRule(formItem.getValidateRule());
					formItemAttribute.setYn(formItem.getYn());
					formItemAttribute.setComputeExpress(formItem.getComputeExpress());
					formItemAttribute.setComputeExpressDesc(formItem.getComputeExpressDesc());
				}
			}
		}
		
	}
	private void createDocumentFormBusinessTable(FormDocument.Form formElement,String formId){
		FormBusinessTable formBusinessTable=getFormBusinessTable(formId);
		if(formBusinessTable != null){
			FormBusinessTableDocument.FormBusinessTable formBusinessTableElement=formElement.addNewFormBusinessTable();
			formBusinessTableElement.setTableName(formBusinessTable.getTableName());
			formBusinessTableElement.setTableChineseName(formBusinessTable.getTableChineseName());
			formBusinessTableElement.setTableDesc(formBusinessTable.getTableDesc());
			formBusinessTableElement.setTableType(formBusinessTable.getTableType());
			formBusinessTableElement.setParentId(formBusinessTable.getParentId());
			formBusinessTableElement.setCreateType(formBusinessTable.getCreateType());
			formBusinessTableElement.setYn(formBusinessTable.getYn());
			formBusinessTableElement.setOwner(formBusinessTable.getOwner());
			
			
			
		}
		
	}
	private void createDocumentFormBusinessField(FormDocument.Form formElement,String formId){
		List<FormBusinessField> formBusinessFields=getFormBusinessField(formId);
		
if(formBusinessFields != null && formBusinessFields.size() > 0){
			
	FormBusinessFieldsDocument.FormBusinessFields formBusinessFieldsElement = formElement.addNewFormBusinessFields();
			for(int i = 0 ; i < formBusinessFields.size() ;i++){
				FormBusinessField formBusinessField = formBusinessFields.get(i);
				if(formBusinessField != null){
					org.apache.xmlbeans.process.xpackage.FormBusinessFieldDocument.FormBusinessField formBusinessFieldAttribute = formBusinessFieldsElement.addNewFormBusinessField();
					formBusinessFieldAttribute.setTableId(formBusinessField.getTableId());
					formBusinessFieldAttribute.setFieldName(formBusinessField.getFieldName());
					formBusinessFieldAttribute.setFieldChineseName(formBusinessField.getFieldChineseName());
					formBusinessFieldAttribute.setDataType(formBusinessField.getDataType());
					formBusinessFieldAttribute.setIsNull(formBusinessField.getIsNull());
					formBusinessFieldAttribute.setLength(formBusinessField.getLength());
					formBusinessFieldAttribute.setDefaultValue(formBusinessField.getDefaultValue());
					formBusinessFieldAttribute.setInputType(formBusinessField.getInputType());
					formBusinessFieldAttribute.setInputTypeConfig(formBusinessField.getInputTypeConfig());
					formBusinessFieldAttribute.setDictTypeCode(formBusinessField.getDictTypeCode());
					formBusinessFieldAttribute.setSortNo(formBusinessField.getSortNo());
					formBusinessFieldAttribute.setYn(formBusinessField.getYn());
					
				}
			}
		}
		
		
	}
	
	/**
	 * create node schema
	 * @param packageElement
	 * @param processDefId
	 */
	private void createDocumentNode(PackageDocument.Package packageElement,String processDefId){
		List<ProcessNode> nodes = getNodes(processDefId);
		if(nodes != null && nodes.size() > 0){
			NodesDocument.Nodes nodesElement = packageElement.addNewNodes();
			for(int i = 0 ; i < nodes.size() ; i++){
				ProcessNode node = nodes.get(i);
				if(node != null){
					Node nodeAttribute = nodesElement.addNewNode();
					nodeAttribute.setId(node.getId());
					nodeAttribute.setProcessDefinitionId(node.getProcessDefinitionId());
					nodeAttribute.setNodeId(node.getNodeId());
					nodeAttribute.setFormTemplateId(node.getFormTemplateId());
					nodeAttribute.setPrintTemplateId(node.getPrintTemplateId());
					nodeAttribute.setIsFirstNode(node.getIsFirstNode());
					nodeAttribute.setAddsignRule(node.getAddsignRule());
					nodeAttribute.setManagerNodeId(node.getManagerNodeId());
					nodeAttribute.setYn(node.getYn());
					createDocumentFormTemplate(nodeAttribute,node.getFormTemplateId());//创建节点关联的表单模板
					createDocumentPrintTemplate(nodeAttribute,node.getPrintTemplateId());//创建节点关联的打印表单模板
				}
			}
		}
	}
	
	
	/**
	 * 创建节点关联的表单模板
	 * @param nodeAttribute
	 * @param formTemplateId
	 */
	private void createDocumentFormTemplate(Node nodeAttribute,String formTemplateId){
		FormTemplate formTemplate = (FormTemplate) formTemplateService.get(formTemplateId);
		if(formTemplate != null){
			FormTemplateDocument.FormTemplate formTemplateElement = nodeAttribute.addNewFormTemplate();
			formTemplateElement.setFormId(formTemplate.getFormId());
			formTemplateElement.setTemplateDesc(formTemplate.getTemplateDesc());
			formTemplateElement.setYn(formTemplate.getYn());
			formTemplateElement.setTemplateName(formTemplate.getTemplateName());
			formTemplateElement.setTemplatePath(formTemplate.getTemplatePath());
			formTemplateElement.setTemplateType(formTemplate.getTemplatePath());
		}
	}
	/**
	 * 创建节点关联的打印表单模板
	 * @param nodeAttribute
	 * @param formTemplateId
	 */
	private void createDocumentPrintTemplate(Node nodeAttribute,String formTemplateId){
		FormTemplate formTemplate = (FormTemplate) formTemplateService.get(formTemplateId);
		if(formTemplate != null){
			PrintTemplateDocument.PrintTemplate printTemplateElement = nodeAttribute.addNewPrintTemplate();
			printTemplateElement.setFormId(formTemplate.getFormId());
			printTemplateElement.setTemplateDesc(formTemplate.getTemplateDesc());
			printTemplateElement.setYn(formTemplate.getYn());
			printTemplateElement.setTemplateName(formTemplate.getTemplateName());
			printTemplateElement.setTemplatePath(formTemplate.getTemplatePath());
			printTemplateElement.setTemplateType(formTemplate.getTemplatePath());
		}
	}
	/**
	 * get ProcessNode List
	 * @param processDefId
	 * @return
	 */
	private List<ProcessNode> getNodes(String processDefId){
		ProcessNode condition = new ProcessNode();
		condition.setProcessDefinitionId(processDefId);
		return processNodeService.find(condition);
	}
	/**
	 * 获取流程相差的配置
	 * @param processDefId
	 * @return
	 */
	private List<ProcessDefinitionConfig> getProcessDefinitionConfigs(String processDefId){
		ProcessDefinitionConfig entity = new ProcessDefinitionConfig();
		entity.setProcessDefinitionId(processDefId);
		return processDefinitionConfigService.find(entity);
	}
	/**
	 * 获取节点绑定的form
	 * @param formId
	 * @return
	 */
	private Form getForm(String formId){
		return formService.get(formId);
	}
	/**
	 * 获取节点绑定的formItem
	 * @param formId
	 * @return
	 */
	private List<FormItem> getFormItem(String formId){
		return formItemService.selectByFormId(formId);
	}
	/**
	 * 获取节点绑定的FormBusinessTable
	 * @param formId
	 * @return
	 */
	private FormBusinessTable getFormBusinessTable(String formId){
		return  formBusinessTableService.get(formService.get(formId).getTableId());
	}
	
	private List<FormBusinessField> getFormBusinessField(String formId){
		return formBusinessFieldService.selectTableFieldsByTableId(formService.get(formId).getTableId());
		
	}
	/**
	 * <p>
	 * Validates the XML, printing error messages when the XML is invalid. Note
	 * that this method will properly validate any instance of a compiled schema
	 * type because all of these types extend XmlObject.
	 * </p>
	 * 
	 * <p>
	 * Note that in actual practice, you'll probably want to use an assertion
	 * when validating if you want to ensure that your code doesn't pass along
	 * invalid XML. This sample prints the generated XML whether or not it's
	 * valid so that you can see the result in both cases.
	 * </p>
	 * 
	 * @param xml
	 *            The XML to validate.
	 * @return <code>true</code> if the XML is valid; otherwise,
	 *         <code>false</code>
	 */
	public boolean validateXml(XmlObject xml,String fileName) {
		
		boolean isXmlValid = false;

		// A collection instance to hold validation error messages.
		ArrayList validationMessages = new ArrayList();

		// Validate the XML, collecting messages.
		isXmlValid = xml.validate(new XmlOptions().setErrorListener(validationMessages));

		if (!isXmlValid) {
			System.out.println("XML文件完整性错误:"+fileName);
			for (int i = 0; i < validationMessages.size(); i++) {
				XmlError error = (XmlError) validationMessages.get(i);
//				System.out.println(xml.toString());
				System.out.println(error.getColumn());
				System.out.println(error.getMessage());
				System.out.println(error.getObjectLocation());
			}
		}
		return isXmlValid;
	}
	/**
	 * Creates a File from the XML path provided in main arguments, then parses
	 * the file's contents into a type generated from schema.
	 */
	public PackageDocument parseXml(String file) {
		File xmlfile = new File(file);
		PackageDocument doc = null;

		try {
			doc = PackageDocument.Factory.parse(xmlfile);
		} catch (XmlException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return doc;
	}
	
}

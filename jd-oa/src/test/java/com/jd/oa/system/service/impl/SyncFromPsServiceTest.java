package com.jd.oa.system.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.jd.cachecloud.driver.jedis.ShardedXCommands;
import com.jd.dts.manager.JobHandlerZkManagerFactoryBean;
import com.jd.oa.common.dao.datasource.DataSourceHandle;
import com.jd.oa.system.dao.PsUserDao;
import com.jd.oa.system.model.PsOrg;
import com.jd.oa.system.model.PsPosition;
import com.jd.oa.system.model.PsUser;
import com.jd.oa.system.service.PsOrgService;
import com.jd.oa.system.service.PsPositionService;
import com.jd.oa.system.service.PsUserService;
import com.jd.oa.system.service.SyncFromPsService;
import com.jd.oa.worker.SyncFromPsExecutor;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class SyncFromPsServiceTest extends AbstractJUnit4SpringContextTests{
	
	static Logger log = Logger.getLogger(SyncFromPsServiceTest.class);
	
	@Resource SyncFromPsService syncFromPsService;
	@Resource PsOrgService psOrgService;
	@Resource PsPositionService psPositionService;
	@Resource PsUserService psUserService;
	@Resource PsUserDao dao;
	
	static String clazz = SyncFromPsExecutor.class.getName();
	@Resource
	private ShardedXCommands redisClient;
	
	@Test
	public void testefsdf()	{
		System.out.println(dao.findAllCount());
	}
	
	@Test
	public void test1()	{
		long start = System.currentTimeMillis();
		log.info("测试--开始从peopleSoft中间表同步[组织机构]...");
		psOrgService.insertOrUpdateSysOrg();
		psOrgService.deleteSysOrg();
		log.info("测试--开始从peopleSoft中间表同步[职位]...");
		psPositionService.insertOrUpdateSysPosition();
		psPositionService.deleteSysPosition();
		log.info("测试--开始从peopleSoft中间表同步[人员]...");
		psUserService.insertOrUpdateSysUser();
		psUserService.deleteSysUser();
		log.info("测试--[组织机构|职位|人员]中间表同步完成,耗时(分钟): " + (System.currentTimeMillis() - start) /1000/60);
	}
	
//	@Autowired
//	private JobHandlerZkManagerFactoryBean jobHandlerZkManager;
//	
//	@Test
//	public void testExecutor() throws Exception	{
//		jobHandlerZkManager.create().start();
//	}
//	
	
	@Test
	public void testWorkerJob()	{
		redisClient.del(clazz);	//TODO 测试用,保证每次都执行
		if(!"true".equals(redisClient.get(clazz)))	{	//redis无该定时器标记时, 开始执行定时器
			try	{
				redisClient.setex(clazz, 60 * 30, "true");	//1小时后标记失效
				
				log.info("开始从peopleSoft中复制[组织机构|人员|职位]至中间表...");
				long start = System.currentTimeMillis();
				
//				DataSourceHandle.setDataSourceType("ps");	//在service层事务开启前切换数据源
//				log.info("从ps中查询所有组织机构...");
//				List<PsOrg> orgs = syncFromPsService.findOrgFromPs();
//				DataSourceHandle.clearDataSourceType();
//				syncFromPsService.copyOrgToSwapTable(orgs);
//				
//				DataSourceHandle.setDataSourceType("ps");
//				log.info("从ps中查询所有职位...");
//				List<PsPosition> pos = syncFromPsService.findPositionFromPs();
//				DataSourceHandle.clearDataSourceType();
//				syncFromPsService.copyPositionToSwapTable(pos);
//				
//				DataSourceHandle.setDataSourceType("ps");
//				log.info("从ps中查询所有员工...");
//				List<PsUser> users = syncFromPsService.findUserFromPs();
//				DataSourceHandle.clearDataSourceType();
//				syncFromPsService.copyUserToSwapTable(users);
				log.info("从PeopleSoft复制[组织机构|职位|人员]到OA中间表,已完成,耗时(分钟): " + (System.currentTimeMillis() - start) /1000/60);
				
				syncFromPsService.syncFromSwapTable();
			} finally	{
				redisClient.del(clazz);
				log.info("定时器执行完成, 已清除执行标记");
			}
		} else	{
			log.info("redis中有定时器[" + clazz + "]执行标记[" + redisClient.get(clazz) + "], 跳过");
		}
	}
	
	@Test
	public void test234()	{
		test2();
		test3();
		test4();
	}
	
	@Test
	public void test2()	{
		DataSourceHandle.setDataSourceType("ps");
		List<PsOrg> orgs = syncFromPsService.findOrgFromPs();

		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyOrgToSwapTable(orgs);
	}
	
	@Test
	public void test3()	{
		DataSourceHandle.setDataSourceType("ps");
		List<PsPosition> pos = syncFromPsService.findPositionFromPs();
		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyPositionToSwapTable(pos);
	}
	
	@Test
	public void test4()	{
		DataSourceHandle.setDataSourceType("ps");
		List<PsUser> users = syncFromPsService.findUserFromPs();
		
		DataSourceHandle.clearDataSourceType();
		syncFromPsService.copyUserToSwapTable(users);
	}

}

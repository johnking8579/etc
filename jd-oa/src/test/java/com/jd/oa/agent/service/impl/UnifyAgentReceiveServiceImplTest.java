package com.jd.oa.agent.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import com.google.gson.Gson;
import com.jd.oa.BaseTest;
import com.jd.oa.agent.service.ApprovalResultDTO;
import com.jd.oa.agent.service.UnifyAgentReceiveService;


public class UnifyAgentReceiveServiceImplTest extends BaseTest {

	@Resource
	UnifyAgentReceiveService service;
	
	@Test
	@Rollback(false)
	public void testReceiveJson()	{
		service.receive(readJson("testApprovalResult.txt"));
	}
	@Test
	@Rollback(false)
	public void testReceiveXml()	{
		service.receive(readJson("testApprovalResult.xml"));
	}

	@Test
	@Rollback(false)
	public void testddd() {
		String json = "{\"submitTime\":\"2012-01-01 00:00:00\", \"dddd\":1111}";
		ApprovalResultDTO dto = new Gson().fromJson(json, ApprovalResultDTO.class);
		System.out.println(dto.getSubmitTime());
	}
	
	@Test
	public void test()	{
		Map<String, String> map = new HashMap<String, String>();
		map.put("111", "1111111111111111\n33333333333333");
		System.out.println(new Gson().toJson(map));
		System.out.println(new Gson().toJsonTree(map).getAsJsonObject().get("111").getAsString());
	}

	private String readJson(String filename) {
		InputStream is = null;
		try {
			is = this.getClass().getResourceAsStream(filename);
			BufferedReader br = new BufferedReader(new InputStreamReader(is,
					"utf-8"));
			StringBuilder sb = new StringBuilder();
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s);
			}
			System.out.println(sb.toString());
			return sb.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

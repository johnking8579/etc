package com.jd.oa.di.service;

import com.jd.oa.common.webservice.AddSoapHeader;
import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.common.webservice.RESULT_DATA_TYPE;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @desc 
 * @author WXJ
 * @date 2014-6-23 下午04:35:13
 *
 */
public class HelloServiceTest {
	@Test
	public void test6(){
		String wsdl="http://oa.jd.com/services/ws/helloService?wsdl";
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		
		AddSoapHeader ash = new AddSoapHeader();				
		ArrayList list = new ArrayList();
		// 添加soap header 信息
		list.add(ash);
		//注入拦截器，getOutInterceptors代表调用服务端时触发,getInInterceptors就是被调用才触发
		factory.setOutInterceptors(list);
		factory.setServiceClass(HelloService.class);//实例化ws
		factory.setAddress(wsdl);
		Object obj = factory.create();
		HelloService service = (HelloService) obj;
		 //下面这行代码是具体调用服务段
		service.sayHello("1","2");
	}
	
	public static  void test7(){
		String wsdl="http://oa.jd.com/services/ws/helloService?wsdl";
		JaxWsDynamicClientFactory factory=JaxWsDynamicClientFactory.newInstance();
		Client client = factory.createClient(wsdl);
		Map<String,Object> wss4jMap=new HashMap<String, Object>();
		wss4jMap.put("action","UsernameToken");
		wss4jMap.put("passwordType","PasswordDigest");
		wss4jMap.put("user","jdoa");
		wss4jMap.put("passwordCallbackClass","com.jd.oa.di.service.ClientCallBack");

		client.getOutInterceptors().add(new WSS4JOutInterceptor(wss4jMap));

		Object[] params=new Object[2];
		params[0]="param1";
		params[1]="param2";
		try {
			Object[] o = client.invoke("sayHello",params);
			System.out.println(o[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}

package com.jd.oa.di.service;

import com.jd.oa.common.webservice.CXFDynamicClient;
import com.jd.oa.common.webservice.RESULT_DATA_TYPE;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangdongxing on 14-3-10.
 * Email : wangdongxing@jd.com
 */
public class DidatasourceServiceTest {
	private DiDatasourceService diDatasourceService;

	@Test
	public void test() throws Exception {
		BeanFactory factory= new ClassPathXmlApplicationContext("/spring-config.xml");
		diDatasourceService = (DiDatasourceService) factory.getBean("diDatasourceService");
		List<Map<String,Object>> list=diDatasourceService.getDatasourceResultList("b8cf6294432a413dbafd905309f28eec",1,100,new HashMap<String, Object>(), "1");
		System.out.println(list.size());
	}


	public static  void test1(){
		String wsdl="http://itilkf:800/arsys/WSDL/public/itilkf/JDXUHX:ApplyItemInterface_ProductOperate";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);

		Map<String,Object> header=new HashMap<String,Object>();
		header.put("userName","oa_ws");
		header.put("password","oa@123#");

		Map<String,Object>  body=new HashMap<String,Object>();

		List<Class<?>> paramters=client.getParamterClass("Create");

		Object[] objects=new Object[paramters.size()];
		objects[0]=client.newInstance(paramters.get(0),body);

		System.out.println("er");
		client.addSoapHeader("AuthenticationInfo",header);
//		objects=new Object[2];
//		objects[0]="1352760153";
//		objects[1]="wdx";
		Object o=client.invoke("Create",objects);
		System.out.println(o.toString());
	}

	public static  void test2(){
		String wsdl="http://itilkf:800/arsys/WSDL/public/itilkf/JDXUHX:PurchasePurposeOperate";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);

		Map<String,Object> header=new HashMap<String,Object>();
		header.put("userName","oa_ws");
		header.put("password","oa@123#");
		System.out.println("er");
		client.addSoapHeader("AuthenticationInfo",header);
		Object o=client.invoke(RESULT_DATA_TYPE.MAP,"GetList",null);
		System.out.println(o.toString());
	}

	public static  void test3(){
		String wsdl="http://itilkf:800/arsys/WSDL/public/itilkf/JDXUHX:ProjectCodeOperate";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);

		Map<String,Object> header=new HashMap<String,Object>();
		header.put("userName","oa_ws");
		header.put("password","oa@123#");
		System.out.println("er");
		client.addSoapHeader("AuthenticationInfo",header);
		Object o=client.invoke(RESULT_DATA_TYPE.MAP,"GetList",new Object[]{"固定资产","1001"});
		System.out.println(o.toString());
	}
	public static  void test4(){
		String wsdl ="http://itsm-ap3/arsys/WSDL/public/itsm-ap3/JDXUHX:ProjectCarCodeOperate";
//		String wsdl="http://itilkf:800/arsys/WSDL/public/itilkf/JDXUHX:ProjectCarCodeOperate";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);

		Map<String,Object> header=new HashMap<String,Object>();
		header.put("userName","oa_ws");
		header.put("password","oa@123#");
		System.out.println("er");
		client.addSoapHeader("AuthenticationInfo",header);
		Object o=client.invoke(RESULT_DATA_TYPE.MAP,"GetList",new Object[]{"1001"});
		System.out.println(o.toString());
	}
	public static  void test5(){
		String wsdl="http://itsm-ap3/ebs0114/ws/ebsper?wsdl";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("pcompanycode","1");
		map.put("pdeptcode","00008429");
		map.put("psrcsystem","PTS");
		map.put("pinvokesystem","");
		List<Class<?>> paramClass=client.getParamterClass("GET_HR_OU_REST");
		Object[] params=new Object[paramClass.size()];
		params[0]=client.newInstance(paramClass.get(0),map);
		Object o=client.invoke(RESULT_DATA_TYPE.MAP,"GET_HR_OU_REST",params);
		System.out.println(o.toString());
	}

	public static  void test6(){
		String wsdl="http://oa.jd.com/services/ws/helloService?wsdl";
		CXFDynamicClient client=new CXFDynamicClient(wsdl);
		Object[] params=new Object[2];
		params[0]="param1";
		params[1]="param2";
		Map<String,Object> soapHeader=new HashMap<String, Object>();
		soapHeader.put("userName","jdoa");
		soapHeader.put("password","ws@jdoa");
		client.addSoapHeader("authInfo",soapHeader);
		Object[] o=client.invoke(RESULT_DATA_TYPE.STRING,"sayHello",params);
		System.out.println(o[0].toString());
	}

	public static  void test7(){
		String wsdl="http://oa.jd.com/services/ws/helloService?wsdl";
		JaxWsDynamicClientFactory factory=JaxWsDynamicClientFactory.newInstance();
		Client client = factory.createClient(wsdl);
		Map<String,Object> wss4jMap=new HashMap<String, Object>();
		wss4jMap.put("action","UsernameToken");
		wss4jMap.put("passwordType","PasswordDigest");
		wss4jMap.put("user","jdoa");
		wss4jMap.put("passwordCallbackClass","com.jd.oa.di.service.ClientCallBack");

		client.getOutInterceptors().add(new WSS4JOutInterceptor(wss4jMap));

		Object[] params=new Object[2];
		params[0]="param1";
		params[1]="param2";
		try {
			Object[] o = client.invoke("sayHello",params);
			System.out.println(o[0].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static  void main(String[] args){
//		test1();
//		test2();
//		test3();
		test4();
//		test5();
//		test6();
//		test7();
	}
}

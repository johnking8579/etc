package com.jd.oa.custom;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;
import com.jd.oa.system.service.SysUserService;
@ContextConfiguration(locations = { "/spring-config.xml" })
public class AdminPurchaseManagerTest  extends SpringContextTestCase{
	
//	@Autowired
//	AdminPurchaseManager adminPurchaseManager;
	@Test
	public void setPurchaseRequisitionID(){
		
		AdminPurchaseManager adminPurchaseManager = new 	AdminPurchaseManager () ;
		adminPurchaseManager.setPurchaseRequisitionID("809526f3f13b4bb886778bbb48b580d8");
	}
	@Test
	public void updateProductStatusToApprove(){//809526f3f13b4bb886778bbb48b580d8
		
		AdminPurchaseManager adminPurchaseManager = new 	AdminPurchaseManager () ;
		adminPurchaseManager.updateProductStatusToApprove("809526f3f13b4bb886778bbb48b580d8");
	}
	
	@Test
	public void updateProductStatusToCompleteApprove(){//809526f3f13b4bb886778bbb48b580d8
		
		AdminPurchaseManager adminPurchaseManager = new 	AdminPurchaseManager () ;
		adminPurchaseManager.updateProductStatusToComplete("809526f3f13b4bb886778bbb48b580d8");
	}

}

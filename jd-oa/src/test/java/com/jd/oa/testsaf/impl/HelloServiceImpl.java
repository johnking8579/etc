/**
 * 
 */
package com.jd.oa.testsaf.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.jd.bk.saf.common.RPCConstants;
import com.jd.bk.saf.common.util.NetUtils;
import com.jd.bk.saf.common.util.SAFRpcContext;
import com.jd.bk.saf.registry.ZKClientManager;
import com.jd.oa.testsaf.TestHelloService;



/**
 * @author 360buy
 *
 */
public class HelloServiceImpl implements TestHelloService{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(HelloServiceImpl.class);
	public String echoStr(String str,String p) throws Exception{
		try {
			String from = "来自调用者"+p+ "  调用者版本： " 	+ SAFRpcContext.getAttribute("SAFVersion");
			LOGGER.info("我是server  "+from);
		} catch (Exception e) {
				
			e.printStackTrace();
		}finally{
		}
		//throw new RuntimeException("aaaa");
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion"))? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
		return str+"   "+NetUtils.getLocalHost() + "提供者版本： " + version;
	}
	
	public String test(String str) {
		System.out.println("我是server 1.0.6.3 changhongqiang  test    "+ str);
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion")) ? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
		return str + "调用者版本： " + version;
	}
	



	@Override
	public String getZkStr() {
		LOGGER.info("调用者版本： " + SAFRpcContext.getAttribute("SAFVersion"));
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion")) ? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
		return ZKClientManager.getZKClient().getConnectString() + "提供者版本： " + version;
	}

	@Override
	public Map<Object, Object> getMap(Map<Object, Object> map) {
		LOGGER.info("调用者版本： " + SAFRpcContext.getAttribute("SAFVersion"));
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion")) ? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
		map.put("提供者版本",version);
		return map;
	}

	@Override
	public List<Object> getList(List<Object> list) {
		LOGGER.info("调用者版本： " + SAFRpcContext.getAttribute("SAFVersion"));
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion")) ? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
				
		list.add("提供者版本： " + version);
		return list;
	}

	@Override
	public Set<Object> getSet(Set<Object> set) {
		LOGGER.info("调用者版本： " + SAFRpcContext.getAttribute("SAFVersion"));
		String version = StringUtils.hasText(SAFRpcContext.getUrlParameter("safversion"))	? SAFRpcContext.getUrlParameter("safversion"):RPCConstants.SAF_VERSION;
		set.add("提供者版本： " + version);
		return set;
	}

}

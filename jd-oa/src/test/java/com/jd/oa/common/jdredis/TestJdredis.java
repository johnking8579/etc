package com.jd.oa.common.jdredis;

import com.jd.oa.doc.model.WfFile;
import com.jd.oa.system.model.SysPrivilege;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: zhaoming
 * Date: 13-9-9
 * Time: 上午10:39
 * To change this template use File | Settings | File Templates.
 */
public class TestJdredis {
    ApplicationContext applicationContext;

    @Before
    public void init() {
        applicationContext = new ClassPathXmlApplicationContext("classpath*:spring-config-jdredis-test.xml");
    }

    @Test
    public void getWfFileListTest() {
        TestAnnotationConfigBean testAnnotationConfigBean = (TestAnnotationConfigBean) applicationContext.getBean("testAnnotationConfigBean");
        WfFile wfFile = new WfFile();
        wfFile.setId("3663cee46bfc4faab89ddcfc17a7781a");
        List<WfFile> wfFileList = null;
        try {
            wfFileList = testAnnotationConfigBean.getWfFileList1(wfFile);
            System.out.println(wfFileList.size());
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    @Test
    public void testRemove() {
        TestAnnotationConfigBean testAnnotationConfigBean = (TestAnnotationConfigBean) applicationContext.getBean("testAnnotationConfigBean");
        WfFile wfFile = new WfFile();
        wfFile.setId("3663cee46bfc4faab89ddcfc17a7781a");
        testAnnotationConfigBean.addFile(wfFile);
    }

    @Test
    public void testV14() {
        TestAnnotationConfigBean testAnnotationConfigBean = (TestAnnotationConfigBean) applicationContext.getBean("testAnnotationConfigBean");
        List<SysPrivilege> privileges = testAnnotationConfigBean.testV14("BJLXDONG");
        System.out.println(privileges);
    }
}

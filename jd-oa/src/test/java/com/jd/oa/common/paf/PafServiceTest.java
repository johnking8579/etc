package com.jd.oa.common.paf;


import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jd.oa.common.paf.model.PafResult;
import com.jd.oa.common.paf.model.TaskInstance;
import com.jd.oa.common.paf.model.processinstance.Comment;
import com.jd.oa.common.paf.model.processinstance.ProcessInstanceDomain;
import com.jd.oa.common.paf.model.processinstance.Task;
import com.jd.oa.common.paf.service.PafService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PAF工作流平台WS服务测试类
 */
public class PafServiceTest {

    private PafService pafService;

    @Before()
    public void setUp() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config-bpm-saf-test.xml");
        pafService =  (PafService)applicationContext.getBean("pafService");
    }

//    /**
//     * 创建流程实例并推进第一个人工任务
//     * @throws Exception
//     */
//	@Test
//	public void testStartProcess() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("init","bjlxdong");
//        PafResult<ProcessInstance> result =  pafService.submitHumanProcessInstance("bjlxdong", "process1382092815468", null, map);
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult().getProcessInstanceId() ============== "+result.getResult().getId());
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//	}


    /**
     * 获取待办任务
     * @throws Exception
     */
    @Test
	public void testTaskQuery() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("start","0");
        map.put("size","500");

        PafResult<List<TaskInstance>>  result = pafService.taskQuery("jdoauser_app3", "process1384742366144",map);
        if(result.isSuccess()){
            System.out.println("result.isSuccess() ============== "+result.isSuccess());
            List<TaskInstance> taskInstanceList = result.getResult();
            for(TaskInstance taskInstance : taskInstanceList){
                System.out.println("taskInstance.getId() ============== "+taskInstance.getId());
                System.out.println("taskInstance.getName() ============== "+taskInstance.getName());
                System.out.println("taskInstance.getTaskDefinitionKey() ============== "+taskInstance.getTaskDefinitionKey());
                System.out.println("taskInstance.getCreateTime() ============== "+taskInstance.getCreateTime());
                System.out.println("taskInstance.getProcessInstanceId() ============== "+taskInstance.getProcessInstanceId());
            }
        }else{
            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
        }
    }


//    /**
//     * 提交任务
//     * @throws Exception
//     */
//	@Test
//	public void testCompleteTask() throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("outcome", "approval");
////        map.put("outcome", "reject");
//        PafResult<Boolean> result =  pafService.completeUserTask("g1u2", "oa-1c464756-5217-11e3-b88a-101f7430a210", map);
//
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult() ============== "+result.getResult());
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//	}
//
//    /**
//     * 历史任务查询
//     * @throws Exception
//     */
//	@Test
//	public void testHistoricTaskQuery() throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("start","0");
//        map.put("size","5");
//
//        PafResult<List<TaskInstance>>  result = pafService.historyTaskQuery("bjlxdong", "settlementRuleCreate", map);
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            List<TaskInstance> taskInstanceList = result.getResult();
//            for(TaskInstance taskInstance : taskInstanceList){
//                System.out.println("taskInstance.getId() ============== "+taskInstance.getId());
//                System.out.println("taskInstance.getName() ============== "+taskInstance.getName());
//                System.out.println("taskInstance.getName() ============== "+taskInstance.getAssignee());
//                System.out.println("taskInstance.getCreateTime() ============== "+taskInstance.getStartTime());
//                System.out.println("taskInstance.getCreateTime() ============== "+taskInstance.getEndTime());
//                System.out.println("taskInstance.getProcessInstanceId() ============== "+taskInstance.getProcessInstanceId());
//            }
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//	}
//
//
//    /**
//     * 流程实例查询
//     * @throws Exception
//     */
//	@Test
//	public void testProcessInstQuery() throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("start","0");
//        map.put("size","5");
//        PafResult<List<ProcessInstance>>  result = pafService.processInstQuery("bjlxdong", "settlementRuleCreate", map);
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            List<ProcessInstance> taskInstanceList = result.getResult();
//            for(ProcessInstance processInstance : taskInstanceList){
//                System.out.println("processInstance.getId() ============== "+processInstance.getId());
//                System.out.println("processInstance.getStartTime() ============== "+processInstance.getStartTime());
//                System.out.println("processInstance.getProcessDefinitionId() ============== "+processInstance.getProcessDefinitionId());
//                System.out.println("processInstance.getCreateTime() ============== "+processInstance.getStartTime());
//                System.out.println("processInstance.getStartUserId() ============== "+processInstance.getStartUserId());
//            }
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//
//	}
//
//    /**
//     * 记录审批意见
//     * @throws Exception
//     */
//    @Test
//    public void testAddTaskComment() throws Exception {
//
//        PafResult<Boolean> result =  pafService.addTaskComment("bjlxdong", "mydo-08421d92-f8f6-11e2-b2de-001f29cc9390", "mydo-ddc6fef8-f4ff-11e2-b2de-001f29cc9390", "测试测试");
//
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult() ============== "+result.getResult());
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//    }
//
//    /**
//     * 重新分配任务
//     * @throws Exception
//     */
//    @Test
//    public void testReassignTask() throws Exception {
//
//        PafResult<Boolean> result =  pafService.reassignTask("bjlxdong", "mydo-b64c806d-f503-11e2-b2de-001f29cc9390", "bjlxdong", "bjyuanwei", "user");
//
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult() ============== "+result.getResult());
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//    }
//
//
//
//    /**
//     * 流程实例详细信息查询
//     * @throws Exception
//     */
//    @Test
//    public void testGetProcessInstanceDetail() throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("start","0");
//        map.put("size","5");
//        PafResult<ProcessInstanceDomain>  result = pafService.getProcessInstanceDetail("bjlxdong", "mydo-727fc41f-41ec-11e3-bb9e-001f29cc9390");
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            ProcessInstanceDomain processInstance = result.getResult();
//            System.out.println("processInstance.getProcessDefinitionId() ============== "+processInstance.getProcessDefinitionId());
//            System.out.println("processInstance.getStartTime() ============== "+processInstance.getStartTime());
//            System.out.println("processInstance.getProcessDefinitionId() ============== "+processInstance.getProcessDefinitionId());
//            System.out.println("processInstance.getCreateTime() ============== "+processInstance.getStartTime());
//            System.out.println("processInstance.getStartUserId() ============== "+processInstance.getStartUserId());
//            List<Task> tasks = processInstance.getTasks();
//            for(Task task : tasks){
//                System.out.println("task.getTaskName() ============== "+task.getTaskName());
//                List<Comment> comments = task.getComments();
//                for(Comment comment : comments){
//                    System.out.println("comment.getContent() ============== "+comment.getContent());
//                }
//                System.out.println("==========================================");
//            }
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//
//    }

//    /**
//     * 取消流程实例
//     * @throws Exception
//     */
//    @Test
//    public void testCancelProcessInstance() throws Exception {
//
//        PafResult<Boolean> result =  pafService.cancelProcessInstance("bjlxdong", "mydo-5b7051ad-f8db-11e2-b2de-001f29cc9390");
//
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult() ============== "+result.getResult());
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//    }

//    /**
//     * 获取流程实例图片
//     */
//    @Test
//    public void testProcessInstanceFlowPictureQuery(){
//        PafResult<byte[]> result = pafService.processInstanceFlowPictureQuery("bjlxdong", "mydo-5b7051ad-f8db-11e2-b2de-001f29cc9390");
//        if(result.isSuccess()){
//            System.out.println("result.getResult() ============== "+result.getResult().toString());
//        }
//    }

//    /**
//     * 查询当前租户下所有的待办任务 (仅支持SAF方式调用)
//     * @throws Exception
//     */
//    @Test
//    public void testTaskQueryAdvanced() throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("start","0");
//        map.put("size","1000");
//        //map.put("init","test_app1");
//        map.put("TASK_PARTICIPATOR","dbhansong");
//
//        PafResult<List<TaskInstance>>  result = pafService.taskQueryAdvanced("dbhansong", map);
//        if(result.isSuccess()){
//            System.out.println("result.isSuccess() ============== "+result.isSuccess());
//            System.out.println("result.getResult().size() ============== "+result.getResult().size());
//            List<TaskInstance> taskInstanceList = result.getResult();
//            for(TaskInstance taskInstance : taskInstanceList){
////                System.out.println("taskInstance.getId() ============== "+taskInstance.getId());
////                System.out.println("taskInstance.getName() ============== "+taskInstance.getName());
////                System.out.println("taskInstance.getTaskDefinitionKey() ============== "+taskInstance.getTaskDefinitionKey());
////                System.out.println("taskInstance.getCreateTime() ============== "+taskInstance.getCreateTime());
////                System.out.println("taskInstance.getProcessInstanceId() ============== "+taskInstance.getProcessInstanceId());
//            	System.out.println("dbhansong:"+taskInstance.getId()+","+taskInstance.getProcessInstanceId());
//            }
//
//        	
//        }else{
//            System.out.println("result.getErrorCode() ============== "+result.getErrorCode());
//            System.out.println("result.getLocalizedMessage() ============== "+result.getLocalizedMessage());
//            System.out.println("result.getErrorStack() ============== "+result.getErrorStack());
//        }
//    }
	
}

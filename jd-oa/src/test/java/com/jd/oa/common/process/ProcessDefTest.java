package com.jd.oa.common.process;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jd.oa.process.model.ProcessDefinition;
import com.jd.oa.process.service.ProcessDefService;

public class ProcessDefTest {
	ProcessDefService processDefService;
    @Before()
    public void setUp() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        processDefService=(ProcessDefService) applicationContext.getBean("processDefService");
    }
    @Test
    public void testAdd(){
    	for(int i=1;i<16;i++){
    		ProcessDefinition processDefinition=new ProcessDefinition();
    		processDefinition.setProcessDefinitionName("流程定义"+i);
    		processDefinition.setProcessTypeId(1+"");
    		processDefinition.setYn(0);
    		processDefinition.setCreateTime(new Date());
    		processDefinition.setIsIssue("0");
    		processDefService.addOrUpdateProcessDefinition(processDefinition);
    	}
    }
    @Test
    public void testUpdate(){
    	ProcessDefinition processDefinition=new ProcessDefinition();
    	processDefinition.setId("49b574e9546e4cc59d46992ec16b6fd6");
    	processDefinition.setProcessDefinitionName("流程定义2");
    	processDefinition.setProcessTypeId(1+"");
    	processDefinition.setIsIssue("0");
    	processDefService.addOrUpdateProcessDefinition(processDefinition);
    }
    @Test
    public void testDelete(){
    	String[] ids={"b47dcaf5f56c4709a18d40344dde4798","b3407a6103dc4b5ab248ec31b69e7858"};
    	processDefService.deleteProcessDefinition(ids);
    }
}

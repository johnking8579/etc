package com.jd.oa.common.sms.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.sms.SmsService;
import com.jd.oa.common.test.SpringContextTestCase;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class SmsServiceImplTest extends SpringContextTestCase {
	
	@Autowired
	private SmsService smsService;

	@Test
	public void testSentMessage() {
		smsService.sentMessage("15810901386", "JDOA Test");
	}

}

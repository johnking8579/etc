package com.jd.oa.common.jss;

import com.jcloud.jss.JingdongStorageService;
import com.jcloud.jss.domain.Bucket;
import com.jcloud.jss.domain.ObjectListing;
import com.jcloud.jss.domain.ObjectSummary;
import com.jcloud.jss.domain.StorageObject;
import com.jd.common.util.StringUtils;
import com.jd.oa.common.jss.service.JssService;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.*;
import java.util.List;
import java.util.Map;


/**
 * JSS测试类
 */
public class JssServiceTest {

    JssService jssService;

    /**
     *  初始化配置文件
     */
    @Before()
    public void setUp() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config-jss-test.xml");
        jssService = (JssService)applicationContext.getBean("jssService");
    }

    /**
     * 创建桶
     */
    @Test
    public void testCreateBucket(){
        String bucketName = "wsoa";
        jssService.createBucket(bucketName);
    }

    /**
     * 删除桶
     */
    @Test
    public void testDeleteBucket(){
        String bucketName = "b3name";
        jssService.deleteBucket(bucketName);
    }

    /**
     * 获取桶列表
     */
    @Test
    public void testGetBuckets(){
        List<Bucket> buckets = jssService.getBuckets();
        for(Bucket bucket:buckets)
        {
            System.out.println("bucketName:"+bucket.getName());
        }
    }

    /**
     * 以文本形式上传到云端
     */
    @Test
    public void testTextUploadFile(){
        String bucketName = "b2name";
        String key = "新人培训计划1.docx";
        String content = "aaaaaaaaaaaaaaaaaa";
        jssService.uploadFile(bucketName, key, content);
    }

    /**
     * 以流形式上传到云端
     */
    @Test
    public void testInputStreamUploadFile(){
        try
        {
            String bucketName = "b2name";
            String key = "新人培训计划.doc";
            InputStream inputStringStream = new FileInputStream(new File("D:\\新人培训计划.doc"));
            jssService.uploadFile(bucketName, key, inputStringStream);
        }catch(Exception e){
            System.out.println("Message : " + e.getMessage());
        }
    }

    /**
     * 查找所有对象(默认返回前1000个ObjectSummary)
     */
    @Test
    public void testFindObjects(){
        String bucketName = "b3name";
        ObjectListing oResult = jssService.findObjects(bucketName);
        for (ObjectSummary okey : oResult.getObjectSummaries()) {
            System.out.println("keyName:" + okey.getKey());
            System.out.println("keyName:" + okey.getSize());
        }
    }

    /**
     * 判断KEY是否存在
     */
    @Test
    public void testExist(){
        String bucketName = "b2name";
        String key = "新人培训计划.doc";
        System.out.println(jssService.exist(bucketName, key));
    }

    /**
     * 删除对象
     */
    @Test
    public void testDeleteObject(){
        String bucketName = "b2name";
        String key = "新人培训计划.doc";
        jssService.deleteObject(bucketName, key);
    }

    /**
     * 下载文件
     */
    @Test
    public void testDownloadFile(){
        try{
            String bucketName = "b2name";
            String key = "新人培训计划1.html";
//            InputStream is = new BufferedInputStream(new FileInputStream(new File("c:\\eac4f671423c4a92a4b24705ad410c80.doc")));                           //本地
            InputStream is = new BufferedInputStream(jssService.downloadFile("OA", "59bb630678d543cfa5e179dd5bc119be.pdf"));

//            System.out.println("=========="+buff.length);
//            InputStream is = jssService.downloadFile("OA", "eac4f671423c4a92a4b24705ad410c80.doc");
            byte[] buff = IOUtils.toByteArray(is);
            System.out.println("==========="+buff.length);

//            byte[] buff = new byte[1];
//            FileOutputStream fos = new FileOutputStream(new File("C:\\\\测试在线编辑d3e13dd28693445b8942183541a0643f.docx"));
//            int read = 0;
//            StringBuffer sb = new StringBuffer();
//            while ((read = is.read(buff)) != -1) {
//                fos.write(buff, 0, read);
//            }
//            System.out.println("========================================="+sb.toString());
//            int i;
//            char[] b =new char[1024];
//            while((i=is.read(b))!=-1){
//                System.out.print(new String(b));
//            }


//            fos.flush();
//            fos.close();
            is.close();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    @Test
    public void head() throws UnsupportedEncodingException {

        JingdongStorageService jss = jssService.getJssService();
        String bucketName = "b2name";
        String key = "新人培训计划1.html";
        try {
            //字符串转换成流
            String content = "aaaaaaa<input type='text' value='11111111'>aaaaaaaaaaa";
            InputStream inputStringStream1 = new ByteArrayInputStream(content.getBytes("UTF-8"));
            jss.bucket(bucketName).object(key).entity(content.length(), inputStringStream1).put();

            //本地文件
//            InputStream inputStringStream2 = new BufferedInputStream(new FileInputStream(new File("D:\\\\新人培训计划.doc")));
//            jss.bucket(bucketName).object(key).entity(inputStringStream2.available(), inputStringStream2).put();
        } catch (Exception e) {
            e.printStackTrace();
        }


        StorageObject so = jss.bucket(bucketName).object(key).head();
        String bucket = so.getBucket();  // 获取bucketName
        long contentLength = so.getContentLength();  // 获取该key的大小
        String contentoType = so.getContentType();  // 获取流类型
        String md5 = so.getETag();  // 获取该key的MD5
        String lastModified = so.getLastModified();  // 获取该key的最后修改时间
        Map<String, String> headers = so.getHeaders(); // 获取服务端返回的headers


        System.out.println("bucket ======" +bucket);
        System.out.println("contentLength ======" +contentLength);
        System.out.println("contentoType ======" +contentoType);
        System.out.println("md5 ======" +md5);
        System.out.println("lastModified ======" +lastModified);


//        MessageDigest md5 = MessageDigest
//                .getInstance(FoundationConstants.TYPE_MD5);
//        md5.update(tempDomain.getBytes());
//        byte[] domain = md5.digest();
//        StringBuffer md5StrBuff = new StringBuffer();
//        // converting domain to String
//        for (int i = 0; i < domain.length; i++) {
//            if (Integer.toHexString(0xFF & domain[i]).length() == 1) {
//                md5StrBuff.append("0").append(
//                        Integer.toHexString(0xFF & domain[i]));
//            } else
//                md5StrBuff.append(Integer.toHexString(0xFF & domain[i]));
//        }

    }

    @Test
    public void teststr(){
        String str = "1111111.doc.doc";
        System.out.println(StringUtils.substringAfterLast(str, "."));
        System.out.println(StringUtils.substringBeforeLast(str, "."));
    }
}

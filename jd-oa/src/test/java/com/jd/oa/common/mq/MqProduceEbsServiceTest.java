package com.jd.oa.common.mq;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.agent.service.MqProduceEbsService;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.common.utils.SpringContextUtils;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class MqProduceEbsServiceTest extends SpringContextTestCase {
	private static final Logger logger = Logger.getLogger(MqProduceEbsServiceTest.class);
	
	private MqProduceEbsService mqProduceEbsService;
	@Test
	public void test() {
		if(mqProduceEbsService == null){
			mqProduceEbsService = SpringContextUtils.getBean(MqProduceEbsService.class);
		}
		mqProduceEbsService.mqProducer("test send Ebs message");
	}
	
}

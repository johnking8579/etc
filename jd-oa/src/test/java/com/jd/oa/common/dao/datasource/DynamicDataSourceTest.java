package com.jd.oa.common.dao.datasource;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.jd.oa.common.dao.sqladpter.MySqlAdapter;
import com.jd.oa.common.dao.sqladpter.SqlAdapter;
import com.jd.oa.common.test.SpringContextTestCase;
import com.jd.oa.system.model.Application;
import com.jd.oa.system.model.SysOrganization;
import com.jd.oa.system.model.SysPosition;
import com.jd.oa.system.model.SysUser;
import com.jd.oa.system.service.ApplicationService;
import com.jd.oa.system.service.PsService;
import com.jd.oa.system.service.SysOrganizationService;
import com.jd.oa.system.service.SysPositionService;
import com.jd.oa.system.service.SysUserService;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class DynamicDataSourceTest extends SpringContextTestCase {

	@Autowired
	public ApplicationService applicationService;
	
	@Autowired
    private PsService psService;
	
	@Autowired
	SysOrganizationService sysOrganizationService;
	
	@Autowired
	SysPositionService sysPositionService;
	
	@Autowired
	SysUserService sysUserService;

	@Test
	public void insert() {
		Application application = new Application();
		application.setCode("01111");
		application.setName("jd-oa");
		application.setIpAddress("127.0.0.1");
		applicationService.insert(application);

		DataSourceHandle.setDataSourceType("jdoa1");
		Application application1 = new Application();
		application1.setCode("011112");
		application1.setName("jd-oa1");
		application1.setIpAddress("127.0.0.1");
		applicationService.insert(application);
	}

	public void sqltest() {
		String sql = "CREATE TABLE new_table (`ID` INT NOT NULL, `NAME` VARCHAR(45) NULL, PRIMARY KEY (`ID`));";
		SqlAdapter sqlAdapter = new MySqlAdapter();
//		sqlAdapter.setSql(sql);
		applicationService.sql(sqlAdapter);
	}

	public void deleteTest() {
		String sql = "CREATE TABLE new_table (`ID` INT NOT NULL, `NAME` VARCHAR(45) NULL, PRIMARY KEY (`ID`));";
		SqlAdapter sqlAdapter = new MySqlAdapter();
//		sqlAdapter.setSql(sql);
		applicationService.sql(sqlAdapter);
	}
	
	@Test
	@Transactional
	public void testPs(){
		long starttime = System.currentTimeMillis();
		System.out.println("+++++++++++++++++++++++++++++++ begin! +++++++++++++++++++++++++++++++++++");
		//DataSourceHandle.setDataSourceType("ps");
		List<SysOrganization> orgs = psService.getAllOrganizations();
		//List<SysPosition> pos = psService.getAllPositions();
//		List<SysUser> users = psService.getAllUsers();
		
		//sysOrganizationService.transferOrgInfoFromHR(orgs);
		//sysPositionService.transferPositionInfoFromHR(pos);
		//sysUserService.transferUserInfoFromHR(users);
		long endtime = System.currentTimeMillis();
		System.out.println("+++++++++++++++++++++++++++++++ end  "+(endtime-starttime)+"ms ! +++++++++++++++++++++++++++++++++++");
		//System.out.println("orgs size="+orgs.size());
		//System.out.println("positions size="+pos.size());
		System.out.println("users size="+orgs.size());
	}
}

package com.jd.oa.common.paf;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jd.oa.common.constant.SystemConstant;
import com.jd.oa.common.jss.service.JssService;
import com.jd.oa.common.paf.service.PafProcessDeployService;

import javax.ws.rs.core.Response;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PAF工作流平台测试类
 */
public class RestServiceTest {

	JssService jssService;
	
	PafProcessDeployService pafProcessDeployService;
	
    /**
     *  初始化配置文件
     */
    @Before()
    public void setUp() {
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config-jss-test.xml");
        jssService = (JssService)applicationContext.getBean("jssService");
    }

    /**
     * 发布流程图
     * @throws Exception
     */
	@Test
	public void testDeployProcess() throws Exception {
        try {
            String bucketName = "jd-oa";
        	String key = "af4aeaae87d84af4a119c6fd1d5222dd.bpmn20.xml";
        	
            File file = jssService.getFile(bucketName, key);
            boolean flag = pafProcessDeployService.deploy(file);
            if(!flag){
                throw new RuntimeException("流程发布失败");
            }
        } catch (Exception e) {
            throw new RuntimeException("流程发布失败");
        }
        

    }
	
}

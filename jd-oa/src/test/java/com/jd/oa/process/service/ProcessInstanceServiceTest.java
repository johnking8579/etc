package com.jd.oa.process.service;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.jd.oa.common.utils.IdUtils;
import com.jd.oa.process.model.ProcessInstance;
import com.jd.oa.process.model.ProcessType;

@ContextConfiguration(locations = { "/spring-config.xml" })
public class ProcessInstanceServiceTest {
	
	@Autowired
	private ProcessInstanceService processInstanceService;
	
	

	@Test
	public void createProcessInstance(){
		ProcessInstance entity = new ProcessInstance();
		entity.setProcessDefinitionId("process1381739097506");
		
		entity.setProcessInstanceId(IdUtils.uuid2());
		entity.setProcessInstanceName("通过测试用例生成的数据");
		entity.setBusinessInstanceId("1234567890-");
		entity.setBeginTime(new Date());
		processInstanceService.insert(entity);
	}
	
	
}

package com.paipai.cms.mongo;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;
import com.paipai.cms.BaseTest;
import com.paipai.cms.common.Config;
import com.paipai.cms.dao.ICustomerDao;
import com.paipai.cms.domain.Customer;
import com.paipai.cms.domain.JsonData;
import com.paipai.cms.domain.PublishInfo;
import com.paipai.cms.domain.RuleRedis;

public class SpringMongoTest extends BaseTest {
	/*
	@Resource
	private CustomerDao repository;
	
	@Test
	public void test1()	{
		repository.deleteAll();

		// save a couple of customers
		repository.save(new Customer("Alice", "Smith"));
		repository.save(new Customer("Bob", "Smith"));

		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		for (Customer customer : repository.findAll()) {
			System.out.println(customer);
		}
		System.out.println();

		// fetch an individual customer
		System.out.println("Customer found with findByFirstName('Alice'):");
		System.out.println("--------------------------------");
		System.out.println(repository.findByFirstName("Alice"));

		System.out.println("Customers found with findByLastName('Smith'):");
		System.out.println("--------------------------------");
		for (Customer customer : repository.findByLastName("Smith")) {
			System.out.println(customer);
		}
		
	}
	*/
	@Resource
	MongoTemplate mongoTemplate;
	@Resource
	ICustomerDao customerDao;
	
//	@Test
//	public void testFind()	{
//		System.out.println("SpringMongoTest.testFind()");
//		
//		DB db = mongoTemplate.getDb();
//		Set<String> colls = db.getCollectionNames();
//		 for(String s : colls) {
//		  System.out.println(s);
//		 }
//		System.out.println("SpringMongoTest.testFind()-------------");
//		
//		List<Customer> list = mongoTemplate.findAll(Customer.class);
//		for(Customer c : list)	{
//			System.out.println(c);
//		}
//	}
//	
//	@Test
//	public void testInsertFind()	{
//		Customer cu = new Customer("liubenlong3","刘本龙" , new Date().getTime(), new Date().getTime());
//		mongoTemplate.insert(cu);
//		List<Customer> list = mongoTemplate.findAll(Customer.class);
//		for(Customer c : list)	{
//			System.out.println(c);
//		}
//	}
//	
//	
//	/**
//	 * 直接插入json字符串
//	 */
//	@Test
//	public void testInsertJson()	{
//		Object parse = JSON.parse("{\"floor4\":[{\"name\":\"舌尖诱惑\",\"sname\":\"年节伴手礼\",\"url\":\"\",\"img\":\"http://pics3.paipaiimg.com/update/20150302/pcd_021454671.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"美食 年货\",\"third\":\"\"},{\"name\":\"休闲零食\",\"key\":\"休闲零食\",\"third\":\"饼干|饼干,果冻|果冻,膨化食品|膨化食品,肉铺|肉铺,蜜饯|蜜饯,豆类食品|豆类食品,海味即食|海味即食,薯片|薯片\"},{\"name\":\"中外名酒\",\"key\":\"酒\",\"third\":\"葡萄酒|葡萄酒,白酒|白酒,香槟|香槟,洋酒|洋酒,冰酒|冰酒,黄酒|黄酒,养生酒|养生酒,啤酒|啤酒\"},{\"name\":\"进口美食\",\"key\":\"进口美食\",\"third\":\"进口牛奶|进口牛奶,进口饼干|进口饼干,进口洋酒|进口洋酒,进口咖啡|进口咖啡,进口糕点|进口糕点,进口巧克力|进口巧克力,进口糖果|进口糖果,进口红酒|进口红酒\"},{\"name\":\"茶叶茗品\",\"key\":\"茶叶茗品\",\"third\":\"红茶|红茶,花草茶|花草茶,绿茶|绿茶,乌龙茶|乌龙茶,普洱茶|普洱茶,大红袍|大红袍,铁观音|铁观音,龙井茶|龙井茶\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/import/2342/index.shtml?ptag=20539.2.1&app=1\",\"title\":\"年节伴手礼\",\"img\":\"http://pics3.paipaiimg.com/update/20150216/pcd_1080-270.jpg\"}],\"shp\":[]},{\"name\":\"电子时代\",\"sname\":\"爆品大抢购\",\"url\":\"\",\"img\":\"http://pics2.paipaiimg.com/update/20150302/pcd_021503102.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"手机数码\",\"third\":\"\"},{\"name\":\"手机通讯\",\"key\":\"手机\",\"third\":\"苹果|苹果,三星|三星,酷派|酷派,华为|华为,荣耀|荣耀,魅族|魅族,联想|联想手机,小米|小米手机\"},{\"name\":\"摄影摄像\",\"key\":\"相机\",\"third\":\"相机|相机,单反|单反,微单|微单,自拍神器|自拍神器,LOMO|LOMO,拍立得|拍立得,佳能|佳能,尼康|尼康\"},{\"name\":\"耳机\",\"key\":\"耳机\",\"third\":\"Beats|Beats,铁三角|铁三角,爱科技|爱科技,漫步者|漫步者,索尼|索尼,魔声|魔声,赛德斯|赛德斯,苹果|苹果\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/6538/index.shtml?ptag=20539.2.2&app=1\",\"title\":\"爆品大抢购\",\"img\":\"http://pics2.paipaiimg.com/update/20150211/index_1080-240.jpg\"}],\"shp\":[]},{\"name\":\"丽人衣柜\",\"sname\":\"显瘦搭配\",\"url\":\"\",\"img\":\"http://pics0.paipaiimg.com/update/20150302/pcd_021511703.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"女装\",\"third\":\"\"},{\"name\":\"女士上衣\",\"key\":\"女装\",\"third\":\"开衫|开衫,毛呢外套|毛呢外套,皮草|皮草,T恤|T恤,打底衫|打底衫,雪纺衫|雪纺衫,旗袍|旗袍,婚纱|婚纱\"},{\"name\":\"女士下装\",\"key\":\"女装\",\"third\":\"铅笔裤|铅笔裤,牛仔裤|牛仔裤,连体裤|连体裤,半身裙|半身裙,打底裤|打底裤,九分裤|九分裤,休闲裤|休闲裤,运动裤|运动裤\"},{\"name\":\"女士内衣\",\"key\":\"女装\",\"third\":\"保暖内衣|保暖内衣,聚拢文胸|聚拢文胸,棉内裤|棉内裤,棉睡衣|棉睡衣,连裤袜|连裤袜,文胸套装|文胸套装,真丝睡衣|真丝睡衣,打底衫|打底衫\"},{\"name\":\"女士配件\",\"key\":\"女装\",\"third\":\"帽子|帽子,鞋包|鞋包,手套|手套,腰带|腰带,腰链|腰链,围巾|围巾,丝巾|丝巾,披肩|披肩\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/6595/index.shtml?ptag=20539.2.3&app=1\",\"title\":\"显瘦搭配\",\"img\":\"http://pics0.paipaiimg.com/update/20150216/pcd_15460815.jpg\"}],\"shp\":[]},{\"name\":\"睿士风尚\",\"sname\":\"微店精选\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_02151994.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"男装\",\"third\":\"\"},{\"name\":\"男士上装\",\"key\":\"男装\",\"third\":\"衬衫|衬衫,POLO衫|POLO衫,马甲|马甲,T恤|T恤,针织衫|针织衫,卫衣|卫衣,棒球衫|棒球衫,风衣|风衣\"},{\"name\":\"男士下装\",\"key\":\"男装\",\"third\":\" 休闲裤|休闲裤,西裤|西裤,工装裤|工装裤,直筒裤|直筒裤,牛仔裤|牛仔裤,九分裤|九分裤,运动裤|运动裤,短裤|短裤\"},{\"name\":\"男士内衣\",\"key\":\"男士\",\"third\":\"男士保暖|男士保暖,内裤|品质内裤,盒装内裤|盒装内裤,棉质睡衣|棉质睡衣,打底T恤|打底T恤,平角内裤|平角内裤,低帮短袜|低帮短袜,男士塑身|男士塑身\"},{\"name\":\"男士配件\",\"key\":\"男装\",\"third\":\"腰带|腰带,帽子|帽子,围巾|围巾,领结|领结,男士手套|男士手套,头巾|头巾,袖扣|袖扣,腰带|腰带\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/import/2394/index.shtml?ptag=20539.2.4&app=1\",\"title\":\"微店精选\",\"img\":\"http://pics3.paipaiimg.com/update/20150216/pcd_154623435.jpg\"}],\"shp\":[]},{\"name\":\"车行天下\",\"sname\":\"年度盘点\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_021527281.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"车品\",\"third\":\"\"},{\"name\":\"内外饰品\",\"key\":\"车饰品\",\"third\":\"座垫|座垫,座套|座套,脚垫|脚垫,车贴|车贴,方向盘套|方向盘套,抱枕|抱枕,车载香水|车载香水,挂件|挂件\"},{\"name\":\"清洗美容\",\"key\":\"车\",\"third\":\"汽油添加剂|汽油添加剂,洗车液|洗车液,车蜡|车蜡,防雾剂|防雾剂,玻璃水|玻璃水,洗车刷|洗车刷,补漆笔|补漆笔,擦车巾|擦车巾\"},{\"name\":\"配件改装\",\"key\":\"配件改装\",\"third\":\"雨刮器|雨刮器,蓄电池|蓄电池,电瓶|电瓶,轮毂|轮毂,挡泥板|挡泥板,扶手箱|扶手箱,汽车喇叭|汽车喇叭,车外灯|车外灯\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/4398/index.shtml?ptag=20539.2.5&app=1\",\"title\":\"年度TOP车品大盘点\",\"img\":\"http://pics0.paipaiimg.com/update/20150213/pcd_154734950.jpg\"}],\"shp\":[]},{\"name\":\"跃动山野\",\"sname\":\"换装过年\",\"url\":\"\",\"img\":\"http://pics3.paipaiimg.com/update/20150302/pcd_021544257.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"运动户外\",\"third\":\"\"},{\"name\":\"户外服装\",\"key\":\"户外服装\",\"third\":\"冲锋衣裤|冲锋衣裤,速干衣裤|速干衣裤,抓绒衣裤|抓绒衣裤,羽绒服|羽绒服,滑雪服|滑雪服,户外风衣|户外风衣,夹克|夹克,军迷服饰|军迷服饰\"},{\"name\":\"户外装备\",\"key\":\"户外装备\",\"third\":\"登山包|登山包,帐篷|帐篷,垫子|垫子,睡袋|睡袋,吊床|吊床,便携桌椅|便携桌椅,户外照明|户外照明,野餐烧烤|野餐烧烤\"},{\"name\":\"健身训练\",\"key\":\"健身\",\"third\":\"乔山|乔山,锐步|锐步,BH|BH,跑步机|跑步机,哑铃|哑铃,器械|健身器械,仰卧板|仰卧板,车|健身车\"},{\"name\":\"体育娱乐\",\"key\":\"运动\",\"third\":\"羽毛球|羽毛球,网球|网球,乒乓球|乒乓球,篮球|篮球,足球|足球,台球|台球,轮滑|轮滑,滑板|滑板\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/6639/index.shtml?ptag=20539.2.6&app=1\",\"title\":\"换装过大年\",\"img\":\"http://pics2.paipaiimg.com/update/20150224/pcd_223741869.jpg\"}],\"shp\":[]},{\"name\":\"辣妈潮宝\",\"sname\":\"处处迎新\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_021553566.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"母婴\",\"third\":\"\"},{\"name\":\"宝宝用品\",\"key\":\"婴儿\",\"third\":\"奶粉|奶粉,尿不湿|尿不湿,奶瓶|奶瓶,体温计|体温计,棉签|棉签,耳温枪|耳温枪,耳掏|耳掏,餐桌|餐桌\"},{\"name\":\"洗护用品\",\"key\":\"母婴\",\"third\":\"纸尿裤|纸尿裤,湿纸巾|湿纸巾,理发器|理发器,洗衣液|洗衣液,洗衣皂|洗衣皂,宝宝浴盆|宝宝浴盆,毛巾|毛巾,浴巾|浴巾\"},{\"name\":\"寝居服饰\",\"key\":\"母婴\",\"third\":\"婴儿外出服|婴儿外出服,婴儿内衣|婴儿内衣,婴儿礼盒|婴儿礼盒,童帽|童帽,童袜|童袜,鞋|婴儿鞋,婴儿睡袋|婴儿睡袋,抱被|抱被\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/promote/2014/5376/index.shtml?ptag=20539.2.7&app=1\",\"title\":\"处处迎新\",\"img\":\"http://pics1.paipaiimg.com/update/20150210/pcd_index_1080-270.jpg\"}],\"shp\":[]},{\"name\":\"美丽妆颜\",\"sname\":\"新年趴\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_021559972.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"美妆护肤\",\"third\":\"\"},{\"name\":\"护肤\",\"key\":\"护肤\",\"third\":\"洗面奶|洗面奶,爽肤水|爽肤水,乳液|乳液,面霜|面霜,精华|精华,面膜|面膜,防晒霜|防晒霜,晒后修复|晒后修复\"},{\"name\":\"彩妆\",\"key\":\"彩妆\",\"third\":\"BB霜|BB霜,隔离霜|隔离霜,粉底液|粉底液,粉饼|粉饼,散粉|散粉,蜜粉|蜜粉,睫毛膏|睫毛膏,假睫毛|假睫毛\"},{\"name\":\"香水\",\"key\":\"香水\",\"third\":\"女士香水|女士香水,男士香水|男士香水,中性香水|中性香水,Q版香水|Q版香水,淡香水|淡香水,古龙水|古龙水,香体喷雾|香体喷雾,迪奥|迪奥\"},{\"name\":\"男士护肤\",\"key\":\"男士护肤\",\"third\":\"洁面|洁面,乳液|乳液,面霜|面霜,爽肤水|爽肤水,面膜|面膜,祛痘|祛痘,眼霜|眼霜,须后水|须后水\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/import/1886/index.shtml?ptag=20539.2.8&app=1\",\"title\":\"正品联盟\",\"img\":\"http://pics0.paipaiimg.com/update/20150215/pcd_143459733.jpg\"}],\"shp\":[]},{\"name\":\"皮革行装\",\"sname\":\"萌动2015\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_021609886.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"高跟鞋\",\"third\":\"\"},{\"name\":\"潮流女鞋\",\"key\":\"女鞋\",\"third\":\"短靴|短靴,马丁靴|马丁靴,长靴|长靴,中筒靴|中筒靴,雪地靴|雪地靴,流苏靴|流苏靴,高帮鞋|高帮鞋,尖头靴|尖头靴\"},{\"name\":\"时尚男鞋\",\"key\":\"男鞋\",\"third\":\"休闲鞋|休闲鞋,帆布鞋|帆布鞋,板鞋|板鞋,手工鞋|手工鞋,韩版鞋|韩版鞋,英伦鞋|英伦鞋,懒人鞋|懒人鞋,增高鞋|增高鞋\"},{\"name\":\"流行女包\",\"key\":\"女包\",\"third\":\"单肩包|单肩包,斜挎包|斜挎包,手提包|手提包,手拿包|手拿包,钱包|钱包,零钱包|零钱包,真皮包|真皮包,贝壳包|贝壳包\"},{\"name\":\"精品男包\",\"key\":\"男包\",\"third\":\"商务包|商务包,休闲包|休闲包,公文包|公文包,单肩包|单肩包,斜跨包|斜跨包,手提包|手提包,手拿包|手拿包,短款钱包|短款钱包\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/import/2328/index.shtml?ptag=20539.2.9&app=1\",\"title\":\"纪念旧情人\",\"img\":\"http://pics0.paipaiimg.com/update/20150224/pcd_224724509.jpg\"}],\"shp\":[]},{\"name\":\"乐享生活\",\"sname\":\"品质在家\",\"url\":\"\",\"img\":\"http://pics1.paipaiimg.com/update/20150302/pcd_021616744.png\",\"sub\":[{\"name\":\"精选\",\"key\":\"家居用品\",\"third\":\"\"},{\"name\":\"家装软饰\",\"key\":\"家居\",\"third\":\"十字绣|十字绣,沙发垫|沙发垫,地毯|地毯,地垫|地垫,窗帘|窗帘,桌布|桌布,墙贴|墙贴,摆件|摆件\"},{\"name\":\"灯具照明\",\"key\":\"家居\",\"third\":\"吸顶灯|吸顶灯,吊灯|吊灯,台灯|台灯,壁灯|壁灯,落地灯|落地灯,水晶灯|水晶灯,小夜灯|小夜灯,射灯|射灯\"},{\"name\":\"家纺用品\",\"key\":\"家居\",\"third\":\"四件套|四件套,毛巾|毛巾,居家拖鞋|居家拖鞋,床垫|床垫,被套|被套,枕套|枕套,毯子|毯子,浴袍|浴袍\"},{\"name\":\"厨卫卫浴\",\"key\":\"家居\",\"third\":\"马桶|马桶,花洒|花洒,水龙头|水龙头,洗手盆|洗手盆,水槽|水槽,浴霸|浴霸,浴室柜|浴室柜,浴巾架|浴巾架\"}],\"ban\":[{\"id\":1,\"url\":\"http://www.paipai.com/m2/2014/import/2296/index.shtml?ptag=20539.2.10&app=1\",\"title\":\"品质在我家\",\"img\":\"http://pics1.paipaiimg.com/update/20150204/life_1080-270.jpg\"}],\"shp\":[]}]}");
//		DBCollection dbCollection = mongoTemplate.getCollection("jsondata");
//		dbCollection.insert((DBObject)parse);
//	}
//	
//	
//	
//	/**
//	 * 查询
//	 */
//	@Test
//	public void testquery()	{
//		Object findOne = mongoTemplate.findById("54f3d5ee6471a84680042c1f", Customer.class	, "customer");
//		System.out.println(findOne);
//		
//		Criteria criteria = new Criteria("erp").is("liubenlong3");
//		
//		JSON json = new JSON();
//		DBObject parse = (DBObject) json.parse("{\"s\":\"sdfgh\"}");
//		Customer customer = mongoTemplate.findAndModify(new Query(criteria), new Update().set("name", parse), Customer.class, "customer");
//		System.out.println(customer.getName());
//	}
//	
//	/**
//	 * 测试原始Java驱动   直接插入json字符串
//	 * @throws UnknownHostException 
//	 */
//	@Test
//	public void testInsertJsonBymongojavadriver() throws UnknownHostException	{
//		Mongo m = new Mongo("192.168.152.67", 27017);
//		 DB db = m.getDB("ppappcms");
//		 Set<String> colls = db.getCollectionNames();
//		 for(String s : colls) {
//		  System.out.println(s);
//		 }
//		 
//		 JSON json = new JSON();
//		 Object parse = json.parse("{  \"erp\" : \"liubwwenlong3\",  \"name\" : \"刘ww本龙\"}");
//		 DBCollection dbCollection = db.getCollection("customer");
//		 dbCollection.insert((DBObject) parse);
//	}
	
	
	/**
	 * 修改规则rule和示例数据
	 * app首页热门类目
	 * @throws UnknownHostException 
	 */
	@SuppressWarnings("static-access")
	@Test
	public void updateRule() throws UnknownHostException	{
		Criteria criteria = new Criteria("_id").is("5501482ef56f2ecf020f1a14");
		DBObject rule = (DBObject) new JSON().parse("{        \"name\": {            \"type\": \"1\",            \"desc\": \"标题\",            \"validate\": [                \"notnull\"            ],            \"level\": \"1\"        },        \"sname\": {            \"type\": \"1\",            \"desc\": \"副标题\",            \"validate\": [                \"notnull\"            ],            \"level\": \"1\"        },        \"img\": {            \"type\": \"1\",            \"desc\": \"图片的URL\",            \"validate\": [                \"url\"            ],            \"level\": \"1\"        },        \"sub\": {            \"type\": \"3\",            \"desc\": \"二级数据\",            \"fields\": {                \"name\": {                    \"type\": \"1\",                    \"desc\": \"标题\",                    \"validate\": [                        \"notnull\"                    ],                    \"level\": \"2\"                },                \"key\": {                    \"type\": \"1\",                    \"desc\": \"副标题\",                    \"validate\": [                        \"notnull\"                    ],                    \"level\": \"2\"                },                \"third\": {                    \"type\": \"1\",                    \"desc\": \"三级标题\",                    \"validate\": [                        \"notnull\"                    ],                    \"level\": \"2\"                }            },            \"level\": \"1\"        },        \"ban\": {            \"type\": \"3\",            \"desc\": \"ban\",            \"fields\": {                \"name\": {                    \"type\": \"1\",                    \"desc\": \"标题\",                    \"validate\": [                        \"notnull\"                    ],                    \"level\": \"2\"                },				\"address\": {                    \"type\": \"2\",                    \"desc\": \"三级标题-地址\",                    \"fields\": {                        \"phone\": {                            \"type\": \"1\",                            \"desc\": \"手机\",                            \"validate\": [                                \"phone\"                            ],                            \"level\": \"2\"                        },                        \"jiedao\": {                            \"type\": \"1\",                            \"desc\": \"街道\",                            \"validate\": [                                \"notnull\"                            ],                            \"level\": \"2\"                        }                    },                    \"level\": \"2\"                },                \"type\": {                    \"type\": \"4\",                    \"desc\": \"类型\",                    \"fill\": {                        \"1\": \"男\",                        \"2\": \"女\"                    },                    \"level\": \"2\"                },                \"aihao\": {                    \"type\": \"5\",                    \"desc\": \"爱好\",                    \"fill\": {                        \"youyong\": \"游泳\",                        \"taiqiu\": \"台球\",                        \"lanqiu\": \"篮球\"                    },                    \"level\": \"2\"                }                            },            \"level\": \"1\"        }    }");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_rule", rule), JsonData.class, "jsondata");
		
		DBObject dataDemo = (DBObject) new JSON().parse("{\"name\":\"zhangsan\",\"sname\":\"副标题\",\"img\":\"dfghjk\",\"sub\":[{\"name\":\"zhangsan\",\"key\":\"zxcvbnjkl\",\"third\":\"11\"}],\"ban\":[{\"name\":\"zhangsan\",\"type\":\"1,2\",\"aihao\":\"youyong,taiqiu\",			\"address\":{				\"phone\":\"zhangsan\",				\"jiedao\":\"1,2\"			}}]}");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_demo", dataDemo), JsonData.class, "jsondata");
		
		
		
		
		
		
		criteria = new Criteria("_id").is("55039493160be8793ba2a47d");
		rule = (DBObject) new JSON().parse("{\"name\":{\"type\":\"1\",\"desc\":\"标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"fdhgfhfsg\":{\"type\":\"1\",\"desc\":\"副标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"img\":{\"type\":\"1\",\"desc\":\"图片的URL\",\"validate\":[\"url\"],\"level\":\"1\"}}");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_rule", rule), JsonData.class, "jsondata");
		
		dataDemo = (DBObject) new JSON().parse("{    \"name\" : \"aaaaaaaaaaa\",    \"fdhgfhfsg\" : \"dddddddddddd是\",    \"img\" : \"www.pp.com/a.jsp\"  }");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_demo", dataDemo), JsonData.class, "jsondata");
		
		
		
		
		
		
		
		criteria = new Criteria("_id").is("550394b6160b91322128b464");
		rule = (DBObject) new JSON().parse("{\"name\":{\"type\":\"1\",\"desc\":\"标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"fdhgfhfsg\":{\"type\":\"1\",\"desc\":\"副标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"tgbyhnujm\":{\"type\":\"1\",\"desc\":\"图片的URL\",\"validate\":[\"url\"],\"level\":\"1\"}}");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_rule", rule), JsonData.class, "jsondata");
		
		dataDemo = (DBObject) new JSON().parse("{    \"name\" : \"aaaaaaaaaaa\",    \"tgbyhnujm\" : \"dddddddddddd是\",    \"fdhgfhfsg\" : \"sssssssssss\"  }");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("json_data_demo", dataDemo), JsonData.class, "jsondata");
	}
	
	
	@Test
	public void test3() throws UnknownHostException	{
		Criteria criteria = new Criteria("_id").is("5501482ef56f2ecf020f1a14");
		mongoTemplate.findAndModify(new Query(criteria), new Update().set("lock_time", 0), JsonData.class);
		
	}
	
	
	
	@Test
	public void tesfft3() throws UnknownHostException	{
	
		String a = "      {  \"name\" : \"app首页V3.1.4_android\",  \"references\" : [{\"id\":\"550288c3c0599875837e31bc\",\"keyName\":\"banner\"}, {\"id\":\"550394ea160b9f1fbd18d168\",\"keyName\":\"floor1\"}],  \"filename\" : \"app_firstpage_3.1.4_ios.js\"  }";
	
		
		Mongo m = new Mongo("192.168.152.67", 27017);
		 DB db = m.getDB("ppappcms");
		 
		 Object parse = new JSON().parse(a);
		 DBCollection dbCollection = db.getCollection("publishinfo");
		 dbCollection.insert((DBObject) parse);
		
	}
	
	

//	@Test
//	public void test3() throws UnknownHostException	{
//	
//		String a = "{\"desc\":\"版本等信息描述\",\"is_lock\":\"0\",\"lock_erp\":\"修改人\",\"lock_time\":\"修改时间\",\"json_data_draft\":{\"name\":\"zhangsan\",\"sname\":\"副标题\",\"img\":\"dfghjk\",\"sub\":[{\"name\":\"zhangsan\",\"key\":\"zxcvbnjkl\",\"third\":\"11\"}],\"ban\":[{\"name\":\"zhangsan\",\"type\":\"1,2\",\"aihao\":\"youyong,taiqiu\",\"address\":{\"phone\":\"18652830521\",\"jiedao\":\"1,2\"}}]},\"json_data_online\":{\"name\":\"zhangsan\",\"sname\":\"副标题\",\"img\":\"dfghjk\",\"sub\":[{\"name\":\"zhangsan\",\"key\":\"zxcvbnjkl\",\"third\":\"11\"}],\"ban\":[{\"name\":\"zhangsan\",\"type\":\"1,2\",\"aihao\":\"youyong,taiqiu\",\"address\":{\"phone\":\"18652830521\",\"jiedao\":\"1,2\"}}]},\"json_data_lastline\":{\"update_erp\":\"修改人\",\"update_time\":\"修改时间\",\"data\":\"编辑的json数据对象\"},\"json_data_rule\":{\"name\":{\"type\":\"1\",\"desc\":\"标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"sname\":{\"type\":\"1\",\"desc\":\"副标题\",\"validate\":[\"notnull\"],\"level\":\"1\"},\"img\":{\"type\":\"1\",\"desc\":\"图片的URL\",\"validate\":[\"url\"],\"level\":\"1\"},\"sub\":{\"type\":\"3\",\"desc\":\"二级数据\",\"fields\":{\"name\":{\"type\":\"1\",\"desc\":\"标题\",\"validate\":[\"notnull\"],\"level\":\"2\"},\"key\":{\"type\":\"1\",\"desc\":\"副标题\",\"validate\":[\"notnull\"],\"level\":\"2\"},\"third\":{\"type\":\"1\",\"desc\":\"三级标题\",\"validate\":[\"notnull\"],\"level\":\"2\"}},\"level\":\"1\"},\"ban\":{\"type\":\"3\",\"desc\":\"ban\",\"fields\":{\"name\":{\"type\":\"1\",\"desc\":\"标题\",\"validate\":[\"notnull\"],\"level\":\"2\"},\"address\":{\"type\":\"2\",\"desc\":\"三级标题-地址\",\"fields\":{\"phone\":{\"type\":\"1\",\"desc\":\"手机\",\"validate\":[\"phone\"],\"level\":\"2\"},\"jiedao\":{\"type\":\"1\",\"desc\":\"街道\",\"validate\":[\"notnull\"],\"level\":\"2\"}},\"level\":\"2\"},\"type\":{\"type\":\"4\",\"desc\":\"类型\",\"fill\":{\"1\":\"男\",\"2\":\"女\"},\"level\":\"2\"},\"aihao\":{\"type\":\"5\",\"desc\":\"爱好\",\"fill\":{\"youyong\":\"游泳\",\"taiqiu\":\"台球\",\"lanqiu\":\"篮球\"},\"level\":\"2\"}},\"level\":\"1\"}},\"json_data_demo\":{\"name\":\"zhangsan\",\"sname\":\"副标题\",\"img\":\"dfghjk\",\"sub\":[{\"name\":\"zhangsan\",\"key\":\"zxcvbnjkl\",\"third\":\"11\"}],\"ban\":[{\"name\":\"zhangsan\",\"type\":\"1,2\",\"aihao\":\"youyong,taiqiu\",\"address\":{\"phone\":\"zhangsan\",\"jiedao\":\"1,2\"}}]}}";
//	
//		
//		Mongo m = new Mongo("192.168.152.67", 27017);
//		 DB db = m.getDB("ppappcms");
//		 
//		 Object parse = new JSON().parse(a);
//		 DBCollection dbCollection = db.getCollection("jsondata");
//		 dbCollection.insert((DBObject) parse);
//		
//	}
		
		
//	
//	@Test
//	public void test3()	{
//		System.out.println(customerDao.count());
//		System.out.println(customerDao.findByErp("cccc"));
//	}

}

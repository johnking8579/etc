package com.jd.common.sso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;

/**
 * 单点登录工具
 * @author JingYing
 * @date 2015年2月2日
 */
public class SsoTool {
	
	private String cookieKey = "erp1.jd.com",	//单点登录时向cookie存放用户信息时使用的key 
				secretKey, 	//解密cookieValue时使用的密钥,测试环境与正式环境不同
				loginUrl = "http://erp1.jd.com/newHrm/Verify.aspx?ReturnUrl=";	//登录页面url, loginUrl后需要跟utf8 Urlencode后的跳转页面, 登录成功后会跳转. returnUrl不支持1个以上的参数
	
	/**
	 * 解析cookie, 不成功时抛出异常
	 * @param cookieValue
	 * @return 解析后的用户erp账号
	 * @throws SsoException
	 */
	public DotnetAuthenticationTicket parseUser(Cookie[] cookies)	{
		if(cookies == null)	{
			throw new SsoException("没有获得用户登录信息");
		}
		for(Cookie c : cookies)	{
			if(cookieKey.equals(c.getName()))	{
				try {
					return DotnetAuthenticationUtil.getFormsAuthenticationTicket(c.getValue(), secretKey);
				} catch (Exception e) {
					throw new SsoException("解析用户cookie异常");
				}
			}
		}
		throw new SsoException("没有获得用户登录信息");
	}
	
	/**
	 * 生成单点登录URL
	 * @param returnUrl 回跳页面
	 * @return
	 */
	public String genLoginUrl(String returnUrl)	{
		try {
			return loginUrl +  URLEncoder.encode(returnUrl, "utf-8");
		} catch (UnsupportedEncodingException e) {
			return loginUrl + returnUrl;
		}
	}

	public String getCookieKey() {
		return cookieKey;
	}

	public void setCookieKey(String cookieKey) {
		this.cookieKey = cookieKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}
}

package com.paipai.cms.domain;

public class Missoin {

	private String createDate;
	private String createUser;
	private String missionCode;
	private String missionSubject;
	private String userid;
	private String retMessage;
	
	
	public String getRetMessage() {
		return retMessage;
	}
	public void setRetMessage(String retMessage) {
		this.retMessage = retMessage;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getMissionCode() {
		return missionCode;
	}
	public void setMissionCode(String missionCode) {
		this.missionCode = missionCode;
	}
	public String getMissionSubject() {
		return missionSubject;
	}
	public void setMissionSubject(String missionSubject) {
		this.missionSubject = missionSubject;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	
	
}

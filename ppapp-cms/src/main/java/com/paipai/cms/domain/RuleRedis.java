package com.paipai.cms.domain;


public class RuleRedis {

    
	@Override
	public String toString() {
		return "RuleRedis [id=" + id + ", desc=" + desc + ", rule=" + rule
				+ "]";
	}
	private String id ;
	private String desc;
	private String rule;
	
	
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	


}

package com.paipai.cms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="publishinfo")
public class PublishInfo {

	@Id
	private String _id;
	
	private String  name;//名称
	private String  references;//引用哪些数据，存对应jsondata表的id
	private String  filename;//文件名
	
	@Override
	public String toString() {
		return "PublishInfo [_id=" + _id + ", name=" + name + ", references="
				+ references + ", filename=" + filename + "]";
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getReferences() {
		return references;
	}
	public void setReferences(String references) {
		this.references = references;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	
		
}

package com.paipai.cms.common.jqgrid;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paipai.cms.common.PageModel;

/**
 * 把实体集合序列化jqgrid所要求的字符串.
 */
public class JqgridSerializer	{
	
	/**
	 * 主算法, 将pagemodel对象转化为jqgrid字符串
	 * total主页数, page当前页, record记录数
	 * id主键
	 * {"total":"1","page":"1","records":"2",
	 * "rows":[
	 * 		{"id":"1","cell":["属性1","属性2","属性3","属性4"]},
	 * 		{"id":"2","cell":["属性a","属性b","属性c","属性d"]}]
	 * }
	 * @param pagemodel
	 * @return
	 */
	public <T> ObjectNode toMaingrid(PageModel<T> pageModel, ColumnMapper<T> columnMapper)	{
		ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode jsonArray = objectMapper.createArrayNode();
		
		for(T t : pageModel.getList()){
			if(t == null) break;
			ObjectNode json = objectMapper.createObjectNode();
//			JsonObject json = new JsonObject();
			List<String> columns = columnMapper.mapColumn(t);
			json.put("id", columnMapper.getRowId(t));
			
			ArrayNode ja = objectMapper.createArrayNode();
//			JsonArray ja = new JsonArray();
			for(String s : columns)	{
				if(s == null) s = "";
				ja.add(s);
			}
			json.put("cell", ja);
			
			jsonArray.add(json);
		}
		
		ObjectNode finalJson = objectMapper.createObjectNode();
//		JsonObject finalJson = new JsonObject();
		if((pageModel.getTotal() % pageModel.getPageSize()) != 0)
			finalJson.put("total", (pageModel.getTotal()/pageModel.getPageSize() + 1) + "");
		else
			finalJson.put("total", (pageModel.getTotal()/pageModel.getPageSize()) + "");
		finalJson.put("page", pageModel.getPageNo() + "");
		finalJson.put("records", pageModel.getTotal() + "");
		finalJson.put("rows", jsonArray);
		
		return finalJson;
	}
	
	
	/**
	 * 生成subGrid用字符串, 格式如下
	 * {"rows": [
	 * 		{"id":"1", "cell":["拉菲正牌-1999",5,150.0,750.0]},
	 * 		{"id":"2", "cell":["ww正牌-2000",3,250.0,750.0]}  ]
	 * }
	 * @param <T>
	 * @param list
	 * @return
	 */
	public <T> String toSubgrid(PageModel<T> pageModel, ColumnMapper<T> columnMapper)	{
		ObjectNode json = toMaingrid(pageModel, columnMapper);
		json.remove("total");
		json.remove("page");
		json.remove("records");
		return json.toString();
	}

	/**
	 * 生成subGrid用字符串, 格式如下
	 * {"rows": [
	 * 		{"id":"1", "cell":["拉菲正牌-1999",5,150.0,750.0]},
	 * 		{"id":"2", "cell":["ww正牌-2000",3,250.0,750.0]}  ]
	 * }
	 * @param <T>
	 * @param list
	 * @return
	 */
	public <T> String toSubgrid(List<T> list, ColumnMapper<T> columnMapper)	{
		
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode json = objectMapper.createObjectNode();
		ArrayNode finalArray = objectMapper.createArrayNode();
		
		
//		JsonObject json = new JsonObject();
//		JsonArray finalArray = new JsonArray();
		for(T t : list)	{
			ObjectNode jo = objectMapper.createObjectNode();
//			JsonObject jo = new JsonObject();
			List<String> columns = columnMapper.mapColumn(t);
			
			ArrayNode array = objectMapper.createArrayNode();
//			JsonArray array = new JsonArray();
			for(String s : columns)	{
				array.add(s);
			}
			jo.put("id", columnMapper.getRowId(t));
			jo.put("cell", array);
			finalArray.add(jo);
		}
		json.put("rows", finalArray);
		return json.toString();
	}
	
//	public static void main(String[] args) {
//	//调用方式:
//		PageModel p = new PageModel();
//		String json = new JqgridSerializer().toMaingrid(p, new ColumnMapper<Example>() {
//
//			@Override
//			public List<String> mapColumn(Example t) {
//				List<String> l = new ArrayList<String>();
//				l.add(t.getKey());
//				l.add(t.getValue());
//				return l;
//			}
//
//			@Override
//			public String getRowId(Example t) {
//				return t.getId()+"";
//			}
//		});
//		System.out.println(json);
//	}
}

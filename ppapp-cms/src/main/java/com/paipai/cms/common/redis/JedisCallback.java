package com.paipai.cms.common.redis;

import redis.clients.jedis.Jedis;

public interface JedisCallback {
	
	Object doWithJedis(Jedis jedis);

}

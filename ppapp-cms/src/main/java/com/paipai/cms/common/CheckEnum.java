package com.paipai.cms.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;


public enum CheckEnum {

	PHONE("^((13|15|17|18)[0-9])\\d{8}$", "请输入手机号码", "phone"),
	URL("(http://|ftp://|https://|www){0,1}[^\u4e00-\u9fa5\\s]*?\\.(com|net|cn|me|tw|fr)[^\u4e00-\u9fa5\\s]*", "请输入URL", "url"),
	NOTNULL("notNull", "不可为空", "notnull");
	
	private String checkRule;//校验规则（可以使正则表达式）
	private String msg;//错误提醒
	private String tag;//说明
	
	// 普通方法
    public static List<String> getCheckEnumByTag(JsonNode jsonNode) {
    	List<String> list = new ArrayList<String>();
    	for (JsonNode jsonNode2 : jsonNode) {
    		for (CheckEnum c : CheckEnum.values()) {
            	if(c.getTag().equals(jsonNode2.asText())){
            		list.add(c.getMsg());
            	}
            }
		}
    	return list;
    }
	
 // 普通方法
    public static CheckEnum getOneCheckEnumByTag(String validateTag) {
		for (CheckEnum c : CheckEnum.values()) {
        	if(c.getTag().equals(validateTag)){
        		return c;
        	}
        }
    	return null;
    }
    
	private CheckEnum(String checkRule, String msg, String tag) {
		this.checkRule = checkRule;
		this.msg = msg;
		this.tag = tag;
	}
	
	public String getCheckRule() {
		return checkRule;
	}
	public void setCheckRule(String checkRule) {
		this.checkRule = checkRule;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
	
}

package com.paipai.cms.common.redis;

import java.io.Serializable;

/**
 * 把byte[]存放到redis时,专用的包装类,用来序列化byte[]. 
 * 注: 除了RedisTemplate,不要调用该类!
 */
class Bytes implements Serializable{
	private static final long serialVersionUID = 2289189232982317664L;
	private byte[] bytes;
	
	public Bytes (byte[] bytes){
		this.bytes = bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}
}

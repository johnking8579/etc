//package com.paipai.cms.common;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//
//import org.codehaus.jackson.JsonEncoding;
//import org.codehaus.jackson.JsonGenerator;
//import org.codehaus.jackson.JsonNode;
//import org.codehaus.jackson.JsonParseException;
//import org.codehaus.jackson.map.JsonMappingException;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.node.JsonNodeFactory;
//import org.codehaus.jackson.node.ObjectNode;
//import org.codehaus.jackson.type.JavaType;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.data.mapping.model.SimpleTypeHolder;
//
//import com.paipai.cms.domain.Customer;
//
// 
//
///**
// * 
// * http://www.cnblogs.com/hoojo/archive/2011/04/22/2024628.html
// * <b>function:</b>Jackson 将java对象转换成JSON字符串，也可以将JSON字符串转换成java对象
// * @version 1.0
//
// */
//
//@SuppressWarnings("unchecked")
//
//public class JacksonTest {
//
//    private JsonGenerator jsonGenerator = null;
//
//    private ObjectMapper objectMapper = null;
//
//    private Customer bean = null;
//
//    
//
//    @Before
//
//    public void init() {
//
//        bean = new Customer("liubenlong3","刘本龙",65486L,9987676L);
//
//        objectMapper = new ObjectMapper();
//
//        try {
//
//            jsonGenerator = objectMapper.getJsonFactory().createJsonGenerator(System.out, JsonEncoding.UTF8);
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//
//    
//
//    @After
//
//    public void destory() {
//
//        try {
//
//            if (jsonGenerator != null) {
//
//                jsonGenerator.flush();
//
//            }
//
//            if (!jsonGenerator.isClosed()) {
//
//                jsonGenerator.close();
//
//            }
//
//            jsonGenerator = null;
//
//            objectMapper = null;
//
//            bean = null;
//
//            System.gc();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//
//    
//    /**
//
//     * <b>function:</b>将java对象转换成json字符串
//
//     * @author hoojo
//
//     * @createDate 2010-11-23 下午06:01:10
//
//     */
//
//    @Test
//    public void writeEntityJSON() {
//
//        
//
//        try {
//
//            System.out.println("jsonGenerator");
//
//            //writeObject可以转换java对象，eg:JavaBean/Map/List/Array等
//
//            jsonGenerator.writeObject(bean);    
//
//            System.out.println();
//
//            
//
//            System.out.println("ObjectMapper");
//
//            //writeValue具有和writeObject相同的功能
//
//            objectMapper.writeValue(System.out, bean);
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    /**
//
//     * <b>function:</b>将map转换成json字符串
//
//     * @author hoojo
//
//     * @createDate 2010-11-23 下午06:05:26
//
//     */
//
//    @Test
//
//    public void writeMapJSON() {
//
//        try {
//
//            Map<String, Object> map = new HashMap<String, Object>();
//
//            map.put("name", bean.getName());
//            map.put("account", bean);
//
//            
//
//            System.out.println("jsonGenerator");
//
//            jsonGenerator.writeObject(map);
//
//            System.out.println("");
//
//            
//
//            System.out.println("objectMapper");
//
//            objectMapper.writeValue(System.out, map);
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    /**
//
//     * <b>function:</b>将list集合转换成json字符串
//
//     * @author hoojo
//
//     * @createDate 2010-11-23 下午06:05:59
//
//     */
//
//    @Test
//
//    public void writeListJSON() {
//
//        try {
//
//            List<Customer> list = new ArrayList<Customer>();
//            list.add(bean);
//
//            bean = new Customer("sfwes", "xxxxxxx", 345678L, 9876543L);
//            list.add(bean);
//
//            
//
//            System.out.println("jsonGenerator");
//
//            //list转换成JSON字符串
//
//            jsonGenerator.writeObject(list);
//
//            System.out.println();
//
//            System.out.println("ObjectMapper");
//
//            //用objectMapper直接返回list转换成的JSON字符串
//
//            System.out.println("1###" + objectMapper.writeValueAsString(list));
//
//            System.out.print("2###");
//
//            //objectMapper list转换成JSON字符串
//
//            objectMapper.writeValue(System.out, list);
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    @Test
//
//    public void writeOthersJSON() {
//
//        try {
//
//            String[] arr = { "a", "b", "c" };
//
//            System.out.println("jsonGenerator");
//
//            String str = "hello world jackson!";
//
//            //byte
//
//            jsonGenerator.writeBinary(str.getBytes());
//
//            //boolean
//
//            jsonGenerator.writeBoolean(true);
//
//            //null
//
//            jsonGenerator.writeNull();
//
//            //float
//
//            jsonGenerator.writeNumber(2.2f);
//
//            //char
//
//            jsonGenerator.writeRaw("c");
//
//            //String
//
//            jsonGenerator.writeRaw(str, 5, 10);
//
//            //String
//
//            jsonGenerator.writeRawValue(str, 5, 5);
//
//            //String
//
//            jsonGenerator.writeString(str);
//
//            jsonGenerator.writeTree(JsonNodeFactory.instance.POJONode(str));
//
//            System.out.println();
//
//            
//
//            //Object
//
//            jsonGenerator.writeStartObject();//{
//
//            jsonGenerator.writeObjectFieldStart("user");//user:{
//
//            jsonGenerator.writeStringField("name", "jackson");//name:jackson
//
//            jsonGenerator.writeBooleanField("sex", true);//sex:true
//
//            jsonGenerator.writeNumberField("age", 22);//age:22
//
//            jsonGenerator.writeEndObject();//}
//
//            
//
//            jsonGenerator.writeArrayFieldStart("infos");//infos:[
//
//            jsonGenerator.writeNumber(22);//22
//
//            jsonGenerator.writeString("this is array");//this is array
//
//            jsonGenerator.writeEndArray();//]
//
//            
//
//            jsonGenerator.writeEndObject();//}
//
//            
//
//            
//
//            Customer bean = new Customer("sdfa", "sssssss",345678L, 9876543L);
//            bean.setName("haha");
//
//            //complex Object
//
//            jsonGenerator.writeStartObject();//{
//
//            jsonGenerator.writeObjectField("user", bean);//user:{bean}
//
//            jsonGenerator.writeObjectField("infos", arr);//infos:[array]
//
//            jsonGenerator.writeEndObject();//}
//
//            
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    /**
//     *  将json字符串转换成JavaBean对象
//     */
//    @Test
//
//    public void readJson2Entity() {
//
//        String json = "{\"id\":null,\"erp\":\"liubenlong3\",\"name\":\"刘本龙\",\"register_time\":65486,\"login_time\":9987676}";
//
//        try {
//
//        	Customer acc = objectMapper.readValue(json, Customer.class);
//            System.out.println(acc.getName());
//            System.out.println(acc);
//
//        } catch (JsonParseException e) {
//
//            e.printStackTrace();
//
//        } catch (JsonMappingException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    /**
//
//     * <b>function:</b>json字符串转换成list<map>
//
//     * @author hoojo
//
//     * @createDate 2010-11-23 下午06:12:01
//
//     */
//
//    @Test
//
//    public void readJson2List() {
//
//        String json = "[{\"id\":null,\"erp\":\"liubenlong3333333333\",\"name\":\"刘本龙333\",\"register_time\":65486,\"login_time\":9987676},"+
//
//                    "{\"id\":null,\"erp\":\"liubenlong31111111111\",\"name\":\"刘本龙111\",\"register_time\":65486,\"login_time\":9987676}]";
//
//        try {
//
//            List<LinkedHashMap<String, Object>> list = objectMapper.readValue(json, List.class);
//
//            System.out.println(list.size());
//
//            for (int i = 0; i < list.size(); i++) {
//
//                Map<String, Object> map = list.get(i);
//
//                Set<String> set = map.keySet();
//
//                for (Iterator<String> it = set.iterator();it.hasNext();) {
//
//                    String key = it.next();
//
//                    System.out.println(key + ":" + map.get(key));
//
//                }
//
//            }
//
//        } catch (JsonParseException e) {
//
//            e.printStackTrace();
//
//        } catch (JsonMappingException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    /**
//
//     * <b>function:</b>json字符串转换成Array
//
//     * @author hoojo
//
//     * @createDate 2010-11-23 下午06:14:01
//
//     */
//
//    @Test
//
//    public void readJson2Array() {
//
//    	 String json = "[{\"id\":null,\"erp\":\"liubenlong3333333333\",\"name\":\"刘本龙333\",\"register_time\":65486,\"login_time\":9987676},"+
//                    "{\"id\":null,\"erp\":\"liubenlong31111111111\",\"name\":\"刘本龙111\",\"register_time\":65486,\"login_time\":9987676}]";
//
//        try {
//
//            Customer[] arr = objectMapper.readValue(json, Customer[].class);
//
//            System.out.println(arr.length);
//
//            for (int i = 0; i < arr.length; i++) {
//
//                System.out.println(arr[i]);
//
//            }
//
//            
//
//        } catch (JsonParseException e) {
//
//            e.printStackTrace();
//
//        } catch (JsonMappingException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    /**
//
//     * <b>function:</b>json字符串转换Map集合
//
//     * @author hoojo
//
//     * @createDate Nov 27, 2010 3:00:06 PM
//
//     */
//
//    @Test
//
//    public void readJson2Map() {
//
//    	String json = "{\"id\":null,\"erp\":\"liubenlong3\",\"name\":\"刘本龙\",\"register_time\":65486,\"login_time\":9987676}";
//
//        try {
//
//            Map<String, Map<String, Object>> maps = objectMapper.readValue(json, Map.class);
//
//            System.out.println(maps.size());
//
//            Set<String> key = maps.keySet();
//
//            Iterator<String> iter = key.iterator();
//
//            while (iter.hasNext()) {
//
//                String field = iter.next();
//
//                System.out.println(field + ":" + maps.get(field));
//
//            }
//
//        } catch (JsonParseException e) {
//
//            e.printStackTrace();
//
//        } catch (JsonMappingException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//
//    }
//    
//    
//    
//    @Test
//
//    public void readJson2Gson() throws JsonParseException, JsonMappingException, IOException {
//    	
//    	ObjectMapper mapper = new ObjectMapper();
//    	ObjectNode createObjectNode = mapper.createObjectNode();
//    	createObjectNode.put("a", "d");
//    	System.out.println(createObjectNode);
//    	
//    	
////    	String json = "{\"erp\":\"liubenlong3\",\"name\":[\"刘本龙\",\"刘本龙11\"],\"register_time\":\"65486\",\"login_time\":{\"erp\":\"liub11111enlong3\",\"name\":\"刘本龙\",\"register_time\":65486,\"login_time\":9987676}}";
////    	JsonNode readTree = objectMapper.readTree(json);
////    	
////    	JsonNode jsonNode = readTree.get("name");
////    	System.out.println(jsonNode.isContainerNode());
////    	
////    	for (JsonNode jsonNode2 : jsonNode) {
////			System.out.println(jsonNode2.asText());
////		}
////    	
////    	System.out.println(jsonNode.size());
////    	Iterator<Entry<String, JsonNode>> fields = readTree.getFields();
////    	while(fields.hasNext()){
////    		Entry<String, JsonNode> next = fields.next();
////    		System.out.println(next.getKey()+":"+next.getValue()+"___"+next.getValue().isContainerNode()+".."+next.getValue().isValueNode());
////    		if(next.getValue().isContainerNode()){
////    			JsonNode value = next.getValue();
////    			System.out.println(value.get("erp").asText());
////    		}
////    	}
//    }
//    
//    
//    
////    /**
////
////     * <b>function:</b>java对象转换成xml文档
////
////     * 需要额外的jar包 stax2-api.jar
////
////     * @author hoojo
////
////     * @createDate 2010-11-23 下午06:11:21
////
////     */
////
////    @Test
////
////    public void writeObject2Xml() {
////
////        //stax2-api-3.0.2.jar
////
////        System.out.println("XmlMapper");
////        XmlMapper xml = new XmlMapper();
////
////        
////
////        try {
////
////            //javaBean转换成xml
////
////            //xml.writeValue(System.out, bean);
////
////            StringWriter sw = new StringWriter();
////
////            xml.writeValue(sw, bean);
////
////            System.out.println(sw.toString());
////
////            //List转换成xml
////
////            List<Customer> list = new ArrayList<Customer>();
////
////            list.add(bean);
////
////            list.add(bean);
////
////            System.out.println(xml.writeValueAsString(list));
////
////            
////
////            //Map转换xml文档
////
////            Map<String, Customer> map = new HashMap<String, Customer>();
////
////            map.put("A", bean);
////
////            map.put("B", bean);
////
////            System.out.println(xml.writeValueAsString(map));
////
////        } catch (JsonGenerationException e) {
////
////            e.printStackTrace();
////
////        } catch (JsonMappingException e) {
////
////            e.printStackTrace();
////
////        } catch (IOException e) {
////
////            e.printStackTrace();
////
////        }
////
////    }
//}
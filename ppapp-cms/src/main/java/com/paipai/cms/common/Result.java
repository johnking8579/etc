package com.paipai.cms.common;

public class Result {

	private boolean success = true;
	private String code;
	private Object msg;
	private Object data;
	
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Object getMsg() {
		return msg;
	}
	public void setMsg(Object msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Result(boolean success, String code, String msg, Object data) {
		super();
		this.success = success;
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Result [success=" + success + ", code=" + code + ", msg=" + msg
				+ ", data=" + data + "]";
	}
	
	
	
	
}

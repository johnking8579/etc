package com.paipai.cms.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.paipai.cms.dao.IUserInfoDao;
import com.paipai.cms.domain.Customer;

@Component
public class UserInfoDaoImpl implements IUserInfoDao{
	
	@Resource
	MongoTemplate mongoTemplate;
	
	public void saveOrUpdate(String erp)	{
		
		Criteria criteria = new Criteria("erp").is(erp);
		long now = new Date().getTime();
		List<Customer> find = mongoTemplate.find(new Query(criteria), Customer.class, "customer");
		if(null == find || find.size() == 0){
			Customer customer = new Customer();
			customer.setErp(erp);
			customer.setLogin_time(now);
			customer.setRegister_time(now);
			
			mongoTemplate.insert(customer, "customer");
		}else{
			mongoTemplate.findAndModify(new Query(criteria), new Update().set("login_time", now), Customer.class, "customer");
		}
	}
	
	
	
}

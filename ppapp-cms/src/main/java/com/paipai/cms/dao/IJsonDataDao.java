package com.paipai.cms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Query;

import com.paipai.cms.domain.JsonData;

public interface IJsonDataDao {
	
	
	public List<JsonData> findAll()	;
	
	public void unLock(String _id);
	
	public List<JsonData> findByParam(Query query);
	
	public JsonData findById(String _id)	;
	
	
	public List<JsonData> getByIds(List<String> ids)	;
	
	/**
	 * 根据id修改数据
	 * @param _id
	 * @param map
	 * @return
	 */
	public void updateById(String _id, Map<String, Object> map)	;
	
	
	public void saveDraft(String _id, String data);
	
	
	public void saveDemoAndRule(String _id, String jsonDataDemo, String jsonDataRule);
	
	public void readyPublish(String _id, String data);
	
}

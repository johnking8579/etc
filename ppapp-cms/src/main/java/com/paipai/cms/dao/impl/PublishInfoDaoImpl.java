package com.paipai.cms.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.paipai.cms.dao.IPublishInfoDao;
import com.paipai.cms.domain.PublishInfo;

@Component
public class PublishInfoDaoImpl implements IPublishInfoDao{
	
	@Resource
	MongoTemplate mongoTemplate;
	
	public List<PublishInfo> findAll()	{
		return mongoTemplate.findAll(PublishInfo.class);
	}
	
	
	public List<PublishInfo> findByParam(Query query)	{
		return mongoTemplate.find(query, PublishInfo.class);
	}
	
	public PublishInfo getById(String _id)	{
		return mongoTemplate.findById(_id, PublishInfo.class);
	}
	
	
}

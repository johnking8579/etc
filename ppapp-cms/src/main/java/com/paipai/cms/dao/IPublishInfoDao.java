package com.paipai.cms.dao;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;

import com.paipai.cms.domain.PublishInfo;

public interface IPublishInfoDao {
	
	public List<PublishInfo> findAll()	;
	
	
	public List<PublishInfo> findByParam(Query query)	;
	
	public PublishInfo getById(String _id)	;
	
	
}

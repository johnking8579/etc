package com.paipai.cms.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonProcessingException;

import com.fasterxml.jackson.databind.JsonNode;
import com.jd.common.sso.DotnetAuthenticationTicket;
import com.paipai.cms.domain.JsonData;

public interface IJsonDataService {
	
	public List<JsonData> findAll();
	
	public List<JsonData> findShow()	;
	
	/**
	 * 根据id查询数据
	 * @param _id
	 * @return
	 */
	public JsonData getById(String _id)	;
	
	/**
	 * 根据id查询配置的规则
	 * @param _id
	 * @return
	 */
	public Map<String, Object> getRuleById(String _id) throws JsonProcessingException, IOException;
	
	
	
	/**
	 * 根据id修改数据
	 * @param _id
	 * @return
	 */
	public void updateById(String _id, Map<String, Object> map);
	
	/**
	 * 根据id查询配置的规则
	 * @param _id
	 * @return
	 */
	
	
	public List<String> checkJsonData(String _id, JsonNode data) throws JsonProcessingException, IOException;
	
	
	
	public void saveDraft(String erp, String _id, String data) throws JsonProcessingException, IOException;
	
	public void saveDemoAndRule(String _id, String jsonDataDemo, String jsonDataRule);
	
	public void readyPublish(String erp, String _id, String data) throws Exception;
	
}

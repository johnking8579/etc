package com.paipai.cms.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.paipai.cms.common.CheckEnum;
import com.paipai.cms.dao.IJsonDataDao;
import com.paipai.cms.dao.IPublishInfoDao;
import com.paipai.cms.domain.JsonData;
import com.paipai.cms.domain.Rule;
import com.paipai.cms.service.IJsonDataService;

@Component
public class JsonDataServiceImpl implements IJsonDataService{
	
	@Resource
	private IJsonDataDao dao;
	@Resource
	private IPublishInfoDao publishInfoDao;
	
	
	public List<JsonData> findAll()	{
		return dao.findAll();
	}
	
	public List<JsonData> findShow()	{
		Criteria criteria = new Criteria("isShow").is(1);
		Query query = new Query(criteria);
		List<JsonData> findByParam = dao.findByParam(query);
		return findByParam;
	}
	
	/**
	 * 根据id查询数据
	 * @param _id
	 * @return
	 */
	public JsonData getById(String _id)	{
		return dao.findById(_id); 
	}
	
	
	
	/**
	 * 根据id修改数据
	 * @param _id
	 * @return
	 */
	public void updateById(String _id, Map<String, Object> map)	{
		dao.updateById(_id, map);
	}
	
	/**
	 * 根据id查询配置的规则
	 * @param _id
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public Map<String, Object> getRuleById(String _id) throws JsonProcessingException, IOException	{
		JsonData data = dao.findById(_id);
		com.fasterxml.jackson.databind.ObjectMapper mapper = new ObjectMapper();
		
		Map<String, Object> map = new HashMap<String, Object>();
		List<Rule> list= new ArrayList<Rule>();
		digui(mapper.readTree(data.getJson_data_rule()), 1, list);
		
		map.put("rule", list);
		map.put("demoData", mapper.readTree(data.getJson_data_demo()));
		map.put("draft", mapper.readTree(data.getJson_data_draft()));
		return map; 
	}

	/**
	 * 解析出配置的规则
	 * @param jsonObject
	 * @param level
	 * @param list
	 */
	private void digui(JsonNode jsonNode, int level, List<Rule> list){
		Iterator<Entry<String, JsonNode>> fields = jsonNode.fields();
    	while(fields.hasNext()){
    		Entry<String, JsonNode> entry = fields.next();
			Rule rule = new Rule();
			rule.setLevel(level);
			
			String key = entry.getKey();
			rule.setKey(StringUtils.leftPad(key, key.length() + 3 * (level - 1), "---"));
			
			JsonNode value = entry.getValue();
			
			int type = value.get("type").asInt();
			rule.setType(type);
			rule.setDesc(value.get("desc").asText());
			
			switch (type) {
			case 4://单选
			case 5://复选
				rule.setFill(value.get("fill").toString());
				list.add(rule);
				break;
				
			case 2://对象
			case 3://数组
				list.add(rule);
				//递归
				digui(value.get("fields"), level + 1, list);
				break;
				
			default://普通文本
				List<String> checkEnumByTag = CheckEnum.getCheckEnumByTag(value.get("validate"));
				rule.setValidate(checkEnumByTag.toString());
				list.add(rule);
				
				break;
			}
		}
	}
	
	
	public void saveDraft(String erp, String _id, String data) throws JsonProcessingException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.put("update_erp", erp);
		objectNode.put("update_time", new Date().getTime());
		objectNode.put("data", objectMapper.readTree(data));
		dao.saveDraft(_id, objectNode.toString());
	}
	
	public void readyPublish(String erp, String _id, String data) throws Exception{
		dao.unLock(_id);
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonData jsonData = dao.findById(_id);
		//校验是否可以准备发布
		String json_data_ready = jsonData.getJson_data_ready();
		if(StringUtils.isNotBlank(json_data_ready)){
			String update_erp = objectMapper.readTree(json_data_ready).get("update_erp").asText();
			if(jsonData.getState() != 1 && !erp.equals(update_erp)){
				throw new Exception("【" + update_erp + "】修改的内容还没有发布，您不能提交数据！");
			}
		}
		
		ObjectNode objectNode = objectMapper.createObjectNode();
		objectNode.put("update_erp", erp);
		objectNode.put("update_time", new Date().getTime());
		objectNode.put("data", objectMapper.readTree(data));
		dao.readyPublish(_id, objectNode.toString());
	}
	
	
	public List<String> checkJsonData(String _id, JsonNode data) throws JsonProcessingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		//规则
		List<String> list= new ArrayList<String>();
		
		check(mapper.readTree(dao.findById(_id).getJson_data_rule()), data, 1, list);
	
		return list;
	}
	
	/**
	 * 校验
	 */
	private void check(JsonNode rule, JsonNode data, int level, List<String> list){
		Iterator<Entry<String, JsonNode>> fields = rule.fields();
    	while(fields.hasNext()){
    		Entry<String, JsonNode> entry = fields.next();
			
			JsonNode jsonNode = data.get(entry.getKey());
			if(null == jsonNode || jsonNode.isNull()){
				list.add(entry.getKey() + ":键不存在！！");
				continue;
			}
			
			JsonNode value = entry.getValue();
			
			int type = value.get("type").asInt();
			
			switch (type) {
			case 4://单选
				
				String dataValue = data.get(entry.getKey()).asText();
				
				JsonNode fill = value.get("fill");
				List<String> keys = new ArrayList<String>();
				
				Iterator<Entry<String, JsonNode>> fields2 = fill.fields();
				while(fields2.hasNext()){
					Entry<String, JsonNode> next = fields2.next();
					keys.add(next.getKey());
				}
				
				List<String> asList = Arrays.asList(dataValue.split(","));
				
				boolean containsAll = keys.containsAll(asList);
				if(!containsAll){
					list.add(entry.getKey() + ":值不对，请从" + keys + "中取值");
				} 
				
				break;
				
			case 5://复选
				
				dataValue = data.get(entry.getKey()).asText();
				if(dataValue.indexOf("，") != -1){
					list.add(entry.getKey() + "：请用英文逗号分割！");
				}
				
				fill = value.get("fill");
				keys = new ArrayList<String>();
				fields2 = fill.fields();
				while(fields2.hasNext()){
					Entry<String, JsonNode> next = fields2.next();
					keys.add(next.getKey());
				}
				
				asList = Arrays.asList(dataValue.split(","));
				
				if(!keys.containsAll(asList)){
					list.add(entry.getKey() + ":值不对，请从" + keys + "中取值");
				} 
				
				break;
				
			case 2://对象
				JsonNode jsonNode2 = data.get(entry.getKey());
				if(!jsonNode2.isContainerNode()){
					list.add(entry.getKey() + ":不是对象！");
				}
				
				check(value.get("fields"), jsonNode2, level + 1, list);
				
				break;
				
			case 3://数组
				JsonNode jsonNode3 = data.get(entry.getKey());
				if(!jsonNode3.isContainerNode()){
					list.add(entry.getKey() + ":不是数组！");
				}else{
					for (JsonNode jsonNode4 : jsonNode3) {
						check(value.get("fields"), jsonNode4, level + 1, list);
					}
				}
				
				break;
				
			default://普通文本
				
				JsonNode validate = value.get("validate");
				
				for (JsonNode jsonNode4 : validate) {
					String validateTag = jsonNode4.asText();
					CheckEnum checkEnum = CheckEnum.getOneCheckEnumByTag(validateTag);
					if("notnull".equals(validateTag)){
						dataValue = data.get(entry.getKey()).asText();
						if(null == dataValue || "".equals(dataValue)){
							list.add(entry.getKey() + ":" + checkEnum.getMsg());
						}
					}else{
						Pattern pattern = Pattern.compile(checkEnum.getCheckRule());   
						Matcher matcher = pattern.matcher(data.get(entry.getKey()).asText());
						if(!matcher.matches()){
							list.add(entry.getKey() + ":" + checkEnum.getMsg());
						}
					}
				}
				break;
			}
			
		}
	}

	@Override
	public void saveDemoAndRule(String _id, String jsonDataDemo,
			String jsonDataRule){
		dao.saveDemoAndRule(_id, jsonDataDemo, jsonDataRule);
	}
}

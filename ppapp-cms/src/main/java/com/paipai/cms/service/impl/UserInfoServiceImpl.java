package com.paipai.cms.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.paipai.cms.dao.IUserInfoDao;
import com.paipai.cms.service.IUserInfoService;

@Component
public class UserInfoServiceImpl implements IUserInfoService{
	
	@Resource
	private IUserInfoDao dao;
	
	public void saveOrUpdateUser(String erp)	{
		if(null != erp)dao.saveOrUpdate(erp);
	}
	
}

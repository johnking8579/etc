package com.paipai.cms.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.paipai.cms.domain.PublishInfo;

public interface IPublishInfoService {
	
	public List<PublishInfo> findAll();
	
	public String createLocalData(String id) throws Exception;
	
	/**
	 * 发布数据
	 * @param id
	 * @param benv
	 * @param eenv
	 * @param erp
	 * @param cookie
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public JsonNode publishData(String id, String benv, String eenv, String erp, String cookie) throws JsonProcessingException, IOException ;
	
}

package com.paipai.cms.web.ctrl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.paipai.cms.common.Config;

public class BaseController {
	
	
	protected String getCookie(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if(cookie.getName().equals(key)){
				if(Config.sysDebug()){
					return "8D801A5E097058492DD48FC668A1B034E6E72E9D58F21A8764AD665B993DA9B915636176EB8D4DCFDB04371F3CC56B96036E79F9D21CAF4AD7C834ED8B52E21843C7F47274827004B3B2F7FCD46CCD76";
				}else{
					return cookie.getValue();
				}
			}
		}
		return null;
	}
}

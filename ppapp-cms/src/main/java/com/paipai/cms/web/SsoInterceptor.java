package com.paipai.cms.web;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jd.common.sso.DotnetAuthenticationTicket;
import com.jd.common.sso.DotnetAuthenticationUtil;
import com.jd.common.sso.SsoTool;
import com.paipai.cms.common.Config;

/**
 * 单点登录拦截器
 * @author JingYing
 * @date 2015年2月2日
 */
public class SsoInterceptor implements HandlerInterceptor	{
	static Logger log = LoggerFactory.getLogger(SsoInterceptor.class);
	
	private SsoTool ssoTool;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if(Config.sysDebug())	{
			if(!Config.sysDebugCookie().equals(getCookie(request, ssoTool.getCookieKey())))	{
				Cookie c = new Cookie(ssoTool.getCookieKey(), Config.sysDebugCookie());
				c.setPath("/");	
				c.setMaxAge(Integer.MAX_VALUE);
				//c.setDomain(".jd.com");	//不设置domain,以便使用任意IP都可以得到cookie
				//c.setHttpOnly(true);		//tomcat6不支持	
				response.addCookie(c);
				response.sendRedirect(genReqUrl(request));
				log.info("debug模式下不进行ERP登录. 初次使用时写入一个伪造的cookie,并重定向一次以刷新cookie");
				return false;
			} else	{
				return true;
			}
		} else {
			boolean isRedirect = false;
			String cookie = getCookie(request, ssoTool.getCookieKey());
			if(cookie != null)	{
				DotnetAuthenticationTicket ticket = DotnetAuthenticationUtil.getFormsAuthenticationTicket(cookie, ssoTool.getSecretKey());
				if(ticket.getUsername() == null || "".equals(ticket.getUsername()) || ticket.isExpired())	{
					isRedirect = true;
				} 
			} else	{
				isRedirect = true;
			}
			
			if(isRedirect)	{
				response.sendRedirect(ssoTool.genLoginUrl(genReqUrl(request)));
				return false;
			} else	{
				return true;
			}
		}
	}
	
	private String genReqUrl(HttpServletRequest request)	{
		StringBuffer url = request.getRequestURL();
		if(request.getQueryString() != null)
			url.append("?").append(request.getQueryString());
		return url.toString();
	}
	
	private String getCookie(HttpServletRequest request, String key) {
		Cookie[] cookies = request.getCookies();
		if(cookies != null)	{
	        for (Cookie c : cookies) {
	            if (c.getName().equals(key)) {
	                return c.getValue();
	            }
	        }
		}
        return null;
    }
	
	
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	public void setSsoTool(SsoTool ssoTool) {
		this.ssoTool = ssoTool;
	}
}

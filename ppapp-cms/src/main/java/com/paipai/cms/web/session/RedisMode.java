package com.paipai.cms.web.session;

import com.paipai.cms.common.redis.RedisTemplate;

/**
 * 使用redis, 实现session的效果
 */
public class RedisMode extends MockSession{
	
	private RedisTemplate redisTemplate;
	
	public RedisMode(RedisTemplate redisTemplate)	{
		this.redisTemplate = redisTemplate;
	}

	@Override
	public String getAsString(String bizkey) {
		return redisTemplate.getString(genRedisKey(bizkey));
	}

	@Override
	public void set(String bizkey, Object value) {
		redisTemplate.set(genRedisKey(bizkey), value);	//不指定过期时间,越长越好
	}

	@Override
	public Object get(String bizkey) {
		return redisTemplate.getObject(genRedisKey(bizkey));
	}
	
	//=session/bizkey//bjadmin
	private String genRedisKey(String bizkey)	{
		return bizkey != null ? "session/" + bizkey + "//" + super.getErpUser() : null;
	}

}

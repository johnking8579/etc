package com.paipai.cms.web.ctrl;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.paipai.cms.service.IUserInfoService;
import com.paipai.cms.web.session.MockSession;
import com.paipai.cms.web.session.SessionBinder;

@Controller
@RequestMapping("")
public class IndexController extends BaseController{
	
	@Resource
	private IUserInfoService userInfoService;
	
	/**
	 * 跳转到index页面，并且添加用户或者更新用户的登陆时间
	 * @param session
	 * @return
	 */
	@RequestMapping("index")
	public ModelAndView toIndex(@SessionBinder MockSession session)	{
		userInfoService.saveOrUpdateUser(session.getErpUser());
		return new ModelAndView("frame/index").addObject("erp", session.getErpUser());
	}
	
}


package com.paipai.cms.web.session;

import javax.servlet.http.HttpServletRequest;

/**
 * 原生session实现
 */
public class SessionMode extends MockSession {
	
	private HttpServletRequest req;
	
	public SessionMode(HttpServletRequest req){
		this.req = req;
	}

	@Override
	public void set(String key, Object value) {
		req.getSession().setAttribute(key, value);
	}

	@Override
	public String getAsString(String key) {
		Object o = req.getSession().getAttribute(key);
		if(o instanceof String)
			return (String)o;
		else
			return String.valueOf(o);
	}

	@Override
	public Object get(String key) {
		return req.getSession().getAttribute(key);
	}


}

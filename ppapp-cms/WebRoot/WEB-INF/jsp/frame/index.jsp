<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
 "http://www.w3.org/TR/html4/loose.dtd" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="<%=basePath %>/static/jquery/jquery-2.1.3.js"></script>

<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/lib/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>/static/frame/stylesheets/theme.css">
    
    
    <style type="text/css">
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
        
        #log {
		    cursor: pointer;
		    border: 0px none;
		    bottom: 124px;
		    min-width: 40px;
		    height: 28px;
		    width: 90px;
		    margin: 0px;
		    padding: 0px;
		    position: fixed;
		    right: 40px;
		    border-radius: 0px;
		    line-height: 16px;
		    z-index: 899;
		}
		
		.icon-caret-down:before           { content: "\f0d7"; }
    </style>
    
    
<script type="text/javascript">
	
function loadEditPage(_id){
	$("#mainIframe").attr("src","<%=basePath %>/publishInfo/toEdit.php?_id=" + _id);
}

function loadMainTainEditPage(_id){
	$("#mainIframe").attr("src","<%=basePath %>/publishInfo/toMainTainEdit.php?_id=" + _id);
}

function dataPublish(){
	$("#mainIframe").attr("src","<%=basePath %>/publishInfo/toPublish.php");
}

function reinitIframe(){
	var iframe = document.getElementById("mainIframe");
	try{
		var bHeight = iframe.contentWindow.document.body.scrollHeight;
		var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
		var height = Math.min(bHeight, dHeight);
		iframe.height = height + 30;
	}catch (ex){}
}

function init(){
	if("${erp == 'liubenlong3'}" == "true"){
		$("#firstPageMainTainDiv").show();
		$("#firstPageMainTain").show();
	}
	$.ajax({
		   type: "POST",
		   url: "<%=basePath %>/jsonData/findShow.php",
		   success: function(result){
			   var data = result.data;
// 			   	var o = JSON.parse(msg);
			   	for(var i = 0 ; i < data.length; i ++){
				   	$("#dashboard-menu").append("<li onclick=\"loadEditPage('" + data[i]._id + "')\"><a href=\"#\">" + data[i].desc + "</a></li>");
				   	if("${erp == 'liubenlong3'}" == "true"){
				   		$("#firstPageMainTain").append("<li onclick=\"loadMainTainEditPage('" + data[i]._id + "')\"><a href=\"#\">" + data[i].desc + "</a></li>");
					}
			   	}
		   }
	});
}

	
window.setInterval("reinitIframe()", 200); //设置每200毫秒监视改变一次
</script>
</head>
<body onload="init()">

	 <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <ul class="nav pull-right">
                    
                    <li id="fat-menu" class="dropdown">
                        <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i> ${erp }
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="http://erp.jd.com/login/logout">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">PPAPP-CMS</span></a>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        
        <div class="row-fluid">
            <div class="span3">
                <div class="sidebar-nav">
                	<div class="nav-header" data-toggle="collapse" data-target="#data-publish"><i class="icon-dashboard"></i>数据发布</div>
                	<ul id="#data-publish" class="nav nav-list collapse in">
                		<li onclick="dataPublish()"><a href="#">数据发布</a></li>
                    </ul>
	                    
                  	<div class="nav-header" data-toggle="collapse" data-target="#dashboard-menu"><i class="icon-dashboard"></i>APP首页</div>
                    <ul id="dashboard-menu" class="nav nav-list collapse in"></ul>
                    
                    <div id="firstPageMainTainDiv" class="nav-header" style="display: none;" data-toggle="collapse" data-target="#firstPageMainTain"><i class="icon-dashboard"></i>APP首页数据维护</div>
                    <ul id="firstPageMainTain" style="display: none;" class="nav nav-list collapse in"></ul>
                    
	                <div class="nav-header" data-toggle="collapse" data-target="#accounts-menu"><i class="icon-briefcase"></i>系统管理</div>
	                <ul id="accounts-menu" class="nav nav-list collapse in">
	                  <li ><a href="#">角色管理</a></li>
	                  <li ><a href="#">权限管理</a></li>
	                </ul>
           	 	</div>
       	 	</div>
        <div class="span9">
			<iframe id="mainIframe" style="width: 100%;margin-top: 50px;" frameborder=0 ></iframe>
<!-- 			<div class="copyrights">Collect from <a href="http://www.cssmoban.com/"  title="网站模板">网站模板</a></div> -->
        </div>
    </div>
</div>
    


    <script src="<%=basePath %>/static/frame/lib/bootstrap/js/bootstrap.js"></script>
	
</body>
</html>
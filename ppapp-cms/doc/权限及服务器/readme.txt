
服务器清单


# 京东IDC下用到的服务器

线上IDC的服务器（docker虚机）

Tomcat：
172.16.116.69
172.16.116.67
DB
172.16.116.68
172.16.116.58



# 京东IDC下gamma环境的服务器

IDC下 docker 测试机：  注意， 不用ssh 登录， 用 g {ip} 方式登录。
172.16.116.168        tomcat     用于移动端测试   /export/servers/tomcat6.0.33/bin   docker自带了
test.pp.xpay.paipai.com 对应 /172.16.116.168:1601

172.16.116.169        tomcat     用于后台研发自测   /export/servers/tomcat6.0.33/bin  docker自带了
dev.pp.xpay.paipai.com 对应 172.16.116.169 

172.16.116.170        mysql		默认用户名 root，密码为空  /usr/bin/mysql
172.16.116.171        redis + go 1.4 编译环境  GOPATH="/export/volume/gocodes"